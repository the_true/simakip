FROM python:2.7-alpine

RUN apk add --update \
    python-dev \
    bash \
    py-pip \
    supervisor \
  && pip install celery==3.1.25 \
  && pip install requests==2.21.0 \
  && pip install redis==2.10.6 \
  && rm -rf /var/cache/apk/*

RUN mkdir -p /var/run/supervisord
RUN mkdir -p /var/log/supervisord
# RUN chown simakip:simakip -R /var/run/supervisor/