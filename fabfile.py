from fabric.api import *
from fabric.decorators import *
import os


def dev():
    env.user = "root"
    env.password = "uhamka123"
    env.hosts = ['simakip.uhamka.ac.id']

def ucup():
    env.user = "root"
    env.key_filename = '/home/jun/.ssh/id_rsa'
    env.hosts = ['simakip.getality.com']



def deploy(commit=""):
    with cd("/var/www/html/devel"):
        if commit:
            run("git checkout develop")
            run("git pull origin develop")
            run("git reset --hard "+commit)
        else:
            run("git checkout develop")
            run("git pull origin develop")

        run("cp application/config/database.php.devel application/config/database.php")
        run("cp application/config/config.php.devel application/config/config.php")
        # run("cp .env.devel .env")
        # run("composer install")

def deploy_ucup(commit=""):
    with cd("/var/www/simakip"):
        run("git reset --hard")
        run("git checkout develop")
        run("git pull origin develop")


        run("cp application/config/database.php.devel application/config/database.php")
        run("cp application/config/config.php.devel application/config/config.php")
        # run("sudo cp /var/www/devel/simakip/application/libraries/Twig.php /var/www/simakip/application/libraries/Twig.php")

# fab dev deploy_hotfix:"hotfix_branch"
def deploy_hotfix(hotfix_branch=""):
    with cd("/var/www/html/devel"):
        run("git fetch")
        run("git checkout hotfix/"+ hotfix_branch)
        run("git pull origin hotfix/"+ hotfix_branch)

        run("cp application/config/database.php.devel application/config/database.php")
        run("cp application/config/config.php.devel application/config/config.php")

def deploy_master(commit=""):
    with cd("/var/www/html/"):
        run("git checkout master")
        run("git pull origin master")

        run("cp application/config/database.php.production application/config/database.php")
        run("cp application/config/config.php.production application/config/config.php")

