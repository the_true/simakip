<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once "application/core/MY_BaseController.php";
class Download extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $type = $this->input->get("type");
        $id = $this->input->get("id");

        if ($type === "pengumuman") {
            $this->pengumuman($id);
        } else if ($type === "jurnal") {
            $this->jurnal($id);
        } else if ($type === "forumilmiah") {
            $this->forum_ilmiah($id);
        } else if ($type === "hki") {
            $this->hki($id);
        } else if ($type === "luaranlain") {
            $this->luaranlain($id);
        } else if ($type === "penelitianhibah") {
            $this->penelitianhibah($id);
        } else if ($type === "penelitianlaporan") {
            $nomor = $this->input->get("nomor");
            if ($nomor == '1') {
                $this->penelitianlaporanwatermark($id, $nomor);
            } else {
                $this->penelitianlaporan($id, $nomor);
            }

        } else if ($type === "pengabdianlaporan") {
            $nomor = $this->input->get("nomor");
            if ($nomor == '1') {
                $this->pengabdianlaporanwatermark($id, $nomor);
            } else {
                $this->pengabdianlaporan($id, $nomor);
            }

        } else if ($type === "penelitian") {
            $no = $this->input->get("no");
            $this->penelitianusulan($id, $no);
        } else if ($type === "borangluaran") {
            $this->borangluaran($id);
        } else if ($type === "borangluaranpengabdian") {
            $this->borangluaranpengabdian($id);
        } else if ($type === "pengabdian") {
            $nomor = $this->input->get("nomor");
            $this->pengabdian($id, $nomor);
        }
    }

    private function pengumuman($id)
    {
        $this->load->model('News_meta');
        $tautan = News_meta::find($id);
        $ori_file = $tautan->path;
        $ori_path = FCPATH . "uploads/pengumuman/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $new_file_name = $tautan->body . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function jurnal($id)
    {
        $jenis = $this->input->get("jenis");
        if ($jenis == "pengabdian") {
            $this->load->model('luaran_pengabdian/Jurnal');
        } else {
            $this->load->model('luaran_penelitian/Jurnal');
        }
        $doc = Jurnal::find($id);
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/jurnals/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function forum_ilmiah($id)
    {
        $jenis = $this->input->get("jenis");
        if ($jenis == "pengabdian") {
            $this->load->model('luaran_pengabdian/Forum_ilmiah');
        } else {
            $this->load->model('luaran_penelitian/Forum_ilmiah');
        }
        $doc = Forum_ilmiah::find($id);
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/forumilmiahs/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function hki($id)
    {
		$jenis = $this->input->get("jenis");
        if ($jenis == "pengabdian") {
            $this->load->model('luaran_pengabdian/Hki');
        } else {
            $this->load->model('luaran_penelitian/Hki');
        }
        $doc = HKI::find($id);
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/hkis/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function luaranlain($id)
    {
		$jenis = $this->input->get("jenis");
        if ($jenis == "pengabdian") {
            $this->load->model('luaran_pengabdian/Luaran_lain');
        } else {
            $this->load->model('luaran_penelitian/Luaran_lain');
        }
        $doc = Luaran_lain::find($id);
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/luaranlains/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function penelitianhibah($id)
    {
		$jenis = $this->input->get("jenis");
        if ($jenis == "pengabdian") {
			$this->load->model('luaran_pengabdian/Pengabdian_mandiri');
			$doc = Pengabdian_mandiri::find($id);
        } else {
			$this->load->model('luaran_penelitian/Penelitian_hibah');
			$doc = Penelitian_hibah::find($id);
        }
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/penelitianhibahs/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function penelitianlaporan($id, $type)
    {
        $this->load->model('Penelitian_laporan');
        $this->load->model('Penelitian');
        $this->load->model('Dosen');
        $doc = Penelitian_laporan::find($id);
        $doc->load('penelitian')->load('penelitian.dosen');
        $ori_file = $doc->laporan;
        $ori_path = FCPATH . "uploads/penelitian_laporan/" . $ori_file;
        if ($type == (int) 2) {
            $ori_file = $doc->artikel;
            $ori_path = FCPATH . "uploads/penelitian_artikel/" . $ori_file;
        }
	else if ($type == (int) 3) {
            $ori_file = $doc->laporan_luaran_tambahan;
            $ori_path = FCPATH . "uploads/penelitian_laporan_luaran_tambahan/" . $ori_file;
        }
        $doc = $doc->toArray();
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc["penelitian"]["judul"]), 0, 4));
        $new_file_name = $doc["penelitian"]["dosen"]["nama_lengkap"] . "-" . $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function pengabdian($id, $type)
    {
        $this->load->model('Pengabdian');
        $this->load->model('Dosen');
        $doc = Pengabdian::find($id);
        $doc->load('dosen');
        $ori_file = $doc->berkas;
        if ($type == 2) {
            $ori_file = $doc->berkas_perbaikan;
        }
        $ori_path = FCPATH . "uploads/pengabdian/" . $ori_file;
        $doc = $doc->toArray();
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc["judul"]), 0, 4));
        $new_file_name = $doc["dosen"]["nama_lengkap"] . "-" . $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function pengabdianlaporan($id, $type)
    {
        $this->load->model('Pengabdian_laporan');
        $this->load->model('Pengabdian');
        $this->load->model('Dosen');
        $doc = Pengabdian_laporan::find($id);
        if ($doc == null) {
            echo "<pre>"; print_r("Laporan pengabdian kosong"); echo "</pre>"; die();
        }
        $doc->load('pengabdian')->load('pengabdian.dosen');
        $ori_file = $doc->laporan;
        $ori_path = FCPATH . "uploads/pengabdian_laporan/" . $ori_file;
        if ($type == (int) 2) {
            $ori_file = $doc->artikel;
            $ori_path = FCPATH . "uploads/pengabdian_artikel/" . $ori_file;
        }
        $doc = $doc->toArray();
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc["pengabdian"]["judul"]), 0, 4));
        $new_file_name = $doc["pengabdian"]["dosen"]["nama_lengkap"] . "-" . $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function penelitianlaporanwatermark($id, $type)
    {
        $this->load->model('Penelitian_laporan');
        $this->load->model('Penelitian');
        $this->load->model('Dosen');
        $doc = Penelitian_laporan::find($id);
        $doc->load('penelitian')->load('penelitian.dosen');
        $ori_file = $doc->laporan;
        $ori_path = FCPATH . "uploads/penelitian_laporan/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);

        $new_file_name = "Download penelitian laporan." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);

        // $this->load->helper('pdf');
        // watermarkpdf();
        // $pdf = new PDF($ori_path,'http://simakip.uhamka.ac.id/assets/images/watermark.png');
        // $pdf->watermark();
        // $pdf->Output();

    }

    private function pengabdianlaporanwatermark($id, $type)
    {
        $this->load->model('Pengabdian_laporan');
        $this->load->model('Pengabdian');
        $this->load->model('Dosen');
        $doc = Pengabdian_laporan::find($id);
        if ($doc == null) {
            echo "<pre>"; print_r("Laporan pengabdian kosong"); echo "</pre>"; die();
        }
        $doc->load('pengabdian')->load('pengabdian.dosen');
        $ori_file = $doc->laporan;
        $ori_path = FCPATH . "uploads/pengabdian_laporan/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);

        $new_file_name = "Download pengabdian laporan." . $ext;
        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);

        // $this->load->helper('pdf');
        // watermarkpdf();
        // $pdf = new PDF($ori_path,'http://simakip.uhamka.ac.id/assets/images/watermark.png');
        // $pdf->watermark();
        // $pdf->Output();

    }

    private function borangluaran($id)
    {
        $this->load->model('Monev_luaran');
        $doc = Monev_luaran::find($id);
        // echo $doc->content->judul;die;
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/borangluaran/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->content->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function borangluaranpengabdian($id)
    {
        $this->load->model('Pengabdian_monev_luaran');
        $doc = Pengabdian_monev_luaran::find($id);
        // echo $doc->content->judul;die;
        $ori_file = $doc->berkas;
        $ori_path = FCPATH . "uploads/borangluaran/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->content->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

    private function penelitianusulan($id, $type)
    {
        $this->load->model('Penelitian');
        $doc = Penelitian::find($id);
        $ori_file = $doc->berkas;
        if ($type == (int) 2) {
            $ori_file = $doc->berkas_perbaikan;
        }
        $ori_path = FCPATH . "uploads/penelitians/" . $ori_file;
        $ext = pathinfo($ori_file)['extension'];
        $content_type = mime_content_type($ori_path);
        $first_four_title = implode(' ', array_slice(explode(' ', $doc->judul), 0, 4));
        $new_file_name = $first_four_title . "-" . Date("dmY") . "." . $ext;

        header('Content-type: ' . $content_type);
        header('Content-Disposition: inline; filename="' . $new_file_name . '"');
        readfile($ori_path);
    }

}
