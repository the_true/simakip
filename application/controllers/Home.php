<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once "application/core/MY_BaseController.php";

use Illuminate\Database\Query\Expression as raw;

class Home extends MY_BaseController
{

    /**
     * @category Libraries
     * @package  CodeIgniter 3.0
     * @author   Yp <purwantoyudi42@gmail.com>
     * @link     https://timexstudio.com
     * @license  Protected
     */
    public $tahun;
    private $month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Tautan');
        $this->load->model('Dosen');

        $this->load->model('Tahun_kegiatan');
        $this->load->model('Program_studi');
        $this->load->model('Fakultas');
        $this->load->model('Jenis_hki');
        $this->load->model('Jenis_luaran');
        $this->load->model('Jurnal_anggota');
        $this->load->model('Penelitian');
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Penelitian_anggota");
        $this->load->model("Penelitian_reviewer");
        $this->load->model("Penelitian_review");
        $this->load->model("Jabatan_akademik");
        $this->load->model("Jabatan_fungsi");
        $this->load->model('Pengabdian');
        $this->load->model('Penelitian_laporan');
    }
    public function index()
    {
        $user = $this->is_login();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            redirect("home/tab3");
            die;
        }
        $tahun_get = $this->input->get('tahun');
        if (!$tahun_get) {
            $tahunkeg = Tahun_kegiatan::where('tahun', '=', date('Y'))->where('isdelete', '=', '0')->first();
            if (!$tahunkeg) {
                $tahunkeg = Tahun_kegiatan::where('tahun', '=', (int) date('Y') - 1)->where('isdelete', '=', '0')->first();
            }

            $this->tahun = $tahunkeg->id;
        } else {
            $tahunkeg = Tahun_kegiatan::find($tahun_get);
            $this->tahun = $tahun_get;
        }
        $data = array();
        $this->getJurnalCount($user, $data);
        $this->getBukuAjarCount($user, $data);
        $this->getForumIlmiahCount($user, $data);
        $this->getHkiCount($user, $data);
        $this->getLuaranLainCount($user, $data);
        $this->getPenyelenggaraForumIlmiahCount($user, $data);
        $this->getPenelitianHibahCount($user, $data);
        $this->getPenelitianCount($user, $data);

        $data["home"] = "active";
        $tahun = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun', 'DESC')->get();
        $data['tahund'] = $tahun->toArray();
        $data['tahunds'] = $tahunkeg->toArray();
        // echo json_decode(json)
        // die;
        // $data = $this->pengumuman($data);
        $this->twig->display('home/tab1', $data);
    }

    public function tab2()
    {
        $user = $this->is_login();
        $tahun_get = $this->input->get('tahun');
        if (!$tahun_get) {
            $tahunkeg = Tahun_kegiatan::where('tahun', '=', date('Y'))->where('isdelete', '=', '0')->first();
            if (!$tahunkeg) {
                $tahunkeg = Tahun_kegiatan::where('tahun', '=', (int) date('Y') - 1)->where('isdelete', '=', '0')->first();
            }
            $this->tahun = $tahunkeg->id;
        } else {
            $tahunkeg = Tahun_kegiatan::find($tahun_get);
            $this->tahun = $tahun_get;
        }
        $data = array();
        $data = $this->pengumuman($data);
        $data["home"] = "active";
        $tahun = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun', 'DESC')->get();
        $data['tahund'] = $tahun->toArray();
        $data['tahunds'] = $tahunkeg->toArray();
        $this->twig->display('home/tab2', $data);
    }

    public function tab3()
    {
        $user = $this->is_login();
        $tahun_get = $this->input->get('tahun');
        if (!$tahun_get) {
            $tahunkeg = Tahun_kegiatan::where('tahun', '=', date('Y'))->where('isdelete', '=', '0')->first();
            if (!$tahunkeg) {
                $tahunkeg = Tahun_kegiatan::where('tahun', '=', (int) date('Y') - 1)->where('isdelete', '=', '0')->first();
            }
            $this->tahun = $tahunkeg->id;
        } else {
            $tahunkeg = Tahun_kegiatan::find($tahun_get);
            $this->tahun = $tahun_get;
        }
        $data = array();
        $this->getPengabdianJurnalCount($user, $data);
        $this->getPengabdianBukuAjarCount($user, $data);
        $this->getPengabdianForumIlmiahCount($user, $data);
        $this->getPengabdianHkiCount($user, $data);
        $this->getPengabdianLuaranLainCount($user, $data);
        $this->getPengabdianPenyelenggaraForumIlmiahCount($user, $data);
        $this->getPengabdianPenelitianHibahCount($user, $data);

        $this->getPengabdianCount($user, $data);
        $data["home"] = "active";
        $tahun = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun', 'DESC')->get();
        $data['tahund'] = $tahun->toArray();
        $data['tahunds'] = $tahunkeg->toArray();

        $this->twig->display('home/tab3', $data);
    }

    public function tab4()
    {
        $user = $this->is_login();
        $tahun_get = $this->input->get('tahun');
        if (!$tahun_get) {
            $tahunkeg = Tahun_kegiatan::where('tahun', '=', date('Y'))->where('isdelete', '=', '0')->first();
            if (!$tahunkeg) {
                $tahunkeg = Tahun_kegiatan::where('tahun', '=', (int) date('Y') - 1)->where('isdelete', '=', '0')->first();
            }
            $this->tahun = $tahunkeg->id;
        } else {
            $tahunkeg = Tahun_kegiatan::find($tahun_get);
            $this->tahun = $tahun_get;
        }
        $data = array();
        // $this->getPengabdianJurnalCount($user, $data);
        // $this->getPengabdianBukuAjarCount($user, $data);
        // $this->getPengabdianForumIlmiahCount($user, $data);
        // $this->getPengabdianHkiCount($user, $data);
        // $this->getPengabdianLuaranLainCount($user, $data);
        // $this->getPengabdianPenyelenggaraForumIlmiahCount($user, $data);

        //new
        $this->getPenelitianMandiriCount($user, $data);
        $this->getPenelitianInternalCount($user, $data);
        // $this->getPengabdianPenelitianHibahCount($user, $data);
        $this->getPenelitianInternasionalCount($user, $data);//??
        $this->getPenelitianNasionalCount($user, $data);//??
        $this->getPenelitianKemenBRINCount($user, $data);//??
        $this->getPenelitianCount($user, $data);
        $data['totalanggaran']=number_format($data['sum1']+$data['sum2']+$data['sum3']+$data['sum4']+$data['sum5'],0,',','.');
        
        

        $this->getPengabdianCount($user, $data);
        $data["home"] = "active";
        $tahun = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun', 'DESC')->get();
        $data['tahund'] = $tahun->toArray();
        $data['tahunds'] = $tahunkeg->toArray();

        $this->twig->display('home/tab4', $data);
    }

    private function pengumuman($data)
    {
        $this->load->model('News');
        $this->load->model('News_meta');
        $tahun_get = $this->input->get('tahunp');
        $bulan_get = $this->input->get('bulan');

        $news = News::where('isdelete', '=', '0');
        if (isset($tahun_get) && $tahun_get != '') {
            $news->whereRaw("extract(YEAR from created_at) = ?", [$tahun_get]);
        } else {
            $news->whereRaw("extract(YEAR from created_at) = ?", [Date('Y')]);
        }

        if (isset($bulan_get) && $bulan_get != '') {
            $news->whereRaw("extract(MONTH from created_at) = ?", [$bulan_get]);
        } else {
            $news->whereRaw("extract(MONTH from created_at) = ?", [Date('n')]);
        }

        $news = $news->orderBy('created_at', 'DESC')->get();
        $news->load('news_meta');
        $tautan = Tautan::where("isdelete", "=", "0")->get();
        $x = array("news" => $news->toArray(), "beranda" => "active", "items" => $tautan->toArray());
        $tahun = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun', 'DESC')->get();
        $x['tahun'] = $tahun->toArray();
        $x['bulan'] = isset($bulan_get) && $bulan_get != '' ? $bulan_get : Date('n');
        $x['bulan_word'] = $this->month[$x['bulan'] - 1];
        $x['tahuns'] = isset($tahun_get) && $tahun_get != '' ? $tahun_get : Date('Y');
        $data = array_merge($data, $x);
        return $data;
    }

    public function pdf()
    {

        $this->load->helper('pdf_helper');
        tcpdf();
        $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $title = "PDF Report";
        $obj_pdf->SetTitle($title);
        $obj_pdf->SetDefaultMonospacedFont('helvetica');
        $obj_pdf->SetHeaderMargin(0);
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $obj_pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $obj_pdf->SetFont('helvetica', '', 9);
        $obj_pdf->setFontSubsetting(false);
        $obj_pdf->setListIndentWidth(1);
        $obj_pdf->AddPage();

        ob_start();
        $data = $this->getDataPDF();
        $this->load->view('pdf_profile', $data);
        $content = ob_get_contents();
        ob_end_clean();
        $obj_pdf->writeHTML($content, true, false, true, false, '');
        $obj_pdf->Output('Laporan Kinerja-' . $this->is_login()['nidn'] . '-' . date('dmY') . '.pdf', 'I');
    }

    public function pdfpengabdian()
    {

        $this->load->helper('pdf_helper');
        tcpdf();
        $obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $title = "PDF Report";
        $obj_pdf->SetTitle($title);
        $obj_pdf->SetDefaultMonospacedFont('helvetica');
        $obj_pdf->SetHeaderMargin(0);
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $obj_pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $obj_pdf->SetFont('helvetica', '', 9);
        $obj_pdf->setFontSubsetting(false);
        $obj_pdf->setListIndentWidth(1);
        $obj_pdf->AddPage();

        ob_start();
        $data = $this->getDataPengabdianPDF();
        $this->load->view('pdf_profile_pengabdian', $data);
        $content = ob_get_contents();
        ob_end_clean();
        $obj_pdf->writeHTML($content, true, false, true, false, '');
        $obj_pdf->Output('Laporan Kinerja-' . $this->is_login()['nidn'] . '-' . date('dmY') . '.pdf', 'I');
    }

    private function getDataPDF()
    {
        $this->load->model('Jurnal_anggota');
        $this->load->model('Forum_ilmiah_anggota');
        $this->load->model('luaran_penelitian/Jurnal');
        $this->load->model('luaran_penelitian/Buku_ajar');
        $this->load->model('luaran_penelitian/Forum_ilmiah');
        $this->load->model('luaran_penelitian/Hki');
        $this->load->model('luaran_penelitian/Luaran_lain');
        $this->load->model('luaran_penelitian/Penyelenggara_forum_ilmiah');
        $this->load->model('luaran_penelitian/Penelitian_hibah');
        $this->load->model('Penelitian_dikti');

        $user = $this->is_login();
        $data = array();
        $data["dosen"] = $user;

        $penelitian_hibah = Penelitian_hibah::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->get();
        $penelitian_hibah->load('dosen');
        $penelitian_hibah->load('tahun');
        $data["penelitian_hibah"] = $penelitian_hibah->toArray();

        $buku_ajar = Buku_ajar::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->with('dosen')->with('dosen.program_studi')->with('dosen.fakultas')->get();
        $data["buku_ajar"] = $buku_ajar->toArray();

        $jurnal = Jurnal::where('isdelete', '=', '0')
            ->where('isvalidate', '=', '1')
            ->whereHas("jurnal_anggota", function ($q) use ($user) {
                $q->where('dosen', $user["id"]);
            })
            ->with('jurnal_anggota.dosen.fakultas')
            ->with('jurnal_anggota.dosen.program_studi')
            ->get();
        $data["jurnal"] = $jurnal->toArray();

        $forum_ilmiah = Forum_ilmiah::where('isdelete', '=', '0')
            ->where('isvalidate', '=', '1')
            ->whereHas("forum_ilmiah_anggota", function ($q) use ($user) {
                $q->where('dosen', $user["id"]);
            })
            ->with('forum_ilmiah_anggota.dosen.program_studi')
            ->get();
        $data["forum_ilmiah"] = $forum_ilmiah->toArray();

        $hki = Hki::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->get();
        $hki->load('dosen');
        $hki->load('jenis_hki');
        $data["hki"] = $hki->toArray();

        $luaran_lain = Luaran_lain::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->with("dosen")->with("dosen.fakultas")->with("dosen.program_studi")->get();
        $luaran_lain->load('jenis_luaran');
        $data["luaran_lain"] = $luaran_lain->toArray();

        $penyelenggara_forum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1');
        $penyelenggara_forum_ilmiah->where('dosen', '=', $user["id"]);
        $penyelenggara_forum_ilmiah = $penyelenggara_forum_ilmiah->get();
        $penyelenggara_forum_ilmiah->load('dosen');
        $penyelenggara_forum_ilmiah->load('tahun_kegiatan');
        $data["peny_forum_ilmiah"] = $penyelenggara_forum_ilmiah->toArray();

        $penelitian_dikti = Penelitian_dikti::where("isdelete", "=", "0")->get();
        $data["penelitian_dikti"] = $penelitian_dikti->toArray();

        $penelitian = Penelitian::where("isdelete", "=", "0")->where('isvalid', '=', '1')->with('dosen.jabatan_akademik')
            ->with('dosen.jabatan_fungsional')
            ->with('dosen.program_studi')
            ->with('dosen_menyetujui')
            ->with('dosen_menyetujui.jabatan_akademik')
            ->with('dosen_menyetujui.program_studi')
            ->with('dosen_menyetujui.jabatan_fungsional')
            ->with('dosen_mengetahui')
            ->with('dosen_mengetahui.jabatan_akademik')
            ->with('dosen_mengetahui.program_studi')
            ->with('dosen_mengetahui.jabatan_fungsional')
            ->with('anggota')->with('anggota.dosen')->with('Penelitian_review')->with('Penelitian_reviewer')->orderBy('created_at', 'DESC');
        // $penelitian = $this->filter($penelitian,$data);
        if ($this->is_login()['akses']['nama'] == 'Dosen') {
            $penelitian->where(function ($q) {
                $q->where('dosen', '=', $this->is_login()['id'])->orWhereHas('anggota', function ($q) {
                    $q->where('anggota', '=', $this->is_login()['id']);
                });
            });
        }

        $penelitian = $penelitian->get();
        $penelitian->load('batch')->load('batch.batch_penelitian.tahun');
        $penelitian->load('jenis_penelitian');
        $penelitian->load('tahun_kegiatan');
        $data["penelitians"] = $penelitian->toArray();
        // echo "<pre>"; print_r($data["penelitians"]);die;
        /*$penelitian = Penelitian::where("isdelete","=","0")->where('isvalid','=','1')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen.fakultas')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->with('Penelitian_review')->with('Penelitian_reviewer');
		// $penelitian = $this->filter($penelitian,$data);
        // $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
 				$q->whereHas('penelitian_reviewer',function($q){
			 		$q->where('dosen','=', $this->is_login()["id"]);
			 	});
			});
	 	}
        $penelitian = $penelitian->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch');
		$penelitian->load('jenis_penelitian');
		$penelitian->load('penelitian_reviewer.dosen');
		$penelitian->load('tahun_kegiatan'); 
        $data["penelitians"] = $penelitian->toArray();*/

        return $data;
    }

    private function getDataPengabdianPDF()
    {
        $this->load->model('Jurnal_anggota');
        $this->load->model('Forum_ilmiah_anggota');
        $this->load->model('luaran_pengabdian/Jurnal');
        $this->load->model('luaran_pengabdian/Buku_ajar');
        $this->load->model('luaran_pengabdian/Forum_ilmiah');
        $this->load->model('luaran_pengabdian/Hki');
        $this->load->model('luaran_pengabdian/Luaran_lain');
        $this->load->model('luaran_pengabdian/Penyelenggara_forum_ilmiah');
        $this->load->model('luaran_pengabdian/Pengabdian_mandiri');

        $user = $this->is_login();
        $data = array();
        $data["dosen"] = $user;

        $penelitian_hibah = Pengabdian_mandiri::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->get();
        $penelitian_hibah->load('dosen');
        $penelitian_hibah->load('tahun');
        $data["penelitian_hibah"] = $penelitian_hibah->toArray();

        $buku_ajar = Buku_ajar::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->with('dosen')->with('dosen.program_studi')->with('dosen.fakultas')->get();
        $data["buku_ajar"] = $buku_ajar->toArray();

        // $jurnal = Jurnal::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->with("dosen")->with("dosen.fakultas")->with("dosen.program_studi")->with('personil')->get();
        // $data["jurnal"] = $jurnal->toArray();

        // $forum_ilmiah = Forum_ilmiah::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->with('dosen')->with('dosen.program_studi')->with('dosen.fakultas')->get();
        // $data["forum_ilmiah"] = $forum_ilmiah->toArray();

        $jurnal = Jurnal::where('isdelete', '=', '0')
            ->where('isvalidate', '=', '1')
            ->whereHas("jurnal_anggota", function ($q) use ($user) {
                $q->where('dosen', $user["id"]);
            })
            ->with('jurnal_anggota.dosen.fakultas')
            ->with('jurnal_anggota.dosen.program_studi')
            ->get();
        $data["jurnal"] = $jurnal->toArray();

        $forum_ilmiah = Forum_ilmiah::where('isdelete', '=', '0')
            ->where('isvalidate', '=', '1')
            ->whereHas("forum_ilmiah_anggota", function ($q) use ($user) {
                $q->where('dosen', $user["id"]);
            })
            ->with('forum_ilmiah_anggota.dosen.program_studi')
            ->get();
        $data["forum_ilmiah"] = $forum_ilmiah->toArray();

        $hki = Hki::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->get();
        $hki->load('dosen');
        $hki->load('jenis_hki');
        $data["hki"] = $hki->toArray();

        $luaran_lain = Luaran_lain::where('dosen', '=', $user["id"])->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->with("dosen")->with("dosen.fakultas")->with("dosen.program_studi")->get();
        $luaran_lain->load('jenis_luaran');
        $data["luaran_lain"] = $luaran_lain->toArray();
        return $data;
    }

    public function getJurnalCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Jurnal');
        $this->load->model('Jurnal_anggota');

        $jurnal = Jurnal::join('jurnal_anggota', function ($q) use ($user) {
            $q->on('jurnal.id', '=', 'jurnal_anggota.jurnal');
        })
            ->where('jurnal_anggota.dosen', '=', $user["id"])
            ->where('jurnal.isvalidate', '=', '1')
            ->where('jurnal.isdelete', '=', '0')
            ->where('jurnal.tahun_kegiatan', '=', $this->tahun)
            ->select('type', new raw('count(jurnal.id) as jumlah'))
            ->groupBy('jurnal.type')
            ->get();

        if (isviewall()) {
            $jurnal = Jurnal::select('type', new raw('count(id) as jumlah'))->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->groupBy('type')->where('tahun_kegiatan', '=', $this->tahun)->get();
        }

        foreach ($jurnal as $j) {
            $data["jurnal" . $j->type] = $j->jumlah;
        }
    }

    private function getPenelitianHibahCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Penelitian_hibah');
        $penelitian_hibah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (isviewall()) {
            $penelitian_hibah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['penelitian_hibah'] = $penelitian_hibah;
    }

    private function getPenyelenggaraForumIlmiahCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Penyelenggara_forum_ilmiah');
        $pforum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (isviewall()) {
            $pforum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('tahun_kegiatan', '=', $this->tahun)->where('isvalidate', '=', '1')->count();
        }
        $data['pforum_ilmiah'] = $pforum_ilmiah;
    }

    private function getLuaranLainCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Luaran_lain');
        $luaran_lain = Luaran_lain::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->count();
        if (isviewall()) {
            $luaran_lain = Luaran_lain::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['luaran_lain'] = $luaran_lain;
    }

    private function getBukuAjarCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Buku_ajar');
        $buku_ajar = Buku_ajar::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (isviewall()) {
            $buku_ajar = Buku_ajar::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['buku_ajar'] = $buku_ajar;
    }

    private function getForumIlmiahCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Forum_ilmiah');
        $this->load->model('Forum_ilmiah_anggota');
        $forum_ilmiah = Forum_ilmiah::where('isdelete', '=', '0')
            ->where('isvalidate', '=', '1')
            ->whereHas("forum_ilmiah_anggota", function ($q) use ($user) {
                $q->where('dosen', $user["id"]);
            })
            ->where('tahun_kegiatan', '=', $this->tahun)
            ->count();
        if (isviewall()) {
            $forum_ilmiah = Forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['forum_ilmiah'] = $forum_ilmiah;
    }

    private function getHkiCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Hki');
        $hki = Hki::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (isviewall()) {
            $hki = Hki::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['hki'] = $hki;
    }

    private function getPenelitianCount($user, &$data)
    {
        $tahun = Tahun_kegiatan::find($this->tahun);
        $penelitian = Penelitian::select('status', new raw('count(id) as jumlah'))->where('isdelete', '=', '0')->where('isvalid', '=', '1')->where('dosen', '=', $user["id"])->groupBy('status')->whereRaw("extract(YEAR from created_at) = ?", [$tahun->tahun])->get();
        if (isviewall()) {
            $penelitian = Penelitian::select('status', new raw('count(id) as jumlah'))->where('isvalid', '=', '1')->where('isdelete', '=', '0')->groupBy('status')->whereRaw("extract(YEAR from created_at) = ?", [$tahun->tahun])->get();
        }
        foreach ($penelitian as $j) {
            $data["penelitian" . $j->status] = $j->jumlah;
        }
    }

    public function getPengabdianJurnalCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Jurnal');
        $jurnal = Jurnal::select('type', new raw('count(id) as jumlah'))->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->groupBy('type')->get();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $jurnal = Jurnal::select('type', new raw('count(id) as jumlah'))->where('isvalidate', '=', '1')->where('isdelete', '=', '0')->groupBy('type')->where('tahun_kegiatan', '=', $this->tahun)->get();
        }
        foreach ($jurnal as $j) {
            $data["pengabdianjurnal" . $j->type] = $j->jumlah;
        }
    }

    private function getPengabdianPenelitianHibahCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Pengabdian_mandiri');
        $pengabdian_hibah = Pengabdian_mandiri::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun', '=', $this->tahun)->count();
        $pengabdian_hibahsum = Pengabdian_mandiri::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun', '=', $this->tahun)->sum("anggaran"); 
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $pengabdian_hibah = Pengabdian_mandiri::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun', '=', $this->tahun)->count();
            $pengabdian_hibahsum = Pengabdian_mandiri::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun', '=', $this->tahun)->sum("anggaran");;
        }
        $data['pengabdian_hibahsum'] = $pengabdian_hibahsum;
        $data['pengabdian_hibah'] = $pengabdian_hibah;
    }

    private function getPengabdianPenyelenggaraForumIlmiahCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Penyelenggara_forum_ilmiah');
        $pforum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $pforum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete', '=', '0')->where('tahun_kegiatan', '=', $this->tahun)->where('isvalidate', '=', '1')->count();
        }
        $data['pengabdianpforum_ilmiah'] = $pforum_ilmiah;
    }

    private function getPengabdianLuaranLainCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Luaran_lain');
        $luaran_lain = Luaran_lain::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->count();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $luaran_lain = Luaran_lain::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['pengabdianluaran_lain'] = $luaran_lain;
    }

    private function getPengabdianBukuAjarCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Buku_ajar');
        $buku_ajar = Buku_ajar::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $buku_ajar = Buku_ajar::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['pengabdianbuku_ajar'] = $buku_ajar;
    }

    private function getPengabdianForumIlmiahCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Forum_ilmiah');
        $forum_ilmiah = Forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $forum_ilmiah = Forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['pengabdianforum_ilmiah'] = $forum_ilmiah;
    }

    private function getPengabdianHkiCount($user, &$data)
    {
        $this->load->model('luaran_pengabdian/Hki');
        $hki = Hki::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun_kegiatan', '=', $this->tahun)->count();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $hki = Hki::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun_kegiatan', '=', $this->tahun)->count();
        }
        $data['pengabdianhki'] = $hki;
    }

    private function getPengabdianCount($user, &$data)
    {
        $tahun = Tahun_kegiatan::find($this->tahun);
        $pengabdian = Pengabdian::select('status', new raw('count(id) as jumlah'))->where('isdelete', '=', '0')->where('isvalid', '=', '1')->where('dosen', '=', $user["id"])->groupBy('status')->whereRaw("extract(YEAR from created_at) = ?", [$tahun->tahun])->get();
        if (roles(["Operator LPPM", "Sekretaris LPPM", "Ketua LPPM"])) {
            $pengabdian = Pengabdian::select('status', new raw('count(id) as jumlah'))->where('isvalid', '=', '1')->where('isdelete', '=', '0')->groupBy('status')->whereRaw("extract(YEAR from created_at) = ?", [$tahun->tahun])->get();
        }
        foreach ($pengabdian as $j) {
            $data["pengabdian" . $j->status] = $j->jumlah;
        }
    }

     

    private function getPenelitianMandiriCount($user, &$data)
    {
        $this->load->model('luaran_penelitian/Penelitian_hibah');
        $Penelitian_hibah = Penelitian_hibah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun', '=', $this->tahun)->count();
        $Penelitian_hibahsum = Penelitian_hibah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('dosen', '=', $user["id"])->where('tahun', '=', $this->tahun)->sum("anggaran");
        if (isviewall()) {
            $Penelitian_hibah = Penelitian_hibah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun', '=', $this->tahun)->count();
            $Penelitian_hibahsum = Penelitian_hibah::where('isdelete', '=', '0')->where('isvalidate', '=', '1')->where('tahun', '=', $this->tahun)->sum("anggaran");
        }
        // print_r($Penelitian_hibahsum);die();
        $data['Penelitian_mandiri'] = number_format($Penelitian_hibah,0,',','.');
        $data['Penelitian_mandirisum'] = number_format($Penelitian_hibahsum,0,',','.');
        $data['sum4'] = $Penelitian_hibahsum;
    }
    
    private function getPenelitianInternasionalCount($user, &$data)
    {
        $this->load->model('Penelitian_dikti');
        $this->load->model('penelitian_dikti_anggota');
        $Penelitian_dikti = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Internasional')
        ->with('penelitian_dikti_anggota')
		->whereHas('penelitian_dikti_anggota',function($q){
            $q->where('dosen','=',$this->is_login()['id']);
        })->with('penelitian_dikti_anggota.dosen')->where('tahun_pelaksanaan', '=', $this->tahun)->count();
        $Penelitian_diktisum = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Internasional')
        ->with('penelitian_dikti_anggota')
		->whereHas('penelitian_dikti_anggota',function($q){
            $q->where('dosen','=',$this->is_login()['id']);
        })->with('penelitian_dikti_anggota.dosen')->where('tahun_pelaksanaan', '=', $this->tahun)->sum("nominal_penelitian");
        if (isviewall()) {
            $Penelitian_dikti = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Internasional')->where('tahun_pelaksanaan', '=', $this->tahun)->count();
            $Penelitian_diktisum = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Internasional')->where('tahun_pelaksanaan', '=', $this->tahun)->sum("nominal_penelitian");
        }
        // print_r($Penelitian_diktisum);die();
        $data['Penelitian_internasional'] = number_format($Penelitian_dikti,0,',','.');
        $data['Penelitian_internasionalsum'] = number_format($Penelitian_diktisum,0,',','.');
        $data['sum3'] = $Penelitian_diktisum;
    }
    
    private function getPenelitianNasionalCount($user, &$data)
    {
        $this->load->model('Penelitian_dikti');
        $this->load->model('penelitian_dikti_anggota');
        $Penelitian_dikti = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Nasional')
        ->with('penelitian_dikti_anggota')
		->whereHas('penelitian_dikti_anggota',function($q){
            $q->where('dosen','=',$this->is_login()['id']);
        })->with('penelitian_dikti_anggota.dosen')->where('tahun_pelaksanaan', '=', $this->tahun)->count();
        $Penelitian_diktisum = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Nasional')
        ->with('penelitian_dikti_anggota')
		->whereHas('penelitian_dikti_anggota',function($q){
            $q->where('dosen','=',$this->is_login()['id']);
        })->with('penelitian_dikti_anggota.dosen')->where('tahun_pelaksanaan', '=', $this->tahun)->sum("nominal_penelitian");
        if (isviewall()) {
            $Penelitian_dikti = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Nasional')->where('tahun_pelaksanaan', '=', $this->tahun)->count();
            $Penelitian_diktisum = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Nasional')->where('tahun_pelaksanaan', '=', $this->tahun)->sum("nominal_penelitian");
        }
        // print_r($Penelitian_diktisum);die();
        $data['Penelitian_nasional'] = number_format($Penelitian_dikti,0,',','.');
        $data['Penelitian_nasionalsum'] = number_format($Penelitian_diktisum,0,',','.');
        $data['sum2'] = $Penelitian_diktisum;
    }

    private function getPenelitianKemenBRINCount($user, &$data)
    {
        $this->load->model('Penelitian_dikti');
        $this->load->model('penelitian_dikti_anggota');
        $Penelitian_dikti = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Kemenristek BRIN')
        ->with('penelitian_dikti_anggota')
		->whereHas('penelitian_dikti_anggota',function($q){
            $q->where('dosen','=',$this->is_login()['id']);
        })->with('penelitian_dikti_anggota.dosen')->where('tahun_pelaksanaan', '=', $this->tahun)->count();
        $Penelitian_diktisum = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Kemenristek BRIN')
        ->with('penelitian_dikti_anggota')
		->whereHas('penelitian_dikti_anggota',function($q){
            $q->where('dosen','=',$this->is_login()['id']);
        })->with('penelitian_dikti_anggota.dosen')->where('tahun_pelaksanaan', '=', $this->tahun)->sum("nominal_penelitian");
        if (isviewall()) {
            $Penelitian_dikti = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Kemenristek BRIN')->where('tahun_pelaksanaan', '=', $this->tahun)->count();
            $Penelitian_diktisum = Penelitian_dikti::where('isdelete', '=', '0')->where('pendanaan_penelitian','=','Kemenristek BRIN')->where('tahun_pelaksanaan', '=', $this->tahun)->sum("nominal_penelitian");
        }
        // print_r($Penelitian_diktisum);die();
        $data['Penelitian_BRIN'] = number_format($Penelitian_dikti,0,',','.');
        $data['Penelitian_BRINsum'] = number_format($Penelitian_diktisum,0,',','.');
        $data['sum5'] = $Penelitian_diktisum;
    }

    private function getPenelitianInternalCount($user, &$data)
    {
        $penelitian = Penelitian::where("isdelete", "=", "0")->where('isvalid', '=', '1')->where('status','=','4')->where('tahun_kegiatan', '=', $this->tahun)
            ->whereHas('penelitian_laporan',function($q){
                $q->where('status','=','1');
            })->whereHas('tahun_kegiatan',function($q){
                $q->where('tahun','=',$this->tahun);
            })
            ->with('dosen.jabatan_akademik')
            ->with('dosen.jabatan_fungsional')
            ->with('dosen.program_studi')
            ->with('dosen_menyetujui')
            ->with('dosen_menyetujui.jabatan_akademik')
            ->with('dosen_menyetujui.program_studi')
            ->with('dosen_menyetujui.jabatan_fungsional')
            ->with('dosen_mengetahui')
            ->with('dosen_mengetahui.jabatan_akademik')
            ->with('dosen_mengetahui.program_studi')
            ->with('dosen_mengetahui.jabatan_fungsional')
            ->with('anggota')->with('anggota.dosen')->with('Penelitian_review')->with('Penelitian_reviewer')->orderBy('created_at', 'DESC');
        // $penelitian = $this->filter($penelitian,$data);
        if ($this->is_login()['akses']['nama'] == 'Dosen') {
            $penelitian->where(function ($q) {
                $q->where('dosen', '=', $this->is_login()['id'])->orWhereHas('anggota', function ($q) {
                    $q->where('anggota', '=', $this->is_login()['id']);
                });
            });
        }
        // $penelitian->load('dosen')->load('tahun_kegiatan')->load('penelitian_laporan'); 

        $penelitiansum = Penelitian::where("isdelete", "=", "0")->where('isvalid', '=', '1')->where('status','=','4')->where('tahun_kegiatan', '=', $this->tahun)
            ->whereHas('penelitian_laporan',function($q){
                $q->where('status','=','1');
            })->whereHas('tahun_kegiatan',function($q){
                $q->where('tahun','=',$this->tahun);
            })->with('dosen.jabatan_akademik')
            ->with('dosen.jabatan_fungsional')
            ->with('dosen.program_studi')
            ->with('dosen_menyetujui')
            ->with('dosen_menyetujui.jabatan_akademik')
            ->with('dosen_menyetujui.program_studi')
            ->with('dosen_menyetujui.jabatan_fungsional')
            ->with('dosen_mengetahui')
            ->with('dosen_mengetahui.jabatan_akademik')
            ->with('dosen_mengetahui.program_studi')
            ->with('dosen_mengetahui.jabatan_fungsional')
            ->with('anggota')->with('anggota.dosen')->with('Penelitian_review')->with('Penelitian_reviewer')->orderBy('created_at', 'DESC');
        // $penelitian = $this->filter($penelitian,$data);
        if ($this->is_login()['akses']['nama'] == 'Dosen') {
            $penelitiansum->where(function ($q) {
                $q->where('dosen', '=', $this->is_login()['id'])->orWhereHas('anggota', function ($q) {
                    $q->where('anggota', '=', $this->is_login()['id']);
                });
            });
        }
        // $penelitiansum->load('dosen')->load('tahun_kegiatan')->load('penelitian_laporan'); 

        $penelitian = $penelitian->count();
        $penelitiansum = $penelitiansum->sum('total_biaya'); 
        
        $data['Penelitian_internal'] = number_format($penelitian,0,',','.');
        $data['Penelitian_internalsum'] = number_format($penelitiansum,0,',','.');
        $data['sum1'] = $penelitiansum;
    }

    
    
}
