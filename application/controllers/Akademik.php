<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Akademik extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p2"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array'],
		];
        parent::__construct($config);
        $this->load->model("Jabatan_akademik");
        $this->load->model("Jabatan_fungsi");
        $this->load->model("Fakultas");
        $this->load->model("Program_studi");
        $this->load->model("Bidangkeahlian");
        $this->load->model("Subbidangkeahlian");
    }	
	public function index(){
		$this->twig->display('pengaturan/akademik/index');
	}

	public function jabatanakademik(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_akademik','%s already exists');
		$this->form_validation->set_rules('nama', 'Jabatan Akademik', 'required|callback_validate_akademik');
        if ($this->form_validation->run() == FALSE){
        	$item = Jabatan_akademik::where("isdelete","=","0");
            $info = $this->create_paging($item);
            $item = $item->take($info["limit"])->skip($info["skip"])->get();
			$this->twig->display('pengaturan/akademik/jabatan_akademik',array_merge($info,$this->menu,array("items"=>$item->toArray())));
        }else{ 
        	$jenis_hki = new Jabatan_akademik;
        	$jenis_hki->nama = $this->input->post('nama');
        	$jenis_hki->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Akademik-Jabatan Akademik baru dengan nama '.$jenis_hki->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        	redirect('akademik/jabatanakademik');
		}
	}

    public function jabatanakademik_edit(){
        $id = $this->uri->segment(3);
        $nama = $this->input->post('nama');
        $jenis_hki = Jabatan_akademik::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Akademik-Jabatan Akademik dengan nama '.$jenis_hki->nama.' menjadi '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $jenis_hki->nama = $nama;
        $jenis_hki->save();
        redirect('akademik/jabatanakademik');
    }

    public function validate_akademik($str){
        $item = Jabatan_akademik::where('nama','=',$str)->count();
        if($item>0)
            return False;
        return True;
    }

    public function jabatan_akademik(){
        $id = $this->uri->segment(3);
        $item = Jabatan_akademik::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Akademik-Jabatan Akademik dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('akademik/jabatanakademik');
    }

	public function jabatanfungsional(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_fungsional','%s already exists');
		$this->form_validation->set_rules('nama', 'Jabatan Fungsional', 'required|callback_validate_fungsional');
        if ($this->form_validation->run() == FALSE){
        	$item = Jabatan_fungsi::where("isdelete","=","0");
            $info = $this->create_paging($item);
            $item = $item->take($info["limit"])->skip($info["skip"])->get();
			$this->twig->display('pengaturan/akademik/jabatan_fungsi',array_merge($info,$this->menu,array("items"=>$item->toArray())));
        }else{ 
        	$jenis_luaran = new Jabatan_fungsi;
        	$jenis_luaran->nama = $this->input->post('nama');
        	$jenis_luaran->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Akademik-Pangkat/Golongan Ruang baru dengan nama '.$jenis_luaran->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('akademik/jabatanfungsional');
		}
    }

    public function jabatanfungsional_edit(){
        $id = $this->uri->segment(3);
        $nama = $this->input->post('nama');
        $jenis_hki = Jabatan_fungsi::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Akademik-Pangkat/Golongan Ruang dengan nama '.$jenis_hki->nama.' menjadi '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $jenis_hki->nama = $nama;
        $jenis_hki->save();
        redirect('akademik/jabatanfungsional');
    }

    public function validate_fungsional($str){
        $item = Jabatan_fungsi::where('nama','=',$str)->count();
        if($item>0)
            return False;
        return True;
    }

    public function jabatan_fungsional(){
        $id = $this->uri->segment(3);
        $item = Jabatan_fungsi::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Akademik-Pangkat/Golongan Ruang dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('akademik/jabatanfungsional');
    }


	public function fakultas(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_fakultas','%s already exists');
		$this->form_validation->set_rules('nama', 'Nama Fakultas', 'required|callback_validate_fakultas');
        if ($this->form_validation->run() == FALSE){
        	$item = Fakultas::where("isdelete","=","0");
            $info = $this->create_paging($item);
            $item = $item->take($info["limit"])->skip($info["skip"])->get();
			$this->twig->display('pengaturan/akademik/fakultas',array_merge($info,$this->menu,array("items"=>$item->toArray())));
        }else{ 
        	$tahun_kegiatan = new Fakultas;
        	$tahun_kegiatan->nama = $this->input->post('nama');
        	$tahun_kegiatan->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Akademik-Fakultas dengan nama '.$tahun_kegiatan->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('akademik/fakultas');
		}
	}

    public function fakultas_edit(){
        $id = $this->uri->segment(3);
        $nama = $this->input->post('nama');
        $jenis_hki = Fakultas::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Akademik-Fakultas dengan nama '.$jenis_hki->nama.' menjadi '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $jenis_hki->nama = $nama;
        $jenis_hki->save();
        redirect('akademik/fakultas');
    }

    public function validate_fakultas($str){
        $item = Fakultas::where('nama','=',$str)->count();
        if($item>0)
            return False;
        return True;
    }

    public function fakultas_delete(){
        $id = $this->uri->segment(3);
        $item = Fakultas::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Akademik-Fakultas dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('akademik/fakultas');
    }


	public function programstudi(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_studi','%s already exists');
		$this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
		$this->form_validation->set_rules('nama', 'Nama Program Studi', 'required|callback_validate_studi');
        if ($this->form_validation->run() == FALSE){
        	$item = Program_studi::where("isdelete","=","0");
            $info = $this->create_paging($item);
            $item = $item->take($info["limit"])->skip($info["skip"])->get();
        	$item->load('fakultas');
        	$items_fakul = Fakultas::where("isdelete","=","0")->get();
			$this->twig->display('pengaturan/akademik/program_studi',array_merge($info,$this->menu,array("items"=>$item->toArray(),"items_fakul"=>$items_fakul->toArray())));
        }else{ 
        	$tahun_kegiatan = new Program_studi;
        	$tahun_kegiatan->nama = $this->input->post('nama');
        	$tahun_kegiatan->fakultas = $this->input->post('fakultas');
        	$tahun_kegiatan->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Akademik-Program Studi baru dengan nama '.$tahun_kegiatan->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('akademik/programstudi');
		}
    }
     
	public function bidangkeahlian(){ 
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_bidangkeahlian','%s already exists');
		$this->form_validation->set_rules('bidangkeahlian', 'Bidang Keahlian', 'required'); 
        if ($this->form_validation->run() == FALSE){   
        	$item = Bidangkeahlian::where("isdelete","=","0");
            $info = $this->create_paging($item);
            $item = $item->take($info["limit"])->skip($info["skip"])->get(); 
			$this->twig->display('pengaturan/akademik/bidangkeahlian',array_merge($info,$this->menu,array("items"=>$item->toArray())));
        }else{ 
        	$bidangkeahlian = new Bidangkeahlian; 
            $bidangkeahlian->bidangkeahlian = $this->input->post('bidangkeahlian');
        	$bidangkeahlian->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Akademik-Bidang Keahlian baru dengan nama '.$bidangkeahlian->bidangkeahlian,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('akademik/bidangkeahlian');
        } 
    } 
    public function subbidangkeahlian(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_subbidangkeahlian','%s already exists');
		$this->form_validation->set_rules('bidangkeahlian', 'Bidang Keahlian', 'required');
		$this->form_validation->set_rules('subbidangkeahlian', 'Sub Bidang Keahlian', 'required');
        if ($this->form_validation->run() == FALSE){
        	$item = Subbidangkeahlian::where("isdelete","=","0");
            $info = $this->create_paging($item);
            $item = $item->take($info["limit"])->skip($info["skip"])->get(); 
            $item->load('bidangkeahlian');
            $items_bk = Bidangkeahlian::where('isdelete', '=', '0')->get();
			$this->twig->display('pengaturan/akademik/subbidangkeahlian',array_merge($info,$this->menu,array("items"=>$item->toArray(),"items_bk"=>$items_bk->toArray())));
        }else{ 
        	$bidangkeahlian = new Subbidangkeahlian;
        	$bidangkeahlian->bidangkeahlian = $this->input->post('bidangkeahlian');
        	$bidangkeahlian->subbidangkeahlian = $this->input->post('subbidangkeahlian');
        	$bidangkeahlian->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Akademik-Bidang Keahlian baru dengan nama '.$bidangkeahlian->bidangkeahlian.' - '.$bidangkeahlian->sub_bidangkeahlian,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('akademik/subbidangkeahlian');
		}
	}

    public function programstudi_edit(){
        $id = $this->uri->segment(3);
        $nama = $this->input->post('nama');
        $jenis_hki = Program_studi::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Akademik-Program Studi dengan nama '.$jenis_hki->nama.' menjadi '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $jenis_hki->nama = $nama;
        $jenis_hki->save();
        redirect('akademik/programstudi');
    }
    
    public function bidangkeahlian_edit(){
        $id = $this->uri->segment(3);
        $postbidangkeahlian = $this->input->post('bidangkeahlian'); 
        $bidangkeahlian = Bidangkeahlian::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Akademik-Program Studi dengan nama '.$jenis_hki->nama.' menjadi '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $bidangkeahlian->bidangkeahlian = $postbidangkeahlian; 
        $bidangkeahlian->save();
        redirect('akademik/bidangkeahlian');
    } 
    public function subbidangkeahlian_edit(){
        $id = $this->uri->segment(3);
        $postbidangkeahlian = $this->input->post('bidangkeahlian');
        $subbidangkeahlian = $this->input->post('subbidangkeahlian'); 
        $dtsubbidangkeahlian = Subbidangkeahlian::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Akademik-Program Studi dengan nama '.$dtsubbidangkeahlian->subbidangkeahlian.' menjadi '.$subbidangkeahlian,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $dtsubbidangkeahlian->bidangkeahlian = $postbidangkeahlian;
        $dtsubbidangkeahlian->subbidangkeahlian = $subbidangkeahlian;
        $dtsubbidangkeahlian->save();
        redirect('akademik/subbidangkeahlian');
    }
    
    public function bidangkeahlian_delete(){
        $id = $this->uri->segment(3);
        $item = Bidangkeahlian::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Bidangkeahlian dengan bidangkeahlian '.$item->bidangkeahlian,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('akademik/bidangkeahlian');
    }

    
    public function subbidangkeahlian_delete(){
        $id = $this->uri->segment(3);
        $item = Subbidangkeahlian::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Sub Bidang Keahlian dengan subbidangkeahlian '.$item->subbidangkeahlian,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('akademik/subbidangkeahlian');
    }

    public function validate_studi($str){
        $item = Program_studi::where('nama','=',$str)->count();
        if($item>0)
            return False;
        return True;
    }
    public function validate_bidangkeahlian($str){
        $item = Bidangkeahlian::where('bidangkeahlian','=',$str)->count();
        if($item>0)
            return False;
        return True;
    }
    public function validate_subbidangkeahlian($str){
        $item = Subbidangkeahlian::where('subbidangkeahlian','=',$str)->count();
        if($item>0)
            return False;
        return True;
    }

    public function program_studi(){
        $id = $this->uri->segment(3);
        $item = Program_studi::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Akademik-Program Studi baru dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('akademik/programstudi');
    }
}
