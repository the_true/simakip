<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Notif extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("notif"=>"active","mk5"=>"active");
  public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        $this->load->helper('celery');
				$this->load->model('Notifikasi');
				$this->load->model('Jenis_penelitian');
				$this->load->model('Batch_penelitian');
				$this->load->model('Penilaian');
				$this->load->model('Penilaian_kriteria');
				$this->load->model('Syarat_penelitian');
				$this->load->model('Batch');
				$this->load->model('Dosen');
				$this->load->model('Pengabdian');
				$this->load->model('Jenis_pengabdian');
  }

  public function index(){
		$notif = Notifikasi::where('isdelete','=','0')->where('dosen','=',$this->is_login()['id'])
		->orderBy('isread','ASC')->orderBy('created_at','DESC');
		$notif = $this->filter($notif);
		$info = $this->create_paging($notif);
		$notif = $notif->take($info["limit"])->skip($info["skip"])->get();

		$data=[];
		$data["items"]=$notif->toArray();

		$this->twig->display("notifikasi/index",array_merge($data,$this->menu,$info));
  }

	public function filter($model){
		$check = $this->input->get('tanggal_mulai');
		if ($check)	$model = $model->where('created_at', '>=',$check);

		$check = $this->input->get('tanggal_selesai');
		if ($check)	$model = $model->where('created_at', '<=',$check);

		$check = $this->input->get('menu');
		if ($check)	$model = $model->where('jenis', '=',$check);

		return $model;
	}

	public function mark(){
		$id=$this->uri->segment(3);
		$item = Notifikasi::find($id);
		if($this->is_login()["id"]==$item->dosen){
			$item->isread=1;
			$item->save();
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$this->twig->display('404');
			return FALSE;
		}
	}

	public function markall(){
		$notifs = $this->input->post("notif");
	    foreach($notifs as $id){
	        $item = Notifikasi::find($id);
					if($this->is_login()["id"]==$item->dosen){
						$item->isread=1;
						$item->save();
					}else{
						$this->twig->display('404');
						return FALSE;
					}
	    }
	    redirect($_SERVER['HTTP_REFERER']);
	}

	public function trigger(){
		$this->load->model('Penelitian');
		$penelitian = Penelitian::find(29);
		$data=["id"=>$penelitian->dosen()->first()->id,"nama"=>$penelitian->dosen()->first()->nama_lengkap,"surel"=>$penelitian->dosen()->first()->surel,
					 "judul"=>$penelitian->judul,"notes"=>"banyak alasan tau lah"];
		// echo json_encode($data); die;
		celery()->PostTask('tasks.penelitian_ditolak', array(base_url(),$data));
	}

	public function addnotif(){
		$dosen = $this->input->post('dosen');
		$content= $this->input->post('content');
		$jenis= $this->input->post('jenis');

		if((int)$dosen==0){
			$dosens = Dosen::where("isdelete","=","0")->whereIn("akses",["4","5","6"])->get()->toArray();
			foreach($dosens as $value){
				$notif = new Notifikasi;
				$notif->content = $content;
				$notif->dosen = $value["id"];
				$notif->jenis = $jenis;
				$notif->save();
			}
		}else{
			$notif = new Notifikasi;
			$notif->content = $content;
			$notif->dosen = $dosen;
			$notif->jenis = $jenis;
			$notif->save();
		}
		echo "berhasil";
	}

	public function penelitiansubmit(){
		$this->load->model("penelitian");
		$this->load->model("Tahun_kegiatan");
		$id = $this->input->post("id");
		$penelitian = Penelitian::find($id);
		$penelitian->load('jenis_penelitian');
		$penelitian->load('batch');
		$penelitian->load('tahun_kegiatan');
		$penelitian->load('dosen');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
		$dosens = Dosen::where("isdelete","=","0")->whereIn("akses",["4","5","6"])->get()->toArray();
		foreach($dosens as $value){
			send_notif_penelitian($value["id"],"Usulan penelitian ".$penelitian["judul"]." telah disubmit. Silakan melakukan validasi.");
			sendemail_penelitian_disubmit(["surel"=>$value["surel"],"operator"=>$value["nama_lengkap"],
																		"jenis_penelitian"=>$penelitian["jenis_penelitian"]["nama"],"judul"=>$penelitian["judul"],
																	  "batch"=>$penelitian["batch"]["nama"],"tahun"=>$penelitian["tahun_kegiatan"]["tahun"],
																	  "pengusul"=>$penelitian["dosen"]["nama_lengkap"]]);
		}
		echo "berhasil";
	}


	public function pengabdiansubmit(){
		$this->load->model("pengabdian");
		$this->load->model("Tahun_kegiatan");
		$id = $this->input->post("id");
		$pengabdian = Pengabdian::find($id);
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('batch');
		$pengabdian->load('tahun_kegiatan');
		$pengabdian->load('dosen');
		$pengabdian = $pengabdian->toArray();
		$dosens = Dosen::where("isdelete","=","0")->whereHas("akses",function($q){
			$q->whereIn("nama",["Operator LPPM","Sekretaris LPPM","Ketua LPPM"]);
		})->get()->toArray();
		foreach($dosens as $value){
			send_notif_pengabdian($value["id"],"Usulan pengabdian ".$pengabdian["judul"]." telah disubmit. Silakan melakukan validasi.");
			sendemail_pengabdian_disubmit(["surel"=>$value["surel"],"operator"=>$value["nama_lengkap"],
																		"jenis_pengabdian"=>$pengabdian["jenis_pengabdian"]["nama"],"judul"=>$pengabdian["judul"],
																	  "batch"=>$pengabdian["batch"]["nama"],"tahun"=>$pengabdian["tahun_kegiatan"]["tahun"],
																	  "pengusul"=>$pengabdian["dosen"]["nama_lengkap"]]);
		}
		echo "berhasil";
	}

	public function manualsendnotif(){
		celery()->PostTask('tasks.send_notif', array(base_url(),0, 'Luaran','test notif waw'));
	}

	public function manualpenelitianactive(){
		celery()->PostTask('tasks.check_penelitian_active',array(base_url()));
	}

	public function manualpenelitianexpire(){
		celery()->PostTask('tasks.check_penelitian_expire',array(base_url()));
	}

	public function laporansubmit(){
		$this->load->model("penelitian_laporan");
		$this->load->model("Penelitian");
		$id = $this->input->post("id");
		$laporan = Penelitian_laporan::find($id);
		$laporan->load('penelitian');
		$laporan->load('penelitian.dosen');
		// echo $penelitian->toJson(); die;
		$laporan = $laporan->toArray();
		$dosens = Dosen::where("isdelete","=","0")->whereIn("akses",["4","5","6"])->get()->toArray();
		foreach($dosens as $value){
			send_notif_penelitian($value["id"],"Laporan penelitian baru saja diupload dengan judul \"".$laporan["penelitian"]["judul"]."\", silakan melakukan validasi.");
			sendemail_laporan_disubmit(["surel"=>$value["surel"],"operator"=>$value["nama_lengkap"],
																		"judul"=>$laporan["penelitian"]["judul"],"pengusul"=>$laporan["penelitian"]["dosen"]["nama_lengkap"]]);
		}
		echo "berhasil";
	}

	public function laporanpengabdiansubmit(){
		$this->load->model("pengabdian_laporan");
		$this->load->model("pengabdian");
		$id = $this->input->post("id");
		$laporan = Pengabdian_laporan::find($id);
		$laporan->load('pengabdian');
		$laporan->load('pengabdian.dosen');
		// echo $penelitian->toJson(); die;
		$laporan = $laporan->toArray();
		$dosens = Dosen::where("isdelete","=","0")->whereHas("akses",function($q){
			$q->whereIn("nama",["Operator LPPM","Sekretaris LPPM","Ketua LPPM"]);
		})->get()->toArray();
		foreach($dosens as $value){
			send_notif_pengabdian($value["id"],"Laporan pengabdian baru saja diupload dengan judul \"".$laporan["pengabdian"]["judul"]."\", silakan melakukan validasi.");
			sendemail_laporan_pengabdian_disubmit(["surel"=>$value["surel"],"operator"=>$value["nama_lengkap"],
																		"judul"=>$laporan["pengabdian"]["judul"],"pengusul"=>$laporan["pengabdian"]["dosen"]["nama_lengkap"]]);
		}
		echo "berhasil";
	}

	public function suratkontraksubmit(){
		$this->load->model("penelitian_laporan");
		$this->load->model("Penelitian");
		$this->load->model("Surat_kontrak");
		$id = $this->input->post("id");
		$suratkontrak = Surat_kontrak::find($id);
		$suratkontrak->load('penelitian');
		$suratkontrak->load('dosen');
		// echo $penelitian->toJson(); die;
		$suratkontrak = $suratkontrak->toArray();
		$dosens = Dosen::where("isdelete","=","0")->whereIn("akses",["4","5","6"])->get()->toArray();
		foreach($dosens as $value){
			send_notif_suratkontrak($value["id"],"Surat kontrak 1 lengkap telah diupload dari usulan yang berjudul \"".$suratkontrak["penelitian"]["judul"]."\"");
			sendemail_suratkontrak_disubmit(["surel"=>$value["surel"],"operator"=>$value["nama_lengkap"],
																		"judul"=>$suratkontrak["penelitian"]["judul"],"pengusul"=>$suratkontrak["dosen"]["nama_lengkap"]]);
		}
		echo "berhasil";
	}

	public function checkpenelitianaktif(){
		$dosen = Dosen::where('isdelete','=','0')->get();
		$jenis_penelitian = Jenis_penelitian::where('isdelete','=','0')->get();
		$jenis_penelitian->load('syarat_penelitian');
		// echo $jenis_penelitian->toJson();
		$lists=[];
		foreach($dosen as $value){
			foreach($jenis_penelitian as $value_x){
				foreach($value_x["syarat_penelitian"] as $value_p){
					if($value["jabatan_akademik"]==$value_p["jabatan_akademik"] && $value["tingkat"]==$value_p["syarat"]
					&& isset($value_x["active_batch"]) && $value_x["active_batch"]["is_send"]){
						$lists[]=["id"=>$value["id"],"surel"=>$value["surel"],"nama_lengkap"=>$value["nama_lengkap"],"jenis_penelitian"=>$value_x["nama"],
					"active_batch"=>$value_x["active_batch"]["nama"],"tanggal_mulai"=>$value_x["active_batch"]["start"],
					"tanggal_selesai"=>$value_x["active_batch"]["finish"]];
					}
				}
			}
		}
		echo json_encode($lists);
	}

	public function checkpengabdianmonevexpired(){
		$this->load->model("akses");
		$dosen_penting = Dosen::where('isdelete','=','0')->whereHas("akses",function($q){
			$q->whereIn("nama",["Operator LPPM","Sekretaris LPPM","Ketua LPPM"]);
		})->get();
		// echo $dosen_penting->toJson(); die;
		$dosen_penting = $dosen_penting->toArray();
		$batch = Batch::where('isdelete','=','0')->whereHas("batch_penelitian", function($q){
			$q->whereNull("jenis_penelitian");
		})->get();
		$batch_lists = [];
		foreach($batch as $value){
			if($value->is_monev_expired) $batch_lists[] = $value->id;
		}

		$dosen_pengusul = Dosen::where('isdelete','=','0')->whereHas('pengabdian',function($q) use ($batch_lists){
			$q->whereIn('batch',$batch_lists);
		})->get();
		$dosen_pengusul = $dosen_pengusul->toArray();

		$combined_dosen = array_merge($dosen_penting, $dosen_pengusul);
		$lists=[];
		foreach($combined_dosen as $value){
			$content = "Periode Monev telah berakhir";
			send_notif_pengabdian($value["id"], $content);
			$lists[] = $value;
		}
		echo json_encode($lists);
	}

	public function checkmonevreview(){

		$this->load->model('Monev_reviewer');
		$this->load->model('Penelitian');
		$this->load->model('jenis_penelitian');

		set_time_limit(100);
		$jenis_penelitian = Jenis_penelitian::whereHas('batch_penelitian',function($q){
			$q->whereHas('batch',function($w){
				$now = date('Y-m-d');
				$w->where('monev_start','<=',$now." 00:00:00")->whereRaw("DATE_ADD(monev_start, INTERVAL 1 DAY) > '$now 00:00:00'");
			});
		})->get();
		$jp = [];
		foreach($jenis_penelitian as $j){
			$jp[] = $j->id;
		}

		$reviewers = Monev_reviewer::whereHas('penelitian',function($q) use ($jp) {
			$q->whereIn('jenis_penelitian',$jp);
		})->get();
		$reviewers->load('penelitian')->load('dosen');
		// echo $reviewers->toJson(); die;
		$reviewers = $reviewers->toArray();
		foreach($reviewers as $r){
			$first_four_title = implode(' ', array_slice(explode(' ', $r["penelitian"]["judul"]), 0, 4));
			celery()->PostTask('tasks.send_notif', array(base_url(),$r["dosen"]["id"], 'Monev',"Monev penelitian dengan judul ".$first_four_title." telah dapat direview"));
		}
	}

	public function checkpengabdianmonevreview(){

		$this->load->model('pengabdian_monev_reviewer');
		$this->load->model('pengabdian');
		$this->load->model('jenis_pengabdian');

		set_time_limit(100);
		$now = date('Y-m-d');
		$batchs = Batch::where('monev_start','<=',$now." 00:00:00")->whereRaw("DATE_ADD(monev_start, INTERVAL 1 DAY) > '$now 00:00:00'")->get();
		
		$jp = [];
		foreach($batchs as $j){
			$jp[] = $j->id;
		}

		$reviewers = Pengabdian_monev_reviewer::whereHas('pengabdian',function($q) use ($jp) {
			$q->whereIn('batch',$jp);
		})->get();
		$reviewers->load('pengabdian')->load('dosen');
		// echo $reviewers->toJson(); die;
		$reviewers = $reviewers->toArray();
		foreach($reviewers as $r){
			$first_four_title = implode(' ', array_slice(explode(' ', $r["pengabdian"]["judul"]), 0, 4));
			celery()->PostTask('tasks.send_notif', array(base_url(),$r["dosen"]["id"], 'Monev',"Monev pengabdian dengan judul ".$first_four_title." telah dapat direview"));
		}
	}

	public function checktanggalpengumuman(){
		set_time_limit(100);
		$jenis_penelitian = Jenis_penelitian::whereHas('batch_penelitian',function($q){
			$q->whereHas('batch',function($w){
				$now = date('Y-m-d');
				// $to = date('Y-m-d', strtotime("+5 days"));
				// $w->whereBetween('review',[$now." 00:00:00",$to. " 00:00:00"]);
				$w->where('review','<=',$now." 00:00:00")->whereRaw("DATE_ADD(review, INTERVAL 1 DAY) > '$now 00:00:00'");
			});
		})->get();
		$jp = [];
		foreach($jenis_penelitian as $j){
			$jp[] = $j->id;
		}
		$this->load->model('Penelitian');
		$this->load->model('Penelitian_review');
		$penelitian = Penelitian::where('status','=','3')->whereIn('jenis_penelitian',$jp)
		->whereHas('penelitian_review',function($q){
			 		$q->where('isselected','=','1')->where('catatan_rekomendasi','<>','');
		})->get();
		$penelitian->load('penelitian_review_inselected');
		$penelitian->load('dosen');
		$lists = [];
		foreach($penelitian as $p){
			$dosen = $p->dosen()->first();
			$data=["id"=>$dosen->id,"nama"=>$dosen->nama_lengkap,"surel"=>$dosen->surel,
								 "judul"=>$p->judul];
			if($p->penelitian_review_inselected[0]->status==1){
				$p->status=4;
				celery()->PostTask('tasks.penelitian_diterima', array(base_url(),$data));
			} 
			else if($p->penelitian_review_inselected[0]->status==2){
				$p->status=11;
				$data["notes"] = $p->penelitian_review_inselected[0]->saran;
				celery()->PostTask('tasks.penelitian_diterimasyarat', array(base_url(),$data));
			} 
			else if($p->penelitian_review_inselected[0]->status==3){
				$p->status=12;
				$data["notes"] = $p->penelitian_review_inselected[0]->alasan;
				celery()->PostTask('tasks.penelitian_ditolak', array(base_url(),$data));
			} 
			$p->save();
			// 
			// $lists[]=[]
			// echo $p->dosen()->first()->nama_lengkap;
		}
		// echo $jenis_penelitian->toJson();
	}

	public function checktanggalpengumumanpengabdian(){
		set_time_limit(100);
		$this->load->model('Jenis_pengabdian');
		// $jenis_pengabdian = Jenis_pengabdian::whereHas('batch_pengabdian',function($q){
		// 	$q->whereHas('batch',function($w){
		// 		$now = date('Y-m-d');
		// 		$w->where('review','<=',$now." 00:00:00")->whereRaw("DATE_ADD(review, INTERVAL 1 DAY) > '$now 00:00:00'");
		// 	});
		// })->get();
		$batch_pengabdian = Batch_penelitian::whereNull("jenis_penelitian")->whereHas('batch',function($w){
					$now = date('Y-m-d', strtotime( '-1 days' ) );
					// $now = date('Y-m-d');
					$w->where('review','<=',$now." 23:00:00");//->whereRaw("DATE_ADD(review, INTERVAL 1 DAY) > '$now 00:00:00'");
				})->count();
		// echo $batch_pengabdian; die;
		$jenis_pengabdian = [];
		if($batch_pengabdian>=1){
			$jenis_pengabdian = Jenis_pengabdian::all();
		}
		
		$jp = [];
		foreach($jenis_pengabdian as $j){
			$jp[] = $j->id;
		}
		// echo print_r($jp); die;
		$this->load->model('Pengabdian');
		$this->load->model('Pengabdian_review');
		$pengabdian = Pengabdian::where('status','=','3')->whereIn('jenis_pengabdian',$jp)
		->whereHas('pengabdian_review',function($q){
			 		$q->where('isselected','=','1')->where('catatan_rekomendasi','<>','');
		})->get();
		$pengabdian->load('pengabdian_review_inselected');
		$pengabdian->load('dosen');
		$lists = [];
		// echo $pengabdian->toJson(); die;
		foreach($pengabdian as $p){
			$dosen = $p->dosen()->first();
			$data=["id"=>$dosen->id,"nama"=>$dosen->nama_lengkap,"surel"=>$dosen->surel,
								 "judul"=>$p->judul];
			if($p->pengabdian_review_inselected[0]->status==1){
				$p->status=4;
				// celery()->PostTask('tasks.penelitian_diterima', array(base_url(),$data));
			} 
			else if($p->pengabdian_review_inselected[0]->status==2){
				$p->status=11;
				$data["notes"] = $p->pengabdian_review_inselected[0]->saran;
				// celery()->PostTask('tasks.penelitian_diterimasyarat', array(base_url(),$data));
			} 
			else if($p->pengabdian_review_inselected[0]->status==3){
				$p->status=12;
				$data["notes"] = $p->pengabdian_review_inselected[0]->alasan;
				// celery()->PostTask('tasks.penelitian_ditolak', array(base_url(),$data));
			} 
			$p->save();
			// 
			// $lists[]=[]
			// echo $p->dosen()->first()->nama_lengkap;
		}

	}

public function sendpenelitianaktif(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		$notif = new Notifikasi;
		$notif->content = "Jenis Penelitian ".$data["jenis_penelitian"]." pada ".$data["active_batch"]." untuk bapak/ibu telah aktif, silahkan ikut serta untuk penelitian";
		$notif->dosen = $data["id"];
		$notif->jenis = "Penelitian";
		$notif->save();
		sendemail_penelitian_tersedia($data);
		echo "sending mail";
	}

	public function checkperpanjangpenelitian(){
		$batch = $this->input->get('batch');
		$dosen = Dosen::where('isdelete','=','0')->whereNotIn("akses",["4","5","6"])->get();
		$jenis_penelitian = Jenis_penelitian::where('isdelete','=','0')->whereHas('Batch_penelitian',function($q){
			$q->whereHas('Batch',function($q2){
				$q2->where('id','=',$this->input->get('batch'));
			});
		})->get();
		$jenis_penelitian->load('syarat_penelitian');
		// echo $jenis_penelitian->toJson();die;
		$lists=[];
		foreach($dosen as $value){
			foreach($jenis_penelitian as $value_x){
				foreach($value_x["syarat_penelitian"] as $value_p){
					if($value["jabatan_akademik"]==$value_p["jabatan_akademik"] && $value["tingkat"]==$value_p["syarat"]){
						$lists[]=["id"=>$value["id"],"surel"=>$value["surel"],"nama_lengkap"=>$value["nama_lengkap"],"jenis_penelitian"=>$value_x["nama"],
					"extend_batch"=>$value_x["active_batch"]["nama"],"tanggal_mulai"=>$value_x["active_batch"]["start"],
					"tanggal_selesai"=>$value_x["active_batch"]["finish"]];
					}
				}
			}
		}
		echo json_encode($lists);
	}

	public function sendperpanjangpenelitian(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		$notif = new Notifikasi;
		$notif->content = "Jenis penelitian ".$data["jenis_penelitian"]." pada ".$data["extend_batch"]." untuk bapak/ibu telah diperpanjang waktu pengusulannya, silakan ikut serta untuk penelitian.";
		$notif->dosen = $data["id"];
		$notif->jenis = "Penelitian";
		$notif->save();
		sendemail_penelitian_diperpanjang($data);
		echo "sending mail";
	}

	public function checkpenelitianexpire(){
		$dosen = Dosen::where('isdelete','=','0')->whereNotIn("akses",["4","5","6"])->get();
		$jenis_penelitian = Jenis_penelitian::where('isdelete','=','0')->get();
		// echo $jenis_penelitian->toJson();die;
		$lists=[];
		foreach($dosen as $value){
			foreach($jenis_penelitian as $value_x){
				foreach($value_x["syarat_penelitian"] as $value_p){
					if($value["jabatan_akademik"]==$value_p["jabatan_akademik"] && $value["tingkat"]==$value_p["syarat"]
					&& isset($value_x["expired_batch"]) && $value_x["expired_batch"]["is_expired"]){
						$isi = ["id"=>$value["id"],"surel"=>$value["surel"],"nama_lengkap"=>$value["nama_lengkap"],"jenis_penelitian"=>$value_x["nama"],
					"expired_batch"=>$value_x["expired_batch"]["nama"],"tanggal_mulai"=>$value_x["expired_batch"]["start"],
					"tanggal_selesai"=>$value_x["expired_batch"]["finish"]];
						if(!in_array($isi,$lists)) $lists[]=$isi;
					}
				}
			}
		}
		echo json_encode($lists);
	}

	public function sendpenelitianexpire(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		$notif = new Notifikasi;
		$notif->content = "Jenis penelitian ".$data["jenis_penelitian"]." pada ".$data["expired_batch"]." untuk bapak/ibu telah ditutup, silakan mengikuti untuk batch penelitian berikutnya.";
		$notif->dosen = $data["id"];
		$notif->jenis = "Penelitian";
		$notif->save();
		sendemail_penelitian_expire($data);
		echo "sending mail";
	}

	public function sendpenelitiananggota(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		sendemail_penelitian_anggota($data);
		echo "sending mail";
	}

	public function sendpengabdiananggota(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		sendemail_pengabdian_anggota($data);
		echo "sending mail";
	}

	public function sendpenelitiandibuat(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		sendemail_penelitian_dibuat($data);
		echo "sending mail";
	}

	public function sendpenelitiandiperbaiki(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		// sendemail_penelitian_diperbaiki($data);
		// echo "sending mail";
		$dosens = Dosen::where("isdelete","=","0")->whereIn("akses",["4","5","6"])->get()->toArray();
		foreach($dosens as $value){
			$judul = implode(' ', array_slice(explode(' ', $data["judul"]), 0, 4));
			send_notif_penelitian($value["id"],"Perbaikan Usulan penelitian dengan judul \"".$judul."\" sudah diupload, silahkan melakukan validasi");
			$waw = $data;
			$waw["surel"] = $value["surel"];
			$waw["operator"] = $value["nama_lengkap"];
			sendemail_penelitian_diperbaiki($waw);
		}
		echo "berhasil";
	}

	public function penelitianinvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Usulan proposal belum valid. Silahkan melakukan koreksi pada usulan proposal dengan judul ".$data["judul"]);
		sendemail_penelitian_invalid($data);
		echo "sending mail";
	}

	public function pengabdianinvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_pengabdian($data["id"],"Usulan proposal belum valid. Silahkan melakukan koreksi pada usulan proposal dengan judul ".$data["judul"]);
		sendemail_pengabdian_invalid($data);
		echo "sending mail";
	}

	public function penelitianvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Usulan proposal valid. Proposal dengan judul \"".$data["judul"]."\" sedang dalam proses review");
		sendemail_penelitian_valid($data);
		echo "sending mail";
	}

	public function pengabdianvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_pengabdian($data["id"],"Usulan proposal berjudul \"".$data["judul"]."\" sudah selesai divalidasi dan sedang dalam proses review");
		sendemail_pengabdian_valid($data);
		echo "sending mail";
	}

	public function penelitianreviewer(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Bapak/ibu terpilih sebagai reviewer untuk usulan penelitian \"".$data["judul"]."\", silahkan melakukan review");
		sendemail_penelitian_reviewer($data);
		echo "sending mail";
	}

	public function pengabdianreviewer(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_pengabdian($data["id"],"Bapak/ibu terpilih sebagai reviewer untuk usulan pengabdian \"".$data["judul"]."\", silahkan melakukan review");
		sendemail_pengabdian_reviewer($data);
		echo "sending mail";
	}

	public function monevpenelitianreviewer(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Bapak/ibu terpilih sebagai monev reviewer untuk usulan penelitian \"".$data["judul"]."\", silahkan melakukan review");
		// sendemail_penelitian_reviewer($data);
		echo "sending mail";
	}

	public function monevpengabdianreviewer(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_pengabdian($data["id"],"Bapak/ibu terpilih sebagai monev reviewer untuk usulan pengabdian \"".$data["judul"]."\", silahkan melakukan review");
		// sendemail_penelitian_reviewer($data);
		echo "sending mail";
	}

	public function penelitianditerima(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Hasil Review usulan proposal bapak/ibu diterima yang berjudul \"".$data["judul"]."\", bapak/ibu sudah bisa membuat surat kontrak penelitian.");
		sendemail_penelitian_diterima($data);
		echo "sending mail";
	}

	public function monevpenelitianditerima(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Hasil Review monev penelitian sudah keluar bapak/ibu diterima yang berjudul \"".$data["judul"]."\".");
		// sendemail_penelitian_diterima($data);
		echo "sending mail";
	}

	public function penelitianditerimasyarat(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Hasil Review usulan proposal bapak/ibu “Diterima dengan Syarat” yang berjudul \"".$data["judul"]."\", silakan melakukan pengecekan hasilnya.");
		sendemail_penelitian_diterimasyarat($data);
		echo "sending mail";
	}

	public function penelitianditolak(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Hasil Review usulan proposal bapak/ibu “Ditolak” yang berjudul \"".$data["judul"]."\", silakan melakukan pengecekan hasilnya.");
		sendemail_penelitian_ditolak($data);
		echo "sending mail";
	}

	public function penelitiansyaratvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Perbaikan usulan proposal bapak/ibu “Valid” yang berjudul \"".$data["judul"]."\", bapak/ibu sudah bisa membuat surat kontrak penelitian.");
		sendemail_penelitian_syarat_valid($data);
		echo "sending mail";
	}

	public function penelitiansyaratinvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Perbaikan usulan proposal bapak/ibu “Belum Valid” yang berjudul \"".$data["judul"]."\", silakan melakukan pengecekan hasilnya.");
		sendemail_penelitian_syarat_invalid($data);
		echo "sending mail";
	}

	public function laporanvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Laporan Penelitian anda dengan judul \"".$data["judul"]."\" Valid, silakan melakukan pengecekan.");
		sendemail_laporan_valid($data);
		echo "sending mail";
	}

	public function laporanpengabdianvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_pengabdian($data["id"],"Laporan Pengabdian anda dengan judul \"".$data["judul"]."\" Valid, silakan melakukan pengecekan.");
		sendemail_laporan_pengabdian_valid($data);
		echo "sending mail";
	}

	public function laporaninvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_penelitian($data["id"],"Laporan Penelitian anda dengan judul \"".$data["judul"]."\" Belum Valid, silakan melakukan pengecekan.");
		sendemail_laporan_invalid($data);
		echo "sending mail";
	}

	public function laporanpengabdianinvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_pengabdian($data["id"],"Laporan Pengabdian anda dengan judul \"".$data["judul"]."\" Belum Valid, silakan melakukan pengecekan.");
		sendemail_laporan_pengabdian_invalid($data);
		echo "sending mail";
	}

	public function suratkontrakvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_suratkontrak($data["id"],"Surat Kontrak 1 anda dengan judul ".$data["type"]." \"".$data["judul"]."\" Valid.");
		sendemail_suratkontrak_valid($data);
		echo "sending mail";
	}

	public function suratkontrakinvalid(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif_suratkontrak($data["id"],"Surat Kontrak 1 anda dengan judul ".$data["type"]." \"".$data["judul"]."\" Belum Valid, silakan melakukan pengecekan.");
		sendemail_suratkontrak_invalid($data);
		echo "sending mail";
	}

	public function pencairandanaproses(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif($data["id"],"Pencairan Dana","Pencairan Dana ".$data["type"]." dengan judul penelitian \"".stringminimize($data["judul"])."\" akan diproses oleh bagian keuangan.");
		sendemail_pencairandanaproses($data);
		echo "sending mail";
	}

	public function pencairandanaconfirm(){
		$data = $this->input->post("list");
		$data =json_decode($data,True);
		send_notif($data["id"],"Pencairan Dana","Pencairan Dana ".$data["type"]." dengan judul penelitian \"".stringminimize($data["judul"])."\" sudah dikonfirmasi oleh bagian keuangan.");
		sendemail_pencairandanaconfirm($data);
		echo "sending mail";
	}

	public function testemail(){
		$email = $this->input->get("email");
		sendemail_testemail($email);
		// echo "sending mail";
	}

	public function testceleryemail(){
		$email = [];
		$email[] = ["anggota"=>"test anggota","surel"=>"ajuna182@gmailcom","pengusul"=>"ajuna kaliantiga","judul"=>"judulnya","nama_batch"=>"Batch 1","tahun_batch"=>"2019"];
		$email[] = ["anggota"=>"test anggota","surel"=>"ajunaict@gmailcom","pengusul"=>"ajuna kaliantiga","judul"=>"judulnya","nama_batch"=>"Batch 1","tahun_batch"=>"2019"];
		celery()->PostTask('tasks.send_mail_anggota_pengabdian',array(base_url(),$email));
	}
}
