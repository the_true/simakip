<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Tahappertama extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
	private $menu = array("mpe"=>"active","pde"=>"active","pde1"=>"active");
    public function __construct() {
		$config = [
	 		"functions" => ["anchor","set_value","set_select","hitung_rupiah","format_rupiah"],
	 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
        parent::__construct($config);
    	
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Batch_lists");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
    	$this->load->model("pengabdian");
    	$this->load->model("pengabdian_anggota");
    	$this->load->model("pengabdian_laporan");
    	$this->load->model("pengabdian_review");
    	$this->load->model("surat_kontrak_pengabdian");
    	$this->load->model("Pengabdian_pencairan_dana_a");
    	$this->load->model("Pajak");

    }	
	public function index()
	{			
		$data = [];
		$jenis_pengabdian = Jenis_pengabdian::where("isdelete","=","0")->get();
		// $jenis_pengabdian->load("Batch_penelitian");
		$data["jenis_pengabdian"] = $jenis_pengabdian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$pengabdian = pengabdian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi');
		$pengabdian = $this->filter($pengabdian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
 			$pengabdian->where(function($q){
 				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
 					$q->where('anggota','=',$this->is_login()['id']);
 				});
 			});
 		}
 		
		$info = $this->create_paging($pengabdian);
		$pengabdian = $pengabdian->take($info["limit"])->skip($info["skip"])->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('surat_kontrak');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$pajak = Pajak::where("isdelete",'=','0')->where('status','=','1')->get()->first();

		foreach($pengabdian as &$item){
			if($item->pencairan_dana_tahap1()->first()===null){
				$cair1 = hitung_rupiah($item->surat_kontrak()->first()->dana,$item->pengabdian_review_inselected()->first()->rekomendasi);
				$pajak_hitung = hitung_rupiah($pajak->nominal,$cair1,1);
				$dana = new Pengabdian_pencairan_dana_a(['pengabdian'=>$item->id,'pajak'=>$pajak->nominal,'dana'=>$cair1,'pajak_hitung'=>$pajak_hitung]);

				$item->pencairan_dana_tahap1()->save($dana);
			}
		}
		$pengabdian->load('pencairan_dana_tahap1');

		$data["pengabdian"] = $pengabdian->toArray();
		$data["batch_lists"] = Batch_lists::pengabdian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("pengabdian/pencairan_dana/tahap1/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_pengabdian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_pengabdian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$bank = $this->input->get('bank');
 		if($bank){
 			$model->whereHas('dosen',function($q) use ($bank){
 				$q->where('bank','=',$bank);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

 		$status = $this->input->get('status');
 		if($status){
 			$model->whereHas('pencairan_dana_tahap1',function($q) use ($status){
 				$q->where('status','=',$status);
 			});
 		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function approval($id){
		$dana = Pengabdian_pencairan_dana_a::find($id);
		$dana->status=2;
		$dana->save();
		$this->session->set_flashdata('success', 'Pencairan Dana pengabdian berhasil di approve');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function approval_bulk(){
		$pengabdian = $this->input->post('pengabdian');
		$data = [];
		$pengabdian = pengabdian::whereIn("id",$pengabdian)
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi'); 		
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('surat_kontrak');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$pajak = Pajak::where("isdelete",'=','0')->where('status','=','1')->get()->first();
		$pengabdian->load('pencairan_dana_tahap1');

		$data["pengabdian"] = $pengabdian->toArray();

		$this->twig->display("pengabdian/pencairan_dana/tahap1/bulk/approve_bulk", $data);
	}

	public function approval_bulk_submit(){
		$pengabdian = $this->input->post('pengabdian');
		$item = pengabdian::whereIn("id",$pengabdian)->get();
		foreach($item as $pengabdian){
			$pencairan_dana_tahap1 = $pengabdian->pencairan_dana_tahap1()->first();
			$pencairan_dana_tahap1->status = 2;
			$pencairan_dana_tahap1->save();
		}
		redirect('pencairandana/tahappertama');
	}

	public function proses($id){
		$pengabdian = pengabdian::where("id","=",$id)
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi');
		$pengabdian = $pengabdian->first();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('surat_kontrak');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$pengabdian->load('pencairan_dana_tahap1');
		$data["item"] = $pengabdian->toArray();
		$data = array_merge($data,$this->menu);
		$this->twig->display("pengabdian/pencairan_dana/tahap1/proses", $data);
	}

	public function proses_submit($id){
		$tanggal_transfer = $this->input->post('tanggal');
		$pengabdian = pengabdian::find($id);
		$pencairan_dana_tahap1 = $pengabdian->pencairan_dana_tahap1()->first();
		$pencairan_dana_tahap1->tanggal_transfer = $tanggal_transfer;
		$pencairan_dana_tahap1->status = 3;
		$pencairan_dana_tahap1->save();

		$dosen = $pengabdian->dosen()->first();
		$fakultas = $dosen->fakultas()->first();
		$program_studi = $dosen->program_studi()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$dana_distujui = $pengabdian->pengabdian_review_inselected()->first();
		$tanggal_transfer = explode('/', $pencairan_dana_tahap1->tanggal_transfer);
		$tanggal_transfer = $tanggal_transfer[0].' '.angkatobulan((int)$tanggal_transfer[1]-1).' '.$tanggal_transfer[2];
		$batch = $pengabdian->batch()->first()->nama;
		$tahun = $pengabdian->tahun_kegiatan()->first()->tahun;

		$notif = [];
		$notif[] = ["id"=>$dosen->id,"surel"=>$dosen->surel,"dosen"=>$dosen->nama_lengkap,"nidn"=>$dosen->nidn,"fakultas"=>$fakultas->nama,"program_studi"=>$program_studi->nama,"judul"=>$pengabdian->judul,"jenis_pengabdian"=>$jenis_pengabdian->nama,"disetujui"=>$dana_distujui->rp_rekomendasi,"bank_rek"=>$dosen->bank_rek,"bank"=>"Bank ".$dosen->bank." - Cabang ".$dosen->bank_cabang, "dana"=>format_rupiah($pencairan_dana_tahap1->dana-$pencairan_dana_tahap1->pajak_hitung),"tanggal_transfer"=>$tanggal_transfer,"type"=>"Tahap 1","batch"=>$batch,"tahun"=>$tahun];

		celery()->PostTask('tasks.pencairan_dana_proses',array(base_url(),$notif));

		redirect('pencairandana/tahappertama');
	}

	public function proses_bulk_submit(){
		$pengabdian = $this->input->post('pengabdian');
		$tanggal_transfers = $this->input->post('tanggal');
		$notif[] = [];
		$item = pengabdian::whereIn("id",$pengabdian)->get();

		foreach($item as $pengabdian){
			$pencairan_dana_tahap1 = $pengabdian->pencairan_dana_tahap1()->first();
			$pencairan_dana_tahap1->tanggal_transfer = $tanggal_transfers;
			$pencairan_dana_tahap1->status = 3;
			$pencairan_dana_tahap1->save();

			$dosen = $pengabdian->dosen()->first();
			$fakultas = $dosen->fakultas()->first();
			$program_studi = $dosen->program_studi()->first();
			$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
			$dana_distujui = $pengabdian->pengabdian_review_inselected()->first();
			$tanggal_transfer = explode('/', $pencairan_dana_tahap1->tanggal_transfer);
			$batch = $pengabdian->batch()->first()->nama;
			$tahun = $pengabdian->tahun_kegiatan()->first()->tahun;

			$tanggal_transfer = $tanggal_transfer[0].' '.angkatobulan((int)$tanggal_transfer[1]-1).' '.$tanggal_transfer[2];
			$notif[] = ["id"=>$dosen->id,"surel"=>$dosen->surel,"dosen"=>$dosen->nama_lengkap,"nidn"=>$dosen->nidn,"fakultas"=>$fakultas->nama,"program_studi"=>$program_studi->nama,"judul"=>$pengabdian->judul,"jenis_pengabdian"=>$jenis_pengabdian->nama,"disetujui"=>$dana_distujui->rp_rekomendasi,"bank_rek"=>$dosen->bank_rek,"bank"=>"Bank ".$dosen->bank." - Cabang ".$dosen->bank_cabang, "dana"=>format_rupiah($pencairan_dana_tahap1->dana-$pencairan_dana_tahap1->pajak_hitung),"tanggal_transfer"=>$tanggal_transfer,"type"=>"Tahap 1","batch"=>$batch,"tahun"=>$tahun];
		}
		celery()->PostTask('tasks.pencairan_dana_proses',array(base_url(),$notif));
		redirect('pencairandana/tahappertama');
	}

	public function proses_bulk(){
		$pengabdian = $this->input->post('pengabdian');
		$data = [];

		$pengabdian = pengabdian::whereIn("id",$pengabdian)
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi'); 		
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('surat_kontrak');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$pajak = Pajak::where("isdelete",'=','0')->where('status','=','1')->get()->first();
		$pengabdian->load('pencairan_dana_tahap1');

		$data["pengabdian"] = $pengabdian->toArray();

		$this->twig->display("pengabdian/pencairan_dana/tahap1/bulk/proses_bulk", $data);

	}

	public function confirm($id){
		$tanggal = $this->input->post('tanggal_konfirm');
		$pengabdian = pengabdian::find($id);
		$pencairan_dana_tahap1 = $pengabdian->pencairan_dana_tahap1()->first();
		$pencairan_dana_tahap1->tanggal_konfirm = $tanggal;
		$pencairan_dana_tahap1->status = 4;
		$pencairan_dana_tahap1->save();

		$dosen = $pengabdian->dosen()->first();
		$fakultas = $dosen->fakultas()->first();
		$program_studi = $dosen->program_studi()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$dana_distujui = $pengabdian->pengabdian_review_inselected()->first();
		$tanggal_transfer = explode('/', $pencairan_dana_tahap1->tanggal_transfer);
		$tanggal_transfer = $tanggal_transfer[0].' '.angkatobulan((int)$tanggal_transfer[1]-1).' '.$tanggal_transfer[2];
		$batch = $pengabdian->batch()->first()->nama;
		$tahun = $pengabdian->tahun_kegiatan()->first()->tahun;

		$notif = [];
		$notif[] = ["id"=>$dosen->id,"surel"=>$dosen->surel,"dosen"=>$dosen->nama_lengkap,"nidn"=>$dosen->nidn,"fakultas"=>$fakultas->nama,"program_studi"=>$program_studi->nama,"judul"=>$pengabdian->judul,"jenis_pengabdian"=>$jenis_pengabdian->nama,"disetujui"=>$dana_distujui->rp_rekomendasi,"bank_rek"=>$dosen->bank_rek,"bank"=>"Bank ".$dosen->bank." - Cabang ".$dosen->bank_cabang, "dana"=>format_rupiah($pencairan_dana_tahap1->dana-$pencairan_dana_tahap1->pajak_hitung),"tanggal_transfer"=>$tanggal_transfer,"type"=>"Tahap 1","batch"=>$batch,"tahun"=>$tahun];

		celery()->PostTask('tasks.pencairan_dana_confirm',array(base_url(),$notif));

		redirect('pencairandana/tahappertama');
	}

	public function confirm_bulk(){
		$pengabdian = $this->input->post('pengabdian');
		$data = [];

		$pengabdian = pengabdian::whereIn("id",$pengabdian)
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi'); 		
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('surat_kontrak');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$pajak = Pajak::where("isdelete",'=','0')->where('status','=','1')->get()->first();
		$pengabdian->load('pencairan_dana_tahap1');

		$data["pengabdian"] = $pengabdian->toArray();

		$this->twig->display("pengabdian/pencairan_dana/tahap1/bulk/confirm_bulk", $data);

	}

	public function confirm_bulk_submit(){
		$pengabdian = $this->input->post('pengabdian');
		$tanggal_konfirm = $this->input->post('tanggal');
		$notif[] = [];
		$item = pengabdian::whereIn("id",$pengabdian)->get();

		foreach($item as $pengabdian){
			$pencairan_dana_tahap1 = $pengabdian->pencairan_dana_tahap1()->first();
			$pencairan_dana_tahap1->tanggal_konfirm = $tanggal_konfirm;
			$pencairan_dana_tahap1->status = 4;
			$pencairan_dana_tahap1->save();

			$dosen = $pengabdian->dosen()->first();
			$fakultas = $dosen->fakultas()->first();
			$program_studi = $dosen->program_studi()->first();
			$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
			$dana_distujui = $pengabdian->pengabdian_review_inselected()->first();
			$tanggal_transfer = explode('/', $pencairan_dana_tahap1->tanggal_konfirm);
			$batch = $pengabdian->batch()->first()->nama;
			$tahun = $pengabdian->tahun_kegiatan()->first()->tahun;

			$tanggal_transfer = $tanggal_transfer[0].' '.angkatobulan((int)$tanggal_transfer[1]-1).' '.$tanggal_transfer[2];
			$notif[] = ["id"=>$dosen->id,"surel"=>$dosen->surel,"dosen"=>$dosen->nama_lengkap,"nidn"=>$dosen->nidn,"fakultas"=>$fakultas->nama,"program_studi"=>$program_studi->nama,"judul"=>$pengabdian->judul,"jenis_pengabdian"=>$jenis_pengabdian->nama,"disetujui"=>$dana_distujui->rp_rekomendasi,"bank_rek"=>$dosen->bank_rek,"bank"=>"Bank ".$dosen->bank." - Cabang ".$dosen->bank_cabang, "dana"=>format_rupiah($pencairan_dana_tahap1->dana-$pencairan_dana_tahap1->pajak_hitung),"tanggal_transfer"=>$tanggal_transfer,"type"=>"Tahap 1","batch"=>$batch,"tahun"=>$tahun];
		}
		celery()->PostTask('tasks.pencairan_dana_confirm',array(base_url(),$notif));
		redirect('pencairandana/tahappertama');
	}

	public function dataExcel(){
		$pengabdian = pengabdian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi');
		$pengabdian = $this->filter($pengabdian,$data);
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('surat_kontrak');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$pengabdian->load('pencairan_dana_tahap1');
		return $pengabdian->toArray();
	}

	public function download(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
 			'borders' => array(
 				'bottom' => $default_border,
 				'left' => $default_border,
 				'top' => $default_border,
 				'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'E1E0F7'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 16,
 				)
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_pengabdian_pencairan_dana.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		$excel->setActiveSheetIndex(0);

 		$status="Semua Data";
 		$fakultas="Semua Jenis pengabdian";
 		$batch="Semua Batch pengabdian";

 		$sts = $this->input->get('status');
 		if($sts=='1')$status = 'Menunggu Persetujuan';
 		else if($sts=='2')$status = 'Sudah Disetujui';
 		else if($sts=='3')$status = 'Proses Transfer';
 		else if($sts=='4')$status = 'Sudah Transfer';

 		$fak = $this->input->get("jenis_pengabdian");
 		if($fak){
 			$fakultas=$data[0]["jenis_pengabdian"]["nama"];
 		}

 		$bat = $this->input->get("batch");
 		if($bat){
 			$batch=$data[0]["batch"]["nama"].' - '.$data[0]["tahun_kegiatan"]["tahun"];
 		}
 
 		$excel->setActiveSheetIndex(0)
 		->setCellValue('F7',$status)
 		->setCellValue('F8','Per Tanggal '.date('d/m/y'))
 		->setCellValue('D9',$fakultas)
 		->setCellValue('D10',$batch);


		$firststyle='B14';
		$laststyle='B14';
		$totals = 0;
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+14;
			$num='B'.$urut;
			$nama_peneliti='C'.$urut;
			$judul_pengabdian='E'.$urut;
			$bank = 'G'.$urut;
			$bank_rek = 'H'.$urut;
			$total = 'I'.$urut;
			$dana = $data[$i]['pencairan_dana_tahap1']['dana']-$data[$i]['pencairan_dana_tahap1']['pajak_hitung'];
			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])->mergeCells($nama_peneliti.':D'.$urut)
			->setCellValue($judul_pengabdian, $data[$i]['judul']." \n".$data[$i]["jenis_pengabdian"]["nama"]." \n".$data[$i]["batch"]["nama"]."-".$data[$i]["tahun_kegiatan"]["tahun"])->mergeCells($judul_pengabdian.':F'.$urut)
			->setCellValue($bank,$data[$i]['dosen']['bank'])
			->setCellValueExplicit($bank_rek, $data[$i]['dosen']['bank_rek'],PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue($total, $dana);

			$totals +=$dana;

			$excel->setActiveSheetIndex(0)->getStyle($judul_pengabdian)->getAlignment()->setWrapText(true);
			$excel->getActiveSheet()->getRowDimension($i+14)->setRowHeight(-1);
			$laststyle=$total;
		}

		$excel->setActiveSheetIndex(0)
		->setCellValue('B'.($urut+1),"Total")
		->mergeCells('B'.($urut+1).':H'.($urut+1))
		->setCellValue("I".($urut+1),$totals);
		$laststyle="I".($urut+1);
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		// for($col = 'A'; $col !== 'I'; $col++) {
		// 	if($col =='D' || $col =='F') continue;
		//     $excel->getActiveSheet()
		//         ->getColumnDimension($col)
		//         ->setAutoSize(true);
		// }
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('pengabdian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_pencairandana_tahap1_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}

}
