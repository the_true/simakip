<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class MonevLists extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
	 private $menu = array("mpe"=>"active","mpey"=>"active","mpey1"=>"active");
    public function __construct() {
		$config = [
	 		"functions" => ["anchor","set_value","set_select"],
	 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
        parent::__construct($config);
    	
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Batch_lists");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
    	$this->load->model("Pengabdian");
    	$this->load->model("Pengabdian_anggota");
    	$this->load->model("Pengabdian_review");
    	$this->load->model("surat_kontrak_pengabdian");

    	$this->load->model('Pengabdian_monev');
    	$this->load->model('Pengabdian_monev_review');
    	$this->load->model('Pengabdian_monev_reviewer');
    	$this->load->model('Pengabdian_monev_nilai_luaran');
		$this->load->model('Pengabdian_monev_nilai_hasil');
    	$this->load->model('Bobot_luaran');
    	$this->load->model('Bobot_hasil');
    	$this->load->model('Bobot_kriteria');
    }	
	public function index()
	{			
		$data = [];
		$jenis_pengabdian = Jenis_pengabdian::where("isdelete","=","0")->get();
		// $jenis_pengabdian->load("batch_penelitian");
		$data["jenis_pengabdian"] = $jenis_pengabdian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$pengabdian = Pengabdian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi');

		$pengabdian = $this->filter($pengabdian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
 			$pengabdian->where(function($q){
 				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
 					$q->where('anggota','=',$this->is_login()['id']);
 				});
 			});
		 }
 		
		$info = $this->create_paging($pengabdian);
		$pengabdian = $pengabdian->take($info["limit"])->skip($info["skip"])->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('jenis_pengabdian')->load('monev')->load('monev_reviewer')->load('monev_reviewer.dosen');
		$pengabdian->load('tahun_kegiatan');

		$data["pengabdian"] = $pengabdian->toArray();
		$data["batch_lists"] = Batch_lists::pengabdian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("pengabdian/monev/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_pengabdian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_pengabdian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

 		$status = $this->input->get('status');
 		if($status==1){
 			$model->whereHas('monev',function($q){
 				$q->where('berkas','LIKE','%pdf');
 			});
 		}else if($status==2){
 			$model->whereHas('monev',function($q){
 				$q->where('borang','=',0);
 			});
 		}else if($status==3){
 			$model->whereHas('monev',function($q){
 				$q->where('reviewer','=',0);
 			});
 		}else if($status==4){
 			$model->whereHas('monev',function($q){
 				$q->where('reviewer','=',1);
 			});
 		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function reviewer(){
 		$data = [];
 		$data["pengabdian"] = $this->uri->segment(5);
 		$dosen = Dosen::where("isdelete","=","0")->where(function($q){
			$q->where("isreviewer","=","2")->orWhere("isreviewer","=","3");
        })->get();
 		$dosen = $dosen->toArray();
 		$item = Pengabdian_monev_reviewer::where("pengabdian","=",$data['pengabdian'])->with('dosen')
 		->with('dosen.program_studi')
 		->with('dosen.jabatan_akademik')
 		->get();
 		$data["reviewer"] = $item->toArray();
 		$data = array_merge($data,$this->menu,["dosen"=>$dosen]);
 		// echo $data['pengabdian']; die;
 		$this->twig->display("pengabdian/monev/reviewer/reviewer", $data);
 	}

 	public function add_reviewer(){
 		$dosen = $this->input->post('dosen');
 		$pengabdian = $this->input->post('pengabdian');
 		$item = new Pengabdian_monev_reviewer;
 		$item->dosen = $dosen;
 		$item->pengabdian = $pengabdian;
 		$item->save();

 		$count = Pengabdian_monev_reviewer::where('pengabdian','=',$pengabdian)->count();
 		if($count>=2){
	 		$data = Pengabdian_monev::where('pengabdian','=',$this->input->post('pengabdian'))->first();
			if($data==null) $data = new Pengabdian_monev;
			$data->reviewer = 1;
			$data->pengabdian = $pengabdian;
			$data->save();
		}


		// echo $item->pengabdian()->first()->toJson(); die();

		$jenis_pengabdian = $item->pengabdian()->first()->jenis_pengabdian()->first();

 		$item->load('dosen');
 		$item->load('pengabdian.dosen');
 		$item = $item->toArray();
 		$data = [];

		$first_four_title = implode(' ', array_slice(explode(' ', $item["pengabdian"]["judul"]), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memilih '.$item["dosen"]["nama_lengkap"].' sebagai monev reviewer Pengabdian pada jenis pengabdian '.$jenis_pengabdian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

 		$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
							"judul"=>$item["pengabdian"]["judul"],"pengusul"=>$item["pengabdian"]["dosen"]["nama_lengkap"]];
 		// echo json_encode($data) ; die;
 		celery()->PostTask('tasks.monevpengabdian_reviewers',array(base_url(),$data));

 		redirect('pengabdian/monev/monevlists/reviewer/'.$item["pengabdian"]["id"]);
 	}

	public function konfirm_reviewer(){
		$pengabdian=$this->input->get('pengabdian');
		$item = Pengabdian_monev_reviewer::where('pengabdian','=',$pengabdian)->get();
		$item->load('dosen');
		$item->load('pengabdian.dosen');
		// echo $item->toJson();die;
		$items = $item->toArray();
		$data = [];
		// foreach($items as $item){
		// 	$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
		// 					"judul"=>$item["pengabdian"]["judul"],"pengusul"=>$item["pengabdian"]["dosen"]["nama_lengkap"],
		// 					"deadline"=>$item["deadline"]];
		// }
		// celery()->PostTask('tasks.monevpengabdian_reviewers',array(base_url(),$data));
		redirect('pengabdian/monev/monevlists/');
	}

 	public function verify_reviewer(){
 		$pengabdian = $this->input->get('pengabdian');
 		$item = Pengabdian_monev_reviewer::where('pengabdian','=',$pengabdian)->count();
 		if($item==2){
 			$this->output->set_header('HTTP/1.0 500 Reviewer sudah penuh');
 			return;
 		}

 		$item = Pengabdian_monev_reviewer::where('pengabdian','=',$pengabdian)
 		->whereHas('dosen',function($q){
 			$q->where('nidn','=',$this->input->get('nidn'));
 		})->count();
 		if($item>0){
 			$this->output->set_header('HTTP/1.0 500 Reviewer sudah ada');
 			return;
 		}

 		echo "berhasil";
 	}

 	public function delete_reviewer(){
 		$id = $this->uri->segment(5);
 		$item = Pengabdian_monev_reviewer::find($id);
 		$pengabdian = $item->pengabdian;


 		$data = Pengabdian_monev::where('pengabdian','=',$pengabdian)->first();
		if($data==null) $data = new Pengabdian_monev;
		$data->pengabdian = $pengabdian;
		$data->reviewer = 0;
		$data->save();
 	// 	$pengabdian = $item->pengabdian()->first();
 	// 	$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
 	// 	$dosen = $item->dosen()->first();
		// $first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus '.$dosen->nama.' sebagai reviewer Pengabdian pada jenis pengabdian '.$jenis_pengabdian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		$item->delete();
 		redirect('pengabdian/monev/monevlists/reviewer/'.$pengabdian);
 	}

 	public function upload_dokumen(){
		$id = $this->uri->segment(4);
		$this->generate_folder('uploads/borang');
		// echo json_encode($_FILES); die;
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/borang'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		// echo $id; die;
		if($uploaded){
			$this->session->set_flashdata('success', 'Upload berkas berhasil!');
			$nomor = $this->input->post("nomor");
			$data = Pengabdian_monev::where('pengabdian','=',$this->input->post('pengabdian'))->first();
			if($data==null) $data = new Pengabdian_monev;
			$data->pengabdian = $this->input->post('pengabdian');
			$data->berkas=$this->upload->data('file_name');
			$data->save();
			echo '{"file":"'.$this->upload->data('file_name').'","id":"'.$id.'"}';
			// redirect($_SERVER['HTTP_REFERER']);
		}else{
			echo $this->upload->display_errors();
		}
	 }
	 
	public function pelaksanaan(){
		$this->session->set_flashdata('success', 'Pelaksanaan waktu dan tempat berhasil!');
		$pengabdian = $this->input->post("pengabdian");
		$waktu = $this->input->post("waktu");
		$lokasi = $this->input->post("lokasi");
		$data = Pengabdian_monev::where('pengabdian','=',$this->input->post('pengabdian'))->first();
		if($data==null) $data = new Pengabdian_monev;
		$data->pengabdian = $pengabdian;
		$data->waktu = $waktu;
		$data->lokasi = $lokasi;
		$data->save();

		$data->load("pengabdian")->load("pengabdian.dosen");
		$this->load->model("akses");
		$dosennotif = Dosen::whereHas("akses",function($q){
			$q->WhereIn("nama",["Operator LPPM", "Sekeretaris LPPM", "Ketua LPPM"]);
		})->get();

		$pengabdian = $data->pengabdian()->first();
		$pengusul = $pengabdian->dosen()->first();
		foreach($dosennotif as $dosen){
			$data_notif = ["surel"=>$dosen->surel,"pengusul"=>$pengusul->nama,"judul"=>$pengabdian->judul,"waktu"=>$waktu,"tempat"=>$lokasi];
			sendemail_pengabdian_pelaksanaan($data_notif);
			send_notif_pengabdian($dosen->id,"Lokasi dan tempat pelaksanaan monev untuk pengabdian ".$data->pengabdian()->first()->judul." akan dilakukan pada ".$waktu." di ".$lokasi);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

 	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
			 mkdir($folder_name,0777, true);
		}
	}

	private function dataExcel(){
		$pengabdian = Pengabdian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('pengabdian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen.fakultas');
		$pengabdian = $this->filter($pengabdian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
 			$pengabdian->where(function($q){
 				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
 					$q->where('anggota','=',$this->is_login()['id']);
 				});
 			});
 		}
 		
		$info = $this->create_paging($pengabdian);
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch');
		$pengabdian->load('jenis_pengabdian')->load('monev')->load('monev_reviewer')->load('monev_reviewer.dosen');
		$pengabdian->load('tahun_kegiatan');
		return $pengabdian->toArray();
	}

 	public function download(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
 			'borders' => array(
 				'bottom' => $default_border,
 				'left' => $default_border,
 				'top' => $default_border,
 				'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'E1E0F7'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 16,
 				)
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				// 'bottom' => $default_border,
 				// 'left' => $default_border,
 				// 'top' => $default_border,
 				// 'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_monev_pengabdian.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_pengabdian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$jenis_pengabdian = 'G'.$urut;
			$batchtahun = 'H'.$urut;
			$fakultas = 'I'.$urut;
			$reviewer1 = 'J'.$urut;
			$reviewer2 = 'K'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_pengabdian, $data[$i]['judul'])->mergeCells($judul_pengabdian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($jenis_pengabdian, $data[$i]['jenis_pengabdian']["nama"])
			->setCellValue($batchtahun, $data[$i]['batch']["nama"]."-".$data[$i]["tahun_kegiatan"]["tahun"])
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);

			if(count($data[$i]['monev_reviewer'])==2){
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer1, $data[$i]['monev_reviewer'][0]["dosen"]["nama_lengkap"]);
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer2, $data[$i]['monev_reviewer'][1]["dosen"]["nama_lengkap"]);
			}

			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle=$reviewer2;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		for($col = 'A'; $col !== 'N'; $col++) {
		    $excel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
		}
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Pengabdian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_Pengabdian_monevPengabdian_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
