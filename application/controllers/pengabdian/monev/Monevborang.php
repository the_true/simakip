<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class MonevBorang extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
	private $menu = array("mpe"=>"active","mpey"=>"active","mpey1"=>"active");
    public function __construct() {
		$config = [
	 		"functions" => ["anchor","set_value","set_select","set_radio"],
	 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
        parent::__construct($config);
    	
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
    	$this->load->model("Pengabdian");
    	$this->load->model("Pengabdian_anggota");
    	$this->load->model("Pengabdian_review");

    	$this->load->model('Pengabdian_monev_review');
    	$this->load->model('Pengabdian_monev_borang');
		$this->load->model('Pengabdian_monev_luaran');
		$this->load->model('Pengabdian_monev');
    }	
	public function index()
	{			
		$data=[];
		$id = $this->uri->segment(4);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$pengabdian->load('batch')->load('pengabdian_review_inselected');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');
		// echo $pengabdian->toJson(); die;
		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Pengabdian_monev_borang::where('pengabdian','=',$id)->where('dosen','=', $this->is_login()['id'])->first();
 		if($monev) $pengabdian["monev"] = $monev->toArray();

 		// echo json_encode($monev->toArray()); die;

		$data = array_merge($pengabdian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("pengabdian/monev/borang/proses_pengabdian", $data);
	}

	public function view(){
		$data=[];
		$id = $this->uri->segment(5);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$pengabdian->load('batch')->load('pengabdian_review_inselected');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');
		// echo $pengabdian->toJson(); die;
		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Pengabdian_monev_borang::where('pengabdian','=',$id)->first();
 		if($monev) $pengabdian["monev"] = $monev->toArray();

 		// echo json_encode($monev->toArray()); die;

		$data = array_merge($pengabdian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("pengabdian/monev/borang_view/proses_pengabdian", $data);
	}

	public function luaranview(){
		$pengabdian = $this->uri->segment(5);
		$data = ["id"=>$pengabdian];
		$data["items1"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','ilmiah')->get()->toArray();
		$data["items2"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','buku')->get()->toArray();
		$data["items3"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','forum')->get()->toArray();
		$data["items4"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','speaker')->get()->toArray();
		$data["items5"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','media_masa')->get()->toArray();
		$data["items6"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','luaran')->get()->toArray();
		$data["items7"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','video')->get()->toArray();
		// echo json_encode($data["item1"]); die;
		$this->twig->display("pengabdian/monev/borang_view/luaran_kegiatan", $data);
	}

	public function luaran(){
		$pengabdian = $this->uri->segment(5);
		$data = ["id"=>$pengabdian];
		$data["items1"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','ilmiah')->get()->toArray();
		$data["items2"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','buku')->get()->toArray();
		$data["items3"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','forum')->get()->toArray();
		$data["items4"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','speaker')->get()->toArray();
		$data["items5"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','media_masa')->get()->toArray();
		$data["items6"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','luaran')->get()->toArray();
		$data["items7"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','video')->get()->toArray();
		// echo json_encode($data["item1"]); die;
		$this->twig->display("pengabdian/monev/borang/luaran_kegiatan", $data);
	}

	public function uploadluaran(){
		$id = $this->uri->segment(5);
		$this->generate_folder('uploads/borangluaran');
		// echo json_encode($_FILES); die;
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/borangluaran'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		// echo $id; die;
		if($uploaded){
			$this->session->set_flashdata('success', 'Upload berkas berhasil!');
			$nomor = $this->input->post("nomor");
			$data = Pengabdian_monev_luaran::find($id);
			$data->berkas=$this->upload->data('file_name');
			$data->save();
			echo '{"file":"'.$this->upload->data('file_name').'","id":'.$id.'}';
			// celery()->PostTask('tasks.suratkontrak_submit', array(base_url(),$data->id));

			// $pengabdian = $data->pengabdian()->first();
			// $jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
			// $first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
			// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengupload Surat Kontrak 1 pada jenis pengabdian '.$jenis_pengabdian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);


		}else{
			echo $this->upload->display_errors();
		}
	}



	public function insertluaran(){
		$pengabdian = $this->uri->segment(5);
		$content = json_encode($_POST);
		$type = $this->input->post('type');

		// echo $content; die;		
		$item = new Pengabdian_monev_luaran;
		$item->pengabdian = $pengabdian;
		$item->content = $content;
		$item->type = $type;
		$item->save();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function editluaran(){
		$pengabdian = $this->uri->segment(4);
		$content = json_encode($_POST);
		$id = $this->input->post('id');	
		$item = Pengabdian_monev_luaran::find($id);
		$item->content = $content;
		$item->save();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function deleteluaran(){
		$id = $this->uri->segment(4);
		$content = json_encode($_POST);
		$item = Pengabdian_monev_luaran::find($id);
		$item->delete();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function insert()
	{			
		$this->session->set_flashdata('success', 'Borang berhasi disimpan!');
		$content = urldecode(json_encode($_POST));
		$pengabdian = $this->input->post('pengabdian');
		$dosen = $this->is_login()['id'];

		$item = Pengabdian_monev_borang::where('pengabdian','=',$pengabdian)->where('dosen','=', $this->is_login()['id'])->first();
		if($item===null) $item = new Pengabdian_monev_borang;
		$item->pengabdian = $pengabdian;
		$item->dosen = $dosen;
		$item->content = $content;

		$item->save();
		// echo $content;


		$data = Pengabdian_monev::where('pengabdian','=',$pengabdian)->first();
		if($data==null) $data = new Pengabdian_monev;
		$data->pengabdian = $pengabdian;
		$data->borang = 1;
		$data->save();
		
		redirect("pengabdian/monev/monevborang/luaran/".$pengabdian);
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
			 mkdir($folder_name,0777, true);
		}
	}

	public function downloadborang(){
		$data=[];
		$id = $this->uri->segment(5);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$pengabdian->load('batch')->load('pengabdian_review_inselected');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');
		// echo $pengabdian->toJson(); die;
		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Pengabdian_monev_borang::where('pengabdian','=',$id)->first();
 		if($monev) $pengabdian["monev"] = $monev->toArray();

 		// echo json_encode($monev->toArray()); die;

		$data = array_merge($pengabdian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$isi = $this->twig->render("pengabdian/monev/borang_view/pdf/borangpdf", $data);
		$title = "Pengabdian_monev Borang Pengabdian ".stringminimize($pengabdian["judul"]).".pdf";
		$this->compose_pdf($isi,$title);
	}

	public function downloadluaranpengabdian(){
		$pengabdian = $this->uri->segment(5);
		$data = ["id"=>$pengabdian];
		$pengabdians = Pengabdian::find($pengabdian)->toArray();
		$data["judul"] = $pengabdians["judul"];
		$data["items1"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','ilmiah')->get()->toArray();
		$data["items2"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','buku')->get()->toArray();
		$data["items3"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','forum')->get()->toArray();
		$data["items4"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','speaker')->get()->toArray();
		$data["items5"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','media_masa')->get()->toArray();
		$data["items6"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','luaran')->get()->toArray();
		$data["items7"] = Pengabdian_monev_luaran::where('pengabdian','=',$pengabdian)->where('type','=','video')->get()->toArray();
		// echo json_encode($data["items1"]); die;
		$isi = $this->twig->render("pengabdian/monev/borang_view/pdf/luaranpdf", $data);
		$title = "Pengabdian_monev Luaran Pengabdian ".stringminimize($pengabdians["judul"]).".pdf";
		$this->compose_pdf($isi,$title);
	}



	private function compose_pdf($isi,$title){
	    $this->load->helper('pdf_helper');
	    $this->load->helper('url');
	    tcpdf();
	    $obj_pdf = new suratkontraklppmpdf('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	    $obj_pdf->SetCreator(PDF_CREATOR);
	    // $title = "PDF Report";
	    $obj_pdf->SetTitle($title);
	    // $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
	    // $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	    // $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	    $obj_pdf->SetDefaultMonospacedFont('helvetica');
	    $obj_pdf->SetHeaderMargin(0);
	    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
	    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	    $obj_pdf->SetFont('helvetica', '', 9);
	    $obj_pdf->setFontSubsetting(false);
	    $obj_pdf->setListIndentWidth(1);
	    $obj_pdf->AddPage();


	    ob_start();
	    echo $isi;
	    $content = ob_get_contents();
	    ob_end_clean();
	    $obj_pdf->writeHTML($content, true, false, true, false, '');
	    $obj_pdf->Output($title,'I');
	    echo $title;
	    // if($hasil=="hasil") $obj_pdf->Output('review_pengabdian_'.$data['dosen']['nama'].'.pdf', 'I');
	    // else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	    return $obj_pdf;
	}

}
