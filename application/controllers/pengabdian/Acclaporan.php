<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Acclaporan extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mpe"=>"active","mpe7"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_select"],
		    "functions_safe" => ["form_open","form_open_multipart","validation_errors_array"],
		];
        parent::__construct($config);
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Pengabdian");
    	$this->load->model("Pengabdian_anggota");
    	$this->load->model("Pengesah");
    	$this->load->model("Penilaian");
    	$this->load->model("Penilaian_kriteria");
    	$this->load->model("Pengabdian_review");
    	$this->load->model("Pengabdian_reviewer");
    	$this->load->model("Pengabdian_nilai");
    	$this->load->model("Pengabdian_laporan");
    	$this->load->model("Batch_lists");
    	$this->load->model("Pengabdian_Monev");
    }
	public function index()
	{
		$data = [];
		$jenis_pengabdian = Jenis_pengabdian::where("isdelete","=","0")->get();
		// $jenis_pengabdian->load("batch_penelitian");
		$data["jenis_pengabdian"] = $jenis_pengabdian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$pengabdian = Pengabdian::where("isdelete","=","0")->where('status','=','4')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_menyetujui')
										 ->with('dosen_mengetahui')
										 ->with('pengabdian_reviewer')
										 ->whereHas('monev',function($q){
										 	$q->where('hasilreview','=',1)->where('reviewer','=',1)->where('borang','=',1);
										 });
	 	$pengabdian = $this->filter($pengabdian,$data);
		if($this->is_login()['akses']['nama']=='Dosen'){
			$pengabdian->where(function($q){
				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
					$q->where('anggota','=',$this->is_login()['id']);
				});
			});
		}
	 	// die;
		$info = $this->create_paging($pengabdian);
		$pengabdian = $pengabdian->take($info["limit"])->skip($info["skip"])->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun')->load('pengabdian_laporan');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');

		$data["pengabdian"] = $pengabdian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("pengabdian/acclaporan/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_pengabdian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_pengabdian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

		$status = $this->input->get('status');
		if($status){
			if($status=="5") $status="0";
			$model->whereHas('pengabdian_laporan',function($q) use ($status) {
				$q->where('status','=',$status);
			});
		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function acc($id){
		// echo $id; die;
		$item = Pengabdian::find($id);
		$item->acc = 1;
		$item->save();

		$item->load('dosen');
		$item = $item->toArray();

		$first_four_title = implode(' ', array_slice(explode(' ', $item['judul']), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'submit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' Melakukan ACC Laporan Pengabdian pada penelitian dengan nama judul/kegiatan '.$first_four_title.' dengan nama ketua peneliti '.$item['dosen']['nama_lengkap'],'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

		$this->session->set_flashdata('success', 'ACC Pengabdian dengan judul '.$item['judul'].' berhasil!');
		redirect($_SERVER['HTTP_REFERER']);
	}
}
