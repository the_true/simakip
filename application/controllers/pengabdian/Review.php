<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Review extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mpe"=>"active","mpe2"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_select"],
		    "functions_safe" => ["form_open","form_open_multipart"],
		];
        parent::__construct($config);
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
      $this->load->model("Mitra");
    	$this->load->model("Iptek");
    	$this->load->model("Pengabdian");
    	$this->load->model("Pengabdian_anggota");
    	$this->load->model("Pengesah");
    	$this->load->model("Penilaian");
    	$this->load->model("Penilaian_kriteria");
    	$this->load->model("Pengabdian_review");
    	$this->load->model("Pengabdian_reviewer");
    	$this->load->model("Pengabdian_nilai");
    	$this->load->model("Batch_lists");
    }
	public function index()
	{
		$data = [];
		$jenis_pengabdian = Jenis_pengabdian::where("isdelete","=","0")->get();
		// $jenis_pengabdian->load("batch_penelitian");
		$data["jenis_pengabdian"] = $jenis_pengabdian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$pengabdian = Pengabdian::where("isdelete","=","0")->where('status','>=','3')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('pengabdian_reviewer')
										 ->with('pengabdian_review_exselected')->with('pengabdian_review_exselected.dosen')->with('pengabdian_review_inselected');
		$pengabdian = $this->filter($pengabdian,$data);
		if($this->is_login()['akses']['nama']=='Dosen'){
			$pengabdian->where(function($q){
 				$q->whereHas('pengabdian_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
		 }
		 

		$info = $this->create_paging($pengabdian,$data);
		$pengabdian = $pengabdian->take($info["limit"])->skip($info["skip"])->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');

		$data["pengabdian"] = $pengabdian->toArray();
		$data["mitra"] = Mitra::where("isdelete","=","0")->get()->toArray();
		$data["iptek"] = Iptek::where("isdelete","=","0")->get()->toArray();
		$data["batch_lists"] = Batch_lists::pengabdian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("pengabdian/review/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_pengabdian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_pengabdian','=',$jp);
		}

		$mitra = $this->input->get('mitra');
		if($mitra){
			$model->where('mitra','=',$mitra);
	   	}

		$iptek = $this->input->get('iptek');
		if($iptek){
			$model->where('iptek','=',$iptek);
	  	}

 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

		$hasil = $this->input->get('hasil');
		if($hasil){
			$model->whereHas('pengabdian_review',function($q) use ($hasil){
				$q->where('status','=',$hasil);
			});
		}

		$status = $this->input->get('status');
		if($status){
			if($status=='1'){
				$model->whereHas('pengabdian_review',function($q) use ($status){
					$q->where('isselected','=','1')->where('catatan_rekomendasi','<>','');
				});
			}else if($status=='2'){
				$model->whereDoesntHave('pengabdian_review', function($q){
					$q->where('isselected','=','1')->where('catatan_rekomendasi','<>','');
				});
			}else if($status=='3'){
				$model->doesntHave('pengabdian_review');
			}
		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function pengabdian(){
		$data = [];
		$id = $this->uri->segment(4);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->find($id);
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');
		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

		$data = array_merge($pengabdian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("pengabdian/review/review",$data);
	}

	public function pengabdianfinal($id){
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.fakultas')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_mengetahui')
										 ->with('pengabdian_review_inselected')
										 ->with('pengabdian_review_exselected')
										 ->with('pengabdian_review_exselected.dosen')
										 ->with('pengabdian_review_exselected.dosen.program_studi')
										 ->with('pengabdian_review_exselected.dosen.jabatan_akademik')
										 ->with('pengabdian_review_exselected.pengabdian_nilai')
										 ->with('pengabdian_review_exselected.pengabdian_nilai.penilaian_kriteria')
										 ->find($id);
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');

		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

		$data = array_merge($pengabdian,["anggaran"=>$anggaran->toArray()],$this->menu);
		// echo json_encode($data); die;
		$this->twig->display('pengabdian/review/operator_review',$data);
	}

	public function pengabdianfinalresult($id){
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.fakultas')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_mengetahui')
										 ->with('pengabdian_review_inselected')
										 ->with('pengabdian_review_exselected')
										 ->with('pengabdian_review_exselected.dosen')
										 ->with('pengabdian_review_exselected.dosen.program_studi')
										 ->with('pengabdian_review_exselected.dosen.jabatan_akademik')
										 ->with('pengabdian_review_exselected.pengabdian_nilai')
										 ->with('pengabdian_review_exselected.pengabdian_nilai.penilaian_kriteria')
										 ->find($id);
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');

		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_pengabdian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

		$data = array_merge($pengabdian,["anggaran"=>$anggaran->toArray()],$this->menu);
		// echo json_encode($data); die;
		$this->twig->display('pengabdian/review/operator_review_result',$data);
	}

	public function postpengabdianfinal(){
		$pengabdian = $this->input->post("id");
		$status = $this->input->post("status");
		$rekomendasi = $this->input->post("rekomendasi");
		// $catatan_rekomendasi = $this->input->post("catatan_review");

		$update = Pengabdian_review::where('pengabdian','=',$pengabdian)->where('isselected','=',1)->first();
		$update->status = $status;
		$update->rekomendasi = $rekomendasi;
		$update->catatan_rekomendasi = "wawaw";
		$update->save();

		$pengabdian = $update->pengabdian()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'review','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' melakukan final review Pengabdian pada jenis pengabdian '.$jenis_pengabdian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
		
		redirect('pengabdian/review/');
	}

	public function result(){
		$data = [];
		$id = $this->uri->segment(4);
		$dosen = $this->uri->segment(6);
		$hasil = $this->uri->segment(7);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
		->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->find($id);
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');
		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where("isdelete","=","0")->first();
 		$data["anggaran"] = $anggaran->toArray();

 		$result = Pengabdian_review::where('pengabdian','=',$id);
		if($hasil=="hasil"){
			$result = $result->where('isselected','=',1);
			$data["status_akhir"] = $pengabdian["status"];
			$data["review_lain"] = Pengabdian_review::where('pengabdian','=',$id)->where('isselected','=','0')->get()->toArray();
			// echo json_encode($data); die;
		}else{
			$result = $result->where('dosen','=',$dosen)
										->with('dosen')
										->with('dosen.program_studi')
										->with('dosen.jabatan_akademik');
		}
		$result= $result->first();
 		if($result!=null){
			$result->load('pengabdian_nilai')->load('pengabdian_nilai.penilaian_kriteria');
	 		$data["hasil"] = $result->toArray();
	 		// echo json_encode($result->toArray());
	 		// die;
 		}
 		$data["id_review"] = $id;
 		$data["id_reviewer"] = $dosen;
 		$data["index"] = $this->uri->segment(6);
 		if($hasil=="hasil") $data["isselected"]= True;
		$data = array_merge($pengabdian,$data,["mp"=>"active"]);
		$this->twig->display('pengabdian/review/result',$data);
	}

	public function step1(){
		$pengabdian = $this->input->post('pengabdian');
		if(empty($pengabdian)){
			$this->output->set_header('HTTP/1.0 500 Pengabdian Not Found');
			return;
		}
		$pengabdian_review = $this->input->post('pengabdian_review');
		$rekomendasi = $this->input->post('rekomendasi');
		$catatan_rekomendasi = $this->input->post('catatan_rekomendasi');
		$id_nilai = $this->input->post('penilaians');
		$skors = $this->input->post('skors');
		$nilai = $this->input->post('nilai');
		$catatan = $this->input->post('catatan');
		$data = Pengabdian_review::firstOrNew(['pengabdian'=>$pengabdian,'dosen'=>$this->is_login()['id']]);
		$data->pengabdian = $pengabdian;
		$data->rekomendasi = is_null($rekomendasi)?0:$rekomendasi;
		$data->catatan_rekomendasi = $catatan_rekomendasi;
		$data->dosen = $this->is_login()['id'];
		$data->save();

		for($i=0;$i<count($id_nilai);$i++){
			$input = ["pengabdian_review"=>$data->id, "pengabdian"=>$pengabdian, "penilaian_kriteria"=>$id_nilai[$i]];
			$datax = Pengabdian_nilai::firstOrNew($input);
			$datax->skor = $skors[$i];
			$datax->nilai = $nilai[$i];
			$datax->catatan = $catatan[$i];
			$datax->save();
		}

		echo $data->toJson();
	}

	public function step2(){
		$pengabdian_review = $this->input->post('pengabdian_review');
		if(empty($pengabdian_review)){
			$this->output->set_header('HTTP/1.0 500 Pengabdian Not Found');
			return;
		}
		$alasan = $this->input->post('alasan');
		$status = $this->input->post('status');
		$saran  = $this->input->post('saran');
		$catatan_review  = $this->input->post('catatan_review');
		$data = Pengabdian_review::find($pengabdian_review);
		$data->berkas = $this->input->post('upl_catatan_review_res');
		$data->alasan = $alasan;
		$data->status = $status;
		$data->saran  = $saran;
		$data->catatan_review = $catatan_review;
		$data->save();

		$data->load('pengabdian_nilai');
		echo $data->toJson();
	}

	public function uploadcatatanreview(){
		$this->generate_folder('uploads/pengabdianreview');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/pengabdianreview'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function step3(){
		$pengabdian_review = $this->input->post('pengabdian_review');
		if(empty($pengabdian_review)){
			$this->output->set_header('HTTP/1.0 500 Pengabdian Not Found');
			return;
		}
		$data = Pengabdian_review::find($pengabdian_review);
		$data->isvalid = 1;
		$data->save();

		$flag = Pengabdian_reviewer::where('pengabdian','=',$data->pengabdian)->where('dosen','=',$data->dosen)->first();
		$flag->status = 1;
		$flag->save();

		$reviewer = Pengabdian_reviewer::where('pengabdian','=',$data->pengabdian);
		$reviewer_count = $reviewer->count();
		$review = Pengabdian_review::where('pengabdian','=',$data->pengabdian)->where('isselected','=','0')->where('isvalid','=','1');
		$review_count = $review->count();
		$pengabdian = Pengabdian::find($data->pengabdian);

 		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'review','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mereview Pengabdian pada jenis pengabdian '.$jenis_pengabdian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

		if($review_count==2){
			$review = $review->get();
			$review_array = $review->toArray();
			$current_status=3;
			$review1 = $review_array[0];
			$review2 = $review_array[1];

			$average_nilai = Pengabdian_nilai::select('pengabdian','penilaian_kriteria',new raw('ROUND(avg(skor),1) AS skor'),new raw('ROUND(avg(nilai),2) AS nilai'), new raw('GROUP_CONCAT(catatan SEPARATOR "\n\n") as catatan'))->where("pengabdian","=",$data->pengabdian)->groupBy('penilaian_kriteria')->get();
			$average_nilai = $average_nilai->toArray();
			$check = Pengabdian_review::where('pengabdian','=',$data->pengabdian)->where('isvalid','=',1)->where('isselected','=',1)->first();
			if($check) redirect('pengabdian/review/');
			$new_review = new Pengabdian_review;
			$new_nilai = new Pengabdian_nilai;
			$new_review->pengabdian=$data->pengabdian;
			$new_review->isvalid= 1;
			$new_review->isselected= 1;

			if($review1["status"]==1 && $review2["status"]==1){
				$new_review->status = 1;
				$new_review->rekomendasi=($review1["rekomendasi"]+$review2["rekomendasi"])/2;
				// $pengabdian->status = 4;
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul];
				// celery()->PostTask('tasks.pengabdian_diterima', array(base_url(),$data));
			}else if($review1["status"]==2 && $review2["status"]==2){
				$new_review->status = 2;
				$new_review->rekomendasi=($review1["rekomendasi"]+$review2["rekomendasi"])/2;
				// if($review1["rekomendasi"]>$review2["rekomendasi"]){
				// 	$selected = Pengabdian_review::find($review1["id"]);
				// 	$selected->isselected=1;
				$new_review->saran = $review1["saran"]."\n\n".$review2["saran"];
				// $pengabdian->status = 11;
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul,"notes"=>$new_review->saran];
				// celery()->PostTask('tasks.pengabdian_diterimasyarat', array(base_url(),$data));
			}else if($review1["status"]==3 && $review2["status"]==3){
				$new_review->status = 3;
				$new_review->rekomendasi=0;
				$new_review->alasan = $review2["alasan"]."\n\n".$review1["alasan"];
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul,"notes"=>$new_review->alasan];
				// celery()->PostTask('tasks.pengabdian_ditolak', array(base_url(),$data));
				// $selected->save();
				// $pengabdian->status=12;
			}else if(($review1["status"]==1 && $review2["status"]==2) || ($review1["status"]==2 && $review2["status"]==1)){
				$new_review->status = 2;
				// $new_review->alasan = $review1["status"]==2? $review1["alasan"]:$review2["alasan"];
				$new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];
				$new_review->rekomendasi=($review1["rekomendasi"]+$review2["rekomendasi"])/2;
				// $pengabdian->status=11;
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul,"notes"=>$new_review->saran];
				// celery()->PostTask('tasks.pengabdian_diterimasyarat', array(base_url(),$data));
			}else if(($review1["status"]==1 && $review2["status"]==3) || ($review1["status"]==3 && $review2["status"]==1)){
				// $new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];

				$nilai1 = Pengabdian_nilai::where("pengabdian_review","=",$review1["id"])->sum("nilai");
				$nilai2 = Pengabdian_nilai::where("pengabdian_review","=",$review2["id"])->sum("nilai");
				$total_nilai = ($nilai1+$nilai2)/2;
				if($total_nilai>=500){
					$new_review->rekomendasi=$review1["status"]==1? $review1["rekomendasi"]:$review2["rekomendasi"];
					$new_review->status = 1;
					// $pengabdian->status=4;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul];
					// celery()->PostTask('tasks.pengabdian_diterima', array(base_url(),$data));
				}else{
					$new_review->rekomendasi=0;
					$new_review->alasan = $review1["status"]==3? $review1["alasan"]:$review2["alasan"];
					$new_review->status = 3;
					// $pengabdian->status= 12;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul,"notes"=>$new_review->alasan];
					// celery()->PostTask('tasks.pengabdian_ditolak', array(base_url(),$data));
				}
			}else if(($review1["status"]==2 && $review2["status"]==3) || ($review1["status"]==3 && $review2["status"]==2)){
				// $new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];


				$nilai1 = Pengabdian_nilai::where("pengabdian_review","=",$review1["id"])->sum("nilai");
				$nilai2 = Pengabdian_nilai::where("pengabdian_review","=",$review2["id"])->sum("nilai");
				$total_nilai = ($nilai1+$nilai2)/2;
				if($total_nilai>=500){
					$new_review->rekomendasi=$review1["status"]==2? $review1["rekomendasi"]:$review2["rekomendasi"];
					$new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];
					$new_review->status =2;
					// $pengabdian->status=11;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul,"notes"=>$new_review->saran];
					// celery()->PostTask('tasks.pengabdian_diterimasyarat', array(base_url(),$data));
				}else{
					$new_review->rekomendasi=0;
					$new_review->alasan = $review1["status"]==3? $review1["alasan"]:$review2["alasan"];
					$new_review->status = 3;
					// $pengabdian->status= 12;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul,"notes"=>$new_review->alasan];
					// celery()->PostTask('tasks.pengabdian_ditolak', array(base_url(),$data));
				}
			}else{
				echo "Some thing is wrong";
				die;
				// $id = 0;
				// foreach($review_array as $value){
				// 	if($value["status"]<$current_status){
				// 		$id=$value["id"];
				// 		$current_status=$value["status"];
				// 	}
				// }
				// if($current_status==1){
				// 	$pengabdian->status = 4;
				// }else if($current_status==2){
				// 	$pengabdian->status = 11;
				// }else if($current_status==3){
				// 	$pengabdian->status=12;
				// }
				// $selected = Pengabdian_review::find($id);
				// $selected->isselected=1;
				// $selected->save();
			}
			$pengabdian->save();
			$new_review->save();
			foreach($average_nilai as $key=>$value){
				$average_nilai[$key]['pengabdian_review']=$new_review->id;
				// $test[$key]['isselected']=1;
			}
			Pengabdian_nilai::insert($average_nilai);
		}
		redirect('pengabdian/review/');
		// $pengabdian
	}

	public function fix_pengabdian(){
		$id=$this->input->get('pengabdian');
		$reviewer = Pengabdian_reviewer::where('pengabdian','=',$id);
		$reviewer_count = $reviewer->count();
		$review = Pengabdian_review::where('pengabdian','=',$id)->where('isselected','=','0');
		$review_count = $review->count();
		$pengabdian = Pengabdian::find($id);
		if($review_count==2){
			$review = $review->get();
			$review_array = $review->toArray();
			$current_status=3;
			$review1 = $review_array[0];
			$review2 = $review_array[1];

			$average_nilai = Pengabdian_nilai::select('pengabdian','penilaian_kriteria',new raw('ROUND(avg(skor),1) AS skor'),new raw('ROUND(avg(nilai),2) AS nilai'))->where("pengabdian","=",$id)->groupBy('penilaian_kriteria')->get();
			$average_nilai = $average_nilai->toArray();

			$check=Pengabdian_review::where('pengabdian','=',$id)->where('isselected','=','1')->count();
			if($check>0){
				echo "sudah ada hasil";
				die;
			}
			$new_review = new Pengabdian_review;
			$new_nilai = new Pengabdian_nilai;
			$new_review->pengabdian=$pengabdian->id;
			$new_review->isvalid= 1;
			$new_review->isselected= 1;

			if($review1["status"]==1 && $review2["status"]==1){
				$new_review->status = 1;
				$new_review->rekomendasi=($review1["rekomendasi"]+$review2["rekomendasi"])/2;
				$pengabdian->status = 4;
				// $pengabdianx =
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul];
				celery()->PostTask('tasks.pengabdian_diterima', array(base_url(),$data));
			}else if($review1["status"]==2 && $review2["status"]==2){
				$new_review->status = 2;
				$new_review->rekomendasi=($review1["rekomendasi"]+$review2["rekomendasi"])/2;
				// if($review1["rekomendasi"]>$review2["rekomendasi"]){
				// 	$selected = Pengabdian_review::find($review1["id"]);
				// 	$selected->isselected=1;
				$new_review->saran = $review1["saran"]."\n\n".$review2["saran"];
				// 	$selected->save();
				// }else{
				// 	$selected = Pengabdian_review::find($review2["id"]);
				// 	$selected->isselected=1;
				// 	$selected->saran = $review2["saran"]."\n\n".$review1["saran"];
				// 	$selected->save();
				// }
				$pengabdian->status = 11;
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul,"notes"=>$new_review->saran];
				celery()->PostTask('tasks.pengabdian_diterimasyarat', array(base_url(),$data));

			}else if($review1["status"]==3 && $review2["status"]==3){
				$new_review->status = 3;
				$new_review->rekomendasi=0;
				// $selected = Pengabdian_review::find($review1["id"]);
				// $selected->rekomendasi=0;
				// $selected->isselected=1;
				$new_review->alasan = $review2["alasan"]."\n\n".$review1["alasan"];
				// $selected->save();
				$pengabdian->status=12;
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul,"notes"=>$new_review->alasan];
				// celery()->PostTask('tasks.pengabdian_ditolak', array(base_url(),$data));
			}else if(($review1["status"]==1 && $review2["status"]==2) || ($review1["status"]==2 && $review2["status"]==1)){
				$new_review->status = 2;
				// $new_review->alasan = $review1["status"]==2? $review1["alasan"]:$review2["alasan"];
				$new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];
				$new_review->rekomendasi=($review1["rekomendasi"]+$review2["rekomendasi"])/2;
				$pengabdian->status=11;
				$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
							 "judul"=>$pengabdian->judul,"notes"=>$new_review->saran];
				// celery()->PostTask('tasks.pengabdian_diterimasyarat', array(base_url(),$data));
			}else if(($review1["status"]==1 && $review2["status"]==3) || ($review1["status"]==3 && $review2["status"]==1)){
				// $new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];

				$nilai1 = Pengabdian_nilai::where("pengabdian_review","=",$review1["id"])->sum("nilai");
				$nilai2 = Pengabdian_nilai::where("pengabdian_review","=",$review2["id"])->sum("nilai");
				$total_nilai = ($nilai1+$nilai2)/2;
				if($total_nilai>=4){
					$new_review->rekomendasi=$review1["status"]==1? $review1["rekomendasi"]:$review2["rekomendasi"];
					$new_review->status = 1;
					$pengabdian->status=4;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul];
					// celery()->PostTask('tasks.pengabdian_diterima', array(base_url(),$data));
				}else{
					$new_review->rekomendasi=0;
					$new_review->alasan = $review1["status"]==3? $review1["alasan"]:$review2["alasan"];
					$new_review->status = 3;
					$pengabdian->status= 12;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul,"notes"=>$new_review->alasan];
					// celery()->PostTask('tasks.pengabdian_ditolak', array(base_url(),$data));
				}
			}else if(($review1["status"]==2 && $review2["status"]==3) || ($review1["status"]==3 && $review2["status"]==2)){
				// $new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];


				$nilai1 = Pengabdian_nilai::where("pengabdian_review","=",$review1["id"])->sum("nilai");
				$nilai2 = Pengabdian_nilai::where("pengabdian_review","=",$review2["id"])->sum("nilai");
				$total_nilai = ($nilai1+$nilai2)/2;
				if($total_nilai>=4){
					$new_review->rekomendasi=$review1["status"]==2? $review1["rekomendasi"]:$review2["rekomendasi"];
					$new_review->saran = $review1["status"]==2? $review1["saran"]:$review2["saran"];
					$new_review->status =2;
					$pengabdian->status=11;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul,"notes"=>$new_review->saran];
					// celery()->PostTask('tasks.pengabdian_diterimasyarat', array(base_url(),$data));
				}else{
					$new_review->rekomendasi=0;
					$new_review->alasan = $review1["status"]==3? $review1["alasan"]:$review2["alasan"];
					$new_review->status = 3;
					$pengabdian->status= 12;
					$data=["id"=>$pengabdian->dosen()->first()->id,"nama"=>$pengabdian->dosen()->first()->nama_lengkap,"surel"=>$pengabdian->dosen()->first()->surel,
								 "judul"=>$pengabdian->judul,"notes"=>$new_review->alasan];
					// celery()->PostTask('tasks.pengabdian_ditolak', array(base_url(),$data));
				}
			}else{
				echo "Some thing is wrong";
				die;
			}
			$pengabdian->save();
			$new_review->save();
			foreach($average_nilai as $key=>$value){
				$average_nilai[$key]['pengabdian_review']=$new_review->id;
				// $test[$key]['isselected']=1;
			}
			// echo json_encode($average_nilai);
			// die;
			Pengabdian_nilai::insert($average_nilai);
			echo json_encode($average_nilai);
			// echo "berhasil";
		}
	}

	public function lembar_penilaian_pdf(){
		$hasil = $this->uri->segment(6);
		$this->load->helper('pdf_helper');
		$this->load->helper('url');
		tcpdf();
		$obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		// $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		// $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(0);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$data = $this->getDataPDF();
		$this->load->view('pengabdian/review/lembar_penilaian.php',$data);
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
 		if($hasil=="hasil") $obj_pdf->Output('review_pengabdian_'.$data['dosen']['nama'].'.pdf', 'I');
 		else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	}

	public function lembar_saran_pdf(){
		$hasil = $this->uri->segment(6);
		$this->load->helper('pdf_helper');
		$this->load->helper('url');
		tcpdf();
		$obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		// $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		// $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$data = $this->getDataPDF();
		$this->load->view('pengabdian/review/lembar_saran.php',$data);
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
 		if($hasil=="hasil") $obj_pdf->Output('review_pengabdian_'.$data['dosen']['nama'].'.pdf', 'I');
 		else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	}

	private function getDataPDF(){
		$data = [];
		$id = $this->uri->segment(4);
		$dosen = $this->uri->segment(6);
		$hasil = $this->uri->segment(7);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->find($id);
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan')->load('jenis_pengabdian');
		$pengabdian = $pengabdian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$pengabdian["jenis_pengabdian"]["id"])->where("status","=","1")->where("isdelete","=","0")->first();
 		$data["anggaran"] = $anggaran->toArray();

 		$result = Pengabdian_review::where('pengabdian','=',$id);
		if($hasil=="hasil"){
			$result = $result->where('isselected','=',1);
		}else{
			$result = $result->where('dosen','=',$dosen)
										->with('dosen')
										->with('dosen.program_studi')
										->with('dosen.jabatan_akademik');
		}
		$result= $result->first();
if($result!=null){
			$result->load('pengabdian_nilai')->load('pengabdian_nilai.penilaian_kriteria');
	 		$data["hasil"] = $result->toArray();
 		}

 		if($hasil=="hasil") $data["isselected"]= False;
 		else  $data["isselected"]= True;

		$data = array_merge($pengabdian,$data);
		// echo "<pre>";
		// print_r($data["hasil"]);
		// die;
		$data["status_akhir"] = $pengabdian["status"];
		$data["review_lain"] = Pengabdian_review::where('pengabdian','=',$id)->where('isselected','=','0')->get()->toArray();
		return $data;
	}

	private function dataExcel(){
		$this->load->model('Fakultas');
		$pengabdian = Pengabdian::where("isdelete","=","0")->where('status','>=','3')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen.fakultas')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->with('Pengabdian_reviewer')->with('pengabdian_review_exselected')->with('pengabdian_review_inselected');
		$pengabdian = $this->filter($pengabdian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$pengabdian->where(function($q){
 				$q->whereHas('pengabdian_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
	 	}
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('pengabdian_reviewer.dosen');
		$pengabdian->load('tahun_kegiatan');
 	// 	echo $pengabdian->toJson();
		// die;
		return $pengabdian->toArray();
	}

 	public function download(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
 			'borders' => array(
 				'bottom' => $default_border,
 				'left' => $default_border,
 				'top' => $default_border,
 				'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'E1E0F7'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 16,
 				)
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_pengabdian.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		// ->setLastModifiedBy("Sigit prasetya n")
 		// ->setTitle("Creating file excel with php Test Document")
 		// ->setSubject("Creating file excel with php Test Document")
 		// ->setDescription("How to Create Excel file from PHP with PHPExcel 1.8.0 Classes by seegatesite.com.")
 		// ->setKeywords("phpexcel")
 		// ->setCategory("Test result file");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_pengabdian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$lokasi = 'G'.$urut;
			$anggaran_usulan = 'H'.$urut;
			$anggaran_rekomendasi= ['I'.$urut,'J'.$urut];
			$fakultas = 'K'.$urut;
			$anggaran_disetujui = 'L'.$urut;
			$reviewer1 = 'M'.$urut;
			$reviewer2 = 'N'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$rekomendasi_string=[];
			for($x=0;$x<count($data[$i]["pengabdian_review_exselected"]);$x++){
				$rekomendasi_string[]= $data[$i]["pengabdian_review_exselected"][$x]["rp_rekomendasi"];
			}

			$disetujui_string = "";
			for($x=0;$x<count($data[$i]["pengabdian_review_inselected"]);$x++){
				if($data[$i]["pengabdian_review_inselected"][$x]["isselected"]==1){
					$disetujui_string = $data[$i]["pengabdian_review_inselected"][$x]["rp_rekomendasi"];
				}
			}

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_pengabdian, $data[$i]['judul'])->mergeCells($judul_pengabdian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($lokasi, $data[$i]['jenis_pengabdian']['nama'])
			->setCellValueExplicit($anggaran_usulan, $data[$i]['rp_total_biaya'],PHPExcel_Cell_DataType::TYPE_STRING);
			// echo json_encode($rekomendasi_string);
			for($waw=0;$waw<count($rekomendasi_string);$waw++){
				// echo $anggaran_rekomendasi[$waw].' '.$rekomendasi_string[$waw]."<br>";
				if($waw>=2) continue;
				$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_rekomendasi[$waw], $rekomendasi_string[$waw],PHPExcel_Cell_DataType::TYPE_STRING);

			}
			
			
			$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_disetujui, $disetujui_string,PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['pengabdian_reviewer'])
			if(count($data[$i]['pengabdian_reviewer'])==2){
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer1, $data[$i]['pengabdian_reviewer'][0]["dosen"]["nama_lengkap"]);
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer2, $data[$i]['pengabdian_reviewer'][1]["dosen"]["nama_lengkap"]);
			}
			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			// for($waw=0;$waw<count($rekomendasi_string);$waw++){
			// 	$excel->setActiveSheetIndex(0)->getStyle($anggaran_rekomendasi[$waw])->getAlignment()->setWrapText(true);
			// }
			// $excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle=$reviewer2;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		// for($col = 'A'; $col !== 'N'; $col++) {
		//     $excel->getActiveSheet()
		//         ->getColumnDimension($col)
		//         ->setAutoSize(true);
		// }
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Pengabdian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_PengabdianHibah_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}

 	private function generate_folder($folder_name){
 		if(!is_dir($folder_name))
 		{
 			mkdir($folder_name,0777, true);
 		}
 	}
}
