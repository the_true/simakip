<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Pforumilmiahs extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("lp"=>"active","lp6"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
		$this->load->model('luaran_pengabdian/Penyelenggara_forum_ilmiah');
		$this->load->model('Tahun_kegiatan');
    }
	public function index()
	{
		$default_view = 10;
		$page = 0;

		$judul_get = $this->input->get("nama");
		$tahun_get = $this->input->get("tahun");
		$tingkat_get = $this->input->get("tingkat");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		$user = $this->is_login();

		$penyelenggara_forum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete','=','0');
		if(isset($page_get)) $page = $page_get-1;
		if(isset($judul_get)) $penyelenggara_forum_ilmiah->where('nama','LIKE','%'.$judul_get.'%');
		if(isset($tahun_get) && $tahun_get!='') $penyelenggara_forum_ilmiah->where('tahun_kegiatan','=',$tahun_get);
		if(isset($tingkat_get) && $tingkat_get!='') $penyelenggara_forum_ilmiah->where('tingkat','=',$tingkat_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $penyelenggara_forum_ilmiah->where('isvalidate','=',(int)$isvalidate-1);

		if(!roles(["Operator LPPM","Sekretaris LPPM","Ketua LPPM","Rektor","Wakil Rektor"])){
			$penyelenggara_forum_ilmiah->where('dosen', '=', $user["id"]);
		}
		$default_skip = $page*$default_view;
		$count  = $penyelenggara_forum_ilmiah->count();
		$penyelenggara_forum_ilmiah = $penyelenggara_forum_ilmiah->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();

		$penyelenggara_forum_ilmiah->load('dosen')->load('tahun_kegiatan');
		$data = array("items"=>$penyelenggara_forum_ilmiah->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['tingkat'] = $tingkat_get;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('pengabdian/luaran/penyelenggara_forum_ilmiah/index', array_merge($data,$this->menu));
	}

	private function validation($edit=FALSE){
		$this->form_validation->set_rules('nama', 'Nama Kegiatan', 'required');
		$this->form_validation->set_rules('unit', 'Unit Pelaksana', 'required');
		// $this->form_validation->set_rules('skala', 'Skala Forum Ilmiah', 'required');
		$this->form_validation->set_rules('mitra', 'Mitra/Sponsorship', 'required');
		$this->form_validation->set_rules('waktu', 'Waktu Pelaksanaan', 'required');
		$this->form_validation->set_rules('tempat', 'Tempat Pelaksanaan', 'required');
		$this->form_validation->set_rules('tingkat', 'Tingkat Forum Penelitian','required');
		$this->form_validation->set_rules('tahun', 'Tahun','required');
  //       if (!$edit){
		// 	if (empty($_FILES['file']['name']))
		// 	{
		// 	    $this->form_validation->set_rules('file', 'Berkas', 'required');
		// 	}
		// }
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data = array("items_tahun"=>$tahun->toArray());
            $this->twig->display('pengabdian/luaran/penyelenggara_forum_ilmiah/add', array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect('pengabdian/luaran/pforumilmiahs/');
        		// $this->twig->display('pengabdian/luaran/penyelenggara_forum_ilmiah/succeed');
        	else echo $this->upload->display_errors();
		}
	}

	public function resubmit(){
        $id = $this->uri->segment(5);
        $item = Penyelenggara_forum_ilmiah::find($id);
        if(!$this->isPengguna($item->dosen)) return;
        $item->isvalidate = 0;
        $item->save();
        redirect($_SERVER['HTTP_REFERER']);
    }


	public function submit($id=0){
		$this->generate_folder('uploads/pforumilmiahs');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/pforumilmiahs'),
			'allowed_types' => 'pdf',
			'max_size' => 2000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$nama = $this->input->post('nama');
		$unit = $this->input->post('unit');
		// $skala = $this->input->post('skala');
		$mitra = $this->input->post('mitra');
		$waktu = $this->input->post('waktu');
		$tempat = $this->input->post('tempat');
		$tingkat = $this->input->post('tingkat');
		$dosen = $this->input->post('dosen');
		$tahun = $this->input->post('tahun');
		// $file_field = "file";
		$uploaded = TRUE;
		// if($id==0){
		// 	$uploaded = $this->upload->do_upload($file_field);
		// }
		if ($uploaded){
			if($id==0){
				$penyelenggara_forum_ilmiah = new Penyelenggara_forum_ilmiah;
			}else{
				$penyelenggara_forum_ilmiah = Penyelenggara_forum_ilmiah::find($id);
			}
			if($penyelenggara_forum_ilmiah->isvalidate!=1){
				$penyelenggara_forum_ilmiah->isvalidate = 3;
			}
			$penyelenggara_forum_ilmiah->nama = $nama;
			$penyelenggara_forum_ilmiah->dosen = $dosen;
			$penyelenggara_forum_ilmiah->unit = $unit;
			// $penyelenggara_forum_ilmiah->skala = $skala;
			$penyelenggara_forum_ilmiah->mitra = $mitra;
			$penyelenggara_forum_ilmiah->waktu = $waktu;
			$penyelenggara_forum_ilmiah->tempat = $tempat;
			$penyelenggara_forum_ilmiah->tingkat = $tingkat;
			$penyelenggara_forum_ilmiah->tahun_kegiatan = $tahun;
			$penyelenggara_forum_ilmiah->identifier = "pengabdian";
			// if($id==0)$penyelenggara_forum_ilmiah->berkas = $this->upload->data('file_name');
			$penyelenggara_forum_ilmiah->save();
			$first_four_title = implode(' ', array_slice(explode(' ', $nama), 0, 4));
			if($id==0){
				
				send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Penyelenggara Forum Ilmiah baru dengan nama kegiatan \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Penyelenggara Forum Ilmiah baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			}else{
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Penyelenggara Forum Ilmiah dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
				$this->session->set_flashdata('success', 'Luaran dengan nama '.$penyelenggara_forum_ilmiah->nama.' berhasil diubah!');
			}
			return TRUE;
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	public function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function show(){
		$id = $this->uri->segment(5);
		$penyelenggara_forum_ilmiah = Penyelenggara_forum_ilmiah::whereId($id)->with('dosen')->with('tahun_kegiatan')->with('dosen.program_studi')->get();
		$data = array("item"=>$penyelenggara_forum_ilmiah->toArray()[0]);
		$this->twig->display('pengabdian/luaran/penyelenggara_forum_ilmiah/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function edit(){
		$this->is_login();
		$id = $this->uri->segment(5);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);


        if ($this->form_validation->run() == FALSE){
					$jurnal = Penyelenggara_forum_ilmiah::whereId($id)->with('dosen')->with('dosen.program_studi')->get();
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
			if(($jurnal[0]->isvalidate==1  && !$this->isPengguna($jurnal[0]->dosen)) || !$this->isOperator() && !$this->isPengguna($jurnal[0]->dosen)  ){ $this->twig->display('404'); return; }
        	$data = array("items_tahun"=>$tahun->toArray());
					$this->twig->display('pengabdian/luaran/penyelenggara_forum_ilmiah/edit', array_merge($jurnal->toArray()[0],$this->menu,$data));
        }else{
        	if ($this->submit($id))
        		redirect('pengabdian/luaran/pforumilmiahs/');
        		// $this->twig->display('pengabdian/luaran/penyelenggara_forum_ilmiah/succeed');
        	else echo $this->upload->display_errors();
		}

	}

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Penyelenggara_forum_ilmiah::find($id);
		if(!$this->isPengguna($item->dosen)) return;
		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->nama), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Penyelenggara Forum Ilmiah dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('pengabdian/luaran/pforumilmiahs/');
	}

	public function validate(){
  if(!$this->isOperator()) return;
		$id = $this->uri->segment(5);
		$item = Penyelenggara_forum_ilmiah::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
		$item->save();

		$first_four_title = implode(' ', array_slice(explode(' ', $item->nama), 0, 4));
        if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Penyelenggara Forum Ilmiah anda dengan nama \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
            send_notif_luaran($item->dosen, "Data Penyelenggara Forum Ilmiah anda dengan nama \"" . $first_four_title . "...\" telah divalidasi Operator");
        }

		$first_four_title = implode(' ', array_slice(explode(' ', $item->nama), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Penyelenggara Forum Ilmiah dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$penyelenggara_forum_ilmiah = Penyelenggara_forum_ilmiah::where('isdelete','=','0');
		$jabatan = "Ketua LPPM UHAMKA";
		$nama = "Prof. Dr. Hj. Nani Solihati, M. Pd.";
		if(!roles(["Operator LPPM","Sekretaris LPPM","Ketua LPPM","Rektor","Wakil Rektor"])){
			$penyelenggara_forum_ilmiah->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}
		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$penyelenggara_forum_ilmiah->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}

		$penyelenggara_forum_ilmiah = $penyelenggara_forum_ilmiah->get();
		$penyelenggara_forum_ilmiah->load('dosen');
		$penyelenggara_forum_ilmiah->load('tahun_kegiatan');
		$this->load->view('pengabdian/luaran/penyelenggara_forum_ilmiah/pdf',array("items"=>$penyelenggara_forum_ilmiah->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_PenyelenggaraForumIlmiah_'.Date('dmY').'.pdf', 'I');
	}


}
