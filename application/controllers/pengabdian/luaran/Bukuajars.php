<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Bukuajars extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("lp"=>"active","lp2"=>"active");
    public function __construct() {
		$config = [
			'function' => ['anchor','set_value'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
        $this->load->model('Fakultas');
		$this->load->model('luaran_pengabdian/Buku_ajar');
		$this->load->model('Tahun_kegiatan');
        $this->load->model('Point');
        $this->load->model('Point_log');
    }
	public function index()
	{
		$default_view = 10;
		$page = 0;

		$judul_get = $this->input->get("judul");
		$tahun_get = $this->input->get("tahun");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");


		$user = $this->is_login();
		$buku_ajar = Buku_ajar::where('isdelete', '=', '0');
		if(isset($page_get)) $page = $page_get-1;
		if(isset($judul_get)) $buku_ajar->where('judul','LIKE','%'.$judul_get.'%');
		if(isset($tahun_get) && $tahun_get!='') $buku_ajar->where('tahun_kegiatan','=',$tahun_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $buku_ajar->where('isvalidate','=',(int)$isvalidate-1);

		if(!roles(["Operator LPPM","Sekretaris LPPM","Ketua LPPM","Rektor","Wakil Rektor"])){
			$buku_ajar->where('dosen', '=', $user["id"]);
		}
		$default_skip = $page*$default_view;
		$count  = $buku_ajar->count();
		$buku_ajar = $buku_ajar->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();
		$buku_ajar->load('dosen')->load('tahun_kegiatan');
		$data = array("items"=>$buku_ajar->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('pengabdian/luaran/buku_ajar/index', array_merge($data,$this->menu));
	}

	private function validation($edit=False){
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('isbn', 'ISBN', 'required');
		$this->form_validation->set_rules('halaman', 'Halaman', 'required');
		$this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
		$this->form_validation->set_rules('tahun', 'Tahun', 'required');
  //       if (!$edit){
		// 	if (empty($_FILES['file']['name']))
		// 	{
		// 	    $this->form_validation->set_rules('file', 'Berkas', 'required');
		// 	}
		// }
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data = array("items_tahun"=>$tahun->toArray());
            $this->twig->display('pengabdian/luaran/buku_ajar/add',array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect('pengabdian/luaran/bukuajars/');
        		// $this->twig->display('pengabdian/luaran/buku_ajar/succeed');
        	else echo $this->upload->display_errors();
		}
	}


	private function submit($id=0){
		$is_upload = FALSE;
		if (!empty($_FILES['file']['name']))
		{
			    $is_upload = TRUE;
		}
		$this->generate_folder('uploads/bukuajars');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/bukuajars'),
			'allowed_types' => 'pdf',
			'max_size' => 2000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$judul = $this->input->post('judul');
		$isbn = $this->input->post('isbn');
		$halaman = $this->input->post('halaman');
		$penerbit = $this->input->post('penerbit');
		$dosen = $this->input->post('dosen');
		$tahun = $this->input->post('tahun');
		// $file_field = "file";
		$uploaded = TRUE;
		// if($is_upload){
		// 	$uploaded = $this->upload->do_upload($file_field);
		// }
		if ($uploaded){
			if($id==0){
				$buku_ajar = new Buku_ajar;
			}else{
				$buku_ajar = Buku_ajar::find($id);
			}
			if($buku_ajar->isvalidate!=1){
				$buku_ajar->isvalidate = 3;
			}
			$buku_ajar->judul = $judul;
			$buku_ajar->dosen = $dosen;
			$buku_ajar->isbn = $isbn;
			$buku_ajar->halaman = $halaman;
			$buku_ajar->penerbit = $penerbit;
			$buku_ajar->tahun_kegiatan = $tahun;
			$buku_ajar->identifier = "pengabdian";
			// if($is_upload) $buku_ajar->berkas = $this->upload->data('file_name');
			$buku_ajar->save();
			$first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
			if($id==0){
				send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Buku Ajar/Teks baru dengan judul \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Buku ajar/Teks baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			}else{
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Buku ajar/Teks dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
				$this->session->set_flashdata('success', 'Luaran dengan nama '.$buku_ajar->judul.' berhasil diubah!');
			}
			return TRUE;
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function show(){
		$id = $this->uri->segment(5);
		$jurnal = Buku_ajar::whereId($id)->with('dosen')->with('dosen.program_studi')->with('tahun_kegiatan')->get();
		$data = array("item"=>$jurnal->toArray()[0]);
		$this->twig->display('pengabdian/luaran/buku_ajar/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function edit(){
		$this->is_login();
		$id = $this->uri->segment(5);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);


        if ($this->form_validation->run() == FALSE){
					$jurnal = Buku_ajar::whereId($id)->with("dosen")->with("dosen.program_studi")->get();
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
            if(($jurnal[0]->isvalidate==1  && !$this->isPengguna($jurnal[0]->dosen)) || !$this->isOperator() && !$this->isPengguna($jurnal[0]->dosen)  ){ $this->twig->display('404'); return; }
            $data = array("items_tahun"=>$tahun->toArray());
			$this->twig->display('pengabdian/luaran/buku_ajar/edit', array_merge($jurnal->toArray()[0],$this->menu,$data));
        }else{
        	if ($this->submit($id))
        		redirect('pengabdian/luaran/bukuajars/');
        		// $this->twig->display('pengabdian/luaran/buku_ajar/succeed');
        	else echo $this->upload->display_errors();
		}
	}

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Buku_ajar::find($id);
		if(!$this->isPengguna($item->dosen)) return;
		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Buku ajar/Teks dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('pengabdian/luaran/bukuajars/');
	}


    public function resubmit(){
        $id = $this->uri->segment(5);
        $item = Buku_ajar::find($id);
        if(!$this->isPengguna($item->dosen)) return;
        $item->isvalidate = 0;
        $item->save();
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function validate(){
        if(!$this->isOperator()) return;
		$id = $this->uri->segment(5);
		$item = Buku_ajar::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
        $item->save();

        $first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
        if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Buku Ajar/Teks anda dengan judul \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
            send_notif_luaran($item->dosen, "Data Buku Ajar/Teks anda dengan judul \"" . $first_four_title . "...\" telah divalidasi Operator");
        }

        $point = Point::active(6)->get()->toArray();
        if(!empty($point)) {
            $array = [];
            $array[] = array("point" => $point[0]['ketua'], "created_date" => date('Y-m-d H:i:s'), "type" => 1, "status" => 6, "user_id" => $item->dosen,'jenis'=>2,'jenis_id'=>$item->id);
            Point_log::insert($array);
        }
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Buku Ajar/Teks dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();

		$user = $this->is_login();
		$buku_ajar = Buku_ajar::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$jabatan = "Ketua LPPM UHAMKA";
		$nama = "Prof. Dr. Hj. Nani Solihati, M. Pd.";
		if(!roles(["Operator LPPM","Sekretaris LPPM","Ketua LPPM","Rektor","Wakil Rektor"])){
			$buku_ajar->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}

		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$buku_ajar->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}

		$buku_ajar = $buku_ajar->with('dosen')->with('dosen.program_studi')->with('dosen.fakultas')->with('tahun_kegiatan')->get();

		$this->load->view('pengabdian/luaran/buku_ajar/pdf', array("items"=>$buku_ajar->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_BukuAjar_'.Date('dmY').'.pdf', 'I');
	}

}
