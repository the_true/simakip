<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Luaranlains extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("lp"=>"active","lp5"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
        $this->load->model('Fakultas');
		$this->load->model('luaran_pengabdian/Luaran_lain');
		$this->load->model('Jenis_luaran');
		$this->load->model('Tahun_kegiatan');
        $this->load->model('Point');
        $this->load->model('Point_log');
    }
	public function index()
	{
		$default_view = 10;
		$page = 0;

		$judul_get = $this->input->get("judul");
		$tahun_get = $this->input->get("tahun");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");

		$user = $this->is_login();
		$luaran_lain = Luaran_lain::where('isdelete', '=', '0');
		if(isset($page_get)) $page = $page_get-1;
		if(isset($judul_get)) $luaran_lain->where('judul','LIKE','%'.$judul_get.'%');
		if(isset($tahun_get) && $tahun_get!='') $luaran_lain->where('tahun_kegiatan','=',$tahun_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $luaran_lain->where('isvalidate','=',(int)$isvalidate-1);

		if(!roles(["Operator LPPM","Sekretaris LPPM","Ketua LPPM","Rektor","Wakil Rektor"])){
			$luaran_lain->where('dosen', '=', $user["id"]);
		}
		$default_skip = $page*$default_view;
		$count  = $luaran_lain->count();
		$luaran_lain = $luaran_lain->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();

		$luaran_lain->load('dosen');
		$luaran_lain->load('jenis_luaran')->load('tahun_kegiatan');
		$data = array("items"=>$luaran_lain->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('pengabdian/luaran/luaran_lain/index', array_merge($data,$this->menu));
	}

	private function validation($edit=FALSE){
        $this->form_validation->set_rules('judul', 'Judul Luaran', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('jenis', 'Jenis Luaran', 'required',
                array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('deskripsi', 'Deskripsi Singkat', 'required');
        $this->form_validation->set_rules('pdf', 'Berkas', 'required');
		// if (!$edit){
		// 	if (empty($_FILES['file']['name']))
		// 	{
		// 	    $this->form_validation->set_rules('file', 'Berkas', 'required');
		// 	}
		// }
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
        	$jenis = Jenis_luaran::where("isdelete","=","0")->get();
        	$data = array("items_luaran"=>$jenis->toArray());
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data["items_tahun"] = $tahun->toArray();
            $this->twig->display('pengabdian/luaran/luaran_lain/add', array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect('pengabdian/luaran/luaranlains/');
        	else echo $this->upload->display_errors();
		}
	}

	public function upload(){
		$this->generate_folder('uploads/luaranlains');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/luaranlains'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}


	private function submit($id=0){
		$is_upload = TRUE;
		// if (!empty($_FILES['file']['name']))
		// {
		// 	    $is_upload = TRUE;
		// }
		// $this->generate_folder('uploads/luaranlains');
		// $filename = $this->gen_uuid();
		// $upload_config = [
		// 	'file_name' => $filename,
		// 	'upload_path' => realpath(APPPATH . '../uploads/luaranlains'),
		// 	'allowed_types' => 'pdf',
		// 	'max_size' => 2000,
		// 	'file_ext_tolower' => TRUE
		// ];
		// $this->load->library('upload', $upload_config);
		$judul = $this->input->post('judul');
		$jenis = $this->input->post('jenis');
		$deskripsi = $this->input->post('deskripsi');
		$dosen = $this->input->post('dosen');
		$tahun = $this->input->post('tahun');
		$berkas = $this->input->post('pdf');
		$file_field = "file";
		$uploaded = TRUE;
		// if($is_upload){
		// 	$uploaded = $this->upload->do_upload($file_field);
		// }
		if ($uploaded){
			if($id==0){
				$luaran_lain = new Luaran_lain;
			}else{
				$luaran_lain = Luaran_lain::find($id);
			}
			if($luaran_lain->isvalidate!=1){
				$luaran_lain->isvalidate = 3;
			}
			$luaran_lain->judul = $judul;
			$luaran_lain->dosen = $dosen;
			$luaran_lain->jenis = $jenis;
			$luaran_lain->deskripsi = $deskripsi;
			$luaran_lain->tahun_kegiatan = $tahun;
			$luaran_lain->identifier = "pengabdian";

			if($is_upload) $luaran_lain->berkas = $berkas;
			$luaran_lain->save();
			$first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
			if($id==0){
				
				send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Luaran lain baru dengan judul \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Luaran Lain baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			}else{
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Luaran Lain dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
				$this->session->set_flashdata('success', 'Luaran dengan nama '.$luaran_lain->judul.' berhasil diubah!');
			}
			return TRUE;
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	public function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function show(){
		$id = $this->uri->segment(5);
		$hki = Luaran_lain::whereId($id)->with('dosen')->with('dosen.program_studi')->with('tahun_kegiatan')->get();
		$hki->load('jenis_luaran');
		$data = array("item"=>$hki->toArray()[0]);
		$this->twig->display('pengabdian/luaran/luaran_lain/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function edit(){
		$this->is_login();
		$id = $this->uri->segment(5);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);


        if ($this->form_validation->run() == FALSE){
					$jurnal = Luaran_lain::whereId($id)->with("dosen")->with("dosen.program_studi")->get();
        	$jenis = Jenis_luaran::where("isdelete","=","0")->get();
			if(($jurnal[0]->isvalidate==1  && !$this->isPengguna($jurnal[0]->dosen)) || !$this->isOperator() && !$this->isPengguna($jurnal[0]->dosen)  ){ $this->twig->display('404'); return; }
        	$data = array("items_luaran"=>$jenis->toArray());
    			$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data["items_tahun"] = $tahun->toArray();
					$this->twig->display('pengabdian/luaran/luaran_lain/edit', array_merge($jurnal->toArray()[0],$data,$this->menu));
        }else{
        	if ($this->submit($id))
        		redirect('pengabdian/luaran/luaranlains/');
        	else echo $this->upload->display_errors();
		}

	}

	public function delete(){
		$this->is_login();
		$id = $this->uri->segment(5);
		$item = Luaran_lain::find($id);
		if(!$this->isPengguna($item->dosen)) return;
		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Luaran Lain dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('pengabdian/luaran/luaranlains/');
	}

	public function validate(){
  if(!$this->isOperator()) return;
		$id = $this->uri->segment(5);
		$item = Luaran_lain::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
		$item->save();

		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
        if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Luaran Lain anda dengan judul \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
            send_notif_luaran($item->dosen, "Data Luaran Lain anda dengan judul \"" . $first_four_title . "...\" telah divalidasi Operator");
        }

        $point = Point::active(8)->get()->toArray();
        if(!empty($point)) {
            $array = [];
            $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => 8, "user_id" => $item->dosen, "created_date" => date('Y-m-d H:i:s'),'jenis'=>5,'jenis_id'=>$item->id);
            Point_log::insert($array);
        }
        $first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Luaran Lain dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function resubmit(){
        $id = $this->uri->segment(5);
        $item = Luaran_lain::find($id);
        if(!$this->isPengguna($item->dosen)) return;
        $item->isvalidate = 0;
        $item->save();
        redirect($_SERVER['HTTP_REFERER']);
    }

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$luaran_lain = Luaran_lain::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$jabatan = "Ketua LPPM UHAMKA";
		$nama = "Prof. Dr. Hj. Nani Solihati, M. Pd.";
		if(!roles(["Operator LPPM","Sekretaris LPPM","Ketua LPPM","Rektor","Wakil Rektor"])){
			$luaran_lain->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}

		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$luaran_lain->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}

		$luaran_lain = $luaran_lain->with("dosen")->with("dosen.fakultas")->with("dosen.program_studi")->with('tahun_kegiatan')->get();
		$luaran_lain->load('jenis_luaran');
		$this->load->view('pengabdian/luaran/luaran_lain/pdf',array("items"=>$luaran_lain->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_LuaranLain_'.Date('dmY').'.pdf', 'I');
	}
}
