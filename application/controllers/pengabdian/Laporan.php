<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Laporan extends MY_BaseController
{

	/**
	 * @category Libraries
	 * @package  CodeIgniter 3.0
	 * @author   Yp <purwantoyudi42@gmail.com>
	 * @link     https://timexstudio.com
	 * @license  Protected
	 */
	private $menu = array("mpe" => "active", "mpe3" => "active");
	public function __construct()
	{
		$config = [
			"functions" => ["anchor", "set_value", "set_select"],
			"functions_safe" => ["form_open", "form_open_multipart", "validation_errors_array"],
		];
		parent::__construct($config);
		$this->load->model("Tahun_kegiatan");
		$this->load->model("Jenis_pengabdian");
		$this->load->model("Pengabdian");
		$this->load->model("Pengabdian_review");
		$this->load->model("Pengabdian_reviewer");
		$this->load->model("Pengabdian_laporan");
		$this->load->model("Batch_penelitian");
		$this->load->model("Batch");
		$this->load->model("Batch_lists");
		$this->load->model("Dosen");
		$this->load->model("Pengabdian_anggota");
		$this->load->model("Jabatan_akademik");
		$this->load->model("Jabatan_fungsi");
		$this->load->model("Program_studi");
		$this->load->model("Fakultas");
	}
	public function index()
	{
		$data = [];
		$jenis_pengabdian = Jenis_pengabdian::where("isdelete", "=", "0")->get();
		// $jenis_penelitian->load("batch_penelitian");
		$data["jenis_pengabdian"] = $jenis_pengabdian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete", "=", "0")->orderBy('tahun', 'desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$pengabdian = Pengabdian::where("isdelete", "=", "0")->where('status', '=', '4')
			->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen_menyetujui')
			->with('dosen_mengetahui')
			->with('pengabdian_reviewer')->whereHas('pengabdian_laporan', function ($q) {
			});
		$pengabdian = $this->filter($pengabdian, $data);
		if ($this->is_login()['akses']['nama'] == 'Dosen') {
			$pengabdian->where(function ($q) {
				$q->where('dosen', '=', $this->is_login()['id'])->orWhereHas('anggota', function ($q) {
					$q->where('anggota', '=', $this->is_login()['id']);
				});
			});
		}
		// die;
		$info = $this->create_paging($pengabdian);
		$pengabdian = $pengabdian->take($info["limit"])->skip($info["skip"])->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun')->load('pengabdian_laporan');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');

		$data["pengabdian"] = $pengabdian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete", "=", "0")->orderBy("nama", "asc")->get()->toArray();
		$data = array_merge($data, $info, $this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("pengabdian/laporan/index", $data);
	}

	public function filter($model, &$data)
	{
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_penelitian');
		if ($dosen) {
			$model->whereHas('dosen', function ($q) {
				$q->where("nama", "LIKE", "%" . $this->input->get('dosen') . "%");
			});
		}
		if ($judul) {
			$model->where('judul', 'LIKE', '%' . $judul . '%');
		}
		if ($jp) {
			$model->where('jenis_penelitian', '=', $jp);
		}
		$batch = $this->input->get('batch');
		if ($batch) {
			$model->whereHas('batch', function ($q) use ($batch) {
				$q->where('batch_lists', '=', $batch);
			});
		}

		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
		if ($tahun_kegiatan) {
			$model = $model->whereHas('batch', function ($q) use ($tahun_kegiatan) {
				$q->whereHas('batch_penelitian', function ($w) use ($tahun_kegiatan) {
					$w->where('tahun', '=', $tahun_kegiatan);
				});
			});
			// $item =$item->where('tahun_kegiatan','=',$tahun_kegiatan);
		}

		$status = $this->input->get('status');
		if ($status) {
			if ($status == "5") $status = "0";
			$model->whereHas('pengabdian_laporan', function ($q) use ($status) {
				$q->where('status', '=', $status);
			});
			// echo $model->get()->load('penelitian_laporan')->toJson(); die;
		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if ($to == "") $to = "DESC";
		if ($orderby) {
			$model->orderby($orderby, $to);
		} else {
			$model->orderby('id', $to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function add()
	{
		$data = [];
		// $data["user"] = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$pengabdian = Pengabdian::where("isdelete", "=", "0")->where('acc', '=', "1")->where("status", "=", "4")->where('dosen', '=', $this->is_login()["id"])->whereDoesntHave('pengabdian_laporan')->get(['id', 'judul']);
		$data["pengabdian"] = $pengabdian->toArray();
		$data = array_merge($data, $this->menu);
		$this->twig->display("pengabdian/laporan/add", $data);
	}

	public function edit()
	{
		$data = [];
		// $data["user"] = $this->is_login();
		$id = $this->uri->segment(4);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$laporan = Pengabdian_laporan::whereId($id)->with('pengabdian')
			->with('pengabdian.jenis_pengabdian')
			->with('pengabdian.batch')
			->with('pengabdian.batch.batch_penelitian.tahun')
			->get();
		// $laporan->load('penelitian.batch')->load('penelitian.batch.batch_penelitian.tahun');
		$data["laporan"] = $laporan->toArray()[0];

		$pengabdian = Pengabdian::where("isdelete", "=", "0")->where("status", "=", "4")->where("acc", "=", "1")
			->where(function ($q) {
				$q->whereDoesntHave('pengabdian_laporan')->orWhereHas('pengabdian_laporan', function ($q) {
					$q->where('id', '=', $this->uri->segment(4));
				});
			})->get(['id', 'judul']);
		$data["pengabdian"] = $pengabdian->toArray();
		// echo json_encode($data);
		// die;
		$data = array_merge($data, $this->menu);
		$this->twig->display("pengabdian/laporan/edit", $data);
	}

	public function upload_laporan()
	{
		$this->generate_folder('uploads/pengabdian_laporan');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/pengabdian_laporan'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "laporan_file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if ($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function upload_artikel()
	{
		$this->generate_folder('uploads/pengabdian_artikel');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/pengabdian_artikel'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "artikel_file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if ($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function update()
	{
		$id = $this->uri->segment(4);
		$laporan = Pengabdian_laporan::find($id);
		$pengabdian = $this->input->post('judul');
		$laporan->pengabdian = $pengabdian;
		$laporan->laporan = $this->input->post('pengabdian_laporan');
		$laporan->artikel = $this->input->post('pengabdian_artikel');
		$laporan->abstrak = $this->input->post('abstrak');
		if ($laporan->status == 2)	$laporan->status = 3;
		$laporan->save();
		celery()->PostTask('tasks.laporan_pengabdian_submit', array(base_url(), $laporan->id));

		$pengabdian = $laporan->pengabdian()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah Laporan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '3', 'created_at' => date('Y-m-d H:i:s')]);


		redirect('pengabdian/laporan');
	}

	public function insert()
	{
		$pengabdian = $this->input->post('judul');
		$laporan = new Pengabdian_laporan;
		$laporan->pengabdian = $pengabdian;
		$laporan->laporan = $this->input->post('pengabdian_laporan');
		$laporan->status = 0;
		$laporan->artikel = $this->input->post('pengabdian_artikel');
		$laporan->abstrak = $this->input->post('abstrak');
		$laporan->save();
		celery()->PostTask('tasks.laporan_pengabdian_submit', array(base_url(), $laporan->id));

		$pengabdian = $laporan->pengabdian()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'insert', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' membuat Laporan Pengabdian pada jenis penelitian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '3', 'created_at' => date('Y-m-d H:i:s')]);

		redirect('pengabdian/laporan');
	}

	public function jsonpengabdian()
	{
		$id = $this->uri->segment(4);
		$data = Pengabdian::with('dosen')->with('jenis_pengabdian')->with('batch')->with('batch.Batch_penelitian')->with('batch.Batch_penelitian.tahun')
			->find($id);
		// $data->load("dosen")->load("dosen.jabatan_akademik")->load("dosen.program_studi");
		echo $data->toJson();
	}

	private function generate_folder($folder_name)
	{
		if (!is_dir($folder_name)) {
			mkdir($folder_name, 0777, true);
		}
	}

	public function validasi_laporan()
	{
		$laporan = $this->input->post('laporan');
		$alasan = $this->input->post('alasan');
		$isvalid = $this->input->post('isvalid');
		$item = Pengabdian_laporan::find($laporan);
		$pengabdian = $item->pengabdian()->first();
		$pengusul = $pengabdian->dosen()->first()->toArray();
		$pengabdianx = $pengabdian;
		$pengabdian = $pengabdian->toArray();
		if ($isvalid == '0') {
			$item->alasan = $item->alasan != "" ? $item->alasan . "<br><br>" . $alasan : $alasan;
			$item->status = 2;
			$item->save();
			$data_email = [
				"id" => $pengusul["id"], "pengusul" => $pengusul["nama_lengkap"], "surel" => $pengusul["surel"],
				"judul" => $pengabdian["judul"], "alasan" => $item->alasan
			];
			celery()->PostTask('tasks.laporan_pengabdian_invalid', array(base_url(), $data_email));

			// $penelitian = $laporan->penelitian()->first();
			$jenis_pengabdian = $pengabdianx->jenis_pengabdian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $pengabdianx->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'validate', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menolak Laporan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '3', 'created_at' => date('Y-m-d H:i:s')]);
		} else {
			$item->status = 1;
			$item->save();
			$data_email = [
				"id" => $pengusul["id"], "pengusul" => $pengusul["nama_lengkap"], "surel" => $pengusul["surel"],
				"judul" => $pengabdian["judul"], "alasan" => $item->alasan
			];
			celery()->PostTask('tasks.laporan_pengabdian_valid', array(base_url(), $data_email));

			$item = $item->pengabdian()->first();
			// $this->load->model("Point_penelitian");
			// $this->load->model("Point_log");
			// $point = Point_penelitian::active(2)->get()->toArray();
			// if(!empty($point)) {
			//     $array = [];
			//     $array[] = array("point" => $point[0]['ketua'], "created_date" => date('Y-m-d H:i:s'), "type" => 1, "status" => 1, "user_id" => $item->dosen,'jenis'=>7,'jenis_id'=>$item->id);
			//     $anggotas = $item->anggota()->get();
			//     foreach($anggotas as $anggota){
			//         $array[] = array("point" => $point[0]['anggota'], "created_date" => date('Y-m-d H:i:s'), "type" => 2, "status" => 1, "user_id" => $anggota->anggota,'jenis'=>7,'jenis_id'=>$item->id);
			//     }
			// }
			// Point_log::insert($array);

			// $penelitian = $laporan->penelitian()->first();
			$jenis_pengabdian = $pengabdianx->jenis_pengabdian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $pengabdianx->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'validate', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menerima Laporan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '3', 'created_at' => date('Y-m-d H:i:s')]);
		}
		// $item->save();
		// echo $item->toJson(); die;
		redirect('pengabdian/laporan');
	}

	public function delete()
	{
		$id = $this->uri->segment(4);
		$item = Pengabdian_laporan::find($id);


		$pengabdian = $item->pengabdian()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'delete', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menghapus Laporan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '3', 'created_at' => date('Y-m-d H:i:s')]);

		$item->delete();
		redirect('pengabdian/laporan/');
	}

	private function dataExcel()
	{
		$pengabdian = Pengabdian::where("isdelete", "=", "0")->where('isvalid', '=', '1')->where('isdelete', '=', '0')
			->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen.fakultas')
			->with('dosen_menyetujui')
			->with('dosen_menyetujui.jabatan_akademik')
			->with('dosen_menyetujui.program_studi')
			->with('dosen_menyetujui.jabatan_fungsional')
			->with('dosen_mengetahui')
			->with('dosen_mengetahui.jabatan_akademik')
			->with('dosen_mengetahui.program_studi')
			->with('dosen_mengetahui.jabatan_fungsional')
			->with('Pengabdian_review')->with('Pengabdian_reviewer')->whereHas('pengabdian_laporan', function ($q) {
			});
		$pengabdian = $this->filter($pengabdian, $data);
		// $akses = $this->is_login()['akses']['nama'];
		if ($this->is_login()['akses']['nama'] == 'Dosen') {
			$pengabdian->where(function ($q) {
				$q->whereHas('pengabdian_reviewer', function ($q) {
					$q->where('dosen', '=', $this->is_login()['id']);
				});
			});
		}
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('pengabdian_reviewer.dosen');
		$pengabdian->load('tahun_kegiatan');
		$pengabdian->load('pengabdian_laporan');
		// echo $pengabdian->toJson();
		// die;
		return $pengabdian->toArray();
	}


	public function download()
	{
		$data = $this->dataExcel();

		$this->load->library('libexcel');

		$default_border = array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3')
		);
		$style_header = array(
			'borders' => array(
				'bottom' => $default_border,
				'left' => $default_border,
				'top' => $default_border,
				'right' => $default_border,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E1E0F7'),
			),
			'font' => array(
				'bold' => true,
				'size' => 16,
			)
		);
		$style_content = array(
			'borders' => array(
				'allborders' => $default_border,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'eeeeee'),
			),
			'font' => array(
				'size' => 12,
			)
		);
		$excel = PHPExcel_IOFactory::load(FCPATH . "assets/template/template_pengabdian_no_reviewer.xlsx");
		$excel->getProperties()->setCreator("Simakip");
		$excel->setActiveSheetIndex(0);
		$firststyle = 'B11';
		$laststyle = 'B11';
		for ($i = 0; $i < count($data); $i++) {
			$urut = $i + 11;
			$num = 'B' . $urut;
			$judul_penelitian = 'C' . $urut;
			$nama_peneliti = 'E' . $urut;
			$anggota = 'F' . $urut;
			$lokasi = 'G' . $urut;
			$anggaran = 'H' . $urut;
			$fakultas = 'I' . $urut;

			$anggota_string = "";
			for ($x = 0; $x < count($data[$i]["anggota"]); $x++) {
				$anggota_string .= "- " . $data[$i]["anggota"][$x]["dosen"]["nama_lengkap"] . "\n";
			}

			$excel->setActiveSheetIndex(0)
				->setCellValue($num, $i + 1)
				->setCellValue($judul_penelitian, $data[$i]['judul'])->mergeCells($judul_penelitian . ':D' . $urut)
				->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
				->setCellValue($anggota, $anggota_string)
				->setCellValue($lokasi, $data[$i]['jenis_pengabdian']["nama"])
				->setCellValue($anggaran, $data[$i]['rp_total_biaya'])
				->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"] . "/" . $data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['penelitian_reviewer'])

			$excel->getActiveSheet()->getRowDimension($i + 11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle = $fakultas;
		}
		$excel->getActiveSheet()->getStyle($firststyle . ':' . $laststyle)->applyFromArray($style_content); // give style to header
		for ($col = 'A'; $col !== 'N'; $col++) {
			$excel->getActiveSheet()
				->getColumnDimension($col)
				->setAutoSize(true);
		}
		$excel->getActiveSheet()
			->getStyle($firststyle . ':' . $laststyle)
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Laporan Pengabdian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $this->is_login()['nidn'] . '_LaporanPengabdian_' . Date('dmY') . '.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
