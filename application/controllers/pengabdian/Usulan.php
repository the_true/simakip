<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Usulan extends MY_BaseController
{

	/**
	 * @category Libraries
	 * @package  CodeIgniter 3.0
	 * @author   Yp <purwantoyudi42@gmail.com>
	 * @link     https://timexstudio.com
	 * @license  Protected
	 */
	private $menu = array("mpe" => "active", "mpe1" => "active");
	public function __construct()
	{
		$config = [
			"functions" => ["anchor", "set_value", "set_select"],
			"functions_safe" => ["form_open", "form_open_multipart"],
		];
		parent::__construct($config);
		$this->load->model("Tahun_kegiatan");
		$this->load->model("Jenis_pengabdian");
		$this->load->model("Batch_penelitian");
		$this->load->model("Batch");
		$this->load->model("Fakultas");
		$this->load->model("Dosen");
		$this->load->model("Jabatan_akademik");
		$this->load->model("Jabatan_fungsi");
		$this->load->model("Program_studi");
		$this->load->model("Pengabdian");
		$this->load->model("Pengabdian_anggota");
		$this->load->model("Penilaian");
		$this->load->model("Penilaian_kriteria");
		$this->load->model("Pengabdian_reviewer");
		$this->load->model("Pengabdian_review");
		$this->load->model("Pengabdian_laporan");
		$this->load->model("Pengesah_pengabdian");
		$this->load->model("Syarat_penelitian");
		$this->load->model("Target_luaran");
		$this->load->model("Batas_anggaran");
		$this->load->model("Keanggotaan");
		$this->load->model("Batch_lists");
		$this->load->model("iptek");
		$this->load->model("mitra");
	}
	public function index()
	{
		$this->twig->addGlobal('session', $this->session);
		$data = [];
		$jenis_pengabdian = Jenis_pengabdian::where("isdelete", "=", "0");
		if ($this->is_login()['akses']['nama'] == 'Dosen') {
			// $jenis_pengabdian = $jenis_pengabdian->whereHas('syarat_penelitian', function($q){
			// 	$q->where('syarat','=',$this->is_login()['tingkat'])->where('jabatan_akademik','=',$this->is_login()['jabatan_akademik']['id'])->where('isdelete','=','0');
			// });
		}
		$jenis_pengabdian = $jenis_pengabdian->get();
		// $jenis_pengabdian->load("batch_penelitian");

		$data["jenis_pengabdian"] = $jenis_pengabdian->toArray();

		// // $penelitian_tanpa_laporan = Pengabdian::where("isdelete","=","0")->where("isvalid","=","1")->where('status','<>','12')->where(function($q){
		// // 	$q->doesntHave('penelitian_laporan')
		// // 	->orWhereHas('penelitian_laporan',function($qa){
		// // 		$qa->where('status','<>','1');
		// // 	});
		// // })->where(function($q){
		// // 	$q->where('dosen','=',$this->is_login()['id']);
		// // })->count();

		// if($penelitian_tanpa_laporan>0){
		// 	foreach($data["jenis_pengabdian"] as &$value){
		// 		unset($value["active_batch"]);
		// 	}
		// }
		$data["active_pengabdian"] = Pengabdian::where("isdelete", "=", "0")->where("isvalid", "=", "1")->where("acc", "=", 0)->where(function ($q) {
			$q->where("status", "<>", "4")->where("status", "<>", "11")->where("status", "<>", "12");
		})->count();

		// echo $data["active_pengabdian"]; die;

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete", "=", "0")->orderBy('tahun', 'desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$pengabdian = Pengabdian::where("isdelete", "=", "0")->where('isvalid', '=', '1')->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen_menyetujui')
			->with('dosen_menyetujui.jabatan_akademik')
			->with('dosen_menyetujui.program_studi')
			->with('dosen_menyetujui.jabatan_fungsional')
			->with('dosen_mengetahui')
			->with('dosen_mengetahui.jabatan_akademik')
			->with('dosen_mengetahui.program_studi')
			->with('dosen_mengetahui.jabatan_fungsional')
			->with('anggota')->with('anggota.dosen')->with('Pengabdian_review')->with('Pengabdian_reviewer');
		$pengabdian = $this->filter($pengabdian, $data);
		if ($this->is_login()['akses']['nama'] == 'Dosen') {
			$pengabdian->where(function ($q) {
				$q->where('dosen', '=', $this->is_login()['id'])->orWhereHas('anggota', function ($q) {
					$q->where('anggota', '=', $this->is_login()['id']);
				});
			});
		}
		$info = $this->create_paging($pengabdian);
		$pengabdian = $pengabdian->take($info["limit"])->skip($info["skip"])->get();
		$pengabdian->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('tahun_kegiatan');
		$data["pengabdian"] = $pengabdian->toArray();
		$data["mitra"] = Mitra::where("isdelete", "=", "0")->get()->toArray();
		$data["iptek"] = Iptek::where("isdelete", "=", "0")->get()->toArray();
		$data["batch_lists"] = Batch_lists::pengabdian()->where("isdelete", "=", "0")->orderBy("nama", "asc")->get()->toArray();
		$data = array_merge($data, $info, $this->menu);
		$this->twig->display("pengabdian/usulan/index", $data);
	}

	public function detail()
	{
		$data = [];
		$jenis_pengabdian = $this->uri->segment(4);
		$batch = $this->uri->segment(6);
		$id = $this->uri->segment(4);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen_menyetujui')
			->with('dosen_menyetujui.jabatan_akademik')
			->with('dosen_menyetujui.program_studi')
			->with('dosen_menyetujui.jabatan_fungsional')
			->with('dosen_menyetujui.jabatan_pengesah_pengabdian')
			->with('dosen_mengetahui')
			->with('dosen_mengetahui.jabatan_akademik')
			->with('dosen_mengetahui.program_studi')
			->with('dosen_mengetahui.jabatan_fungsional')
			->with('anggota')->with('anggota.dosen')->with('anggota.dosen.program_studi')->with('anggota.dosen.jabatan_akademik')->find($id);
		$pengabdian->load('batch')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan');
		$data = $pengabdian->toArray();
		$data = array_merge($data, $this->menu);
		$this->twig->display("pengabdian/usulan/detail", $data);
	}

	public function filter($model, &$data)
	{
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_pengabdian');
		$status = $this->input->get('status');
		if ($dosen) {
			$model->where(function ($qt) {
				$qt->whereHas('dosen', function ($q) {
					$q->where("nama", "LIKE", "%" . $this->input->get('dosen') . "%");
				})->orWhereHas('anggota.dosen', function ($q) {
					$q->where("nama", "LIKE", "%" . $this->input->get('dosen') . "%");
				});
			});
		}

		if ($status) {
			$model->where('status', '=', $status);
		}

		$mitra = $this->input->get('mitra');
		if ($mitra) {
			$model->where('mitra', '=', $mitra);
		}

		$iptek = $this->input->get('iptek');
		if ($iptek) {
			$model->where('iptek', '=', $iptek);
		}

		if ($judul) {
			$model->where('judul', 'LIKE', '%' . $judul . '%');
		}
		if ($jp) {
			$model->where('jenis_pengabdian', '=', $jp);
		}

		$batch = $this->input->get('batch');
		if ($batch) {
			$model->whereHas('batch', function ($q) use ($batch) {
				$q->where('batch_lists', '=', $batch);
			});
		}

		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
		if ($tahun_kegiatan) {
			$model->where('tahun_kegiatan', '=', $tahun_kegiatan);
		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if ($to == "") $to = "DESC";
		if ($orderby) {
			$model->orderby($orderby, $to);
		} else {
			$model->orderby('id', $to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function add()
	{
		$data = [];
		$jenis_pengabdian = $this->uri->segment(5);
		$batch = $this->uri->segment(7);
		$data["jenis_pengabdian"] = $jenis_pengabdian;
		$jenpen_data = Jenis_pengabdian::find($jenis_pengabdian);
		$jenpen_data->load('keanggotaan');
		$data["nama"] = $jenpen_data->nama;
		$data['notes'] = $jenpen_data->keanggotaan->notes;
		$data["batch"] = $batch;

		$batch_penelitian = Batch::find($batch);
		$batch_penelitian->load('batch_lists')->load('batch_penelitian')->load('batch_penelitian.tahun');
		$data["batch_data"] = $batch_penelitian->toArray();

		$Batas_anggaran = Batas_anggaran::where("jenis_penelitian", "=", $jenis_pengabdian)->where("status", "=", "1")->where("isdelete", "=", "0")->first();
		$data["batas_anggaran"] = $Batas_anggaran->toArray();

		$iptek = iptek::where('isdelete', '=', '0')->get();
		$data["iptek"] = $iptek->toArray();

		$mitra = mitra::where('isdelete', '=', '0')->get();
		$data["mitra"] = $mitra->toArray();

		$curr_tahun = array((int) date("Y") - 1, date("Y"), (int) date("Y") + 1);
		$tahun = Tahun_kegiatan::whereIn('tahun', $curr_tahun)->where("isdelete", "=", "0")->orderBy('tahun', 'desc')->get();
		$data["tahun"] = $tahun->toArray();

		$pengesah = Pengesah_pengabdian::where('isdelete', '=', '0')->where("status", "=", "1")->first();
		$pengesah->load('dosen')->load('dosen.jabatan_akademik');
		$data["pengesah"] = $pengesah->toArray();

		$data["addmode"] = True;
		$target_luaran = Target_luaran::where("isdelete", "=", "0")->where("flag", "=", "wajib")->where("jenis_penelitian", "=", $jenis_pengabdian)->get();
		$target_luaran_tambahan = Target_luaran::where("isdelete", "=", "0")->where("flag", "=", "tambahan")->where("jenis_penelitian", "=", $jenis_pengabdian)->get();
		$data = array_merge($data, $this->menu, ["target_luaran" => $target_luaran->toArray(), "target_luaran_tambahan" => $target_luaran_tambahan->toArray()]);
		$this->twig->display("pengabdian/usulan/add", $data);
	}

	public function edit()
	{
		$data = [];
		// $jenis_pengabdian = $this->uri->segment(4);
		$batch = $this->uri->segment(6);
		// $data["jenis_pengabdian"] = $jenis_pengabdian;
		$data["batch"] = $batch;

		$curr_tahun = array((int) date("Y") - 1, date("Y"), (int) date("Y") + 1);
		$tahun = Tahun_kegiatan::whereIn('tahun', $curr_tahun)->where("isdelete", "=", "0")->orderBy('tahun', 'desc')->get();
		$data["tahun"] = $tahun->toArray();
		$tahun = Tahun_kegiatan::where("isdelete", "=", "0")->orderBy('tahun', 'desc')->get();

		$id = $this->uri->segment(4);
		$pengabdian = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen_menyetujui')
			->with('dosen_menyetujui.jabatan_akademik')
			->with('dosen_menyetujui.program_studi')
			->with('dosen_menyetujui.jabatan_fungsional')
			->with('dosen_mengetahui')
			->with('dosen_mengetahui.jabatan_akademik')
			->with('dosen_mengetahui.program_studi')
			->with('dosen_mengetahui.jabatan_fungsional')
			->find($id);
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian')->load('batch.batch_penelitian.tahun');
		$pengabdian->load('tahun_kegiatan');
		$target_luaran = Target_luaran::where("isdelete", "=", "0")->where("flag", "=", "wajib")->where("jenis_penelitian", "=", $pengabdian->jenis_pengabdian)->get();
		$target_luaran_tambahan = Target_luaran::where("isdelete", "=", "0")->where("flag", "=", "tambahan")->where("jenis_penelitian", "=", $pengabdian->jenis_pengabdian)->get();
		$data["pengabdian"] = $pengabdian->toArray();
		$data["target_luaran"] = $target_luaran->toArray();
		$data["target_luaran_tambahan"] = $target_luaran_tambahan->toArray();
		$pengesah = Pengesah_pengabdian::where('isdelete', '=', '0')->where("status", "=", "1")->first();
		$pengesah->load('dosen')->load('dosen.jabatan_akademik');
		$data["pengesah"] = $pengesah->toArray();

		$iptek = iptek::where('isdelete', '=', '0')->get();
		$data["iptek"] = $iptek->toArray();

		$mitra = mitra::where('isdelete', '=', '0')->get();
		$data["mitra"] = $mitra->toArray();

		$batas_anggaran = Batas_anggaran::where("jenis_penelitian", "=", $pengabdian->jenis_pengabdian)->where("status", "=", "1")->where("isdelete", "=", "0")->first();
		$data["batas_anggaran"] = $batas_anggaran->toArray();

		$data = array_merge($data, $this->menu);
		$this->twig->display("pengabdian/usulan/add", $data);
	}

	public function pilih_reviewer()
	{
		$data = [];
		$data["pengabdian"] = $this->uri->segment(4);
		$data["batch"] = Pengabdian::where("id", "=", $data["pengabdian"])->with("batch")->first()->toArray()["batch"];
		// echo $data["batch"]->toJson(); die;
		$dosen = Dosen::where("isdelete", "=", "0")->where(function ($q) {
			$q->where("isreviewer", "=", "2")->orWhere("isreviewer", "=", "3");
		})->get();
		$dosen = $dosen->toArray();
		$item = Pengabdian_reviewer::where("pengabdian", "=", $data['pengabdian'])->with('dosen')
			->with('dosen.program_studi')
			->with('dosen.jabatan_akademik')
			->get();
		$data["reviewer"] = $item->toArray();
		$data = array_merge($data, $this->menu, ["dosen" => $dosen], ["deadline" => $this->input->get('deadline')]);
		$this->twig->display("pengabdian/usulan/pilih_reviewer", $data);
	}

	public function add_reviewer()
	{
		$dosen = $this->input->post('dosen');
		$pengabdian = $this->input->post('pengabdian');
		$deadline = $this->input->post('deadline');
		$deadline = date('Y-m-d', strtotime($deadline));
		$item = new Pengabdian_reviewer;
		$item->dosen = $dosen;
		$item->pengabdian = $pengabdian;
		$item->deadline = $deadline;
		$item->save();

		$jenis_pengabdian = $item->pengabdian()->first()->jenis_pengabdian()->first();

		$item->load('dosen');
		$item->load('pengabdian.dosen');
		$item = $item->toArray();
		$data = [];

		$first_four_title = implode(' ', array_slice(explode(' ', $item["pengabdian"]["judul"]), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'insert', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' memilih ' . $item["dosen"]["nama_lengkap"] . ' sebagai reviewer Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);

		$data[] = [
			"id" => $item["dosen"]["id"], "reviewer" => $item["dosen"]["nama_lengkap"], "surel" => $item["dosen"]["surel"],
			"judul" => $item["pengabdian"]["judul"], "pengusul" => $item["pengabdian"]["dosen"]["nama_lengkap"],
			"deadline" => $item["deadline"]
		];
		// echo json_encode($data) ; die;
		celery()->PostTask('tasks.pengabdian_reviewers', array(base_url(), $data));

		redirect('pengabdian/usulan/pilih_reviewer/' . $item["pengabdian"]["id"] . '?deadline=' . $this->input->post('deadline'));
	}

	public function konfirm_reviewer()
	{
		$pengabdian = $this->input->get('pengabdian');
		$item = Pengabdian_reviewer::where('pengabdian', '=', $pengabdian)->get();
		$item->load('dosen');
		$item->load('pengabdian.dosen');
		// echo $item->toJson();die;
		$items = $item->toArray();
		$data = [];
		// foreach($items as $item){
		// 	$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
		// 					"judul"=>$item["pengabdian"]["judul"],"pengusul"=>$item["pengabdian"]["dosen"]["nama_lengkap"],
		// 					"deadline"=>$item["deadline"]];
		// }
		// celery()->PostTask('tasks.penelitian_reviewers',array(base_url(),$data));
		redirect('pengabdian/usulan/');
	}

	public function edit_reviewer()
	{
		$id = $this->uri->segment(4);
		$item = Pengabdian_reviewer::find($id);

		$pengabdian = $item->pengabdian()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$dosen = $item->dosen()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah tanggal deadline reviewer ' . $dosen->nama . ' dari tanggal ' . $item->deadline . ' menjadi ' . $this->input->post('deadline') . ' pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);

		$item->deadline = $this->input->post('deadline');
		$item->save();


		redirect('pengabdian/usulan/pilih_reviewer/' . $item->pengabdian . '?deadline=' . $this->input->post('deadline'));
	}

	public function verify_reviewer()
	{
		$pengabdian = $this->input->get('pengabdian');
		$item = Pengabdian_reviewer::where('pengabdian', '=', $pengabdian)->count();
		if ($item == 2) {
			$this->output->set_header('HTTP/1.0 500 Reviewer sudah penuh');
			return;
		}

		$item = Pengabdian_reviewer::where('pengabdian', '=', $pengabdian)
			->whereHas('dosen', function ($q) {
				$q->where('nidn', '=', $this->input->get('nidn'));
			})->count();
		if ($item > 0) {
			$this->output->set_header('HTTP/1.0 500 Reviewer sudah ada');
			return;
		}

		echo "berhasil";
	}

	public function delete_reviewer()
	{
		$id = $this->uri->segment(4);
		$item = Pengabdian_reviewer::find($id);
		$pengabdian = $item->pengabdian;

		$pengabdian = $item->pengabdian()->first();
		$jenis_pengabdian = $pengabdian->jenis_pengabdian()->first();
		$dosen = $item->dosen()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'delete', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menghapus ' . $dosen->nama . ' sebagai reviewer Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		$item->delete();
		redirect('pengabdian/usulan/pilih_reviewer/' . $pengabdian->id);
	}

	public function delete()
	{
		$id = $this->uri->segment(4);
		$item = Pengabdian::find($id);
		$item->isdelete = 1;
		$this->session->set_flashdata('success', 'Delete proposal pengabdian ' . $item->judul . ' berhasil!');
		$item->save();
		$jenis_pengabdian = $item->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'delete', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menghapus Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function tolak()
	{
		$id = $this->uri->segment(4);
		$item = Pengabdian::find($id);
		$item->status = 12;
		$this->session->set_flashdata('success', 'Tolak proposal pengabdian ' . $item->judul . ' berhasil!');
		$item->save();
		$jenis_pengabdian = $item->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' Menolak Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function validasi()
	{
		$pengabdian = $this->input->post('pengabdian');
		$alasan = $this->input->post('alasan');
		$isvalid = $this->input->post('isvalid');
		$item = Pengabdian::find($pengabdian);
		$pengusul = $item->dosen()->first()->toArray();
		if ($isvalid == '0') {
			$item->alasan = $item->alasan != "" ? $item->alasan . " <br><br> " . $alasan : $alasan;
			$item->status = 10;
			$jenis_pengabdian =	$item->jenis_pengabdian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'validate', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menolak Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);

			$data_email = [
				"id" => $pengusul["id"], "pengusul" => $pengusul["nama_lengkap"], "surel" => $pengusul["surel"],
				"judul" => $item->judul, "alasan" => $item->alasan
			];
			celery()->PostTask('tasks.pengabdian_invalid', array(base_url(), $data_email));
		} else {
			$item->status = 3;
			$data_email = [
				"id" => $pengusul["id"], "pengusul" => $pengusul["nama_lengkap"], "surel" => $pengusul["surel"],
				"judul" => $item->judul
			];

			$jenis_pengabdian =	$item->jenis_pengabdian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'validate', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menerima Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);

			celery()->PostTask('tasks.pengabdian_valid', array(base_url(), $data_email));
		}
		$item->save();
		$this->session->set_flashdata('success', 'Validasi proposal pengabdian ' . $item->judul . ' berhasil!');
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function validasi_perbaikan()
	{
		$pengabdian = $this->input->post('pengabdian');
		$alasan = $this->input->post('alasan');
		$isvalid = $this->input->post('isvalid');
		$item = Pengabdian::find($pengabdian);
		$pengusul = $item->dosen()->first()->toArray();
		if ($isvalid == '0') {
			$item->alasan = $alasan;
			$item->status = 9;
			$data_email = [
				"id" => $pengusul["id"], "pengusul" => $pengusul["nama_lengkap"], "surel" => $pengusul["surel"],
				"judul" => $item->judul, "alasan" => $item->alasan
			];
			celery()->PostTask('tasks.penelitian_syarat_invalid', array(base_url(), $data_email));

			$jenis_pengabdian =	$item->jenis_pengabdian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'validate', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menolak perbaikan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		} else {
			$item->status = 4;
			$data_email = [
				"id" => $pengusul["id"], "pengusul" => $pengusul["nama_lengkap"], "surel" => $pengusul["surel"],
				"judul" => $item->judul
			];
			celery()->PostTask('tasks.penelitian_syarat_valid', array(base_url(), $data_email));

			$jenis_pengabdian =	$item->jenis_pengabdian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'validate', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menerima perbaikan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		}
		$item->save();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function submit()
	{
		$id = $this->uri->segment(4);
		$item = Pengabdian::find($id);
		$item->status = 2;
		$item->submited_at = date('Y-m-d H:i:s');
		$item->save();

		$jenis_pengabdian =	$item->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'submit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' mensubmit Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);

		$this->session->set_flashdata('success', 'Submit proposal pengabdian ' . $item->judul . ' berhasil!');
		celery()->PostTask('tasks.pengabdian_submit', array(base_url(), $id));
		redirect('pengabdian/usulan/');
	}

	public function perbaikan()
	{
		$id = $this->input->post("pengabdian");
		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}
		if (empty($_FILES['file']['name'])) {
			return;
		}
		$this->generate_folder('uploads/pengabdian');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/pengabdian'),
			'allowed_types' => 'pdf',
			'max_size' => 5000,
			'file_ext_tolower' => TRUE
		];
		$file_field = "file";
		$this->load->library('upload', $upload_config);
		$uploaded = $this->upload->do_upload($file_field);
		if ($uploaded) {
			$data = Pengabdian::find($id);
			$data->berkas_perbaikan = $this->upload->data('file_name');
			$data->status = 7;
			$data->save();
			$this->session->set_flashdata('success', 'Perbaikan proposal pengabdian ' . $data->judul . ' berhasil!');

			$jenis_pengabdian = $data->jenis_pengabdian()->first();
			$pengusul = $data->dosen()->first()->toArray();
			$anggota =  $data->anggota()->with('dosen')->get()->toArray();
			$batch = $data->batch()->first()->toArray();
			$batch_tahun = $data->batch()->first()->batch_penelitian()->first()->tahun()->first()->toArray();

			send_email_penelitian_diperbaiki(["surel" => $pengusul["surel"], "pengusul" => $pengusul["nama_lengkap"], "judul" => $data->judul, "jenis_pengabdian" => $jenis_pengabdian->nama, "nama_batch" => $batch->nama, "tahun_batch" => $batch_tahun->tahun]);

			$first_four_title = implode(' ', array_slice(explode(' ', $data->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'submit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' mengupload perbaikan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function konfirmasi()
	{
		$id = $this->uri->segment(4);
		$konfirmasi = $this->uri->segment(5);
		$item = Pengabdian_anggota::find($id);
		$pengabdian = $item->pengabdian()->first()->toArray();
		if ($konfirmasi == 1) {
			$item->konfirmasi = 1;
			$item->save();
			$message = "menerima";
		} else if ($konfirmasi == 0) {
			$item->konfirmasi = 2;
			$item->save();
			$message = "menolak";
		}

		$jenis_pengabdian =	$item->pengabdian()->first()->jenis_pengabdian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $pengabdian['judul']), 0, 4));
		Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'confirm', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' ' . $message . ' keanggotaan Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);

		send_notif_pengabdian($pengabdian["dosen"], "Dosen " . $this->is_login()["nama_lengkap"] . " telah " . $message . " keanggotaan pengabdian yang berjudul " . $pengabdian["judul"]);
		// $pengabdian = Pengabdian::find($item->pengabdian);
		// $pengabdian->status=
		$this->session->set_flashdata('success', 'Konfirmasi proposal pengabdian ' . $item->judul . ' berhasil!');
		redirect('pengabdian/usulan/');
	}

	public function step1()
	{
		$id = $this->input->post("pengabdian");
		$batch = $this->input->post("batch");
		$jenis_pengabdian = $this->input->post("jenis_pengabdian");
		$judul = $this->input->post("judul");
		$abstrak = $this->input->post("abstrak");
		$keyword = $this->input->post("keyword");
		$iptek = $this->input->post("iptek");
		$mitra = $this->input->post("mitra");
		$lokasi = $this->input->post("lokasi");

		$data = new Pengabdian;
		if ($id != "") {
			$data = Pengabdian::find($id);
		}
		$data->batch = $batch;
		$data->jenis_pengabdian = $jenis_pengabdian;
		$data->judul = $judul;
		$data->keyword = $keyword;
		$data->abstrak = $abstrak;
		$data->iptek = $iptek;
		$data->mitra = $mitra;
		$data->lokasi = $lokasi;
		$data->dosen = $this->is_login()["id"];
		$data->save();

		echo $data->toJson();
	}

	public function step2()
	{
		$id = $this->input->post("pengabdian");
		$tahun = $this->input->post("tahun");
		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}
		$data = Pengabdian::find($id);
		$data->tahun_kegiatan = $tahun;
		$data->save();
		echo $data->toJson();
	}

	public function step3()
	{
		$pengabdian = $this->input->post("pengabdian");
		$anggota = $this->input->post("anggota");
		$peran = $this->input->post("peran");

		if (empty($pengabdian) || empty($anggota) || empty($peran)) {
			$this->output->set_header('HTTP/1.0 403 Error');
			return;
		}

		$data = new Pengabdian_anggota;
		$data->pengabdian = $pengabdian;
		$data->anggota = $anggota;
		$data->peran = $peran;
		$data->save();

		echo $data->toJson();
	}

	public function step3_2()
	{
		$id = $this->input->post("pengabdian");
		$anggota_mahasiswa = $this->input->post("anggota_mahasiswa_nama[]");
		$anggota_alumni = $this->input->post("anggota_alumni_nama[]");
		$anggota_staff = $this->input->post("anggota_staff_nama[]");

		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}

		$data = Pengabdian::find($id);
		$data->anggota_mahasiswa = $anggota_mahasiswa;
		$data->anggota_alumni = $anggota_alumni;
		$data->anggota_staff = $anggota_staff;
		$data->save();
		echo $data->toJson();
	}

	public function step4()
	{
		$id = $this->input->post("pengabdian");
		$target_post = $this->input->post("target[]");
		$target_jenis = $this->input->post("jenis[]");
		$satuan = $this->input->post("satuan");
		$lama = (int) $this->input->post("lama");
		$targets = [];
		foreach ($target_post as $key => $value) {
			$targets[] = array("nama" => $value, "type" => $target_jenis[$key]);
		}
		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}

		$data = Pengabdian::find($id);
		$data->lama = $lama;
		$data->target = $targets;
		$data->save();



		echo $data->toJson();
	}

	public function checkanggota()
	{
		$jenis_pengabdian = $this->input->get('jenis_pengabdian');
		$pengabdian = $this->input->get('pengabdian');
		$nidn = $this->input->get('nidn');

		if ($nidn == $this->is_login()['nidn']) {
			$this->output->set_header('HTTP/1.0 477 Tidak dapat menambahkan diri sendiri sebagai anggota');
			return;
		}

		$anggota = Pengabdian_anggota::where('pengabdian', '=', $pengabdian)->whereHas('dosen', function ($q) {
			$q->where('nidn', '=', $this->input->get('nidn'));
		})->count();
		if ($anggota > 0) {
			$this->output->set_header('HTTP/1.0 477 Anggota sudah ada');
			return;
		}

		$anggota = Pengabdian_anggota::where('pengabdian', '=', $pengabdian)->count();
		$this->load->model('keanggotaan');
		$max_anggota = Keanggotaan::where('jenis_penelitian', '=', $jenis_pengabdian)->first();
		if ($max_anggota->anggota == $anggota) {
			$this->output->set_header('HTTP/1.0 477 Anggota sudah penuh');
			return;
		}

		$anggota = Pengabdian_anggota::whereHas('dosen', function ($q) {
			$q->where('nidn', '=', $this->input->get('nidn'));
		})->whereHas('pengabdian', function ($q) {
			$q->where("isdelete", "=", "0")->where("isvalid", "=", "1")->where("status", "<>", "12")->where("status", "<>", "11")->where("status", "<>", "4");
		})->count();
		if ($anggota > 1) {
			$this->output->set_header('HTTP/1.0 477 Dosen sudah menjadi anggota di pengabdian lain');
			return;
		}


		$anggota = Pengabdian::whereHas('dosen', function ($q) {
			$q->where('nidn', '=', $this->input->get('nidn'));
		})->where("isdelete", "=", "0")->where("isvalid", "=", "1")->where("status", "<>", "12")->where("acc", "=", 0)->where("jenis_pengabdian", "=", $jenis_pengabdian)->count();
		if ($anggota > 1) {
			$this->output->set_header('HTTP/1.0 477 Dosen mempunyai usulan pengabdian dengan jenis pengabdian yg sama');
		}

		echo 'berhasil';
	}

	public function step4_anggota()
	{
		$pengabdian = $this->uri->segment(4);
		$data = Pengabdian_anggota::where("pengabdian", "=", $pengabdian)->with("dosen")->with("dosen.jabatan_akademik")->with("dosen.program_studi")->get();
		echo $data->toJson();
	}

	public function step4_hapus()
	{
		$pengabdian = $this->uri->segment(4);
		$data = Pengabdian_anggota::find($pengabdian);
		$data->delete();
	}

	public function step5()
	{
		$id = $this->input->post("pengabdian");
		$total_biaya = $this->input->post("total_biaya");
		$mandiri_total_biaya = $this->input->post("mandiri_total_biaya");
		$mandiri_biaya_instansi = $this->input->post("mandiri_biaya_instansi");
		$dosen_mengetahui = $this->input->post("dosen_mengetahui");
		$dosen_menyetujui = $this->input->post("dosen_menyetujui");
		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}
		$data = Pengabdian::find($id);
		$data->total_biaya = $total_biaya;
		$data->mandiri_total_biaya = $mandiri_total_biaya;
		$data->mandiri_biaya_instansi = $mandiri_biaya_instansi;
		$data->dosen_mengetahui = $dosen_mengetahui;
		$data->dosen_menyetujui = $dosen_menyetujui;
		$data->save();
		echo $data->toJson();
	}

	public function step5_check_anggaran()
	{
		$jenis_pengabdian = $this->input->get("jenis_pengabdian");
		$total_biaya = $this->input->get("total_biaya");
		$rekomendasi = $this->input->get("rekomendasi");
		$this->load->model("Batas_anggaran");
		$data = Batas_anggaran::where("jenis_penelitian", "=", $jenis_pengabdian)->where("status", "=", "1")->where("isdelete", "=", "0")->first();
		$batas = (float) $data->batas;
		$total_biaya = (float) $total_biaya;
		$rekomendasi = (float) $rekomendasi;
		if ($total_biaya > $batas || $rekomendasi > $batas) {
			$this->output->set_header('HTTP/1.0 400 Melebihi Anggaran');
			return;
		}
		$this->output->set_header('HTTP/1.0 200 OK');
		echo "Aprove";
	}

	public function step6()
	{
		$id = $this->input->post("pengabdian");
		$berkas_proposal = $this->input->post("berkas_proposal_res");
		$surat_mitra = $this->input->post("surat_mitra_res");
		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}

		$data = Pengabdian::find($id);
		$data->berkas = $berkas_proposal;
		$data->berkas_surat = $surat_mitra;
		$data->save();
		echo $data->toJson();
	}

	public function upload_step6()
	{
		$this->generate_folder('uploads/pengabdian');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/pengabdian'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if ($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	private function generate_folder($folder_name)
	{
		if (!is_dir($folder_name)) {
			mkdir($folder_name, 0777, true);
		}
	}

	public function step7()
	{
		$id = $this->input->post("pengabdian");
		$abstrak = $this->input->post("abstrak");
		if ($id == "") {
			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
			return;
		}
		$data = Pengabdian::find($id);
		$jenis_pengabdian = $data->jenis_pengabdian()->first();
		if ($data->isvalid == 0) {
			$this->session->set_flashdata('success', 'Proposal berhasil dibuat!');
			$first_four_title = implode(' ', array_slice(explode(' ', $data->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'insert', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' membuat Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		} else {
			$this->session->set_flashdata('success', 'Proposal berhasil diedit!');
			$first_four_title = implode(' ', array_slice(explode(' ', $data->judul), 0, 4));
			Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah Pengabdian pada jenis pengabdian ' . $jenis_pengabdian->nama . ' dengan nama judul/kegiatan ' . $first_four_title, 'type' => '8', 'created_at' => date('Y-m-d H:i:s')]);
		}



		// echo $data->jenis_pengabdian()->get()->toJson(); die;
		// $jenis_pengabdian = $data->jenis_pengabdian()->first();
		$pengusul = $data->dosen()->first()->toArray();
		$anggota =  $data->anggota()->with('dosen')->get()->toArray();
		$batch = $data->batch()->first()->toArray();
		$batch_tahun = $data->batch()->first()->batch_penelitian()->first()->tahun()->first()->toArray();
		$email = [];
		$data->isvalid = 1;
		$data->save();
		foreach ($anggota as $ang) {
			send_notif_pengabdian($ang["anggota"], "bapak/ibu terpilih menjadi anggota pada pengabdian " . $data->judul . ", silakan konfirmasi");
			$email[] = ["anggota" => $ang["dosen"]["nama_lengkap"], "surel" => $ang["dosen"]["surel"], "pengusul" => $pengusul["nama_lengkap"], "judul" => $data->judul, "nama_batch" => $batch["nama"], "tahun_batch" => $batch_tahun["tahun"]];
		}
		// send_email_penelitian_anggota($email);
		celery()->PostTask('tasks.send_mail_anggota_pengabdian', array(base_url(), $email));
		// send_email_penelitian_dibuat(["surel"=>$pengusul["surel"],"pengusul"=>$pengusul["nama_lengkap"],"judul"=>$data->judul,"jenis_pengabdian"=>$jenis_pengabdian->nama,"nama_batch"=>$batch->nama,"tahun_batch"=>$batch_tahun->tahun]);
		redirect('pengabdian/usulan/');
	}

	public function json()
	{
		$id = $this->uri->segment(4);
		$data = Pengabdian::with('dosen')->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen_menyetujui')
			->with('dosen_menyetujui.jabatan_akademik')
			->with('dosen_menyetujui.program_studi')
			->with('dosen_menyetujui.jabatan_fungsional')
			->with('dosen_menyetujui.jabatan_pengesah_pengabdian')
			->with('dosen_mengetahui')
			->with('dosen_mengetahui.jabatan_akademik')
			->with('dosen_mengetahui.program_studi')
			->with('dosen_mengetahui.jabatan_fungsional')
			->with('anggota')->with('anggota.dosen')->with('anggota.dosen.jabatan_akademik')->with('anggota.dosen.program_studi')
			->find($id);
		$data->load('batch');
		$data->load('batch.batch_penelitian')->load('batch.batch_penelitian.tahun');
		$data->load('tahun_kegiatan');
		$data->load('iptek');
		$data->load('mitra');
		// $data->load("dosen")->load("dosen.jabatan_akademik")->load("dosen.program_studi");
		echo $data->toJson();
	}

	private function dataExcel()
	{
		$pengabdian = Pengabdian::where("isdelete", "=", "0")->where('isvalid', '=', '1')->where('isdelete', '=', '0')
			->with('dosen.jabatan_akademik')
			->with('dosen.jabatan_fungsional')
			->with('dosen.program_studi')
			->with('dosen.fakultas')
			->with('dosen_menyetujui')
			->with('dosen_menyetujui.jabatan_akademik')
			->with('dosen_menyetujui.program_studi')
			->with('dosen_menyetujui.jabatan_fungsional')
			->with('dosen_mengetahui')
			->with('dosen_mengetahui.jabatan_akademik')
			->with('dosen_mengetahui.program_studi')
			->with('dosen_mengetahui.jabatan_fungsional')
			->with('Pengabdian_review')->with('Pengabdian_reviewer');
		$pengabdian = $this->filter($pengabdian, $data);
		// $akses = $this->is_login()['akses']['nama'];
		if ($this->is_login()['akses']['nama'] == 'Dosen') {
			$pengabdian->where(function ($q) {
				$q->whereHas('pengabdian_reviewer', function ($q) {
					$q->where('dosen', '=', $this->is_login()['id']);
				});
			});
		}
		$pengabdian = $pengabdian->get();
		$pengabdian->load('anggota')->load('anggota.dosen')->load('batch');
		$pengabdian->load('jenis_pengabdian');
		$pengabdian->load('pengabdian_reviewer.dosen');
		$pengabdian->load('tahun_kegiatan');
		// 	echo $pengabdian->toJson();
		// die;
		return $pengabdian->toArray();
	}

	public function download()
	{
		$data = $this->dataExcel();

		$this->load->library('libexcel');

		$default_border = array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3')
		);
		$style_header = array(
			'borders' => array(
				'bottom' => $default_border,
				'left' => $default_border,
				'top' => $default_border,
				'right' => $default_border,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'E1E0F7'),
			),
			'font' => array(
				'bold' => true,
				'size' => 16,
			)
		);
		$style_content = array(
			'borders' => array(
				'allborders' => $default_border,
				// 'bottom' => $default_border,
				// 'left' => $default_border,
				// 'top' => $default_border,
				// 'right' => $default_border,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => 'eeeeee'),
			),
			'font' => array(
				'size' => 12,
			)
		);
		$excel = PHPExcel_IOFactory::load(FCPATH . "assets/template/template_pengabdian_no_reviewer.xlsx");
		$excel->getProperties()->setCreator("Simakip");
		// ->setLastModifiedBy("Sigit prasetya n")
		// ->setTitle("Creating file excel with php Test Document")
		// ->setSubject("Creating file excel with php Test Document")
		// ->setDescription("How to Create Excel file from PHP with PHPExcel 1.8.0 Classes by seegatesite.com.")
		// ->setKeywords("phpexcel")
		// ->setCategory("Test result file");
		$excel->setActiveSheetIndex(0);
		// $dataku=array(
		// 	array('C001','Iphone 6'),
		// 	array('C002','Samsung Galaxy S4'),
		// 	array('C003','Nokia Lumia'),
		// 	array('C004','Blackberry Curve'));
		$firststyle = 'B11';
		$laststyle = 'B11';
		for ($i = 0; $i < count($data); $i++) {
			$urut = $i + 11;
			$num = 'B' . $urut;
			$judul_penelitian = 'C' . $urut;
			$nama_peneliti = 'E' . $urut;
			$anggota = 'F' . $urut;
			$lokasi = 'G' . $urut;
			$anggaran = 'H' . $urut;
			$fakultas = 'I' . $urut;

			$anggota_string = "";
			for ($x = 0; $x < count($data[$i]["anggota"]); $x++) {
				$anggota_string .= "- " . $data[$i]["anggota"][$x]["dosen"]["nama_lengkap"] . "\n";
			}

			$excel->setActiveSheetIndex(0)
				->setCellValue($num, $i + 1)
				->setCellValue($judul_penelitian, $data[$i]['judul'])->mergeCells($judul_penelitian . ':D' . $urut)
				->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
				->setCellValue($anggota, $anggota_string)
				->setCellValue($lokasi, $data[$i]['jenis_pengabdian']["nama"])
				->setCellValue($anggaran, $data[$i]['rp_total_biaya'])
				->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"] . "/" . $data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['penelitian_reviewer'])

			$excel->getActiveSheet()->getRowDimension($i + 11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle = $fakultas;
		}
		$excel->getActiveSheet()->getStyle($firststyle . ':' . $laststyle)->applyFromArray($style_content); // give style to header
		for ($col = 'A'; $col !== 'N'; $col++) {
			$excel->getActiveSheet()
				->getColumnDimension($col)
				->setAutoSize(true);
		}
		$excel->getActiveSheet()
			->getStyle($firststyle . ':' . $laststyle)
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Pengabdian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $this->is_login()['nidn'] . '_UsulanPengabdian_' . Date('dmY') . '.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
