<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Penelitianhibahs extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mk"=>"active","mk7"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
		$this->load->model('luaran_penelitian/Penelitian_hibah');
		$this->load->model('Tahun_kegiatan');
		$this->load->model('Point_penelitian');
		$this->load->model('Point_log');

    }
	public function index()
	{
		$default_view = 10;
		$page = 0;
		$user = $this->is_login();
		$penelitian_hibah = Penelitian_hibah::where('isdelete', '=','0');
		$judul_get = $this->input->get("judul");
		$tahun_get = $this->input->get("tahun");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($judul_get)) $penelitian_hibah->where('judul','LIKE','%'.$judul_get.'%');
		if(isset($tahun_get) && $tahun_get!='') $penelitian_hibah->where('tahun','=',$tahun_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $penelitian_hibah->where('isvalidate','=',(int)$isvalidate-1);

		if(!isviewall()){
			$penelitian_hibah->where('dosen', '=', $user["id"]);
		}
		$default_skip = $page*$default_view;
		$count  = $penelitian_hibah->count();
		$penelitian_hibah = $penelitian_hibah->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();

		$penelitian_hibah->load('dosen');
		$penelitian_hibah->load('tahun');
		$data = array("items"=>$penelitian_hibah->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('manajement_kerja/penelitian_hibah/index', array_merge($data,$this->menu));
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'Judul Penelitian', 'required');
        $this->form_validation->set_rules('abstrak', 'Abstrak', 'required');
        $this->form_validation->set_rules('anggaran', 'Anggaran', 'required');
        $this->form_validation->set_rules('sumber', 'Sumber Dana', 'required');
        $this->form_validation->set_rules('pdf', 'Berkas', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun Pelaksanaan', 'required',
                array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('lokasi', 'Lokasi Penelitian', 'required');

        if ($this->form_validation->run() == FALSE){
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data = array("items_tahun"=>$tahun->toArray());
            $this->twig->display('manajement_kerja/penelitian_hibah/add', array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect('penelitianhibahs/');
		}
	}

	public function edit(){
		$user = $this->is_login();
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'Judul Penelitian', 'required');
        $this->form_validation->set_rules('abstrak', 'Abstrak', 'required');
        $this->form_validation->set_rules('anggaran', 'Anggaran', 'required');
        $this->form_validation->set_rules('sumber', 'Sumber Dana', 'required');
        $this->form_validation->set_rules('pdf', 'Berkas', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun Pelaksanaan', 'required',
                array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('lokasi', 'Lokasi Penelitian', 'required');

        if ($this->form_validation->run() == FALSE){
					$item = Penelitian_hibah::whereId($id)->with("dosen")->with("dosen.program_studi")->get();
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
			if(($item[0]->isvalidate==1 && !$this->isPengguna($item[0]->dosen)) || !$this->isOperator()){ $this->twig->display('404'); return; }
        	$data = $item->toArray()[0];
        	$data['items_tahun'] = $tahun->toArray();
            $this->twig->display('manajement_kerja/penelitian_hibah/edit', array_merge($data,$this->menu));
        }else{
        	if ($this->submit($id))
        		redirect('penelitianhibahs/');
		}
	}

	public function upload(){
		$this->generate_folder('uploads/penelitianhibahs');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/penelitianhibahs'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function resubmit(){
        $id = $this->uri->segment(3);
        $item = Penelitian_hibah::find($id);
        if(!$this->isPengguna($item->dosen)) return;
        $item->isvalidate = 0;
        $item->save();
        redirect($_SERVER['HTTP_REFERER']);
    }

	private function submit($id=0){
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$lokasi = $this->input->post('lokasi');
		$dosen = $this->input->post('dosen');
		$berkas = $this->input->post('pdf');
		$abstrak = $this->input->post('abstrak');
		$anggaran = $this->input->post('anggaran');
		$sumber = $this->input->post('sumber');
		if($id==0){
			$penelitian_hibah = new Penelitian_hibah;
		}else{
			$penelitian_hibah = Penelitian_hibah::find($id);
		}
		if($penelitian_hibah->isvalidate!=1){
			$penelitian_hibah->isvalidate = 3;
		}
		$penelitian_hibah->judul = $judul;
		$penelitian_hibah->dosen = $dosen;
		$penelitian_hibah->tahun = $tahun;
		$penelitian_hibah->lokasi = $lokasi;
		$penelitian_hibah->berkas = $berkas;
		$penelitian_hibah->abstrak = $abstrak;
		$penelitian_hibah->anggaran = $anggaran;
		$penelitian_hibah->sumber = $sumber;
		$penelitian_hibah->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
		if($id==0){
			send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Penelitian Mandiri baru dengan judul \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Penelitian Mandiri baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		}else{
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Penelitian Mandiri dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			$this->session->set_flashdata('success', 'Luaran dengan nama '.$penelitian_hibah->judul.' berhasil diubah!');
		}
		return TRUE;
	}

	public function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function show(){
		$id = $this->uri->segment(3);
		$penelitian_hibah = Penelitian_hibah::whereId($id)->with('dosen')->with('dosen.program_studi')->get();
		$penelitian_hibah->load('tahun');
		$data = array("item"=>$penelitian_hibah->toArray()[0]);
		$this->twig->display('manajement_kerja/penelitian_hibah/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function delete(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$item = Penelitian_hibah::find($id);
		if(!$this->isPengguna($item->dosen)) return;
		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Penelitian Mandiri dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('penelitianhibahs/');
	}

	public function validate(){
  		if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$item = Penelitian_hibah::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
		$item->save();

		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Penelitian Mandiri anda dengan judul \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
            send_notif_luaran($item->dosen, "Data Penelitian Mandiri anda dengan judul \"" . $first_four_title . "...\" telah divalidasi Operator");
        }

		$point = Point_penelitian::active(1)->get()->toArray();
        if(!empty($point)) {
            $array = [];
            $array[] = array("point" => $point[0]['ketua'], "created_date" => date('Y-m-d H:i:s'), "type" => 7, "status" => 6, "user_id" => $item->dosen,'jenis'=>6,'jenis_id'=>$item->id);
            Point_log::insert($array);
        }
        $first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Penelitian Mandiri dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$penelitian_hibah = Penelitian_hibah::where("isdelete","=","0")->where('isvalidate','=','1');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$penelitian_hibah->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}

		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$penelitian_hibah->where('tahun','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}
		$penelitian_hibah = $penelitian_hibah->with('dosen')->with('dosen.program_studi')->get();
		$penelitian_hibah->load('tahun');
		$this->load->view('manajement_kerja/penelitian_hibah/pdf',array("items"=>$penelitian_hibah->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_PenelitianMandiri_'.Date('dmY').'.pdf', 'I');
	}


}
