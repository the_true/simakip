<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Hkis extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mk"=>"active","mk4"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
		$this->load->model('luaran_penelitian/Hki');
		$this->load->model('Jenis_hki');
		$this->load->model('Tahun_kegiatan');
        $this->load->model('Point');
        $this->load->model('Point_log');
    }
	public function index()
	{
		$default_view = 10;
		$page = 0;

		$judul_get = $this->input->get("judul");
		$jenis_get = $this->input->get("jenis");
		$tahun_get = $this->input->get("tahun");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");

		$user = $this->is_login();
		$hki = Hki::where('isdelete', '=', '0');
		if(isset($page_get)) $page = $page_get-1;
		if(isset($jenis_get) && $jenis_get!='') $hki->where('jenis','=',$jenis_get);
		if(isset($judul_get)) $hki->where('judul','LIKE','%'.$judul_get.'%');
		if(isset($tahun_get) && $tahun_get!='') $hki->where('tahun_kegiatan','=',$tahun_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $hki->where('isvalidate','=',(int)$isvalidate-1);

		if(!isviewall()){
			$hki->where('dosen', '=', $user["id"]);
		}
		$default_skip = $page*$default_view;
		$count  = $hki->count();


		$hki = $hki->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();
		$hki->load('dosen');
		$hki->load('jenis_hki')->load('tahun_kegiatan');
		$data = array("items"=>$hki->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$jenis = Jenis_hki::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['jenis_hki'] = $jenis->toArray();
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('manajement_kerja/hki/index', array_merge($data,$this->menu));
	}

	private function validation($edit=FALSE){
        $this->form_validation->set_rules('judul', 'Judul HKI', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('jenis', 'Jenis HKI', 'required',
                array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('pendaftaran', 'No. Pendaftaran', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('pdf', 'Berkas', 'required');
  //       if (!$edit){
		// 	if (empty($_FILES['file']['name']))
		// 	{
		// 	    $this->form_validation->set_rules('file', 'Berkas', 'required');
		// 	}
		// }
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
        	$jenis = Jenis_hki::where("isdelete","=","0")->get();
        	$data = array("items_hki"=>$jenis->toArray());
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data["items_tahun"] = $tahun->toArray();
            $this->twig->display('manajement_kerja/hki/add', array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect('hkis');
        		// $this->twig->display('manajement_kerja/hki/succeed');
        	else echo $this->upload->display_errors();
		}
	}

	public function upload(){
		$this->generate_folder('uploads/hkis');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/hkis'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}


	private function submit($id=0){
		$is_upload = TRUE;
		// if (!empty($_FILES['file']['name']))
		// {
		// 	    $is_upload = TRUE;
		// }
		// $this->generate_folder('uploads/hkis');
		// $filename = $this->gen_uuid();
		// $upload_config = [
		// 	'file_name' => $filename,
		// 	'upload_path' => realpath(APPPATH . '../uploads/hkis'),
		// 	'allowed_types' => 'pdf',
		// 	'max_size' => 2000,
		// 	'file_ext_tolower' => TRUE
		// ];
		$this->load->library('upload', $upload_config);
		$judul = $this->input->post('judul');
		$jenis = $this->input->post('jenis');
		$pendaftaran = $this->input->post('pendaftaran');
		$status = $this->input->post('status');
		$dosen = $this->input->post('dosen');
		$tahun = $this->input->post('tahun');
		$berkas = $this->input->post('pdf');
		// $file_field = "file";
		$uploaded = TRUE;
		// if($is_upload){
		// 	$uploaded = $this->upload->do_upload($file_field);
		// }
		if ($uploaded){
			if($id==0){
				$hki = new Hki;
			}else{
				$hki = Hki::find($id);
			}
			if($hki->isvalidate!=1){
				$hki->isvalidate = 3;
			}
			$hki->judul = $judul;
			$hki->dosen = $dosen;
			$hki->jenis = $jenis;
			$hki->pendaftaran = $pendaftaran;
			$hki->status = $status;
			$hki->tahun_kegiatan = $tahun;

			if($is_upload) $hki->berkas = $berkas;
			$hki->save();
			$first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
			if($id==0){
				
				send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Hak Kekayaan Intelektual baru dengan judul \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Hak Kekayaan Intelektual baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			}else{
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Hak Kekayaan Intelektual dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
				$this->session->set_flashdata('success', 'Luaran dengan nama '.$hki->judul.' berhasil diubah!');
			}
			return TRUE;
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function show(){
		$id = $this->uri->segment(3);
		$hki = Hki::whereId($id)->with('dosen')->with('dosen.program_studi')->with('tahun_kegiatan')->get();
		$hki->load('jenis_hki');
		$data = array("item"=>$hki->toArray()[0]);
		$this->twig->display('manajement_kerja/hki/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function edit(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);


        if ($this->form_validation->run() == FALSE){
					$jurnal = Hki::whereId($id)->with('dosen')->with('dosen.program_studi')->get();
        	$jenis = Jenis_hki::where("isdelete","=","0")->get();
            if(($jurnal[0]->isvalidate==1  && !$this->isPengguna($jurnal[0]->dosen)) || !$this->isOperator() && !$this->isPengguna($jurnal[0]->dosen)  ){ $this->twig->display('404'); return; }
            $data = array("items_hki"=>$jenis->toArray());
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data["items_tahun"] = $tahun->toArray();
			$this->twig->display('manajement_kerja/hki/edit', array_merge($jurnal->toArray()[0],$data,$this->menu));
        }else{
        	if ($this->submit($id))
        		redirect('hkis');
        	else echo $this->upload->display_errors();
		}

	}

	public function delete(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$item = Hki::find($id);
		if(!$this->isPengguna($item->dosen)) return;
		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Hak Kekayaan Intelektual dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('hkis/');
	}

	public function validate(){
  if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$item = Hki::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
        $item->save();

        $first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
        if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Hak Kekayaan Intelektual anda dengan judul \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
            send_notif_luaran($item->dosen, "Data Hak Kekayaan Intelektual anda dengan judul \"" . $first_four_title . "...\" telah divalidasi Operator");
        }

		$point = Point::active(7)->get()->toArray();
        if(!empty($point)) {
            $array = [];
            $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => 7, "user_id" => $item->dosen, "created_date" => date('Y-m-d H:i:s'),'jenis'=>4,'jenis_id'=>$item->id);
            Point_log::insert($array);
        }
        $first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Hak Kekayaan Intelektual dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}


    public function resubmit(){
        $id = $this->uri->segment(3);
        $item = Hki::find($id);
        if(!$this->isPengguna($item->dosen)) return;
        $item->isvalidate = 0;
        $item->save();
        redirect($_SERVER['HTTP_REFERER']);
    }

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$hki = Hki::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$hki->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}

		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$hki->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}

		$hki = $hki->get();
		$hki->load('dosen');
		$hki->load('jenis_hki');
		$hki->load('tahun_kegiatan');
		$this->load->view('manajement_kerja/hki/pdf',array("items"=>$hki->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_HKI_'.Date('dmY').'.pdf', 'I');
	}

}
