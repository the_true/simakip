<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Forumilmiahs extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mk"=>"active","mk3"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
        $this->load->model('Fakultas');
        $this->load->model('Forum_ilmiah_anggota');
		$this->load->model('luaran_penelitian/Forum_ilmiah');
		$this->load->model('Tahun_kegiatan');
        $this->load->model('Point');
        $this->load->model('Point_log');
    }
	public function index()
	{
		$default_view = 10;
		$page = 0;

		$judul_get = $this->input->get("judul");
		$tahun_get = $this->input->get("tahun");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		$tingkat_get = $this->input->get("tingkat");
		$user = $this->is_login();
		$forum_ilmiah = Forum_ilmiah::where('isdelete','=','0');


		if(isset($page_get)) $page = $page_get-1;
		if(isset($judul_get)) $forum_ilmiah->where('judul','LIKE','%'.$judul_get.'%');
		if(isset($tahun_get) && $tahun_get!='') $forum_ilmiah->where('tahun_kegiatan','=',$tahun_get);
		if(isset($tingkat_get) && $tingkat_get!='') $forum_ilmiah->where('tingkat','=',$tingkat_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $forum_ilmiah->where('isvalidate','=',(int)$isvalidate-1);

		if(!isviewall()){
			$forum_ilmiah = $forum_ilmiah->whereHas("forum_ilmiah_anggota", function($q) use ($user){
				$q->where('dosen', $user["id"]);
			});
		}



		$default_skip = $page*$default_view;
		$count  = $forum_ilmiah->count();
		$forum_ilmiah = $forum_ilmiah->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();
		$forum_ilmiah->load('forum_ilmiah_anggota.dosen')->load('tahun_kegiatan');
		$data = array("items"=>$forum_ilmiah->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['tingkat'] = $tingkat_get;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('manajement_kerja/forum_ilmiah/index', array_merge($data,$this->menu));
	}

	private function validation($edit=False){
		$this->form_validation->set_rules('judul', 'Judul Makalah', 'required');
		$this->form_validation->set_rules('forum', 'Nama Forum', 'required');
		$this->form_validation->set_rules('abstrak', 'Abstrak', 'required');
		$this->form_validation->set_rules('penyelenggara', 'Institusi Penyelenggara', 'required');
		$this->form_validation->set_rules('waktu', 'Waktu Pelaksanaan', 'required');
		$this->form_validation->set_rules('tempat', 'Tempat Pelaksanaan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('tingkat', 'Tingkat Forum Ilmiah', 'required');
		$this->form_validation->set_rules('tahun', 'Tahun', 'required');
		$this->form_validation->set_rules('pdf', 'Berkas', 'required');
  //       if (!$edit){
		// 	if (empty($_FILES['file']['name']))
		// 	{
		// 	    $this->form_validation->set_rules('file', 'Berkas', 'required');
		// 	}
		// }
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data = array("items_tahun"=>$tahun->toArray());
			$u['user'] = $user;
			$data = array_merge($u, $data);

            $this->twig->display('manajement_kerja/forum_ilmiah/add',array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect('forumilmiahs');
        	else echo $this->upload->display_errors();
		}
	}

	public function upload(){
		$this->generate_folder('uploads/forumilmiahs');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/forumilmiahs'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function submit_anggota($data, $is_edit){
	    if (!$is_edit) {
	        if ($data['anggota_add']) {
				$pd = [];
	            foreach ($data['anggota_add'] as $key => $value) {
	                $pd[$key]['forum_ilmiah'] = $data['forum_ilmiah_id'];
	                $pd[$key]['dosen'] = $value['dosen_id'];
	                $pd[$key]['status'] = $value['status_anggota'];
	            }

	            try{
	                $res = Forum_ilmiah_anggota::insert($pd);
	                return ['status'=> 'ok', 'messages'=>$res];
	            }catch(Exception $e){
	                return ['status'=> 'error', 'messages'=>$e->getMessage()];
	            }
	        }
	    }else {
	        if ($data['anggota_add']) {
	            $pd = [];
	            foreach ($data['anggota_add'] as $key => $value) {
	                $pd[$key]['forum_ilmiah'] = $data['forum_ilmiah_id'];
	                $pd[$key]['dosen'] = $value['dosen_id'];
	                $pd[$key]['status'] = $value['status_anggota'];
	            }
	            $res = Forum_ilmiah_anggota::insert($pd);
	        }

			if ($data['anggota_edit']) {
				foreach ($data['anggota_edit'] as $key => $value) {
		            $item = Forum_ilmiah_anggota::find($value['anggota_id']);
		            $item->dosen = $value['dosen_id'];
		            $item->status = $value['status_anggota'];
		            $item->save();
		        }
			}

	    }
	}

	private function submit($id=0){
		$is_upload = TRUE;
		// if (!empty($_FILES['file']['name']))
		// {
		// 	    $is_upload = TRUE;
		// }
		// $this->generate_folder('uploads/forumilmiahs');
		// $filename = $this->gen_uuid();
		// $upload_config = [
		// 	'file_name' => $filename,
		// 	'upload_path' => realpath(APPPATH . '../uploads/forumilmiahs'),
		// 	'allowed_types' => 'pdf',
		// 	'max_size' => 2000,
		// 	'file_ext_tolower' => TRUE
		// ];
		// $this->load->library('upload', $upload_config);
		$judul = $this->input->post('judul');
		$forum = $this->input->post('forum');
		$penyelenggara = $this->input->post('penyelenggara');
		$waktu = $this->input->post('waktu');
		$tempat = $this->input->post('tempat');
		$status = $this->input->post('status');
		$tingkat = $this->input->post('tingkat');
		$tahun = $this->input->post('tahun');
		$berkas = $this->input->post('pdf');
		$file_field = "file";
		$uploaded = TRUE;
		// if($is_upload){
		// 	$uploaded = $this->upload->do_upload($file_field);
		// }
		if ($uploaded){
			if($id==0){
				$forum_ilmiah = new Forum_ilmiah;
			}else{
				$forum_ilmiah = Forum_ilmiah::find($id);
			}
			if($forum_ilmiah->isvalidate!=1){
				$forum_ilmiah->isvalidate = 3;
			}
			$forum_ilmiah->judul = $judul;
			$forum_ilmiah->forum = $forum;
			$forum_ilmiah->penyelenggara = $penyelenggara;
			$forum_ilmiah->waktu = $waktu;
			$forum_ilmiah->tempat = $tempat;
			$forum_ilmiah->tingkat = $tingkat;
			$forum_ilmiah->status = $status;
			$forum_ilmiah->tahun_kegiatan = $tahun;
			$abstrak = $this->input->post('abstrak');
			$forum_ilmiah->abstrak = $abstrak;
			if($is_upload) $forum_ilmiah->berkas = $berkas;
			$forum_ilmiah->save();

			if($id==0){
				$forum_ilmiah_id = $forum_ilmiah->id;
				$is_edit = false;

				$data_anggota['anggota_add'] = $this->input->post('anggota_add');
				$data_anggota['forum_ilmiah_id'] = $forum_ilmiah_id;
			}else{
				$forum_ilmiah_id = $id;
				$is_edit = true;

				$data_anggota['anggota_edit'] = $this->input->post('anggota');
				$data_anggota['anggota_add'] = $this->input->post('anggota_add');
				$data_anggota['forum_ilmiah_id'] = $forum_ilmiah_id;
			}

			$this->submit_anggota($data_anggota, $is_edit);

			$first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
			if($id==0){
				send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Pemakalah Forum Ilmiah baru dengan judul \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Pemakalah Forum Ilmiah baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			}else{
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Pemakalah Forum Ilmiah dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
				$this->session->set_flashdata('success', 'Luaran dengan nama '.$forum_ilmiah->judul.' berhasil diubah!');
			}
			return TRUE;
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function edit(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);

      	if ($this->form_validation->run() == FALSE){
			$jurnal = Forum_ilmiah::whereId($id)->with(['forum_ilmiah_anggota' => function($query){ $query->orderBy('status', 'desc'); }, 'forum_ilmiah_anggota.dosen.program_studi'])->get();
	    	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
            if(($jurnal[0]->isvalidate==1  && !$this->isPengguna($jurnal[0]->dosen)) || !$this->isOperator() && !$this->isPengguna($jurnal[0]->dosen)  ){ $this->twig->display('404'); return; }
            $data = array("items_tahun"=>$tahun->toArray());
			$this->twig->display('manajement_kerja/forum_ilmiah/edit', array_merge($jurnal->toArray()[0],$this->menu,$data));
        }else{
        	if ($this->submit($id))
        		redirect('forumilmiahs');
        	else echo $this->upload->display_errors();
		}

	}

	public function show(){
		$id = $this->uri->segment(3);
		$forum_ilmiah = Forum_ilmiah::whereId($id)->with('forum_ilmiah_anggota.dosen.program_studi')->with('tahun_kegiatan')->first();
		$data = array("item"=>$forum_ilmiah->toArray());
		$this->twig->display('manajement_kerja/forum_ilmiah/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function delete(){
		$id = $this->uri->segment(3);
        $forum_ilmiah = Forum_ilmiah::whereId($id)->with(["forum_ilmiah_anggota" => function($query){ $query->orderBy('status', 'desc'); }])->first()->toArray();

		$arr_dosen = [];
		foreach ($forum_ilmiah['forum_ilmiah_anggota'] as $key => $value) {
			$arr_dosen[] = $value['dosen'];
		}

		if (!in_array($this->is_login()['id'], $arr_dosen) && !$this->isOperator()) {
			$this->twig->display('404');
		}

        $item = Forum_ilmiah::find($id);
		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Pemakalah Forum Ilmiah dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('forumilmiahs/');
	}


	public function validate(){
  if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$item = Forum_ilmiah::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
		$item->save();

        $first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
        if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Pemakalah Forum Ilmiah anda dengan judul \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
            send_notif_luaran($item->dosen, "Data Pemakalah Forum Ilmiah anda dengan judul \"" . $first_four_title . "...\" telah divalidasi Operator");
        }

        //        Insert Point;
        $type = $item->tingkat + 3;
        $point = Point::active($type)->get()->toArray();
        if(!empty($point) && $item->tingkat < 3) {
            $array = [];
            $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => $type, "user_id" => $item->dosen,"created_date" => $item->created_at,'jenis'=>3,'jenis_id'=>$item->id);
            Point_log::insert($array);
        }
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Pemakalah Forum Ilmiah dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}


    public function resubmit(){
        $id = $this->uri->segment(3);
        $item = Forum_ilmiah::find($id);

        $forum_ilmiah = Forum_ilmiah::whereId($id)->with(["forum_ilmiah_anggota" => function($query){ $query->orderBy('status', 'desc'); }])->first()->toArray();

		$arr_dosen = [];
		foreach ($forum_ilmiah['forum_ilmiah_anggota'] as $key => $value) {
			$arr_dosen[] = $value['dosen'];
		}

		if (!in_array($this->is_login()['id'], $arr_dosen) && !$this->isOperator()) {
			$this->twig->display('404');
		}

        $item->isvalidate = 0;
        $item->save();
        redirect($_SERVER['HTTP_REFERER']);
    }

	public function excel(){
		$user = $this->is_login();
		$tingkat_get = $this->input->get("tingkat");
		$forum_ilmiah = Forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$forum_ilmiah->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}
		if(isset($tingkat_get) && $tingkat_get!='') $forum_ilmiah->where('tingkat','=',$tingkat_get);
		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$forum_ilmiah->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}
		$forum_ilmiah = $forum_ilmiah->with('dosen')->with('dosen.program_studi')->with('dosen.fakultas')->with('tahun_kegiatan')->get()->toArray();

		$this->load->library('libexcel');
		$default_border = array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb'=>'1006A3')
		);
 		$style_content = array(
 			'borders' => array(
				'allborders' => $default_border,
			),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
			),
 			'font' => array(
				'size' => 12,
			)
		);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_forum_ilmiah.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
		$excel->setActiveSheetIndex(0);

		$firststyle='B11';
		$laststyle='B11';

		for($i=0;$i<count($forum_ilmiah);$i++)
		{
			$urut = $i+11;
			$num = 'B'.$urut;
			$nama_dosen = 'C'.$urut;
			$judul = 'E'.$urut;
			$penyelenggara = 'F'.$urut;

			$value_nama_dosen = $forum_ilmiah[$i]["dosen"]["gelar_depan"]." ".$forum_ilmiah[$i]["dosen"]["nama"]." ". $forum_ilmiah[$i]["dosen"]["gelar_belakang"]."\n";
			$value_nama_dosen.='NIDN '.$forum_ilmiah[$i]["dosen"]["nidn"];
			$value_nama_dosen.='Status '.$forum_ilmiah[$i]["statusShow"];

			$value_judul = 'Tingkat '.$forum_ilmiah[$i]["type_show"]."\n";
			$value_judul .= 'Tahun '.$forum_ilmiah[$i]["tahun_kegiatan"]["tahun"]."\n";
			$value_judul .= $forum_ilmiah[$i]["judul"]."\n";
			$value_judul .= 'forum '.$forum_ilmiah[$i]["forum"]."\n";

			$value_penyelenggara = 'Institusi '.$forum_ilmiah[$i]["penyelenggara"]."\n";
			$value_penyelenggara .= 'Tgl '.$forum_ilmiah[$i]["waktu"]."\n";
			$value_penyelenggara .= 'Jml Halaman '.$forum_ilmiah[$i]["tempat"]."\n";

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($nama_dosen, $value_nama_dosen)->mergeCells($nama_dosen.':D'.$urut)
			->setCellValue($judul, $value_judul)
			->setCellValue($penyelenggara, $value_penyelenggara)->mergeCells($penyelenggara.':G'.$urut);

			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($nama_dosen)->getAlignment()->setWrapText(true);
			$excel->setActiveSheetIndex(0)->getStyle($judul)->getAlignment()->setWrapText(true);
			$excel->setActiveSheetIndex(0)->getStyle($penyelenggara)->getAlignment()->setWrapText(true);
			$laststyle='G'.$urut;
		}

		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content );
		$excel->getActiveSheet()
		->getStyle($firststyle.':'.$laststyle)
		->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$excel->getActiveSheet()->setTitle('Penelitian');
		$excel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_forum_ilmiah_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
    }

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$tingkat_get = $this->input->get("tingkat");
		$forum_ilmiah = Forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$forum_ilmiah->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}
		if(isset($tingkat_get) && $tingkat_get!='') $forum_ilmiah->where('tingkat','=',$tingkat_get);
		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$forum_ilmiah->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}
		$forum_ilmiah = $forum_ilmiah->with('dosen')->with('dosen.program_studi')->with('dosen.fakultas')->with('tahun_kegiatan')->get();
		$this->load->view('manajement_kerja/forum_ilmiah/pdf',array("items"=>$forum_ilmiah->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_ForumIlmiah_'.Date('dmY').'.pdf', 'I');
	}
}
