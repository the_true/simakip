<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Penelitiandiktis extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mk"=>"active","mk8"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
		$this->load->model('Dosen');
		$this->load->model('Program_studi');
		$this->load->model('Penelitian_dikti');
		$this->load->model('Penelitian_dikti_anggota');
        $this->load->model('Sumberdana');
        $this->load->model('Subsumberdana');
    }
	
	public function index()
	{
		$default_view = 10;
		$page = 0;
		$user = $this->is_login();
		$penelitian_dikti = Penelitian_dikti::where('isdelete', '=','0')->with('penelitian_dikti_anggota');
		$judul_penelitian = $this->input->get("judul_penelitian");
		$tahun_kegiatan = $this->input->get("tahun_kegiatan");
		$status_validasi = $this->input->get("status_validasi");
		
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($tahun_kegiatan) && $tahun_kegiatan!='') $penelitian_dikti->where('tahun_usulan','=',$tahun_get);
		if(isset($view_get)) $default_view = $view_get;
		$default_skip = $page*$default_view;
		$count  = $penelitian_dikti->count();
		$penelitian_dikti = $penelitian_dikti->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();

		$data = array("items"=>$penelitian_dikti->toArray());
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('manajement_kerja/penelitian_dikti/index', array_merge($data,$this->menu));
	}
  
  public function upload_laporan_penelitian(){
		$this->generate_folder('uploads/laporan_penelitian');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/laporan_penelitian'),
			'allowed_types' => 'pdf',
			'max_size' => 100000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}
	
	public function upload_laporan_penelitian_tercapai(){
  		$this->generate_folder('uploads/laporan_penelitian_tercapai');
  		$filename = $this->gen_uuid();
  		$upload_config = [
  			'file_name' => $filename,
  			'upload_path' => realpath(APPPATH . '../uploads/laporan_penelitian_tercapai'),
  			'allowed_types' => 'jpg|png|gif',
  			'max_size' => 10000,
  			'file_ext_tolower' => TRUE
  		];
  		$this->load->library('upload', $upload_config);
  		$file_field = "file";
  		$uploaded = TRUE;
  		$uploaded = $this->upload->do_upload($file_field);
  		if($uploaded) echo $this->upload->data('file_name');
  		echo $this->upload->display_errors();
  	}

	public function add(){
		$user = $this->is_login();
		
		$item = Sumberdana::where('isdelete', '=', '0')->get();  
		$this->load->helper('form');
		$this->load->library('form_validation');
		
        $this->form_validation->set_rules('pendanaan_penelitian', 'Pendanaan Penelitian', 'required');
        $this->form_validation->set_rules('tahun_usulan', 'Tahun Usulan', 'required');
        $this->form_validation->set_rules('pendanaan_penelitian2', 'Pendanaan Penelitian', 'required');
        $this->form_validation->set_rules('tahun_pelaksanaan', 'Tahun Pelaksanaan', 'required');
        $this->form_validation->set_rules('skema_penelitian', 'Skema Penelitian', 'required');
        // $this->form_validation->set_rules('kat_penelitian', 'Kategori Penelitian', 'required');
        $this->form_validation->set_rules('nominal_penelitian', 'Nominal Penelitian', 'required');
        $this->form_validation->set_rules('lama_penelitian', 'Lama Penelitian', 'required');
        $this->form_validation->set_rules('tahun_ke', 'Tahun Ke', 'required');
        $this->form_validation->set_rules('produk', 'Produk', 'required');
        $this->form_validation->set_rules('omzet', 'Omzet', 'required');
        $this->form_validation->set_rules('perusahaan', 'Perusahaan', 'required');
        $this->form_validation->set_rules('revenue', 'Revenue', 'required');
        $this->form_validation->set_rules('mitra', 'Mitra', 'required');
        $this->form_validation->set_rules('laporan_penelitian_res', 'Laporan Penelitian', 'required');
        $this->form_validation->set_rules('laporan_penelitian_tercapai_res', 'Laporan Penelitian Tercapai', 'required');
        
		$u['user'] = $user;
		$u['sumberdana'] = $item->toArray();
		$data = array_merge($u, $this->menu);
        if ($this->form_validation->run() == FALSE){ //print_r($this->form_validation->run());die();
            $this->twig->display('manajement_kerja/penelitian_dikti/add', $data);
        }else{
			if ($this->submit())
				redirect('penelitiandiktis/');
		}
	}
	
	public function subsumberdana($sub){
		$penelitian = $this->uri->segment(3);
		$data = Subsumberdana::where("isdelete","=","0")->where('sumberdana','=',$sub)->get();
		echo $data->toJson();
	}

	public function edit(){
		$user = $this->is_login();
		$id = $this->uri->segment(3);
		$itemsd = Sumberdana::where('isdelete', '=', '0')->get();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pendanaan_penelitian', 'Pendanaan Penelitian', 'required');
        $this->form_validation->set_rules('tahun_usulan', 'Tahun Usulan', 'required');
        $this->form_validation->set_rules('pendanaan_penelitian2', 'Pendanaan Penelitian', 'required');
        $this->form_validation->set_rules('tahun_pelaksanaan', 'Tahun Pelaksanaan', 'required');
        $this->form_validation->set_rules('skema_penelitian', 'Skema Penelitian', 'required');
        $this->form_validation->set_rules('lama_penelitian', 'Lama Penelitian', 'required');
        $this->form_validation->set_rules('tahun_ke', 'Tahun Ke', 'required');
        $this->form_validation->set_rules('produk', 'Produk', 'required');
        $this->form_validation->set_rules('omzet', 'Omzet', 'required');
        $this->form_validation->set_rules('abstrak', 'abstrak', 'required');
        $this->form_validation->set_rules('perusahaan', 'Perusahaan', 'required');
        $this->form_validation->set_rules('revenue', 'Revenue', 'required');
        $this->form_validation->set_rules('mitra', 'Mitra', 'required');
        $this->form_validation->set_rules('laporan_penelitian_res', 'Laporan Penelitian', 'required');
        $this->form_validation->set_rules('laporan_penelitian_tercapai_res', 'Laporan Penelitian Tercapai', 'required');
		
        if ($this->form_validation->run() == FALSE){
			$item = Penelitian_dikti::whereId($id)->with('penelitian_dikti_anggota.dosen.program_studi')->get();
			$data = $item->toArray()[0];
			
			$data['sumberdana'] = $itemsd->toArray();
            $this->twig->display('manajement_kerja/penelitian_dikti/edit', array_merge($data,$this->menu));
        }else{
        	if ($this->submit($id))
        		redirect('penelitiandiktis/');
		}
	}

	public function upload(){
		$this->generate_folder('uploads/penelitianhibahs');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/penelitianhibahs'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}
	
	public function submit_anggota($data, $is_edit){
		if (!$is_edit) {
			if ($data['anggota_add']) {
				$pd = []; 
	            foreach ($data['anggota_add'] as $key => $value) {
	                $pd[$key]['penelitian_dikti'] = $data['penelitian_dikti_id'];
	                $pd[$key]['dosen'] = $value['dosen_id'];
	                $pd[$key]['status'] = $value['status_anggota'];
	            }
				
				try{
					$res = Penelitian_dikti_anggota::insert($pd);
					return ['status'=> 'ok', 'messages'=>$res];
				}catch(Exception $e){
					return ['status'=> 'error', 'messages'=>$e->getMessage()];
				}			
			}
		}else {
			
			if ($data['anggota_add']) {
				$pd = []; 
				foreach ($data['anggota_add'] as $key => $value) {
					$pd[$key]['penelitian_dikti'] = $data['penelitian_dikti_id'];
					$pd[$key]['dosen'] = $value['dosen_id'];
					$pd[$key]['status'] = $value['status_anggota'];
				}
				$res = Penelitian_dikti_anggota::insert($pd);
			}
			
			if ($data['anggota_edit']) {
				foreach ($data['anggota_edit'] as $key => $value) {				
					$item = Penelitian_dikti_anggota::find($value['anggota_id']);
					$item->dosen = $value['dosen_id'];
					$item->status = $value['status_anggota'];
					$item->save();
				}
			}
			
		}
	}
	
	public function deleteanggota(){
		try {
			$item = Penelitian_dikti_anggota::find($this->input->post('anggota_id'))->delete();
			
			$result = [
				"status" => 'ok'
			];
			
			echo json_encode($result); exit;
		} catch (\Exception $e) {
			$result = [
				"status" => 'error',
				"message" => $e
			];
			
			echo json_encode($result); exit;
		}
	}

	private function submit($id=0){
		$pendanaan_penelitian = $this->input->post("pendanaan_penelitian");
		$tahun_usulan = $this->input->post("tahun_usulan");
		$pendanaan_penelitian2 = $this->input->post("pendanaan_penelitian2");
		$tahun_pelaksanaan = $this->input->post("tahun_pelaksanaan");
		$skema_penelitian = $this->input->post("skema_penelitian");
		// $kat_penelitian = $this->input->post("kat_penelitian");
		$nominal_penelitian = $this->input->post("nominal_penelitian");
		$skema_penelitian2 = $this->input->post("skema_penelitian2");
		$lama_penelitian = $this->input->post("lama_penelitian");
		$tahun_ke = $this->input->post("tahun_ke");
		$produk = $this->input->post("produk");
		$omzet = $this->input->post("omzet");
		$perusahaan = $this->input->post("perusahaan");
		$revenue = $this->input->post("revenue");
		$mitra = $this->input->post("mitra");
		$abstrak = $this->input->post("abstrak");
		$laporan_penelitian = $this->input->post("laporan_penelitian_res");
		$laporan_penelitian_tercapai = $this->input->post("laporan_penelitian_tercapai_res");

		if($id==0){
			$penelitian_dikti = new Penelitian_dikti;
		}else{
			$penelitian_dikti = Penelitian_dikti::find($id);
		}
		
		$penelitian_dikti->pendanaan_penelitian = $pendanaan_penelitian;
		$penelitian_dikti->tahun_usulan = $tahun_usulan;
		$penelitian_dikti->pendanaan_penelitian2 = $pendanaan_penelitian2;
		$penelitian_dikti->tahun_pelaksanaan = $tahun_pelaksanaan;
		$penelitian_dikti->skema_penelitian = $skema_penelitian;
		// $penelitian_dikti->kat_penelitian = $kat_penelitian;
		$penelitian_dikti->nominal_penelitian = $nominal_penelitian;
		$penelitian_dikti->skema_penelitian2 = $skema_penelitian2;
		$penelitian_dikti->lama_penelitian = $lama_penelitian;
		$penelitian_dikti->tahun_ke = $tahun_ke;
		$penelitian_dikti->produk = $produk;
		$penelitian_dikti->omzet = $omzet;
		$penelitian_dikti->perusahaan = $perusahaan;
		$penelitian_dikti->revenue = $revenue;
		$penelitian_dikti->mitra = $mitra;
		$penelitian_dikti->abstrak = $abstrak;
		$penelitian_dikti->laporan_penelitian = $laporan_penelitian;
		$penelitian_dikti->laporan_penelitian_tercapai = $laporan_penelitian_tercapai;
		
		// print_r($penelitian_dikti);die('wkwk');
		$penelitian_dikti->save();
		
		if($id==0){
			$penelitian_dikti_id = $penelitian_dikti->id;
			$is_edit = false;
			
			$data_anggota['anggota_add'] = $this->input->post('anggota_add');
			$data_anggota['penelitian_dikti_id'] = $penelitian_dikti_id;
		}else{
			$penelitian_dikti_id = $id;
			$is_edit = true;
            
			$data_anggota['anggota_edit'] = $this->input->post('anggota');
			$data_anggota['anggota_add'] = $this->input->post('anggota_add');
			$data_anggota['penelitian_dikti_id'] = $penelitian_dikti_id;
		}
		
		$this->submit_anggota($data_anggota, $is_edit);

		if($id==0){
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Penelitian External baru dengan skema penelitian '.$skema_penelitian,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		}else{
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Penelitian Mandiri dengan skema penelitian '.$skema_penelitian,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			$this->session->set_flashdata('success', 'Data berhasil diubah!');
		}
		return TRUE;
	}

	public function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	// public function show(){
	// 	$id = $this->uri->segment(3);
	// 	$penelitian_hibah = Penelitian_hibah::whereId($id)->with('dosen')->with('dosen.program_studi')->get();
	// 	$penelitian_hibah->load('tahun');
	// 	$data = array("item"=>$penelitian_hibah->toArray()[0]);
	// 	$this->twig->display('manajement_kerja/penelitian_dikti/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	// }

	public function delete(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$item = Penelitian_dikti::find($id);
		$item->isdelete = 1;
		$item->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Penelitian external','type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('penelitiandiktis/');
	}

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$penelitian_dikti = Penelitian_dikti::where("isdelete","=","0")->get();
		$this->load->view('manajement_kerja/penelitian_dikti/pdf',array("items"=>$penelitian_dikti->toArray()));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_PenelitianDikti_'.Date('dmY').'.pdf', 'I');
	}


}
