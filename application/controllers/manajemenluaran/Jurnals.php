<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Jurnals extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mk"=>"active","mk1"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
        // $this->userinfo = $this->is_login();
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
        $this->load->model('Fakultas');
        $this->load->model('Jurnal_anggota');
		$this->load->model('luaran_penelitian/Jurnal');
		$this->load->model('Point');
		$this->load->model('Point_log');
		$this->load->model('Tahun_kegiatan');
    }
	public function index()
	{
		$default_view = 10;
		$page = 0;

		$type_get = $this->input->get("type");
		$judul_get = $this->input->get("judul");
		$tahun_get = $this->input->get("tahun");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");

		$user = $this->is_login();
		$jurnal = Jurnal::where('isdelete', '=', '0');
		if(isset($page_get)) $page = $page_get-1;
		if(isset($judul_get)) $jurnal->where('judul','LIKE','%'.$judul_get.'%');
		if(!empty($type_get)) $jurnal->where('type','=',$type_get);
		if(isset($tahun_get) && $tahun_get!='') $jurnal->where('tahun_kegiatan','=',$tahun_get);
		if(isset($view_get)) $default_view = $view_get;

		$isvalidate = $this->input->get("isvalidate");
		if(isset($isvalidate) && $isvalidate!='') $jurnal->where('isvalidate','=',(int)$isvalidate-1);

		if(!isviewall()){
			$jurnal = $jurnal->whereHas("jurnal_anggota", function($q) use ($user){
				$q->where('dosen', $user["id"]);
			});
		}
		$default_skip = $page*$default_view;
		$count  = $jurnal->count();
		$jurnal = $jurnal->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();
		$jurnal->load('tahun_kegiatan');
		$jurnal->load('jurnal_anggota.dosen');
		$data = array("items"=>$jurnal->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$data['tahun'] = $tahun->toArray();
		$data['isvalidate'] = $isvalidate;
		$data['views'] = $default_view;
		$data['page'] = $page+1;
		$data['type'] = $type_get;
		$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('manajement_kerja/jurnal/index', array_merge($data,$this->menu));
	}

	private function validation($edit=FALSE){
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('nama_jurnal', 'Nama Jurnal', 'required',
                array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('abstrak', 'Abstrak', 'required');
        $this->form_validation->set_rules('issn', 'ISSN', 'required');
        $this->form_validation->set_rules('volume', 'Volume', 'required');
        $this->form_validation->set_rules('nomor', 'Nomor', 'required');
        $this->form_validation->set_rules('halaman_mulai', 'Halaman Dari', 'required');
        $this->form_validation->set_rules('halaman_akhir', 'Halaman Sampai', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('pdf', 'Berkas', 'required');
        //$this->form_validation->set_rules('url', 'URL', 'required');
  //       if (!$edit){
		// 	if (empty($_FILES['file']['name']))
		// 	{
		// 	    $this->form_validation->set_rules('file', 'Berkas', 'required');
		// 	}
		// }
	}

	public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
        	$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
        	$data = array("items_tahun"=>$tahun->toArray());
            $u['user'] = $user;
			$data = array_merge($u, $data);
            $this->twig->display('manajement_kerja/jurnal/add',array_merge($data,$this->menu));
        }else{
        	if ($this->submit())
        		redirect("jurnals/");
        	else echo $this->upload->display_errors();
		}
	}

	public function upload(){
		$this->generate_folder('uploads/jurnals');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/jurnals'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function submit_jurnal_anggota($data){
		if ($data['nidn_anggota'] && $data['jurnal_id']) {
			$jurnal_anggota = [];
			foreach ($data['dosen_id'] as $key_nid => $value_dosen_id) {
				$jurnal_anggota[$key_nid]['jurnal'] = $data['jurnal_id'];
				$jurnal_anggota[$key_nid]['dosen'] = $value_dosen_id;
				$jurnal_anggota[$key_nid]['status'] = $data['status_anggota'][$key_nid];
			}

			try{
				$res = Jurnal_anggota::insert($jurnal_anggota);
				return ['status'=> 'ok', 'messages'=>$res];
			}catch(Exception $e){
				return ['status'=> 'error', 'messages'=>$e->getMessage()];
			}
		}
	}

	public function submit_anggota($data, $is_edit){
	    if (!$is_edit) {
	        if ($data['anggota_add']) {
				$pd = [];
	            foreach ($data['anggota_add'] as $key => $value) {
	                $pd[$key]['jurnal'] = $data['jurnal_id'];
	                $pd[$key]['dosen'] = $value['dosen_id'];
	                $pd[$key]['status'] = $value['status_anggota'];
	            }

	            try{
	                $res = Jurnal_anggota::insert($pd);
	                return ['status'=> 'ok', 'messages'=>$res];
	            }catch(Exception $e){
	                return ['status'=> 'error', 'messages'=>$e->getMessage()];
	            }
	        }
	    }else {
	        if ($data['anggota_add']) {
	            $pd = [];
	            foreach ($data['anggota_add'] as $key => $value) {
	                $pd[$key]['jurnal'] = $data['jurnal_id'];
	                $pd[$key]['dosen'] = $value['dosen_id'];
	                $pd[$key]['status'] = $value['status_anggota'];
	            }
	            $res = Jurnal_anggota::insert($pd);
	        }

			if ($data['anggota_edit']) {
				foreach ($data['anggota_edit'] as $key => $value) {
		            $item = Jurnal_anggota::find($value['anggota_id']);
		            $item->dosen = $value['dosen_id'];
		            $item->status = $value['status_anggota'];
		            $item->save();
		        }
			}
	    }
	}

	private function submit($id=0){
		$is_upload = FALSE;
		if (!empty($_FILES['file']['name']))
		{
			    $is_upload = TRUE;
		}

		// $filename = $this->gen_uuid();
		// $upload_config = [
		// 	'file_name' => $filename,
		// 	'upload_path' => realpath(APPPATH . '../uploads/jurnals'),
		// 	'allowed_types' => 'pdf',
		// 	'max_size' => 2000,
		// 	'file_ext_tolower' => TRUE
		// ];
		// $this->load->library('upload', $upload_config);
		// $this->upload->initialize($upload_config);
		// echo "<pre>".print_r($this->upload->data())."</pre>";
		$jenis = $this->input->post("jenis");
		$judul = $this->input->post('judul');
		$nama_jurnal = $this->input->post('nama_jurnal');
		$issn = $this->input->post('issn');
		$volume = $this->input->post('volume');
		$nomor = $this->input->post('nomor');
		$halaman_mulai = $this->input->post('halaman_mulai');
		$halaman_akhir = $this->input->post('halaman_akhir');
		$nama_nondosen = $this->input->post('nama_nondosen');
		$tahun = $this->input->post('tahun');
		$url = $this->input->post('url');
		$berkas = $this->input->post('pdf');
		$file_field = "file";
		$uploaded = TRUE;
		$is_upload = TRUE;
		// if($is_upload){
		// 	$uploaded = $this->upload->do_upload($file_field);
		// }
		if ($uploaded){
			if($id==0){
				$jurnal = new Jurnal;
			}else{
				$jurnal = Jurnal::find($id);
			}

			if($jurnal->isvalidate!=1){
				$jurnal->isvalidate = 3;
			}
			$jurnal->type = $jenis;
			$jurnal->judul = $judul;
			$jurnal->tahun_kegiatan = $tahun;
			$jurnal->nama_jurnal = $nama_jurnal;
			$jurnal->issn = $issn;
			$jurnal->volume = $volume;
			$jurnal->nomor = $nomor;
			$jurnal->halaman = $halaman_mulai." - ".$halaman_akhir;
			$jurnal->url = $url;
			$abstrak = $this->input->post('abstrak');
			$jurnal->abstrak = $abstrak;
			if($is_upload) $jurnal->berkas = $berkas;
			$jurnal->save();

			if($id==0){
				$jurnal_id = $jurnal->id;
				$is_edit = false;

				$data_anggota['anggota_add'] = $this->input->post('anggota_add');
				$data_anggota['jurnal_id'] = $jurnal_id;
			}else{
				$jurnal_id = $id;
				$is_edit = true;

				$data_anggota['anggota_edit'] = $this->input->post('anggota');
				$data_anggota['anggota_add'] = $this->input->post('anggota_add');
				$data_anggota['jurnal_id'] = $jurnal_id;
			}

			$this->submit_anggota($data_anggota, $is_edit);

			$first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
			if($id==0){
				send_notif_luaran(0,"Dosen ".$this->is_login()["nama"]." membuat Publikasi Jurnal baru dengan judul \"".$first_four_title."...\", segera di validasi");
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Jurnal baru dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
			}else{
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Jurnal dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
				$this->session->set_flashdata('success', 'Luaran dengan nama '.$jurnal->judul.' berhasil diubah!');
			}
			return TRUE;
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function show(){
		$id = $this->uri->segment(3);
        $jurnal = Jurnal::whereId($id)->with('jurnal_anggota.dosen.program_studi')->with('tahun_kegiatan')->get();
		$data = array("item"=>$jurnal->toArray()[0]);
		$this->twig->display('manajement_kerja/jurnal/show', array_merge($data,$this->menu,array("login"=>$this->check_login())));
	}

	public function edit(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);

        if ($this->form_validation->run() == FALSE){
			$jurnal = Jurnal::whereId($id)->with(["jurnal_anggota" => function($query){ $query->where('isdelete', 0)->orderBy('status', 'desc'); }, "jurnal_anggota.dosen.program_studi"])->get();
			$tahun = Tahun_kegiatan::where("isdelete","=","0")->get();
			if(($jurnal[0]->isvalidate==1  && !$this->isPengguna($jurnal[0]->jurnal_anggota[0]->dosen)) || !$this->isOperator() && !$this->isPengguna($jurnal[0]->jurnal_anggota[0]->dosen)  ){ $this->twig->display('404'); return; }
        	$data = array("items_tahun"=>$tahun->toArray());
			$this->twig->display('manajement_kerja/jurnal/edit', array_merge($jurnal->toArray()[0],$this->menu,$data));
        }else{
        	if ($this->submit($id))
        		redirect('jurnals/');
        		// $this->twig->display('manajement_kerja/jurnal/succeed');
        	else echo $this->upload->display_errors();
		}
	}

	public function updatejurnalanggota(){
		try {
			$item = Jurnal_anggota::find($this->input->post('jurnal_anggota_id'));
	        $item->dosen = $this->input->post('dosen_id');
	        $item->jurnal = $this->input->post('jurnal_id');
	        $item->status = $this->input->post('status_anggota');
			$item->save();

			$result = [
				"status" => 'ok'
			];

			echo json_encode($result); exit;
		} catch (\Exception $e) {
			$result = [
				"status" => 'error',
				"message" => $e
			];

			echo json_encode($result); exit;
		}
	}

	public function deletejurnalanggota(){
		try {
			$item = Jurnal_anggota::find($this->input->post('jurnal_anggota_id'))->delete();

			$result = [
				"status" => 'ok'
			];

			echo json_encode($result); exit;
		} catch (\Exception $e) {
			$result = [
				"status" => 'error',
				"message" => $e
			];

			echo json_encode($result); exit;
		}
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$item = Jurnal::find($id);

		$jurnal = Jurnal::whereId($id)->with(["jurnal_anggota" => function($query){ $query->where('isdelete', 0)->orderBy('status', 'desc'); }])->first()->toArray();
		$arr_dosen = [];
		foreach ($jurnal['jurnal_anggota'] as $key => $value) {
			$arr_dosen[] = $value['dosen'];
		}

		if (!in_array($this->is_login()['id'], $arr_dosen) && !$this->isOperator()) {
			$this->twig->display('404');
		}

		$item->isdelete = 1;
		$item->save();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Jurnal dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect('jurnals/');
	}

	public function validate(){
        if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$item = Jurnal::find($id);
        $item->isvalidate = $this->input->post('pengecekan');
        $item->alasan = $this->input->post('alasan');
		$item->save();

		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
        if($item->isvalidate == 2) {
            send_notif_luaran($item->dosen, "Data Jurnal anda dengan judul \"" . $first_four_title . "...\" ditolak operator, silakan melakukan pengecekan.");
        }else {
           send_notif_luaran($item->dosen, "Data Jurnal anda dengan judul \"" . $first_four_title . "...\" telah divalidasi Operator");
        }
		$point = Point::active($item->type)->get()->toArray();
        if(!empty($point)) {
            $array = [];
            $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => $item->type, "user_id" => $item->dosen, "created_date" => date('Y-m-d H:i:s'),'jenis'=>1,'jenis_id'=>$item->id);
            if (!empty($item->personil)) {
                $array[] = array("point" => $point[0]['anggota'], "type" => 2, "status" => $item->type, "user_id" => $item->personil, "created_date" => date('Y-m-d H:i:s'),'jenis'=>1,'jenis_id'=>$item->id);
            }
            Point_log::insert($array);
        }
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Jurnal dengan nama judul/kegiatan '.$first_four_title,'type'=>'1','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}


    public function resubmit(){
        $id = $this->uri->segment(3);
        $item = Jurnal::find($id);
        $jurnal = Jurnal::whereId($id)->with(["jurnal_anggota" => function($query){ $query->where('isdelete', 0)->orderBy('status', 'desc'); }])->first()->toArray();

		$arr_dosen = [];
		foreach ($jurnal['jurnal_anggota'] as $key => $value) {
			$arr_dosen[] = $value['dosen'];
		}

		if (!in_array($this->is_login()['id'], $arr_dosen) && !$this->isOperator()) {
			$this->twig->display('404');
		}

        $item->isvalidate = 0;
        $item->save();
        redirect('jurnals/');
    }

    public function excel(){
		$user = $this->is_login();
		$type_get = $this->input->get("type");

		$jurnal = Jurnal::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$jurnal->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}
		if(!empty($type_get)) $jurnal->where('type','=',$type_get);

		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$jurnal->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}

		$jurnal = $jurnal->with('personil')->with("dosen")->with("dosen.fakultas")->with("dosen.program_studi")->with('tahun_kegiatan')->get()->toArray();
		$this->load->library('libexcel');
		$default_border = array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb'=>'1006A3')
		);
 		$style_content = array(
 			'borders' => array(
				'allborders' => $default_border,
			),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
			),
 			'font' => array(
				'size' => 12,
			)
		);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_publikasi_jurnal.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
		$excel->setActiveSheetIndex(0);

		$firststyle='B11';
		$laststyle='B11';

		for($i=0;$i<count($jurnal);$i++)
		{
			$urut = $i+11;
			$num = 'B'.$urut;
			$judul_penelitian = 'C'.$urut;
			$penulis_publikasi = 'E'.$urut;
			$data_jurnal = 'F'.$urut;
			$fakultas_prodi = 'G'.$urut;

			$value_judul_penelitian = 'Tahun '.$jurnal[$i]["tahun_kegiatan"]["tahun"]."\n";
			$value_judul_penelitian .= $jurnal[$i]['judul'];

			$value_penulis_publikasi = $jurnal[$i]["dosen"]["nama"];

			if ($jurnal[$i]["personil"]["nama"]) {
				$value_penulis_publikasi .= "\n".$jurnal[$i]["personil"]["nama"];
			}

			$value_data_jurnal=$jurnal[$i]["nama_jurnal"]."\n";
			$value_data_jurnal.='ISSN '.$jurnal[$i]["issn"]."\n";
			$value_data_jurnal.= 'Volume '.$jurnal[$i]["volume"]."\n";
			$value_data_jurnal.= 'Nomor '.$jurnal[$i]["nomor"]."\n";
			$value_data_jurnal.= 'Halaman '.$jurnal[$i]["halaman"]."\n";
			$value_data_jurnal.= 'URL '.$jurnal[$i]["url"]."\n";

			$value_fakultas_prodi = 'Fakultas '.$jurnal[$i]["dosen"]["fakultas"]["nama"]."\n";
			$value_fakultas_prodi .= 'Program Studi '.$jurnal[$i]["dosen"]["program_studi"]["nama"];

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_penelitian, $value_judul_penelitian)->mergeCells($judul_penelitian.':D'.$urut)
			->setCellValue($penulis_publikasi, $value_penulis_publikasi)
			->setCellValue($data_jurnal, $value_data_jurnal)
			->setCellValue($fakultas_prodi, $value_fakultas_prodi);

			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($judul_penelitian)->getAlignment()->setWrapText(true);
			$excel->setActiveSheetIndex(0)->getStyle($penulis_publikasi)->getAlignment()->setWrapText(true);
			$excel->setActiveSheetIndex(0)->getStyle($data_jurnal)->getAlignment()->setWrapText(true);
			$excel->setActiveSheetIndex(0)->getStyle($fakultas_prodi)->getAlignment()->setWrapText(true);
			$laststyle=$fakultas_prodi;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content );
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$excel->getActiveSheet()->setTitle('Penelitian');
		$excel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_publikasi_jurnal_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
    }

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$type_get = $this->input->get("type");

		$jurnal = Jurnal::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$jurnal->where('dosen', '=', $user["id"]);
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}
		if(!empty($type_get)) $jurnal->where('type','=',$type_get);

		$tahun_get = $this->input->get("tahun");
		$info = [];
		if(!empty($tahun_get) && $tahun_get!=''){
			$jurnal->where('tahun_kegiatan','=',$tahun_get);
			$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
			$info = $tahun_kegiatan->toArray();
		}

		$jurnal = $jurnal->with('personil')->with("dosen")->with("dosen.fakultas")->with("dosen.program_studi")->with('tahun_kegiatan')->get();
		$this->load->view('manajement_kerja/jurnal/pdf',array("items"=>$jurnal->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>$info));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_Jurnal_'.Date('dmY').'.pdf', 'I');
	}

}
