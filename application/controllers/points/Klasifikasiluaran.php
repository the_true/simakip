<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Klasifikasiluaran extends MY_BaseController {


  private $menu = array("p"=>"active","p2"=>"active");
  private $id_jenis = array(1,2,3,4,5);
  public function __construct() {
    $config = [
      'functions' => ['anchor','set_value','set_select'],
      'functions_safe' => ['validation_errors_array'],
    ];
    parent::__construct($config);
    $this->load->model("Klasifikasi_luaran");
  }

  public function index(){
    $data_group = Klasifikasi_luaran::select('versi','tanggal','status')->where('isdelete', '=', '0')->whereIn('type',$this->id_jenis)
    ->orderBy('tanggal','ASC');
		$info = $this->create_paging($data_group);
		$data_group = $data_group->take($info["limit"])->skip($info["skip"])->get();
    // echo $data_group->toJson(); die;
    $data_group = $data_group->toArray();
    $group_limit = [];
    foreach($data_group as $value){
      $group_limit[]=$value["tanggal"];
    }

    $data = Klasifikasi_luaran::whereIn('tanggal',$group_limit)->where('isdelete','=','0')->orderBy('tanggal')->orderBy('type')->get();
    $result = array();
    foreach ($data->toArray() as $data) {
      $id = $data['versi'].'<br>'.$data['tanggal'];
      if (isset($result[$id])) {
         $result[$id]["point"][] = $data;
         $result[$id]["status_text"] = $data["status_text"];
         $result[$id]["versi"] = $data["versi"];
         $result[$id]["tanggal"] = $data["tanggal"];
         $result[$id]["status"] = $data["status"];
      } else {
         $result[$id]["point"] = array($data);
      }
    };

		$items = ["items"=>$result];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/point/klasifikasi_luaran/index',array_merge($items,$this->menu));
  }

  public function insert(){
  $versi = $this->input->post("versi");
  $tanggal = $this->input->post('tanggal');

  $array = [];

  $poor_ketua = $this->input->post("poor_ketua");
  $poor_anggota = $this->input->post("poor_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$poor_ketua, "anggota"=>$poor_anggota,"type"=>1,"status"=>0);

  $good_ketua = $this->input->post("good_ketua");
  $good_anggota = $this->input->post("good_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$good_ketua, "anggota"=>$good_anggota,"type"=>2,"status"=>0);

  $very_good_ketua = $this->input->post("very_good_ketua");
  $very_good_anggota = $this->input->post("very_good_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$very_good_ketua, "anggota"=>$very_good_anggota,"type"=>3,"status"=>0);

  $excelent_ketua = $this->input->post("excelent_ketua");
  $excelent_anggota = $this->input->post("excelent_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$excelent_ketua, "anggota"=>$excelent_anggota,"type"=>4,"status"=>0);

  echo json_encode($array);
  Klasifikasi_luaran::insert($array);

  Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Point-Klasifikasi Luaran dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

  redirect("points/klasifikasiluaran");
  }

  public function update()
  {
    $versi = $this->input->post("versi");
    $tanggal = $this->input->post('tanggal');
    $status = $this->input->post('status');

    $poor_ketua = $this->input->post("poor_ketua");
    $poor_anggota = $this->input->post("poor_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$poor_ketua, "anggota"=>$poor_anggota,"type"=>1,"status"=>$status);
    Klasifikasi_luaran::where('type',1)->where('tanggal',$tanggal)->update($array);

    $good_ketua = $this->input->post("good_ketua");
    $good_anggota = $this->input->post("good_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$good_ketua, "anggota"=>$good_anggota,"type"=>2,"status"=>$status);
    Klasifikasi_luaran::where('type',2)->where('tanggal',$tanggal)->update($array);

    $very_good_ketua = $this->input->post("very_good_ketua");
    $very_good_anggota = $this->input->post("very_good_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$very_good_ketua, "anggota"=>$very_good_anggota,"type"=>3,"status"=>$status);
    Klasifikasi_luaran::where('type',3)->where('tanggal',$tanggal)->update($array);

    $excelent_ketua = $this->input->post("excelent_ketua");
    $excelent_anggota = $this->input->post("excelent_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$excelent_ketua, "anggota"=>$excelent_anggota,"type"=>4,"status"=>$status);
    Klasifikasi_luaran::where('type',4)->where('tanggal',$tanggal)->update($array);


    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Point-Klasifikasi Luaran dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect("points/klasifikasiluaran");

  }

  public function active($tanggal)
  {
    $aktiv = Klasifikasi_luaran::where('versi', str_replace("%20", " ", $tanggal))->update(array('status' => 1));
    Klasifikasi_luaran::where('versi', '!=', str_replace("%20", " ", $tanggal))->update(array('status' => 0));

    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengaktifkan konfigurasi Point-Klasifikasi Luaran dengan nama versi '.$aktiv->versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect($_SERVER['HTTP_REFERER']);
  }

  public function delete(){
    $versi = $this->uri->segment(4);
    $item = Klasifikasi_luaran::where('versi','=',urldecode($versi))->delete();
    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Point-Klasifikasi Luaran dengan nama versi '.urldecode($versi),'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    redirect($_SERVER['HTTP_REFERER']);
  }

  public function create_paging($model){
    $default_view = 10;
    $page = 0;

    $view_get = $this->input->get("views");
    $page_get = $this->input->get("page");
    if(isset($page_get)) $page = $page_get-1;
    if(isset($view_get)) $default_view = $view_get;

    $default_skip = ($page*$default_view)*4;
    $count  = $model->count();
    $total_pages = ($count/4)>0? ceil(($count/4)/($default_view)):1;
    // echo $default_view;
    return ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view*4,"skip"=>$default_skip,"total_pages"=>$total_pages];
  }


}
?>
