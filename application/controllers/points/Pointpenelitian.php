<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Pointpenelitian extends MY_BaseController {


  private $menu = array("p"=>"active","p2"=>"active");
  private $id_jenis = array(1,2);
  public function __construct() {
    $config = [
      'functions' => ['anchor','set_value','set_select'],
      'functions_safe' => ['validation_errors_array'],
    ];
    parent::__construct($config);
    $this->load->model("Point_penelitian");
  }

  public function index(){
    $data_group = Point_penelitian::select('versi','tanggal','status')->where('isdelete', '=', '0')->whereIn('type',$this->id_jenis)
    ->orderBy('tanggal','ASC');
		$info = $this->create_paging($data_group);
		$data_group = $data_group->take($info["limit"])->skip($info["skip"])->get();
    $data_group = $data_group->toArray();
    $group_limit = [];
    foreach($data_group as $value){
      $group_limit[]=$value["tanggal"];
    }

    $data = Point_penelitian::whereIn('tanggal',$group_limit)->where('isdelete','=','0')->orderBy('tanggal')->orderBy('type')->get();
      $result = array();
    foreach ($data->toArray() as $data) {
      $id = $data['versi'].'<br>'.$data['tanggal'];
      if (isset($result[$id])) {
         $result[$id]["point"][] = $data;
         $result[$id]["status_text"] = $data["status_text"];
         $result[$id]["versi"] = $data["versi"];
         $result[$id]["tanggal"] = $data["tanggal"];
         $result[$id]["status"] = $data["status"];
      } else {
         $result[$id]["point"] = array($data);
      }
    };

		$items = ["items"=>$result];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/point/point_penelitian/index',array_merge($items,$this->menu));
  }


  public function active($tanggal){
    $aktiv = Point_penelitian::where('versi',str_replace("%20"," ",$tanggal))->update(array('status' => 1));
    Point_penelitian::where('versi','!=',str_replace("%20"," ",$tanggal))->update(array('status' => 0));

    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengaktifkan konfigurasi Point-Point Penelitian dengan nama versi '.$aktiv->versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect("points/Pointpenelitian");
  }

  public function delete($versi){
    $item = Point_penelitian::where('versi','=',urldecode($versi))->delete();
    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Point-Point Penelitian dengan nama versi '.urldecode($versi),'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    redirect($_SERVER['HTTP_REFERER']);
  }

  public function insert(){
  $versi = $this->input->post("versi");
  $tanggal = $this->input->post('tanggal');

  $array = [];

  $mandiri_ketua = $this->input->post("mandiri_ketua");
  $mandiri_anggota = $this->input->post("mandiri_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$mandiri_ketua, "anggota"=>$mandiri_anggota,"type"=>1,"status"=>0);

  $internal_ketua = $this->input->post("internal_ketua");
  $internal_anggota = $this->input->post("internal_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$internal_ketua, "anggota"=>$internal_anggota,"type"=>2,"status"=>0);
  echo json_encode($array);
  Point_penelitian::insert($array);

  Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Point-Point Penelitian dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

  redirect("points/pointpenelitian");
  }

  public function update()
  {
    $versi = $this->input->post("versi");
    $tanggal = $this->input->post('tanggal');
    $status = $this->input->post('status');

    $mandiri_ketua = $this->input->post("mandiri_ketua");
    $mandiri_anggota = $this->input->post("mandiri_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$mandiri_ketua, "anggota"=>$mandiri_anggota,"type"=>1,"status"=>$status);
    Point_penelitian::where('type',1)->where('tanggal',$tanggal)->update($array);

    $internal_ketua = $this->input->post("internal_ketua");
    $internal_anggota = $this->input->post("internal_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$internal_ketua, "anggota"=>$internal_anggota,"type"=>2,"status"=>$status);
    Point_penelitian::where('type',2)->where('tanggal',$tanggal)->update($array);


    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Point-Point Penelitian dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect("points/pointpenelitian");

  }

  public function create_paging($model){
    $default_view = 10;
    $page = 0;

    $view_get = $this->input->get("views");
    $page_get = $this->input->get("page");
    if(isset($page_get)) $page = $page_get-1;
    if(isset($view_get)) $default_view = $view_get;

    $default_skip = $page*$default_view;
    $count  = $model->count();
    $total_pages = $count>0? ceil(($count/2)/$default_view):1;
    return ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages];
  }


}
?>
