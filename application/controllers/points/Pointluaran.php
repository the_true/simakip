<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Pointluaran extends MY_BaseController {


  private $menu = array("p"=>"active","p2"=>"active");
  private $id_jenis = array(1,2,3,4,5,6,7,8);
  public function __construct() {
    $config = [
      'functions' => ['anchor','set_value','set_select'],
      'functions_safe' => ['validation_errors_array'],
    ];
    parent::__construct($config);
    $this->load->model("Point");
  }

  public function index(){
    $data_group = Point::select('versi','tanggal','status')->where('isdelete', '=', '0')->whereIn('type',$this->id_jenis)
    ->orderBy('tanggal','ASC');
		$info = $this->create_paging($data_group);
		$data_group = $data_group->take($info["limit"]*8)->skip($info["skip"]*8)->get();
    $data_group = $data_group->toArray();
    $group_limit = [];
    foreach($data_group as $value){
      $group_limit[]=$value["tanggal"];
    }

    $data = Point::whereIn('tanggal',$group_limit)->where('isdelete','=','0')->orderBy('tanggal')->orderBy('type')->get();
      $result = array();
    foreach ($data->toArray() as $data) {
      $id = $data['versi'].'<br>'.$data['tanggal'];
      if (isset($result[$id])) {
         $result[$id]["point"][] = $data;
         $result[$id]["status_text"] = $data["status_text"];
         $result[$id]["versi"] = $data["versi"];
         $result[$id]["tanggal"] = $data["tanggal"];
         $result[$id]["status"] = $data["status"];
      } else {
         $result[$id]["point"] = array($data);
      }
    };

		$items = ["items"=>$result];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/point/point_luaran/index',array_merge($items,$this->menu));
  }


  public function active($tanggal)
  {
    $aktiv = Point::where('tanggal', str_replace("%20", " ", $tanggal))->update(array('status' => 1));
    Point::where('tanggal', '!=', str_replace("%20", " ", $tanggal))->update(array('status' => 0));

    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengaktifkan konfigurasi Point-Point Luaran dengan nama versi '.$aktiv->versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect("points/pointluaran");
  }

  public function delete(){
    $versi = $this->uri->segment(4);
    $item = Point::where('versi','=',urldecode($versi))->delete();
    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Point-Point Luaran dengan nama versi '.urldecode($versi),'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    redirect($_SERVER['HTTP_REFERER']);
  }

  public function create_paging($model){
    $default_view = 10;
    $page = 0;

    $view_get = $this->input->get("views");
    $page_get = $this->input->get("page");
    if(isset($page_get)) $page = $page_get-1;
    if(isset($view_get)) $default_view = $view_get;

    $default_skip = $page*$default_view;
    $count  = $model->count();
    $total_pages = $count>0? ceil(($count/8)/$default_view):1;
    return ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages];
  }

  public function insert(){
  $versi = $this->input->post("versi");
  $tanggal = $this->input->post('tanggal');

  $array = [];
  $jurni_ketua = $this->input->post("jurni_ketua");
  $jurni_anggota = $this->input->post("jurni_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$jurni_ketua, "anggota"=>$jurni_anggota,"type"=>1,"status"=>0);

  $jurnas_ketua = $this->input->post("jurnas_ketua");
  $jurnas_anggota = $this->input->post("jurnas_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$jurnas_ketua, "anggota"=>$jurnas_anggota,"type"=>2,"status"=>0);

  $jurnatak_ketua = $this->input->post("jurnatak_ketua");
  $jurnatak_anggota = $this->input->post("jurnatak_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$jurnatak_ketua, "anggota"=>$jurnatak_anggota,"type"=>3,"status"=>0);

  $penas_ketua = $this->input->post("penas_ketua");
  $penas_anggota = $this->input->post("penas_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$penas_ketua, "anggota"=>$penas_anggota,"type"=>4,"status"=>0);

  $pemin_ketua = $this->input->post("pemin_ketua");
  $pemin_anggota = $this->input->post("pemin_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$pemin_ketua, "anggota"=>$pemin_anggota,"type"=>5,"status"=>0);

  $buku_ketua = $this->input->post("buku_ketua");
  $buku_anggota = $this->input->post("buku_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$buku_ketua, "anggota"=>$buku_anggota,"type"=>6,"status"=>0);

  $hki_ketua = $this->input->post("hki_ketua");
  $hki_anggota = $this->input->post("hki_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$hki_ketua, "anggota"=>$hki_anggota,"type"=>7,"status"=>0);

  $lula_ketua = $this->input->post("lula_ketua");
  $lula_anggota = $this->input->post("lula_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$lula_ketua, "anggota"=>$lula_anggota,"type"=>8,"status"=>0);

  // if($status=='1'){
  //   $updates = Pengesah::where("isdelete","=","0")->get();
  //   foreach($updates as $d){
  //     $d->status='0';
  //     $d->save();
  //   }
  // }
  // $data = new Pengesah;
  // $data->dosen = $dosen;
  // $data->status = $status;
  // $data->jabatan = $jabatan;
  // $data->save();
  echo json_encode($array);
  Point::insert($array);

  Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Point-Point Luaran dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

  redirect("points/pointluaran");
  }

  public function update()
  {
    $versi = $this->input->post("versi");
    $tanggal = $this->input->post('tanggal');
    $status = $this->input->post('status');

    $jurni_ketua = $this->input->post("jurni_ketua");
    $jurni_anggota = $this->input->post("jurni_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$jurni_ketua, "anggota"=>$jurni_anggota,"type"=>1,"status"=>$status);
    Point::where('type',1)->where('tanggal',$tanggal)->update($array);

    $jurnas_ketua = $this->input->post("jurnas_ketua");
    $jurnas_anggota = $this->input->post("jurnas_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$jurnas_ketua, "anggota"=>$jurnas_anggota,"type"=>2,"status"=>$status);
    Point::where('type',2)->where('tanggal',$tanggal)->update($array);

    $jurnatak_ketua = $this->input->post("jurnatak_ketua");
    $jurnatak_anggota = $this->input->post("jurnatak_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$jurnatak_ketua, "anggota"=>$jurnatak_anggota,"type"=>3,"status"=>$status);
    Point::where('type',3)->where('tanggal',$tanggal)->update($array);

    $penas_ketua = $this->input->post("penas_ketua");
    $penas_anggota = $this->input->post("penas_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$penas_ketua, "anggota"=>$penas_anggota,"type"=>4,"status"=>$status);
    Point::where('type',4)->where('tanggal',$tanggal)->update($array);

    $pemin_ketua = $this->input->post("pemin_ketua");
    $pemin_anggota = $this->input->post("pemin_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$pemin_ketua, "anggota"=>$pemin_anggota,"type"=>5,"status"=>$status);
    Point::where('type',5)->where('tanggal',$tanggal)->update($array);

    $buku_ketua = $this->input->post("buku_ketua");
    $buku_anggota = $this->input->post("buku_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$buku_ketua, "anggota"=>$buku_anggota,"type"=>6,"status"=>$status);
    Point::where('type',6)->where('tanggal',$tanggal)->update($array);

    $hki_ketua = $this->input->post("hki_ketua");
    $hki_anggota = $this->input->post("hki_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$hki_ketua, "anggota"=>$hki_anggota,"type"=>7,"status"=>$status);
    Point::where('type',7)->where('tanggal',$tanggal)->update($array);

    $lula_ketua = $this->input->post("lula_ketua");
    $lula_anggota = $this->input->post("lula_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$lula_ketua, "anggota"=>$lula_anggota,"type"=>8,"status"=>$status);
    Point::where('type',8)->where('tanggal',$tanggal)->update($array);

    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Point-Point Luaran dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect("points/pointluaran");

  }

  public function migrate_jurnal(){
    $this->load->model('luaran_penelitian/Jurnal');
    $this->load->model('Point_log');
    $items = Jurnal::where('isdelete','=','0')->where('isvalidate','=','1')->get();

    $array = [];
    foreach($items as $item){
      $point = Point::active($item->type)->get()->toArray();
      if(!empty($point)) {
          $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => $item->type, "user_id" => $item->dosen, "created_date" => $item->created_at,'jenis'=>1,'jenis_id'=>$item->id);
          if (!empty($item->personil)) {
              $array[] = array("point" => $point[0]['anggota'], "type" => 2, "status" => $item->type, "user_id" => $item->personil, "created_date" => $item->created_at,'jenis'=>1,'jenis_id'=>$item->id);
          }
      }
    }
    Point_log::insert($array);
  }

  public function migrate_bukuajar(){
    $this->load->model('luaran_penelitian/Buku_ajar');
    $this->load->model('Point_log');
    $items = Buku_ajar::where('isdelete','=','0')->where('isvalidate','=','1')->get();
    $array = [];
    foreach($items as $item){
      $point = Point::active(6)->get()->toArray();
      if(!empty($point)) {
          $array[] = array("point" => $point[0]['ketua'], "created_date" => $item->created_at, "type" => 1, "status" => 6, "user_id" => $item->dosen,'jenis'=>2,'jenis_id'=>$item->id);
      }
    }
    Point_log::insert($array);
  }

  public function migrate_forumilmiah(){
    $this->load->model('luaran_penelitian/Forum_ilmiah');
    $this->load->model('Point_log');
    $items = Forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1')->get();
    $array = [];
    foreach($items as $item){
      $type = $item->tingkat + 3;
      $point = Point::active($type)->get()->toArray();
      if(!empty($point) && $item->tingkat < 3) {
          $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => $type, "user_id" => $item->dosen,'jenis'=>3,'jenis_id'=>$item->id,"created_date" => $item->created_at);
          // echo print_r($array);
      }
    }
    Point_log::insert($array);
  }

  public function migrate_hki(){
    $this->load->model('luaran_penelitian/Hki');
    $this->load->model('Point_log');
    $items = Hki::where('isdelete','=','0')->where('isvalidate','=','1')->get();
    $array = [];

    foreach($items as $item){
        $point = Point::active(7)->get()->toArray();
        if(!empty($point)) {
            $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => 7, "user_id" => $item->dosen,'jenis'=>4,'jenis_id'=>$item->id,"created_date" => $item->created_at);
        }
    }
    Point_log::insert($array);
  }

  public function migrate_luaranlain(){
    $this->load->model('luaran_penelitian/Luaran_lain');
    $this->load->model('Point_log');
    $items = Luaran_lain::where('isdelete','=','0')->where('isvalidate','=','1')->get();
    $array = [];

    foreach($items as $item){
      $point = Point::active(8)->get()->toArray();
      if(!empty($point)) {
          $array[] = array("point" => $point[0]['ketua'], "type" => 1, "status" => 8, "user_id" => $item->dosen,'jenis'=>5,'jenis_id'=>$item->id,"created_date" => $item->created_at);
      }
    }
    Point_log::insert($array);
  }

  public function migrate_penelitianmandiri(){
    $this->load->model('luaran_penelitian/Penelitian_hibah');
    $this->load->model('Point_log');
    $this->load->model('Point_penelitian');
    $items = Penelitian_hibah::where('isdelete','=','0')->where('isvalidate','=','1')->get();
    $array = [];

    foreach($items as $item){
      $point = Point_penelitian::active(1)->get()->toArray();
      if(!empty($point)) {
          $array[] = array("point" => $point[0]['ketua'], "created_date" => $item->created_at, "type" => 7, "status" => 6, "user_id" => $item->dosen,'jenis'=>6,'jenis_id'=>$item->id);
      }
    }
    Point_log::insert($array);
  }
  
  public function migrate_penelitianinternal(){
    $this->load->model('penelitian');
    $this->load->model('Point_log');
    $this->load->model('Point_penelitian');
    $this->load->model('Penelitian_anggota');
    $this->load->model('penelitian_laporan');
    $items = Penelitian::where('isdelete','=','0')->where('isvalid','=','1')->where('status','=','4')->whereHas('penelitian_laporan',function($q){
      $q->where('status','=','1');
     })->get();
    $array = [];

    foreach($items as $item){
      $point = Point_penelitian::active(2)->get()->toArray();
      if(!empty($point)) {
          $array[] = array("point" => $point[0]['ketua'], "created_date" => $item->created_at, "type" => 1, "status" => 1, "user_id" => $item->dosen,'jenis'=>7,'jenis_id'=>$item->id);
          $anggotas = $item->anggota()->get();
          foreach($anggotas as $anggota){
            $array[] = array("point" => $point[0]['anggota'], "created_date" => $item->created_at, "type" => 2, "status" => 1, "user_id" => $anggota->anggota,'jenis'=>7,'jenis_id'=>$item->id);
          }
      }
    }
    Point_log::insert($array);
  }

}
?>
