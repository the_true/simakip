<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Klasifikasitotal extends MY_BaseController {


  private $menu = array("p"=>"active","p2"=>"active");
  private $id_jenis = array(0,1,2,3,4,5,6,7,8,9);
  public function __construct() {
    $config = [
      'functions' => ['anchor','set_value','set_select'],
      'functions_safe' => ['validation_errors_array'],
    ];
    parent::__construct($config);
    $this->load->model("Klasifikasi_total");
  }

  public function index(){
    $data_group = Klasifikasi_total::select('versi','tanggal','status')->where('isdelete', '=', '0')->whereIn('type',$this->id_jenis)
    ->orderBy('tanggal','ASC');
		$info = $this->create_paging($data_group);
		$data_group = $data_group->take($info["limit"]*10)->skip($info["skip"])->get();
    $data_group = $data_group->toArray();
    $group_limit = [];
    foreach($data_group as $value){
      $group_limit[]=$value["tanggal"];
    }

    $data = Klasifikasi_total::whereIn('tanggal',$group_limit)->where('isdelete','=','0')->orderBy('tanggal')->orderBy('type')->get();
      $result = array();
    foreach ($data->toArray() as $data) {
      $id = $data['versi'].'<br>'.$data['tanggal'];
      if (isset($result[$id])) {
         $result[$id]["point"][] = $data;
         $result[$id]["status_text"] = $data["status_text"];
         $result[$id]["versi"] = $data["versi"];
         $result[$id]["tanggal"] = $data["tanggal"];
         $result[$id]["status"] = $data["status"];
      } else {
         $result[$id]["point"] = array($data);
      }
    };

		$items = ["items"=>$result];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/point/klasifikasi_total/index',array_merge($items,$this->menu));
  }

  public function active($tanggal)
  {
    $aktiv = Klasifikasi_total::where('versi', str_replace("%20", " ", $tanggal))->update(array('status' => 1));
    Klasifikasi_total::where('versi', '!=', str_replace("%20", " ", $tanggal))->update(array('status' => 0));

    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengaktifkan konfigurasi Point-Klasifikasi Total dengan nama versi '.$aktiv->versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect($_SERVER['HTTP_REFERER']);
  }

  public function delete(){
    $versi = $this->uri->segment(4);
    $item = Klasifikasi_total::where('versi','=',urldecode($versi))->delete();
    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Point-Klasifikasi Total dengan nama versi '.urldecode($versi),'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    redirect($_SERVER['HTTP_REFERER']);
  }

  public function insert(){
  $versi = $this->input->post("versi");
  $tanggal = $this->input->post('tanggal');

  $array = [];

  $bintangh_ketua = $this->input->post("bintangh_ketua");
  $bintangh_anggota = $this->input->post("bintangh_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintangh_ketua, "anggota"=>$bintangh_anggota,"type"=>0,"status"=>0);

  $bintang1_ketua = $this->input->post("bintang1_ketua");
  $bintang1_anggota = $this->input->post("bintang1_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang1_ketua, "anggota"=>$bintang1_anggota,"type"=>1,"status"=>0);

  $bintang1h_ketua = $this->input->post("bintang1h_ketua");
  $bintang1h_anggota = $this->input->post("bintang1h_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang1h_ketua, "anggota"=>$bintang1h_anggota,"type"=>2,"status"=>0);

  $bintang2_ketua = $this->input->post("bintang2_ketua");
  $bintang2_anggota = $this->input->post("bintang2_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang2_ketua, "anggota"=>$bintang2_anggota,"type"=>3,"status"=>0);

  $bintang2h_ketua = $this->input->post("bintang2h_ketua");
  $bintang2h_anggota = $this->input->post("bintang2h_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang2h_ketua, "anggota"=>$bintang2h_anggota,"type"=>4,"status"=>0);

  $bintang3_ketua = $this->input->post("bintang3_ketua");
  $bintang3_anggota = $this->input->post("bintang3_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang3_ketua, "anggota"=>$bintang3_anggota,"type"=>5,"status"=>0);

  $bintang3h_ketua = $this->input->post("bintang3h_ketua");
  $bintang3h_anggota = $this->input->post("bintang3h_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang3h_ketua, "anggota"=>$bintang3h_anggota,"type"=>6,"status"=>0);

  $bintang4_ketua = $this->input->post("bintang4_ketua");
  $bintang4_anggota = $this->input->post("bintang4_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang4_ketua, "anggota"=>$bintang4_anggota,"type"=>7,"status"=>0);

  $bintang4h_ketua = $this->input->post("bintang4h_ketua");
  $bintang4h_anggota = $this->input->post("bintang4h_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang4h_ketua, "anggota"=>$bintang4h_anggota,"type"=>8,"status"=>0);

  $bintang5_ketua = $this->input->post("bintang5_ketua");
  $bintang5_anggota = $this->input->post("bintang5_anggota");
  $array[] = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang5_ketua, "anggota"=>$bintang5_anggota,"type"=>9,"status"=>0);

    echo json_encode($array);
    Klasifikasi_total::insert($array);

  Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Point-Klasifikasi Total dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
  redirect("points/klasifikasitotal");
  }

  public function update()
  {
    $versi = $this->input->post("versi");
    $tanggal = $this->input->post('tanggal');
    $status = $this->input->post('status');


    $bintang1_ketua = $this->input->post("bintang1_ketua");
    $bintang1_anggota = $this->input->post("bintang1_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang1_ketua, "anggota"=>$bintang1_anggota,"type"=>1,"status"=>$status);
    Klasifikasi_total::where('type',1)->where('tanggal',$tanggal)->update($array);

    $bintang1h_ketua = $this->input->post("bintang1h_ketua");
    $bintang1h_anggota = $this->input->post("bintang1h_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang1h_ketua, "anggota"=>$bintang1h_anggota,"type"=>2,"status"=>$status);
    Klasifikasi_total::where('type',2)->where('tanggal',$tanggal)->update($array);

    $bintang2_ketua = $this->input->post("bintang2_ketua");
    $bintang2_anggota = $this->input->post("bintang2_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang2_ketua, "anggota"=>$bintang2_anggota,"type"=>3,"status"=>$status);
    Klasifikasi_total::where('type',3)->where('tanggal',$tanggal)->update($array);

    $bintang2h_ketua = $this->input->post("bintang2h_ketua");
    $bintang2h_anggota = $this->input->post("bintang2h_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang2h_ketua, "anggota"=>$bintang2h_anggota,"type"=>4,"status"=>$status);
    Klasifikasi_total::where('type',4)->where('tanggal',$tanggal)->update($array);

    $bintang3_ketua = $this->input->post("bintang3_ketua");
    $bintang3_anggota = $this->input->post("bintang3_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang3_ketua, "anggota"=>$bintang3_anggota,"type"=>5,"status"=>$status);
    Klasifikasi_total::where('type',5)->where('tanggal',$tanggal)->update($array);

    $bintang3h_ketua = $this->input->post("bintang3h_ketua");
    $bintang3h_anggota = $this->input->post("bintang3h_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang3h_ketua, "anggota"=>$bintang3h_anggota,"type"=>6,"status"=>$status);
    Klasifikasi_total::where('type',6)->where('tanggal',$tanggal)->update($array);

    $bintang4_ketua = $this->input->post("bintang4_ketua");
    $bintang4_anggota = $this->input->post("bintang4_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang4_ketua, "anggota"=>$bintang4_anggota,"type"=>7,"status"=>$status);
    Klasifikasi_total::where('type',7)->where('tanggal',$tanggal)->update($array);

    $bintang4h_ketua = $this->input->post("bintang4h_ketua");
    $bintang4h_anggota = $this->input->post("bintang4h_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang4h_ketua, "anggota"=>$bintang4_anggota,"type"=>8,"status"=>$status);
    Klasifikasi_total::where('type',8)->where('tanggal',$tanggal)->update($array);

    $bintang5_ketua = $this->input->post("bintang5_ketua");
    $bintang5_anggota = $this->input->post("bintang5_anggota");
    $array = array("versi"=>$versi,"tanggal"=>$tanggal,"ketua"=>$bintang5_ketua, "anggota"=>$bintang5_anggota,"type"=>9,"status"=>$status);
    Klasifikasi_total::where('type',9)->where('tanggal',$tanggal)->update($array);

    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Point-Klasifikasi Total dengan nama versi '.$versi,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    redirect("points/klasifikasitotal");

  }

  public function create_paging($model){
    $default_view = 10;
    $page = 0;

    $view_get = $this->input->get("views");
    $page_get = $this->input->get("page");
    if(isset($page_get)) $page = $page_get-1;
    if(isset($view_get)) $default_view = $view_get;

    $default_skip = $page*$default_view;
    $count  = $model->count();
    $total_pages = $count>0? ceil(($count/10)/$default_view):1;
    return ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages];
  }


}
?>
