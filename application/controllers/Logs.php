<?php
defined('BASEPATH') or exit('No direct script access allowed');
include_once "application/core/MY_BaseController.php";
class Logs extends MY_BaseController
{

    /**
     * @category Libraries
     * @package  CodeIgniter 3.0
     * @author   Yp <purwantoyudi42@gmail.com>
     * @link     https://timexstudio.com
     * @license  Protected
     */
    private $menu = array("logs" => "active", "aaa" => "active");
    public function __construct()
    {
        $config = [
            'functions' => ['anchor', 'set_value', 'set_select'],
            'functions_safe' => ['validation_errors_array', 'form_open_multipart'],
        ];
        parent::__construct($config, false);
        // $this->load->helper('celery');
        $this->load->model('Log_activity');
        $this->load->model('Dosen');
    }

    public function index()
    {
        $item = Log_activity::orderBy('created_at', 'DESC');
        $item = $this->filter($item);
        $info = $this->create_paging($item);
        $item = $item->take($info["limit"])->skip($info["skip"])->get();
        $item->load('dosen');
        $data = [];
        $data["items"] = $item->toArray();

        $this->twig->display("log/index", array_merge($data, $this->menu, $info));
    }

    public function filter($model)
    {
        $check = $this->input->get('tanggal_mulai');
        if ($check) {
            $model = $model->where('created_at', '>=', $check);
        }

        $check = $this->input->get('tanggal_selesai');
        if ($check) {
            $model = $model->where('created_at', '<=', $check);
        }

        $check = $this->input->get('aksi');
        if ($check) {
            $model = $model->where('action', '=', $check);
        }

        $check = $this->input->get('menu');
        if ($check) {
            $model = $model->where('type', '=', ((int) $check) - 1);
        }

        $nama = $this->input->get('nama');
        if ($nama) {
            $model = $model->whereHas('dosen', function ($q) {
                $q->where('nama', 'LIKE', '%' . $this->input->get('nama') . '%');
            });
        }

        $nidn = $this->input->get('nidn');
        if ($nidn) {
            $model = $model->whereHas('dosen', function ($q) {
                $q->where('nidn', '=', $this->input->get('nidn'));
            });
        }

        if (roles(["Operator LPPM"])) {
            $model = $model->whereIn('type', [8, 4]);
        }

        return $model;
    }

}
