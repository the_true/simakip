<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Beranda extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p3"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model("Kontak");
        $this->load->model("Tautan");
    }	
	public function index()
	{
		$this->twig->display('pengaturan/beranda/index');
	}

	public function kontak(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('isi', 'Alamat Kontak', 'required');
		$kontak = Kontak::find(1);

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/beranda/kontak',array_merge($kontak->toArray(),$this->menu));
        }else{ 
        	if ($this->submit_kontak())
        		redirect('beranda/kontak');
		}	
	}

	public function tautan(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama Tautan', 'required');
		$type = $this->input->post('type');
		if($type==1)
			$this->form_validation->set_rules('url', 'URL', 'required');
		else{
			if (empty($_FILES['file']['name']))
				$this->form_validation->set_rules('file', 'Berkas', 'required');
		}
		$this->form_validation->set_rules('nama', 'Nama Tautan', 'required');
		$kontak = Tautan::where('isdelete','=','0')->get();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/beranda/tautan',array_merge($this->menu,array("items"=>$kontak->toArray())));
        }else{ 
        	if ($this->submit_tautan())
        		redirect('beranda/tautan');
		}
	}

	private function submit_tautan(){
		$this->generate_folder('uploads/tautans');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/tautans'),
			'allowed_types' => 'pdf',
			'max_size' => 2000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$type = $this->input->post('type');
		$nama = $this->input->post('nama');
		if($type==0){
			$file_field = "file";
			$uploaded = $this->upload->do_upload($file_field);
		}
		$tautan = new Tautan;
		$tautan->nama = $nama;
		$tautan->type = $type;
		if($type==0){
			$tautan->berkas =  $this->upload->data('file_name');
		}else{
			$tautan->url = $this->input->post('url');
		}
		$tautan->save();
		return TRUE;
	}

    public function tautan_delete(){
        $id = $this->uri->segment(3);
        $item = Tautan::find($id);
        $item->isdelete = 1;
        $item->save();
        redirect('beranda/tautan');
    }


	private function submit_kontak(){
		$isi = $this->input->post('isi');
		$kontak = Kontak::find(1);
		$kontak->isi = $isi;
		$kontak->save();
		return TRUE;
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}
}
