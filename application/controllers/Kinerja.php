<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Kinerja extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p1"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Jenis_hki');
        $this->load->model('Jenis_luaran');
        $this->load->model('Tahun_kegiatan');
        $this->load->model('Sumberdana');
        $this->load->model('Subsumberdana');
    }	
	public function index(){
        // $this->load->helper('form');
        // $jenis_hki = Jenis_hki::all();
        // $jenis_luaran = Jenis_luaran::all();
        // $tahun_kegiatan = Tahun_kegiatan::all();
        // $data = array("jenis_hki"=>$jenis_hki->toArray(),"jenis_luaran"=>$jenis_luaran->toArray(),"tahun_kegiatan"=>$tahun_kegiatan->toArray());
		$this->twig->display('pengaturan/kinerja/index',$this->menu);
	}

	public function jenishki(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_jenishki','%s already exists');
		$this->form_validation->set_rules('nama', 'Jenis HKI', 'required|callback_validate_jenishki');
        if ($this->form_validation->run() == FALSE){
        	$item = Jenis_hki::where('isdelete', '=', '0')->get();
			$this->twig->display('pengaturan/kinerja/jenis_hki',array_merge(array("jenis_hki"=>$item->toArray()),$this->menu));
        }else{ 
        	$jenis_hki = new Jenis_hki;
        	$jenis_hki->nama = $this->input->post('nama');
        	$jenis_hki->save();
        	$item = Jenis_hki::where('isdelete', '=', '0')->get();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Luaran Penelitian-jenis HKI baru dengan nama '.$jenis_hki->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        	redirect('kinerja/jenishki');
		}
	}

    public function jenishki_edit(){
        $id = $this->uri->segment(3);
        $nama = $this->input->post('nama');
        $jenis_hki = Jenis_hki::find($id);
        $jenis_hki->nama = $nama;
        $jenis_hki->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Luaran Penelitian-jenis HKI dengan nama '.$jenis_hki->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/jenishki');
    }

    public function validate_jenishki($str){
        $item = Jenis_hki::where('nama','=',$str)->where('isdelete', '=', '0')->count();
        if($item>0)
            return False;
        return True;
    }

    public function jenis_hki(){
        $id = $this->uri->segment(3);
        $item = Jenis_hki::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Luaran Penelitian-jenis HKI dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/jenishki');
    }

	public function jenisluaran(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_jenisluaran','%s already exists');
		$this->form_validation->set_rules('nama', 'Jenis Luaran', 'required|callback_validate_jenisluaran');
        if ($this->form_validation->run() == FALSE){
        	$item = Jenis_luaran::where('isdelete', '=', '0')->get();
			$this->twig->display('pengaturan/kinerja/jenis_luaran',array_merge($this->menu,array("jenis_luaran"=>$item->toArray())));
        }else{ 
        	$jenis_luaran = new Jenis_luaran;
        	$jenis_luaran->nama = $this->input->post('nama');
        	$jenis_luaran->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Luaran Penelitian-jenis Luaran baru dengan nama '.$jenis_luaran->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        	redirect('kinerja/jenisluaran/');
		}
	}

    public function jenisluaran_edit(){
        $id = $this->uri->segment(3);
        $nama = $this->input->post('nama');
        $jenis_luaran = Jenis_luaran::find($id);
        $jenis_luaran->nama = $nama;
        $jenis_luaran->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Luaran Penelitian-jenis Luaran dengan nama '.$jenis_luaran->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/jenisluaran');
    }

    public function jenis_luaran(){
        $id = $this->uri->segment(3);
        $item = Jenis_luaran::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Luaran Penelitian-jenis Luaran dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/jenisluaran');
    }

    public function validate_jenisluaran($str){
        $item = Jenis_luaran::where('nama','=',$str)->where('isdelete', '=', '0')->count();
        if($item>0)
            return False;
        return True;
    }


	public function tahunkegiatan(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_tahunkegiatan','%s already exists');
		$this->form_validation->set_rules('tahun', 'Tahun Kegiatan', 'required|callback_validate_tahunkegiatan');
        if ($this->form_validation->run() == FALSE){
        	$item = Tahun_kegiatan::where('isdelete', '=', '0')->get();
			$this->twig->display('pengaturan/kinerja/tahun_kegiatan',array_merge($this->menu,array("tahun_kegiatan"=>$item->toArray())));
        }else{ 
        	$tahun_kegiatan = new Tahun_kegiatan;
        	$tahun_kegiatan->tahun = $this->input->post('tahun');
        	$tahun_kegiatan->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Luaran Penelitian-Tahun Kegiatan baru dengan nama '.$tahun_kegiatan->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('kinerja/tahunkegiatan/');
		}
	}
	public function sumberdana(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_sumberdana','%s already exists');
		$this->form_validation->set_rules('sumberdana', 'Sumber Dana', 'required|callback_validate_sumberdana');
		if ($this->form_validation->run() == FALSE){
        	$item = Sumberdana::where('isdelete', '=', '0')->get();
			$this->twig->display('pengaturan/kinerja/sumberdana',array_merge($this->menu,array("sumberdana"=>$item->toArray())));
        }else{ 
        	$sumberdana = new Sumberdana;
        	$sumberdana->sumberdana = $this->input->post('sumberdana');
        	$sumberdana->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Luaran Penelitian-SumberDana baru dengan sumberdana '.$sumberdana->sumberdana." - ".$sumberdana->sub_sumberdana,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('kinerja/sumberdana/');
		}
	}
	public function subsumberdana(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('validate_subsumberdana','%s already exists');
		$this->form_validation->set_rules('subsumberdana', 'Sub Sumber Dana', 'required|callback_validate_subsumberdana');
		$this->form_validation->set_rules('sumberdana', 'Sumber Dana', 'required|callback_validate_subsumberdana');
        if ($this->form_validation->run() == FALSE){
            $item = Subsumberdana::where('isdelete', '=', '0')->get();
            $item->load('sumberdana');
            $items_sumber = Sumberdana::where('isdelete', '=', '0')->get();
			$this->twig->display('pengaturan/kinerja/subsumberdana',array_merge($this->menu,array("sumberdana"=>$item->toArray(),"items_sumber"=>$items_sumber->toArray())));
        }else{ 
        	$sumberdana = new Subsumberdana;
        	$sumberdana->subsumberdana = $this->input->post('subsumberdana');
        	$sumberdana->sumberdana = $this->input->post('sumberdana');
        	$sumberdana->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Luaran Penelitian-SumberDana baru dengan sumberdana '.$sumberdana->sumberdana." - ".$sumberdana->subsumberdana,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
            redirect('kinerja/subsumberdana/');
		}
	}

    public function tahunkegiatan_edit(){
        $id = $this->uri->segment(3);
        $tahun = $this->input->post('tahun');
        $tahun_kegiatan = Tahun_kegiatan::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Luaran Penelitian-Tahun Kegiatan dengan nama '.$tahun_kegiatan->tahun.' menjadi '.$tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $tahun_kegiatan->tahun = $tahun;
        $tahun_kegiatan->save();
        redirect('kinerja/tahunkegiatan');
    }
    public function sumberdana_edit(){
        $id = $this->uri->segment(3);
        $postsumberdana = $this->input->post('sumberdana');
        // $sub_sumberdana = $this->input->post('sub_sumberdana');
        $sumberdana = Sumberdana::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Luaran Penelitian-Sumber Dana dengan nama '.$sumberdana->sumberdana.' menjadi '.$sumberdana,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $sumberdana->sumberdana = $postsumberdana;
        // $sumberdana->sub_sumberdana = $sub_sumberdana;
        // print_r($sumberdana);die();
        $sumberdana->save();
        redirect('kinerja/sumberdana');
    }
    
    public function subsumberdana_edit(){
        $id = $this->uri->segment(3);
        $postsumberdana = $this->input->post('sumberdana');
        $subsumberdana = $this->input->post('subsumberdana');
        $sumberdana = Subsumberdana::find($id);
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Luaran Penelitian-Sumber Dana dengan nama '.$sumberdana->sumberdana." - ".$sumberdana->subsumberdana.' menjadi '.$sumberdana." - ".$subsumberdana,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        $sumberdana->sumberdana = $postsumberdana;
        $sumberdana->subsumberdana = $subsumberdana;
        // print_r($sumberdana);die();
        $sumberdana->save();
        redirect('kinerja/subsumberdana');
    }

    public function tahun_kegiatan(){
        $id = $this->uri->segment(3);
        $item = Tahun_kegiatan::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Luaran Penelitian-Tahun Kegiatan dengan nama '.$item->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/tahunkegiatan');
    }
    public function sumberdana_delete(){
        $id = $this->uri->segment(3);
        $item = Sumberdana::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Luaran Penelitian-Tahun Kegiatan dengan nama '.$item->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/sumberdana');
    }

    public function subsumberdana_delete(){
        $id = $this->uri->segment(3);
        $item = Subsumberdana::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Luaran Penelitian-Tahun Kegiatan dengan nama '.$item->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
        redirect('kinerja/subsumberdana');
    }

    public function validate_tahunkegiatan($str){
        $item = Tahun_kegiatan::where('tahun','=',$str)->where('isdelete', '=', '0')->count();
        if($item>0)
            return False;
        return True;
    }
    public function validate_sumberdana($str){
        $item = Sumberdana::where('sumberdana','=',$str)->where('isdelete', '=', '0')->count();
        if($item>0)
            return False;
        return True;
    }
    public function validate_subsumberdana($str){
        $item = Subsumberdana::where('subsumberdana','=',$str)->where('isdelete', '=', '0')->count();
        if($item>0)
            return False;
        return True;
    }
}
