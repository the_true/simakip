<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Migration extends MY_BaseController {

    public function index(){
        $this->load->library('migration');

        if ($this->migration->current() === FALSE)
        {
                show_error($this->migration->error_string());
        }
    }

    public function migrate_target_luaran(){
        $this->load->model("penelitian");
        $this->load->model("penelitian_target_luaran");
        $this->load->model("target_luaran");
        
        $items = Penelitian::where("isdelete","=","0")->where("isvalid","=","1")->where("target","<>","0")->get();
        $array = [];
        foreach($items as $item){
            if(!empty($item->target)){
                Penelitian_target_luaran::where("penelitian", "=", $item->id)->delete();
                $array[]= array("penelitian"=>$item->id, "target_luaran"=>$item->target);
            }
        }
        Penelitian_target_luaran::insert($array);
        echo $items->toJson(); die;
    }
}