<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Pengumuman extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/

    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model('News');
        $this->load->model('News_meta');
        $this->load->model('Tahun_kegiatan');
    }	
	public function index(){       
        $default_view = 10;
        $page = 0;

        $tahun_get = $this->input->get("tahun");
        $view_get = $this->input->get("views");
        $page_get = $this->input->get("page");

        $news = News::where('isdelete','=','0');
        if(isset($page_get)) $page = $page_get-1;
        if(isset($tahun_get) && $tahun_get!='') $news->whereRaw("extract(YEAR from created_at) = ?",[$tahun_get]);
        if(isset($view_get)) $default_view = $view_get;
        $default_skip = $page*$default_view;
        $count = $news->count();
        $news = $news->orderBy('created_at','DESC')->take($default_view)->skip($default_skip)->get();
        $news->load("news_meta");
        $data = array("items"=>$news->toArray());
        $tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
        $data['tahun'] = $tahun->toArray();
        $data['views'] = $default_view;
        $data['tahuns'] = $tahun_get;
        $data['page'] = $page+1;
        $data['total_pages'] = $count>0? ceil($count/$default_view):1;
		$this->twig->display('pengumuman/index',$data);
	}

    public function add(){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'Judul', 'required');

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengumuman/add');
        }else{ 
            if ($this->submit()){
                redirect('pengumuman');
            }
                //$this->twig->display('pengumuman/add');
        }
    }

    private function submit(){
        $this->generate_folder('uploads/pengumuman');  
        $this->load->library('upload');
        

        $title = $this->input->post('judul');
        $index = (int) $this->input->post('news_meta');
        $news_meta = array();
        for($i=0; $i<$index;$i++){
            $check = $this->input->post('body_'.$i);
            if(!$check) continue;
            $type = $this->input->post('type_'.$i);
            if((int)$type==0){
                $path = $this->uploadFile("file_".$i);
                $body = $this->input->post('body_'.$i);
                $news_meta[] = array("body"=>$body,"path"=>$path,"type"=>$type);
            }else{
                $body = $this->input->post('body_'.$i);
                $url =  $this->input->post('url_'.$i);
                $news_meta[] = array("body"=>$body,"url"=>$url,"type"=>$type);
            }
        }

        $news = new News;
        $news->title = $title;
        // $news->created = date("Y-m-d H:i:s");
        $news->save();

        foreach($news_meta as $nan){
            $nm = new News_meta;
            $nm->body = $nan["body"];
            $nm->type = $nan["type"];
            $nm->news = $news->id;
            if($nan["type"]==0) $nm->path = $nan["path"];
            else $nm->url = "http://".$nan["url"];
            $nm->save();
        }

        $first_four_title = implode(' ', array_slice(explode(' ', $title), 0, 4));
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Pengumuman dengan judul '.$first_four_title,'type'=>'5','created_at'=>date('Y-m-d H:i:s')]);

        return TRUE;
    }

    private function uploadFile($file_field){
        $filename = $this->gen_uuid();
        $upload_config = [
            'file_name' => $filename,
            'upload_path' => realpath(APPPATH . '../uploads/pengumuman'),
            'allowed_types' => '*',
            'max_size' => 100000,
            'file_ext_tolower' => TRUE
        ];
        $this->upload->initialize($upload_config);
        $uploaded = $this->upload->do_upload($file_field);
        if ($uploaded){
            return $this->upload->data("file_name");
        }else{
            return "upload failed";
        }
    }

    public function edit(){
        $data = [];
        $id = $this->uri->segment(3);
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        if ($this->form_validation->run() == FALSE){
            $item = News::find($id);
            $item->load('news_meta');
            $data["items"] = $item->toArray();
            $this->twig->display('pengumuman/edit',$data);
        }else{ 
            if ($this->update($id))
                redirect('pengumuman/');
        }
    }

    private function update(){
        $this->generate_folder('uploads/pengumuman');  
        $this->load->library('upload');
        
        $title = $this->input->post('judul');
        $index = (int) $this->input->post('news_meta');
        $news_meta = array();
        for($i=1; $i<=$index;$i++){
            $check = $this->input->post('body_'.$i);
            if(!$check) continue;
            $type = $this->input->post('type_'.$i);
            if((int)$type==0){
                $data = [];
                $path = $this->uploadFile("file_".$i);
                $body = $this->input->post('body_'.$i);
                $id = $this->input->post('id_'.$i);
                $data["body"] = $body;
                $data["path"] = $path;
                $data["type"] = $type;
                if($id) $data["id"] = $id;
                $news_meta[] = $data;
            }else{
                $data = [];
                $body = $this->input->post('body_'.$i);
                $url =  $this->input->post('url_'.$i);
                $id = $this->input->post('id_'.$i);
                $data["body"] = $body;
                $data["url"] = $url;
                $data["type"] = $type;
                if($id) $data["id"] = $id;
                $news_meta[] = $data;
            }
        }


        $news = News::find($this->uri->segment(3));
        $news->title = $title;
        $news->save();

        foreach($news_meta as $nan){
           $nm = new News_meta;
           if(array_key_exists('id', $nan)){
                $nm = News_meta::find($nan["id"]);
                echo "berid<br>";
           }else{
                echo "tidak id<br>";
           }
            $nm->body = $nan["body"];
            $nm->type = $nan["type"];
            $nm->news = $news->id;
            if($nan["type"]==0 && $nan["path"]!="upload failed") $nm->path = $nan["path"];
            else if ($nan["type"]==1) $nm->url = "http://".$nan["url"];
            $nm->save();
        }

        $deleted = $this->input->post("deleted");
        foreach($deleted as $del){
             $nm = News_meta::find($del);
             $nm->delete();
        }

        $first_four_title = implode(' ', array_slice(explode(' ', $title), 0, 4));
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Pengumuman dengan judul '.$first_four_title,'type'=>'5','created_at'=>date('Y-m-d H:i:s')]);


        return TRUE;


    }

    public function delete(){
        $id = $this->uri->segment(3);
        $item = News::find($id);
        $item->isdelete = 1;
        $item->save();

        $first_four_title = implode(' ', array_slice(explode(' ', $item->title), 0, 4));
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Pengumuman dengan judul '.$first_four_title,'type'=>'5','created_at'=>date('Y-m-d H:i:s')]);

        redirect('pengumuman/');
    }

    private function generate_folder($folder_name){
        if(!is_dir($folder_name))
        {
           mkdir($folder_name,0777, true);
        }
    }
}
