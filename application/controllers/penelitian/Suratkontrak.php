<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Suratkontrak extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mp"=>"active","mp6"=>"active");
  public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select','angka_huruf','hitung_persen','format_rupiah','hitung_rupiah','tanggaltoformat'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,false);
				$this->load->model('Surat_kontrak');
				$this->load->model('Batasan_dana');
				$this->load->model('Penelitian');
				$this->load->model('Dosen');
				$this->load->model('Batch');
				$this->load->model('Batch_penelitian');
				$this->load->model('Tahun_kegiatan');
	      		$this->load->model('Penelitian_laporan');
				$this->load->model('Penelitian_review');
				$this->load->model('Jenis_penelitian');
				$this->load->model('Format_nomor');
				$this->load->model('Persetujuan');
				$this->load->model('Tembusan');
				$this->load->model('Surat');
				$this->load->model("Batch_lists");
  }
	public function index()
	{
		$data=[];
		$item = Surat_kontrak::where("isdelete","=","0")->with('penelitian')->with('penelitian.penelitian_review_inselected')
		->with('penelitian.tahun_kegiatan')
		->with('penelitian.batch')->with('penelitian.batch.batch_penelitian')->with('penelitian.batch.batch_penelitian.tahun');
		$item = $this->filter($item);
		if($this->is_login()['akses']['nama']=='Dosen'){
			$item->where(function($q){
				$q->where('dosen','=',$this->is_login()['id']);
			});
		}
		$penelitian = Penelitian::where('status','=','4')->doesntHave('surat_kontrak');
		$penelitian = $penelitian->where(function($q){
		 				$q->where('dosen','=',$this->is_login()['id']);
		 			});
		$penelitian = $penelitian->with("penelitian_review_inselected")->get();
		// echo $penelitian->toJson();
		// die;
		$info = $this->create_paging($item);
		$item = $item->orderBy('id','DESC')->take($info["limit"])->skip($info["skip"])->get();

		$batasan = Batasan_dana::where("isdelete","=","0")->where("status","=","1")->first();
		$tahun  = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		// $item->load('penelitian.batch');
		$item->load('dosen');
		$data["items"] = $item->toArray();
		$data["penelitian"] = $penelitian->toArray();
		$data["batasan"] = isset($batasan) ? $batasan->toArray():[];
		$this->load->helper('suratkontrak');
		$data["formatnomor"] = sk_formatnomor1('#FORMATSURAT1');
		$data["aktivasi"] = check_aktivasi();
		$data["nowyear"] = date('Y');
		$data["tahun"] = $tahun->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$this->twig->display('suratkontrak/index', array_merge($data,$info,$this->menu));
	}

	private function filter($item){
		$nomor = $this->input->get('nomor');
		if($nomor) $item = $item->where('nomor','=',$nomor);

		$tahun_penelitian = $this->input->get('tahun_penelitian');
		if($tahun_penelitian) $item = $item->whereHas('penelitian',function($q){
			$q->where('tahun_kegiatan','=',$this->input->get('tahun_penelitian'));
		});

		$dosen = $this->input->get('dosen');
		if($dosen) $item = $item->whereHas('dosen',function($q){
			$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
		});

		$batch = $this->input->get('batch');
		$judul = $this->input->get('judul');
		if($batch || $judul) $item = $item->whereHas('penelitian',function($q) use ($batch){
			$batch = $this->input->get('batch');
			if($batch) $q->whereHas('batch',function($qs) use ($batch){
				$qs->where('batch_lists','=',$batch);
			});
			$judul = $this->input->get('judul');
			if($judul) $q->where('judul','LIKE',"%$judul%");
		});



		$status = $this->input->get('status');
		if($status) $item = $item->where('status','=',(int)$status-1);

		return $item;
	}

	public function nomor(){
		$id = $this->uri->segment(3);
		$nomor = $this->input->post("nomor");
		$data = Surat_kontrak::find($id);
		$data->nomor = $nomor;
		$data->save();

		if($data->penelitian()->count()>=1){
			$penelitian = $data->penelitian()->first();
			$jenis_penelitian = $penelitian->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menginput nomor Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);
		}
		$this->session->set_flashdata('success', 'Input nomor kontrak berhasil!');
		redirect('suratkontrak/');
	}


	public function validate(){
		$id = $this->uri->segment(3);
		$note = $this->input->post("note");
		$isvalid= $this->input->post("isvalid");
		$data = Surat_kontrak::find($id);
		$data->isvalidate = $isvalid;
		$data->status = (int)$isvalid==1?3:2;
		$data->note = $note;
		$data->save();
		$pengusul = $data->dosen()->first()->toArray();
		$penelitian = $data->penelitian()->first()->toArray();
		if((int)$isvalid==1){
			//valid
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$penelitian["judul"],"alasan"=>$item->note,"type"=>"penelitian"];
			celery()->PostTask('tasks.suratkontrak_valid',array(base_url(),$data_email));

			$penelitian = $data->penelitian()->first();
			$jenis_penelitian = $penelitian->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memvalidasi Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);

		}else{
			//invalid
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$penelitian["judul"],"alasan"=>$item->note,"type"=>"penelitian"];
			celery()->PostTask('tasks.suratkontrak_invalid',array(base_url(),$data_email));

			$penelitian = $data->penelitian()->first();
			$jenis_penelitian = $penelitian->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menolak Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);

		}
		$this->session->set_flashdata('success', 'Validasi surat kontrak berhasil');
		redirect('suratkontrak/');
	}



	public function insert(){
		if(!role('Dosen')){
			$this->twig->display('404');
			return;
		}
		$dosen = $this->is_login()["id"];
		$id = $this->input->post("penelitian");
		$penelitian = Penelitian::find($id);
		$penelitian->load('penelitian_review_inselected');
		$penelitian = $penelitian->toArray();
		$dana = $this->input->post("dana");
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_selesai = $this->input->post('tanggal_selesai');

		$data = new Surat_kontrak;
		$data->dosen = $dosen;
		$data->penelitian = $id;
		$data->dana = (float)$dana*100/(float)$penelitian["penelitian_review_inselected"][0]["rekomendasi"];
		$data->tanggal_mulai = formattotanggal($tanggal_mulai);
		$data->tanggal_selesai = formattotanggal($tanggal_selesai);

		$format = Format_nomor::where("status","=","1")->where("isdelete","=","0")->first();
		$data->format_nomor=$format->id;

		$peran1 = Persetujuan::where('isdelete','=','0')->where('status','=','1')->where('nomor','=','1')->first();
		$data->persetujuan1=$peran1->id;

		$peran2 = Persetujuan::where('isdelete','=','0')->where('status','=','1')->where('nomor','=','2')->first();
		$data->persetujuan2=$peran2->id;

		$batasan = Batasan_dana::where("isdelete","=","0")->where("status","=","1")->first();
		$data->batasan_dana=$batasan->id;

		$tem = Tembusan::where('isdelete','=','0')->where('status','=','1')->first();
		$data->tembusan = $tem->id;

		$surat1 = Surat::where("isdelete","=","0")->where("nomor","=",'1')->where("status","=","1")->first();
		$data->surat1=$surat1->id;

		$surat2 = Surat::where("isdelete","=","0")->where("nomor","=",'2')->where("status","=","1")->first();
		$data->surat2=$surat2->id;



		// echo print_r($data);die;
		$data->save();

		$penelitian = $data->penelitian()->first();
		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);

		redirect("suratkontrak/");
	}

	public function edit(){
		$dosen = $this->is_login()["id"];
		$penelitian = $this->input->post("penelitian");
		$penelitian = Penelitian::find($penelitian);
		$penelitian->load('penelitian_review_inselected');
		$penelitian = $penelitian->toArray();
		$dana = $this->input->post("dana");
		$tanggal_mulai = $this->input->post('tanggal_mulai');
		$tanggal_selesai = $this->input->post('tanggal_selesai');

		$data = Surat_kontrak::find($this->uri->segment(3));
		if(role("Operator Lemlitbang")){
			// $data->dosen = $dosen;
			// $data->penelitian = $penelitian;
			$data->dana = (float)$dana*100/(float)$penelitian["penelitian_review_inselected"][0]["rekomendasi"];
		}
		$data->tanggal_mulai = formattotanggal($tanggal_mulai);
		$data->tanggal_selesai = formattotanggal($tanggal_selesai);
		$data->save();

		$penelitian = $data->penelitian()->first();
		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);
		// echo "waw" die;
		redirect("suratkontrak/");
	}

	public function check_batasan(){
		$id = $this->input->get("penelitian");
		$dana = $this->input->get("dana");
		$penelitian = Penelitian::find($id);
		$penelitian->load('surat_kontrak');
		$penelitian->load('penelitian_review_inselected');
		$penelitian = $penelitian->toArray();
		if(empty($penelitian["suratkontrak"])){
			$batasan = Batasan_dana::where("isdelete","=","0")->where("status","=","1")->first();
		}else{
			$batasan = Batasan_dana::where("id","=",$penelitian["suratkontrak"]["batasan_dana"])->first();
		}
		$hitung = (float)$dana*100/(float)$penelitian["penelitian_review_inselected"][0]["rekomendasi"];
		if($dana % 50 != 0){
			$this->output->set_header('HTTP/1.0 400 Dana yang diambil kurang dari minimum nilai penarikan');
			return;
		}

		if($hitung<=$batasan->batasan+100) return true;
		else{
			$this->output->set_header('HTTP/1.0 400 Melebihi Batasan');
			return;
		}
	}

	private function is_decimal( $val ){
	    return is_numeric( $val ) && floor( $val ) != $val;
	}

	public function terbilang(){
		$nominal = $this->input->get('nominal');
		return angka_huruf($nominal);
	}

	public function uploadsk1(){
		$id = $this->uri->segment(3);
		$this->generate_folder('uploads/suratkontraks');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/suratkontraks'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded){
			$this->session->set_flashdata('success', 'Input nomor kontrak berhasil!');
			$nomor = $this->input->post("nomor");
			$data = Surat_kontrak::find($id);
			$data->berkas=$this->upload->data('file_name');
			$data->isvalidate=0;
			$data->status=1;
			$data->save();
			echo '{"file":"'.$this->upload->data('file_name').'","id":'.$id.'}';
			celery()->PostTask('tasks.suratkontrak_submit', array(base_url(),$data->id));

			$penelitian = $data->penelitian()->first();
			$jenis_penelitian = $penelitian->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengupload Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);

			send_notif_suratkontrak(0,"Dosen ".$this->is_login()["nama"]." telah mengupload surat kontrak 1 \"".$first_four_title."...\", segera di validasi");


		}else{
			echo $this->upload->display_errors();
		}
	}

	public function is_nomor_exists(){
		$nomor = $this->input->get('nomor');
		$sk = $this->input->get('sk');
		// $juml = Surat_kontrak::where('nomor','=',$nomor)->where('id','<>',$sk)->count();
		// if($juml>0){
		// 	$this->output->set_header('HTTP/1.0 470 Nomor sudah ada');
		// 	return;
		// }
		$this->output->set_header('HTTP/1.0 200 OK');
		echo "Aprove";
	}

	public function compose_surat($nomor){
		$id = $this->uri->segment(3);
		$this->load->helper('suratkontrak');
		$suratkontrak = Surat_kontrak::find($id);
		$suratkontrak->load('Penelitian');
		$suratkontrak->load('Dosen');
		$suratkontrak->load('Penelitian.penelitian_review_inselected');
		$suratkontrak = $suratkontrak->toArray();
		$this->load->model('Surat');
		// $surat1 = Surat::where("id","=",$suratkontrak["surat1"])->first();
		if($nomor==1){
			$surat1 = Surat::where("id","=",$suratkontrak["surat1"])->first();
			$content = $surat1->isi;
		}else{
			$surat1 = Surat::where("id","=",$suratkontrak["surat2"])->first();
			$content = $surat1->isi;
		} 
		

		//replace NOKONTRAK1
		$content = sk_nokontrak($content,$suratkontrak["nomor"]);
		$content = sk_formatnomor1($content,$suratkontrak["format_nomor"]);
		$content = sk_formatnomor2($content,$suratkontrak["format_nomor"]);
		$content = sk_ejatgl($content);

		//replace JUDUL PENGUSUL
		$content = sk_judul($content,strtoupper($suratkontrak["penelitian"]["judul"])); 
		$content = sk_namapengusul($content,$suratkontrak["dosen"]["nama_lengkap"]);

		//replace TANGGALMULAI-TANGGALSELESAI TANGGALPEMBUATAN
		$content = sk_tglmulai($content,$suratkontrak["tanggal_mulai"]);
		$content = sk_tglselesai($content,$suratkontrak["tanggal_selesai"]);
		$content = sk_tanggalpembuatan($content,$suratkontrak["updated_at"]);

		//replace DANA
		$content = sk_jmldana($content,$suratkontrak["dana"],$suratkontrak["penelitian"]["penelitian_review_inselected"][0]["rekomendasi"]);

		//replace TEMBUSAN
		$content = sk_tembusan($content,strtoupper($suratkontrak["tembusan"]));

		//replace KETUA-MENGETAHUI
		$content = sk_peran($content,strtoupper($suratkontrak["persetujuan1"]),strtoupper($suratkontrak["persetujuan2"]));

		return array("content"=>$content,"kop"=>$surat1->kop);
	}

	public function surat1(){
		$this->load->helper('pdf_helper');
		$surat = $this->compose_surat(1);
		tcpdf();
		$obj_pdf = new SURATKONTRAKPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false,false, $surat["kop"]);
		if($surat["kop"]) $MARGIN_TOP = PDF_MARGIN_TOP+10;
		else $MARGIN_TOP = 10;
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(20, $MARGIN_TOP, 20);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(10);
		$obj_pdf->AddPage();
	
	
		ob_start();
		$user = $this->is_login();
		// $this->load->view('suratkontrak/pdf',array("content"=>$content));
	
		$surat["content"] = '<style>'.file_get_contents(FCPATH.'assets/dist/css/suratkontrak.css').'</style>'.$surat["content"];
		$surat["content"] = preg_replace('/<p\s[^>]*style\s*=\s*"(.*?)">/', '<table style="$1"><tr><td>', $surat["content"]);
		$surat["content"] = preg_replace('/<p\s[^>]*>/', '<table><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('<p>', '<table><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('</p>', '</td></tr></table>', $surat["content"]);
	
		$surat["content"] = preg_replace('/<div\s[^>]*style\s*=\s*"(.*?)">/', '<table style="$1"><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('<div>', '<table><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('</div>', '</td></tr></table>', $surat["content"]);
	
		$surat["content"] = preg_replace('/<li\s[^>]*style\s*=\s*"(.*?)">/', '<li style="$1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$surat["content"]);
		$surat["content"] = str_replace('<li>', '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $surat["content"]);
		echo $surat["content"];
		// die;
    	$content = ob_get_contents();
		ob_end_clean();
		// echo "<pre>";print_r($content);die();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output('Surat Kontrak1.pdf', 'I');
	}

	// public function surat1(){
	// 	$this->load->helper('pdf_helper');
	// 	$surat = $this->compose_surat(1);
	// 	mpdf();
	// 	$obj_pdf = new mPdf();

	// 	ob_start();
	// 	$user = $this->is_login();
	// 	$surat["content"] = '<htmlpageheader name="firstpage" style="display:none">
	// 						    <div style="text-align:center">First Page</div>
	// 						</htmlpageheader><sethtmlpageheader name="firstpage" value="on" show-this-page="1" />
	// 						<sethtmlpageheader name="otherpages" value="on" />
	// 						'.$surat["content"];
	// 	$margin_top = 20;
	// 	if($surat["kop"]){
	// 		$margin_top = 40;
	// 	}
	// 	$obj_pdf->AddPage('', '', '', '', '',	20,	20, $margin_top, 50, 	5, 	10); // margin footer
	// 	$htmlheader = '
	// 	<div style="margin-left:-15mm;margin-right:-15mm;">
	// 	            <div style="float:left;width:18%;text-align:right;">
	// 	            &nbsp;&nbsp;<img src="http://127.0.0.1/simakip/assets/images/lemlit.png" style="width:110px;height:auto;">
	// 	            </div>
	// 	            <div style="width:80%;">
	// 	              <div style="text-align:center;font-size:14pt;font-family: "Times New Roman", Times, serif;">UNIVERSITAS MUHAMMADIYAH PROF. DR. HAMKA</div>
	// 	              <div style="line-height:30pt;font-size:17pt;font-weight:bold;text-align:center;font-family: "Times New Roman", Times, serif;">LEMBAGA PENELITIAN DAN PENGEMBANGAN</div>
	// 	              <div style="font-size:12pt;font-weight:bold;text-align:center;font-family: Arial, Helvetica, sans-serif;">Jln. Tanah Merdeka, Pasar Rebo, Jakarta Timur <br>
	// 	Telp. 021-8416624, 87781809; Fax. 87781809
	// 	              </div>
	// 	            </div>
	// 							<hr style="margin-top:2px;"><hr style="margin:-10px;">
	// 	        </div>';
	// 	if($surat["kop"]){
	// 		$obj_pdf->setHtmlHeader($htmlheader,'1', true);
	// 	}
	// 	$htmlfooter = '<table style="width:100%;">
	// 									<tr>
	// 										<td style="width:20%;text-align:left;font-size:7pt;font-family: "Times New Roman", Times, serif;">Hak Cipta © http://simakip.uhamka.ac.id</td>
	// 										<td style="width:40%;text-align:center;font-size:7pt;font-family: "Times New Roman", Times, serif;">Tanggal Download:'.date('d-m-Y').'</td>
	// 										<td style="width:20%;text-align:right;font-size:7pt;font-family: "Times New Roman", Times, serif;">Halaman {PAGENO} dari {nb}</td>
	// 									</tr>
	// 								</table>';
 //    // echo $content;		die;
	// 	$obj_pdf->setHtmlFooter($htmlfooter);
	// 	$obj_pdf->WriteHTML($surat["content"]);
	// 	$obj_pdf->Output('Surat Kontrak 1.pdf', 'I');
	// }


	// public function surat2(){
	// 	$this->load->helper('pdf_helper');
	// 	$surat = $this->compose_surat(2);
	// 	mpdf();
	// 	$obj_pdf = new mPdf();

	// 	ob_start();
	// 	$user = $this->is_login();
	// 	$surat["content"] = '<htmlpageheader name="firstpage" style="display:none">
	// 						    <div style="text-align:center">First Page</div>
	// 						</htmlpageheader><sethtmlpageheader name="firstpage" value="on" show-this-page="1" />
	// 						<sethtmlpageheader name="otherpages" value="on" />
	// 						'.$surat["content"];
	// 	$margin_top = 20;
	// 	if($surat["kop"]){
	// 		$margin_top = 40;
	// 	}
	// 	$obj_pdf->AddPage('', '', '', '', '',	20,	20, $margin_top, 50, 	5, 	10); // margin footer
	// 	$htmlheader = '
	// 	<div style="margin-left:-15mm;margin-right:-15mm;">
	// 	            <div style="float:left;width:18%;text-align:right;">
	// 	            &nbsp;&nbsp;<img src="http://127.0.0.1/simakip/assets/images/lemlit.png" style="width:110px;height:auto;">
	// 	            </div>
	// 	            <div style="width:80%;">
	// 	              <div style="text-align:center;font-size:14pt;font-family: "Times New Roman", Times, serif;">UNIVERSITAS MUHAMMADIYAH PROF. DR. HAMKA</div>
	// 	              <div style="line-height:30pt;font-size:17pt;font-weight:bold;text-align:center;font-family: "Times New Roman", Times, serif;">LEMBAGA PENELITIAN DAN PENGEMBANGAN</div>
	// 	              <div style="font-size:12pt;font-weight:bold;text-align:center;font-family: Arial, Helvetica, sans-serif;">Jln. Tanah Merdeka, Pasar Rebo, Jakarta Timur <br>
	// 	Telp. 021-8416624, 87781809; Fax. 87781809
	// 	              </div>
	// 	            </div>
	// 							<hr style="margin-top:2px;"><hr style="margin:-10px;">
	// 	        </div>';
	// 	if($surat["kop"]){
	// 		$obj_pdf->setHtmlHeader($htmlheader,'1', true);
	// 	}
	// 	$htmlfooter = '<table style="width:100%;">
	// 									<tr>
	// 										<td style="width:20%;text-align:left;font-size:7pt;font-family: "Times New Roman", Times, serif;">Hak Cipta © http://simakip.uhamka.ac.id</td>
	// 										<td style="width:40%;text-align:center;font-size:7pt;font-family: "Times New Roman", Times, serif;">Tanggal Download:'.date('d-m-Y').'</td>
	// 										<td style="width:20%;text-align:right;font-size:7pt;font-family: "Times New Roman", Times, serif;">Halaman {PAGENO} dari {nb}</td>
	// 									</tr>
	// 								</table>';
 //    // echo $content;		die;
	// 	$obj_pdf->setHtmlFooter($htmlfooter);
	// 	$obj_pdf->WriteHTML($surat["content"]);
	// 	$obj_pdf->Output('Surat Kontrak 2.pdf', 'I');
	// }

	public function surat2(){
		$this->load->helper('pdf_helper');
		$surat = $this->compose_surat(2);
		tcpdf();
		$obj_pdf = new SURATKONTRAKPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false,false,$surat["kop"]);
		if($surat["kop"]) $MARGIN_TOP = PDF_MARGIN_TOP+10;
		else $MARGIN_TOP = 10;
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(20, $MARGIN_TOP, 20);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(10);
	
	
		$obj_pdf->AddPage();

		ob_start();
		$user = $this->is_login();
		// $surat["content"] = '<style>'.file_get_contents(FCPATH.'assets\dist\css\suratkontrak.css').'</style>'.$surat["content"];
		// $surat["content"] = preg_replace('/<p style="(.*)px;">/', '<table style="$1;"><tr><td>', $surat["content"]);
		// $surat["content"] = str_replace('<p>', '<table><tr><td>', $surat["content"]);
		// $surat["content"] = str_replace('</p>', '</td></tr></table>', $surat["content"]);

		// $surat["content"] = str_replace('<ol>', '<table style="padding-left:10px"><tr><td><ol>', $surat["content"]);
		// $surat["content"] = str_replace('<li>', '<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $surat["content"]);
		// $surat["content"] = str_replace('</li>', '</td></tr></table></li>', $surat["content"]);
		// $surat["content"] = str_replace('</ol>', '</ol></td></tr></table>', $surat["content"]);
		// echo $surat["content"];


		$surat["content"] = '<style>'.file_get_contents(FCPATH.'assets/dist/css/suratkontrak.css').'</style>'.$surat["content"];
		$surat["content"] = preg_replace('/<p\s[^>]*style\s*=\s*"(.*?)">/', '<table style="$1"><tr><td>', $surat["content"]);
		$surat["content"] = preg_replace('/<p\s[^>]*>/', '<table><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('<p>', '<table><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('</p>', '</td></tr></table>', $surat["content"]);

		$surat["content"] = preg_replace('/<div\s[^>]*style\s*=\s*"(.*?)">/', '<table style="$1"><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('<div>', '<table><tr><td>', $surat["content"]);
		$surat["content"] = str_replace('</div>', '</td></tr></table>', $surat["content"]);

		// $content = str_replace('<ol>', '<ol style="padding-right:200px;">', $content);
		$surat["content"] = preg_replace('/<li\s[^>]*style\s*=\s*"(.*?)">/', '<li style="$1">&nbsp;&nbsp;&nbsp;&nbsp;', $surat["content"]);
		$surat["content"] = str_replace('<li>', '<li>&nbsp;&nbsp;&nbsp;&nbsp;', $surat["content"]);
		echo $surat["content"];

	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output('Surat Kontrak2.pdf', 'I');
	}

	public function pdfa(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$type_get = $this->input->get("type");

		$item = Surat_kontrak::where("isdelete","=","0")->with('penelitian')
		->with('penelitian.tahun_kegiatan')
		->with('penelitian.batch')->with('penelitian.batch.batch_penelitian')->with('penelitian.batch.batch_penelitian.tahun');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";
		if(!isviewall()){
			$item->where(function($q){
				$q->where('dosen','=',$this->is_login()['id']);
			});
			$nama=$user["gelar_depan"]." ".$user["nama"]." ".$user["gelar_belakang"];
			$jabatan="";
		}
		// if(!empty($type_get)) $jurnal->where('type','=',$type_get);

		// $tahun_get = $this->input->get("tahun");
		// $info = [];
		// if(!empty($tahun_get) && $tahun_get!=''){
		// 	$jurnal->where('tahun_kegiatan','=',$tahun_get);
		// 	$tahun_kegiatan = Tahun_kegiatan::find($tahun_get);
		// 	$info = $tahun_kegiatan->toArray();
		// }
		$item = $this->filter($item);
		$item = $item->get();
		$item->load('dosen');
		$this->load->view('suratkontrak/pdf',array("items"=>$item->toArray(),"nama"=>$nama,"jabatan"=>$jabatan,"tahun"=>[]));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_SuratKontrak_'.Date('dmY').'.pdf', 'I');
	}

	private function dataExcel(){
		$this->load->model('Penelitian_nilai');
		$this->load->model('Penilaian');
		$this->load->model('Penilaian_kriteria');
		$this->load->model('Penelitian_Anggota');
		$this->load->model('Jenis_penelitian');
		$this->load->model('Fakultas');
		$this->load->model('Program_studi');
		$suratkontrak = Surat_kontrak::where("isdelete","=","0")
										->with('dosen')->with('dosen.fakultas')->with('dosen.program_studi')
								        ->with('penelitian')->with('penelitian.anggota.dosen')
										->with('penelitian.tahun_kegiatan')->with('penelitian.jenis_penelitian')
										->with('penelitian.batch')->with('penelitian.batch.batch_penelitian')
										->with('penelitian.batch.batch_penelitian.tahun')->with('penelitian.penelitian_review_exselected')->with('penelitian.penelitian_review_inselected.penelitian_nilai');
		$suratkontrak = $this->filter($suratkontrak);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$suratkontrak->where('dosen','=',$this->is_login()['id']);
	 	}
		$suratkontrak = $suratkontrak->get();
	 	// $suratkontrak->load('penelitian.anggota')->load('penelitian.anggota.dosen');
	 	// $suratkontrak->load('penelitian.jenis_penelitian');
 		// echo $suratkontrak->toJson(); die;
		return $suratkontrak->toArray();
	}

 	public function pdf(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				// 'bottom' => $default_border,
 				// 'left' => $default_border,
 				// 'top' => $default_border,
 				// 'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_surat_kontrak.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		// ->setLastModifiedBy("Sigit prasetya n")
 		// ->setTitle("Creating file excel with php Test Document")
 		// ->setSubject("Creating file excel with php Test Document")
 		// ->setDescription("How to Create Excel file from PHP with PHPExcel 1.8.0 Classes by seegatesite.com.")
 		// ->setKeywords("phpexcel")
 		// ->setCategory("Test result file");
 		$excel->setActiveSheetIndex(0);
		// $dataku=array(
		// 	array('C001','Iphone 6'),
		// 	array('C002','Samsung Galaxy S4'),
		// 	array('C003','Nokia Lumia'),
		// 	array('C004','Blackberry Curve'));
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_penelitian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$jenis_penelitian = 'G'.$urut;
			$nilai_akumulasi = 'H'.$urut;
			$anggaran_usulan = 'I'.$urut;
			$anggaran_rekomendasi= ['J'.$urut,'K'.$urut];
			$fakultas = 'L'.$urut;
			$anggaran_disetujui = 'M'.$urut;
			$tahap1 = 'N'.$urut;
			$tahap2 = 'O'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]['penelitian']["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]['penelitian']["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$rekomendasi_string=[];
			for($x=0;$x<count($data[$i]['penelitian']["penelitian_review_exselected"]);$x++){
				$rekomendasi_string[]= $data[$i]['penelitian']["penelitian_review_exselected"][$x]["rp_rekomendasi"];
			}

			$disetujui_string = "";
			$nilai_akumulasix = 0;
			$anggaran = 0;
			for($x=0;$x<count($data[$i]['penelitian']["penelitian_review_inselected"]);$x++){
				if($data[$i]['penelitian']["penelitian_review_inselected"][$x]["isselected"]==1){
					$anggaran = $data[$i]['penelitian']["penelitian_review_inselected"][$x]["rekomendasi"];
					$disetujui_string = $data[$i]['penelitian']["penelitian_review_inselected"][$x]["rp_rekomendasi"];
					foreach($data[$i]['penelitian']["penelitian_review_inselected"][$x]['penelitian_nilai'] as $value){
						$nilai_akumulasix += (float) $value['nilai'];
					}
				}
			}

			$hitung = (float) $data[$i]['dana'] * (float) $anggaran/ 100;
			$hitung = ceil($hitung/100000);
			$dana1 = (int) $hitung*100000;
			$dana2 = (int) $anggaran-$dana1;

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_penelitian, $data[$i]['penelitian']['judul'])->mergeCells($judul_penelitian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($jenis_penelitian, $data[$i]['penelitian']['jenis_penelitian']['nama'])
			->setCellValue($nilai_akumulasi, $nilai_akumulasix)
			->setCellValueExplicit($anggaran_usulan, $data[$i]['penelitian']['rp_total_biaya'],PHPExcel_Cell_DataType::TYPE_STRING);

			for($waw=0;$waw<count($rekomendasi_string);$waw++){
				$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_rekomendasi[$waw], $rekomendasi_string[$waw],PHPExcel_Cell_DataType::TYPE_STRING);
			}
			
			
			$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_disetujui, $disetujui_string,PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['penelitian_reviewer'])
			if($data[$i]['dana']){
				$excel->setActiveSheetIndex(0)->setCellValue($tahap1, $dana1);
				$excel->setActiveSheetIndex(0)->setCellValue($tahap2, $dana2);
			}
			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$laststyle=$tahap2;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		// for($col = 'A'; $col !== 'N'; $col++) {
		//     $excel->getActiveSheet()
		//         ->getColumnDimension($col)
		//         ->setAutoSize(true);
		// }
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_SuratKontrak_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}


	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
			 mkdir($folder_name,0777, true);
		}
	}
}
