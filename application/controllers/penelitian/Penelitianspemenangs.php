<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Penelitianspemenangs extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mp"=>"active","mp3"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_select"],
		    "functions_safe" => ["form_open","form_open_multipart"],
		];
        parent::__construct($config);
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Fakultas");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Penelitian");
    	$this->load->model("Penelitian_anggota");
    	$this->load->model("Pengesah");
    	$this->load->model("Penilaian");
    	$this->load->model("Penilaian_kriteria");
    	$this->load->model("Penelitian_review");
    	$this->load->model("Penelitian_reviewer");
    	$this->load->model("Penelitian_nilai");
    	$this->load->model("Batch_lists");
    }	
	public function index()
	{
		$data = [];
		$jenis_penelitian = Jenis_penelitian::where("isdelete","=","0")->get();
		// $jenis_penelitian->load("batch_penelitian");
		$data["jenis_penelitian"] = $jenis_penelitian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','4')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->with('Penelitian_review')->with('Penelitian_reviewer')->with('penelitian_review_inselected');
		$penelitian = $this->filter($penelitian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
 				$q->whereHas('penelitian_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
	 	}
		$info = $this->create_paging($penelitian);
		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$penelitian->load('jenis_penelitian');
		$penelitian->load('tahun_kegiatan');

		$data["penelitian"] = $penelitian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("penelitian/pemenang/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_penelitian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_penelitian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}
		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}


	private function dataExcel(){
		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','4')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen.fakultas')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->with('Penelitian_review')->with('Penelitian_reviewer')->with('penelitian_review_exselected')->with('penelitian_review_inselected');
		$penelitian = $this->filter($penelitian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
 				$q->whereHas('penelitian_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
	 	}
		$penelitian = $penelitian->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch');
		$penelitian->load('jenis_penelitian');
		$penelitian->load('penelitian_reviewer.dosen');
		$penelitian->load('tahun_kegiatan');
 	// 	echo $penelitian->toJson();
		// die;
		return $penelitian->toArray();
	}

 	public function download(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
 			'borders' => array(
 				'bottom' => $default_border,
 				'left' => $default_border,
 				'top' => $default_border,
 				'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'E1E0F7'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 16,
 				)
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_penelitian.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		// ->setLastModifiedBy("Sigit prasetya n")
 		// ->setTitle("Creating file excel with php Test Document")
 		// ->setSubject("Creating file excel with php Test Document")
 		// ->setDescription("How to Create Excel file from PHP with PHPExcel 1.8.0 Classes by seegatesite.com.")
 		// ->setKeywords("phpexcel")
 		// ->setCategory("Test result file");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_penelitian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$lokasi = 'G'.$urut;
			$anggaran_usulan = 'H'.$urut;
			$anggaran_rekomendasi= ['I'.$urut,'J'.$urut];
			$fakultas = 'K'.$urut;
			$anggaran_disetujui = 'L'.$urut;
			$reviewer1 = 'M'.$urut;
			$reviewer2 = 'N'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$rekomendasi_string=[];
			for($x=0;$x<count($data[$i]["penelitian_review_exselected"]);$x++){
				$rekomendasi_string[]= $data[$i]["penelitian_review_exselected"][$x]["rp_rekomendasi"];
			}

			$disetujui_string = "";
			for($x=0;$x<count($data[$i]["penelitian_review_inselected"]);$x++){
				if($data[$i]["penelitian_review_inselected"][$x]["isselected"]==1){
					$disetujui_string = $data[$i]["penelitian_review_inselected"][$x]["rp_rekomendasi"];
				}
			}

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_penelitian, $data[$i]['judul'])->mergeCells($judul_penelitian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($lokasi, $data[$i]['jenis_penelitian']['nama'])
			->setCellValueExplicit($anggaran_usulan, $data[$i]['rp_total_biaya'],PHPExcel_Cell_DataType::TYPE_STRING);

			for($waw=0;$waw<count($rekomendasi_string);$waw++){
				$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_rekomendasi[$waw], $rekomendasi_string[$waw],PHPExcel_Cell_DataType::TYPE_STRING);
			}
			
			
			$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_disetujui, $disetujui_string,PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['penelitian_reviewer'])
			if(count($data[$i]['penelitian_reviewer'])==2){
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer1, $data[$i]['penelitian_reviewer'][0]["dosen"]["nama_lengkap"]);
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer2, $data[$i]['penelitian_reviewer'][1]["dosen"]["nama_lengkap"]);
			}
			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			// for($waw=0;$waw<count($rekomendasi_string);$waw++){
			// 	$excel->setActiveSheetIndex(0)->getStyle($anggaran_rekomendasi[$waw])->getAlignment()->setWrapText(true);
			// }
			// $excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle=$reviewer2;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		// for($col = 'A'; $col !== 'N'; $col++) {
		//     $excel->getActiveSheet()
		//         ->getColumnDimension($col)
		//         ->setAutoSize(true);
		// }
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_PenelitianHibah_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
