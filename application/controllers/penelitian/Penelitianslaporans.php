<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Penelitianslaporans extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mp"=>"active","mp5"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_select"],
		    "functions_safe" => ["form_open","form_open_multipart","validation_errors_array"],
		];
        parent::__construct($config);
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Mahasiswa");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Penelitian");
    	$this->load->model("Penelitian_anggota");
		$this->load->model("Penelitian_anggotamhs");
    	$this->load->model("Pengesah");
    	$this->load->model("Penilaian");
    	$this->load->model("Penilaian_kriteria");
    	$this->load->model("Penelitian_review");
    	$this->load->model("Penelitian_reviewer");
    	$this->load->model("Penelitian_nilai");
    	$this->load->model("Penelitian_laporan");
    	$this->load->model("Batch_lists");
    }
	public function index()
	{
		$data = [];
		$jenis_penelitian = Jenis_penelitian::where("isdelete","=","0")->get();
		// $jenis_penelitian->load("batch_penelitian");
		$data["jenis_penelitian"] = $jenis_penelitian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','4')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_menyetujui')
										 ->with('dosen_mengetahui')
										 ->with('penelitian_reviewer')->whereHas('penelitian_laporan',function($q){
										 });
	 	$penelitian = $this->filter($penelitian,$data);
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
					$q->where('anggota','=',$this->is_login()['id']);
				});
			});
		}
	 	// die;
		$info = $this->create_paging($penelitian);
		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
		$penelitian->load('anggotamhs')->load('anggotamhs.mahasiswa')->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun')->load('penelitian_laporan');
		$penelitian->load('jenis_penelitian');
		$penelitian->load('tahun_kegiatan');

		$data["penelitian"] = $penelitian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("penelitian/laporan/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_penelitian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_penelitian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
		if($tahun_kegiatan){
			$model =$model->whereHas('batch',function($q) use ($tahun_kegiatan){
				$q->whereHas('batch_penelitian',function($w) use ($tahun_kegiatan){
					$w->where('tahun','=',$tahun_kegiatan);
					});
				});
 			// $item =$item->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

		$status = $this->input->get('status');
		if($status){
			if($status=="5") $status="0";
			$model->whereHas('penelitian_laporan',function($q) use ($status) {
				$q->where('status','=',$status);
			});
			// echo $model->get()->load('penelitian_laporan')->toJson(); die;
		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function add(){
		$data = [];
		// $data["user"] = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$penelitian = Penelitian::where("isdelete","=","0")->where('acc','=',"1")->where("status","=","4")->where('dosen','=',$this->is_login()["id"])->whereDoesntHave('penelitian_laporan')->get(['id','judul']);
		$data["penelitian"] = $penelitian->toArray();
		$data = array_merge($data,$this->menu);
		$this->twig->display("penelitian/laporan/add",$data);
	}

	public function edit(){
		$data = [];
		// $data["user"] = $this->is_login();
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');

		$laporan = Penelitian_laporan::whereId($id)->with('penelitian')
												   ->with('penelitian.jenis_penelitian')
												   ->with('penelitian.batch')
												   ->with('penelitian.batch.batch_penelitian.tahun')
												   ->get();
		// $laporan->load('penelitian.batch')->load('penelitian.batch.batch_penelitian.tahun');
		$data["laporan"] = $laporan->toArray()[0];
		
		
		$penelitian = Penelitian::where("isdelete","=","0")->where("status","=","4")
		->where(function($q){
			$q->whereDoesntHave('penelitian_laporan')->orWhereHas('penelitian_laporan',function($q){
				$q->where('id','=',$this->uri->segment(3));
			});
		})->get(['id','judul']);
		$data["penelitian"] = $penelitian->toArray();
		// echo json_encode($data);
		// die;
		$data = array_merge($data,$this->menu);
		if (isset($_GET['gedebug'])) {
            echo "<pre>"; print_r($data); echo "</pre>"; die("Asd");
        }
		$this->twig->display("penelitian/laporan/edit",$data);
	}

	public function upload_laporan(){
		$this->generate_folder('uploads/penelitian_laporan');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/penelitian_laporan'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "laporan_file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function upload_artikel(){
		$this->generate_folder('uploads/penelitian_artikel');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/penelitian_artikel'),
			'allowed_types' => '*',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "artikel_file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function upload_laporan_luaran_tambahan(){
		$this->generate_folder('uploads/penelitian_laporan_luaran_tambahan');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/penelitian_laporan_luaran_tambahan'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "laporan_file_luaran_tambahan";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function update(){
		$id = $this->uri->segment(3);
		$laporan = Penelitian_laporan::find($id);
		$penelitian = $this->input->post('judul');
		$laporan->penelitian = $penelitian;
		$laporan->laporan = $this->input->post('penelitian_laporan');
		$laporan->artikel = $this->input->post('penelitian_artikel');
		$laporan->laporan_luaran_tambahan = $this->input->post('penelitian_laporan_luaran_tambahan');
		$laporan->abstrak = $this->input->post('abstrak');
		if($laporan->status==2)	$laporan->status = 3;
		$laporan->save();
		celery()->PostTask('tasks.laporan_submit', array(base_url(),$laporan->id));

		$penelitian = $laporan->penelitian()->first();
		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Laporan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);


		redirect('penelitianslaporans');
	}

	public function insert(){
		$penelitian = $this->input->post('judul');
		$laporan = new Penelitian_laporan;
		$laporan->penelitian = $penelitian;
		$laporan->laporan = $this->input->post('penelitian_laporan');
		$laporan->status = 0;
		$laporan->artikel = $this->input->post('penelitian_artikel');
		$laporan->laporan_luaran_tambahan = $this->input->post('penelitian_laporan_luaran_tambahan');
		$laporan->abstrak = $this->input->post('abstrak');
		$laporan->save();
		celery()->PostTask('tasks.laporan_submit', array(base_url(),$laporan->id));

		$penelitian = $laporan->penelitian()->first();
		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Laporan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

		redirect('penelitianslaporans');
	}

	public function jsonpenelitian(){
		$id = $this->uri->segment(3);
		$data = Penelitian::with('dosen')->with('jenis_penelitian')->with('batch')->with('batch.batch_penelitian')->with('batch.batch_penelitian.tahun')
										 ->find($id);
		// $data->load("dosen")->load("dosen.jabatan_akademik")->load("dosen.program_studi");
		echo $data->toJson();
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function validasi_laporan(){
		$laporan = $this->input->post('laporan');
		$alasan = $this->input->post('alasan');
		$isvalid = $this->input->post('isvalid');
		$isvalid_luaran_wajib = $this->input->post('isvalid_luaran_wajib');
		$isvalid_luaran_tambahan = $this->input->post('isvalid_luaran_tambahan');
		$item = Penelitian_laporan::find($laporan);
		$penelitian = $item->penelitian()->first();
		$pengusul = $penelitian->dosen()->first()->toArray();
		$penelitianx = $penelitian;
		$penelitian = $penelitian->toArray();
		$item->status_luaran_wajib = $isvalid_luaran_wajib == '0' ? 2 : 1; 
		$item->status_luaran_tambahan = $isvalid_luaran_tambahan == '0' ? 2 : 1; 
		if($isvalid=='0'){
			$item->alasan = $item->alasan!=""?$item->alasan."<br><br>".$alasan:$alasan;
			$item->status = 2;
			$item->save();
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$penelitian["judul"],"alasan"=>$item->alasan];
			celery()->PostTask('tasks.laporan_invalid',array(base_url(),$data_email));

			// $penelitian = $laporan->penelitian()->first();
			$jenis_penelitian = $penelitianx->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $penelitianx->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menolak Laporan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

		}else{
			$item->status = 1;
			$item->save();
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$penelitian["judul"],"alasan"=>$item->alasan];
			celery()->PostTask('tasks.laporan_valid',array(base_url(),$data_email));

			$item = $item->penelitian()->first();
			$this->load->model("Point_penelitian");
 			$this->load->model("Point_log");
        	$point = Point_penelitian::active(2)->get()->toArray();
	        if(!empty($point)) {
	            $array = [];
	            $array[] = array("point" => $point[0]['ketua'], "created_date" => date('Y-m-d H:i:s'), "type" => 1, "status" => 1, "user_id" => $item->dosen,'jenis'=>7,'jenis_id'=>$item->id);
	            $anggotas = $item->anggota()->get();
	            foreach($anggotas as $anggota){
		            $array[] = array("point" => $point[0]['anggota'], "created_date" => date('Y-m-d H:i:s'), "type" => 2, "status" => 1, "user_id" => $anggota->anggota,'jenis'=>7,'jenis_id'=>$item->id);
	            }
	        }
	        Point_log::insert($array);

    		// $penelitian = $laporan->penelitian()->first();
			$jenis_penelitian = $penelitianx->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $penelitianx->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menerima Laporan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
		}
		// $item->save();
		// echo $item->toJson(); die;
		redirect('penelitianslaporans');
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$item = Penelitian_laporan::find($id);


		$penelitian = $laporan->penelitian()->first();
		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Laporan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

		$item->delete();
		redirect('penelitianslaporans/');
	}
}
