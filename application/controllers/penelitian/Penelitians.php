<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Penelitians extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mp"=>"active","mp1"=>"active");
 	public function __construct() {
 		$config = [
 		"functions" => ["anchor","set_value","set_select"],
 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
 		parent::__construct($config);
 		$this->load->model("Tahun_kegiatan");
 		$this->load->model("Jenis_penelitian");
 		$this->load->model("Batch_penelitian");
 		$this->load->model("Batch");
 		$this->load->model("Fakultas");
 		$this->load->model("Mahasiswa");
 		$this->load->model("Dosen");
 		$this->load->model("Jabatan_akademik");
 		$this->load->model("Jabatan_fungsi");
 		$this->load->model("Program_studi");
 		$this->load->model("Penelitian");
 		$this->load->model("Penilaian");
 		$this->load->model("Penilaian_kriteria");
 		$this->load->model("Penelitian_anggota");
 		$this->load->model("Penelitian_anggotamhs");
 		$this->load->model("Penelitian_reviewer");
 		$this->load->model("Penelitian_review");
 		$this->load->model("Penelitian_laporan");
 		$this->load->model("Pengesah");
 		$this->load->model("Syarat_penelitian");
		$this->load->model("Target_luaran");
		$this->load->model("Target_luaran_master");
		$this->load->model("Penelitian_target_luaran");
 		$this->load->model("Batas_anggaran");
 		$this->load->model("Keanggotaan");
 		$this->load->model("Batch_lists");
 		$this->load->model("Penelitian_target_luaran_wajib");
 		$this->load->model("Penelitian_target_luaran_tambahan");
 		$this->load->model("Target_luaran_sub");
 		$this->load->model("Janji_luaran");
        $this->load->model("Target_luaran_status");
 	}
 	public function index()
 	{
 		$data = [];
 		$jenis_penelitian = Jenis_penelitian::where("isdelete","=","0")->orderBy('status','asc');
 		if($this->is_login()['akses']['nama']=='Dosen'){
 			$jenis_penelitian = $jenis_penelitian->whereHas('syarat_penelitian', function($q){
 				$q->where('syarat','=',$this->is_login()['tingkat'])->where('jabatan_akademik','=',$this->is_login()['jabatan_akademik']['id'])->where('isdelete','=','0');
 			});
 		}
 		$jenis_penelitian = $jenis_penelitian->get();
		// $jenis_penelitian->load("batch_penelitian");

 		$data["jenis_penelitian"] = $jenis_penelitian->toArray();

 		$penelitian_tanpa_laporan = Penelitian::where("isdelete","=","0")->where("isvalid","=","1")->where('status','<>','12')->where("acc","=",0)->where(function($q){
 			$q->doesntHave('penelitian_laporan')
 			->orWhereHas('penelitian_laporan',function($qa){
 				$qa->where('status','<>','1');
 			});
 		})->where(function($q){
 			$q->where('dosen','=',$this->is_login()['id']);
 		})->count();

 		if($penelitian_tanpa_laporan>0){
 			foreach($data["jenis_penelitian"] as &$value){
 				unset($value["active_batch"]);
 			}
 		}

 		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
 		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

 		$penelitian = Penelitian::where("isdelete","=","0")->where('isvalid','=','1')->with('dosen.jabatan_akademik')
 		->with('dosen.jabatan_fungsional')
 		->with('dosen.program_studi')
 		->with('dosen_menyetujui')
 		->with('dosen_menyetujui.jabatan_akademik')
 		->with('dosen_menyetujui.program_studi')
 		->with('dosen_menyetujui.jabatan_fungsional')
 		->with('dosen_mengetahui')
 		->with('dosen_mengetahui.jabatan_akademik')
 		->with('dosen_mengetahui.program_studi')
		->with('dosen_mengetahui.jabatan_fungsional')
		->with('anggotamhs')->with('anggotamhs.mahasiswa')
 		->with('anggota')->with('anggota.dosen')->with('Penelitian_review')->with('Penelitian_reviewer');
 		$penelitian = $this->filter($penelitian,$data);
 		if($this->is_login()['akses']['nama']=='Dosen'){
 			$penelitian->where(function($q){
 				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
 					$q->where('anggota','=',$this->is_login()['id']);
 				});
 			});
 		}
 		$info = $this->create_paging($penelitian);
 		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
 		$penelitian->load('batch')->load('batch.batch_penelitian.tahun');
 		$penelitian->load('jenis_penelitian');
 		$penelitian->load('tahun_kegiatan');
 		$data["penelitian"] = $penelitian->toArray();
 		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
 		$data = array_merge($data,$info,$this->menu);
    // echo "<pre>"; print_r($data); echo "</pre>";
	// 	die;
 		$this->twig->display("penelitian/usulan/index", $data);
 	}

 	public function detail(){
 		$data = [];
 		$jenis_penelitian = $this->uri->segment(4);
 		$batch = $this->uri->segment(6);
 		$id = $this->uri->segment(3);
 		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
 		->with('dosen.jabatan_fungsional')
 		->with('dosen.program_studi')
 		->with('dosen_menyetujui')
 		->with('dosen_menyetujui.jabatan_akademik')
 		->with('dosen_menyetujui.program_studi')
 		->with('dosen_menyetujui.jabatan_fungsional')
 		->with('dosen_menyetujui.jabatan_pengesah')
 		->with('dosen_mengetahui')
 		->with('dosen_mengetahui.jabatan_akademik')
 		->with('dosen_mengetahui.program_studi')
 		->with('dosen_mengetahui.jabatan_fungsional')
		->with('anggotamhs')->with('anggotamhs.mahasiswa')
 		->with('anggota')->with('anggota.dosen')->with('anggota.dosen.program_studi')->with('anggota.dosen.jabatan_akademik')->find($id);
 		$penelitian->load('batch')->load('batch.batch_penelitian.tahun');
        $penelitian->load('luaran_wajib', 'luaran_wajib.target_luaran_sub', 'luaran_wajib.target_luaran_status');
        $penelitian->load('luaran_tambahan', 'luaran_tambahan.target_luaran_sub', 'luaran_tambahan.target_luaran_status');
 		$penelitian->load('tahun_kegiatan')->load('target_luaran')->load('target_luarans.target');
 		$data= $penelitian->toArray();
 		$data = array_merge($data,$this->menu);
 		$this->twig->display("penelitian/usulan/detail",$data);
 	}

 	public function filter($model,&$data){
 		$dosen = $this->input->get('dosen');
 		$judul = $this->input->get('judul');
 		$jp = $this->input->get('jenis_penelitian');
 		$status = $this->input->get('status');
 		if($dosen){
 			$model->where(function($qt){
 				$qt->whereHas('dosen',function($q){
 					$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
 				})->orWhereHas('anggota.dosen',function($q){
 					$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
 				});
 			});
 		}

 		if($status){
 			$model->where('status','=',$status);
 		}

 		if($judul){
 			$model->where('judul','LIKE','%'.$judul.'%');
 		}
 		if($jp){
 			$model->where('jenis_penelitian','=',$jp);
 		}

 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

 		$orderby = $this->input->get('orderby');
 		$to = $this->input->get('to');
 		if($to=="") $to="DESC";
 		if($orderby){
 			$model->orderby($orderby,$to);
 		}else{
 			$model->orderby('id',$to);
 		}
 		$data["orderby"] = $orderby;
 		$data["to"] = $to;
 		return $model;
 	}

 	public function add(){
 		$data = [];
 		$jenis_penelitian = $this->uri->segment(4);
 		$batch = $this->uri->segment(6);
 		$data["jenis_penelitian"] = $jenis_penelitian;
 		$jenpen_data = Jenis_penelitian::find($jenis_penelitian);
 		$jenpen_data->load('keanggotaan');
 		$data["nama"] = $jenpen_data->nama;
 		$data['notes'] = $jenpen_data->keanggotaan->notes;
 		$data["batch"] = $batch;

 		$batch_penelitian = Batch::find($batch);
 		$batch_penelitian->load('batch_lists')->load('batch_penelitian')->load('batch_penelitian.tahun');
 		$data["batch_data"] = $batch_penelitian->toArray();

 		$Batas_anggaran = Batas_anggaran::where("jenis_penelitian","=",$jenis_penelitian)->where("status","=","1")->where("isdelete","=","0")->first();
 		$data["batas_anggaran"] = $Batas_anggaran->toArray();

 		$tahun = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
 		$data["tahun"] = $tahun->toArray();

 		$pengesah = Pengesah::where('isdelete','=','0')->where("status","=","1")->first();
 		$pengesah->load('dosen')->load('dosen.jabatan_akademik');
 		$data["pengesah"] = $pengesah->toArray();
 		$data["addmode"] = True;
 		$target_luaran = Target_luaran::where("isdelete","=","0")->where("jenis_penelitian","=",$jenis_penelitian)->get();

    $target_luaran_master = Target_luaran_master::where("isdelete", "=", "0")->get()->toArray();
    $data['target_luaran_master'] = $target_luaran_master;

    $janji_luaran = Janji_luaran::where("jenis_penelitian_id", $jenis_penelitian)->where("isdelete", "=", "0")->first();
    if ($janji_luaran) {
      $data['janji_luaran'] = $janji_luaran->toArray();
    }else {
      $data['janji_luaran'] = [];
    }

 		$data = array_merge($data,$this->menu,["target_luaran"=>$target_luaran->toArray()]);
 		$this->twig->display("penelitian/usulan/add", $data);
 	}

 	public function edit(){
 		$data = [];
 		$jenis_penelitian = $this->uri->segment(4);
 		$batch = $this->uri->segment(6);
 		$data["jenis_penelitian"] = $jenis_penelitian;
 		$data["batch"] = $batch;

 		$tahun = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
 		$data["tahun"] = $tahun->toArray();

 		$id = $this->uri->segment(3);
 		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
 		->with('dosen.jabatan_fungsional')
 		->with('dosen.program_studi')
 		->with('dosen_menyetujui')
 		->with('dosen_menyetujui.jabatan_akademik')
 		->with('dosen_menyetujui.program_studi')
 		->with('dosen_menyetujui.jabatan_fungsional')
 		->with('dosen_mengetahui')
 		->with('dosen_mengetahui.jabatan_akademik')
 		->with('dosen_mengetahui.program_studi')
 		->with('dosen_mengetahui.jabatan_fungsional')
 		->find($id);
 		$penelitian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian')->load('batch.batch_penelitian.tahun');
		$penelitian->load('tahun_kegiatan')->load('target_luarans');
    $penelitian->load(['luaran_wajib', 'luaran_wajib.target_luaran_sub', 'luaran_wajib.target_luaran_status']);
    $penelitian->load(['luaran_tambahan', 'luaran_tambahan.target_luaran_sub', 'luaran_tambahan.target_luaran_status']);

 		$target_luaran = Target_luaran::where("isdelete","=","0")->where("jenis_penelitian","=",$penelitian->jenis_penelitian)->get();
 		$data["penelitian"] = $penelitian->toArray();
 		$data["target_luaran"] = $target_luaran->toArray();
 		$pengesah = Pengesah::where('isdelete','=','0')->where("status","=","1")->first();
 		$pengesah->load('dosen')->load('dosen.jabatan_akademik');
 		$data["pengesah"] = $pengesah->toArray();

    $target_luaran_master = Target_luaran_master::where("isdelete", "=", "0")->get()->toArray();
    $data['target_luaran_master'] = $target_luaran_master;

    $janji_luaran = Janji_luaran::where("jenis_penelitian_id", $penelitian->jenis_penelitian)->where("isdelete", "=", "0")->first();
    if ($janji_luaran) {
      $data['janji_luaran'] = $janji_luaran->toArray();
    }else {
      $data['janji_luaran'] = [];
    }

 		$Batas_anggaran = Batas_anggaran::where("jenis_penelitian","=",$penelitian->jenis_penelitian)->where("status","=","1")->where("isdelete","=","0")->first();
 		$data["batas_anggaran"] = $Batas_anggaran->toArray();

 		$data = array_merge($data,$this->menu);
 		$this->twig->display("penelitian/usulan/add", $data);
 	}

 	public function pilih_reviewer(){
 		$data = [];
		$data["penelitian"] = $this->uri->segment(3);
		$dosen = Dosen::where("isdelete","=","0")->where(function($q){
			$q->where("isreviewer","=","1")->orWhere("isreviewer","=","3");
        })->get();
 		$dosen = $dosen->toArray();
 		$item = Penelitian_reviewer::where("penelitian","=",$data['penelitian'])->with('dosen')
 		->with('dosen.program_studi')
 		->with('dosen.jabatan_akademik')
 		->get();
 		$data["reviewer"] = $item->toArray();
 		$data = array_merge($data,$this->menu,["dosen"=>$dosen],["deadline"=>$this->input->get('deadline')]);
 		$this->twig->display("penelitian/usulan/pilih_reviewer", $data);
 	}

 	public function add_reviewer(){
 		$dosen = $this->input->post('dosen');
 		$penelitian = $this->input->post('penelitian');
 		$deadline = $this->input->post('deadline');
 		$deadline = date('Y-m-d',strtotime($deadline));
 		$item = new Penelitian_reviewer;
 		$item->dosen = $dosen;
 		$item->penelitian = $penelitian;
 		$item->deadline = $deadline;
 		$item->save();

		$jenis_penelitian = $item->penelitian()->first()->jenis_penelitian()->first();

 		$item->load('dosen');
 		$item->load('penelitian.dosen');
 		$item = $item->toArray();
 		$data = [];

		$first_four_title = implode(' ', array_slice(explode(' ', $item["penelitian"]["judul"]), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memilih '.$item["dosen"]["nama_lengkap"].' sebagai reviewer Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

 		$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
							"judul"=>$item["penelitian"]["judul"],"pengusul"=>$item["penelitian"]["dosen"]["nama_lengkap"],
							"deadline"=>$item["deadline"]];
 		// echo json_encode($data) ; die;
 		celery()->PostTask('tasks.penelitian_reviewers',array(base_url(),$data));

 		redirect('penelitians/pilih_reviewer/'.$item["penelitian"]["id"].'?deadline='.$this->input->post('deadline'));
 	}

	public function konfirm_reviewer(){
		$penelitian=$this->input->get('penelitian');
		$item = Penelitian_reviewer::where('penelitian','=',$penelitian)->get();
		$item->load('dosen');
		$item->load('penelitian.dosen');
		// echo $item->toJson();die;
		$items = $item->toArray();
		$data = [];
		// foreach($items as $item){
		// 	$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
		// 					"judul"=>$item["penelitian"]["judul"],"pengusul"=>$item["penelitian"]["dosen"]["nama_lengkap"],
		// 					"deadline"=>$item["deadline"]];
		// }
		// celery()->PostTask('tasks.penelitian_reviewers',array(base_url(),$data));
		redirect('penelitians/');
	}

 	public function edit_reviewer(){
 		$id = $this->uri->segment(3);
 		$item = Penelitian_reviewer::find($id);

 		$penelitian = $item->penelitian()->first();
 		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
 		$dosen = $item->dosen()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah tanggal deadline reviewer '.$dosen->nama.' dari tanggal '.$item->deadline.' menjadi '.$this->input->post('deadline').' pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

 		$item->deadline = $this->input->post('deadline');
 		$item->save();


 		redirect('penelitians/pilih_reviewer/'.$item->penelitian.'?deadline='.$this->input->post('deadline'));
 	}

 	public function verify_reviewer(){
 		$penelitian = $this->input->get('penelitian');
 		$item = Penelitian_reviewer::where('penelitian','=',$penelitian)->count();
 		if($item==2){
 			$this->output->set_header('HTTP/1.0 500 Reviewer sudah penuh');
 			return;
 		}

 		$item = Penelitian_reviewer::where('penelitian','=',$penelitian)
 		->whereHas('dosen',function($q){
 			$q->where('nidn','=',$this->input->get('nidn'));
 		})->count();
 		if($item>0){
 			$this->output->set_header('HTTP/1.0 500 Reviewer sudah ada');
 			return;
 		}

 		echo "berhasil";
 	}

 	public function delete_reviewer(){
 		$id = $this->uri->segment(3);
 		$item = Penelitian_reviewer::find($id);
 		$penelitian = $item->penelitian;

 		$penelitian = $item->penelitian()->first();
 		$jenis_penelitian = $penelitian->jenis_penelitian()->first();
 		$dosen = $item->dosen()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus '.$dosen->nama.' sebagai reviewer Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		$item->delete();
 		redirect('penelitians/pilih_reviewer/'.$penelitian->id);
 	}

 	public function delete(){
 		$id = $this->uri->segment(3);
 		$item = Penelitian::find($id);
 		$item->isdelete = 1;
 		$this->session->set_flashdata('success', 'Delete proposal penelitian '.$item->judul.' berhasil!');
 		$item->save();
 		$jenis_penelitian = $item->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
 	}

 	public function tolak(){
 		$id = $this->uri->segment(3);
 		$item = Penelitian::find($id);
 		$item->status = 12;
 		$this->session->set_flashdata('success', 'Tolak proposal penelitian '.$item->judul.' berhasil!');
 		$item->save();
 		$jenis_penelitian = $item->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' Menolak Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
 	}

 	public function validasi(){
 		$penelitian = $this->input->post('penelitian');
 		$alasan = $this->input->post('alasan');
 		$isvalid = $this->input->post('isvalid');
 		$item = Penelitian::find($penelitian);
		$pengusul= $item->dosen()->first()->toArray();
 		if($isvalid=='0'){
 			$item->alasan = $item->alasan!=""?$item->alasan." <br><br> ".$alasan:$alasan;
 			$item->status = 10;
			$jenis_penelitian =	$item->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menolak Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$item->judul,"alasan"=>$item->alasan];
			celery()->PostTask('tasks.penelitian_invalid',array(base_url(),$data_email));
 		}else{
 			$item->status = 3;
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$item->judul];

			$jenis_penelitian =	$item->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menerima Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

			celery()->PostTask('tasks.penelitian_valid',array(base_url(),$data_email));
 		}
 		$item->save();
 		$this->session->set_flashdata('success', 'Validasi proposal penelitian '.$item->judul.' berhasil!');
		redirect($_SERVER['HTTP_REFERER']);
 	}

 	public function validasi_perbaikan(){
 		$penelitian = $this->input->post('penelitian');
 		$alasan = $this->input->post('alasan');
 		$isvalid = $this->input->post('isvalid');
 		$item = Penelitian::find($penelitian);
		$pengusul= $item->dosen()->first()->toArray();
 		if($isvalid=='0'){
 			$item->alasan = $alasan;
 			$item->status = 9;
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$item->judul,"alasan"=>$item->alasan];
			celery()->PostTask('tasks.penelitian_syarat_invalid',array(base_url(),$data_email));

			$jenis_penelitian =	$item->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menolak perbaikan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		}else{
 			$item->status = 4;
			$data_email = ["id"=>$pengusul["id"],"pengusul"=>$pengusul["nama_lengkap"],"surel"=>$pengusul["surel"],
										"judul"=>$item->judul];
			celery()->PostTask('tasks.penelitian_syarat_valid',array(base_url(),$data_email));

			$jenis_penelitian =	$item->jenis_penelitian()->first();
			$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'validate','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menerima perbaikan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		}
 		$item->save();
 		redirect($_SERVER['HTTP_REFERER']);
 	}

 	public function submit(){
 		$id = $this->uri->segment(3);
 		$item = Penelitian::find($id);
 		$item->status = 2;
 		$item->submited_at = date('Y-m-d H:i:s');
 		$item->save();

 		$jenis_penelitian =	$item->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'submit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mensubmit Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

 		$this->session->set_flashdata('success', 'Submit proposal penelitian '.$item->judul.' berhasil!');
		celery()->PostTask('tasks.penelitian_submit',array(base_url(),$id));
 		redirect('penelitians/');
 	}

 	public function perbaikan(){
 		$id = $this->input->post("penelitian");
 		if($id==""){
 			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
 			return;
 		}
 		if (empty($_FILES['file']['name']))
 		{
 			return;
 		}
 		$this->generate_folder('uploads/penelitians');
 		$filename = $this->gen_uuid();
 		$upload_config = [
 		'file_name' => $filename,
 		'upload_path' => realpath(APPPATH . '../uploads/penelitians'),
 		'allowed_types' => 'pdf',
 		'max_size' => 5000,
 		'file_ext_tolower' => TRUE
 		];
 		$file_field = "file";
 		$this->load->library('upload', $upload_config);
 		$uploaded = $this->upload->do_upload($file_field);
 		if($uploaded){
 			$data = Penelitian::find($id);
 			$data->berkas_perbaikan = $this->upload->data('file_name');
 			$data->status = 7;
 			$data->save();
 			$this->session->set_flashdata('success', 'Perbaikan proposal penelitian '.$data->judul.' berhasil!');

			$jenis_penelitian = $data->jenis_penelitian()->first();
			$pengusul = $data->dosen()->first()->toArray();
			$anggota =  $data->anggota()->with('dosen')->get()->toArray();
			$batch = $data->batch()->first()->toArray();
			$batch_tahun = $data->batch()->first()->batch_penelitian()->first()->tahun()->first()->toArray();

 			send_email_penelitian_diperbaiki(["surel"=>$pengusul["surel"],"pengusul"=>$pengusul["nama_lengkap"],"judul"=>$data->judul,"jenis_penelitian"=>$jenis_penelitian->nama,"nama_batch"=>$batch->nama,"tahun_batch"=>$batch_tahun->tahun]);

 			$first_four_title = implode(' ', array_slice(explode(' ', $data->judul), 0, 4));
 			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'submit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengupload perbaikan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
  		}
 		redirect($_SERVER['HTTP_REFERER']);
 	}

 	public function konfirmasi(){
 		$id = $this->uri->segment(3);
 		$konfirmasi = $this->uri->segment(4);
 		$item = Penelitian_anggota::find($id);
		$penelitian = $item->penelitian()->first()->toArray();
 		if($konfirmasi==1){
 			$item->konfirmasi=1;
 			$item->save();
			$message="menerima";
 		}else if($konfirmasi==0){
 			$item->konfirmasi=2;
 			$item->save();
			$message="menolak";
 		}

 		$jenis_penelitian =	$item->penelitian()->first()->jenis_penelitian()->first();
		$first_four_title = implode(' ', array_slice(explode(' ', $penelitian['judul']), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'confirm','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' '.$message.' keanggotaan Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

		send_notif_penelitian($penelitian["dosen"],"Dosen ".$this->is_login()["nama_lengkap"]." telah ".$message." keanggotaan penelitian pada penelitian ".$penelitian["judul"]);
		// $penelitian = Penelitian::find($item->penelitian);
		// $penelitian->status=
 		$this->session->set_flashdata('success', 'Konfirmasi proposal penelitian '.$item->judul.' berhasil!');
 		redirect('penelitians/');
 	}

 	public function step1(){
 		$id = $this->input->post("penelitian");
 		$batch = $this->input->post("batch");
 		$jenis_penelitian = $this->input->post("jenis_penelitian");
 		$judul = $this->input->post("judul");
 		$abstrak = $this->input->post("abstrak");
 		$keyword = $this->input->post("keyword");
 		$lokasi = $this->input->post("lokasi");

 		$data = new Penelitian;
 		if($id!=""){
 			$data = Penelitian::find($id);
 		}
 		$data->batch = $batch;
 		$data->jenis_penelitian = $jenis_penelitian;
 		$data->judul = $judul;
 		$data->keyword = $keyword;
 		$data->abstrak = $abstrak;
 		$data->lokasi = $lokasi;
 		$data->dosen = $this->is_login()["id"];
 		$data->save();

 		echo $data->toJson();
 	}

 	public function step2(){
 		$id = $this->input->post("penelitian");
 		$tahun = $this->input->post("tahun");
 		if($id==""){
 			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
 			return;
 		}
 		$data = Penelitian::find($id);
 		$data->tahun_kegiatan = $tahun;
 		$data->save();
 		echo $data->toJson();
 	}

 	public function step3(){
 		$id = $this->input->post("penelitian");
 		$target = $this->input->post("target");
 		$satuan = $this->input->post("satuan");
		$lama = (int) $this->input->post("lama");
		$target_post = $this->input->post("targets[]");

 		if($id==""){
 			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
 			return;
 		}
 		$lama = $satuan=="tahun"?$lama*12:$lama;

		$data = Penelitian::find($id);
		if(!empty($target)) $data->target = $target;
		$data->lama = $lama;
		$data->save();



		if(!empty($target_post)){
			$array = [];
			Penelitian_target_luaran::where("penelitian", "=", $id)->delete();
			foreach($target_post as $tid){
				$array[]= array("penelitian"=>$id, "target_luaran"=>$tid);
			}
			Penelitian_target_luaran::insert($array);
		}

 		echo $data->toJson();
 	}

 	public function step4anggotamhs(){ 
		$nama = $this->input->post("nama");
		$nim = $this->input->post("nim");
		$penelitian = $this->input->post("penelitian");
		$program_studi = $this->input->post("program_studi");

		if(empty($nama) || empty($nim) || empty($program_studi)){
			$this->output->set_header('HTTP/1.0 403 Error');
			return;
		}
		
		$mahasiswa = new Mahasiswa;
		$mahasiswa->nama = $nama;
		$mahasiswa->nim = $nim;
		$mahasiswa->program_studi = $program_studi;
		$mahasiswa->save();

		$mhs = Mahasiswa::select('id')->get()->toArray();
		// echo "<pre>";print_r(end($mhs)['id']);die();
		$data = new Penelitian_anggotamhs;
		$data->penelitian = $penelitian;
		$data->anggotamhs = end($mhs)['id'];
		$data->peran = 1;
		$data->save();

		echo $data->toJson();
	}

 	public function step4(){
 		$penelitian = $this->input->post("penelitian");
 		$anggota = $this->input->post("anggota");
 		$peran = $this->input->post("peran");

 		if(empty($penelitian) || empty($anggota) || empty($peran)){
 			$this->output->set_header('HTTP/1.0 403 Error');
 			return;
 		}

 		$data = new Penelitian_anggota;
 		$data->penelitian = $penelitian;
 		$data->anggota = $anggota;
 		$data->peran = $peran;
 		$data->save();

 		echo $data->toJson();
 	}

  public function addluaranwajib(){
    $penelitian_id            = $this->input->post("penelitian");
    $target_luaran_master_id  = $this->input->post("target_luaran_master");
    $target_luaran_sub_id     = $this->input->post("target_luaran_sub");
    $target_luaran_status_id  = $this->input->post("target_luaran_status");
    $url_jurnal               = $this->input->post("url_jurnal");
    $rencana_jurnal           = $this->input->post("rencana_jurnal");
    $bukti_luaran             = $this->input->post("bukti_luaran");

    // Penelitian_target_luaran_wajib
    $ptlw = new Penelitian_target_luaran_wajib;
 		$ptlw->penelitian_id = $penelitian_id;
 		$ptlw->target_luaran_master_id = $target_luaran_master_id;
 		$ptlw->target_luaran_sub_id = $target_luaran_sub_id;
 		$ptlw->target_luaran_status_id = $target_luaran_status_id;
 		$ptlw->url_jurnal = $url_jurnal;
 		$ptlw->rencana_jurnal = $rencana_jurnal;
 		$ptlw->bukti_luaran = $bukti_luaran;
 		$ptlw->save();

    return true;
  }

  public function addluarantambahan(){
    $penelitian_id            = $this->input->post("penelitian");
    $target_luaran_master_id  = $this->input->post("target_luaran_master");
    $target_luaran_sub_id     = $this->input->post("target_luaran_sub");
    $target_luaran_status_id  = $this->input->post("target_luaran_status");
    $url_jurnal               = $this->input->post("url_jurnal");
    $rencana_jurnal           = $this->input->post("rencana_jurnal");
    $bukti_luaran             = $this->input->post("bukti_luaran");

    // Penelitian_target_luaran_wajib
    $ptlw = new Penelitian_target_luaran_tambahan;
 		$ptlw->penelitian_id = $penelitian_id;
 		$ptlw->target_luaran_master_id = $target_luaran_master_id;
 		$ptlw->target_luaran_sub_id = $target_luaran_sub_id;
 		$ptlw->target_luaran_status_id = $target_luaran_status_id;
 		$ptlw->url_jurnal = $url_jurnal;
 		$ptlw->rencana_jurnal = $rencana_jurnal;
 		$ptlw->bukti_luaran = $bukti_luaran;
 		$ptlw->save();

    return true;
  }

  public function luaranwajibbypenelitian($penelitian_id){
    $data = Penelitian_target_luaran_wajib
          ::with(['target_luaran_sub' => function($q){
            $q->where('isdelete', 0);
          }, 'target_luaran_status' => function($q){
            $q->where('isdelete', 0);
          }])
          ->where('penelitian_id', $penelitian_id)->get();
    echo $data->toJson();
    exit;
 	}

  public function luarantambahanbypenelitian($penelitian_id){
    $data = Penelitian_target_luaran_tambahan
          ::with(['target_luaran_sub' => function($q){
            $q->where('isdelete', 0);
          }, 'target_luaran_status' => function($q){
            $q->where('isdelete', 0);
          }])
          ->where('penelitian_id', $penelitian_id)->get();
    echo $data->toJson();
    exit;
 	}

 	public function checkanggota(){
 		$jenis_penelitian = $this->input->get('jenis_penelitian');
 		$penelitian = $this->input->get('penelitian');
 		$nidn = $this->input->get('nidn');

 		if($nidn==$this->is_login()['nidn']){
 			$this->output->set_header('HTTP/1.0 477 Tidak dapat menambahkan diri sendiri sebagai anggota');
 			return;
 		}

 		$anggota = Penelitian_anggota::where('penelitian','=',$penelitian)->whereHas('dosen',function($q){
 			$q->where('nidn','=',$this->input->get('nidn'));
 		})->count();
 		if($anggota>0){
 			$this->output->set_header('HTTP/1.0 477 Anggota sudah ada');
 			return;
 		}

 		$anggota = Penelitian_anggota::where('penelitian','=',$penelitian)->count();
 		$this->load->model('keanggotaan');
 		$max_anggota = Keanggotaan::where('jenis_penelitian','=',$jenis_penelitian)->first();
 		if($max_anggota->anggota==$anggota){
 			$this->output->set_header('HTTP/1.0 477 Anggota sudah penuh');
 			return;
		}
		$anggotamhs = Penelitian_anggotamhs::where('penelitian','=',$penelitian)->count();
 		$this->load->model('keanggotaan');
 		$max_anggotamhs = Keanggotaan::where('jenis_penelitian','=',$jenis_penelitian)->first();
		if($max_anggotamhs->anggotamhs==$anggotamhs){
			$this->output->set_header('HTTP/1.0 477 Anggota Mahasiswa sudah penuh');
			return;
		}
 		$anggota = Penelitian_anggota::whereHas('dosen',function($q){
 			$q->where('nidn','=',$this->input->get('nidn'));
 		})->whereHas('penelitian',function($q){
 			$q->where("isdelete","=","0")->where("isvalid","=","1")->where("status","<>","12")->doesntHave('penelitian_laporan');
 		})->count();
 		if($anggota>1){
 			$this->output->set_header('HTTP/1.0 477 Dosen sudah menjadi anggota di penelitian lain');
 			return;
 		}

 		$anggota = Penelitian::whereHas('dosen',function($q){
 			$q->where('nidn','=',$this->input->get('nidn'));
 		})->where("isdelete","=","0")->where("isvalid","=","1")->where("status","<>","12")->doesntHave('penelitian_laporan')->count();
 		if($anggota>1){
 			$this->output->set_header('HTTP/1.0 477 Dosen sedang dalam usulan penelitain aktif');
 		}

 		echo 'berhasil';
	}
	 
	public function checkanggotamhs(){
		$jenis_penelitian = $this->input->get('jenis_penelitian');
		$penelitian = $this->input->get('penelitian'); 
 
	    $anggotamhs = Penelitian_anggotamhs::where('penelitian','=',$penelitian)->count();
		$this->load->model('keanggotaan');
		$max_anggotamhs = Keanggotaan::where('jenis_penelitian','=',$jenis_penelitian)->first();
	    if($max_anggotamhs->anggotamhs==$anggotamhs){
		   $this->output->set_header('HTTP/1.0 477 Anggota Mahasiswa sudah penuh');
		   return;
	    } 

		echo 'berhasil';
	}

 	public function step4_anggota(){
 		$penelitian = $this->uri->segment(3);
 		$data = Penelitian_anggota::where("penelitian","=",$penelitian)->with("dosen")->with("dosen.jabatan_akademik")->with("dosen.program_studi")->get();
 		echo $data->toJson();
 	}
 	public function step4_anggotamhs(){
 		$penelitian = $this->uri->segment(3);
 		$data = Penelitian_anggotamhs::where("penelitian","=",$penelitian)->with("mahasiswa")->get();
 		echo $data->toJson();
 	}

 	public function step4_hapus(){
 		$penelitian = $this->uri->segment(3);
 		$data = Penelitian_anggota::find($penelitian);
 		$data->delete();
 	}
 	public function step4_hapusmhs(){
 		$penelitian = $this->uri->segment(3);
 		$data = Penelitian_anggotamhs::find($penelitian);
 		$data->delete();
 	}

 	public function step5(){
 		$id = $this->input->post("penelitian");
 		$total_biaya = $this->input->post("total_biaya");
 		$mandiri_total_biaya = $this->input->post("mandiri_total_biaya");
 		$mandiri_biaya_instansi = $this->input->post("mandiri_biaya_instansi");
 		$dosen_mengetahui = $this->input->post("dosen_mengetahui");
 		$dosen_menyetujui = $this->input->post("dosen_menyetujui");
 		if($id==""){
 			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
 			return;
 		}
 		$data = Penelitian::find($id);
 		$data->total_biaya = $total_biaya;
 		$data->mandiri_total_biaya = $mandiri_total_biaya;
 		$data->mandiri_biaya_instansi = $mandiri_biaya_instansi;
 		$data->dosen_mengetahui = $dosen_mengetahui;
 		$data->dosen_menyetujui = $dosen_menyetujui;
 		$data->save();
 		echo $data->toJson();
 	}

 	public function step5_check_anggaran(){
 		$jenis_penelitian = $this->input->get("jenis_penelitian");
 		$total_biaya = $this->input->get("total_biaya");
 		$rekomendasi = $this->input->get("rekomendasi");
 		$this->load->model("Batas_anggaran");
 		$data = Batas_anggaran::where("jenis_penelitian","=","$jenis_penelitian")->where("status","=","1")->where("isdelete","=","0")->first();
 		$batas = (float) $data->batas;
 		$total_biaya = (float) $total_biaya;
 		$rekomendasi = (float) $rekomendasi;
 		if($total_biaya>$batas || $rekomendasi>$batas){
 			$this->output->set_header('HTTP/1.0 400 Melebihi Anggaran');
 			return;
 		}
 		$this->output->set_header('HTTP/1.0 200 OK');
 		echo "Aprove";
 	}

 	public function step6(){
 		$this->generate_folder('uploads/penelitians');
 		$filename = $this->gen_uuid();
 		$upload_config = [
 		'file_name' => $filename,
 		'upload_path' => realpath(APPPATH . '../uploads/penelitians'),
 		'allowed_types' => 'pdf',
 		'max_size' => 10000,
 		'file_ext_tolower' => TRUE
 		];
 		$file_field = "file";
 		$this->load->library('upload', $upload_config);
 		$id = $this->input->post("penelitian");
 		echo json_encode($_POST);
 		if($id==""){
 			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
 			return;
 		}
 		if (empty($_FILES['file']['name']))
 		{
 			return;
 		}

 		$uploaded = $this->upload->do_upload($file_field);
 		if($uploaded){
 			$data = Penelitian::find($id);
 			$data->berkas= $this->upload->data('file_name');
 			$data->save();
 			echo $data->toJson();
 			return;
 		}
 	}

 	private function generate_folder($folder_name){
 		if(!is_dir($folder_name))
 		{
 			mkdir($folder_name,0777, true);
 		}
 	}

 	public function step7(){
 		$id = $this->input->post("penelitian");
 		$abstrak = $this->input->post("abstrak");
 		if($id==""){
 			$this->output->set_header('HTTP/1.0 500 Error, ID not Found');
 			return;
 		}
 		$data = Penelitian::find($id);
		$jenis_penelitian = $data->jenis_penelitian()->first();
 		if($data->isvalid==0){
 			$this->session->set_flashdata('success', 'Proposal berhasil dibuat!');
			$first_four_title = implode(' ', array_slice(explode(' ', $data->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		}else{
 			$this->session->set_flashdata('success', 'Proposal berhasil diedit!');
 			$first_four_title = implode(' ', array_slice(explode(' ', $data->judul), 0, 4));
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		}



		// echo $data->jenis_penelitian()->get()->toJson(); die;
		// $jenis_penelitian = $data->jenis_penelitian()->first();
		$pengusul = $data->dosen()->first()->toArray();
		$anggota =  $data->anggota()->with('dosen')->get()->toArray();
		$batch = $data->batch()->first()->toArray();
		$batch_tahun = $data->batch()->first()->batch_penelitian()->first()->tahun()->first()->toArray();
		$email = [];
 		$data->isvalid=1;
 		$data->save();
		foreach($anggota as $ang){
			send_notif_penelitian($ang["anggota"],"bapak/ibu terpilih menjadi anggota peneliti pada penelitian ".$data->judul.", silakan konfirmasi");
			$email[] = ["anggota"=>$ang["dosen"]["nama_lengkap"],"surel"=>$ang["dosen"]["surel"],"pengusul"=>$pengusul["nama_lengkap"],"judul"=>$data->judul,"nama_batch"=>$batch["nama"],"tahun_batch"=>$batch_tahun["tahun"]];
		}
		// send_email_penelitian_anggota($email);
		celery()->PostTask('tasks.send_mail_anggota',array(base_url(),$email));
		send_email_penelitian_dibuat(["surel"=>$pengusul["surel"],"pengusul"=>$pengusul["nama_lengkap"],"judul"=>$data->judul,"jenis_penelitian"=>$jenis_penelitian->nama,"nama_batch"=>$batch->nama,"tahun_batch"=>$batch_tahun->tahun]);
 		redirect('penelitians/');
 	}

 	public function json(){
 		$id = $this->uri->segment(3);
 		$data = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
 		->with('dosen.jabatan_fungsional')
 		->with('dosen.program_studi')
 		->with('dosen_menyetujui')
 		->with('dosen_menyetujui.jabatan_akademik')
 		->with('dosen_menyetujui.program_studi')
 		->with('dosen_menyetujui.jabatan_fungsional')
 		->with('dosen_menyetujui.jabatan_pengesah')
 		->with('dosen_mengetahui')
 		->with('dosen_mengetahui.jabatan_akademik')
 		->with('dosen_mengetahui.program_studi')
 		->with('dosen_mengetahui.jabatan_fungsional')
		->with('anggotamhs')->with('anggotamhs.mahasiswa')
 		->with('anggota')->with('anggota.dosen')->with('anggota.dosen.jabatan_akademik')->with('anggota.dosen.program_studi')
 		->find($id);
 		$data->load('batch');
 		$data->load('batch.batch_penelitian')->load('batch.batch_penelitian.tahun');
 		$data->load('tahun_kegiatan');
 		$data->load(['luaran_wajib',
                      'luaran_wajib.target_luaran_sub' => function($q){
                        $q->where('isdelete', 0);
                      }, 'luaran_wajib.target_luaran_status' => function($q){
                        $q->where('isdelete', 0);
                      }]);
    $data->load(['luaran_tambahan',
                      'luaran_tambahan.target_luaran_sub' => function($q){
                        $q->where('isdelete', 0);
                      }, 'luaran_tambahan.target_luaran_status' => function($q){
                        $q->where('isdelete', 0);
                      }]);
		// $data->load("dosen")->load("dosen.jabatan_akademik")->load("dosen.program_studi");
 		echo $data->toJson();
 	}

  public function luaranwajibeditform(){
    $penelitian_id = $this->input->post('id');
    $target_luaran_master = Target_luaran_master::where("isdelete", "=", "0")->get()->toArray();
    $data['target_luaran_master'] = $target_luaran_master;

    $ptlw = Penelitian_target_luaran_wajib
          ::with(['target_luaran_sub' => function($q){
            $q->where('isdelete', 0);
          }, 'target_luaran_status' => function($q){
            $q->where('isdelete', 0);
          }])
          ->where('penelitian_id', $penelitian_id)->first()->toArray();

    $tlsub = Target_luaran_sub::where("target_luaran_master_id", $ptlw['target_luaran_master_id'])->where("isdelete", "=", "0")->get()->toArray();
    $tlstatus = Target_luaran_status::where("target_luaran_master_id", $ptlw['target_luaran_master_id'])->where("isdelete", "=", "0")->get()->toArray();

    $data['luaran_wajib'] = $ptlw;
    $data['target_luaran_sub_option'] = $tlsub;
    $data['target_luaran_status_option'] = $tlstatus;

    $this->twig->display("penelitian/luaranwajib/edit", $data);
 	}

  public function updateluaranwajib(){
    $ptlw = Penelitian_target_luaran_wajib::find($this->input->post('id'));
    $ptlw->penelitian_id = $this->input->post('penelitian_id');
 		$ptlw->target_luaran_master_id = $this->input->post('target_luaran_master');
 		$ptlw->target_luaran_sub_id = $this->input->post('target_luaran_sub');
 		$ptlw->target_luaran_status_id = $this->input->post('target_luaran_status');
 		$ptlw->url_jurnal = $this->input->post('url_jurnal');
 		$ptlw->rencana_jurnal = $this->input->post('rencana_jurnal');
 		$ptlw->bukti_luaran = $this->input->post('bukti_luaran');
 		$ptlw->save();
    echo json_encode(['code' => 200]);
    exit;
  }

  public function luarantambahaneditform(){
    $penelitian_id = $this->input->post('id');
    $target_luaran_master = Target_luaran_master::where("isdelete", "=", "0")->get()->toArray();
    $data['target_luaran_master'] = $target_luaran_master;

    $ptlw = Penelitian_target_luaran_tambahan
          ::with(['target_luaran_sub' => function($q){
            $q->where('isdelete', 0);
          }, 'target_luaran_status' => function($q){
            $q->where('isdelete', 0);
          }])
          ->where('penelitian_id', $penelitian_id)->first()->toArray();

    $tlsub = Target_luaran_sub::where("target_luaran_master_id", $ptlw['target_luaran_master_id'])->where("isdelete", "=", "0")->get()->toArray();
    $tlstatus = Target_luaran_status::where("target_luaran_master_id", $ptlw['target_luaran_master_id'])->where("isdelete", "=", "0")->get()->toArray();

    $data['luaran_tambahan'] = $ptlw;
    $data['target_luaran_sub_option'] = $tlsub;
    $data['target_luaran_status_option'] = $tlstatus;

    $this->twig->display("penelitian/luarantambahan/edit", $data);
  }

  public function updateluarantambahan(){
    $ptlw = Penelitian_target_luaran_tambahan::find($this->input->post('id'));
    $ptlw->penelitian_id = $this->input->post('penelitian_id');
        $ptlw->target_luaran_master_id = $this->input->post('target_luaran_master');
        $ptlw->target_luaran_sub_id = $this->input->post('target_luaran_sub');
        $ptlw->target_luaran_status_id = $this->input->post('target_luaran_status');
        $ptlw->url_jurnal = $this->input->post('url_jurnal');
        $ptlw->rencana_jurnal = $this->input->post('rencana_jurnal');
        $ptlw->bukti_luaran = $this->input->post('bukti_luaran');
        $ptlw->save();
    echo json_encode(['code' => 200]);
    exit;
  }

	private function dataExcel(){
		$penelitian = Penelitian::where("isdelete","=","0")->where('isvalid','=','1')->where('isdelete','=','0')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen.fakultas')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->with('Penelitian_review')->with('Penelitian_reviewer');
		$penelitian = $this->filter($penelitian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
 				$q->whereHas('penelitian_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
	 	}
		$penelitian = $penelitian->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch');
		$penelitian->load('jenis_penelitian');
		$penelitian->load('penelitian_reviewer.dosen');
		$penelitian->load('tahun_kegiatan');
 	// 	echo $penelitian->toJson();
		// die;
		return $penelitian->toArray();
	}

 	public function download(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
 			'borders' => array(
 				'bottom' => $default_border,
 				'left' => $default_border,
 				'top' => $default_border,
 				'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'E1E0F7'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 16,
 				)
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				// 'bottom' => $default_border,
 				// 'left' => $default_border,
 				// 'top' => $default_border,
 				// 'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_penelitian_no_reviewer.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		// ->setLastModifiedBy("Sigit prasetya n")
 		// ->setTitle("Creating file excel with php Test Document")
 		// ->setSubject("Creating file excel with php Test Document")
 		// ->setDescription("How to Create Excel file from PHP with PHPExcel 1.8.0 Classes by seegatesite.com.")
 		// ->setKeywords("phpexcel")
 		// ->setCategory("Test result file");
 		$excel->setActiveSheetIndex(0);
		// $dataku=array(
		// 	array('C001','Iphone 6'),
		// 	array('C002','Samsung Galaxy S4'),
		// 	array('C003','Nokia Lumia'),
		// 	array('C004','Blackberry Curve'));
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_penelitian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$lokasi = 'G'.$urut;
			$anggaran = 'H'.$urut;
			$fakultas = 'I'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_penelitian, $data[$i]['judul'])->mergeCells($judul_penelitian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($lokasi, $data[$i]['jenis_penelitian']["nama"])
			->setCellValue($anggaran, $data[$i]['rp_total_biaya'])
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['penelitian_reviewer'])

			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle=$fakultas;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		for($col = 'A'; $col !== 'N'; $col++) {
		    $excel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
		}
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_UsulanPenelitian_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
