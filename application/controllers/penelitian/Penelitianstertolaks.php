<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Penelitianstertolaks extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $menu = array("mp"=>"active","mp4"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_select"],
		    "functions_safe" => ["form_open","form_open_multipart"],
		];
        parent::__construct($config);
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Penelitian");
    	$this->load->model("Penelitian_anggota");
    	$this->load->model("Pengesah");
    	$this->load->model("Penilaian");
    	$this->load->model("Penilaian_kriteria");
    	$this->load->model("Penelitian_review");
    	$this->load->model("Penelitian_reviewer");
    	$this->load->model("Penelitian_nilai");
    	$this->load->model("Batch_lists");
    }	
	public function index()
	{
		$data = [];
		$jenis_penelitian = Jenis_penelitian::where("isdelete","=","0")->get();
		// $jenis_penelitian->load("batch_penelitian");
		$data["jenis_penelitian"] = $jenis_penelitian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();
		
		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','12')
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen_menyetujui')
										 ->with('dosen_menyetujui.jabatan_akademik')
										 ->with('dosen_menyetujui.program_studi')
										 ->with('dosen_menyetujui.jabatan_fungsional')
										 ->with('dosen_mengetahui')
										 ->with('dosen_mengetahui.jabatan_akademik')
										 ->with('dosen_mengetahui.program_studi')
										 ->with('dosen_mengetahui.jabatan_fungsional')
										 ->with('Penelitian_review')->with('Penelitian_reviewer')->with('penelitian_review_inselected');
		$penelitian = $this->filter($penelitian,$data);
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
 				$q->whereHas('penelitian_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
	 	}
		$info = $this->create_paging($penelitian);
		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch');
		$penelitian->load('jenis_penelitian');
		$penelitian->load('tahun_kegiatan');

		$data["penelitian"] = $penelitian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo json_encode($data);
		// die;
		$this->twig->display("penelitian/tertolak/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_penelitian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_penelitian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}
		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}
}
