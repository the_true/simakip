<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Manageakreditas extends MY_BaseController {

  private $menu = array("akreditas"=>"active");
  public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model('Tahun_kegiatan');
        $this->load->model('Akreditas');
        $this->load->model('Akreditas_dosen');
        $this->load->model('Dosen');
        $this->load->model('Fakultas');
        $this->load->model('Program_studi');
        $this->load->model('Jabatan_akademik');

        $this->load->model('Penelitian');

        $this->load->model('luaran_penelitian/Jurnal');
        $this->load->model('luaran_penelitian/Buku_ajar');
        $this->load->model('luaran_penelitian/Forum_ilmiah');
        $this->load->model('luaran_penelitian/Hki');
        $this->load->model('luaran_penelitian/Luaran_lain');
        $this->load->model('luaran_penelitian/Penyelenggara_forum_ilmiah');
        $this->load->model('luaran_penelitian/Penelitian_hibah');
  }

  public function index(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $tahun = Tahun_kegiatan::where('isdelete','=',0)->get();

    $akreditas = Akreditas::where('isdelete','=',0);
    if($this->is_login()["isketuaprodi"]==1 and role('Dosen')){
        $akreditas = $akreditas->where('dosen','=',$this->is_login()['id'])->orWhere(function($q){
          $q->where('fakultas','=',$this->is_login()['fakultas']['id'])->where('program_studi','=',$this->is_login()['program_studi']['id']);
        });
    }
    $info = $this->create_paging($akreditas);
    $akreditas = $this->filter($akreditas);
    $akreditas = $akreditas->orderBy('id','desc')->take($info["limit"])->skip($info["skip"])->get();

    $akreditas->load('dosen')->load('fakultas')->load('program_studi')->load('tahun_dari')->load('tahun_sampai');
    $akreditas->load('dosen.jabatan_akademik');
    $fakultas = Fakultas::where('isdelete','=','0')->get();

    $data = [];
    $data["tahun"] = $tahun->toArray();
    $data["fakultas"] = $fakultas->toArray();
    $data["items"] = $akreditas->toArray();
    $this->twig->display('akreditas/index',array_merge($this->menu,$data,$info));
  }

  private function filter($akreditas){
    $nama = $this->input->get('ketua_prodi');
    if($nama){
      $akreditas = $akreditas->whereHas('dosen',function($q){
        $q->where('nama','LIKE','%'.$this->input->get('ketua_prodi').'%');
      });
    }

    $fakultas = $this->input->get('fakultas');
    if($fakultas && $fakultas != 0){
      $akreditas = $akreditas->where('fakultas','=',$fakultas);
    }

    $program_studi = $this->input->get('program_studi');
    if($program_studi && $program_studi != 0){
      $akreditas = $akreditas->where('program_studi','=',$program_studi);
    }

    $tahun = $this->input->get('tahun');
    if($tahun){
      $akreditas = $akreditas->whereHas('tahun_dari',function($q){
        $q->whereRaw('convert(tahun,UNSIGNED INTEGER) <= ?',[$this->input->get('tahun')]);
      })->whereHas('tahun_sampai',function($q){
        $q->whereRaw('convert(tahun,UNSIGNED INTEGER) >= ?',[$this->input->get('tahun')]);
      });
    }
    // echo $akreditas->toSql();
    // die;
    return $akreditas;
  }

  public function insert(){
      $tahun_dari = $this->input->post('tahun_dari');
      $tahun_sampai = $this->input->post('tahun_sampai');
      $dosen = $this->is_login()['id'];
      $fakultas = $this->is_login()['fakultas']['id'];
      $program_studi = $this->is_login()['program_studi']['id'];

      $data = new Akreditas;
      $data->dosen = $dosen;
      $data->fakultas = $fakultas;
      $data->program_studi = $program_studi;
      $data->tahun_dari = $tahun_dari;
      $data->tahun_sampai = $tahun_sampai;
      $data->save();

      $dosen = Dosen::where('isdelete','=','0')->where('program_studi','=',$program_studi)->where('fakultas','=',$fakultas)->get();
      foreach($dosen as $dos){
        $q = Akreditas_dosen::firstOrNew(["dosen"=>$dos->id,"akreditas"=>$data->id]);
        $q->save();
      }

      redirect("akreditas/manageakreditas/");
  }

  public function edit(){
    $id = $this->uri->segment(4);
    $tahun_dari = $this->input->post('tahun_dari');
    $tahun_sampai = $this->input->post('tahun_sampai');

    $data = Akreditas::find($id);
    $data->tahun_dari = $tahun_dari;
    $data->tahun_sampai = $tahun_sampai;
    $data->save();

    redirect($_SERVER['HTTP_REFERER']);
  }

  public function delete(){
    $id = $this->uri->segment(4);
    $item = Akreditas::find($id);
    if(!$this->isPengguna($item->dosen)) return;
    $item->delete();# = 1;
    #$item->save();
    redirect($_SERVER['HTTP_REFERER']);
    // redirect('akreditas/manageakreditas/');
  }

}




 ?>
