<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Akreditasdosen extends MY_BaseController {

  private $menu = array("akreditas"=>"active");
  public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model('Akreditas');
        $this->load->model('Tahun_kegiatan');
        $this->load->model('Akreditas_dosen');
        $this->load->model('Dosen');
        $this->load->model('Fakultas');
        $this->load->model('Program_studi');
        $this->load->model('Jabatan_akademik');

        $this->load->model('Penelitian');

        $this->load->model('luaran_penelitian/Jurnal');
        $this->load->model('luaran_penelitian/Buku_ajar');
        $this->load->model('luaran_penelitian/Forum_ilmiah');
        $this->load->model('luaran_penelitian/Hki');
        $this->load->model('luaran_penelitian/Luaran_lain');
        $this->load->model('luaran_penelitian/Penyelenggara_forum_ilmiah');
        $this->load->model('luaran_penelitian/Penelitian_hibah');
  }

  public function index(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    
    $id_akreditas = $this->uri->segment(3);

    $akreditas = Akreditas::find($id_akreditas);
    // $akreditas->load('anggota')
    // echo $akreditas->toJson();
    // die;
    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data = [];

    $anggota_akreditas = Akreditas_dosen::where('akreditas','=',$id_akreditas);
    $anggota_akreditas = $this->filter($anggota_akreditas);
    $info = $this->create_paging($anggota_akreditas);
    $anggota_akreditas = $anggota_akreditas->take($info["limit"])->skip($info["skip"])->get();
    $anggota_akreditas->load('dosen');
    $anggota_akreditas->load('dosen.jabatan_akademik');

    $data["akreditass"] = $akreditas->toArray();
    $data["items"] = $anggota_akreditas->toArray();
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/index_akreditas',array_merge($this->menu,$data,$info));
  }

  public function listluaran(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");

    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data = [];

    $anggota_akreditas = Akreditas_dosen::where('akreditas','=',$id_akreditas)->where("dosen","=",$dosen)->first();
    $anggota_akreditas->load('dosen')->load('dosen.jabatan_akademik');
    // echo $anggota_akreditas->toJson(); die;


    $data["items"] = $anggota_akreditas->toArray();
    $data["akreditass"] = $akreditas->toArray();
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/list_luaran',array_merge($this->menu,$data));
  }

  public function detailjurnal(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Jurnal::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun_kegiatan');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_jurnal',array_merge($this->menu,$data,$info));
  }

  public function detailbukuajar(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Buku_ajar::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun_kegiatan');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_bukuajar',array_merge($this->menu,$data,$info));
  }

  public function detailforumilmiah(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun_kegiatan');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_forumilmiah',array_merge($this->menu,$data,$info));
  }

  public function detailhki(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Hki::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun_kegiatan');
    $this->load->model('Jenis_hki');
    $items->load('jenis_hki');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_hki',array_merge($this->menu,$data,$info));
  }

  public function detailluaranlain(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Luaran_lain::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun_kegiatan');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_luaranlain',array_merge($this->menu,$data,$info));
  }

  public function detailpforumilmiah(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Penyelenggara_forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun_kegiatan');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_pforumilmiah',array_merge($this->menu,$data,$info));
  }

  public function detailmandiri(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }

    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Penelitian_hibah::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('tahun');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_mandiri',array_merge($this->menu,$data,$info));
  }

  public function detailpenelitian(){
    if(!$this->isAkses(['Rektor','Wakil Rektor','Ketua Lemlitbang',"Sekretaris Lemlitbang","Operator Lemlitbang"]) && $this->is_login()["isketuaprodi"]!=1){
      $this->twig->display('404');
      return;
    }
    
    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Penelitian::where('isdelete','=','0')->where('status','=','4')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $this->load->model('Penelitian_review');
    $this->load->model('Penelitian_reviewer');
    $this->load->model('Batch');
    $this->load->model('Batch_penelitian');
    $this->load->model('Jenis_penelitian');
    $this->load->model('Penilaian');
    $this->load->model('Penilaian_kriteria');
    $this->load->model('Penelitian_laporan');
    $items->load('batch');
    $items->load('dosen')->load('dosen.jabatan_akademik');
    $items->load('jenis_penelitian');
    $items->load('tahun_kegiatan');
    $items->load('Penelitian_review');
    $items->load('Penelitian_reviewer');
    $items->load('Penelitian_laporan');
    // echo $items->toJson(); die;


    $data["items"] = $items->toArray();
    
    $data["id"] = $id_akreditas;
    $this->twig->display('akreditas/detail/detail_penelitian',array_merge($this->menu,$data,$info));
  }

  public function add(){
    $id_akreditas = $this->uri->segment(4);
    $fakultas_selected = $this->input->get('fakultas') ? $this->input->get('fakultas') : $this->is_login()['fakultas']["id"];
    $fakultas = Fakultas::where("isdelete","=","0")->get();
    $program_studi_selected = $this->input->get('program_studi') ? $this->input->get('program_studi') : $this->is_login()['program_studi']["id"];
    $program_studi = Program_studi::where("isdelete","=","0")->where("fakultas","=",$fakultas_selected)->get();

    $dosen = Dosen::where("isdelete","=",0)->where("fakultas","=",$fakultas_selected)->where("program_studi","=",$program_studi_selected)
                    ->whereDoesntHave('akreditas_dosen',function($q){
                      $q->where('akreditas','=',$this->uri->segment(4));
                    });

    $nama_get = $this->input->get("nama");
    if(isset($nama_get) && $nama_get!=''){
      $dosen->where("nama","LIKE","%".$this->input->get("nama")."%");
    }
    
    // echo $dosen->toSql(); die;
    $info = $this->create_paging($dosen);
    $dosen = $dosen->take($info["limit"])->skip($info["skip"])->get();
    $data = [];
    $data["id"] = $id_akreditas;
    $data["fakultas_selected"] = $fakultas_selected;
    $data["fakultas"]=$fakultas->toArray();
    $data["program_studi_selected"] = $program_studi_selected;
    $data["program_studi"] = $program_studi->toArray();
    $data["items"] = $dosen->toArray();
    $this->twig->display('akreditas/add_dosen',array_merge($this->menu,$data,$info));
  }

  private function filter($dosen){
    $nidn_get = $this->input->get("nidn");
    if(isset($nidn_get) && $nidn_get!='' && is_numeric($nidn_get)){
      $dosen->whereHas('dosen',function($q){$q->where('nidn','=',$this->input->get("nidn"));});
    }

    $nama_get = $this->input->get("nama");
    if(isset($nama_get) && $nama_get!=''){
      $dosen->whereHas('dosen',function($q){$q->where("nama","LIKE","%".$this->input->get("nama")."%");});
    }
    return $dosen;
  }

  public function delete(){
    $id = $this->uri->segment(4);
    $item = Akreditas_dosen::find($id);
    $this->session->set_flashdata('success', 'Delete anggota akreditasi berhasil!');
    $item->delete();
    redirect($_SERVER['HTTP_REFERER']);
    // redirect('akreditas/akreditasdosen/'.$this->input->get('akreditas'));
  }

  public function add_satuan(){
    $dosen = $this->uri->segment(4);
    $akreditas = $this->input->get("akreditas");

    $q = Akreditas_dosen::firstOrNew(["dosen"=>$dosen,"akreditas"=>$akreditas]);
    $q->save();

    redirect('akreditas/akreditasdosen/add/'.$akreditas);
  }

  public function add_semua(){
    $dosens = $this->input->post("dosen");
    $akreditas = $this->input->post("akreditas");

    foreach($dosens as $dosen){
        $q = Akreditas_dosen::firstOrNew(["dosen"=>$dosen,"akreditas"=>$akreditas]);
        $q->save();
    }
    redirect('akreditas/akreditasdosen/'.$akreditas);
  }

  public function pdf(){
      $this->load->helper('pdf_helper');
      tcpdf();
      $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $obj_pdf->SetCreator(PDF_CREATOR);
      $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
      $obj_pdf->SetTitle($title);
      $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $obj_pdf->SetDefaultMonospacedFont('helvetica');
      $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
      $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $obj_pdf->SetFont('helvetica', '', 9);
      $obj_pdf->setFontSubsetting(false);
      $obj_pdf->setListIndentWidth(1);
      $obj_pdf->AddPage();


      ob_start();
      $user = $this->is_login();
      $id_akreditas = $this->uri->segment(4);

      $akreditas = Akreditas::find($id_akreditas);
      $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
      $data = [];

      $anggota_akreditas = Akreditas_dosen::where('akreditas','=',$id_akreditas)->get();
      $anggota_akreditas = $this->filter($anggota_akreditas);
      $anggota_akreditas->load('dosen');

      $data["akreditass"] = $akreditas->toArray();
      $data["items"] = $anggota_akreditas->toArray();
      $data["jabatan"] = "Ketua Lemlitbang UHAMKA";
      $data["nama"] = "Prof. Dr. Suswandari, M. Pd.";

      $this->load->view('akreditas/pdfmanagedosen',$data);
      $content = ob_get_contents();
      ob_end_clean();
      $obj_pdf->writeHTML($content, true, false, true, false, '');
      $obj_pdf->Output($this->is_login()['nidn'].'_Akreditasi_dosen_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfjurnal(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Jurnal::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->with('personil')->get();
    $items->load('dosen');
    $items->load('tahun_kegiatan');

    $this->load->view('akreditas/pdf/pdf_jurnal',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_Jurnal_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfbukuajar(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Buku_ajar::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->get();
    $items->load('dosen');
    $items->load('tahun_kegiatan');

    $this->load->view('akreditas/pdf/pdf_bukuajar',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_BukuAjar_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfforumilmiah(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->get();
    $items->load('dosen');
    $items->load('tahun_kegiatan');

    $this->load->view('akreditas/pdf/pdf_forumilmiah',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_ForumIlmiah_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfhki(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Hki::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->get();
    $items->load('dosen');
    $items->load('tahun_kegiatan');
    $this->load->model('Jenis_hki');
    $items->load('jenis_hki');

    $this->load->view('akreditas/pdf/pdf_hki',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_HKI_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfluaranlain(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Luaran_lain::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->get();
    $items->load('dosen');
    $items->load('tahun_kegiatan');
    $this->load->model('Jenis_luaran');
    $items->load('Jenis_luaran');

    $this->load->view('akreditas/pdf/pdf_luaranlain',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_LuaranLain_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfpforumilmiah(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Penyelenggara_forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->get();
    $items->load('dosen');
    $items->load('tahun_kegiatan');

    $this->load->view('akreditas/pdf/pdf_pforumilmiah',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_PenyelenggaraForumIlmiah_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfmandiri(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Penelitian_hibah::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen)->get();
    $items->load('dosen');
    $items->load('tahun');

    $this->load->view('akreditas/pdf/pdf_mandiri',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_PenelitianMandiri_'.Date('dmY').'.pdf', 'I');
  }

  public function pdfpenelitian(){
    $this->load->helper('pdf_helper');
    tcpdf();
    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Sistem Informasi Manajemen & Kinerja Penelitian";
    $obj_pdf->SetTitle($title);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->setListIndentWidth(1);
    $obj_pdf->AddPage();


    ob_start();
    $user = $this->is_login();
    $type_get = $this->input->get("type");



    $id_akreditas = $this->uri->segment(4);
    $dosen = $this->input->get("dosen");
    $dosenss = Dosen::find($dosen);
    $data = [];
    $akreditas = Akreditas::find($id_akreditas);

    $akreditas->load('fakultas')->load('program_studi')->load('dosen')->load('tahun_sampai')->load('tahun_dari');
    $data["akreditass"] = $akreditas->toArray();

    $this->tahun_range = $akreditas["tahun_range_array"];
    // echo print_r($this->tahun_range); die;

    $items = Penelitian::where('isdelete','=','0')->where('status','=','4')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->where("dosen","=",$dosen);
    $info = $this->create_paging($items);
    $items = $items->take($info["limit"])->skip($info["skip"])->get();
    $this->load->model('Penelitian_review');
    $this->load->model('Penelitian_reviewer');
    $this->load->model('Batch');
    $this->load->model('Batch_penelitian');
    $this->load->model('Jenis_penelitian');
    $this->load->model('Penilaian');
    $this->load->model('Penilaian_kriteria');
    $this->load->model('Penelitian_anggota');
    $items->load('anggota')->load('anggota.dosen');
    $items->load('batch');
    $items->load('dosen');
    $items->load('jenis_penelitian');
    $items->load('tahun_kegiatan');
    $items->load('Penelitian_review');
    $items->load('Penelitian_reviewer');

    $this->load->view('akreditas/pdf/pdf_penelitian',array("items"=>$items->toArray(),"akreditas"=>$akreditas->toArray(),"dosen"=>$dosenss->toArray()));
      $content = ob_get_contents();
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output($this->is_login()['nidn'].'_Penelitian_'.Date('dmY').'.pdf', 'I');
  }

}