<?php
defined("BASEPATH") or exit("No direct script access allowed");
include_once "application/core/MY_BaseController.php";

class Targetluaranmasters extends MY_BaseController
{

    /**
     * @category Libraries
     * @package  CodeIgniter 3.0
     * @author   Yp <purwantoyudi42@gmail.com>
     * @link     https://timexstudio.com
     * @license  Protected
     */
    private $menu = array("p" => "active", "p1" => "active");
    public function __construct()
    {
        $config = [
            "functions" => ["anchor", "set_value", "set_radio"],
            "functions_safe" => ["validation_errors_array", "form_open"],
        ];
        parent::__construct($config);
        $this->load->model("Target_luaran_master");
        $this->load->model("Target_luaran_sub");
        $this->load->model("Target_luaran_status");
    }

    public function index()
    {
        // $item = Target_luaran_master::where("isdelete", "=", "0");
        $item = Target_luaran_master
                ::with(['target_luaran_status' => function($query)
                        {
                             $query->where('isdelete', 0);
                        }, 'target_luaran_sub' => function($query)
                        {
                             $query->where('isdelete', 0);
                        }])
                ->where("isdelete", "=", "0");
        
        $info = $this->create_paging($item);
        $item = $item->take($info["limit"])->skip($info["skip"])->get();
        $items = ["items" => $item->toArray()];
        $items = array_merge($items, $info);
        $this->twig->display("pengaturan/penelitian/target_luaran_master/index", array_merge($items, $this->menu));
    }
    
    public function json($id_target_luaran)
    {
        $item = Target_luaran_master
                ::with(['target_luaran_status' => function($query)
                        {
                             $query->where('isdelete', 0);
                        }, 'target_luaran_sub' => function($query)
                        {
                             $query->where('isdelete', 0);
                        }])
                ->where("id", "=", $id_target_luaran)
                ->where("isdelete", "=", "0")
                ->first()
                ->toArray();
        $item = json_encode($item);
        echo $item;
        exit;
        
        $info = $this->create_paging($item);
        $item = $item->take($info["limit"])->skip($info["skip"])->get();
        $items = ["items" => $item->toArray()];
        $items = array_merge($items, $info);
        $this->twig->display("pengaturan/penelitian/target_luaran_master/index", array_merge($items, $this->menu));
    }

    public function add()
    {
        $user = $this->is_login();
        $data = $this->insert();
        if ($data) {
            redirect("pengaturan/penelitian/targetluaranmasters/");
        }

        redirect('login/pagenotfound', 'refresh');
    }

    public function edit()
    {
        $user = $this->is_login();
        $data = $this->update();
        if ($data) {
            redirect("pengaturan/penelitian/targetluaranmasters/" . $data->jenis_penelitian);
        }

        redirect('login/pagenotfound', 'refresh');
    }
    
    public function sub()
    {
        $user = $this->is_login();
        $data = $this->updateSub();
        if ($data) {
            redirect("pengaturan/penelitian/targetluaranmasters/" . $data->jenis_penelitian);
        }

        redirect('login/pagenotfound', 'refresh');
    }

    public function insert()
    {
        $nama = $this->input->post("nama");
        if (empty($nama)) {
            return false;
        }

        $data = new Target_luaran_master;
        $data->nama = $nama;
        $data->save();

        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'insert', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' membuat konfigurasi Penelitian-Target Luaran Master baru dengan nama batch ' . $nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);

        return $data;
    }

    public function update()
    {
        $id = $this->uri->segment(5);
        $nama = $this->input->post("nama");
        if (empty($nama)) {
            return false;
        }

        $data = Target_luaran_master::find($id);
        $data->nama = $nama;
        $data->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah konfigurasi Penelitian-Target Luaran Master dengan nama batch ' . $nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        return $data;
    }
    
    
    public function updateSub()
    {
        $id = $this->uri->segment(5);
        
        $nama_luaran = [];
        $nama_luaran_edit = [];
        
        foreach ($this->input->post('nama_luaran') as $key => $value) {
            if (!isset($this->input->post('nama_luaran_id')[$key])) {
                $nama_luaran[$key]['target_luaran_master_id'] = $this->input->post('_id');
                $nama_luaran[$key]['nama'] = $value;
            }else {
                $nama_luaran_edit[$key]['target_luaran_master_id'] = $this->input->post('_id');
                $nama_luaran_edit[$key]['nama'] = $value;
                $nama_luaran_edit[$key]['id'] = $this->input->post('nama_luaran_id')[$key];
            }
        }
        
        $status_luaran = [];
        $status_luaran_edit = [];
        
        foreach ($this->input->post('status_luaran') as $key => $value) {
            if (!isset($this->input->post('status_luaran_id')[$key])) {
                $status_luaran[$key]['target_luaran_master_id'] = $this->input->post('_id');
                $status_luaran[$key]['status'] = $value;
            }else {
                $status_luaran_edit[$key]['target_luaran_master_id'] = $this->input->post('_id');
                $status_luaran_edit[$key]['status'] = $value;
                $status_luaran_edit[$key]['id'] = $this->input->post('status_luaran_id')[$key];
            }
        }
        
        $nama = $this->input->post("nama");
        $catatan1 = $this->input->post("catatan1");
        $catatan2 = $this->input->post("catatan2");
        
        if (empty($nama)) {
            return false;
        }

        $data = Target_luaran_master::find($id);
        $data->nama = $nama;
        $data->catatan1 = $catatan1;
        $data->catatan2 = $catatan2;
        $data->save();
        
        $target_luaran_sub = Target_luaran_sub::insert($nama_luaran);
        $target_luaran_status = Target_luaran_status::insert($status_luaran);
        
        if (!empty($nama_luaran_edit)) {
            foreach ($nama_luaran_edit as $key => $value) {
                $tlsub = Target_luaran_sub::find($value['id']);
                $tlsub->nama = $value['nama'];
                $tlsub->save();
            }
        }
        
        if (!empty($status_luaran_edit)) {
            foreach ($status_luaran_edit as $key => $value) {
                $tlstatus = Target_luaran_status::find($value['id']);
                $tlstatus->status = $value['status'];
                $tlstatus->save();
            }
        }

        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah konfigurasi Penelitian-Target Luaran Master dengan nama batch ' . $nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        return $data;
    }

    public function delete()
    {
        $id = $this->uri->segment(5);
        $item = Target_luaran_master::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'delete', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menghapus konfigurasi Penelitian-Target Luaran Master dengan nama batch ' . $item->nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        redirect("pengaturan/penelitian/targetluaranmasters/");
    }
    
    public function deleteluaransub()
    {
        $id = $this->input->post('id');
        $tlsub = Target_luaran_sub::find($id);
        $tlsub->isdelete = 1;
        $tlsub->save();
        
        echo json_encode(['code'=>200, 'msg'=>$tlsub]);
    }
    
    public function deleteluaranstatus()
    {
        $id = $this->input->post('id');
        $tlsub = Target_luaran_status::find($id);
        $tlsub->isdelete = 1;
        $tlsub->save();
        
        echo json_encode(['code'=>200, 'msg'=>$tlsub]);
    }
}
