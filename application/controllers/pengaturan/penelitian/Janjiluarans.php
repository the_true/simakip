<?php
defined("BASEPATH") or exit("No direct script access allowed");
include_once "application/core/MY_BaseController.php";

class Janjiluarans extends MY_BaseController
{

    /**
     * @category Libraries
     * @package  CodeIgniter 3.0
     * @author   Yp <purwantoyudi42@gmail.com>
     * @link     https://timexstudio.com
     * @license  Protected
     */
    private $menu = array("p" => "active", "p1" => "active");
    public function __construct()
    {
        $config = [
            "functions" => ["anchor", "set_value", "set_radio"],
            "functions_safe" => ["validation_errors_array", "form_open"],
        ];
        parent::__construct($config);
        $this->load->model("Janji_luaran");
        $this->load->model("Jenis_penelitian");
    }

    public function index()
    {
        $id = $this->uri->segment(2);
        $data = Jenis_penelitian::find($id);
        $ids = $data->toArray();
        
        $item = Janji_luaran::where('jenis_penelitian_id', $id)->where("isdelete", "=", "0");
        
        $info = $this->create_paging($item);
        $item = $item->take($info["limit"])->skip($info["skip"])->get();
        $items = ["jenpen"=>$ids,"items" => $item->toArray()];
        $items = array_merge($items, $info);
        $this->twig->display("pengaturan/penelitian/janji_luaran/index", array_merge($items, $this->menu));
    }

    public function add()
    {
        $user = $this->is_login();
        $data = $this->insert();
        if ($data) {
            redirect("/janjiluarans/".$data->jenis_penelitian_id);
        }

        redirect('login/pagenotfound', 'refresh');
    }

    public function edit()
    {
        $user = $this->is_login();
        $data = $this->update();
        if ($data) {
            redirect("/janjiluarans/" . $data->jenis_penelitian_id);
        }

        redirect('login/pagenotfound', 'refresh');
    }

    public function insert()
    {
        $informasi = $this->input->post("informasi");
        $jenis_penelitian_id = $this->input->post("jenis_penelitian_id");
        if (empty($informasi)) {
            return false;
        }

        $data = new Janji_luaran();
        $data->jenis_penelitian_id = $jenis_penelitian_id;
        $data->informasi = $informasi;
        $data->save();

        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'insert', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' membuat konfigurasi Penelitian-Janji Luaran baru dengan informasi ' . $informasi, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);

        return $data;
    }

    public function update()
    {
        $id = $this->uri->segment(5);
        $informasi = $this->input->post("informasi");
        $jenis_penelitian_id = $this->input->post("jenis_penelitian_id");
        if (empty($informasi)) {
            return false;
        }

        $data = Janji_luaran::find($id);
        $data->jenis_penelitian_id = $jenis_penelitian_id;
        $data->informasi = $informasi;
        $data->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah konfigurasi Penelitian-Janji Luaran dengan informasi ' . $informasi, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        return $data;
    }

    public function delete()
    {
        $id = $this->uri->segment(5);
        $jenis_penelitian_id = $this->uri->segment(7);
        $item = Janji_luaran::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'delete', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menghapus konfigurasi Penelitian-Janji Luaran dengan informasi ' . $item->nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        redirect("/janjiluarans/".$jenis_penelitian_id);
    }
}
