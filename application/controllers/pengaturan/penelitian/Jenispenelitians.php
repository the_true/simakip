<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Jenispenelitians extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Jenis_penelitian');
        $this->load->model('Jabatan_akademik');
        $this->load->model('Batas_anggaran');
        $this->load->model('Batch_penelitian');
        $this->load->model('Penilaian');
        $this->load->model('Penilaian_kriteria');
        $this->load->model('Jabatan_akademik');
        $this->load->model('Batch');
    }

	public function index(){
		$data = Jenis_penelitian::where('isdelete', '=', '0')->orderBy('status','asc');
		//take out page
		// $info = $this->create_paging($data);
		// $data = $data->take($info["limit"])->skip($info["skip"])->get();
		// $items = array_merge($items,$info);
		$data = $data->get();
		$data->load("batas_anggaran_aktif")->load("jabatan_akademik");
		$jabak = Jabatan_akademik::where('isdelete','=', '0')->get();
		// $data->load("dosen")->load("dosen.jabatan_akademik"); 
		$data = $data->toArray();  
		$items = array("items"=>$data,"jabatan_akademik"=>$jabak->toArray());
		$this->twig->display('pengaturan/penelitian/jenis_penelitian/index',array_merge($items,$this->menu));
	}

	public function moveElement(&$array,$a,$b){
		$out = array_splice($array,$a,1);
		array_splice($array,$b,0,$out);
	}

	public function updposition(){  
		$data = Jenis_penelitian::where('isdelete', '=', '0')->orderBy('status','asc')->get();
		// $data->load("batas_anggaran_aktif")->load("jabatan_akademik");
		$old = $this->input->post("old");
		$new = $this->input->post("new");
		$data = $data->toArray();
		$this->moveElement($data, $old , $new); 
		foreach($data as $i => $object){
			$jenis_penelitian = $object['id'];//$data->toArray()[$old]['id']; 
			$data = Jenis_penelitian::find($jenis_penelitian);
			$data->status = $i; 
			$data->save();
		} 
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
    	if ($data)
    		redirect("/batasanggarans/".$data->id);
    	redirect("/jenispenelitians/");
    }

    public function edit(){
		$jenis_penelitian = $this->uri->segment(3); 
		$nama = $this->input->post("nama");
		$syarat = $this->input->post("syarat");		
		$jabatan_akademik = $this->input->post("jabatan_akademik");
		$data = Jenis_penelitian::find($jenis_penelitian);
		$data->nama = $nama;
		$data->syarat = $syarat;
		$data->jabatan_akademik = $jabatan_akademik;
		$data->status = $status;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("jenispenelitians/");
    }

    public function insert(){
		$nama = $this->input->post("nama");
		$syarat = $this->input->post("syarat");		
		$jabatan_akademik = $this->input->post("jabatan_akademik");
		if(empty($nama)) return False;
		$data = new Jenis_penelitian;
		$data->nama = $nama;
		$data->syarat = $syarat;
		$data->jabatan_akademik = $jabatan_akademik;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

	public function delete(){
		$id = $this->uri->segment(3);
		$item = Jenis_penelitian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('jenispenelitians/');
	}
}
