<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Batchpenelitians extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch_lists");
        $this->load->model("Batch");
        $this->load->model('Batas_anggaran');
        $this->load->model('Penilaian');
        $this->load->model('Penilaian_kriteria');
        $this->load->model('Tahun_kegiatan');
    }

	public function index(){
		$id = $this->uri->segment(2);
		$data = Jenis_penelitian::find($id);
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		// $items = array("items"=>$data->toArray());
		$item = Batch_penelitian::where("isdelete","=","0")->where("jenis_penelitian","=",$id)->orderBy('tahun','DESC');
		$info = $this->create_paging($item);
		$item = $item->take($info["limit"])->skip($info["skip"])->get();
		$item->load("batch")->load("batch.batch_lists");
		$item->load("tahun");
		$id = $data->toArray();
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->get();
		$items = ["jenpen"=>$id,"items"=>$item->toArray(),"tahun_kegiatan"=>$tahun->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display("pengaturan/penelitian/batch_penelitian/index",array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/batchpenelitians/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
			redirect("/batchpenelitians/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function insert(){
		$jenis_penelitian = $this->uri->segment(3);
		$tahun = $this->input->post("tahun");
		if(empty($jenis_penelitian) || empty($tahun)) return False;
		$data = new Batch_penelitian;
		$data->jenis_penelitian = $jenis_penelitian;
		$data->tahun = $tahun;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Batch Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

    public function update(){
    	$id = $this->uri->segment(3);
    	$tahun = $this->input->post("tahun");
    	$jenis_penelitian = $this->input->post("jenis_penelitian");
    	if(empty($jenis_penelitian) || empty($tahun)) return False;
    	$data = Batch_penelitian::find($id);
    	$data->tahun = $tahun;
    	$data->save();
    	Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian-Batch Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    	return $data;
    }

	public function delete(){
		$id = $this->uri->segment(3);
		$jenis_penelitian = $this->uri->segment(5);
		$item = Batch_penelitian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian-Batch Penelitian dengan tahun '.$item->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("batchpenelitians/".$jenis_penelitian);
	}

	public function is_exist(){
 		$data = Batch_penelitian::where('tahun','=',$this->input->get('tahun'))->where('jenis_penelitian','=',$this->input->get('jenis_penelitian'))->where('isdelete','=','0')->count();
 		if($data>0){
			$this->output->set_header('HTTP/1.0 470 Data sudah ada');
			return;
 		}
		$this->output->set_header('HTTP/1.0 200 OK');
	}

    public function json(){
    	$id = $this->uri->segment(3);
    	$data = Batch_penelitian::where("isdelete","=","0")->where("jenis_penelitian","=",$id)->get();
    	$data = $data->load('tahun');
    	echo $data->toJson();
    }
}
