<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Batasanggarans extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Jenis_penelitian");
        $this->load->model('Batas_anggaran');
        $this->load->model('Batch_penelitian');
        $this->load->model('Penilaian');
        $this->load->model('Penilaian_kriteria');
        $this->load->model('Batch');
    }

	public function index(){
		$id = $this->uri->segment(2);
		$data = Jenis_penelitian::find($id);
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		// $items = array("items"=>$data->toArray());
		$item = Batas_anggaran::where('isdelete','=','0')->where('jenis_penelitian','=',$id);
		$info = $this->create_paging($item);
		$item = $item->take($info["limit"])->skip($info["skip"])->get();
		$id = $data->toArray();
		$items = ["jenpen"=>$id,"items"=>$item->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display("pengaturan/penelitian/batas_anggaran/index",array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/batasanggarans/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
			redirect("/batasanggarans/".$data->jenis_penelitian);
		// redirect('login/pagenotfound', 'refresh');
    }

    public function insert(){
		$jenis_penelitian = $this->uri->segment(3);
		$batas = $this->input->post("batas");
		$status = $this->input->post("status");
		if(empty($jenis_penelitian) || empty($batas) || is_null($status)) return False;
		$data = new Batas_anggaran;
		$data->jenis_penelitian = $jenis_penelitian;
		$data->batas = $batas;
		$data->status = $status;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Batas Anggaran dengan batasan '.$batas,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

    public function update(){
    	$id = $this->uri->segment(3);
    	// $batas = $this->input->post("batas");
    	$status = $this->input->post("status");
    	$jenis_penelitian = $this->input->post("jenis_penelitian");
    	if(empty($jenis_penelitian) || is_null($status)) return False;
		if($status=="1"){
			$updates = Batas_anggaran::where("isdelete","=","0")->where("jenis_penelitian","=",$jenis_penelitian)->get();
			foreach($updates as $d){
				$d->status="0";
				$d->save();
			}
		}
    	$data = Batas_anggaran::find($id);
    	// $data->batas = $batas;
    	$data->status = $status;
    	$data->save();
    	Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian-Batas Anggaran dengan batasan '.$data->batas,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    	return $data;
    }

	public function delete(){
		$id = $this->uri->segment(3);
		$jenis_penelitian = $this->uri->segment(5);
		$item = Batas_anggaran::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian-Batas Anggaran dengan batasan '.$item->batas,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("batasanggarans/".$jenis_penelitian);
	}

 	public function is_exist(){
 		$data = Batas_anggaran::where('batas','=',$this->input->get('batas'))->where('jenis_penelitian','=',$this->input->get('jenis_penelitian'))->where('isdelete','=','0')->count();
 		if($data>0){
			$this->output->set_header('HTTP/1.0 470 Data sudah ada');
			return;
 		}
		$this->output->set_header('HTTP/1.0 200 OK');
		echo "Aprove";
 	}
}
