<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Pajaks extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Pajak');
    }

	public function index(){
		$data = Pajak::where('isdelete', '=', '0');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$items = array("items"=>$data->toArray());
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/penelitian/pajak/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
    	redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit(){
    	$id = $this->uri->segment(5);
		$nominal = $this->input->post("nominal");
		$status = $this->input->post('status');
		if($status=='1'){
			$updates = Pajak::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Pajak::find($id);
		$data->nominal = $nominal;
		$data->status = $status;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Pajak dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
    }

    public function insert(){
		$nominal = $this->input->post("nominal");
		if(empty($nominal)) return False;
		$data = new Pajak;
		$data->nominal = $nominal;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Pajak dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Pajak::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Pajak dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}
}
