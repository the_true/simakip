<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Batchs extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch");
        $this->load->model("Batch_lists");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Penilaian");
        $this->load->model("Penilaian_kriteria");
        $this->load->model("Tahun_kegiatan");
    }

	public function index(){
		$id = $this->uri->segment(2);
		$data = Batch_penelitian::find($id);
		$data->load('jenis_penelitian')->load('tahun');
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		// $items = array("items"=>$data->toArray());
		$item = Batch::where("isdelete","=","0")->where("batch_penelitian","=",$id);
		$info = $this->create_paging($item);
		$item = $item->take($info["limit"])->skip($info["skip"])->get();
		$item->load('batch_lists');
		$id = $data->toArray();
		$batch_lists  = Batch_lists::penelitian()->where("isdelete","=","0")->get();
		$items = ["jenpen"=>$id,"items"=>$item->toArray(),"batch_lists"=>$batch_lists->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display("pengaturan/penelitian/batch_penelitian/batch/index",array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/batchs/".$data->batch_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
			redirect("/batchs/".$data->batch_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function json(){
    	$id = $this->uri->segment(3);
    	$data = Batch::where("isdelete","=","0")->where("batch_penelitian","=",$id)->get();
    	echo $data->toJson();
    }

    public function insert(){
		$batch_penelitian = $this->uri->segment(3);
		$tanggal = $this->input->post("tanggal");
		$monev = $this->input->post("tanggalmonev");
		$monevfinish = $this->input->post("tanggalmonevfinish");
		$nama = $this->input->post("nama");
		$review = $this->input->post("review");
		if(empty($batch_penelitian) || empty($tanggal)) return False;
		
		$split = explode(' - ', $tanggal);
		$start = str_replace('/', '-', $split[0]);
		$finish = str_replace('/', '-', $split[1]);

		// $splitx = explode(' - ', $monev);
		// $monev_start = str_replace('/', '-', $splitx[0]);
		// $monev_finish = str_replace('/', '-', $splitx[1]);


		$review = str_replace('/', '-', $review);
		$monev_start = str_replace('/', '-', $monev);
		$monev_finish = str_replace('/', '-', $monevfinish);
		// echo date("d-m-Y", strtotime($start)).' '.date("d-m-Y", strtotime($finish));
		// die;
		$data = new Batch;
		$data->nama = Batch_lists::penelitian()->where("id","=",$nama)->first()->nama;
		$data->batch_lists = $nama;
		$data->batch_penelitian = $batch_penelitian;
		$data->start = date("Y-m-d", strtotime($start));
		$data->finish = date("Y-m-d", strtotime($finish));
		$data->monev_start = date("Y-m-d", strtotime($monev_start));
		$data->monev_finish = date("Y-m-d", strtotime($monev_finish));
		$data->review = date("Y-m-d", strtotime($review));
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Batch Penelitian-Detail baru dengan nama batch '.$nama.' dengan periode '.$tanggal,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

		return $data;
    }

    public function update(){
    	$id = $this->uri->segment(3);
    	$tanggal = $this->input->post("tanggal");
		$monev = $this->input->post("tanggalmonev");
		$monevfinish = $this->input->post("tanggalmonevfinish");
    	$nama = $this->input->post("nama");
    	$batch_penelitian = $this->input->post("batch_penelitian");
    	$review = $this->input->post("review");
    	if(empty($batch_penelitian) || empty($tanggal)) return False;

		$split = explode(' - ', $tanggal);
		$start = str_replace('/', '-', $split[0]);
		$finish = str_replace('/', '-', $split[1]);

		// $splitx = explode(' - ', $monev);
		// $monev_start = str_replace('/', '-', $splitx[0]);
		// $monev_finish = str_replace('/', '-', $splitx[1]);

		$review = str_replace('/', '-', $review);
		$monev_start = str_replace('/', '-', $monev);
		$monev_finish = str_replace('/', '-', $monevfinish);
		$data = Batch::find($id);
		$data->nama = Batch_lists::penelitian()->where("id","=",$nama)->first()->nama;
		$data->batch_lists = $nama;
		$data->start = date("Y-m-d", strtotime($start));
		$data->finish = date("Y-m-d", strtotime($finish));
		$data->review = date("Y-m-d", strtotime($review));
		$data->monev_start = date("Y-m-d", strtotime($monev_start));
		$data->monev_finish = date("Y-m-d", strtotime($monev_finish));
		
    	$data->save();

		$finish = strtotime($finish." 23:59:59");
		$today = time();
		if($today<$finish){
			celery()->PostTask('tasks.check_perpanjang_penelitian',array(base_url(),$data->id));
		}

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian-Batch Penelitian-Detail baru dengan nama batch '.$nama.' dengan periode '.$tanggal,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

    	return $data;
    }

	public function delete(){
		$id = $this->uri->segment(3);
		$batch_penelitian = $this->uri->segment(5);
		$item = Batch::find($id);
		$item->isdelete = 1;
		$item->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian-Batch Penelitian-Detail baru dengan nama batch '.$item->nama.' dengan periode '.$item->start.' - '.$item->finish,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

		redirect("batchs/".$batch_penelitian);
	}
}
