<?php
defined("BASEPATH") or exit("No direct script access allowed");
include_once "application/core/MY_BaseController.php";

class Targetluarans extends MY_BaseController
{

    /**
     * @category Libraries
     * @package  CodeIgniter 3.0
     * @author   Yp <purwantoyudi42@gmail.com>
     * @link     https://timexstudio.com
     * @license  Protected
     */
    private $menu = array("p" => "active", "p1" => "active");
    public function __construct()
    {
        $config = [
            "functions" => ["anchor", "set_value", "set_radio"],
            "functions_safe" => ["validation_errors_array", "form_open"],
        ];
        parent::__construct($config);
        $this->load->model("Jenis_penelitian");
        $this->load->model("Target_luaran");
        $this->load->model("Target_luaran_master");
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch");
        $this->load->model("Penilaian");
        $this->load->model("Penilaian_kriteria");
    }

    public function index()
    {
        $id = $this->uri->segment(2);
        $data = Jenis_penelitian::find($id);
        $item = Target_luaran::where("isdelete", "=", "0")->where("jenis_penelitian", "=", $id);
        $master = Target_luaran_master::where("isdelete", "=", "0")->get();
        $info = $this->create_paging($item);
        $item = $item->take($info["limit"])->skip($info["skip"])->get();
        $item->load("target_luaran_master");
        $id = $data->toArray();
        $items = ["jenpen" => $id, "items" => $item->toArray(), "master" => $master->toArray()];
        $items = array_merge($items, $info);
        $this->twig->display("pengaturan/penelitian/target_luaran/index", array_merge($items, $this->menu));
    }

    public function add()
    {
        $user = $this->is_login();
        $data = $this->insert();
        if ($data) {
            redirect("/targetluarans/" . $data->jenis_penelitian);
        }

        redirect('login/pagenotfound', 'refresh');
    }

    public function edit()
    {
        $user = $this->is_login();
        $data = $this->update();
        if ($data) {
            redirect("/targetluarans/" . $data->jenis_penelitian);
        }

        redirect('login/pagenotfound', 'refresh');
    }

    public function insert()
    {
        $jenis_penelitian = $this->uri->segment(3);
        $nama = $this->input->post("nama");
        $target_luaran_master = $this->input->post("target_luaran_master");
        if (empty($jenis_penelitian) || empty($nama)) {
            return false;
        }

        $data = new Target_luaran;
        $data->jenis_penelitian = $jenis_penelitian;
        $data->nama = $nama;
        if (!empty($target_luaran_master)){
            $target_luaran_master = Target_luaran_master::find($target_luaran_master);
            $data->nama = $target_luaran_master->nama;
            $data->target_luaran_master = $target_luaran_master->id;
        }
        $data->save();

        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'insert', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' membuat konfigurasi Penelitian-Jenis Penelitian-Target Luaran baru dengan nama batch ' . $nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);

        return $data;
    }

    public function update()
    {
        $id = $this->uri->segment(3);
        $nama = $this->input->post("nama");
        $target_luaran_master = $this->input->post("target_luaran_master");
        $jenis_penelitian = $this->input->post("jenis_penelitian");
        if (empty($jenis_penelitian) || empty($nama)) {
            return false;
        }

        $data = Target_luaran::find($id);
        $data->nama = $nama;

        if (!empty($target_luaran_master)){
            $target_luaran_master = Target_luaran_master::find($target_luaran_master);
            $data->nama = $target_luaran_master->nama;
            $data->target_luaran_master = $target_luaran_master->id;
        }
        $data->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' merubah konfigurasi Penelitian-Jenis Penelitian-Target Luaran dengan nama batch ' . $nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        return $data;
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $jenis_penelitian = $this->uri->segment(5);
        $item = Target_luaran::find($id);
        $item->isdelete = 1;
        $item->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'delete', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' menghapus konfigurasi Penelitian-Jenis Penelitian-Target Luaran dengan nama batch ' . $item->nama, 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        redirect("targetluarans/" . $jenis_penelitian);
    }
}
