<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Keanggotaans extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Jenis_penelitian");
        $this->load->model("Keanggotaan");
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch");
        $this->load->model("Penilaian");
        $this->load->model("Penilaian_kriteria");
    }

	public function index(){
        $this->twig->addGlobal('session', $this->session);
		$id = $this->uri->segment(2);
		$data = Jenis_penelitian::find($id);
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		// $items = array("items"=>$data->toArray());
		$item = Keanggotaan::firstOrNew(['jenis_penelitian'=>$data->id]);
        $item->jenis_penelitian = $data->id;
        $item->save();
		$jenis_penelitian = $data->toArray();
		$items = ["jenpen"=>$jenis_penelitian];
        $items["items"] = $item->toArray();
		$this->twig->display("pengaturan/penelitian/keanggotaan/index",array_merge($items,$this->menu));
	}


    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
            $this->session->set_flashdata('success', 'Data Berhasil disimpan!');
			redirect("/keanggotaans/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function update(){
    	$id = $this->uri->segment(3);
    	$notes = $this->input->post("notes");
    	$anggota = $this->input->post("anggota");
    	$anggotamhs = $this->input->post("anggotamhs");
    	$jenis_penelitian = $this->input->post("jenis_penelitian");
    	if(empty($jenis_penelitian) || empty($notes) || is_null($anggota)) return False;
    	$data = Keanggotaan::find($id);
    	if(empty($data)){
			$data = new Keanggotaan;
			$data->jenis_penelitian = $jenis_penelitian;
    	}
    	$data->anggota = $anggota;
    	$data->anggotamhs = $anggotamhs;
    	$data->notes = $notes;
    	$data->save();
        Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Data Keanggotaan','type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    	return $data;
    }

}
