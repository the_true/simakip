<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Petapotensidetail extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Petapotensi');
        $this->load->model('Tahun_kegiatan');
        $this->load->model('Jabatan_akademik');
        $this->load->model('Petapotensi_syarat');

    }

	public function index(){
		$id = $this->uri->segment(4);
		$items = Petapotensi::find($id);
		$items = $items->load('tahun');
		$items = $items->toArray();

		$data = Petapotensi_syarat::where('peta_potensi','=',$id);
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$data->load("jabatan_akademik");
		$items["items"] = $data->toArray();
		$jabak = Jabatan_akademik::where('isdelete','=', '0')->get();
		$items["jabatan_akademik"]= $jabak->toArray();
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/petapotensi/detail/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
    	// if ($data)
    	// 	redirect("/batasanggarans/".$data->id);
    	redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit(){
    	$id = $this->uri->segment(5);
		$syarat = $this->input->post("syarat");
		$jabatan_akademik = $this->input->post("jabatan_akademik");
		$luaran = $this->input->post("target");
		$jumlah = $this->input->post("jumlah");
		$data = Petapotensi_syarat::find($id);
		$data->syarat = $syarat;
		$data->jabatan_akademik = $jabatan_akademik;
		$data->luaran = json_encode($luaran);
		$data->jumlah = json_encode($jumlah);
		$data->save();

		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Peta Potensi Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
    }

    public function insert(){
    	$petapotensi = $this->input->post("petapotensi");
		$syarat = $this->input->post("syarat");
		$jabatan_akademik = $this->input->post("jabatan_akademik");
		$luaran = $this->input->post("target");
		$jumlah = $this->input->post("jumlah");
		$data = new Petapotensi_syarat;
		$data->peta_potensi = $petapotensi;
		$data->syarat = $syarat;
		$data->jabatan_akademik = $jabatan_akademik;
		$data->luaran = json_encode($luaran);
		$data->jumlah = json_encode($jumlah);
		$data->save();

		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Peta Potensi Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Petapotensi_syarat::find($id);
		// $item->isdelete = 1;
		$item->delete();
		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Peta Potensi Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}
}
