<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Petapotensitahun extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p8"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Petapotensi');
        $this->load->model('Tahun_kegiatan');

    }

	public function index(){
		$data = Petapotensi::where('isdelete', '=', '0');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$data->load("tahun");
		$jabak = Tahun_kegiatan::where('isdelete','=', '0')->orderBy('tahun','DESC')->get();
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		$items = array("items"=>$data->toArray(),"tahuns"=>$jabak->toArray());
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/petapotensi/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
    	// if ($data)
    	// 	redirect("/batasanggarans/".$data->id);
    	redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit(){
    	$id = $this->uri->segment(4);
		$tahun = $this->input->post("tahun");		
		$jabatan_akademik = $this->input->post("jabatan_akademik");
		$data = Petapotensi::find($id);
		$data->tahun = $tahun;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Peta Potensi Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
    }

    public function insert(){
		$tahun = $this->input->post("tahun");
		$data = new Petapotensi;
		$data->tahun = $tahun;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Peta Potensi Penelitian dengan tahun '.$data->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Petapotensi::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Peta Potensi Penelitian dengan tahun '.$item->tahun()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function validator(){
		$tahun = $this->input->get('tahun');
		$count = Petapotensi::where('isdelete', '=', '0')->where('tahun','=',$tahun)->count();
		if($count>1){
			$this->output->set_header('HTTP/1.0 500 Peta Potensi tahun tersebut sudah ada');
 			return;
		}

	}
}
