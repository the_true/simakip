<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Isianpengesahan extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p9"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Pengesah_pengabdian');
        $this->load->model('Dosen');
        $this->load->model('Jabatan_akademik');
    }

	public function index(){
		$data = Pengesah_pengabdian::where('isdelete', '=', '0')->orderBy('status','DESC')->orderBy('id','DESC');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$data->load("dosen")->load("dosen.jabatan_akademik");
		$items = ["items"=>$data->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/pengabdian/isianpengesahan/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/pengabdian/isian_pengesahan/add',$this->menu);
        }else{ 
        	if ($this->insert())
        		redirect("pengaturan/pengabdian/isianpengesahan/");
        	else echo $this->upload->display_errors();
		}
    }

    public function edit(){
    	$id = $this->uri->segment(5);
		$status = $this->input->post('status');
		$jabatan = $this->input->post('jabatan');
		if($status=='1'){
			$updates = Pengesah_pengabdian::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Pengesah_pengabdian::find($id);
		$data->status = $status;
		$data->jabatan = $jabatan;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Isian Pengesahan dengan nama dosen '.$data->dosen()->first()->nama.' menjadi berstatus '.$jabatan,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("pengaturan/pengabdian/isianpengesahan/");
    }

    public function validation($edit=False){
    	$this->form_validation->set_rules('nidn', 'NIDN', 'required');
    	$this->form_validation->set_rules('dosen', 'Dosen', 'required');
    	$this->form_validation->set_rules('status', 'Status', 'required');
    }

    public function insert(){
		$dosen = $this->input->post("dosen");
		$status = $this->input->post('status');
		$jabatan = $this->input->post('jabatan');
		if($status=='1'){
			$updates = Pengesah_pengabdian::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = new Pengesah_pengabdian;
		$data->dosen = $dosen;
		$data->status = $status;
		$data->jabatan = $jabatan;
		$data->type = "pengabdian";
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Isian Pengesahan baru dengan nama dosen '.$data->dosen()->first()->nama.' menjadi berstatus '.$jabatan,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("isianpengesahans/");
    }

 	public function is_exist(){
 		$data = Pengesah_pengabdian::whereHas("dosen",function($q){
 			$q->where('nidn','=',$this->input->get("nidn"));
 		})->where("isdelete","=","0")->count();
 		if($data>0){
			$this->output->set_header('HTTP/1.0 470 Data sudah ada');
			return;
 		}
		$this->output->set_header('HTTP/1.0 200 OK');
		echo "Aprove";
 	}

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Pengesah_pengabdian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Isian Pengesahan baru dengan nama dosen '.$item->dosen()->first()->nama.' dengan berstatus '.$item->jabatan,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('pengaturan/pengabdian/isianpengesahan/');
	}
}
