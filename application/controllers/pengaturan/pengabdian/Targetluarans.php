<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Targetluarans extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p9"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Target_luaran");
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch");
        $this->load->model("Penilaian");
        $this->load->model("Penilaian_kriteria");
    }

	public function index(){
		$id = $this->uri->segment(4);
		$data = Jenis_pengabdian::find($id);
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		// $items = array("items"=>$data->toArray());
		$item = Target_luaran::where("isdelete","=","0")->where("jenis_penelitian","=",$id);
		$info = $this->create_paging($item);
		$item = $item->take($info["limit"])->skip($info["skip"])->get();
		$id = $data->toArray();
		$items = ["jenpen"=>$id,"items"=>$item->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display("pengaturan/pengabdian/target_luaran/index",array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/pengaturan/pengabdian/targetluarans/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
			redirect("/pengaturan/pengabdian/targetluarans/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function insert(){
		$jenis_penelitian = $this->uri->segment(5);
		$nama = $this->input->post("nama");
		$flag = $this->input->post("flag");
		if(empty($jenis_penelitian) || empty($nama)) return False;
		$data = new Target_luaran;
		$data->jenis_penelitian = $jenis_penelitian;
		$data->nama = $nama;
		$data->flag = $flag;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Target Luaran baru dengan nama batch '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);

		return $data;
    }

    public function update(){
    	$id = $this->uri->segment(5);
			$nama = $this->input->post("nama");
			$flag = $this->input->post("flag");
    	$jenis_penelitian = $this->input->post("jenis_penelitian");
    	if(empty($jenis_penelitian) || empty($nama)) return False;
    	$data = Target_luaran::find($id);
			$data->nama = $nama;
			$data->flag = $flag;
    	$data->save();
    	Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian-Target Luaran dengan nama batch '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    	return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$jenis_penelitian = $this->uri->segment(7);
		$item = Target_luaran::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian-Target Luaran dengan nama batch '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("/pengaturan/pengabdian/targetluarans/".$jenis_penelitian);
	}
}
