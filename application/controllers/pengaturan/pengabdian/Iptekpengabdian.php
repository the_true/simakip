<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class IptekPengabdian extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p9"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('iptek');
    }

	public function index(){
		$data = iptek::where('isdelete', '=', '0');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		$items = array("items"=>$data->toArray());
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/pengabdian/iptek/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
    	redirect("/iptek/");
    }

    public function edit(){
    	$jenis_penelitian = $this->uri->segment(3);
		$nama = $this->input->post("nama");
		$data = iptek::find($jenis_penelitian);
		$data->nama = $nama;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Pengabdian-Iptek dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("iptek/");
    }

    public function insert(){
		$nama = $this->input->post("nama");
		if(empty($nama)) return False;
		$data = new iptek;
		$data->nama = $nama;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Pengabdian-Iptek dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

	public function delete(){
		$id = $this->uri->segment(3);
		$item = iptek::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Pengabdian-Iptek dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('iptek/');
	}
}
