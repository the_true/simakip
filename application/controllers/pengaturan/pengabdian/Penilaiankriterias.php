<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Penilaiankriterias extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p9"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Penilaian");
        $this->load->model("Penilaian_kriteria");
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Batch");
        $this->load->model("Batch_penelitian");
    }

	public function index(){
		$id = $this->uri->segment(4);
		$data = Penilaian::find($id);
		$data->load('jenis_pengabdian');
		// $data->load("dosen")->load("dosen.jabatan_akademik");
		// $items = array("items"=>$data->toArray());
		$items = Penilaian_kriteria::where("isdelete","=","0")->where("penilaian","=",$id);
		$info = $this->create_paging($items);
		$items = $items->take($info["limit"])->skip($info["skip"])->get();
		$id = $data->toArray();
		$items = ["jenpen"=>$id,"items"=>$items->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display("pengaturan/pengabdian/penilaian/penilaian_kriteria/index",array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/pengaturan/pengabdian/penilaiankriterias/".$data->penilaian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
			redirect("/pengaturan/pengabdian/penilaiankriterias/".$data->penilaian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function insert(){
		$penilaian = $this->uri->segment(5);
		$nama = $this->input->post("nama");
		$indikator = $this->input->post("indikator");
		$bobot = $this->input->post("bobot");
		if(empty($penilaian) || empty($nama) || empty($indikator) || is_null($bobot)) return False;
		$data = new Penilaian_kriteria;
		$data->penilaian = $penilaian;
		$data->nama = $nama;
		$data->indikator = $indikator;
		$data->bobot = $bobot;
		$data->save();
		$nama = implode(' ', array_slice(explode(' ', $nama), 0, 2));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Penilaian-Kriteria baru dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

    public function update(){
    	$id = $this->uri->segment(5);
		$nama = $this->input->post("nama");
		$indikator = $this->input->post("indikator");
		$bobot = $this->input->post("bobot");
    	$penilaian = $this->input->post("penilaian");
    	if(empty($penilaian) || empty($nama) || empty($indikator) || is_null($bobot)) return False;
    	$data = Penilaian_kriteria::find($id);
		$data->nama = $nama;
		$data->indikator = $indikator;
		$data->bobot = $bobot;
    	$data->save();
		$nama = implode(' ', array_slice(explode(' ', $nama), 0, 2));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian-Penilaian-Kriteria dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    	return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$penilaian = $this->uri->segment(7);
		$item = Penilaian_kriteria::find($id);
		$item->isdelete = 1;
		$item->save();
		$nama = implode(' ', array_slice(explode(' ', $item->nama), 0, 2));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian-Penilaian-Kriteria dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("/pengaturan/pengabdian/penilaiankriterias/".$penilaian);
	}

	public function is_valid(){
		$penilaian = $this->input->get('penilaian');
		$bobot = $this->input->get('bobot');
		$id = $this->input->get('id');
		$item = Penilaian_kriteria::groupBy('penilaian')->selectRaw('sum(bobot) as total_bobot')->where('penilaian','=',$penilaian)->where('isdelete','=','0');
		if($id){
			$item = $item->where('id','<>',$id);
		}
		$item = $item->first();
		if(((int)$item->total_bobot+(int)$bobot)<=100){
			echo "accept";
			return;
		}
		$this->output->set_header('HTTP/1.0 470 Data melebihi 100');
		return;
	}
}
