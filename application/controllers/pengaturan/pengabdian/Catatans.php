<?php
defined("BASEPATH") or exit("No direct script access allowed");
include_once "application/core/MY_BaseController.php";

class Catatans extends MY_BaseController
{

    /**
     * @category Libraries
     * @package  CodeIgniter 3.0
     * @author   Yp <purwantoyudi42@gmail.com>
     * @link     https://timexstudio.com
     * @license  Protected
     */
    private $menu = array("p" => "active", "p9" => "active");
    public function __construct()
    {
        $config = [
            "functions" => ["anchor", "set_value", "set_radio"],
            "functions_safe" => ["validation_errors_array", "form_open"],
        ];
        parent::__construct($config);
        $this->load->model("Notes");
    }

    public function index()
    {
        // $this->twig->addGlobal('session', $this->session);
        $item = Notes::firstOrNew(['key' => "pengabdian_catatan_penilaian"]);
        $item->key = "pengabdian_catatan_penilaian";
        $item->save();
        $items["item"] = $item->toArray();
        $this->twig->display("pengaturan/pengabdian/catatan_penilaian/index", array_merge($items, $this->menu));
    }

    public function edit()
    {
        $user = $this->is_login();
        $data = $this->update();
        if ($data) {
            $this->session->set_flashdata('success', 'Data Berhasil disimpan!');
            redirect("/pengaturan/pengabdian/catatans/");
        }
        redirect('login/pagenotfound', 'refresh');
    }

    public function update()
    {
        $notes = $this->input->post("notes");
        if (empty($notes)) {
            return false;
        }

        $data = Notes::where('key', '=', 'pengabdian_catatan_penilaian')->first();
        if (empty($data)) {
            $data = new Notes;
            $data->key = "pengabdian_catatan_penilaian";
        }
        $data->string = $notes;
        $data->save();
        Log_Activity::insert(["dosen" => $this->is_login()["id"], 'action' => 'edit', 'content' => 'User ' . $this->is_login()['nama'] . ' sebagai ' . $this->is_login()['akses']['nama'] . ' membuat konfigurasi Pengabdian-Catatan Penilaian', 'type' => '6', 'created_at' => date('Y-m-d H:i:s')]);
        return $data;
    }

}
