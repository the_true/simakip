<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Bobothasils extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p4"=>"active");
    public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
		    "functions_safe" => ["validation_errors_array","form_open"],
		];
        parent::__construct($config);
        $this->load->model("Jenis_pengabdian");
        $this->load->model("Bobot_hasil");
        $this->load->model("Bobot_kriteria");
    }

	public function index(){
		$id = $this->uri->segment(4);
		$data = Jenis_pengabdian::find($id);
		$item = Bobot_hasil::where("isdelete","=","0")->where("jenis_penelitian","=",$id);
		$info = $this->create_paging($item);
		$item = $item->take($info["limit"])->skip($info["skip"])->get();
		$id = $data->toArray();
		$items = ["jenpen"=>$id,"items"=>$item->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display("pengaturan/pengabdian/bobot_hasil/index",array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/pengaturan/pengabdian/bobothasils/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
			redirect("/pengaturan/pengabdian/bobothasils/".$data->jenis_penelitian);
		redirect('login/pagenotfound', 'refresh');
    }

    public function insert(){
		$jenis_pengabdian = $this->uri->segment(5);
		$nama = $this->input->post("nama");
		$status = $this->input->post("status");
		if(empty($jenis_pengabdian) || empty($nama) || is_null($status)) return False;
		$data = new Bobot_hasil;
		$data->jenis_penelitian = $jenis_pengabdian;
		$data->nama = $nama;
		$data->status = $status;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Penelitian-Jenis Penelitian-Penilaian baru dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

    public function update(){
    	$id = $this->uri->segment(5);
		$nama = $this->input->post("nama");
		$status = $this->input->post("status");
    	$jenis_pengabdian = $this->input->post("jenis_pengabdian");
    	if(empty($jenis_pengabdian) || empty($nama) || is_null($status)) return False;
		if($status=="1"){
			$updates = Bobot_hasil::where("isdelete","=","0")->where("jenis_penelitian","=",$jenis_pengabdian)->get();
			foreach($updates as $d){
				$d->status="0";
				$d->save();
			}
		}
    	$data = Bobot_hasil::find($id);
		$data->nama = $nama;
		$data->status = $status;
    	$data->save();
    	Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Penelitian-Jenis Penelitian-Penilaian dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    	return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$jenis_pengabdian = $this->uri->segment(7);
		$item = Bobot_hasil::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Penelitian-Jenis Penelitian-Penilaian dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("/pengaturan/pengabdian/bobothasils/".$jenis_pengabdian);
	}
}
