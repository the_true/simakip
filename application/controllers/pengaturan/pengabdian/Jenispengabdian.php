<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class JenisPengabdian extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p9"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
				$this->load->model('jenis_pengabdian');
				$this->load->model('batas_anggaran');
    }

	public function index(){
		$data = jenis_pengabdian::where('isdelete', '=', '0');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$data->load("batas_anggaran_aktif");
		$items = array("items"=>$data->toArray());
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/pengabdian/jenis_pengabdian/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$data = $this->insert();
		if ($data)
			redirect("/pengaturan/pengabdian/batasanggarans/".$data->id);
		redirect("/pengaturan/pengabdian/jenispengabdian/");
    }

    public function edit(){
		$jenis_penelitian = $this->uri->segment(5);
		$nama = $this->input->post("nama");
		$data = jenis_pengabdian::find($jenis_penelitian);
		$data->nama = $nama;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Pengabdian-Jenis Pengabdian dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("/pengaturan/pengabdian/jenispengabdian/");
    }

    public function insert(){
		$nama = $this->input->post("nama");
		$status = ($this->input->post("status")) ? $this->input->post('status') : 0;
		if(empty($nama)) return False;
		$data = new jenis_pengabdian;
		$data->nama = $nama;
		$data->status = $status;
		$data->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Pengabdian-Jenis Pengabdian dengan nama '.$nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
    }

	public function delete(){
		$id = $this->uri->segment(5);
		$item = jenis_pengabdian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Pengabdian-Jenis Pengabdian dengan nama '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('/pengaturan/pengabdian/jenispengabdian/');
	}
}
