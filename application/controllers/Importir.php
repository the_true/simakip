<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Importir extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/

    public function __construct() {
		$config = [
			'function' => ['anchor','set_value'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
    	
        $this->load->model('Dosen');
    }	
	public function index()
	{			
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('hidden', 'HIDDEN', 'required');
		if (empty($_FILES['file']['name']))
		{
		    $this->form_validation->set_rules('file', 'Berkas', 'required');
		}

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('importir/index');
        }else{ 
        	$result = $this->submit();
        	$this->readCSV($result['full_path']);
        	if ($result){}
        		// $this->twig->display('importir/index');
        	else echo $this->upload->display_errors();
		}
	}

	private function readCSV($file){
		$this->load->helper('security');
        $this->load->library('csvreader');
        $result =   $this->csvreader->parse_file($file);
        foreach($result as $account){
        	$this->create_account($account);
        }
	}

	private function create_account($data){
		$salt = $this->config->item('salt');
		$uuid = uniqid();
		$password = $data["password"];
		$combined = $salt.$password.$uuid;
		$hash = do_hash($combined);

		$dosen = new Dosen;

		$dosen->nidn = $data["nidn"];
		$dosen->nama = $data["nama"];
		$dosen->akses = 1;
		$dosen->status_dosen = 1;
		$dosen->salt = $uuid;
		$dosen->password = $hash;
		$dosen->save();
	}


	private function submit(){
		$this->generate_folder('uploads/csv');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/csv'),
			'allowed_types' => '*',
			'max_size' => 2000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if ($uploaded){	
			return $this->upload->data();
		}else{
			// echo "<pre>".print_r($this->upload->data())."</pre>";
			return FALSE;
		}
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function rollbackstatus(){
		$this->load->model('Penelitian');
		$this->load->model('Jenis_penelitian');
		$this->load->model('Tahun_kegiatan');
		$this->load->model('Batch');
		$this->load->model('Batch_penelitian');
		$fixer = Penelitian::whereHas('batch',function($q){
			$q->whereHas('batch_penelitian',function($w){
				$w->whereHas('tahun',function($x){
					$x->where('tahun','=',"2017");
				});
			})->where('nama','=','Bacth 2');
		})->where('isvalid','=','1')->get();
		$fixer->load('jenis_penelitian.batch_penelitian.tahun');
		// echo json_encode($fixer->toArray());
		foreach($fixer as $f){
			// echo $f->batch()->first()->batch_penelitian()->first()->tahun()->first()->tahun." ".$f->batch()->first()->nama."<br>";
			echo $f->status_text."<br>";
			// $f->status=3;
			// $f->save();
		}

		// echo $fixer;
	}
}
