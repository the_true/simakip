<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;
class Luaran extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r1"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("fakultas");
        $this->load->helper('report');
        

    }

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$items=[];
		$fakultas = Fakultas::where('isdelete','=','0')->get();
		$result = $this->getFakultasData($items);
		$items["fakultas"] = $fakultas->toArray();
		$items["items"] = $result;
		$this->twig->display('report/luaran',array_merge($items,$this->menu));
	}

	public function getFakultasData(&$items){
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');
		$urutkan = $this->input->get('orderby');

		$where = "";
		$group = "";
		$nama = "";
		$order = "DESC";

		$fakultasx = "";
		if(!empty($fakultas)){
			$fakultasx=" AND dosen.fakultas=".$fakultas;
		}

		if(isset($urutkan)){
			$order = $urutkan;
		}

		$items["orderby"] = $order;

		if(isset($dari) && !empty($dari)){
			$dari = explode('-',$dari);
			$dari = $dari[1].'-'.$dari[0].'-01 00:00:00';
			$dari = " AND created_at>='$dari'";
		}else{
			return;
		}
		if(isset($dari) && !empty($dari)){
			$sampai = explode('-',$sampai);
			$sampai = $sampai[1].'-'.$sampai[0].'-31 23:59:59';
			$sampai = " AND created_at<='$sampai'";
		}else{
			return;
		}

		if($program_studi=="all"){
			$group = ',dosen.program_studi';
			$nama = ' program_studi.nama, ';
		}else if($program_studi!="all" && isset($program_studi)){
			$where = "AND dosen.program_studi=$program_studi";
			$group = ',dosen.program_studi, dosen.id';
			$nama = ' dosen.nama, ';
		}

		$query = "SELECT dosen.fakultas, fakultas.nama,dosen.program_studi, $nama
				count(DISTINCT jurnal.id) as jurnalnya, 
			    count(DISTINCT buku_ajar.id) as buku_ajarnya,
			    count(DISTINCT forum_ilmiah.id) as forum_ilmiahnya,
			    count(DISTINCT hki.id) as hkinya,
			    count(DISTINCT luaran_lain.id) as luaran_lainnya,
			    count(DISTINCT penyelenggara_forum_ilmiah.id) as penyelenggara_forum_ilmiahnya,
			    count(DISTINCT penelitian_hibah.id) as penelitian_hibahnya,
			    count(DISTINCT jurnal.id)+count(DISTINCT buku_ajar.id)+count(DISTINCT forum_ilmiah.id)+count(DISTINCT hki.id)+count(DISTINCT luaran_lain.id)+count(DISTINCT penyelenggara_forum_ilmiah.id)+count(DISTINCT penelitian_hibah.id) as total
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
			LEFT JOIN jurnal ON jurnal.dosen=dosen.id AND jurnal.isdelete=0 AND jurnal.isvalidate=1 fc($dari,jurnal) fc($sampai,jurnal)
			LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1  fc($dari,buku_ajar) fc($sampai,buku_ajar)
			LEFT JOIN forum_ilmiah ON forum_ilmiah.dosen = dosen.id AND forum_ilmiah.isdelete=0 AND forum_ilmiah.isvalidate=1 fc($dari,forum_ilmiah) fc($sampai,forum_ilmiah)
			LEFT JOIN hki ON hki.dosen = dosen.id AND hki.isdelete=0 AND hki.isvalidate=1  fc($dari,hki) fc($sampai,hki)
			LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1 fc($dari,luaran_lain) fc($sampai,luaran_lain)
			LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen = dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1  fc($dari,penyelenggara_forum_ilmiah) fc($sampai,penyelenggara_forum_ilmiah)
			LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1  fc($dari,penelitian_hibah) fc($sampai,penelitian_hibah)
			WHERE dosen.fakultas<>0 $fakultasx $where
			GROUP BY dosen.fakultas $group
			ORDER BY total $order";

		$allowedFunctions = array("fc");
		// echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
		$query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
		     function ($m) {
		          return $m[1]($m[2],$m[3]);
		     }, $query);
		$data = DB::select($query);
		// echo json_encode($data); die;
		return $data;
	}

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+8, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();
		$type_get = $this->input->get("type");
		$items = [];
		$result = $this->getFakultasData($items);
		$title = "Semua Fakultas";

		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');
		if($program_studi=="all"){
			$fakultas = Fakultas::find($fakultas);
			$title = " Fakultas ".$fakultas->nama;
		}else if($program_studi!="all" && isset($program_studi)){
			$this->load->model('Program_studi');
			$program_studi = Program_studi::find($program_studi);
			$title= " Program Studi ".$program_studi->nama;
		}

		$dari = explode('-',$this->input->get('dari'));
		$dari = angkatobulan(((int)$dari[0])-1).' '.$dari[1];

		$sampai = explode('-',$this->input->get('sampai'));
		$sampai = angkatobulan(((int)$sampai[0])-1).' '.$sampai[1];

		$this->load->view('report/pdf/luaran',array("items"=>$result,"dari"=>$dari,"sampai"=>$sampai,"title"=>$title));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output($this->is_login()['nidn'].'_Jurnal_'.Date('dmY').'.pdf', 'I');
	}

	/*
use devel_simakip;
SELECT dosen.nidn, dosen.nama, 
	count(DISTINCT jurnal.id) as jurnalnya, 
    count(DISTINCT buku_ajar.id) as buku_ajarnya,
    count(DISTINCT forum_ilmiah.id) as forum_ilmiahnya,
    count(DISTINCT hki.id) as hkinya,
    count(DISTINCT luaran_lain.id) as luaran_lainnya,
    count(DISTINCT penyelenggara_forum_ilmiah.id) as penyelenggara_forum_ilmiahnya,
    count(DISTINCT penelitian_hibah.id) as penelitian_hibahnya
FROM dosen 
LEFT JOIN jurnal ON jurnal.dosen=dosen.id AND jurnal.isdelete=0 AND jurnal.isvalidate=1
LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1
LEFT JOIN forum_ilmiah ON forum_ilmiah.dosen = dosen.id AND forum_ilmiah.isdelete=0 AND forum_ilmiah.isvalidate=1
LEFT JOIN hki ON hki.dosen = dosen.id AND hki.isdelete=0 AND hki.isvalidate=1
LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1
LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen = dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1
LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1
GROUP BY dosen.nidn
ORDER BY jurnalnya DESC;
	*/
}
