<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
use Illuminate\Database\Capsule\Manager as DB;
class Dashboard extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Dosen");
        $this->load->helper("report");

    }

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$items = [];
		$items["point_fakultas"] = getPointFakultas();
		$items["point_prostud"] = getPointProstud();
		$items["point_dosen"] = getPointDosen();
		$items["jabatan_akademik"] = getJabatanAkademik();
		$items["jenjang_pendidikan"]= getPendidikanDosen();
		$items["jabatan_fungsional"] = getJabatanFungsi();
		$items["total_luaran"] = $this->getTotalLuaran();
		$items["total_penelitian"] = $this->getTotalPenelitian();
		$items["periode"] = getPeriode();
		// echo json_encode($items["period"]); die;
		$items["total_point"] = $this->getTotalPoint($items["periode"]["dari"],$items["periode"]["sampai"]);
		$this->twig->display('report/dashboard',array_merge($items,$this->menu));
	}

	public function getTotalLuaran(){
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');
		$urutkan = $this->input->get('orderby');

		$where = "";
		$group = "";
		$nama = "";
		$order = "DESC";

		$fakultasx = "";

		$items["orderby"] = $order;


		$query = "SELECT dosen.fakultas, max(fakultas.nama) as nama,max(dosen.program_studi), $nama
				count(DISTINCT jurnal.id) as jurnalnya, 
			    count(DISTINCT buku_ajar.id) as buku_ajarnya,
			    count(DISTINCT forum_ilmiah.id) as forum_ilmiahnya,
			    count(DISTINCT hki.id) as hkinya,
			    count(DISTINCT luaran_lain.id) as luaran_lainnya,
			    count(DISTINCT penyelenggara_forum_ilmiah.id) as penyelenggara_forum_ilmiahnya,
			    count(DISTINCT penelitian_hibah.id) as penelitian_hibahnya,
			    count(DISTINCT jurnal.id)+count(DISTINCT buku_ajar.id)+count(DISTINCT forum_ilmiah.id)+count(DISTINCT hki.id)+count(DISTINCT luaran_lain.id)+count(DISTINCT penyelenggara_forum_ilmiah.id)+count(DISTINCT penelitian_hibah.id) as total
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
			LEFT JOIN jurnal ON jurnal.dosen=dosen.id AND jurnal.isdelete=0 AND jurnal.isvalidate=1 
			LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1
			LEFT JOIN forum_ilmiah ON forum_ilmiah.dosen = dosen.id AND forum_ilmiah.isdelete=0 AND forum_ilmiah.isvalidate=1
			LEFT JOIN hki ON hki.dosen = dosen.id AND hki.isdelete=0 AND hki.isvalidate=1  
			LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1 
			LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen = dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1 
			LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1
			WHERE dosen.fakultas<>0 $fakultasx $where
			GROUP BY dosen.fakultas $group
			ORDER BY total $order";
		
		$data = DB::select($query);
		// echo json_encode($data); die;
		return $data;
	}

	private function getTotalPenelitian(){
		// echo $jurnal; die;
		// $dari = $d["dari"];
		// $sampai = $d["sampai"];
		$where = "";
		$group = "";
		$select = ", max(nama) as nama";
		$order = "DESC";
		$query = "
		SELECT max(id) as id, max(program_studi_id) as program_studi_id $select, 
		    SUM(total) as total
		FROM
		(

			SELECT 
					max(fakultas.id) as id, max(fakultas.nama) as nama, max(program_studi.nama) as program_studi, max(program_studi.id) as program_studi_id, max(dosen.nama) as nidn, 
					count(penelitian.id) as total,
					'penelitian' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN penelitian ON penelitian.dosen=dosen.id AND penelitian.isdelete=0 AND penelitian.isvalid=1
			WHERE dosen.fakultas<>0 $where
			GROUP BY dosen.fakultas $group
			having total>0
		) as dosen
		GROUP BY dosen.id $group
		ORDER BY total $order
			";
		$data = DB::select($query);
		// echo $query; die;
		// echo json_encode($data); die;
		return $data;
	}

	private function getTotalPoint($daris,$sampais){
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');
		$status= $this->input->get('status');
		$urutkan = $this->input->get('orderby');

		$where = "";
		$group = "";
		$select = ", nama";
		$order = "DESC";

		$query = "
		SELECT id, program_studi_id $select, 
		    SUM(total) as total
		FROM
		(

			SELECT 
					fakultas.id, fakultas.nama as nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as nidn, 
					sum(point_log.point) as total,
					'penelitian' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN point_log ON point_log.user_id=dosen.id
			WHERE dosen.fakultas<>0 $where AND point_log.created_date BETWEEN '$daris-01-01' AND '$sampais-12-31'
			GROUP BY dosen.fakultas $group
			having total>0
		) as dosen
		GROUP BY dosen.id $group
		ORDER BY total $order
			";

		$data = DB::select($query);
		// echo $query; die;
		// echo json_encode($data); die;
		return $data;
	}

}
