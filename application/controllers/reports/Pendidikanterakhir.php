<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Pendidikanterakhir extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r3"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config,false);
        $this->load->model("program_studi");
        $this->load->model("Dosen");
        $this->load->model("Fakultas");
    }

	public function index(){
		$items = [];
		$fakultas = Fakultas::where('isdelete','=','0')->get();

		$items = $this->datareport($items);

		$items["fakultas"] = $fakultas->toArray();
		$this->twig->display('report/pendidikanterakhir',array_merge($items,$this->menu));
	}

	private function datareport($item){
		$cari = $this->input->get("cari");
		if(!$cari) return $item;
		$dosens = Dosen::select(new raw("SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1) AS nama"),new 					raw("count(SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1)) as jumlah"))
		            ->groupBy(new raw("SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1)"))
		            ->whereRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1) <> ''")->where('isdelete','=','0');
		$dosens = $this->filter($dosens);
        $dosens = $dosens->get();
        $data = [];
        foreach($dosens->toArray() as $value){
        	$data[] = ["name"=>$value["nama"],"y"=>(int)$value["jumlah"]];
        }
        // echo json_encode($data); die;
 		$item["items"] = $data;
		return $item;
	}

	private function filter($item){
		$fakultas = $this->input->get('fakultas');
		if($fakultas) $item->where('fakultas','=',$this->input->get('fakultas'));

		$program_studi = $this->input->get('program_studi');
		if($program_studi) $item->where('program_studi','=',$this->input->get('program_studi'));


		return $item;
	}
}
