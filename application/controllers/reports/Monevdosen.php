<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;

class Monevdosen extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r1"=>"active");
    public function __construct() {
        $config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Batch");
        $this->load->model("Batch_lists");
        $this->load->model("Batch_penelitian");
        $this->load->model("Dosen");
        $this->load->model("Jenis_penelitian");
        $this->load->model("fakultas");
        $this->load->model("Monev");
        $this->load->model("Monev_reviewer");
        $this->load->model("Penelitian");
        $this->load->model("Penelitian_anggota");
        $this->load->model("Penelitian_reviewer");
        $this->load->model("Penelitian_review");
        $this->load->model("Penelitian_target_luaran_wajib");
        $this->load->model("Penelitian_target_luaran_tambahan");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Target_luaran");
        $this->load->model("Target_luaran_master");
        $this->load->helper('report');
    }

	public function index(){
        $jenis_penelitian = Jenis_penelitian::where('isdelete','=','0')->get();
        $batch_lists = Batch_lists::where('isdelete','=','0')->where('type','penelitian')->get();
        $tahun_kegiatan = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun','desc')->get();
        $items["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$items['jenis_penelitian'] = $jenis_penelitian->toArray();
		$items['batch_lists'] = $batch_lists->toArray();
        $items['data_reviewer'] = $this->search();

        $this->twig->display('report/monevdosen', $items);
	}

    public function search(){
        $input = $this->input->get();
        $jenis_penelitian = $this->input->get('jenis_penelitian');
        $tahun_kegiatan = $this->input->get('tahun_usulan');
        $batch_list = $this->input->get('batch');
        $jenis_kegiatan = $this->input->get('jenis_kegiatan');

        // $tahun_kegiatan = 18;
        // $batch_list = 5;

        if ($jenis_kegiatan == 'monev') {
            $monev_reviewer = $this->monev_reviewers($jenis_penelitian, $batch_list, $tahun_kegiatan);
            return $monev_reviewer;
        } elseif ($jenis_kegiatan == 'usulan') {
            $penelitian_reviewer = $this->penelitian_reviewer($jenis_penelitian, $batch_list, $tahun_kegiatan);
            return $penelitian_reviewer;
        }else {
            $penelitian_reviewer = $this->penelitian_reviewer($jenis_penelitian, $batch_list, $tahun_kegiatan);
            $monev_reviewer = $this->monev_reviewers($jenis_penelitian, $batch_list, $tahun_kegiatan);
            // echo '<pre>';print_r($penelitian_reviewer);die();
            $merge_reviewer = array_merge($penelitian_reviewer, $monev_reviewer);
            // echo '<pre>';print_r($merge_reviewer);die("aaaaaaaaaa");
            $new = [];
            foreach ($merge_reviewer as $key => $value) {
                $new[$value['dosen']]['dosen'] = $value['dosen'];
                $new[$value['dosen']]['jumlah_proposal'] = (isset($new[$value['dosen']]['jumlah_proposal']) ? $new[$value['dosen']]['jumlah_proposal'] : 0) + $value['jumlah_proposal'];
                $new[$value['dosen']]['sudah_review'] = (isset($new[$value['dosen']]['sudah_review']) ? $new[$value['dosen']]['sudah_review'] : 0) + $value['sudah_review'];
                $new[$value['dosen']]['belum_review'] = (isset($new[$value['dosen']]['belum_review']) ? $new[$value['dosen']]['belum_review'] : 0) + $value['belum_review'];
            }
            return $new;
        }
	}

    public function penelitian_reviewer($jenis_penelitian, $batch_list, $tahun_kegiatan){
        $batch_penelitian = Batch::where('batch_lists', $batch_list)->where('isdelete','=','0')->lists('id');
        $pr = Penelitian_reviewer::where('isdelete','=','0');
        if ($jenis_penelitian) {
            $pr =   $pr->whereHas('penelitian', function($qz) use ($jenis_penelitian){
                        $qz->whereIn('jenis_penelitian', $jenis_penelitian)->where('isdelete','=','0');
                    });
        }

        if ($batch_list) {
            $pr =   $pr->whereHas('penelitian', function($qz) use ($batch_penelitian){
                        $qz->whereIn('batch', $batch_penelitian)->where('isdelete','=','0');
                    });
        }
        if ($tahun_kegiatan) {
            $pr =   $pr->whereHas('penelitian', function($qz) use ($tahun_kegiatan){
                        $qz->where('tahun_kegiatan', $tahun_kegiatan)->where('isdelete','=','0');
                    });
        }

        $pr =   $pr->with('dosen')->has('dosen')
                ->select('dosen', DB::raw('count(*) as jumlah_proposal'))
                ->groupBy('dosen')
                ->get()
                ->toArray();

        $group = [];

        foreach ($pr as $key => $value_pr) {
            $pl =   Penelitian_reviewer::where('isdelete','=','0')
                    ->with('penelitian');

            if ($jenis_penelitian) {
                $pl =   $pr->whereHas('penelitian', function($qz) use ($jenis_penelitian){
                            $qz->whereIn('jenis_penelitian', $jenis_penelitian)->where('isdelete','=','0');
                        });
            }

            if ($batch_list) {
                $pl =   $pl->whereHas('penelitian', function($qz) use ($batch_penelitian){
                            $qz->whereIn('batch', $batch_penelitian)->where('isdelete','=','0');
                        });
            }
            if ($tahun_kegiatan) {
                $pl =   $pl->whereHas('penelitian', function($qz) use ($tahun_kegiatan){
                            $qz->where('tahun_kegiatan', $tahun_kegiatan)->where('isdelete','=','0');
                        });
            }

            $pl =   $pl->where('dosen', $value_pr['dosen']['id'])
                    ->where('isdelete','=','0')
                    ->get()
                    ->toArray();

            $grouppl = [];
            $count_penelitian_reviewer_sudah= 0;
            $count_penelitian_reviewer_belum= 0;
            foreach ($pl as $key_pl => $value_pl) {
                if ($value_pl['status'] == 1) {
                    $count_penelitian_reviewer_sudah++;
                }else {
                    $count_penelitian_reviewer_belum++;
                }

                $grouppl['penelitian'][$key_pl]['id'] = $value_pl['id'];
                $grouppl['penelitian'][$key_pl]['judul'] = $value_pl['penelitian']['judul'];
            }

            $group[$key]['dosen'] = $value_pr['dosen']['nama'];
            $group[$key]['jumlah_proposal'] = $value_pr['jumlah_proposal'];
            $group[$key]['penelitian'] = $grouppl['penelitian'];
            $group[$key]['sudah_review'] = $count_penelitian_reviewer_sudah;
            $group[$key]['belum_review'] = $count_penelitian_reviewer_belum;
        }

        return $group;
    }

    public function monev_reviewers($jenis_penelitian, $batch_list, $tahun_kegiatan){
        $batch_penelitian = Batch::where('batch_lists', $batch_list)->where('isdelete','=','0')->lists('id');
        $pr = Monev_reviewer::where('isdelete','=','0');
        if ($jenis_penelitian) {
            $pr =   $pr->whereHas('penelitian', function($qz) use ($jenis_penelitian){
                        $qz->whereIn('jenis_penelitian', $jenis_penelitian)->where('isdelete','=','0');
                    });
        }

        if ($batch_list) {
            $pr =   $pr->whereHas('penelitian', function($qz) use ($batch_penelitian){
                        $qz->whereIn('batch', $batch_penelitian)->where('isdelete','=','0');
                    });
        }

        if ($tahun_kegiatan) {
            $pr =   $pr->whereHas('penelitian', function($qz) use ($tahun_kegiatan){
                        $qz->where('tahun_kegiatan', $tahun_kegiatan)->where('isdelete','=','0');
                    });
        }
        $pr =   $pr->with('dosen')
                ->select('dosen', DB::raw('count(*) as jumlah_proposal'))
                ->groupBy('dosen')
                ->get()
                ->toArray();

        $group = [];

        foreach ($pr as $key => $value_pr) {
            $pl =   Monev_reviewer::where('isdelete','=','0')
                    ->with('penelitian');

            if ($jenis_penelitian) {
                $pl =   $pr->whereHas('penelitian', function($qz) use ($jenis_penelitian){
                            $qz->whereIn('jenis_penelitian', $jenis_penelitian)->where('isdelete','=','0');
                        });
            }

            if ($batch_list) {
                $pl =   $pl->whereHas('penelitian', function($qz) use ($batch_penelitian){
                            $qz->whereIn('batch', $batch_penelitian)->where('isdelete','=','0');
                        });
            }
            if ($tahun_kegiatan) {
                $pl =   $pl->whereHas('penelitian', function($qz) use ($tahun_kegiatan){
                            $qz->where('tahun_kegiatan', $tahun_kegiatan)->where('isdelete','=','0');
                        });
            }

            $pl =   $pl->where('dosen', $value_pr['dosen']['id'])
                    ->where('isdelete','=','0')
                    ->get()
                    ->toArray();

            $grouppl = [];
            $count_monev_reviewer_sudah= 0;
            $count_monev_reviewer_belum= 0;
            foreach ($pl as $key_pl => $value_pl) {
                if ($value_pl['status'] == 1) {
                    $count_monev_reviewer_sudah++;
                }else {
                    $count_monev_reviewer_belum++;
                }

                $grouppl['penelitian'][$key_pl]['id'] = $value_pl['id'];
                $grouppl['penelitian'][$key_pl]['judul'] = $value_pl['penelitian']['judul'];
            }

            $group[$key]['dosen'] = $value_pr['dosen']['nama'];
            $group[$key]['jumlah_proposal'] = $value_pr['jumlah_proposal'];
            $group[$key]['penelitian'] = $grouppl['penelitian'];
            $group[$key]['sudah_review'] = $count_monev_reviewer_sudah;
            $group[$key]['belum_review'] = $count_monev_reviewer_belum;
        }

        return $group;
    }

}
