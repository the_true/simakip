<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;

class Luarandikti extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r1"=>"active");
    public function __construct() {
        $config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Batch");
        $this->load->model("Batch_lists");
        $this->load->model("Batch_penelitian");
        $this->load->model("Dosen");
        $this->load->model("Jenis_penelitian");
        $this->load->model("fakultas");
        $this->load->model("Monev_reviewer");
        $this->load->model("Penelitian");
        $this->load->model("Penelitian_anggota");
        $this->load->model("Penelitian_reviewer");
        $this->load->model("Penelitian_review");
        $this->load->model("Penelitian_target_luaran_wajib");
        $this->load->model("Penelitian_target_luaran_tambahan");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Target_luaran");
        $this->load->model("Target_luaran_master");
        $this->load->helper('report');
    }

	public function index(){
        $tahun_kegiatan = Tahun_kegiatan::where('isdelete','=','0')->orderBy('tahun', 'asc')->get();
        $items['tahun_kegiatan'] = $tahun_kegiatan->toArray();
        $items['penelitian'] = $this->search();

        $this->twig->display('report/luarandikti', $items);
	}
    
    public function search(){
        $input = $this->input->get();
        $judul_penelitian = $this->input->get('judul_penelitian');
        $tahun_kegiatan = $this->input->get('tahun_usulan');
        $status = $this->input->get('status');
        
        $penelitian = [];
        
        if ($judul_penelitian || $tahun_kegiatan || $status) {
            $penelitian = Penelitian::with('jenis_penelitian', 'target_luaran')
                            ->where('isvalid','=','1')
                            ->where("isdelete","=","0");
            
            if($judul_penelitian){
     			$penelitian = $penelitian->where('judul', $judul_penelitian);
     		}
            
            if($tahun_kegiatan){
     			$penelitian = $penelitian->where('tahun_kegiatan', $tahun_kegiatan);
     		}
            
            $penelitian = $penelitian->get();
            $penelitian = $penelitian->toArray();    
            // echo "<pre>"; print_r($penelitian); echo "</pre>"; die("aaa");

        }
        
        // echo "<pre>"; print_r($penelitian); echo "</pre>"; die("asddssds");
        return $penelitian;
	}
}
