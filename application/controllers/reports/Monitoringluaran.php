<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;

class Monitoringluaran extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r1"=>"active");
    public function __construct() {
        $config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Batch");
        $this->load->model("Batch_lists");
        $this->load->model("Batch_penelitian");
        $this->load->model("Dosen");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Monev_luaran");
        $this->load->model("fakultas");
        $this->load->model("Penelitian");
        $this->load->model("Penelitian_laporan");
        $this->load->model("Penelitian_target_luaran_wajib");
        $this->load->model("Penelitian_target_luaran_tambahan");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Target_luaran");
        $this->load->model("Target_luaran_master");
        $this->load->helper('report');
    }

	public function index(){
        $tahun_kegiatan_qs = $this->input->get('tahun_usulan');
        $batch_list_qs = $this->input->get('batch');
        $fakultas_qs = $this->input->get('fakultas');
        $program_studi_qs = $this->input->get('program_studi');
        $luaran_penelitian_qs = $this->input->get('luaran_penelitian');

        $fakultas = Fakultas::where('isdelete','=','0')->get();
        $jenis_penelitian = Jenis_penelitian::where('isdelete','=','0')->get();
        $batch_lists = Batch_lists::where('isdelete','=','0')->where('type','penelitian')->get();
        $tahun_kegiatan = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun','desc')->get();

		$items["fakultas"] = $fakultas->toArray();
		$items["jenis_penelitian"] = $jenis_penelitian->toArray();
		$items["batch_lists"] = $batch_lists->toArray();
        $items["tahun_kegiatan"] = $tahun_kegiatan->toArray();
        $items['group_penelitian'] = $this->search();

        $items['target_luaran_master'] = Target_luaran_master::where('isdelete','=','0')->get()->toArray();

        $items['params'] = [
            'tahun_usulan' => $tahun_kegiatan_qs,
            'batch' => $batch_list_qs,
            'fakultas' => $fakultas_qs,
            'program_studi' => $program_studi_qs,
            'luaran_penelitian' => $luaran_penelitian_qs,
        ];

        $this->twig->display('report/monitoringluaran', $items);
	}

    public function detail(){
        $items['group_penelitian'] = $this->searchdetail();

        $items['target_luaran_master'] = Target_luaran_master::where('isdelete','=','0')->get()->toArray();

        $this->twig->display('report/monitoringluarandetail', $items);
    }

    public function search(){
        $input = $this->input->get();

        $default_view = 10;
        $page = 0;

        $view_get = $this->input->get("views");
		$page_get = $this->input->get("page");

        if(isset($page_get)) $page = $page_get-1;
        if(isset($view_get)) $default_view = $view_get;

        $default_skip = $page * $default_view;

        $jenis_penelitian = $this->input->get('jenis_penelitian');
        $tahun_kegiatan = $this->input->get('tahun_usulan');
        $batch_list = $this->input->get('batch');
        $fakultas = $this->input->get('fakultas');
        $program_studi = $this->input->get('program_studi');
        $luaran_penelitian = $this->input->get('luaran_penelitian');

        $gp = [];
        $total_ptlw = 0;
        $total_ptlt = 0;
        $count = 0;

        $penelitian = DB::table('penelitian')->join('jenis_penelitian', 'penelitian.jenis_penelitian', '=', 'jenis_penelitian.id');

		if($fakultas){
            $penelitian = $penelitian->join('dosen', 'penelitian.dosen', '=', 'dosen.id');
            $penelitian = $penelitian->where('fakultas','=',$fakultas);

            if($program_studi){
                $penelitian = $penelitian->where('program_studi','=',$program_studi);
            }
        }

        if($batch_list){
 			$penelitian = $penelitian->join('batch', 'penelitian.batch', '=', 'batch.batch_penelitian');
 			$penelitian = $penelitian->join('batch_lists', 'batch.batch_lists', '=', 'batch_lists.id');
            $penelitian = $penelitian->where('batch_lists.id','=',$batch_list);
 		}

        if($jenis_penelitian){
 			$penelitian = $penelitian->where('penelitian.jenis_penelitian', $jenis_penelitian);
 		}

        if($tahun_kegiatan){
            $penelitian = $penelitian->join('tahun_kegiatan', 'penelitian.tahun_kegiatan', '=', 'tahun_kegiatan.id');
 			$penelitian = $penelitian->where('tahun_kegiatan.id', $tahun_kegiatan);
 		}

        $penelitian = $penelitian->groupBy('penelitian.jenis_penelitian')
                        ->selectRaw('penelitian.jenis_penelitian, jenis_penelitian.nama');

        $penelitian = $penelitian->where('penelitian.isvalid','=','1')
                        ->where("penelitian.isdelete","=","0")
                        ->orderBy('jenis_penelitian.id','desc');

        $count = count($penelitian->get());

        $penelitian = $penelitian->take($default_view)->skip($default_skip);

        $penelitian = $penelitian->get();

        $group_penelitian = $penelitian;

	    foreach ($group_penelitian as $key => $value) {
            $penelitian_id = DB::table('penelitian')->select('penelitian.id');

            if($fakultas){
                $penelitian_id = $penelitian_id->join('dosen', 'penelitian.dosen', '=', 'dosen.id');
                $penelitian_id = $penelitian_id->where('fakultas','=',$fakultas);

                if($program_studi){
                    $penelitian_id = $penelitian_id->where('program_studi','=',$program_studi);
                }
            }

    		if($program_studi) $penelitian_id->where('dosen',function($q){ $q->where('program_studi','=',$this->input->get('program_studi'));});

            if($batch_list){
     			$penelitian_id = $penelitian_id->join('batch', 'penelitian.batch', '=', 'batch.batch_penelitian');
     			$penelitian_id = $penelitian_id->join('batch_lists', 'batch.batch_lists', '=', 'batch_lists.id');
                $penelitian_id = $penelitian_id->where('batch_lists.id','=',$batch_list);
     		}

            if($tahun_kegiatan){
                $penelitian_id = $penelitian_id->join('tahun_kegiatan', 'penelitian.tahun_kegiatan', '=', 'tahun_kegiatan.id');
     			$penelitian_id = $penelitian_id->where('tahun_kegiatan.id', $tahun_kegiatan);
     		}

            $penelitian_id = $penelitian_id->where('jenis_penelitian','=', $value['jenis_penelitian'])
                    ->where("penelitian.isdelete","=","0")
                    ->lists('penelitian.id');

            $group_by_target = DB::table('penelitian')
				->join('target_luaran', 'penelitian.target', '=', 'target_luaran.id')
                ->groupBy('penelitian.target')
                ->selectRaw('penelitian.target, count(*) as total, target_luaran.nama')
				->whereNotNull('penelitian.target')
				->where('penelitian.jenis_penelitian','=', $value['jenis_penelitian'])
                ->where("penelitian.isdelete","=","0");

            if($fakultas){
                $group_by_target = $group_by_target->join('dosen', 'penelitian.dosen', '=', 'dosen.id');
                $group_by_target = $group_by_target->where('fakultas','=',$fakultas);

                if($program_studi){
                    $group_by_target = $group_by_target->where('program_studi','=',$program_studi);
                }
            }

    		if($program_studi) $group_by_target->where('penelitian.dosen',function($q){ $q->where('program_studi','=',$this->input->get('program_studi'));});

            if($batch_list){
     			$group_by_target = $group_by_target->join('batch', 'penelitian.batch', '=', 'batch.batch_penelitian');
     			$group_by_target = $group_by_target->join('batch_lists', 'batch.batch_lists', '=', 'batch_lists.id');
                $group_by_target = $group_by_target->where('batch_lists.id','=',$batch_list);
     		}

            if($jenis_penelitian){
     			$group_by_target = $group_by_target->where('penelitian.jenis_penelitian', $jenis_penelitian);
     		}

            if($tahun_kegiatan){
                $group_by_target = $group_by_target->join('tahun_kegiatan', 'penelitian.tahun_kegiatan', '=', 'tahun_kegiatan.id');
     			$group_by_target = $group_by_target->where('tahun_kegiatan.id', $tahun_kegiatan);
     		}

   			$group_by_target = $group_by_target->get();

            $group_penelitian[$key]['report_target_luaran'] = $group_by_target;

            $count_ptlw = DB::table('penelitian_target_luaran_wajib')
                      ->whereIn('penelitian_id', $penelitian_id)
                      ->count();
            $count_ptlt = DB::table('penelitian_target_luaran_tambahan')
                    ->whereIn('penelitian_id', $penelitian_id)
                    ->count();

            $group_penelitian[$key]['jumlah_ptlw'] = $count_ptlw;
            $group_penelitian[$key]['jumlah_ptlt'] = $count_ptlt;

            $total_ptlw = $total_ptlw + $count_ptlw;
            $total_ptlt = $total_ptlt + $count_ptlt;
        }

        $data['items'] = $group_penelitian;
        $data['total_ptlw'] = $total_ptlw;
        $data['total_ptlt'] = $total_ptlt;
    	$data['views'] = $default_view;
    	$data['page'] = $page+1;
    	$data['total_pages'] = $count>0? ceil($count/$default_view):1;
        return $data;
	}

    public function searchdetail(){
        $input = $this->input->get();
        $jenis_penelitian = $this->input->get('jenis_penelitian');;
        $tahun_kegiatan = $this->input->get('tahun_usulan');
        $batch_list = $this->input->get('batch');
        $fakultas = $this->input->get('fakultas');
        $program_studi = $this->input->get('program_studi');
        $group_penelitian = [];

        if ($jenis_penelitian || $tahun_kegiatan || $batch_list || $fakultas || $program_studi) {
            $penelitian = DB::table('penelitian')
                        ->join('jenis_penelitian', 'penelitian.jenis_penelitian', '=', 'jenis_penelitian.id')
            			->groupBy('penelitian.jenis_penelitian')
                        ->selectRaw('penelitian.jenis_penelitian, jenis_penelitian.nama')
            			->where('penelitian.isvalid','=','1')
                        ->where("penelitian.isdelete","=","0");

    		if($fakultas) $penelitian->whereHas('dosen',function($q){ $q->where('fakultas','=',$this->input->get('fakultas'));});

    		if($program_studi) $penelitian->whereHas('dosen',function($q){ $q->where('program_studi','=',$this->input->get('program_studi'));});

            if($batch_list){
     			$penelitian = $penelitian->join('batch', 'penelitian.batch', '=', 'batch.batch_penelitian');
     			$penelitian = $penelitian->join('batch_lists', 'batch.batch_lists', '=', 'batch_lists.id');
                $penelitian = $penelitian->where('batch_lists.id','=',$batch_list);
     		}

            if($jenis_penelitian){
     			$penelitian = $penelitian->where('penelitian.jenis_penelitian', $jenis_penelitian);
     		}

            if($tahun_kegiatan){
                $penelitian = $penelitian->join('tahun_kegiatan', 'penelitian.tahun_kegiatan', '=', 'tahun_kegiatan.id');
     			$penelitian = $penelitian->where('tahun_kegiatan.id', $tahun_kegiatan);
     		}

            $group_penelitian['detail'] = $penelitian->get()[0];

            $penelitian_id = DB::table('penelitian')->select('id')
                     ->where('jenis_penelitian','=', $jenis_penelitian)
                     ->where("isdelete","=","0")
                     ->lists('id');

            $group_by_target = DB::table('penelitian')
				->join('target_luaran', 'penelitian.target', '=', 'target_luaran.id')
                ->groupBy('penelitian.target')
                ->selectRaw('penelitian.target, count(*) as total, target_luaran.nama')
				->whereNotNull('penelitian.target')
				->where('penelitian.jenis_penelitian','=', $jenis_penelitian)
                ->where("penelitian.isdelete","=","0");

            if($fakultas) $group_by_target->where('penelitian.dosen',function($q){ $q->where('fakultas','=',$fakultas);});
            if($program_studi) $group_by_target->where('penelitian.dosen',function($q){ $q->where('program_studi','=',$program_studi);});

            if($batch_list){
     			$group_by_target = $group_by_target->join('batch', 'penelitian.batch', '=', 'batch.batch_penelitian');
     			$group_by_target = $group_by_target->join('batch_lists', 'batch.batch_lists', '=', 'batch_lists.id');
                $group_by_target = $group_by_target->where('batch_lists.id','=',$batch_list);
     		}

            if($tahun_kegiatan){
                $group_by_target = $group_by_target->join('tahun_kegiatan', 'penelitian.tahun_kegiatan', '=', 'tahun_kegiatan.id');
     			$group_by_target = $group_by_target->where('tahun_kegiatan.id', $tahun_kegiatan);
     		}

            $group_by_target = $group_by_target->get();

            $group_penelitian['report_target_luaran'] = $group_by_target;

            $penelitian_laporan = DB::table('penelitian_laporan')
                    ->whereIn('penelitian', $penelitian_id)
                    ->get();

            foreach ($group_by_target as $key_target => $value_target) {
                $total_penelitian_laporan_lw = DB::table('penelitian_laporan')
                            ->join('penelitian', 'penelitian_laporan.penelitian', '=', 'penelitian.id')
                			->where('penelitian.isvalid','=','1')
                			->where('penelitian.jenis_penelitian','=',$jenis_penelitian)
                            ->where("penelitian.isdelete","=","0")
                            ->where("penelitian_laporan.status_luaran_wajib","=","1")
                            ->count();

                $total_penelitian_laporan_lt = DB::table('penelitian_laporan')
                            ->join('penelitian', 'penelitian_laporan.penelitian', '=', 'penelitian.id')
                			->where('penelitian.isvalid','=','1')
                			->where('penelitian.jenis_penelitian','=',$jenis_penelitian)
                            ->where("penelitian.isdelete","=","0")
                            ->where("penelitian_laporan.status_luaran_tambahan","=","1")
                            ->count();

                $group_penelitian['report_target_luaran'][$key_target]['total_tercapai'] = $total_penelitian_laporan_lw;
                $group_penelitian['report_target_luaran'][$key_target]['total_belum_tercapai'] = $total_penelitian_laporan_lt;
            }
        }

        return $group_penelitian;
	}

    public function batchjson($jenis_penelitian = null){
        // $batch_penelitian = Batch_penelitian::with('batch')->where('isdelete','=','0')->where('jenis_penelitian', $jenis_penelitian)->get();
        $batch_penelitian = Batch_penelitian::where("isdelete","=","0")->where("jenis_penelitian",12)->orderBy('tahun','DESC')->get();
		$batch_penelitian->load("batch")->load("batch.batch_lists");
		$batch_penelitian = $batch_penelitian->toArray();
        echo "<pre>"; print_r(json_encode($batch_penelitian)); echo "</pre>"; die();
        echo json_encode($batch_penelitian);
        exit;
        // $this->twig->display('report/monitoringluaran', $items);
	}

    public function tahunjson($jenis_penelitian = null){
    	$jenis_penelitian = $this->uri->segment(4);
        $penelitian = Penelitian::where("isdelete","=","0")->where("jenis_penelitian",$jenis_penelitian)->whereNotNull('tahun_kegiatan')->distinct()->get(['tahun_kegiatan']);
		$penelitian = $penelitian->load('tahun_kegiatan');
		$penelitian = $penelitian->toArray();
        header('Content-Type: application/json');
        echo json_encode($penelitian);
        exit;
	}
}
