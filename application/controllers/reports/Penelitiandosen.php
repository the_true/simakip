<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;

class Penelitiandosen extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r1"=>"active");
    public function __construct() {
        $config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Batch");
        $this->load->model("Batch_lists");
        $this->load->model("Batch_penelitian");
        $this->load->model("Dosen");
        $this->load->model("Jenis_penelitian");
        $this->load->model("fakultas");
        $this->load->model("Penelitian");
        $this->load->model("Penelitian_anggota");
        $this->load->model("Penelitian_reviewer");
        $this->load->model("Penelitian_review");
        $this->load->model("Penelitian_target_luaran_wajib");
        $this->load->model("Penelitian_target_luaran_tambahan");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Target_luaran");
        $this->load->model("Target_luaran_master");
        $this->load->helper('report');
    }

	public function index(){
        $jenis_penelitian = Jenis_penelitian::where('isdelete','=','0')->get();
        $batch_lists = Batch_lists::where('isdelete','=','0')->where('type','penelitian')->get();
		$items['jenis_penelitian'] = $jenis_penelitian->toArray();
		$items['batch_lists'] = $batch_lists->toArray();
        $items['penelitian'] = $this->search();

        $this->twig->display('report/penelitiandosen', $items);
	}
    
    public function search(){
        $input = $this->input->get();
        
        $default_view = 10;
        $page = 0;
        
        $view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		
        if(isset($page_get)) $page = $page_get-1;
        if(isset($view_get)) $default_view = $view_get;
        
        $default_skip = $page * $default_view;
        
        $jenis_penelitian = $this->input->get('jenis_penelitian');
        $tahun_kegiatan = $this->input->get('tahun_usulan');
        $batch_list = $this->input->get('batch');
        $nama_dosen = $this->input->get('nama_dosen');
        
        $penelitian = [];
        
        if ($jenis_penelitian || $tahun_kegiatan || $batch_list) {
            $penelitian = Penelitian::with('jenis_penelitian', 'target_luaran')
                            ->where('isvalid','=','1')
                            ->where("isdelete","=","0")
                            ->with('anggota')
                            ->with('anggota.dosen')
                            ->with('Penelitian_review')
                            ->with('Penelitian_reviewer');
            if ($nama_dosen) {
                $penelitian = $penelitian->with(['dosen.fakultas' => function($q) use ($nama_dosen) {
                    $q->where('nama_dosen', $nama_dosen);
                }]);
            }else {
                $penelitian = $penelitian->with('dosen.fakultas');
            }
            
            if($batch_list){
     			$penelitian = $penelitian->whereHas('batch',function($q) use ($batch_list) {
                    $q->where('batch_lists', $batch_list);
                });
     		}
            
            if($jenis_penelitian){
     			$penelitian = $penelitian->where('jenis_penelitian', $jenis_penelitian);
     		}
            
            if($tahun_kegiatan){
     			$penelitian = $penelitian->where('tahun_kegiatan', $tahun_kegiatan);
     		}
            
            $penelitian = $penelitian->orderBy('created_at','desc')->take($default_view)->skip($default_skip)->get();
            $penelitian->load('batch')->load('batch.batch_penelitian.tahun');
            $penelitian = $penelitian->toArray();            
        }
        return $penelitian;
	}
}
