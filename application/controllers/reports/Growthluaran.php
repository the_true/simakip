<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;
class Growthluaran extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r2"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio','angkatofirstbulan','angkatobulan'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("fakultas");
        $this->load->helper('report');
        $this->load->helper('tanggal');

    }

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$items=[];
		$fakultas = Fakultas::where('isdelete','=','0')->get();

		$d = $this->getQuery();
		if(!empty($d)){
			$result = $this->getData($d);
			$items["items"] = $result;
			$items["table"] = $d["format_view"];
			$items["orderby"] = $d["order"];
			$w = $this->getQueryChart();
			$items["charts"] = $this->getData($w);
			$items["dari"] = $d["format_date"][0];
			$items["sampai"] = end($d["format_date"]);
		}
		// echo json_encode($d); die;
		$items["fakultas"] = $fakultas->toArray();
		// echo json_encode($result);
		$this->twig->display('report/growthluaran',array_merge($items,$this->menu));
	}

	public function getData($d){
		// echo json_encode($d);
		$jurnal = cm($d["format_date"],"jurnal");
		$buku_ajar = cm($d["format_date"],"buku_ajar");
		$forum_ilmiah = cm($d["format_date"],"forum_ilmiah");
		$hki = cm($d["format_date"],"hki");
		$luaran_lain = cm($d["format_date"],"luaran_lain");
		$penyelenggara_forum_ilmiah = cm($d["format_date"],"penyelenggara_forum_ilmiah");
		$penelitian_hibah =  cm($d["format_date"],"penelitian_hibah");
		$sum = cs($d["format_date"]);
		// echo $jurnal; die;
		$dari = $d["dari"];
		$sampai = $d["sampai"];
		$where = $d["where"];
		$group = $d["group"];
		$select = $d["select"];
		$order = $d["order"];
		$query = "
		SELECT id, program_studi_id $select, 
		    $sum
		    SUM(total) as total
		FROM
		(

			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as nidn, 
			        $jurnal
					count(jurnal.id) as total,
					'jurnal' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN jurnal ON jurnal.dosen=dosen.id AND jurnal.isdelete=0 AND jurnal.isvalidate=1  fc($dari,jurnal) fc($sampai,jurnal)
			WHERE dosen.fakultas<>0 $where
			GROUP BY dosen.fakultas $group
			having total>0
			UNION
			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as dosen, 
					$buku_ajar
					count(buku_ajar.id) as total,
					'buku_ajar' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN buku_ajar ON buku_ajar.dosen=dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1  fc($dari,buku_ajar) fc($sampai,buku_ajar)
			WHERE dosen.fakultas<>0 $where
			GROUP BY dosen.fakultas $group
			having total>0
			UNION			
			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as dosen, 
					$forum_ilmiah
					count(forum_ilmiah.id) as total,
					'forum_ilmiah' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN forum_ilmiah ON forum_ilmiah.dosen=dosen.id AND forum_ilmiah.isdelete=0 AND forum_ilmiah.isvalidate=1  fc($dari,forum_ilmiah) fc($sampai,forum_ilmiah)
			WHERE dosen.fakultas<>0 $where
			GROUP BY dosen.fakultas $group
			having total>0
			UNION			
			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as dosen, 
					$hki
					count(hki.id) as total,
					'hki' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN hki ON hki.dosen=dosen.id AND hki.isdelete=0 AND hki.isvalidate=1  fc($dari,hki) fc($sampai,hki)
			WHERE dosen.fakultas<>0 $where
			GROUP BY dosen.fakultas $group
			having total>0
			UNION			
			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as dosen, 
					$luaran_lain
					count(luaran_lain.id) as total,
					'luaran_lain' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN luaran_lain ON luaran_lain.dosen=dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1  fc($dari,luaran_lain) fc($sampai,luaran_lain)
			WHERE dosen.fakultas<>0 $where
			GROUP BY dosen.fakultas $group
			having total>0
			UNION			
			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as dosen, 
					$penyelenggara_forum_ilmiah
					count(penyelenggara_forum_ilmiah.id) as total,
					'penyelenggara_forum_ilmiah' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen=dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1  fc($dari,penyelenggara_forum_ilmiah) fc($sampai,penyelenggara_forum_ilmiah)
			WHERE dosen.fakultas<>0 $where  
			GROUP BY dosen.fakultas $group
			having total>0
			UNION			
			SELECT 
					fakultas.id, fakultas.nama, program_studi.nama as program_studi, program_studi.id as program_studi_id, dosen.nama as dosen, 
					$penelitian_hibah
					count(penelitian_hibah.id) as total,
					'penelitian_hibah' as type
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
			LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen=dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1  fc($dari,penelitian_hibah) fc($sampai,penelitian_hibah)
			WHERE dosen.fakultas<>0 $where   
			GROUP BY dosen.fakultas $group
			having total>0
		) as dosen
		GROUP BY dosen.id $group
		ORDER BY total $order
			";
		$allowedFunctions = array("fc");
		// echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
		$query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
		     function ($m) {
		          return $m[1]($m[2],$m[3]);
		     }, $query);
		$data = DB::select($query);
		// echo $query; die;
		// echo json_encode($data); die;
		return $data;
	}

	public function getQuery(){
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');
		$urutkan = $this->input->get('orderby');

		$where = "";
		$group = "";
		$select = ", nama";
		$order = "DESC";

		$fakultasx = "";
		if(!empty($fakultas)){
			$where=" AND dosen.fakultas=".$fakultas." ";
			// echo $fakultasx; die;
		}

		if(isset($urutkan)){
			$order = $urutkan;
		}

		$items["orderby"] = $order;

		if(isset($dari) && !empty($dari)){
			$dari = explode('-',$dari);
			$dari = $dari[1].'-'.$dari[0].'-01 00:00:00';
			$dari_list = " AND created_at>='$dari'";
		}else{
			return [];
		}
		if(isset($dari) && !empty($dari)){
			$sampai = explode('-',$sampai);
			$sampai = $sampai[1].'-'.$sampai[0].'-31 23:59:59';
			$sampai_list = " AND created_at<='$sampai'";
		}else{
			return [];
		}

		$date_list = mbetweendate($dari,$sampai);

		if($program_studi=="all"){
			$group = ',dosen.program_studi';
			$select = ', program_studi as nama ';
		}else if($program_studi!="all" && isset($program_studi)){
			$where = "AND dosen.program_studi=$program_studi";
			$group = ',dosen.program_studi, dosen.nidn';
			$select = ', nidn as nama ';
		}

		return ["select"=>$select,"where"=>$where,"group"=>$group,"dari"=>$dari_list,"sampai"=>$sampai_list,"format_date"=>$date_list[0],"format_view"=>$date_list[1],"order"=>$order];
	}


	public function getQueryChart(){
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');

		$where = "";
		$group = "";
		$select = ", nama";

		$fakultasx = "";
		if(!empty($fakultas)){
			$where=" AND dosen.fakultas=".$fakultas." ";
			// echo $fakultasx; die;
		}

		if(isset($dari) && !empty($dari)){
			$dari = explode('-',$dari);
			$dari = $dari[1].'-'.$dari[0].'-01 00:00:00';
			$dari_list = " AND created_at>='$dari'";
		}else{
			return [];
		}
		if(isset($dari) && !empty($dari)){
			$sampai = explode('-',$sampai);
			$sampai = $sampai[1].'-'.$sampai[0].'-31 23:59:59';
			$sampai_list = " AND created_at<='$sampai'";
		}else{
			return [];
		}

		$date_list = mbetweendate($dari,$sampai);

		if($program_studi=="all"){
			$group = '';
			$select = ', nama ';
		}else if($program_studi!="all" && isset($program_studi)){
			$where = "AND dosen.program_studi=$program_studi";
			$group = ',dosen.program_studi';
			$select = ', program_studi as nama ';
		}

		return ["select"=>$select,"where"=>$where,"group"=>$group,"dari"=>$dari_list,"sampai"=>$sampai_list,"format_date"=>$date_list[0],"format_view"=>$date_list[1],"order"=>"DESC"];
	}

 	public function pdf(){
 		$d = $this->getQuery();
 		$result = $this->getData($d);

 		$this->load->library('libexcel');
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_growthluaran.xlsx");
 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
			'borders' => array(
 				'allborders' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'8E7CC3'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 12,
 				)
 			);
 		$i = 4;
 		$alphabet = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
 		foreach($d["format_view"] as $key=>$value){
 			$excel->setActiveSheetIndex(0)->setCellValue($alphabet[$i].'9', "Tahun ".$key);
 			for($x=0;$x<count($value);$x++){
 				$a = $x+$i;
 				$w = $excel->setActiveSheetIndex(0)->setCellValue($alphabet[$a].'10',"  ".angkatofirstbulan($value[$x])."  ");
 			}

 			$i+=count($value);
 			$excel->setActiveSheetIndex(0)->mergeCells($alphabet[$i-count($value)].'9:'.$alphabet[$i-1].'9');
 		}
 		$indexterakhir = $i;
 		$excel->setActiveSheetIndex(0)->setCellValue($alphabet[$i]."9","Total")->mergeCells($alphabet[$i]."9:".$alphabet[$i]."10");
 		$excel->getActiveSheet()->getStyle('E9:'.$alphabet[$i].'10')->applyFromArray( $style_header );

		for($col = 'E'; $col !== $alphabet[$i-1]; $col++) {
		    $excel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
		}

 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel->getProperties()->setCreator("Simakip");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($result);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$fakultas='C'.$urut;

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($fakultas, $result[$i]["nama"])->mergeCells($fakultas.':D'.$urut);
			$a=1;
			foreach($result[$i] as $key=>$value){
				if(strpos($key, '-') !== false){
					$excel->setActiveSheetIndex(0)
					->setCellValue($alphabet[$a].$urut, $value);
				}
				$a++;
			}
			$excel->setActiveSheetIndex(0)->setCellValue($alphabet[$indexterakhir].$urut,$result[$i]["total"]);
			$laststyle=$alphabet[$indexterakhir].$urut;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header

		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_GrowthLuaran_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}

}


// SELECT id, nama, 
// 	SUM(bulan201611) as '2016-11',
//     SUM(bulan201612) as '2016-12',
//     SUM(bulan201701) as '2017-01',
//     SUM(bulan201702) as '2017-02',
//     SUM(bulan201703) as '2017-03',
//     SUM(bulan201704) as '2017-04',
//     SUM(total) as total
// FROM
// (
// SELECT 
// 		fakultas.id, fakultas.nama,
//         count(CASE WHEN MONTH(jurnal.created_at)=11 AND YEAR(jurnal.created_at)=2016 THEN jurnal.id END) as bulan201611,
//         count(CASE WHEN MONTH(jurnal.created_at)=12 AND YEAR(jurnal.created_at)=2016 THEN jurnal.id END) as bulan201612,
//         count(CASE WHEN MONTH(jurnal.created_at)=1 AND YEAR(jurnal.created_at)=2017 THEN jurnal.id END) as bulan201701,
//         count(CASE WHEN MONTH(jurnal.created_at)=2 AND YEAR(jurnal.created_at)=2017 THEN jurnal.id END) as bulan201702,
//         count(CASE WHEN MONTH(jurnal.created_at)=3 AND YEAR(jurnal.created_at)=2017 THEN jurnal.id END) as bulan201703,
//         count(CASE WHEN MONTH(jurnal.created_at)=4 AND YEAR(jurnal.created_at)=2017 THEN jurnal.id END) as bulan201704,
// 		count(jurnal.id) as total,
// 		'jurnal' as type
// FROM dosen 
// LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
// LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
// LEFT JOIN jurnal ON jurnal.dosen=dosen.id AND jurnal.isdelete=0 AND jurnal.isvalidate=1  AND jurnal.created_at>='2016-01-01 00:00:00'  AND jurnal.created_at<='2017-03-31 23:59:59'
// WHERE dosen.fakultas<>0
// GROUP BY dosen.fakultas   
// having total>0
// UNION
// SELECT 
// 		fakultas.id, fakultas.nama,
// 		count(CASE WHEN MONTH(buku_ajar.created_at)=11 AND YEAR(buku_ajar.created_at)=2016 THEN buku_ajar.id END) as bulan201611,
//         count(CASE WHEN MONTH(buku_ajar.created_at)=12 AND YEAR(buku_ajar.created_at)=2016 THEN buku_ajar.id END) as bulan201612,
//         count(CASE WHEN MONTH(buku_ajar.created_at)=1 AND YEAR(buku_ajar.created_at)=2017 THEN buku_ajar.id END) as bulan201701,
//         count(CASE WHEN MONTH(buku_ajar.created_at)=2 AND YEAR(buku_ajar.created_at)=2017 THEN buku_ajar.id END) as bulan201702,
//         count(CASE WHEN MONTH(buku_ajar.created_at)=3 AND YEAR(buku_ajar.created_at)=2017 THEN buku_ajar.id END) as bulan201703,
//         count(CASE WHEN MONTH(buku_ajar.created_at)=4 AND YEAR(buku_ajar.created_at)=2017 THEN buku_ajar.id END) as bulan201704,
// 		count(buku_ajar.id) as total,
// 		'buku_ajar' as type
// FROM dosen 
// LEFT JOIN fakultas ON fakultas.id=dosen.fakultas 
// LEFT JOIN program_studi ON program_studi.id = dosen.program_studi 
// LEFT JOIN buku_ajar ON buku_ajar.dosen=dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1  AND buku_ajar.created_at>='2016-01-01 00:00:00'  AND buku_ajar.created_at<='2017-03-31 23:59:59'
// WHERE dosen.fakultas<>0   
// GROUP BY dosen.fakultas
// having total>0
// ) as result
// GROUP BY result.id