<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Review extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r4"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Jenis_Penelitian");
        $this->load->model("Fakultas");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Batch_lists");

    }

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$items = [];
		$jenis_penelitian = Jenis_Penelitian::where('isdelete', '=', '0')->get();
		$tahun_kegiatan = Tahun_kegiatan::where('isdelete', '=', '0')->orderBy('tahun','desc')->get();
		$batch_lists = Batch_lists::penelitian()->where('isdelete', '=', '0')->orderBy('nama','asc')->get();
		$fakultas = Fakultas::where('isdelete','=','0')->get();

		$items = $this->datareport($items);

		$items["jenis_penelitian"] = $jenis_penelitian->toArray();
		$items["tahun_kegiatan"] = $tahun_kegiatan->toArray();
		$items["batch_lists"] = $batch_lists->toArray();
		$items["fakultas"] = $fakultas->toArray();
		$this->twig->display('report/review',array_merge($items,$this->menu));
	}

	private function filter($item){
		$jenis_penelitian = $this->input->get('jenis_penelitian');
		if($jenis_penelitian) $item = $item->whereHas('penelitian',function($q){
			$q->where('jenis_penelitian','=',$this->input->get('jenis_penelitian'));
		});

		$fakultas = $this->input->get('fakultas');
		if($fakultas) $item->whereHas('dosen',function($q){ $q->where('fakultas','=',$this->input->get('fakultas'));});

		$fakultas = $this->input->get('program_studi');
		if($fakultas) $item->whereHas('dosen',function($q){ $q->where('program_studi','=',$this->input->get('program_studi'));});

		$batch = $this->input->get('batch');
		if($batch){
 			$item = $item->whereHas('penelitian',function($q) use ($batch){
			$q->whereHas('batch',function($qa) use ($batch) {
				$qa->where('batch_lists','=',$batch);
			});
		});
 		}

		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
		if($tahun_kegiatan){
 			$item = $item->whereHas('penelitian',function($q) use ($tahun_kegiatan){
				$q->where('tahun_kegiatan','=',$tahun_kegiatan);
			});
 		}

		return $item;
	}

	private function datareport($item,$paging=True){
		$cari = $this->input->get("cari");
		if(!$cari) return $item;
		$this->load->model('Batch');
		$this->load->model('batch_penelitian');
		$this->load->model('penelitian_review');
		$this->load->model('penelitian_laporan');
		$this->load->model('Penelitian');
		$this->load->model('tahun_kegiatan');
		$this->load->model('Penelitian_nilai');
		$this->load->model('Penelitian_Anggota');
		$this->load->model('Jenis_penelitian');
		$this->load->model('Fakultas');
		$this->load->model('Program_studi');
		$this->load->model('Surat_kontrak');
		$this->load->model('Dosen');
		$suratkontrak = Surat_kontrak::where("isdelete","=","0")
										->with('dosen')->with('dosen.fakultas')->with('dosen.program_studi')
								        ->with('penelitian')->with('penelitian.anggota.dosen')
										->with('penelitian.tahun_kegiatan')->with('penelitian.jenis_penelitian')
										->with('penelitian.batch')->with('penelitian.batch.batch_penelitian')
										->with('penelitian.batch.batch_penelitian.tahun')->with('penelitian.penelitian_review_exselected')->with('penelitian.penelitian_review_inselected.penelitian_nilai');
		$suratkontrak = $this->filter($suratkontrak);
 		$info = $this->create_paging($suratkontrak);
 		if($paging) $suratkontrak = $suratkontrak->take($info["limit"])->skip($info["skip"]);
 		$suratkontrak = $suratkontrak->get();
 		$item["items"] = $suratkontrak->toArray();
 		$item = array_merge($item,$info);
		return $item;
	}

 	public function pdf(){
 		$items = [];
 		$data = $this->datareport($items,False);
 		$data = $data["items"];

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				// 'bottom' => $default_border,
 				// 'left' => $default_border,
 				// 'top' => $default_border,
 				// 'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_surat_kontrak.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_penelitian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$jenis_penelitian = 'G'.$urut;
			$nilai_akumulasi = 'H'.$urut;
			$anggaran_usulan = 'I'.$urut;
			$anggaran_rekomendasi= ['J'.$urut,'K'.$urut];
			$fakultas = 'L'.$urut;
			$anggaran_disetujui = 'M'.$urut;
			$tahap1 = 'N'.$urut;
			$tahap2 = 'O'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]['penelitian']["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]['penelitian']["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$rekomendasi_string=[];
			for($x=0;$x<count($data[$i]['penelitian']["penelitian_review_exselected"]);$x++){
				$rekomendasi_string[]= $data[$i]['penelitian']["penelitian_review_exselected"][$x]["rp_rekomendasi"];
			}

			$disetujui_string = "";
			$nilai_akumulasix = 0;
			$anggaran = 0;
			for($x=0;$x<count($data[$i]['penelitian']["penelitian_review_inselected"]);$x++){
				if($data[$i]['penelitian']["penelitian_review_inselected"][$x]["isselected"]==1){
					$anggaran = $data[$i]['penelitian']["penelitian_review_inselected"][$x]["rekomendasi"];
					$disetujui_string = $data[$i]['penelitian']["penelitian_review_inselected"][$x]["rp_rekomendasi"];
					foreach($data[$i]['penelitian']["penelitian_review_inselected"][$x]['penelitian_nilai'] as $value){
						$nilai_akumulasix += (float) $value['nilai'];
					}
				}
			}

			$hitung = (float) $data[$i]['dana'] * (float) $anggaran/ 100;
			$hitung = ceil($hitung/100000);
			$dana1 = (int) $hitung*100000;
			$dana2 = (int) $anggaran-$dana1;

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_penelitian, $data[$i]['penelitian']['judul'])->mergeCells($judul_penelitian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($jenis_penelitian, $data[$i]['penelitian']['jenis_penelitian']['nama'])
			->setCellValue($nilai_akumulasi, $nilai_akumulasix)
			->setCellValueExplicit($anggaran_usulan, $data[$i]['penelitian']['rp_total_biaya'],PHPExcel_Cell_DataType::TYPE_STRING);

			for($waw=0;$waw<count($rekomendasi_string);$waw++){
				$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_rekomendasi[$waw], $rekomendasi_string[$waw],PHPExcel_Cell_DataType::TYPE_STRING);
			}
			
			
			$excel->setActiveSheetIndex(0)->setCellValueExplicit($anggaran_disetujui, $disetujui_string,PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);
			// if($data[$i]['penelitian_reviewer'])
			if($data[$i]['dana']){
				$excel->setActiveSheetIndex(0)->setCellValue($tahap1, $dana1);
				$excel->setActiveSheetIndex(0)->setCellValue($tahap2, $dana2);
			}
			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$laststyle=$tahap2;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		// for($col = 'A'; $col !== 'N'; $col++) {
		//     $excel->getActiveSheet()
		//         ->getColumnDimension($col)
		//         ->setAutoSize(true);
		// }
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_HasilReview_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
