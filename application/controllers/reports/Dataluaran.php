<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Capsule\Manager as DB;
class Dataluaran extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r2"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config,false);
        $this->load->model("program_studi");
        $this->load->model("Dosen");
        $this->load->model("Fakultas");
        $this->load->helper('report');
    }

	public function index(){
		$items = [];
		$fakultas = Fakultas::where('isdelete','=','0')->get();
		$result = $this->getFakultasData();
		$items["items"] = $result;
		$items["fakultas"] = $fakultas->toArray();
		$this->twig->display('report/dataluaran',array_merge($items,$this->menu));
	}

	public function getFakultasData(){
		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		$fakultas = $this->input->get('fakultas');
		$program_studi= $this->input->get('program_studi');

		$where = "";
		$group = "";
		$fakultasx = "";
		if(!empty($fakultas)){
			$fakultasx=" AND dosen.fakultas=".$fakultas;
		}

		if(isset($dari) && !empty($dari)){
			$dari = explode('-',$dari);
			$dari = $dari[1].'-'.$dari[0].'-01 00:00:00';
			$dari = " AND created_at>='$dari'";
		}else{
			return;
		}
		if(isset($dari) && !empty($dari)){
			$sampai = explode('-',$sampai);
			$sampai = $sampai[1].'-'.$sampai[0].'-31 23:59:59';
			$sampai = " AND created_at<='$sampai'";
		}else{
			return;
		}

		if($program_studi=="all" || $program_studi==""){
			$group = ',dosen.program_studi';
		}else if($program_studi!="all" && isset($program_studi)){
			$where = "AND dosen.program_studi=$program_studi";
			$group = ',dosen.program_studi, dosen.id';
		}

		$query = "
		SELECT 
			SUM(jurnalnya) as jurnalnya,
			SUM(buku_ajarnya) as buku_ajarnya,
			SUM(forum_ilmiahnya) as forum_ilmiahnya,
			SUM(hkinya) as hkinya,
			SUM(luaran_lainnya) as luaran_lainnya,
			SUM(penyelenggara_forum_ilmiahnya) as penyelenggara_forum_ilmiahnya,
			SUM(penelitian_hibahnya) as penelitian_hibahnya

		FROM (
				SELECT dosen.fakultas, fakultas.nama as fakultas_str ,dosen.program_studi, program_studi.nama as program_studi_str, dosen.nama,
				'jenis' as type, 
				count(DISTINCT jurnal.id) as jurnalnya, 
			    count(DISTINCT buku_ajar.id) as buku_ajarnya,
			    count(DISTINCT forum_ilmiah.id) as forum_ilmiahnya,
			    count(DISTINCT hki.id) as hkinya,
			    count(DISTINCT luaran_lain.id) as luaran_lainnya,
			    count(DISTINCT penyelenggara_forum_ilmiah.id) as penyelenggara_forum_ilmiahnya,
			    count(DISTINCT penelitian_hibah.id) as penelitian_hibahnya,
			    count(DISTINCT jurnal.id)+count(DISTINCT buku_ajar.id)+count(DISTINCT forum_ilmiah.id)+count(DISTINCT hki.id)+count(DISTINCT luaran_lain.id)+count(DISTINCT penyelenggara_forum_ilmiah.id)+count(DISTINCT penelitian_hibah.id) as total
			FROM dosen 
			LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
			LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
			LEFT JOIN jurnal ON jurnal.dosen=dosen.id AND jurnal.isdelete=0 AND jurnal.isvalidate=1 fc($dari,jurnal) fc($sampai,jurnal)
			LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1  fc($dari,buku_ajar) fc($sampai,buku_ajar)
			LEFT JOIN forum_ilmiah ON forum_ilmiah.dosen = dosen.id AND forum_ilmiah.isdelete=0 AND forum_ilmiah.isvalidate=1 fc($dari,forum_ilmiah) fc($sampai,forum_ilmiah)
			LEFT JOIN hki ON hki.dosen = dosen.id AND hki.isdelete=0 AND hki.isvalidate=1  fc($dari,hki) fc($sampai,hki)
			LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1 fc($dari,luaran_lain) fc($sampai,luaran_lain)
			LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen = dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1  fc($dari,penyelenggara_forum_ilmiah) fc($sampai,penyelenggara_forum_ilmiah)
			LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1  fc($dari,penelitian_hibah) fc($sampai,penelitian_hibah)
			WHERE dosen.fakultas<>0 $fakultasx $where
			GROUP BY dosen.fakultas $group
			ORDER BY total DESC
		) AS tab
	GROUP BY type
		";

		$allowedFunctions = array("fc");
		// echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
		$query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
		     function ($m) {
		          return $m[1]($m[2],$m[3]);
		     }, $query);
		$data = DB::select($query);
		// echo json_encode($data[0]); die;
		return $data;
	}

}
