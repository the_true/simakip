<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Penelitianprodi extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("r"=>"active","r8"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("batasan_dana");
        $this->load->model("Jenis_Penelitian");
        $this->load->model("Batch_lists");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Fakultas");
    }

	public function index(){
		// if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True) && $this->is_login()["isketuaprodi"]==1) return;
		$items = [];
		$jenis_penelitian = Jenis_Penelitian::where('isdelete', '=', '0')->get();
		$fakultas = Fakultas::where('isdelete','=','0')->get();

		$items = $this->datareport($items);

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
 		$items["tahun_kegiatan"] = $tahun_kegiatan->toArray();
 		$items["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$items["jenis_penelitian"] = $jenis_penelitian->toArray();
		$items["fakultas"] = $fakultas->toArray();
		$this->twig->display('report/penelitianprodi',array_merge($items,$this->menu));
	}

	private function datareport($item){
		// $cari = $this->input->get("cari");
		// if(!$cari) return $item;
		$this->load->model('Penelitian');
		$this->load->model('Dosen');
		$this->load->model('Jabatan_akademik');
		$this->load->model('Jabatan_fungsi');
		$this->load->model('program_studi');
		$this->load->model('Penelitian_anggota');
		$this->load->model('Batch');
		$this->load->model('tahun_kegiatan');
		$penelitian = Penelitian::where("isdelete","=","0")->whereIn('status',['4','12','3'])
								 ->with('anggota.dosen')
								 ->with('jenis_penelitian')
								 ->with('dosen.fakultas')
								 ->with('dosen.program_studi')
								 ->with('batch')
								 ->with('tahun_kegiatan');
		$penelitian = $this->filter($penelitian,$item);

		if($this->is_login()["isketuaprodi"]==1){
			$penelitian = $penelitian->whereHas('dosen',function($q){
				$q->where('program_studi','=',$this->is_login()['program_studi']['id']);
			});
		}
		// echo $penelitian->toSql(); die;
 		$info = $this->create_paging($penelitian);
 		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
 		// echo $penelitian->toJson(); die;
 		$item["items"] = $penelitian->toArray();
 		$item = array_merge($item,$info);
		return $item;
	}

	private function filter($item,&$data){
		// $item = $item->whereHas('dosen',function($q){
		// 	$q->where('isketuaprodi','=','1');
		// });

		$nama = $this->input->get('nama');
		if($nama) $item = $item->whereHas('dosen',function($q){$q->where('nama','LIKE','%'.$this->input->get('nama').'%');});

		$judul = $this->input->get('judul');
		if($judul) $item = $item->where('judul','LIKE','%'.$this->input->get('judul').'%');

		$jenis_penelitian = $this->input->get('jenis_penelitian');
		if($jenis_penelitian) $item = $item->where('jenis_penelitian','=',$this->input->get('jenis_penelitian'));

		$fakultas = $this->input->get('fakultas');
		if($fakultas) $item = $item->whereHas('dosen',function($q){ $q->where('fakultas','=',$this->input->get('fakultas'));});

		$fakultas = $this->input->get('program_studi');
		if($fakultas) $item = $item->whereHas('dosen',function($q){ $q->where('program_studi','=',$this->input->get('program_studi'));});

		$batch = $this->input->get('batch');
		if($batch){
 			$item = $item->whereHas('batch',function($qa) use ($batch) {
				$qa->where('batch_lists','=',$batch);
			});
 		}

		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
		if($tahun_kegiatan){
 			$item =$item->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

 		$status = $this->input->get('status');
 		if($status) $item = $item->where('status','=',$status);
 	// 	if($status=='1') $item = $item->whereHas('penelitian_laporan',function($q){
 	// 		$q->where('status','=',1);
 	// 	});
 	// 	else if($status=='2') $item = $item->whereHas('penelitian_laporan',function($q){
 	// 		$q->where('status','<>',1);
 	// 	});
		// else if($status=='3') $item = $item->whereDoesntHave('penelitian_laporan');

 		$orderby = $this->input->get('orderby');
 		$to = $this->input->get('to');
 		if($to=="") $to="DESC";
 		if($orderby){
 			$item->orderby($orderby,$to);
 		}else{
 			$item->orderby('id',$to);
 		}
 		$data["orderby"] = $orderby;
 		$data["to"] = $to;

		return $item;
	}

 	public function pdf(){
 		$items = [];
 		$data = $this->datareport($items);
 		$data = $data["items"];

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				// 'bottom' => $default_border,
 				// 'left' => $default_border,
 				// 'top' => $default_border,
 				// 'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_report_penelitianprodi.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$peneliti='C'.$urut;
			$fakultasprod='E'.$urut;
			$jenis_penelitian = 'F'.$urut;
			$judul = 'G'.$urut;
			$batch = 'H'.$urut;
			$status= 'I'.$urut;

			$anggota_string = "Ketua:\n".$data[$i]['dosen']['nama_lengkap'];
			if(count($data[$i]["anggota"])>0) $anggota_string.="\nAnggota:\n";
			for($x=0;$x<count($data[$i]["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}


			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($peneliti, $anggota_string)->mergeCells($peneliti.':D'.$urut)
			->setCellValueExplicit($fakultasprod, "Fakultas: ".$data[$i]['dosen']['fakultas']['nama']."\n".
										"Program Studi: ".$data[$i]['dosen']['program_studi']['nama'])
			->setCellValue($jenis_penelitian,$data[$i]['jenis_penelitian']['nama'])
			->setCellValue($judul, $data[$i]['judul'])
			->setCellValue($batch, $data[$i]['tahun_kegiatan']['tahun']."\n".$data[$i]['batch']['nama'])
			->setCellValue($status, $data[$i]['status_text']);

			$excel->getActiveSheet()->getStyle($peneliti)->getAlignment()->setWrapText(true);
			$excel->getActiveSheet()->getStyle($fakultasprod)->getAlignment()->setWrapText(true);
			$excel->getActiveSheet()->getStyle($batch)->getAlignment()->setWrapText(true);
			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$laststyle=$status;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		// for($col = 'A'; $col !== 'N'; $col++) {
		//     $excel->getActiveSheet()
		//         ->getColumnDimension($col)
		//         ->setAutoSize(true);
		// }
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_ReportPenelitianProdi_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
