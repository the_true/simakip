<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class MonevReview extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
	private $menu = array("mp"=>"active","me"=>"active","me2"=>"active");
    public function __construct() {
		$config = [
	 		"functions" => ["anchor","set_value","set_select","set_radio"],
	 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
        parent::__construct($config);
    	
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
        $this->load->model("Batch_lists");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
    	$this->load->model("Penelitian");
    	$this->load->model("Penelitian_anggota");
    	$this->load->model("Penelitian_review");

    	$this->load->model('Monev');
    	$this->load->model('Monev_borang');
    	$this->load->model('Monev_review');
    	$this->load->model('Monev_reviewer');
    	$this->load->model('Monev_nilai_luaran');
		$this->load->model('Monev_nilai_hasil');
    	$this->load->model('Bobot_luaran');
    	$this->load->model('Bobot_hasil');
    	$this->load->model('Bobot_kriteria');
    }	
	public function index()
	{			
		$data = [];
		$jenis_penelitian = Jenis_penelitian::where("isdelete","=","0")->get();
		// $jenis_penelitian->load("batch_penelitian");
		$data["jenis_penelitian"] = $jenis_penelitian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('monev',function($q){
								  	$q->where('reviewer','=','1');
								  })
										 ->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')->with('penelitian_review_inselected');;
		$penelitian = $this->filter($penelitian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
			$penelitian->where(function($q){
 				$q->whereHas('monev_reviewer',function($q){
			 		$q->where('dosen','=',$this->is_login()['id']);
			 	});
			});
	 	}
		$info = $this->create_paging($penelitian);
		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$penelitian->load('jenis_penelitian')->load('monev')->load('monev_reviewer')->load('monev_reviewer.dosen');
		$penelitian->load('tahun_kegiatan');

		$data["penelitian"] = $penelitian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);

		// echo json_encode($data); die;

		$this->twig->display("monev/review", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_penelitian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_penelitian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

 		$status = $this->input->get('status');
 		if($status==1){
 			$model->whereHas('monev',function($q){
 				$q->where('reviewer','=',1)->where('hasilreview','=',0);
 			});
 		}else if($status==2){
 			$model->whereHas('monev',function($q){
 				$q->where('reviewer','=',1)->where('hasilreview','=',1);
 			});
 		}


		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function review(){
		$data=[];
		$id = $this->uri->segment(4);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian')->load('jenis_penelitian.bobot_luaran_aktif');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_borang::where('penelitian','=',$id)->first();
 		if($monev) $penelitian["monev"] = $monev->toArray();

 		$step1 = Monev_review::where('penelitian','=',$id)->where('dosen','=', $this->is_login()['id'])->first();
 		// echo $step1;die;
 		if($step1){
 			$penelitian["step1"] = $step1->toArray();
 			$step2 = Monev_nilai_luaran::where('monev_review','=',$step1->id)->first();
	 		if($step2) $penelitian["step2"] = $step2->toArray();

	 		$step3 = Monev_nilai_hasil::where('monev_review','=',$step1->id)->first();
	 		if($step3) $penelitian["step3"] = $step3->toArray();
 		}

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("monev/review/review", $data);


	}

	public function results(){
		$data=[];
		$id = $this->uri->segment(4);
		$dosen = $this->uri->segment(6);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian')->load('jenis_penelitian.bobot_luaran_aktif');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_review::where('penelitian','=',$id)->where('dosen','=', $dosen)->first();
 		if($monev) $penelitian["monev"] = $monev->toArray();

 		$monev_luaran = Monev_nilai_luaran::where('penelitian','=',$id)->where('monev_review','=', $monev->id)->first();
 		if($monev_luaran) $penelitian["monev_nilai_luaran"] = $monev_luaran->toArray();

 		$monev_hasil = Monev_nilai_hasil::where('penelitian','=',$id)->where('monev_review','=', $monev->id)->first();
 		if($monev_luaran) $penelitian["monev_nilai_hasil"] = $monev_hasil->toArray();

 		// echo $monev_hasil->toJson(); die;

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("monev/hasil_review/index", $data);
	}

	public function resultsakhir(){
		$data=[];
		$id = $this->uri->segment(4);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian')->load('jenis_penelitian.bobot_luaran_aktif');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_review::where('penelitian','=',$id)->get();
 		if($monev) $penelitian["monevs"] = $monev->toArray();

 		$monev_luaran = Monev_nilai_luaran::where('penelitian','=',$id)->where('isselected','=', '1')->first();
 		if($monev_luaran) $penelitian["monev_nilai_luaran"] = $monev_luaran->toArray();

 		$monev_hasil = Monev_nilai_hasil::where('penelitian','=',$id)->where('isselected','=','1')->first();
 		if($monev_luaran) $penelitian["monev_nilai_hasil"] = $monev_hasil->toArray();

 		// echo $monev_hasil->toJson(); die;

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("monev/hasil_review/index_gabungan", $data);
	}

	public function step1()
	{			
		$content = json_encode($_POST);
		$penelitian = $this->input->post('penelitian');
		$dosen = $this->is_login()['id'];

		$item = Monev_review::where('penelitian','=',$penelitian)->where('dosen','=', $this->is_login()['id'])->first();
		if($item===null) $item = new Monev_review;
		$item->penelitian = $penelitian;
		$item->dosen = $dosen;
		$item->content = $content;

		$item->save();
		echo(json_encode($_POST));

	}

	public function step2(){
		$penelitian = $this->input->post('penelitian');
		$bobot_luaran = $this->input->post('bobot_luaran');
		$monev_review = Monev_review::where('penelitian','=',$penelitian)->where('dosen','=', $this->is_login()['id'])->first();
		$skor = $this->input->post('skors');
		$nilai = $this->input->post('nilai');
		$komentar = $this->input->post('komentar_penilai_step2');

		$item = Monev_nilai_luaran::where('monev_review','=',$monev_review->id)->first();
		if($item===null) $item = new Monev_nilai_luaran;
		$item->penelitian = $penelitian;
		$item->monev_review = $monev_review->id;
		$item->bobot_luaran = $bobot_luaran;
		$item->content = json_encode($_POST);
		$item->skor = json_encode($skor);
		$item->nilai = json_encode($nilai);
		$item->komentar = $komentar;
		$item->save();
		echo(json_encode($_POST));
	}

	public function step3(){
		$penelitian = $this->input->post('penelitian');
		$bobot_hasil = $this->input->post('bobot_hasil');
		$monev_review = Monev_review::where('penelitian','=',$penelitian)->where('dosen','=', $this->is_login()['id'])->first();
		$skor = $this->input->post('skors');
		$nilai = $this->input->post('nilai');
		$komentar = $this->input->post('komentar_penilai_step3');

		$item = Monev_nilai_hasil::where('monev_review','=',$monev_review->id)->first();
		if($item===null) $item = new Monev_nilai_hasil;
		$item->penelitian = $penelitian;
		$item->monev_review = $monev_review->id;
		$item->bobot_hasil = $bobot_hasil;
		$item->skor = json_encode($skor);
		$item->nilai = json_encode($nilai);
		$item->komentar = $komentar;
		$item->save();

		$reviewer = Monev_reviewer::where('penelitian','=',$penelitian)->where('dosen','=', $this->is_login()['id'])->first();
		$reviewer->status = 1;
		$reviewer->save();

		$itemx = Monev_review::where('penelitian','=',$penelitian)->where('dosen','=', $this->is_login()['id'])->first();
		if($item===null) $itemx = new Monev_review;
		$itemx->isvalid=1;
		$itemx->save();

		$count1 = Monev_nilai_hasil::where('penelitian','=',$penelitian)->count();
		$count2 = Monev_nilai_luaran::where('penelitian','=',$penelitian)->count();
 		if($count1<2 && $count2<2){
 			redirect('monev/monevreview');
		}

		$monev_nilai_hasil = Monev_nilai_hasil::where('penelitian','=',$penelitian)->get();
		$skor1 = $monev_nilai_hasil[0]->skor;
		$skor2 = $monev_nilai_hasil[1]->skor;
		$nilai1 = $monev_nilai_hasil[0]->nilai;
		$nilai2 = $monev_nilai_hasil[1]->nilai;
		$newskor = [];
		$newnilai = [];
		for($i=0;$i<count($skor1);$i++){
			$newskor[] = round((float) $skor1[$i]+(float) $skor2[$i],2);
			$newnilai[] = (int) $nilai1[$i]+(int) $nilai2[$i];
		}
		$item = Monev_nilai_hasil::where('monev_review','=',0)->where('penelitian','=',$penelitian)->first();
		if($item===null) $item = new Monev_nilai_hasil;
		$item->penelitian = $penelitian;
		$item->monev_review = 0;
		$item->bobot_hasil = $bobot_hasil;
		$item->skor = json_encode($newskor);
		$item->nilai = json_encode($newnilai);
		$item->komentar = $monev_nilai_hasil[0]->komentar."\n\n".$monev_nilai_hasil[1]->komentar;
		$item->isselected = 1;
		$item->save();



		$monev_nilai_luaran = Monev_nilai_luaran::where('penelitian','=',$penelitian)->get();
		$skor1 = $monev_nilai_luaran[0]->skor;
		$skor2 = $monev_nilai_luaran[1]->skor;
		$nilai1 = $monev_nilai_luaran[0]->nilai;
		$nilai2 = $monev_nilai_luaran[1]->nilai;
		$newskor = [];
		$newnilai = [];
		for($i=0;$i<count($skor1);$i++){
			$newskor[] = round((float) $skor1[$i]+(float) $skor2[$i],2);
			$newnilai[] = (int) $nilai1[$i]+(int) $nilai2[$i];
		}
		$item = Monev_nilai_luaran::where('monev_review','=',0)->where('penelitian','=',$penelitian)->first();
		if($item===null) $item = new Monev_nilai_luaran;
		$item->penelitian = $penelitian;
		$item->monev_review = 0;
		$item->bobot_luaran = $monev_nilai_luaran[0]->bobot_luaran;
		$item->content = json_encode($monev_nilai_luaran[0]->content);
		$item->skor = json_encode($newskor);
		$item->nilai = json_encode($newnilai);
		$item->isselected = 1;
		$item->komentar = $monev_nilai_luaran[0]->komentar."\n\n".$monev_nilai_luaran[1]->komentar;
		$item->save();



 		$data = Monev::where('penelitian','=',$this->input->post('penelitian'))->first();
		if($data==null) $data = new Monev;
		$data->hasilreview = 1;
		$data->save();

		$penelitian = $item->penelitian()->first();
		$data=["id"=>$penelitian->dosen()->first()->id,"nama"=>$penelitian->dosen()->first()->nama_lengkap,"surel"=>$penelitian->dosen()->first()->surel,
								 "judul"=>$penelitian->judul];
		celery()->PostTask('tasks.monev_penelitian_diterima', array(base_url(),$data));

		redirect('monev/monevreview');
	}

	private function getDataPDF(){
		$data=[];
		$id = $this->uri->segment(4);
		$dosen = $this->uri->segment(6);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian')->load('jenis_penelitian.bobot_luaran_aktif');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_review::where('penelitian','=',$id)->where('dosen','=', $dosen)->first();
 		if($monev) $penelitian["monev"] = $monev->toArray();

 		if($dosen){
	 		$monev_luaran = Monev_nilai_luaran::where('penelitian','=',$id)->where('monev_review','=', $monev->id)->first();
	 		if($monev_luaran) $penelitian["monev_nilai_luaran"] = $monev_luaran->toArray();

	 		$monev_hasil = Monev_nilai_hasil::where('penelitian','=',$id)->where('monev_review','=', $monev->id)->first();
	 		if($monev_luaran) $penelitian["monev_nilai_hasil"] = $monev_hasil->toArray();
 		}else{
 			$monev_luaran = Monev_nilai_luaran::where('penelitian','=',$id)->where('isselected','=', '1')->first();
	 		if($monev_luaran) $penelitian["monev_nilai_luaran"] = $monev_luaran->toArray();

	 		$monev_hasil = Monev_nilai_hasil::where('penelitian','=',$id)->where('isselected','=','1')->first();
	 		if($monev_luaran) $penelitian["monev_nilai_hasil"] = $monev_hasil->toArray();
 		}


 		// echo $monev_hasil->toJson(); die;

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		return $data;
	}


	public function instrumen_monev(){
		$hasil = $this->uri->segment(6);
		$this->load->helper('pdf_helper');
		$this->load->helper('url');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		// $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		// $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(0);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$data = $this->getDataPDF();
		// $this->load->view('penelitian/review/lembar_penilaian.php',$data);
		echo $this->twig->render('monev/pdf/instrumen_monev',$data);
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
	 	$obj_pdf->Output('monev_review.pdf','I');
 		// if($hasil=="hasil") $obj_pdf->Output('review_penelitian_'.$data['dosen']['nama'].'.pdf', 'I');
 		// else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	}

	public function luaran_penelitian(){
		$hasil = $this->uri->segment(6);
		$this->load->helper('pdf_helper');
		$this->load->helper('url');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		// $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		// $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(0);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$data = $this->getDataPDF();
		// $this->load->view('penelitian/review/lembar_penilaian.php',$data);
		echo $this->twig->render('monev/pdf/luaran_penelitian',$data);
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
	 	$obj_pdf->Output('luaran_penelitian.pdf','I');
 		// if($hasil=="hasil") $obj_pdf->Output('review_penelitian_'.$data['dosen']['nama'].'.pdf', 'I');
 		// else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	}

	public function proses_penelitian(){
		$hasil = $this->uri->segment(6);
		$this->load->helper('pdf_helper');
		$this->load->helper('url');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		// $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		// $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(0);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$data = $this->getDataPDF();
		// $this->load->view('penelitian/review/lembar_penilaian.php',$data);
		echo $this->twig->render('monev/pdf/proses_penelitian',$data);
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
	 	$obj_pdf->Output('proses_penelitian.pdf','I');
 		// if($hasil=="hasil") $obj_pdf->Output('review_penelitian_'.$data['dosen']['nama'].'.pdf', 'I');
 		// else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	}

}
