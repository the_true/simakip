<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class MonevBorang extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
	private $menu = array("me"=>"active","mp10"=>"active");
    public function __construct() {
		$config = [
	 		"functions" => ["anchor","set_value","set_select","set_radio"],
	 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
        parent::__construct($config);

        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Dosen");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
    	$this->load->model("Penelitian");
    	$this->load->model("Penelitian_anggota");
    	$this->load->model("Penelitian_review");

		$this->load->model("Penelitian_target_luaran");
		$this->load->model("Penelitian_target_luaran_wajib");
 		$this->load->model("Penelitian_target_luaran_tambahan");
		$this->load->model("Target_luaran");
		$this->load->model("Target_luaran_sub");
		$this->load->model("Target_luaran_status");

    	$this->load->model('Monev_review');
    	$this->load->model('Monev_borang');
		$this->load->model('monev_luaran');
		$this->load->model('monev');
    }
	public function index()
	{
		$data=[];
		$id = $this->uri->segment(3);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_borang::where('penelitian','=',$id)->where('dosen','=', $this->is_login()['id'])->first();
 		if($monev) $penelitian["monev"] = $monev->toArray();

 		// echo json_encode($monev->toArray()); die;

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("monev/borang/proses_penelitian", $data);
	}

	public function view(){
		$data=[];
		$id = $this->uri->segment(4);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_borang::where('penelitian','=',$id)->first();
 		if($monev) $penelitian["monev"] = $monev->toArray();

 		// echo json_encode($monev->toArray()); die;

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$this->twig->display("monev/borang_view/proses_penelitian", $data);
	}

	public function luaranview(){
		$penelitian = $this->uri->segment(4);
		$data = ["id"=>$penelitian];
		$data["items1"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','ilmiah')->get()->toArray();
		$data["items2"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','buku')->get()->toArray();
		$data["items3"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','forum')->get()->toArray();
		$data["items4"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','speaker')->get()->toArray();
		$data["items5"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','scientist')->get()->toArray();
		$data["items6"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','luaran')->get()->toArray();

		$data_penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
		->with('dosen.jabatan_fungsional')
		->with('dosen.program_studi')
		->with('anggota')->with('anggota.dosen')->with('anggota.dosen.program_studi')->with('anggota.dosen.jabatan_akademik')->find($penelitian);
		$data_penelitian->load('batch')->load('batch.batch_penelitian.tahun');
		$data_penelitian->load('luaran_wajib', 'luaran_wajib.target_luaran_sub', 'luaran_wajib.target_luaran_status');
		$data_penelitian->load('luaran_tambahan', 'luaran_tambahan.target_luaran_sub', 'luaran_tambahan.target_luaran_status');
		$data_penelitian->load('tahun_kegiatan')->load('target_luaran')->load('target_luarans.target');
		$data['penelitian'] = $data_penelitian->toArray();
		// echo json_encode($data["item1"]); die;
		$this->twig->display("monev/borang_view/luaran_kegiatan", $data);
	}

	public function luaran(){
		$penelitian = $this->uri->segment(4);
		$data = ["id"=>$penelitian];
		$data["items1"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','ilmiah')->get()->toArray();
		$data["items2"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','buku')->get()->toArray();
		$data["items3"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','forum')->get()->toArray();
		$data["items4"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','speaker')->get()->toArray();
		$data["items5"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','scientist')->get()->toArray();
		$data["items6"] = Monev_luaran::where('penelitian','=',$penelitian)->where('type','=','luaran')->get()->toArray();
		// echo json_encode($data["item1"]); die;
		$this->twig->display("monev/borang/luaran_kegiatan", $data);
	}

	public function uploadluaran(){
		$id = $this->uri->segment(4);
		$this->generate_folder('uploads/borangluaran');
		// echo json_encode($_FILES); die;
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/borangluaran'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		// echo $id; die;
		if($uploaded){
			$this->session->set_flashdata('success', 'Upload berkas berhasil!');
			$nomor = $this->input->post("nomor");
			$data = Monev_luaran::find($id);
			$data->berkas=$this->upload->data('file_name');
			$data->save();
			echo '{"file":"'.$this->upload->data('file_name').'","id":'.$id.'}';
			// celery()->PostTask('tasks.suratkontrak_submit', array(base_url(),$data->id));

			// $penelitian = $data->penelitian()->first();
			// $jenis_penelitian = $penelitian->jenis_penelitian()->first();
			// $first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
			// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengupload Surat Kontrak 1 pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'4','created_at'=>date('Y-m-d H:i:s')]);


		}else{
			echo $this->upload->display_errors();
		}
	}



	public function insertluaran(){
		$penelitian = $this->uri->segment(4);
		$content = json_encode($_POST);
		$type = $this->input->post('type');

		// echo $content; die;
		$item = new Monev_luaran;
		$item->penelitian = $penelitian;
		$item->content = $content;
		$item->type = $type;
		$item->save();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function editluaran(){
		$penelitian = $this->uri->segment(4);
		$content = json_encode($_POST);
		$id = $this->input->post('id');
		$item = Monev_luaran::find($id);
		$item->content = $content;
		$item->save();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function deleteluaran(){
		$id = $this->uri->segment(4);
		$content = json_encode($_POST);
		$item = Monev_luaran::find($id);
		$item->delete();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function insert()
	{
		$this->session->set_flashdata('success', 'Borang berhasi disimpan!');
		$content = urldecode(json_encode($_POST));
		$penelitian = $this->input->post('penelitian');
		$dosen = $this->is_login()['id'];

		$item = Monev_borang::where('penelitian','=',$penelitian)->where('dosen','=', $this->is_login()['id'])->first();
		if($item===null) $item = new Monev_borang;
		$item->penelitian = $penelitian;
		$item->dosen = $dosen;
		$item->content = $content;

		$item->save();
		// echo $content;


		$data = Monev::where('penelitian','=',$penelitian)->first();
		if($data==null) $data = new Monev;
		$data->penelitian = $penelitian;
		$data->borang = 1;
		$data->save();

		redirect($_SERVER['HTTP_REFERER']);
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
			 mkdir($folder_name,0777, true);
		}
	}

	public function downloadborang(){
		$data=[];
		$id = $this->uri->segment(4);
		$penelitian = Penelitian::with('dosen')->with('dosen.jabatan_akademik')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.fakultas')
										 ->with('dosen.program_studi')
										 ->with('anggota.dosen.fakultas')
										 ->with('anggota')
										 ->with('anggota.dosen')
										 ->with('anggota.dosen.program_studi')
										 ->find($id);
		$penelitian->load('batch')->load('penelitian_review_inselected');
		$penelitian->load('tahun_kegiatan')->load('jenis_penelitian');
		// echo $penelitian->toJson(); die;
		$penelitian = $penelitian->toArray();
 		$this->load->model("Batas_anggaran");
 		$anggaran = Batas_anggaran::select(array('batas','id'))->where("jenis_penelitian","=",$penelitian["jenis_penelitian"]["id"])->where("status","=","1")->where('isdelete','=','0')->first();

 		$monev = Monev_borang::where('penelitian','=',$id)->first();
 		if($monev) $penelitian["monev"] = $monev->toArray();

 		// echo json_encode($monev->toArray()); die;

		$data = array_merge($penelitian,["anggaran"=>$anggaran->toArray()],$this->menu);
		$isi = $this->twig->render("monev/borang_view/pdf/borangpdf", $data);
		$title = "Monev Borang Penelitian ".stringminimize($penelitian["judul"]).".pdf";
		$this->compose_pdf($isi,$title);
	}

	public function downloadluaranpenelitian(){
		$penelitian = $this->uri->segment(4);
		$data = ["id"=>$penelitian];
		$penelitian = Penelitian::find($penelitian)->toArray();
		$data["judul"] = $penelitian["judul"];
		$data["items1"] = Monev_luaran::where('penelitian','=',$penelitian["id"])->where('type','=','ilmiah')->get()->toArray();
		$data["items2"] = Monev_luaran::where('penelitian','=',$penelitian["id"])->where('type','=','buku')->get()->toArray();
		$data["items3"] = Monev_luaran::where('penelitian','=',$penelitian["id"])->where('type','=','forum')->get()->toArray();
		$data["items4"] = Monev_luaran::where('penelitian','=',$penelitian["id"])->where('type','=','speaker')->get()->toArray();
		$data["items5"] = Monev_luaran::where('penelitian','=',$penelitian["id"])->where('type','=','scientist')->get()->toArray();
		$data["items6"] = Monev_luaran::where('penelitian','=',$penelitian["id"])->where('type','=','luaran')->get()->toArray();
		// echo json_encode($data["item1"]); die;
		$isi = $this->twig->render("monev/borang_view/pdf/luaranpdf", $data);
		$title = "Monev Luaran Penelitian ".stringminimize($penelitian["judul"]).".pdf";
		$this->compose_pdf($isi,$title);
	}



	private function compose_pdf($isi,$title){
	    $this->load->helper('pdf_helper');
	    $this->load->helper('url');
	    tcpdf();
	    $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	    $obj_pdf->SetCreator(PDF_CREATOR);
	    // $title = "PDF Report";
	    $obj_pdf->SetTitle($title);
	    // $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
	    // $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	    // $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	    $obj_pdf->SetDefaultMonospacedFont('helvetica');
	    $obj_pdf->SetHeaderMargin(0);
	    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
	    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	    $obj_pdf->SetFont('helvetica', '', 9);
	    $obj_pdf->setFontSubsetting(false);
	    $obj_pdf->setListIndentWidth(1);
	    $obj_pdf->AddPage();


	    ob_start();
	    echo $isi;
	    $content = ob_get_contents();
	    ob_end_clean();
	    $obj_pdf->writeHTML($content, true, false, true, false, '');
	    $obj_pdf->Output($title,'I');
	    echo $title;
	    // if($hasil=="hasil") $obj_pdf->Output('review_penelitian_'.$data['dosen']['nama'].'.pdf', 'I');
	    // else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
	    return $obj_pdf;
	}

}
