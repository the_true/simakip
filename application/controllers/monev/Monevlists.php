<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class MonevLists extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
	private $menu = array("mp"=>"active","me"=>"active","me1"=>"active");
    public function __construct() {
		$config = [
	 		"functions" => ["anchor","set_value","set_select"],
	 		"functions_safe" => ["form_open","form_open_multipart"],
 		];
        parent::__construct($config);
    	
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Jenis_penelitian");
        $this->load->model("Batch_penelitian");
    	$this->load->model("Batch");
    	$this->load->model("Batch_lists");
    	$this->load->model("Dosen");
		$this->load->model("Mahasiswa");
    	$this->load->model("Jabatan_akademik");
    	$this->load->model("Jabatan_fungsi");
    	$this->load->model("Program_studi");
    	$this->load->model("Fakultas");
    	$this->load->model("Penelitian");
    	$this->load->model("Penelitian_anggota");
		$this->load->model("Penelitian_anggotamhs");
    	$this->load->model("Penelitian_review");
    	$this->load->model("surat_kontrak");

    	$this->load->model('Monev');
    	$this->load->model('Monev_review');
    	$this->load->model('Monev_reviewer');
    	$this->load->model('Monev_nilai_luaran');
		$this->load->model('Monev_nilai_hasil');
    	$this->load->model('Bobot_luaran');
    	$this->load->model('Bobot_hasil');
    	$this->load->model('Bobot_kriteria');
    }	
	public function index()
	{			
		$data = [];
		$jenis_penelitian = Jenis_penelitian::where("isdelete","=","0")->get();
		// $jenis_penelitian->load("batch_penelitian");
		$data["jenis_penelitian"] = $jenis_penelitian->toArray();

		$tahun_kegiatan = Tahun_kegiatan::where("isdelete","=","0")->orderBy('tahun','desc')->get();
		$data["tahun_kegiatan"] = $tahun_kegiatan->toArray();

		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('penelitian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi');

		$penelitian = $this->filter($penelitian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
 			$penelitian->where(function($q){
 				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
 					$q->where('anggota','=',$this->is_login()['id']);
 				});
 			});
		 }
 		
		$info = $this->create_paging($penelitian);
		$penelitian = $penelitian->take($info["limit"])->skip($info["skip"])->get();
		$penelitian->load('anggotamhs')->load('anggotamhs.mahasiswa')->load('anggota')->load('anggota.dosen')->load('batch')->load('batch.batch_penelitian.tahun');
		$penelitian->load('jenis_penelitian')->load('monev')->load('monev_reviewer')->load('monev_reviewer.dosen');
		$penelitian->load('tahun_kegiatan');

		$data["penelitian"] = $penelitian->toArray();
		$data["batch_lists"] = Batch_lists::penelitian()->where("isdelete","=","0")->orderBy("nama","asc")->get()->toArray();
		$data = array_merge($data,$info,$this->menu);
		// echo "<pre>"; print_r($data); echo "</pre>"; 
		// die;
		$this->twig->display("monev/index", $data);
	}

	public function filter($model,&$data){
		$dosen = $this->input->get('dosen');
		$judul = $this->input->get('judul');
		$jp = $this->input->get('jenis_penelitian');
		if($dosen){
			$model->whereHas('dosen',function($q){
				$q->where("nama","LIKE","%".$this->input->get('dosen')."%");
			});
		}
		if($judul){
			$model->where('judul','LIKE','%'.$judul.'%');
		}
		if($jp){
			$model->where('jenis_penelitian','=',$jp);
		}
 		$batch = $this->input->get('batch');
 		if($batch){
 			$model->whereHas('batch',function($q) use ($batch) {
 				$q->where('batch_lists','=',$batch);
 			});
 		}

 		$tahun_kegiatan = $this->input->get('tahun_kegiatan');
 		if($tahun_kegiatan){
 			$model->where('tahun_kegiatan','=',$tahun_kegiatan);
 		}

 		$status = $this->input->get('status');
 		if($status==1){
 			$model->whereHas('monev',function($q){
 				$q->where('berkas','LIKE','%pdf');
 			});
 		}else if($status==2){
 			$model->whereHas('monev',function($q){
 				$q->where('borang','=',0);
 			});
 		}else if($status==3){
 			$model->whereHas('monev',function($q){
 				$q->where('reviewer','=',0);
 			});
 		}else if($status==4){
 			$model->whereHas('monev',function($q){
 				$q->where('reviewer','=',1);
 			});
 		}

		$orderby = $this->input->get('orderby');
		$to = $this->input->get('to');
		if($to=="") $to="DESC";
		if($orderby){
			$model->orderby($orderby,$to);
		}else{
			$model->orderby('id',$to);
		}
		$data["orderby"] = $orderby;
		$data["to"] = $to;
		return $model;
	}

	public function reviewer(){
 		$data = [];
 		$data["penelitian"] = $this->uri->segment(4);
 		$dosen = Dosen::where("isdelete","=","0")->where(function($q){
			$q->where("isreviewer","=","1")->orWhere("isreviewer","=","3");
        })->get();
 		$dosen = $dosen->toArray();
 		$item = Monev_reviewer::where("penelitian","=",$data['penelitian'])->with('dosen')
 		->with('dosen.program_studi')
 		->with('dosen.jabatan_akademik')
 		->get();
 		$data["reviewer"] = $item->toArray();
 		$data = array_merge($data,$this->menu,["dosen"=>$dosen]);
 		// echo $data['penelitian']; die;
 		$this->twig->display("monev/reviewer/reviewer", $data);
 	}

 	public function add_reviewer(){
 		$dosen = $this->input->post('dosen');
 		$penelitian = $this->input->post('penelitian');
 		$item = new Monev_reviewer;
 		$item->dosen = $dosen;
 		$item->penelitian = $penelitian;
 		$item->save();

 		$count = Monev_reviewer::where('penelitian','=',$penelitian)->count();
 		if($count>=2){
	 		$data = Monev::where('penelitian','=',$this->input->post('penelitian'))->first();
			if($data==null) $data = new Monev;
			$data->reviewer = 1;
			$data->save();
		}


		// echo $item->penelitian()->first()->toJson(); die();

		$jenis_penelitian = $item->penelitian()->first()->jenis_penelitian()->first();

 		$item->load('dosen');
 		$item->load('penelitian.dosen');
 		$item = $item->toArray();
 		$data = [];

		$first_four_title = implode(' ', array_slice(explode(' ', $item["penelitian"]["judul"]), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' memilih '.$item["dosen"]["nama_lengkap"].' sebagai monev reviewer Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);

 		$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
							"judul"=>$item["penelitian"]["judul"],"pengusul"=>$item["penelitian"]["dosen"]["nama_lengkap"]];
 		// echo json_encode($data) ; die;
 		celery()->PostTask('tasks.monevpenelitian_reviewers',array(base_url(),$data));

 		redirect('monev/monevlists/reviewer/'.$item["penelitian"]["id"]);
 	}

	public function konfirm_reviewer(){
		$penelitian=$this->input->get('penelitian');
		$item = Monev_reviewer::where('penelitian','=',$penelitian)->get();
		$item->load('dosen');
		$item->load('penelitian.dosen');
		// echo $item->toJson();die;
		$items = $item->toArray();
		$data = [];
		// foreach($items as $item){
		// 	$data[]=["id"=>$item["dosen"]["id"],"reviewer"=>$item["dosen"]["nama_lengkap"],"surel"=>$item["dosen"]["surel"],
		// 					"judul"=>$item["penelitian"]["judul"],"pengusul"=>$item["penelitian"]["dosen"]["nama_lengkap"],
		// 					"deadline"=>$item["deadline"]];
		// }
		// celery()->PostTask('tasks.monevpenelitian_reviewers',array(base_url(),$data));
		redirect('monev/monevlists/');
	}

 	public function verify_reviewer(){
 		$penelitian = $this->input->get('penelitian');
 		$item = Monev_reviewer::where('penelitian','=',$penelitian)->count();
 		if($item==2){
 			$this->output->set_header('HTTP/1.0 500 Reviewer sudah penuh');
 			return;
 		}

 		$item = Monev_reviewer::where('penelitian','=',$penelitian)
 		->whereHas('dosen',function($q){
 			$q->where('nidn','=',$this->input->get('nidn'));
 		})->count();
 		if($item>0){
 			$this->output->set_header('HTTP/1.0 500 Reviewer sudah ada');
 			return;
 		}

 		echo "berhasil";
 	}

 	public function delete_reviewer(){
 		$id = $this->uri->segment(4);
 		$item = Monev_reviewer::find($id);
 		$penelitian = $item->penelitian;


 		$data = Monev::where('penelitian','=',$penelitian)->first();
		if($data==null) $data = new Monev;
		$data->reviewer = 0;
		$data->save();
 	// 	$penelitian = $item->penelitian()->first();
 	// 	$jenis_penelitian = $penelitian->jenis_penelitian()->first();
 	// 	$dosen = $item->dosen()->first();
		// $first_four_title = implode(' ', array_slice(explode(' ', $penelitian->judul), 0, 4));
		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus '.$dosen->nama.' sebagai reviewer Penelitian pada jenis penelitian '.$jenis_penelitian->nama.' dengan nama judul/kegiatan '.$first_four_title,'type'=>'3','created_at'=>date('Y-m-d H:i:s')]);
 		$item->delete();
 		redirect('monev/monevlists/reviewer/'.$penelitian);
 	}

 	public function upload_dokumen(){
		$id = $this->uri->segment(4);
		$this->generate_folder('uploads/borang');
		// echo json_encode($_FILES); die;
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/borang'),
			'allowed_types' => 'pdf',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		// echo $id; die;
		if($uploaded){
			$this->session->set_flashdata('success', 'Upload berkas berhasil!');
			$nomor = $this->input->post("nomor");
			$data = Monev::where('penelitian','=',$this->input->post('penelitian'))->first();
			if($data==null) $data = new Monev;
			$data->penelitian = $this->input->post('penelitian');
			$data->berkas=$this->upload->data('file_name');
			$data->save();
			// echo '{"file":"'.$this->upload->data('file_name').'","id":'.$id.'}';
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			echo $this->upload->display_errors();
		}
 	}

 	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
			 mkdir($folder_name,0777, true);
		}
	}

	private function dataExcel(){
		$penelitian = Penelitian::where("isdelete","=","0")->where('status','=','4')
								  ->whereHas('surat_kontrak',function($q){
								  	$q->where('isvalidate','=','1');
								  })
										 ->with('dosen.jabatan_akademik')->with('penelitian_review_inselected')
										 ->with('dosen.jabatan_fungsional')
										 ->with('dosen.program_studi')
										 ->with('dosen.fakultas');
		$penelitian = $this->filter($penelitian,$data);
		// $akses = $this->is_login()['akses']['nama'];
		if($this->is_login()['akses']['nama']=='Dosen'){
 			$penelitian->where(function($q){
 				$q->where('dosen','=',$this->is_login()['id'])->orWhereHas('anggota', function($q){
 					$q->where('anggota','=',$this->is_login()['id']);
 				});
 			});
 		}
 		
		$info = $this->create_paging($penelitian);
		$penelitian = $penelitian->get();
		$penelitian->load('anggota')->load('anggota.dosen')->load('batch');
		$penelitian->load('jenis_penelitian')->load('monev')->load('monev_reviewer')->load('monev_reviewer.dosen');
		$penelitian->load('tahun_kegiatan');
		return $penelitian->toArray();
	}

 	public function download(){
 		$data = $this->dataExcel();

 		$this->load->library('libexcel');

 		$default_border = array(
 			'style' => PHPExcel_Style_Border::BORDER_THIN,
 			'color' => array('rgb'=>'1006A3')
 			);
 		$style_header = array(
 			'borders' => array(
 				'bottom' => $default_border,
 				'left' => $default_border,
 				'top' => $default_border,
 				'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'E1E0F7'),
 				),
 			'font' => array(
 				'bold' => true,
 				'size' => 16,
 				)
 			);
 		$style_content = array(
 			'borders' => array(
 				'allborders' => $default_border,
 				// 'bottom' => $default_border,
 				// 'left' => $default_border,
 				// 'top' => $default_border,
 				// 'right' => $default_border,
 				),
 			'fill' => array(
 				'type' => PHPExcel_Style_Fill::FILL_SOLID,
 				'color' => array('rgb'=>'eeeeee'),
 				),
 			'font' => array(
 				'size' => 12,
 				)
 			);
 		$excel = PHPExcel_IOFactory::load(FCPATH."assets/template/template_monev.xlsx");
 		$excel->getProperties()->setCreator("Simakip");
 		$excel->setActiveSheetIndex(0);
		$firststyle='B11';
		$laststyle='B11';
		for($i=0;$i<count($data);$i++)
		{
			$urut=$i+11;
			$num='B'.$urut;
			$judul_penelitian='C'.$urut;
			$nama_peneliti='E'.$urut;
			$anggota = 'F'.$urut;
			$jenis_penelitian = 'G'.$urut;
			$batchtahun = 'H'.$urut;
			$fakultas = 'I'.$urut;
			$reviewer1 = 'J'.$urut;
			$reviewer2 = 'K'.$urut;

			$anggota_string = "";
			for($x=0;$x<count($data[$i]["anggota"]);$x++){
				$anggota_string.="- ".$data[$i]["anggota"][$x]["dosen"]["nama_lengkap"]."\n";
			}

			$excel->setActiveSheetIndex(0)
			->setCellValue($num, $i+1)
			->setCellValue($judul_penelitian, $data[$i]['judul'])->mergeCells($judul_penelitian.':D'.$urut)
			->setCellValue($nama_peneliti, $data[$i]['dosen']['nama_lengkap'])
			->setCellValue($anggota,$anggota_string)
			->setCellValue($jenis_penelitian, $data[$i]['jenis_penelitian']["nama"])
			->setCellValue($batchtahun, $data[$i]['batch']["nama"]."-".$data[$i]["tahun_kegiatan"]["tahun"])
			->setCellValue($fakultas, $data[$i]['dosen']['fakultas']["nama"]."/".$data[$i]['dosen']['program_studi']["nama"]);

			if(count($data[$i]['monev_reviewer'])==2){
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer1, $data[$i]['monev_reviewer'][0]["dosen"]["nama_lengkap"]);
				$excel->setActiveSheetIndex(0)->setCellValue($reviewer2, $data[$i]['monev_reviewer'][1]["dosen"]["nama_lengkap"]);
			}

			$excel->getActiveSheet()->getRowDimension($i+11)->setRowHeight(-1);
			$excel->setActiveSheetIndex(0)->getStyle($anggota)->getAlignment()->setWrapText(true);
			$laststyle=$reviewer2;
		}
		$excel->getActiveSheet()->getStyle($firststyle.':'.$laststyle)->applyFromArray( $style_content ); // give style to header
		for($col = 'A'; $col !== 'N'; $col++) {
		    $excel->getActiveSheet()
		        ->getColumnDimension($col)
		        ->setAutoSize(true);
		}
		$excel->getActiveSheet()
	    ->getStyle($firststyle.':'.$laststyle)
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		// Rename worksheet
		$excel->getActiveSheet()->setTitle('Penelitian');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$excel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->is_login()['nidn'].'_MonevPenelitian_'.Date('dmY').'.xls"'); // file name of excel
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save('php://output');
	}
}
