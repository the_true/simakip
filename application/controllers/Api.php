<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
use Illuminate\Database\Capsule\Manager as DB;
class Api extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/

    public function __construct() {
    	$config = [];
        parent::__construct($config,false);
        $this->load->model('Dosen');
        $this->load->model('Tahun_kegiatan');
        $this->load->model('Subject_penelitian');
		$this->load->model('Subject');
        $this->load->model('Jabatan_akademik');
        $this->load->model('Bidangkeahlian');
        $this->load->model('Pendidikan');
        $this->load->model('Subbidangkeahlian');
        $this->load->model('Jabatan_fungsi');
        $this->load->model('Fakultas');
        $this->load->model('Program_studi');
		$this->load->model('Penelitian_dikti');
		$this->load->model('penelitian_dikti_anggota');
    }	 
	public function jurnal($iddosen = null){
		$this->load->model('luaran_penelitian/Jurnal');
		$item = Jurnal::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$item = $this->filter($item);
		$info = null;
		if ($iddosen){
			$item = $item->where('dosen','=',$iddosen)->get();
		}else{
			$info = $this->create_paging($item);
			$item = $item->take($info["limit"])->skip($info["skip"])->get();
		}

		$item->load('dosen')->load('personil')->load('tahun_kegiatan');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function bukuajar($iddosen = null){
		$this->load->model('luaran_penelitian/Buku_ajar');
		$item = Buku_ajar::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$item = $this->filter($item);
		$info = null;
		if ($iddosen){
			$item = $item->where('dosen','=',$iddosen)->get();
		}else{
			$info = $this->create_paging($item);
			$item = $item->take($info["limit"])->skip($info["skip"])->get();
		}

		$item->load('dosen')->load('tahun_kegiatan');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function forumilmiah($iddosen = null){
		$this->load->model('luaran_penelitian/Forum_ilmiah');
		$item = Forum_ilmiah::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$item = $this->filter($item);
		$info = null; 
		if ($iddosen){
			$item = $item->where('dosen','=',$iddosen)->get();
		}else{
			$info = $this->create_paging($item); 
			$item = $item->take($info["limit"])->skip($info["skip"])->get();
		}

		$item->load('dosen')->load('tahun_kegiatan');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function hki($iddosen = null){
		$this->load->model('luaran_penelitian/Hki');
		$this->load->model('Jenis_hki');
		$item = Hki::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$item = $this->filter($item);
		$info = null;
		if ($iddosen){
			$item = $item->where('dosen','=',$iddosen)->get();
		}else{
			$info = $this->create_paging($item);
			$item = $item->take($info["limit"])->skip($info["skip"])->get();
		}

		$item->load('dosen')->load('tahun_kegiatan')->load('jenis_hki');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function luaranlain($iddosen =null){
		$this->load->model('luaran_penelitian/Luaran_lain');
		$this->load->model('Jenis_luaran');
		$item = Luaran_lain::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$item = $this->filter($item);
		$info = $this->create_paging($item);
		if ($iddosen){
			$item->where('dosen','=',$iddosen);
		}
		$item = $item->take($info["limit"])->skip($info["skip"])->get();

		$item->load('dosen')->load('tahun_kegiatan')->load('jenis_luaran');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function penelitianmandiri($iddosen = null,$sumber = null){
		$this->load->model('luaran_penelitian/Penelitian_hibah');
		$item = Penelitian_hibah::where('isdelete', '=', '0')->where('isvalidate','=','1');
		$item = $this->filter($item,'tahun');
		$info = $this->create_paging($item); 
		if ($iddosen){
			$item->where('dosen','=',$iddosen);
		}
		if ($sumber){
			$item->where('sumber','=',$sumber);
		}
		$item = $item->take($info["limit"])->skip($info["skip"])->get();

		$item->load('dosen')->load('tahun');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function penelitianinternal($iddosen=null){
		$this->load->model('Penelitian');
		$this->load->model('Penelitian_anggota');
		$this->load->model('Penelitian_laporan');
		$item = Penelitian::with('dosen')
 		->with('dosen_menyetujui')
 		->with('dosen_mengetahui')
 		->with('anggota')->with('anggota.dosen')->where('isdelete', '=', '0')->where('status','=','4')->whereHas('penelitian_laporan',function($q){
	      $q->where('status','=','1');
		 });
		if ($iddosen){
			$item->where('dosen','=',$iddosen);
			$isvalid = $this->input->get("isvalid");
			if ($isvalid=='0' || $isvalid=='1'){ //echo $isvalid; die('asds');
				$item->where('isvalid','=',$isvalid);
			}
		} 
		
		$item = $this->filter($item,'tahun');
		$info = $this->create_paging($item);
		
		$item = $item->take($info["limit"])->skip($info["skip"])->get();

		$item->load('dosen')->load('tahun_kegiatan')->load('penelitian_laporan');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	} 
	public function penelitianexternal($sumberdana=null){ 
		$item = Penelitian_dikti::where('isdelete', '=','0')->with('penelitian_dikti_anggota');

		if ($this->input->get("iddosen")){
			$item->whereHas('penelitian_dikti_anggota',function($q){
				$q->where('dosen','=',$this->input->get("iddosen"));
			  })->with('penelitian_dikti_anggota.dosen');
		} 
		 
		if ($sumberdana){
			$item->where('pendanaan_penelitian','=',$sumberdana);
		}
		
		$item = $this->filter($item,'tahun');
		$info = $this->create_paging($item);
		
		$item = $item->take($info["limit"])->skip($info["skip"])->get();
		$item = $item->toArray(); 
		
		$data = array("items"=>$item);

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function penelitian($iddosen=null,$isvalid=null){
		$this->load->model('Penelitian');
		$this->load->model('Penelitian_anggota');
		$this->load->model('Penelitian_laporan');
		$item = Penelitian::with('dosen')
 		->with('dosen_menyetujui')
 		->with('dosen_mengetahui')
 		->with('anggota')->with('anggota.dosen')->where('isdelete', '=', '0')->where('status','=','4')->whereHas('penelitian_laporan',function($q){
	      $q->where('status','=','1');
		 });
		if ($iddosen){
			$item->where('dosen','=',$iddosen);
		}
		$item = $this->filter($item,'tahun');
		$info = $this->create_paging($item);
		
		$item = $item->take($info["limit"])->skip($info["skip"])->get();

		$item->load('dosen')->load('tahun_kegiatan')->load('penelitian_laporan');
		$item->load('subject_penelitian')->load('subject_penelitian.subject')->load('subject_penelitian.subject.parentnya');
		$data = array("items"=>$item->toArray());

		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function tahun_kegiatan(){
		$this->load->model('Tahun_kegiatan');
		$item = Tahun_kegiatan::where('isdelete','=','0')->orderBy('tahun','DESC')->get();
		// $item = $Item->toArray();
		echo $item->toJson();
	}

	public function billboard(){
		$this->load->model('Billboard');
		$item = Billboard::where('status','=','1')->get();
		// $item = $Item->toArray();
		echo $item->toJson();
	}

	public function subject(){
		$this->load->model('subject');
		$this->load->model('subject_penelitian');
		$item = Subject::where('isdelete','=','0')->whereNull('parent')->orderBy('nama','ASC')->get();
		$item->load('child');
		echo $item->toJson();
	}

	public function subjectlistsorder(){
		$this->load->model('subject');
		$this->load->model('subject_penelitian');
		$item = Subject_penelitian::select(new raw('count(subject) as total'),'subject')->groupBy('subject_penelitian.subject')->orderBy('total','DESC')->get();
		$item->load('subject');
		echo $item->toJson();
	}

	public function subjectlists(){
		$subject_id = $this->input->get('subject');
		$jurnal = DB::table('jurnal')->select('jurnal.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','jurnal.judul','tahun_kegiatan.tahun','created_at',new raw("'Publikasi Jurnal' as type"),new raw("'jurnal' as url"))->leftJoin('tahun_kegiatan','jurnal.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','jurnal.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('jurnal.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('1'));
		})->where('jurnal.isdelete', '=', '0')->where('isvalidate','=','1');
		$jurnal = $this->filter($jurnal,null,'jurnal.','1');

		$bukuajar = DB::table('buku_ajar')->select('buku_ajar.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','buku_ajar.judul','tahun_kegiatan.tahun','created_at',new raw("'Buku Ajar' as type"),new raw("'bukuajar' as url"))->leftJoin('tahun_kegiatan','buku_ajar.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','buku_ajar.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('buku_ajar.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('2'));
		})->where('buku_ajar.isdelete', '=', '0')->where('isvalidate','=','1');
		$bukuajar = $this->filter($bukuajar,null,'buku_ajar.','2');

		$forumilmiah = DB::table('forum_ilmiah')->select('forum_ilmiah.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','forum_ilmiah.judul','tahun_kegiatan.tahun','created_at',new raw("'Penyelenggara Forum Ilmiah' as type"),new raw("'forumilmiah' as url"))->leftJoin('tahun_kegiatan','forum_ilmiah.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','forum_ilmiah.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('forum_ilmiah.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('3'));
		})->where('forum_ilmiah.isdelete', '=', '0')->where('isvalidate','=','1');
		$forumilmiah = $this->filter($forumilmiah,null,'forum_ilmiah.','3');

		$hki = DB::table('hki')->select('hki.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','hki.judul','tahun_kegiatan.tahun','created_at',new raw("'Hak Kekayaan Intelektual' as type"),new raw("'hki' as url"))->leftJoin('tahun_kegiatan','hki.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','hki.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('hki.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('4'));
		})->where('hki.isdelete', '=', '0')->where('isvalidate','=','1');
		$hki = $this->filter($hki,null,'hki.','4');

		$luaranlain = DB::table('luaran_lain')->select('luaran_lain.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','luaran_lain.judul','tahun_kegiatan.tahun','created_at',new raw("'Luaran Lain' as type"),new raw("'luaranlain' as url"))->leftJoin('tahun_kegiatan','luaran_lain.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','luaran_lain.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('luaran_lain.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('5'));
		})->where('luaran_lain.isdelete', '=', '0')->where('isvalidate','=','1');
		$luaranlain = $this->filter($luaranlain,null,'luaran_lain.','5');

		$penelitianmandiri = DB::table('penelitian_hibah')->select('penelitian_hibah.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','penelitian_hibah.judul','tahun_kegiatan.tahun','created_at',new raw("'Penelitian Mandiri' as type"),new raw("'penelitianmandiri' as url"))->leftJoin('tahun_kegiatan','penelitian_hibah.tahun','=','tahun_kegiatan.id')->leftJoin('dosen','penelitian_hibah.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('penelitian_hibah.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('6'));
		})->where('penelitian_hibah.isdelete', '=', '0')->where('isvalidate','=','1');
		$penelitianmandiri = $this->filter($penelitianmandiri,'tahun_kegiatan','penelitian_hibah.','6');

		$penelitianinternal = DB::table('penelitian')->select('penelitian.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','penelitian.judul','tahun_kegiatan.tahun','created_at',new raw("'Penelitian Internal' as type"),new raw("'penelitianinternal' as url"))->leftJoin('tahun_kegiatan','penelitian.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','penelitian.dosen','=','dosen.id')->leftJoin('subject_penelitian',function($join){
			$join->on('penelitian.id','=','subject_penelitian.data')->on('subject_penelitian.type','=',new raw('7'));
		})->where('penelitian.isdelete', '=', '0')->where('status','=','4');
		$penelitianinternal = $this->filter($penelitianinternal,null,'penelitian.','7');

		$results = $jurnal->union($bukuajar)->union($forumilmiah)->union($hki)->union($luaranlain)->union($penelitianinternal)->union($penelitianmandiri)->orderBy('created_at','desc')->get();

		$default_view = 10;
		$page = 0;

		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($view_get)) $default_view = $view_get;

		$default_skip = $page*$default_view;
		$count  = count($results);
		$total_pages = $count>0? ceil($count/$default_view):1;
		$info = ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages,"total"=>$count];

		$data = [];
		$data["items"]=array_slice($results,$info["skip"],$info["limit"],true);
		$data["paging"] = $info;
		echo json_encode($data);
	}

	public function detailpeneliti($idpeneliti){
		$user = Dosen::find($idpeneliti);
		$user->load("program_studi");
		$user->load("fakultas");
		$user->load("jabatan_fungsional");
		$user->load("jabatan_akademik");
		// $user->load("bidangkeahlian");
		$user->load("subbidangkeahlian"); 
		$user=$user->toArray();
		$bk = Bidangkeahlian::where('id','=',$user['subbidangkeahlian']['bidangkeahlian'])->get();
		$user['bidangkeahlian'] = $bk->toArray();
		$pendidikan = Pendidikan::where('isdelete','=','0')->where('status','=','Verified')->where('dosen','=',$idpeneliti)->get(); 
		$user['pendidikan'] = $pendidikan->toArray();
		$this->load->helper('report');
		$user["point_dosen"] = getPointDosen($user["id"]);
		$user['base_url'] = base_url();
		echo json_encode($user);
		// $this->twig->display('/pengguna/profile',$user);
	} 
	public function dosen(){
		$this->load->model('luaran_penelitian/Jurnal');$this->load->model('luaran_penelitian/Forum_ilmiah');$this->load->model('luaran_penelitian/Luaran_lain');$this->load->model('luaran_penelitian/Hki');
		$this->load->model('luaran_penelitian/Buku_ajar');$this->load->model('luaran_penelitian/Penelitian_hibah');$this->load->model('penelitian');
		$item = Dosen::select('id','nidn','nama','surel','jabatan_akademik','subbidangkeahlian','telp','hp','gelar_depan','gelar_belakang','photo')->where('isdelete','=','0')->
		where(function($q){$q->has('jurnal')->orHas('buku_ajar')->orHas('forum_ilmiah')->orHas('luaran_lain')->orHas('hki')->orHas('penelitian_hibah')->orHas('penelitian');})->orderBy('nama','ASC');
		
		$name = $this->input->get("name");
		$sub_bk = $this->input->get("sub_bk");
		if($name){ 
			$item->where('nama','like','%'.$name.'%');
		}
		if($sub_bk){  
			$item->where('jabatan_akademik','=',$sub_bk);
		}
		$info = $this->create_paging($item);
		$item = $item->take($info["limit"])->skip($info["skip"])->get(); 
		$item->load("jabatan_akademik");
		$item->load("subbidangkeahlian");
		
		
		$data = array("items"=>$item->toArray(),'base_url'=> base_url());
		$data['paging'] = $info;
		echo json_encode($data);
	}

	public function subbidangkeahlian(){  
		$item = Subbidangkeahlian::where('isdelete','=','0')->orderBy('subbidangkeahlian','ASC')->get();
		$item->load('bidangkeahlian');
		echo $item->toJson(); 
	}
	public function Jabatan_akademik(){  
		$item = Jabatan_akademik::where('isdelete','=','0')->orderBy('nama','ASC')->get(); 
		echo $item->toJson(); 
	}

	public function filter($item,$tahun_kegiatan='tahun_kegiatan',$table_name='',$type='1'){
		$id = $this->input->get('id');
		if($id){
			return $item->where('id','=',$id);
		}

		$searchby = $this->input->get('searchby');
		$key = $this->input->get('key');
		if($searchby=='title' && $key!=''){
			$item = $item->where('judul','LIKE',"%$key%");
		}else if($searchby=='year' && $key!=''){
			$item= $item->whereHas($tahun_kegiatan, function($q){
				$q->where('tahun','=',$this->input->get('key'));
			});
		}

		$tahun = $this->input->get('tahun');
		if($tahun){
			$item = $item->where($tahun_kegiatan.'.id','=',$tahun);
		}

		$dari = $this->input->get('dari');
		$sampai = $this->input->get('sampai');
		if($dari && $sampai && (int)$dari<(int)$sampai){
		    $data = [];
		    for($v=$dari;$v<=$sampai;$v++){
		        $data[]=$v;
		    }
			$item = $item->whereIn($tahun_kegiatan.'.tahun', $data);
		}

		$letter = $this->input->get('letter');
		if($letter){
			if($letter=='90'){
				$item = $item->where(function($q) use ($table_name){
					$q->where($table_name.'judul','LIKE','0%')->orWhere($table_name.'judul','LIKE','1%')->orWhere($table_name.'judul','LIKE','2%')->orWhere($table_name.'judul','LIKE','3%')->orWhere($table_name.'judul','LIKE','4%')->orWhere($table_name.'judul','LIKE','5%')->orWhere($table_name.'judul','LIKE','6%')->orWhere($table_name.'judul','LIKE','7%')->orWhere($table_name.'judul','LIKE','8%')->orWhere($table_name.'judul','LIKE','9%');
				});
			}else{
				$item = $item->where($table_name.'judul','LIKE',$letter.'%');
			}
		}

		$dosen = $this->input->get('dosen');
		if($dosen){
			$item = $item->where('dosen'.'.id','=',$dosen);
		}

		$nama_dosen = $this->input->get('author');
		if($nama_dosen){
			$item = $item->where('dosen.nama','LIKE',"%$nama_dosen%");
		}

		$subject_id = $this->input->get('subject');
		// $subject = $this->input->get('sbj');
		if($subject_id){
		// 	$item = $item->leftJoin('subject_penelitian',function($q) use ($subject,$table_name,$type){
		// 		$q->on('data','=',$table_name.'id');
		// 		$q->on('subject_penelitian.type','=', new Raw($type));
		// 	});
		// 	$item->where('subject_penelitian.subject','=',$type);
			$item = $item->where('subject_penelitian.subject','=',$subject_id);
		}

		$title = $this->input->get('title');
		if($title){
			$item = $item->where($table_name.'judul','LIKE',"%$title%");
		}

		$url = $this->input->get('url');
		if($url){
			$item = $item->havingRaw("url = '$url'");
		}

		$order = $this->input->get('order');
		if($order){
			$item = $item->orderBy('created_at',$order); 
		}else{
			$item = $item->orderBy('created_at','DESC'); 
		}
		return $item;
	}

	public function union(){

		$jurnal = DB::table('jurnal')->select('jurnal.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','jurnal.nama_jurnal as name','jurnal.judul','jurnal.abstrak','tahun_kegiatan.tahun','created_at',new raw("'Publikasi Jurnal' as type"),new raw("'jurnal' as url"))->leftJoin('tahun_kegiatan','jurnal.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','jurnal.dosen','=','dosen.id')->where('jurnal.isdelete', '=', '0')->where('isvalidate','=','1');
		$jurnal = $this->filter($jurnal,'tahun_kegiatan','jurnal.','1');

		$bukuajar = DB::table('buku_ajar')->select('buku_ajar.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','buku_ajar.judul as name','buku_ajar.judul','buku_ajar.judul as abstrak','tahun_kegiatan.tahun','created_at',new raw("'Buku Ajar' as type"),new raw("'bukuajar' as url"))->leftJoin('tahun_kegiatan','buku_ajar.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','buku_ajar.dosen','=','dosen.id')->where('buku_ajar.isdelete', '=', '0')->where('isvalidate','=','1');
		$bukuajar = $this->filter($bukuajar,'tahun_kegiatan','buku_ajar.','2');

		$forumilmiah = DB::table('forum_ilmiah')->select('forum_ilmiah.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','forum_ilmiah.forum as name','forum_ilmiah.judul','forum_ilmiah.abstrak','tahun_kegiatan.tahun','created_at',new raw("'Penyelenggara Forum Ilmiah' as type"),new raw("'forumilmiah' as url"))->leftJoin('tahun_kegiatan','forum_ilmiah.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','forum_ilmiah.dosen','=','dosen.id')->where('forum_ilmiah.isdelete', '=', '0')->where('isvalidate','=','1');
		$forumilmiah = $this->filter($forumilmiah,'tahun_kegiatan','forum_ilmiah.','3');

		$hki = DB::table('hki')->select('hki.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','hki.judul as name','hki.judul','hki.judul as abstrak','tahun_kegiatan.tahun','created_at',new raw("'Hak Kekayaan Intelektual' as type"),new raw("'hki' as url"))->leftJoin('tahun_kegiatan','hki.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','hki.dosen','=','dosen.id')->where('hki.isdelete', '=', '0')->where('isvalidate','=','1');
		$hki = $this->filter($hki,'tahun_kegiatan','hki.','4');

		$luaranlain = DB::table('luaran_lain')->select('luaran_lain.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','luaran_lain.judul as name','luaran_lain.judul','luaran_lain.judul as abstrak','tahun_kegiatan.tahun','created_at',new raw("'Luaran Lain' as type"),new raw("'luaranlain' as url"))->leftJoin('tahun_kegiatan','luaran_lain.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','luaran_lain.dosen','=','dosen.id')->where('luaran_lain.isdelete', '=', '0')->where('isvalidate','=','1');
		$luaranlain = $this->filter($luaranlain,'tahun_kegiatan','luaran_lain.','5');

		$penelitianmandiri = DB::table('penelitian_hibah')->select('penelitian_hibah.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','penelitian_hibah.judul as name','penelitian_hibah.judul','penelitian_hibah.abstrak','tahun_kegiatan.tahun','created_at',new raw("'Penelitian Mandiri' as type"),new raw("'penelitianmandiri' as url"))->leftJoin('tahun_kegiatan','penelitian_hibah.tahun','=','tahun_kegiatan.id')->leftJoin('dosen','penelitian_hibah.dosen','=','dosen.id')->where('penelitian_hibah.isdelete', '=', '0')->where('isvalidate','=','1');
		$penelitianmandiri = $this->filter($penelitianmandiri,'tahun_kegiatan','penelitian_hibah.','6');

		$penelitianinternal = DB::table('penelitian')->select('penelitian.id','dosen.nama','dosen.gelar_depan','dosen.gelar_belakang','jenis_penelitian.nama as name','penelitian.judul','penelitian.abstrak','tahun_kegiatan.tahun','created_at',new raw("'Penelitian Internal' as type"),new raw("'penelitianinternal' as url"))->leftJoin('tahun_kegiatan','penelitian.tahun_kegiatan','=','tahun_kegiatan.id')->leftJoin('dosen','penelitian.dosen','=','dosen.id')->where('penelitian.isdelete', '=', '0')->leftJoin('jenis_penelitian','penelitian.jenis_penelitian','=','jenis_penelitian.id')->join('penelitian_laporan','penelitian.id','=','penelitian_laporan.penelitian')->where('penelitian.status','=','4');
		$penelitianinternal = $this->filter($penelitianinternal,'tahun_kegiatan','penelitian.','7');

		$results = $jurnal->union($bukuajar)->union($forumilmiah)->union($hki)->union($luaranlain)->union($penelitianinternal)->union($penelitianmandiri)->orderBy('created_at','desc')->get();

		$default_view = 10;
		$page = 0;

		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($view_get)) $default_view = $view_get;

		$default_skip = $page*$default_view;
		$count  = count($results);
		$total_pages = $count>0? ceil($count/$default_view):1;
		$info = ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages,"total"=>$count];

		$data = [];
		$data["items"]=array_slice($results,$info["skip"],$info["limit"],true);
		$data["paging"] = $info;
		echo json_encode($data);
	}

	public function tahun_kegiatan_range(){
		$query = "
			SELECT id, dari, sampai, sum(total) as total
			FROM (
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(jurnal.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun_kegiatan FROM jurnal WHERE jurnal.isdelete=0 AND jurnal.isvalidate=1 group by tahun_kegiatan ) AS jurnal 
				ON jurnal.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)


				UNION
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(buku_ajar.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun_kegiatan FROM buku_ajar WHERE buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1 group by tahun_kegiatan ) AS buku_ajar 
				ON buku_ajar.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)

				UNION
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(forum_ilmiah.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun_kegiatan FROM forum_ilmiah WHERE forum_ilmiah.isdelete=0 AND forum_ilmiah.isvalidate=1 group by tahun_kegiatan ) AS forum_ilmiah 
				ON forum_ilmiah.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)

				UNION
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(luaran_lain.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun_kegiatan FROM luaran_lain WHERE luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1 group by tahun_kegiatan ) AS luaran_lain 
				ON luaran_lain.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)

				UNION
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(hki.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun_kegiatan FROM hki WHERE hki.isdelete=0 AND hki.isvalidate=1 group by tahun_kegiatan ) AS hki 
				ON hki.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)

				UNION
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(penelitian_hibah.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun as tahun_kegiatan FROM penelitian_hibah WHERE penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1 group by tahun_kegiatan ) AS penelitian_hibah 
				ON penelitian_hibah.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)

				UNION
				SELECT subject_tahun.id,dari.tahun as dari,sampai.tahun as sampai, sum(penelitian.total) as total
				FROM subject_tahun
				LEFT JOIN tahun_kegiatan as dari ON subject_tahun.tahun_dari = dari.id
				LEFT JOIN tahun_kegiatan as sampai ON subject_tahun.tahun_sampai = sampai.id
				LEFT JOIN (SELECT count(*) as total,tahun_kegiatan FROM penelitian WHERE penelitian.isdelete=0 AND penelitian.status=4 group by tahun_kegiatan ) AS penelitian 
				ON penelitian.tahun_kegiatan in 
				(SELECT id FROM tahun_kegiatan where (CAST(tahun AS SIGNED INTEGER))>=(CAST(dari.tahun AS SIGNED INTEGER)) and (CAST(tahun AS SIGNED INTEGER))<=(CAST(sampai.tahun AS SIGNED INTEGER)))
				GROUP BY CONCAT(CONCAT(dari.tahun,'-'),sampai.tahun)
			) as gabungan
			GROUP BY CONCAT(CONCAT(dari,'-'),sampai)
			ORDER BY total DESC
			LIMIT 5
			";
		$data = DB::select($query);
		echo json_encode($data);
	}

	public function dosen_tertinggi(){
		$letter = $this->input->get('letter');
		if($letter){
			$letter = " AND nama LIKE '$letter%'";
		}

		$query = "SELECT id,nama,gelar_depan,gelar_belakang, sum(total) as total
			FROM
			(
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(jurnal.dosen) as total
			FROM dosen
			LEFT JOIN jurnal ON jurnal.dosen = dosen.id and jurnal.isvalidate=1 and jurnal.isdelete=0
			group by jurnal.dosen

			UNION
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(buku_ajar.dosen) as total
			FROM dosen
			LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id and buku_ajar.isvalidate=1 and buku_ajar.isdelete=0
			group by buku_ajar.dosen

			UNION
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(forum_ilmiah.dosen) as total
			FROM dosen
			LEFT JOIN forum_ilmiah ON forum_ilmiah.dosen = dosen.id and forum_ilmiah.isvalidate=1 and forum_ilmiah.isdelete=0
			group by forum_ilmiah.dosen

			UNION
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(luaran_lain.dosen) as total
			FROM dosen
			LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id and luaran_lain.isvalidate=1 and luaran_lain.isdelete=0
			group by luaran_lain.dosen

			UNION
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(hki.dosen) as total
			FROM dosen
			LEFT JOIN hki ON hki.dosen = dosen.id and hki.isvalidate=1 and hki.isdelete=0
			group by hki.dosen

			UNION
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(penelitian_hibah.dosen) as total
			FROM dosen
			LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id and penelitian_hibah.isvalidate=1 and penelitian_hibah.isdelete=0
			group by penelitian_hibah.dosen

			UNION
			SELECT dosen.id, dosen.nama, dosen.gelar_depan, dosen.gelar_belakang, count(penelitian.dosen) as total
			FROM dosen
			LEFT JOIN penelitian ON penelitian.dosen = dosen.id and penelitian.status=4 and penelitian.isdelete=0
			group by penelitian.dosen
			) as gabungan
			WHERE total>0 $letter
			GROUP BY nama
			ORDER BY total DESC";
		$results = DB::select($query);

		$default_view = 10;
		$page = 0;

		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($view_get)) $default_view = $view_get;

		$default_skip = $page*$default_view;
		$count  = count($results);
		$total_pages = $count>0? ceil($count/$default_view):1;
		$info = ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages,"total"=>$count];

		$data = [];
		$data["items"]=array_slice($results,$info["skip"],$info["limit"],true);
		$data["paging"] = $info;

		echo json_encode($data);
	}

	public function pengumuman(){
		$this->load->model('News');
        $this->load->model('News_meta');

		$news = News::where('isdelete','=','0');
		$info = $this->create_paging($news);
		$news = $news->take($info["limit"])->skip($info["skip"]);
		$news = $news->orderBy('created_at','DESC')->get();
		$news->load('news_meta');
		echo $news->toJson();
	}

	public function create_paging($model){
		$default_view = 10;
		$page = 0;

		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($view_get)) $default_view = $view_get;

		$default_skip = $page*$default_view;
		$count  = $model->count();
		$total_pages = $count>0? ceil($count/$default_view):1;
		return ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages,"total"=>$count];
	}

}
