<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Subjects extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p7"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model('Subject');
        $this->load->model('Subject_penelitian');
    }	
	public function index(){
		$data = [];
		
		$subject = Subject::where("isdelete","=","0");
		$subject = $this->filter($subject);
		$info = $this->create_paging($subject);
		$subject = $subject->take($info["limit"])->skip($info["skip"])->get();


		$data["items"] = $subject->toArray();
		$id =$this->input->get('id');
		if(!empty($id)){
			$parent = Subject::find($this->input->get('id'));
			$parent->load('parentnya');
			$tree = $this->recursive_check($parent->toArray());
			$data["trees"] = $tree;
			$data["parent"] = $parent->toArray();
		}
		// $data[""]
		$this->twig->display('pengaturan/repository/subject/index',array_merge($data,$this->menu,$info));
	}

	private function recursive_check($items,$data=[]){
		if($items["parentnya"]){
			$data = $this->recursive_check($items["parentnya"],$data);
			$data[] = ["nama"=>$items["nama"],"id"=>$items["id"]];
			return $data;
		}else{
			return [["nama"=>$items["nama"],"id"=>$items["id"]]];
		}
	}

	private function filter($q){
		$id=$this->input->get('id');
		if(!empty($id)) $q->where('parent','=',$id);
		else $q->whereNull('parent');
		return $q;
	}

	public function insert(){
		$nama = $this->input->post("nama");
		$deskripsi = $this->input->post("deskripsi");
		$parent = $this->input->post("parent");

		$data = new Subject;
		$data->nama = $nama;
		$data->deskripsi = $deskripsi;
		if(!empty($parent)) $data->parent=$parent;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Repository-Subject baru dengan judul '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete(){
		$id = $this->uri->segment(4);
		$item = Subject::find($id);
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Repository-Subject baru dengan judul '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		$item->delete();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function update(){
		$id = $this->uri->segment(4);
		$nama = $this->input->post("nama");
		$deskripsi = $this->input->post("deskripsi");
		// $parent = $this->input->post("parent");

		$data = Subject::find($id);
		$data->nama = $nama;
		$data->deskripsi = $deskripsi;
		// if(!empty($parent)) $data->parent=$parent;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Repository-Subject dengan judul '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function assign($id){
		$data = [];
		$this->load->model('luaran_penelitian/Jurnal');$this->load->model('luaran_penelitian/Buku_ajar');$this->load->model('luaran_penelitian/Forum_ilmiah');$this->load->model('luaran_penelitian/Hki');$this->load->model('luaran_penelitian/Luaran_lain');$this->load->model('luaran_penelitian/Penelitian_hibah');$this->load->model('luaran_penelitian/Penelitian_hibah');$this->load->model('Penelitian');
		$subject = Subject_penelitian::where("id",">","0")->where("subject","=",$id);
		$subject = $this->filter_assign($subject);
		$info = $this->create_paging($subject);
		$subject = $subject->take($info["limit"])->skip($info["skip"])->get();

		if(!empty($id)){
			$parent = Subject::find($id);
			$data["parent"] = $parent->toArray();
		}

		$data["items"] = $subject->toArray();
		$data["subject"] = $this->uri->segment(4);
		$this->twig->display('pengaturan/repository/subject/assign_subject',array_merge($data,$this->menu,$info));
	}

	private function filter_assign($q){
		$jenis = $this->input->get("jenis");
		if($jenis){
			$q->where('type','=',$jenis);
		}

		$judul = $this->input->get("judul");
		if($judul){
			$q->where('judul','LIKE',"%$judul%");
		}

		return $q;
	}

	private function fetch_data(){
		$items = [];
		$info = [];
		$jenis = $this->input->get('jenis');
		switch($jenis){
			case '1':
				$this->load->model('luaran_penelitian/Jurnal');
				$items = Jurnal::where('isdelete','=','0')->where('isvalidate','=','1')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;
			case '2':
				$this->load->model('luaran_penelitian/Buku_ajar');
				$items = Buku_ajar::where('isdelete','=','0')->where('isvalidate','=','1')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;
			case '3':
				$this->load->model('luaran_penelitian/Forum_ilmiah');
				$items = Forum_ilmiah::where('isdelete','=','0')->where('isvalidate','=','1')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;
			case '4':
				$this->load->model('luaran_penelitian/Hki');
				$items = Hki::where('isdelete','=','0')->where('isvalidate','=','1')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;
			case '5':
				$this->load->model('luaran_penelitian/Luaran_lain');
				$items = Luaran_lain::where('isdelete','=','0')->where('isvalidate','=','1')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;
			case '6':
				$this->load->model('luaran_penelitian/Penelitian_hibah');
				$items = Penelitian_hibah::where('isdelete','=','0')->where('isvalidate','=','1')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;
			case '7':
				$this->load->model('Penelitian');
				$items = Penelitian::where('isdelete','=','0')->where('status','=','4')->whereDoesntHave('subject_penelitian',function($q){
                      $q->where('subject','=',$this->uri->segment(4))->where('type','=',$this->input->get('jenis'));
                    });
				break;			
			default:
				break;
		}

		$judul = $this->input->get('judul');
		if(!empty($judul)) $items = $items->where('judul','LIKE','%'.$judul.'%');

		// echo $items->toSql(); die;
		if(!empty($jenis)){
			$info = $this->create_paging($items);
			$items = $items->take($info["limit"])->skip($info["skip"])->get()->toArray();
		}
		return ["items"=>$items,"info"=>$info];
	}

	public function add_data($id){
		$data = [];
		
		$items = $this->fetch_data();


		if(!empty($id)){
			$parent = Subject::find($id);
			$data["parent"] = $parent->toArray();
		}

		$data["items"] = $items["items"];
		$data["subject"] = $this->uri->segment(4);
		$data["jenis"] = $this->input->get('jenis');
		$data["judul"] = $this->input->get('judul');
		$this->twig->display('pengaturan/repository/subject/add_data',array_merge($data,$this->menu,$items["info"]));
	}

  	public function add_satuan(){
	    $id = $this->uri->segment(4);
	    $subject = $this->input->get("subject");
	    $type = $this->input->get("type");
	    $judul = $this->input->get("judul");

	    $q = Subject_penelitian::firstOrNew(["subject"=>$subject,"data"=>$id,"type"=>$type,"judul"=>$judul]);
	    $q->save();
	    $first_four_title = implode(' ', array_slice(explode(' ', $judul), 0, 4));
	    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menambah data pada konfigurasi Repository-Subject pada nama subject '.$q->subject()->first()->nama.' dengan jenis penelitian '.$this->get_jenis($type).' yang berjudul '.$first_four_title,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
	    redirect($_SERVER["HTTP_REFERER"]);
    }

  	public function add_semua(){
	    $subject = $this->uri->segment(4);
	    $datas = $this->input->post("data");
	    $type = $this->input->post("type");
	    $juduls = $this->input->post("judul");

	    // echo $type;die;
	    foreach($datas as $key=>$data){
		    $q = Subject_penelitian::firstOrNew(["subject"=>$subject,"data"=>$data,"type"=>$type,"judul"=>$juduls[$data]]);
		    $q->save();
		    $first_four_title = implode(' ', array_slice(explode(' ', $q->judul), 0, 4));
		    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menambah data pada konfigurasi Repository-Subject pada nama subject '.$q->subject()->first()->nama.' dengan jenis penelitian '.$this->get_jenis($type).' yang berjudul '.$first_four_title,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
	    }
	    redirect($_SERVER["HTTP_REFERER"]);
	    // redirect('repos/subjects/'.$subject);
  }

	public function delete_data(){
		$id = $this->uri->segment(4);
		$item = Subject_penelitian::find($id);
		$first_four_title = implode(' ', array_slice(explode(' ', $item->judul), 0, 4));
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus data pada konfigurasi Repository-Subject pada nama subject '.$item->subject()->first()->nama.' dengan jenis penelitian '.$this->get_jenis($type).' yang berjudul '.$first_four_title,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		$item->delete();
		redirect($_SERVER['HTTP_REFERER']);
	}

  	private function get_jenis($jenis){
		switch($jenis){
			case '1':
				return 'Jurnal';
				break;
			case '2':
				return 'Buku Ajar';
				break;
			case '3':
				return 'Forum Ilmiah';
				break;
			case '4':
				return 'HKI';
				break;
			case '5':
				return 'Luaran Lain';
				break;
			case '6':
				return 'Penelitian Mandiri';
				break;
			case '7':
				return 'Penelitian';
				break;			
			default:
				return '';
				break;
  		}
	}
}
