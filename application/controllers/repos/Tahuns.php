<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Tahuns extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p7"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model('Subject_tahun');
        $this->load->model('Tahun_kegiatan');
    }	
public function index(){
		$data = [];

		$tahun = Tahun_kegiatan::where('isdelete','=',0)->orderBy('tahun','asc')->get();
		
		$bill = Subject_tahun::where('id','>','0');
    $info = $this->create_paging($bill);
    $bill = $bill->take($info["limit"])->skip($info["skip"])->get();
		$bill->load('tahun_dari')->load('tahun_sampai');

		$data["tahun"] = $tahun->toArray();
		$data["items"] = $bill->toArray();
		$this->twig->display('pengaturan/repository/tahun/index',array_merge($data,$this->menu,$info));
	}

public function insert(){
      $tahun_dari = $this->input->post('tahun_dari');
      $tahun_sampai = $this->input->post('tahun_sampai');


      $data = new Subject_tahun;
      $data->tahun_dari = $tahun_dari;
      $data->tahun_sampai = $tahun_sampai;
      $data->save();
      Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Repository-Billboard dengan tahun '.$data->tahun_dari()->first()->tahun.' sampai '.$data->tahun_sampai()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
      redirect($_SERVER['HTTP_REFERER']);
  }

public function update(){
	$id = $this->uri->segment(4);
  	$tahun_dari = $this->input->post('tahun_dari');
  	$tahun_sampai = $this->input->post('tahun_sampai');


  	$data = Subject_tahun::find($id);
  	$data->tahun_dari = $tahun_dari;
  	$data->tahun_sampai = $tahun_sampai;
  	$data->save();
    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Repository-Tahun dengan tahun '.$data->tahun_dari()->first()->tahun.' sampai '.$data->tahun_sampai()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
  	redirect($_SERVER['HTTP_REFERER']);
}

public function delete(){
    $id = $this->uri->segment(4);
    $item = Subject_tahun::find($id);
    // if(!$this->isPengguna($item->dosen)) return;
    Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Repository-Tahun dengan tahun '.$item->tahun_dari()->first()->tahun.' sampai '.$item->tahun_sampai()->first()->tahun,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
    $item->delete();# = 1;
    #$item->save();
    redirect($_SERVER['HTTP_REFERER']);
    // redirect('akreditas/manageakreditas/');
  }

}
