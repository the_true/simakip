<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Billboards extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p7"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model('Billboard');
    }	
	public function index(){
		$data = [];
		
		$bill = Billboard::where('id','>','0');
		$info = $this->create_paging($bill);
		$bill = $bill->take($info["limit"])->skip($info["skip"])->get();

		$data["items"] = $bill->toArray();
		$this->twig->display('pengaturan/repository/billboard/index',array_merge($data,$this->menu,$info));
	}

	private function upload(){
		$this->generate_folder('uploads/billboard');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/billboard'),
			'allowed_types' => 'jpeg|jpg|png|gif',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		$filename = "";
		if($uploaded) $filename =  $this->upload->data('file_name');
		return $filename;
	}

	public function delete($id){
		$data = Billboard::find($id);
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Repository-Billboard dengan judul '.$data->judul,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		$data->delete();
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function insert(){
		$filename = $this->upload();
		$judul = $this->input->post('judul');
		$url = $this->input->post('url');
		$deskripsi = $this->input->post('deskripsi');

		$data = new Billboard;
		$data->judul = $judul;
		$data->url = $url;
		$data->deskripsi = $deskripsi;
		$data->image = $filename;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Repository-Billboard baru dengan judul '.$data->judul,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function update($id){
		$filename = $this->upload();
		$judul = $this->input->post('judul');
		$url = $this->input->post('url');
		$status = $this->input->post('status');
		$deskripsi = $this->input->post('deskripsi');

		$data = Billboard::find($id);
		$data->judul = $judul;
		$data->url = $url;
		$data->deskripsi = $deskripsi;
		$data->status = $status;
		if($filename!="") $data->image = $filename;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Repository-Billboard dengan judul '.$data->judul,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect($_SERVER['HTTP_REFERER']);
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}
}
