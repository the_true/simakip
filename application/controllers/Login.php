<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Login extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
 	private $month = ["Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember"];
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio','set_select'],
		    'functions_safe' => ['validation_errors_array'],
		];
        parent::__construct($config,False);
        $this->load->helper('security');
        $this->load->model('News');
        $this->load->model('News_meta');
        $this->load->model('Tautan');
        $this->load->model('Dosen');
        $this->load->model('Jabatan_akademik');
        $this->load->model('Jabatan_fungsi');
        $this->load->model('Fakultas');
        $this->load->model('Program_studi');
        $this->load->model('Akses');
        $this->load->model('Tahun_kegiatan');
    }


	public function index()
	{
        if($this->check_login()) redirect('home');
		$this->load->helper('captcha');
		$this->twig->addGlobal('session', $this->session);

		$tahun_get = $this->input->get('tahun');
		$bulan_get = $this->input->get('bulan');

		$news = News::where('isdelete','=','0');
		if(isset($tahun_get) && $tahun_get!='') $news->whereRaw("extract(YEAR from created_at) = ?",[$tahun_get]);
		else $news->whereRaw("extract(YEAR from created_at) = ?",[Date('Y')]);
		if(isset($bulan_get) && $bulan_get!='') $news->whereRaw("extract(MONTH from created_at) = ?",[$bulan_get]);
		else $news->whereRaw("extract(MONTH from created_at) = ?",[Date('n')]);
		$news = $news->orderBy('created_at','DESC')->get();
		$news->load('news_meta');
		$tautan = Tautan::where("isdelete","=","0")->get();
		$data = array("news"=>$news->toArray(),"beranda"=>"active","items"=>$tautan->toArray());
		$tahun = Tahun_kegiatan::where('isdelete','=','0')->orderBy('tahun','DESC')->get();
		$data['tahun'] = $tahun->toArray();
		$data['bulan'] = isset($bulan_get) && $bulan_get!=''?$bulan_get:Date('n');
		$data['bulan_word'] = $this->month[$data['bulan']-1];
		$data['tahuns'] = isset($tahun_get) && $tahun_get!=''?$tahun_get:Date('Y');
		$this->twig->display('login', $data);
	}

	public function pagenotfound(){
		$data = array();
		$this->twig->display('404', $data);
	}

	public function indexlistl(){
        if($this->check_login()) redirect('home');
		$this->load->helper('captcha');
		$default_view = 10;
		$page = 0;

		$nidn_get = $this->input->get("nidn");
		$nama_get = $this->input->get("nama");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		$data = array("login"=>False);

		if(isset($page_get)) $page = $page_get-1;
		if(isset($nidn_get) && $nidn_get!='' && is_numeric($nidn_get)){
			$dosen = Dosen::where('isdelete','=','0');
			$dosen->where('nidn','=',$nidn_get);
		}
		if(isset($nama_get) && $nama_get!=''){
			$dosen = Dosen::where('isdelete','=','0');
			$dosen->where("nama","LIKE","%".$nama_get."%");
		}
		if(isset($view_get)) $default_view = $view_get;
		if((isset($nidn_get)&&$nidn_get!='')||(isset($nama_get)&&$nama_get!='')){
			$count = $dosen->count();
			$dosen = $dosen->get();

			$dosen->load('Jabatan_akademik');
			$dosen->load('Jabatan_fungsional');
			$dosen->load('Fakultas');
			$dosen->load('Program_studi');
			$data["items"] = $dosen->toArray();
			$data['views'] = $default_view;
			$data['page'] = $page+1;
			$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		}
		$this->load->helper('report');
		$data["point_fakultas"] = getPointFakultas();
		$data["point_prostud"] = getPointProstud();
		$data["point_dosen"] = getPointDosen();
		$data["jabatan_akademik"] = getJabatanAkademik();
		$data["jenjang_pendidikan"]= getPendidikanDosen();
		$data["jabatan_fungsional"] = getJabatanFungsi();
		$this->twig->display('pengguna/list',$data);
	}


	public function clear(){
		$this->session->sess_destroy();
		redirect('/');
	}

	public function auth(){
		$salt = $this->config->item('salt');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$captcha = $this->input->post('captcha');

		if(!$username || !$password){
			$this->session->set_flashdata('error', 'empty username or password');
			redirect('/login');
			return;
		}
		if($this->input->get('bepas')!="true"){
			if($captcha=="" or empty($captcha)){
				$this->session->set_flashdata('error', 'Captcha must not be empty!');
				redirect('login/');
				return;
			}else if(!$this->check_captcha($captcha)){
				redirect('login/');
				return;
			}
		}
		$user = Dosen::where('nidn', '=', $username)->where('status_data','=',1)->with("fakultas")->with("program_studi")->with("jabatan_akademik")->with("jabatan_fungsional")->with("akses")->first();
		if(!isset($user)){
			$this->session->set_flashdata('error', 'wrong password or username!');
			redirect('/login');
		}
		$combined = $salt.$password.$user->salt;
		$hash_password = do_hash($combined);
		if(($user->password)==$hash_password){
		    $this->session->set_userdata('userinfo',$user->toArray());
			redirect('/');
		}else{
			$this->session->set_flashdata('error', 'wrong password or username!');
			redirect('/login');
		}
	}

	public function check_captcha($str){
	    $word = $this->session->userdata('captchaWord');
        $this->load->library('Mobile_Detect');
	    $detect = new Mobile_Detect();
	    if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
	        return true;
	    }
	    if(strcmp(strtoupper($str),strtoupper($word)) == 0){
	      return true;
	    }
	    else{
	      $this->session->set_flashdata('error', 'Please enter correct captcha!');
	      return false;
	    }
	}

	public function captcha_image(){
		$this->load->helper('captcha');
		$random_number = substr(number_format(time() * rand(),0,'',''),0,4);
		// setting up captcha config
		$vals = array(
		     'word' => $random_number,
		     'font_size' => 5,
		     'img_path' => './captcha/',
		     'img_url' => base_url().'captcha/',
		     'img_width' => 94,
		     'img_height' => 33,
		     'expiration' => 7200
		    );
		$data = create_captcha($vals);
		$this->session->set_userdata('captchaWord',$data['word']);
		echo $data["image"];
	  }

	public function register(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_message('nidn_verification','%s already exists');
		$this->form_validation->set_message('nidn_zerocheck',' 0 can\'t be in the front');
		$this->form_validation->set_rules('nidn', 'NIDN', 'required|integer|callback_nidn_verification');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tingkat', 'Tingkat Pendidikan', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan Pendidikan', 'required');
		$this->form_validation->set_rules('jabatan_akademik', 'Jabatan Akademik', 'required');
		$this->form_validation->set_rules('jabatan_fungsional', 'Jabatan Fungsional', 'required');
		$this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
		$this->form_validation->set_rules('program_studi', 'Nama Program Studi', 'required');
		$this->form_validation->set_rules('hp', 'No. HP', 'required');
		$this->form_validation->set_rules('surel', 'Surel', 'required|valid_email');
		$this->form_validation->set_rules('status_dosen', 'Status Dosen', 'required');
		$this->form_validation->set_rules('status_data', 'Status Data', 'required');
		$this->form_validation->set_rules('akses', 'Hak Akses', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[konfirmasi]');
		$this->form_validation->set_rules('konfirmasi', 'Konfirmasi Password', 'required');
		$this->form_validation->set_rules('passworddikti', 'Password Dikti', 'required');

		$jab_ak = Jabatan_akademik::all();
		$jab_fu = Jabatan_fungsi::all();
		$fak = Fakultas::all();
		$pro_stu = Program_studi::all();
		$akslemlit = Akses::WhereIn("nama",["Operator Lemlitbang","Sekretaris Lemlitbang","Ketua Lemlitbang"])->get();
		$akslppm = Akses::WhereIn("nama",["Operator LPPM","Sekretaris LPPM","Ketua LPPM"])->get();
		$aks = Akses::WhereIn("nama",["Dosen","Wakil Rektor","Rektor","Keuangan"])->get();

		$data = array("jab_ak"=>$jab_ak->toArray(),"jab_fu"=>$jab_fu->toArray(),
			'fak'=>$fak->toArray(),'pro_stu'=>$pro_stu->toArray(),"aks"=>$aks->toArray(),"akslemlit"=>$akslemlit->toArray(),"akslppm"=>$akslppm->toArray());

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('register',$data);
        }else{
        	if ($this->submit())
        		$this->session->set_flashdata('success', 'Dosen berhasil ditambah!');
        		redirect('pengguna');
		}
	}

	public function nidn_zerocheck($nidn){
		if($nidn[0]=='0') return False;
		return True;
	}

	public function nidn_verification($nidn){
        $item = Dosen::where('nidn','=',$nidn)->count();
        if($item>0)
            return False;
        return True;
	}

	private function submit(){
		$nidn = $this->input->post('nidn');
		$nama = $this->input->post('nama');
		$gelar_depan = $this->input->post('gelar_depan');
		$gelar_belakang = $this->input->post('gelar_belakang');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$jenjang_pendidikan = $this->input->post('tingkat').' - '.$this->input->post('jurusan');
		$jabatan_akademik = $this->input->post('jabatan_akademik');
		$jabatan_fungsional = $this->input->post('jabatan_fungsional');
		$fakultas = $this->input->post('fakultas');
		$program_studi = $this->input->post('program_studi');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('prefix_telp').'-'.$this->input->post('telp');
		$hp = $this->input->post('hp');
		$surel = $this->input->post('surel');
		$status_dosen = $this->input->post('status_dosen');
		$status_data = $this->input->post('status_data');
		$akses = $this->input->post('akses');
		$isreviewer = $this->input->post('reviewer');
		$isketuaprodi = $this->input->post('ketuaprodi');
		$id_schopus = $this->input->post('id_schopus');
		$id_scholar = $this->input->post('id_scholar');
		$id_sinta = $this->input->post('id_sinta');
		$id_orchid = $this->input->post('id_orchid');
		$uuid = uniqid();
		$salt = $this->config->item('salt');
		$password = do_hash($salt.$this->input->post('password').$uuid);


		$dosen = new Dosen;
		$dosen->nidn = $nidn;
		$dosen->nama = $nama;
		$dosen->gelar_depan = $gelar_depan;
		$dosen->gelar_belakang = $gelar_belakang;
		$dosen->jenis_kelamin = $jenis_kelamin;
		$dosen->jenjang_pendidikan = $jenjang_pendidikan;
		$dosen->jabatan_akademik = $jabatan_akademik;
		$dosen->jabatan_fungsional = $jabatan_fungsional;
		$dosen->fakultas = $fakultas;
		$dosen->program_studi = $program_studi;
		$dosen->alamat = $alamat;
		$dosen->telp = $telp;
		$dosen->hp = $hp;
		$dosen->surel = $surel;
		$dosen->status_dosen = $status_dosen;
		$dosen->status_data = $status_data;
		$dosen->akses = $akses;
		$dosen->password = $password;
		$dosen->salt = $uuid;
		$dosen->isreviewer = $isreviewer;
		$dosen->isketuaprodi = $isketuaprodi;
		$dosen->id_schopus = $id_schopus;
		$dosen->id_scholar = $id_scholar;
		$dosen->id_sinta = $id_sinta;
		$dosen->id_orchid = $id_orchid;
		$dosen->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat data Dosen baru dengan nama '.$dosen->nama,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);
		return True;
	}

	public function changepwd(){
		$salt = $this->config->item('salt');
		$uuid = uniqid();
		$password = '4dm1n';
		$combined = $salt.$password.$uuid;
		$hash = do_hash($combined);

		$dosen = new Dosen;

		$dosen->nidn = "111";
		$dosen->nama = "SUPERADMIN";
		$dosen->gelar_depan = "Tuan";
		$dosen->gelar_belakang = "Saudagar";
		$dosen->jenis_kelamin = "L";
		$dosen->jenjang_pendidikan = "S9";
		$dosen->jabatan_akademik = "SUPERADMIN";
		$dosen->jabatan_fungsional = "SUPERADMIN";
		$dosen->salt = $uuid;
		$dosen->password = $hash;
		$dosen->save();
	}

	public function forgotpassword(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_message('check_email','%s belum terdaftar sebagai user');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email');
        if ($this->form_validation->run() == FALSE){
			$this->twig->display('pengguna/forgotpassword');
        }else{
        	$item = Dosen::where('surel','=',$this->input->post('email'))->first();
        	$this->sendemail($item);
    		$this->twig->display('pengguna/forgotpassword',["success"=>true]);
		}
	}

	private function sendemail($dosen){
		$this->load->helper('url');
		$this->load->library('encrypt');
		$id = $dosen->id;
		$salt = $dosen->salt;
		$expiration = strtotime('+2 days');
		$link = site_url()."login/changepassword?key=".rawurlencode($this->encrypt->encode($salt.",".$id.",reset_password,".$expiration));
		$this->load->library('email');

		$this->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
		$this->email->to($dosen->surel);

		$this->email->subject('Permintaan Pembaruan Password');
		$this->email->message($this->load->view('email_template/reset_password_email_template', ["link"=>$link,"nama"=>$dosen->nama,"email"=>$dosen->surel], true));
		$this->email->set_mailtype('html');
		$this->email->send();
	}

	public function changepassword(){
		$this->load->library('encrypt');
		$key = $this->input->get('key');
		$plain_text = $this->encrypt->decode(rawurldecode($key));
		$array_info = explode(',', $plain_text);
		// print_r($array_info);
		// die;

		$this->expiration_check($array_info[3]);

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[konfirmasi]');
		$this->form_validation->set_rules('konfirmasi', 'Konfirmasi Password', 'required');
        if ($this->form_validation->run() == FALSE){
			$this->twig->display('changepassword',["id"=>$array_info[1],"salt"=>$array_info[0]]);
        }else{
			$salt = $this->config->item('salt');
			$combined = $salt.$this->input->post('password').$this->input->post('salt');
			$hash = do_hash($combined);
			$item = Dosen::where("id","=",$this->input->post('id'))->first();
			$item->password = $hash;
			$item->save();
			$this->sendemail_notice($item,$this->input->post('password'));
    		$this->twig->display('changepassword',["success"=>true]);
		}
	}

	private function sendemail_notice($dosen, $password){
		$this->load->helper('url');
		$this->load->library('encrypt');
		$id = $dosen->id;
		$salt = $dosen->salt;
		$this->load->library('email');

		$this->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
		$this->email->to($dosen->surel);

		$this->email->subject('Permintaan Pembaruan Password');
		$this->email->message($this->load->view('email_template/notice_password_change_template', ["nama"=>$dosen->nama,"nidn"=>$dosen->nidn,"password"=>$password], true));
		$this->email->set_mailtype('html');
		$this->email->send();
	}

	private function expiration_check($timestamp){
		$today = strtotime("now");
		if($today>(float) $timestamp){
			$this->twig->display('pengguna/forgotpassword',["warn"=>true]);
			return;
		}
	}


	public function check_email($email){
        $item = Dosen::where('surel','=',$email)->count();
        if($item>0)
            return True;
        return False;
	}
}
