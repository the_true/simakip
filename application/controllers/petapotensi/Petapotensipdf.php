<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
use Illuminate\Database\Capsule\Manager as DB;
class Petapotensipdf extends MY_BaseController {

  private $menu = array("akreditas"=>"active");
  public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select','searchpetapotensi'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Fakultas");
        $this->load->helper('report');
  }

  public function index(){

  }

  public function downloadpotensi(){
    $items = [];
    $items["syarat"] = $this->getSyaratItem($items);
    $items["items"] = $this->querypotensi($items);
    $items["download"] = date('d-M-Y');
    // echo json_encode($items); die;
   // $this->twig->display('petapotensi/pdf/potensipdf',array_merge($this->menu,$items));
     if(count($items["items"])>400){
      echo 'PDF to big, not enough memory to generate'; die;
     } 
   // echo count($items["items"]); die;

   $isi = $this->twig->render('petapotensi/pdf/potensipdf',array_merge($this->menu,$items));
   // echo $isi;die;
   
   $nama_prodi = $this->input->get('nama_prodi');
   $nama_fakultas = $this->input->get('nama_fakultas');
   $prodi = $this->input->get('program_studi');
   $fakultas = $this->input->get('fakultas');
   $title = 'Peta Potensi Publikasi - ';

   if($nama_prodi!='') $title .= 'Program Studi '.$program_studi;
   elseif($prodi=='' && $nama_fakultas!='') $title.= 'Semua Program Studi Fakultas '.$nama_fakultas;
   else $title .= 'Semua Fakultas';
   $title .='.pdf';
   // $title = 'download potensi.pdf';
   $this->compose_pdf($isi,$title);

  }

  public function downloadrealisasi(){
    $items = [];
    $items["syarat"] = $this->getSyaratItem($items);
    $items["items"] = $this->queryrealisasi($items);
    $items["download"] = date('d-M-Y');
    // echo json_encode($items["items"]); die;
   // $this->twig->display('petapotensi/pdf/realisasipdf',array_merge($this->menu,$items));
   $isi = $this->twig->render('petapotensi/pdf/realisasipdf',array_merge($this->menu,$items));
   
   $nama_prodi = $this->input->get('nama_prodi');
   $nama_fakultas = $this->input->get('nama_fakultas');
   $prodi = $this->input->get('program_studi');
   $fakultas = $this->input->get('fakultas');
   $title = 'Peta Potensi Publikasi - ';

   if($nama_prodi!='') $title .= 'Program Studi '.$program_studi;
   elseif($prodi=='' && $nama_fakultas!='') $title.= 'Semua Program Studi Fakultas '.$nama_fakultas;
   else $title .= 'Semua Fakultas';
   $title .='.pdf';
   // $title = 'download potensi.pdf';
   $this->compose_pdf($isi,$title);

  }

  private function queryrealisasi($items){
    $dari = $this->input->get('dari');
    $sampai = $this->input->get('sampai');
    $fakultas = $this->input->get('fakultas');
    $program_studi= $this->input->get('program_studi');
    $urutkan = $this->input->get('orderby');

    $where = "";
    $group = "";
    $nama = "";
    $order = "DESC";

    $fakultasx = "";
    if(!empty($fakultas)){
      $fakultasx=" AND dosen.fakultas=".$fakultas;
    }

    if(isset($urutkan)){
      $order = $urutkan;
    }

    $items["orderby"] = $order;

    if(isset($dari) && !empty($dari)){
      $dari = $dari.'-01-01 00:00:00';
      $dari = " AND created_at>='$dari'";
    }else{
      return;
    }
    if(isset($dari) && !empty($dari)){
      $sampai = $sampai.'-12-31 23:59:59';
      $sampai = " AND created_at<='$sampai'";
    }else{
      return;
    }

    if($program_studi=="all" && $fakultas!=""){
      // $group = ',dosen.program_studi';
      // $nama = ' program_studi.nama, ';
    }else if($program_studi!="all"  && !empty($program_studi)){
      $where = "AND dosen.program_studi=$program_studi";
      // $group = ',dosen.program_studi, dosen.id';
      // $nama = ' dosen.nama, ';
    }
    // $where = "AND dosen.program_studi=$program_studi";
    $group = ',dosen.program_studi, dosen.id';
    $nama = ' dosen.nama, dosen.nidn, dosen.jenjang_pendidikan, program_studi.nama as program_studi, jabatan_akademik.nama as jabatan_akademik, ';

    $sql = $this->sqlCompose($items,$dari,$sampai);
    $tampil = $sql["SELECT"];
    $tampil2 = $sql["SELECT2"];
    $join = $sql["JOIN"];
    $total = $sql["TOTAL"];
    $wherex = $sql["WHERE"];

    $query = "SELECT dosen.fakultas, fakultas.nama,dosen.program_studi, $nama
          $tampil
          $tampil2
          $total as total
      FROM dosen 
      LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
      LEFT JOIN jabatan_akademik ON jabatan_akademik.id=dosen.jabatan_akademik
      LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
      $join
      WHERE dosen.fakultas<>0 $fakultasx $where AND (1=2 $wherex)
      GROUP BY fakultas.nama $group
      HAVING total>0
      ORDER BY total $order";

    $allowedFunctions = array("fc");
    // echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
    $query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
         function ($m) {
              return $m[1]($m[2],$m[3]);
         }, $query);
    // echo $query; die;
    $data = DB::select($query);
    // echo json_encode($data); die;
    $this->load->model('luaran_penelitian/Jurnal');
    $this->load->model('luaran_penelitian/Buku_ajar');
    $this->load->model('luaran_penelitian/Forum_ilmiah');
    $this->load->model('luaran_penelitian/Hki');
    $this->load->model('luaran_penelitian/Luaran_lain');
    $this->load->model('luaran_penelitian/Penyelenggara_forum_ilmiah');
    $this->load->model('luaran_penelitian/Penelitian_hibah');
    $this->load->model('jenis_hki');
    $array_class = [jurnal::where('id','>','0'),jurnal::where('id','>','0'),jurnal::where('id','>','0'),jurnal::where('id','>','0'),buku_ajar::where('id','>','0'),forum_ilmiah::where('id','>','0'),forum_ilmiah::where('id','>','0'),forum_ilmiah::where('id','>','0'),hki::where('id','>','0')->with('jenis_hki'),luaran_lain::where('id','>','0'),penyelenggara_forum_ilmiah::where('id','>','0'),penelitian_hibah::where('id','>','0')];
    foreach($data as $key=>$value){
      foreach($value as $key2=>$x){
        if(strpos($key2, '_id' ) === false) continue;
        $check  = str_replace('_id', '', $key2);
        // echo $data[$key][$check];die;
        if($data[$key][$check]<$items['target_total'][$check]) continue;
        // echo json_encode($items['target_total'][$check]);die;
        $data[$key][$key2] = explode(',', $data[$key][$key2]);
        $class = clone $array_class[(int)str_replace("_id","",$key2)];
        $data[$key][$key2] = $class->whereIn('id',$data[$key][$key2])->get()->toArray();
      }
    }
    return $data;
  }

  private function querypotensi($items){
    $dari = $this->input->get('dari');
    $sampai = $this->input->get('sampai');
    $fakultas = $this->input->get('fakultas');
    $program_studi= $this->input->get('program_studi');
    $urutkan = $this->input->get('orderby');

    $where = "";
    $group = "";
    $nama = "";
    $order = "DESC";

    $where2 = "";
    if(($program_studi=="all" || $program_studi=="") && $fakultas ){
      $where2 = " AND fakultas.id=".$fakultas;
    }else if($fakultas && $program_studi){
      $where2 = " AND fakultas.id=".$fakultas." AND program_studi.id=".$program_studi;
    }else if($fakultas){
      $where2 = " AND fakultas.id=".$fakultas;
    }

    if(isset($urutkan)){
      $order = $urutkan;
    }

    $items["orderby"] = $order;

    if(isset($dari) && !empty($dari)){
      $dari = $dari.'-01-01 00:00:00';
      $dari = " AND created_at>='$dari'";
    }else{
      return;
    }
    if(isset($dari) && !empty($dari)){
      $sampai = $sampai.'-12-31 23:59:59';
      $sampai = " AND created_at<='$sampai'";
    }else{
      return;
    }

    // $where = "AND dosen.program_studi=$program_studi";
    $group = ',dosen.program_studi, dosen.id';
    $nama = ' dosen.nama, dosen.nidn, dosen.jenjang_pendidikan, program_studi.nama as program_studi, jabatan_akademik.nama as jabatan_akademik, jabatan_akademik.id as jabatan_akademik_id, SUBSTRING_INDEX(dosen.jenjang_pendidikan, \' \', 1) as tingkat, ';

    $sql = $this->sqlCompose($items,$dari,$sampai);
    $tampil = $sql["SELECT"];
    $join = $sql["JOIN"];
    $total = $sql["TOTAL"];
    $wherex = $sql["WHERE"];
    $syarat = $sql["WHERE2"];

    $query = "
      SELECT * FROM
        ( SELECT dosen.fakultas, $nama
            $tampil
            $total as total
        FROM dosen 
        LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
        LEFT JOIN jabatan_akademik ON jabatan_akademik.id=dosen.jabatan_akademik
        LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
        $join
        WHERE dosen.fakultas<>0 $where2 $where AND (1=2 $wherex)
        GROUP BY fakultas.nama $group
        ORDER BY total $order ) AS tmp
      -- WHERE 1=2 $syarat
      ";


    $allowedFunctions = array("fc");
    // echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
    $query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
         function ($m) {
              return $m[1]($m[2],$m[3]);
         }, $query);
    // echo $query; die;
    $data = DB::select($query);
    // echo json_encode($data); die;
    return $data;
  }

private function sqlCompose($data,$dari,$sampai){

    $join_array = [
      " LEFT JOIN jurnal AS jurnal_internasional ON jurnal_internasional.dosen=dosen.id AND jurnal_internasional.isdelete=0 AND jurnal_internasional.isvalidate=1 AND jurnal_internasional.type=1 fc($dari,jurnal_internasional) fc($sampai,jurnal_internasional)",
      " LEFT JOIN jurnal AS jurnal_nasionalakreditasi ON jurnal_nasionalakreditasi.dosen=dosen.id AND jurnal_nasionalakreditasi.isdelete=0 AND jurnal_nasionalakreditasi.isvalidate=1 AND jurnal_nasionalakreditasi.type=2 fc($dari,jurnal_nasionalakreditasi) fc($sampai,jurnal_nasionalakreditasi)",
      " LEFT JOIN jurnal AS jurnal_nasionaltakakreditasi ON jurnal_nasionaltakakreditasi.dosen=dosen.id AND jurnal_nasionaltakakreditasi.isdelete=0 AND jurnal_nasionaltakakreditasi.isvalidate=1 AND jurnal_nasionaltakakreditasi.type=3 fc($dari,jurnal_nasionaltakakreditasi) fc($sampai,jurnal_nasionaltakakreditasi)",
      " LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1  fc($dari,buku_ajar) fc($sampai,buku_ajar)",
      " LEFT JOIN forum_ilmiah AS forum_ilmiah_internasional ON forum_ilmiah_internasional.dosen = dosen.id AND forum_ilmiah_internasional.isdelete=0 AND forum_ilmiah_internasional.isvalidate=1 AND forum_ilmiah_internasional.tingkat=1 fc($dari,forum_ilmiah_internasional) fc($sampai,forum_ilmiah_internasional)",
      " LEFT JOIN forum_ilmiah AS forum_ilmiah_nasional ON forum_ilmiah_nasional.dosen = dosen.id AND forum_ilmiah_nasional.isdelete=0 AND forum_ilmiah_nasional.isvalidate=1 AND forum_ilmiah_nasional.tingkat=2 fc($dari,forum_ilmiah_nasional) fc($sampai,forum_ilmiah_nasional)",
      " LEFT JOIN forum_ilmiah AS forum_ilmiah_regional ON forum_ilmiah_regional.dosen = dosen.id AND forum_ilmiah_regional.isdelete=0 AND forum_ilmiah_regional.isvalidate=1 AND forum_ilmiah_regional.tingkat=3 fc($dari,forum_ilmiah_regional) fc($sampai,forum_ilmiah_regional)",
      " LEFT JOIN hki ON hki.dosen = dosen.id AND hki.isdelete=0 AND hki.isvalidate=1  fc($dari,hki) fc($sampai,hki)",
      " LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1 fc($dari,luaran_lain) fc($sampai,luaran_lain)",
      " LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen = dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1  fc($dari,penyelenggara_forum_ilmiah) fc($sampai,penyelenggara_forum_ilmiah)",
      " LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1  fc($dari,penelitian_hibah) fc($sampai,penelitian_hibah)"];

      $select_array = [
          "count(DISTINCT jurnal_internasional.id) as '1', ", 
          "count(DISTINCT jurnal_nasionalakreditasi.id) as '2', ", 
          "count(DISTINCT jurnal_nasionaltakakreditasi.id) as '3', ", 
          "count(DISTINCT buku_ajar.id) as '4', ", 
          "count(DISTINCT forum_ilmiah_internasional.id) as '5', ", 
          "count(DISTINCT forum_ilmiah_nasional.id) as '6', ", 
          "count(DISTINCT forum_ilmiah_regional.id) as '7', ", 
          "count(DISTINCT hki.id) as '8', ", 
          "count(DISTINCT luaran_lain.id) as '9', ", 
          "count(DISTINCT penyelenggara_forum_ilmiah.id) as '10', ", 
          "count(DISTINCT penelitian_hibah.id) as '11', "];

      $select_array2 = [
        "GROUP_CONCAT(jurnal_internasional.id) as 1_id,",
        "GROUP_CONCAT(jurnal_nasionalakreditasi.id) as 2_id,",
        "GROUP_CONCAT(jurnal_nasionaltakakreditasi.id) as 3_id,",
        "GROUP_CONCAT(buku_ajar.id) as 4_id,",
        "GROUP_CONCAT(forum_ilmiah_internasional.id) as 5_id,",
        "GROUP_CONCAT(forum_ilmiah_nasional.id) as 6_id,",
        "GROUP_CONCAT(forum_ilmiah_regional.id) as 7_id,",
        "GROUP_CONCAT(hki.id) as 8_id,",
        "GROUP_CONCAT(luaran_lain.id) as 9_id,",
        "GROUP_CONCAT(penyelenggara_forum_ilmiah.id) as 10_id,",
        "GROUP_CONCAT(penelitian_hibah.id) as 11_id,"
      ];

      $total_array = [
        "count(DISTINCT jurnal_internasional.id)",
        "count(DISTINCT jurnal_nasionalakreditasi.id)",
        "count(DISTINCT jurnal_nasionaltakakreditasi.id)",
        "count(DISTINCT buku_ajar.id)",
        "count(DISTINCT forum_ilmiah_internasional.id)",
        "count(DISTINCT forum_ilmiah_nasional.id)",
        "count(DISTINCT forum_ilmiah_regional.id)",
        "count(DISTINCT hki.id)",
        "count(DISTINCT luaran_lain.id)",
        "count(DISTINCT penyelenggara_forum_ilmiah.id)",
        "count(DISTINCT penelitian_hibah.id)"];

    $WHERE = "";
    $WHERE2 = "";
    foreach($data["syarat_total"] as $key=>$value){
      $syarat_kecil = "";
      foreach($value["items"] as $item=>$item_value){
        $syarat_kecil.= " OR tmp.".$item."_id>=".$item_value;
      }
      $WHERE.= " OR (dosen.jenjang_pendidikan LIKE '".$value["pendidikan"]."%' AND  dosen.jabatan_akademik=".$value["jabatan_akademik"].")";
      $WHERE2.= " OR (tmp.jenjang_pendidikan LIKE '".$value["pendidikan"]."%' AND  tmp.jabatan_akademik=".$value["jabatan_akademik"]." AND (1=2".$syarat_kecil."))";
    }

    $JOIN = "";
    $SELECT = "";
    $SELECT2 = "";
    $TOTAL ="";
    foreach($data["target_total"] as $key=>$value){
      $JOIN .= $join_array[(int)$key-1];
      $SELECT .= $select_array[(int)$key-1];
      $SELECT .= $select_array2[(int)$key-1];
      $TOTAL .=$total_array[(int)$key-1].'+';
    }
    $TOTAL = substr($TOTAL."0+0+", 0, -1);
    return array("JOIN"=>$JOIN,"SELECT"=>$SELECT,"SELECT2"=>$SELECT2,"TOTAL"=>$TOTAL,"WHERE"=>$WHERE,"WHERE2"=>$WHERE2);
  }

private function getSyaratItem(&$data){
  $dari = $this->input->get('dari');
  $sampai = $this->input->get('sampai');
  // $dari = (int) $dari;
  // $sampai = (int) $sampai;
  $datax= [];
  for($v=$dari;$v<=$sampai;$v++){
    $datax[]=$v;
  }

  $this->load->model('Petapotensi_syarat');
  $this->load->model('Petapotensi');
  $item = Petapotensi_syarat::whereHas('peta_potensi',function($q) use ($datax) {
    $q->whereHas('tahun',function($qs) use ($datax){
      $qs->whereIn('tahun',$datax);
    });
  })->get();
  $item->load('peta_potensi');
  // echo $item->toJson(); die;
  $syarat = [];
  $syarat_total = [];
  $target_total = [];
  foreach($item as $value){
    $target = [];
    for($i=0;$i<count($value->jumlah);$i++){
      $target[$value->luaran[$i]] = $value->jumlah[$i];
      if(array_key_exists($value->luaran[$i],$target_total))
        $target_total[$value->luaran[$i]] += (int)$value->jumlah[$i];
      else $target_total[$value->luaran[$i]] = (int)$value->jumlah[$i];
    }
    $syarat[] = array("pendidikan"=>$value->syarat,"jabatan_akademik"=>$value->jabatan_akademik,'tahun'=>$value->peta_potensi()->first()->tahun()->first()->tahun, "target" => $target);
    $t = [];
    for($i=0;$i<count($value->jumlah);$i++){
      $t[$value->luaran[$i]] = $value->jumlah[$i];
    }
    $syarat_total[] = array("pendidikan"=>$value->syarat,"jabatan_akademik"=>$value->jabatan_akademik,"items"=>$t);
  }
  ksort($target_total);
  // echo json_encode($syarat_total); die;
  $data["target_total"] = $target_total;
  $data["syarat_total"] = $syarat_total;
  $data["luaran_key"] = ["Publikasi Jurnal - Internasional","Publikasi Jurnal - Nasional Terakreditasi","Publikasi Jurnal - Nasional Tidak Terakreditasi","Buku Ajar/Teks","Pemakalah Forum Ilmiah - Internasional","Pemakalah Forum Ilmiah - Nasional","Pemakalah Forum Ilmiah - Regional","Hak Kekayaan Intelektual","Luaran Lain","Penyelenggara Forum Ilmiah","Penelitian Mandiri"];
  return $syarat;
}


private function compose_pdf($isi,$title){
    ini_set('memory_limit','640M');
    try{
      $this->load->helper('pdf_helper');
      $this->load->helper('url');
      tcpdf();
      $obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      $obj_pdf->SetCreator(PDF_CREATOR);
      // $title = "PDF Report";
      $obj_pdf->SetTitle($title);
      // $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
      // $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      // $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
      $obj_pdf->SetDefaultMonospacedFont('helvetica');
      $obj_pdf->SetHeaderMargin(0);
      $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
      $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+5, PDF_MARGIN_RIGHT);
      $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
      $obj_pdf->SetFont('helvetica', '', 9);
      $obj_pdf->setFontSubsetting(false);
      $obj_pdf->setListIndentWidth(1);
      $obj_pdf->AddPage();


      ob_start();
      echo $isi;
      $content = ob_get_contents();
      ob_end_clean();
      $obj_pdf->writeHTML($content, true, false, true, false, '');
      $obj_pdf->Output($title,'I');
      echo $title;
      // if($hasil=="hasil") $obj_pdf->Output('review_penelitian_'.$data['dosen']['nama'].'.pdf', 'I');
      // else  $obj_pdf->Output('reviewer_'.$data['hasil']['dosen']['nama'].'.pdf', 'I');
      return $obj_pdf;
    }catch (Exception $e) {
      echo 'PDF to big, not enough memory';
    }
  }












}




 ?>
