<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
use Illuminate\Database\Capsule\Manager as DB;
class Petapenelitian extends MY_BaseController {

  private $menu = array("petapotensi"=>"active");
  public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config);
        $this->load->model("Dosen");
        $this->load->model("Akses");
        $this->load->model("Tahun_kegiatan");
        $this->load->model("Fakultas");
        $this->load->model("Petapotensi");
        $this->load->helper('report');
  }

  public function index(){
    $data = [];
    $data["tahuns"] = Tahun_kegiatan::where('isdelete','=','0')->whereHas('petapotensi',function($q){
                     })->orderBy('tahun','desc')->get()->toArray();
    $data["fakultas"] = Fakultas::where('isdelete','=','0')->get()->toArray();
    $data["syarat"] = $this->getSyaratItem($data);
    $result = $this->getFakultasData($data);
    $data["chart"] = $this->dosenKomposisi($data);
    $data["potensichart"] = $this->getLuaranData($data);
    // echo json_encode($result); die;
    $data["items"] = $result;
    // echo json_encode($data['potensichart']); die;
    $this->twig->display('petapotensi/index',array_merge($this->menu,$data));
  }

  private function getSyaratItem(&$data){
    $dari = $this->input->get('dari');
    $sampai = $this->input->get('sampai');
    $fakultas = $this->input->get('fakultas');
    $program_studi = $this->input->get('program_studi');
    // $dari = (int) $dari;
    // $sampai = (int) $sampai;
    $datax= [];
    for($v=$dari;$v<=$sampai;$v++){
      $datax[]=$v;
    }

    $this->load->model('Petapotensi_syarat');
    $this->load->model('Petapotensi');
    $item = Petapotensi_syarat::whereHas('peta_potensi',function($q) use ($datax) {
      $q->whereHas('tahun',function($qs) use ($datax){
        $qs->whereIn('tahun',$datax);
      });
    })->get();
    $item->load('peta_potensi');
    // echo $item->toJson(); die;
    $syarat = [];
    $syarat_total = [];
    $target_total = [];
    foreach($item as $value){
      $target = [];
      $syarat[] = array("pendidikan"=>$value->syarat,"jabatan_akademik"=>$value->jabatan_akademik,'tahun'=>$value->peta_potensi()->first()->tahun()->first()->tahun, "target" => $target);
      $syarat_total[] = array("pendidikan"=>$value->syarat,"jabatan_akademik"=>$value->jabatan_akademik);
      $dosen = Dosen::whereHas('akses',function($q){
          $q->where('nama','=','Dosen');
        })->where(function($q) use ($value,$fakultas,$program_studi){
              $q->where('jenjang_pendidikan','LIKE',$value->syarat.'%')->where('jabatan_akademik','=',$value->jabatan_akademik);
              if($fakultas){
                $q->where('fakultas','=',$fakultas);
              }
              if($program_studi and $program_studi!='all'){
                $q->where('program_studi','=',$program_studi);
              }
        })->count();
      for($i=0;$i<count($value->jumlah);$i++){
        $target[$value->luaran[$i]] = $dosen*$value->jumlah[$i];
        if(array_key_exists($value->luaran[$i],$target_total))
          $target_total[$value->luaran[$i]] += $dosen*(int)$value->jumlah[$i];
        else $target_total[$value->luaran[$i]] = $dosen*(int)$value->jumlah[$i];
      }
    }
    // die;
    ksort($target_total);
    // $dosen = Dosen::whereHas('akses',function($q){
    //   $q->where('nama','=','Dosen');
    // })->where(function($q) use ($syarat_total){
    //   foreach($syarat_total as $item){
    //     $q->orWhere(function($qs) use ($item){
    //       $qs->where('jenjang_pendidikan','LIKE',$item['pendidikan'].'%')->where('jabatan_akademik','=',$item['jabatan_akademik']);
    //     });
    //   }
    // })->count();
    // echo $dosen; die;
    // $data["target"] = $target_total;
    // foreach($target_total as &$item){
    //   $item = $dosen;
    // }
    // echo json_encode($target_total); die;
    $data["target_total"] = $target_total;
    $data["syarat_total"] = $syarat_total;
    $data["luaran_key"] = ["Publikasi Jurnal - Internasional","Publikasi Jurnal - Nasional Terakreditasi","Publikasi Jurnal - Nasional Tidak Terakreditasi","Buku Ajar/Teks","Pemakalah Forum Ilmiah - Internasional","Pemakalah Forum Ilmiah - Nasional","Pemakalah Forum Ilmiah - Regional","Hak Kekayaan Intelektual","Luaran Lain","Penyelenggara Forum Ilmiah","Penelitian Mandiri"];
    return $syarat;
  }

  public function getFakultasData(&$items){
    $dari = $this->input->get('dari');
    $sampai = $this->input->get('sampai');
    $fakultas = $this->input->get('fakultas');
    $program_studi= $this->input->get('program_studi');
    $urutkan = $this->input->get('orderby');

    // $dari = Tahun_kegiatan::where('isdelete','=','0')->where('id','=',$dari)->first()->tahun;
    // $sampai = Tahun_kegiatan::where('isdelete','=','0')->where('id','=',$sampai)->first()->tahun;

    $where = "";
    $group = "";
    $nama = "";
    $order = "DESC";

    $fakultasx = "";
    if(!empty($fakultas)){
      $fakultasx=" AND dosen.fakultas=".$fakultas;
    }

    if(isset($urutkan)){
      $order = $urutkan;
    }

    $items["orderby"] = $order;

    if(isset($dari) && !empty($dari)){
      $dari = $dari.'-01-01 00:00:00';
      $dari = " AND created_at>='$dari'";
    }else{
      return;
    }
    if(isset($dari) && !empty($dari)){
      $sampai = $sampai.'-12-31 23:59:59';
      $sampai = " AND created_at<='$sampai'";
    }else{
      return;
    }

    if($program_studi=="all"){
      $group = ',dosen.program_studi';
      $nama = ' program_studi.nama, ';
    }else if($program_studi!="all" && isset($program_studi)){
      $where = "AND dosen.program_studi=$program_studi";
      // $group = ',dosen.program_studi, dosen.id';
      $nama = ' program_studi.nama, ';
    }else if($fakultas){
      $where = "AND dosen.fakultas=$fakultas";
      $nama = " program_studi.id as id_prodi, ";
    }
    // $where = "AND dosen.program_studi=$program_studi";
    // $group = ',dosen.program_studi, dosen.id';
    // $nama = ' dosen.nama, dosen.nidn, dosen.jenjang_pendidikan,';

    $sql = $this->sqlCompose($items,$dari,$sampai);
    $tampil = $sql["SELECT"];
    $join = $sql["JOIN"];

    $query = "SELECT fakultas.id, dosen.fakultas, fakultas.nama,dosen.program_studi, $nama
          $tampil
          -- count(DISTINCT jurnal_nasionaltakakreditasi.id)+count(DISTINCT jurnal_internasional.id)+count(DISTINCT jurnal_nasionalakreditasi.id)+count(DISTINCT buku_ajar.id)+count(DISTINCT forum_ilmiah_internasional.id)+count(DISTINCT forum_ilmiah_nasional.id)+count(DISTINCT forum_ilmiah_regional.id)+count(DISTINCT hki.id)+count(DISTINCT luaran_lain.id)+count(DISTINCT penyelenggara_forum_ilmiah.id)+count(DISTINCT penelitian_hibah.id) as total
          0 as total
      FROM dosen 
      LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
      LEFT JOIN jabatan_akademik ON jabatan_akademik.id=dosen.jabatan_akademik
      LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
      $join
      WHERE dosen.fakultas<>0 $fakultasx $where
      GROUP BY fakultas.nama $group
      ORDER BY total $order";

    $allowedFunctions = array("fc");
    // echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
    $query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
         function ($m) {
              return $m[1]($m[2],$m[3]);
         }, $query);
    // echo $query; die;
    $data = DB::select($query);
    // echo json_encode($data); die;
    return $data;
  }

  public function getLuaranData(&$items){
    $dari = $this->input->get('dari');
    $sampai = $this->input->get('sampai');
    $fakultas = $this->input->get('fakultas');
    $program_studi= $this->input->get('program_studi');
    $urutkan = $this->input->get('orderby');

    $where = "";
    $group = "";
    $nama = "";
    $order = "DESC";

    $where2 = "";
    if(($program_studi=="all" || $program_studi=="") && $fakultas ){
      $where2 = " AND fakultas.id=".$fakultas;
    }else if($fakultas && $program_studi){
      $where2 = " AND fakultas.id=".$fakultas." AND program_studi.id=".$program_studi;
    }else if($fakultas){
      $where2 = " AND fakultas.id=".$fakultas;
    }

    if(isset($urutkan)){
      $order = $urutkan;
    }

    $items["orderby"] = $order;

    if(isset($dari) && !empty($dari)){
      $dari = $dari.'-01-01 00:00:00';
      $dari = " AND created_at>='$dari'";
    }else{
      return;
    }
    if(isset($dari) && !empty($dari)){
      $sampai = $sampai.'-12-31 23:59:59';
      $sampai = " AND created_at<='$sampai'";
    }else{
      return;
    }

    $sql = $this->sqlCompose($items,$dari,$sampai);
    $tampil = $sql["SELECT"];
    $join = $sql["JOIN"];
    $total = $sql["TOTAL"];
    $where = $sql["WHERE"];

    $query = "SELECT 'aa' as test, dosen.nama as dosen, dosen.fakultas, fakultas.nama,dosen.program_studi, $nama
          $tampil
          $total as total
      FROM dosen 
      LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
      LEFT JOIN jabatan_akademik ON jabatan_akademik.id=dosen.jabatan_akademik
      LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
      $join
      WHERE dosen.fakultas<>0 $where2  AND (1=2 $where)
      GROUP BY test
      ORDER BY total $order";
    
    $allowedFunctions = array("fc");
    // echo "~(".implode("|",$allowedFunctions).")\(.*\)~";
    $query = preg_replace_callback("~(".implode("|",$allowedFunctions).")\((.*?),(.*?)\)~", 
         function ($m) {
              return $m[1]($m[2],$m[3]);
         }, $query);
    // echo $query; die;
    $datas = DB::select($query);
    // $dosennya = [];
    // foreach($items["target"] as $key=>$item){ 
    //   $dosennya[$key] = 0;
    // }
    // foreach($datas as $data){
    //   foreach($items["target"] as $key=>$item){
    //     if($data[$key]==$item){
    //       $dosennya[$key] += 1;
    //     }
    //   }
    // }
    // echo json_encode($items); die;
    // echo json_encode($datas[0]); die;
    return $datas[0];
  }

private function sqlCompose($data,$dari,$sampai){

    $join_array = [
      " LEFT JOIN jurnal AS jurnal_internasional ON jurnal_internasional.dosen=dosen.id AND jurnal_internasional.isdelete=0 AND jurnal_internasional.isvalidate=1 AND jurnal_internasional.type=1 fc($dari,jurnal_internasional) fc($sampai,jurnal_internasional)",
      " LEFT JOIN jurnal AS jurnal_nasionalakreditasi ON jurnal_nasionalakreditasi.dosen=dosen.id AND jurnal_nasionalakreditasi.isdelete=0 AND jurnal_nasionalakreditasi.isvalidate=1 AND jurnal_nasionalakreditasi.type=2 fc($dari,jurnal_nasionalakreditasi) fc($sampai,jurnal_nasionalakreditasi)",
      " LEFT JOIN jurnal AS jurnal_nasionaltakakreditasi ON jurnal_nasionaltakakreditasi.dosen=dosen.id AND jurnal_nasionaltakakreditasi.isdelete=0 AND jurnal_nasionaltakakreditasi.isvalidate=1 AND jurnal_nasionaltakakreditasi.type=3 fc($dari,jurnal_nasionaltakakreditasi) fc($sampai,jurnal_nasionaltakakreditasi)",
      " LEFT JOIN buku_ajar ON buku_ajar.dosen = dosen.id AND buku_ajar.isdelete=0 AND buku_ajar.isvalidate=1  fc($dari,buku_ajar) fc($sampai,buku_ajar)",
      " LEFT JOIN forum_ilmiah AS forum_ilmiah_internasional ON forum_ilmiah_internasional.dosen = dosen.id AND forum_ilmiah_internasional.isdelete=0 AND forum_ilmiah_internasional.isvalidate=1 AND forum_ilmiah_internasional.tingkat=1 fc($dari,forum_ilmiah_internasional) fc($sampai,forum_ilmiah_internasional)",
      " LEFT JOIN forum_ilmiah AS forum_ilmiah_nasional ON forum_ilmiah_nasional.dosen = dosen.id AND forum_ilmiah_nasional.isdelete=0 AND forum_ilmiah_nasional.isvalidate=1 AND forum_ilmiah_nasional.tingkat=2 fc($dari,forum_ilmiah_nasional) fc($sampai,forum_ilmiah_nasional)",
      " LEFT JOIN forum_ilmiah AS forum_ilmiah_regional ON forum_ilmiah_regional.dosen = dosen.id AND forum_ilmiah_regional.isdelete=0 AND forum_ilmiah_regional.isvalidate=1 AND forum_ilmiah_regional.tingkat=3 fc($dari,forum_ilmiah_regional) fc($sampai,forum_ilmiah_regional)",
      " LEFT JOIN hki ON hki.dosen = dosen.id AND hki.isdelete=0 AND hki.isvalidate=1  fc($dari,hki) fc($sampai,hki)",
      " LEFT JOIN luaran_lain ON luaran_lain.dosen = dosen.id AND luaran_lain.isdelete=0 AND luaran_lain.isvalidate=1 fc($dari,luaran_lain) fc($sampai,luaran_lain)",
      " LEFT JOIN penyelenggara_forum_ilmiah ON penyelenggara_forum_ilmiah.dosen = dosen.id AND penyelenggara_forum_ilmiah.isdelete=0 AND penyelenggara_forum_ilmiah.isvalidate=1  fc($dari,penyelenggara_forum_ilmiah) fc($sampai,penyelenggara_forum_ilmiah)",
      " LEFT JOIN penelitian_hibah ON penelitian_hibah.dosen = dosen.id AND penelitian_hibah.isdelete=0 AND penelitian_hibah.isvalidate=1  fc($dari,penelitian_hibah) fc($sampai,penelitian_hibah)"];

      $select_array = [
          "count(DISTINCT jurnal_internasional.id) as '1', ", 
          "count(DISTINCT jurnal_nasionalakreditasi.id) as '2', ", 
          "count(DISTINCT jurnal_nasionaltakakreditasi.id) as '3', ", 
          "count(DISTINCT buku_ajar.id) as '4', ", 
          "count(DISTINCT forum_ilmiah_internasional.id) as '5', ", 
          "count(DISTINCT forum_ilmiah_nasional.id) as '6', ", 
          "count(DISTINCT forum_ilmiah_regional.id) as '7', ", 
          "count(DISTINCT hki.id) as '8', ", 
          "count(DISTINCT luaran_lain.id) as '9', ", 
          "count(DISTINCT penyelenggara_forum_ilmiah.id) as '10', ", 
          "count(DISTINCT penelitian_hibah.id) as '11', "];

      $total_array = [
        "count(DISTINCT jurnal_internasional.id)",
        "count(DISTINCT jurnal_nasionalakreditasi.id)",
        "count(DISTINCT jurnal_nasionaltakakreditasi.id)",
        "count(DISTINCT buku_ajar.id)",
        "count(DISTINCT forum_ilmiah_internasional.id)",
        "count(DISTINCT forum_ilmiah_nasional.id)",
        "count(DISTINCT forum_ilmiah_regional.id)",
        "count(DISTINCT hki.id)",
        "count(DISTINCT luaran_lain.id)",
        "count(DISTINCT penyelenggara_forum_ilmiah.id)",
        "count(DISTINCT penelitian_hibah.id)"];

    $WHERE = "";
    foreach($data["syarat_total"] as $key=>$value){
      $WHERE.= " OR (dosen.jenjang_pendidikan LIKE '".$value["pendidikan"]."%' AND  dosen.jabatan_akademik=".$value["jabatan_akademik"].")";
    }

    $JOIN = "";
    $SELECT = "";
    $SELECT2 = "";
    $TOTAL ="";
    foreach($data["target_total"] as $key=>$value){
      $JOIN .= $join_array[(int)$key-1];
      $SELECT .= $select_array[(int)$key-1];
      $TOTAL .=$total_array[(int)$key-1].'+';
    }
    $TOTAL = substr($TOTAL."0+0+", 0, -1);
    return array("JOIN"=>$JOIN,"SELECT"=>$SELECT,"SELECT2"=>$SELECT2,"TOTAL"=>$TOTAL,"WHERE"=>$WHERE);
  }

  private function dosenKomposisi(&$data){

    $fakultas = $this->input->get('fakultas');
    $program_studi= $this->input->get('program_studi');

    $where = "";
    foreach($data["syarat_total"] as $key=>$value){
      $where.= " OR (jenjang_pendidikan LIKE '".$value["pendidikan"]."%' AND  dosen.jabatan_akademik=".$value["jabatan_akademik"].")";
    }
    $where2 = "";
    if(($program_studi=="all" || $program_studi=="") && $fakultas ){
      $where2 = " AND fakultas.id=".$fakultas;
    }else if($fakultas && $program_studi){
      $where2 = " AND fakultas.id=".$fakultas." AND program_studi.id=".$program_studi;
    }else if($fakultas){
      $where2 = " AND fakultas.id=".$fakultas;
    }

    $select = "
    SELECT nama, tingkat, count(tingkat) as total
    FROM
      (SELECT SUBSTRING_INDEX(dosen.jenjang_pendidikan, ' - ', 1) AS tingkat, jabatan_akademik.nama
      FROM dosen
      LEFT JOIN jabatan_akademik ON jabatan_akademik.id=dosen.jabatan_akademik
      LEFT JOIN fakultas ON fakultas.id=dosen.fakultas
      LEFT JOIN program_studi ON program_studi.id = dosen.program_studi
      WHERE (1=2 $where) $where2 ) as dosen
      GROUP BY nama, tingkat";
    $items = DB::select($select);
    $komposisi = [];
    $array = ["Professor","Lektor Kepala","Lektor","Asisten Ahli","Tenaga Pengajar"];
    foreach($items as $key=>$value){
      $komposisi[$value["tingkat"]][]=array("jabatan"=>$value["nama"],"jumlah"=>$value["total"],"index"=>array_search($value["nama"], $array));
    }
    // echo $select; die;
    foreach($komposisi as $key=>$value){
      // ksort($value);
      $komposisi[$key] = $this->msort($value,"index");
    }
    // echo json_encode($komposisi); die;
    $data["klasifikasi"] = $komposisi;
    return $data;
  }

  function msort($array, $id="id") {
    $temp_array = array();
    while(count($array)>0) {
        $lowest_id = 0;
        $index=0;
        foreach ($array as $item) {
            if ($item[$id]<$array[$lowest_id][$id]) {
                $lowest_id = $index;
            }
            $index++;
        }
        $temp_array[] = $array[$lowest_id];
        $array = array_merge(array_slice($array, 0,$lowest_id), array_slice($array, $lowest_id+1));
    }
    return $temp_array;
  }
  // private function getDosen

















}




 ?>
