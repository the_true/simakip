<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Pengguna extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/

	 private $menu = array("pengguna"=>"active","pengguna"=>"active");
	 private $gaksnonlembaga = array("Dosen","Wakil Rektor","Rektor","Keuangan");
	 private $gakslppm = array("Operator LPPM","Sekretaris LPPM","Ketua LPPM");
	 private $gakslemlit = array("Operator Lemlitbang","Sekretaris Lemlitbang","Ketua Lemlitbang");
    public function __construct() {
		$config = [
			'functions' => ['set_radio','set_select'],
		    'functions_safe' => ['validation_errors_array','form_open_multipart'],
		];
        parent::__construct($config,False);
        $this->load->helper('security');
        $this->load->model('Dosen');
        $this->load->model('Jabatan_akademik');
        $this->load->model('Jabatan_fungsi');
        $this->load->model('Fakultas');
        $this->load->model('Program_studi');
		$this->load->model('Akses'); 
        $this->load->model('Bidangkeahlian');
        $this->load->model('Subbidangkeahlian');
        $this->load->model('Pendidikan');
        $this->twig->addGlobal('session', $this->session);
    }

	public function index()
	{
		if(!$this->isOperator()) return;
		$data = [];
		$dosen = Dosen::where('isdelete','=','0');
		$dosen = $this->filter($dosen,$data);
 		$info = $this->create_paging($dosen);
 		$dosen = $dosen->take($info["limit"])->skip($info["skip"])->get();

		$dosen->load('Jabatan_akademik');
		$dosen->load('Jabatan_fungsional');
		$dosen->load('Fakultas');
		$dosen->load('Program_studi');
		$dosen->load('Akses');
		$data["items"] = $dosen->toArray();
		$data["fakultas"] = Fakultas::where('isdelete','=','0')->get()->toArray();

		$data = array_merge($data,$info,$this->menu);


		$this->twig->display('pengguna/index',$data);
	}

	public function add($iddosen=null)
	{ 
		$this->is_login();
		$id = $this->uri->segment(3);
		if(!$this->isPengguna($id)) return; 
		 
		$this->load->helper('form');
		$this->load->library('form_validation');
        // $this->form_validation->set_message('validate_bidangkeahlian','%s already exists');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'required'); 
		$this->form_validation->set_rules('asalpendidikan', 'Asal Pendidikan', 'required'); 
		$this->form_validation->set_rules('tahun', 'Tahun', 'required'); 
        if ($this->form_validation->run() == FALSE){    
			$dosen = Dosen::where('isdelete','=','0')->whereId($id)->get();  
			$dosen =$dosen->toArray()[0]; 
			if(!$this->isOperator()) $dosen['operator']=false;
			if($this->isOperator()) $dosen['operator']=true;
			$pendidikan = Pendidikan::where('isdelete','=','0')->where('dosen','=',$id)->get();  
			// print_r($pendidikan->toArray());die();
            // $info = $this->create_paging($dosen);
            // $dosen = $dosen->take($info["limit"])->skip($info["skip"])->get(); 
			// $this->twig->display('pengaturan/akademik/bidangkeahlian',array_merge($info,$this->menu,array("items"=>$item->toArray())));
			$this->twig->display('pengguna/add',array("res"=>$dosen,"pendidikan"=>$pendidikan->toArray()));
        }else{ 
        	$pendidikan = new Pendidikan; 
            $pendidikan->pendidikan = $this->input->post('pendidikan');
            $pendidikan->asalpendidikan = $this->input->post('asalpendidikan');
            $pendidikan->tahun = $this->input->post('tahun'); 
            $pendidikan->dosen = $iddosen; 
        	$pendidikan->save();
            Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'Pendidikan '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat Pendidikan baru dengan nama '.$pendidikan->pendidikan,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);
			$this->session->set_flashdata('success', 'Save pendidikan dosen berhasil!');
			redirect('pengguna/add/'.$iddosen);
        } 
	}

	public function filter($dosen,&$data){
		$nidn = $this->input->get('nidn');
		if($nidn) $dosen = $dosen->where('nidn','=',$nidn);

		$nama = $this->input->get('nama');
		if($nama) $dosen = $dosen->where('nama','LIKE',"%$nama%");

		$pendidikan = $this->input->get('pendidikan');
		if($pendidikan) $dosen = $dosen->where('jenjang_pendidikan','LIKE',"$pendidikan%");

		$akses = $this->input->get('akses');
		if($akses) $dosen = $dosen->whereHas('akses', function($q){
			$q->where('nama','LIKE', $this->input->get('akses')."%");
		});

		// echo $dosen->toSql(); die;

		$array_reviewer = $this->input->get('reviewer[]');
		$count_reviewer = count($array_reviewer);
		$real_reviewer = null;
		if($count_reviewer==1){
			$real_reviewer=$array_reviewer[0];
		}else if($count_reviewer==2){
			$real_reviewer=3;
		}
		if($real_reviewer) $dosen = $dosen->where('isreviewer','=',(int)$real_reviewer)->orWhere('isreviewer','=',3);

		$ketuaprodi = $this->input->get('ketuaprodi');
		if($ketuaprodi) $dosen = $dosen->where('isketuaprodi','=',(int)$ketuaprodi -1);

		$fakultas = $this->input->get('fakultas');
		if($fakultas) $dosen = $dosen->where('fakultas','=',$fakultas);

		return $dosen;
	}

	public function indexlist(){
		$this->is_login();
		$default_view = 10;
		$page = 0;

		$nidn_get = $this->input->get("nidn");
		$nama_get = $this->input->get("nama");
		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		$data = array("login"=>True);

		if(isset($page_get)) $page = $page_get-1;
		if(isset($nidn_get) && $nidn_get!='' && is_numeric($nidn_get)){
			$dosen = Dosen::where('isdelete','=','0');
			$dosen->where('nidn','=',$nidn_get);
		}
		if(isset($nama_get) && $nama_get!=''){
			$dosen = Dosen::where('isdelete','=','0');
			$dosen->where("nama","LIKE","%".$nama_get."%");
		}
		if(isset($view_get)) $default_view = $view_get;
		if((isset($nidn_get)&&$nidn_get!='')||(isset($nama_get)&&$nama_get!='')){
			$count = $dosen->count();
			$dosen = $dosen->get();

			$dosen->load('Jabatan_akademik');
			$dosen->load('Jabatan_fungsional');
			$dosen->load('Fakultas');
			$dosen->load('Program_studi');
			$data =array("items"=>$dosen->toArray(),"login"=>True);
			$data['views'] = $default_view;
			$data['page'] = $page+1;
			$data['total_pages'] = $count>0? ceil($count/$default_view):1;
		}

		$this->load->helper('report');
		$data["point_fakultas"] = getPointFakultas();
		$data["point_prostud"] = getPointProstud();
		$data["point_dosen"] = getPointDosen();
		$data["jabatan_akademik"] = getJabatanAkademik();
		$data["jenjang_pendidikan"]= getPendidikanDosen();
		$data["jabatan_fungsional"] = getJabatanFungsi();

		$this->twig->display('pengguna/list',$data);
	}

	private function generate_captcha(){
	      $random_number = substr(number_format(time() * rand(),0,'',''),0,4);
	      // setting up captcha config
	      $vals = array(
	             'word' => $random_number,
	             'font_size' => 5,
	             'img_path' => './captcha/',
	             'img_url' => base_url().'captcha/',
	             'img_width' => 94,
	             'img_height' => 33,
	             'expiration' => 7200
	            ); 
	      $data = create_captcha($vals);
	      $this->session->set_userdata('captchaWord',$data['word']);
	      return $data;
	}


	private function validation($edit=False){
		$this->form_validation->set_message('nidn_verification','%s already exists');
		$this->form_validation->set_message('nidn_zerocheck','%s 0 can\'t be in the front');
		$this->form_validation->set_rules('nidn', 'NIDN', 'required|integer|callback_nidn_verification');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
		// $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		// $this->form_validation->set_rules('tingkat', 'Tingkat Pendidikan', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan Pendidikan', 'required');
		$this->form_validation->set_rules('jabatan_akademik', 'Jabatan Akademik', 'required');
		// $this->form_validation->set_rules('jabatan_fungsional', 'Jabatan Fungsional', 'required');
		// $this->form_validation->set_rules('fakultas', 'Fakultas', 'required');
		$this->form_validation->set_rules('program_studi', 'Nama Program Studi', 'required');
		// $this->form_validation->set_rules('hp', 'No. HP', 'required');
		$this->form_validation->set_rules('surel', 'Surel', 'required|valid_email');
		if(!$edit){
			$this->form_validation->set_rules('status_dosen', 'Status Dosen', 'required');
			$this->form_validation->set_rules('status_data', 'Status Data', 'required');
			$this->form_validation->set_rules('akses', 'Hak Akses', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|matches[konfirmasi]');
			$this->form_validation->set_rules('konfirmasi', 'Konfirmasi Password', 'required');
			$this->form_validation->set_rules('passworddikti', 'Password Dikti', 'required');
		}

	}

	public function nidn_zerocheck($nidn){
		if($nidn[0]=='0') return False;
		return True;
	}

	public function nidn_verification($nidn){
		$id = $this->uri->segment(3);
        $item = Dosen::where('nidn','=',$nidn)->where('id','!=',$id)->count();
        if($item>0)
            return False;
        return True;
	}

	public function show(){
        $this->load->model('Dosen');
        $this->load->model('Program_studi');
        $this->load->model('Fakultas');
        $this->load->model('Jurnal_anggota');
        $this->load->model('Forum_ilmiah_anggota');

        $this->load->model('luaran_penelitian/Jurnal');
        $this->load->model('luaran_penelitian/Hki');
        $this->load->model('luaran_penelitian/Forum_ilmiah');
        $this->load->model('luaran_penelitian/Buku_ajar');
        $this->load->model('luaran_penelitian/Penyelenggara_forum_ilmiah');
        $this->load->model('luaran_penelitian/Penelitian_hibah');
        $this->load->model('luaran_penelitian/Luaran_lain');

        $this->load->model('Jenis_hki');
        $this->load->model('Jenis_luaran');
        $this->load->model('Tahun_kegiatan');


		$id = $this->uri->segment(3);
		$dosens = Dosen::where('id','=',$id)->where('isdelete','=','0')->get();
		$dosens->load('jabatan_akademik')->load('jabatan_fungsional')->load('fakultas')->load('program_studi');
		// $dosens->load('jurnal')->load('jurnal.personil');
		// $dosens->load('forum_ilmiah')->load('forum_ilmiah.tahun_kegiatan');
		$dosens->load('buku_ajar')->load('buku_ajar.tahun_kegiatan');
		$dosens->load('penyelenggara_forum_ilmiah')->load('penyelenggara_forum_ilmiah.tahun_kegiatan');
		$dosens->load('luaran_lain')->load('luaran_lain.jenis_luaran')->load('luaran_lain.tahun_kegiatan');
		$dosens->load('penelitian_hibah')->load('penelitian_hibah.tahun');
		$dosens->load('hki')->load('hki.jenis_hki')->load('hki.tahun_kegiatan');

		$jurnal = Jurnal::where('isdelete','=','0')->where('isvalidate','=','1')->whereHas("jurnal_anggota", function($q) use ($id) {
			$q->where('dosen','=',$id);
		})->with('tahun_kegiatan')->get();

		$forum_ilmiah = Forum_ilmiah::where('isdelete','=','0')->whereHas("forum_ilmiah_anggota", function($q) use ($id) {
			$q->where('dosen','=',$id);
		})->get();

		$dosen = $dosens->toArray()[0];
		$dosen["jurnal"] = $jurnal->toArray();
		$dosen["forum_ilmiah"] = $forum_ilmiah->toArray();
		// echo $jurnal->toJson(); die;
       	$this->twig->display('/pengguna/show',$dosen);
	}

	public function profile(){
		$this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'ID', 'required');
		if (empty($_FILES['file']['name']))
		{
		    $this->form_validation->set_rules('file', 'Photo', 'required');
		}

       	if ($this->form_validation->run() == FALSE){
       		$user = Dosen::find($this->is_login()["id"]);
       		$user->load("program_studi");
       		$user->load("fakultas");
       		$user->load("jabatan_fungsional");
       		$user->load("jabatan_akademik");
			$user->load("subbidangkeahlian"); 
			$user=$user->toArray(); 
			$bk = Bidangkeahlian::where('id','=',$user['subbidangkeahlian']['bidangkeahlian'])->get();
			$user['bidangkeahlian'] = $bk->toArray();
       		$this->load->helper('report');
			$user["point_dosen"] = getPointDosen($user["id"]); 
	       	$this->twig->display('/pengguna/profile',$user);
        }else{
        	if ($this->upload_photo()){
        		$this->refresh_session();
	       		$user = Dosen::find($this->is_login()["id"]);
	       		$user->load("program_studi");
	       		$user->load("fakultas");
	       		$user->load("jabatan_fungsional");
	       		$user->load("jabatan_akademik");
	       		$user=$user->toArray();
	       		$this->load->helper('report');
	       		$user["point_dosen"] = getPointDosen($user["id"]);
		       	$this->twig->display('/pengguna/profile',$user);
        	}
        	else echo $this->upload->display_errors();
		}
	}

	private function upload_photo(){
		$this->generate_folder('uploads/user_photo');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/user_photo'),
			'allowed_types' => 'jpg|png|gif',
			'max_size' => 2000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		$user = $this->is_login();
		if($uploaded){
			$dosen = Dosen::find($user["id"]);
			$dosen->photo = $this->upload->data('file_name');
			$dosen->save();
			return TRUE;
		}
	}

	private function generate_folder($folder_name){
		if(!is_dir($folder_name))
		{
		   mkdir($folder_name,0777, true);
		}
	}

	public function register(){
		if(!$this->isOperator()) return;
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

		$jab_ak = Jabatan_akademik::all();
		$jab_fu = Jabatan_fungsi::all();
		$fak = Fakultas::all();
		$pro_stu = Program_studi::all();
		$akslemlit = Akses::WhereIn("nama",["Operator Lemlitbang","Sekretaris Lemlitbang","Ketua Lemlitbang"])->get();
		$akslppm = Akses::WhereIn("nama",["Operator LPPM","Sekretaris LPPM","Ketua LPPM"])->get();
		$aks = Akses::WhereIn("nama",["Dosen","Wakil Rektor","Rektor","Keuangan"])->get();

		$data = array("jab_ak"=>$jab_ak->toArray(),"jab_fu"=>$jab_fu->toArray(),
			'fak'=>$fak->toArray(),'pro_stu'=>$pro_stu->toArray(),"aks"=>$aks->toArray());

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('register',$data);
        }else{
        	if ($this->submit()){
				Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat data Dosen baru dengan nama '.$dosen->nama,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);
        		$this->twig->display('pengguna/index');
        	}
		}
	}

	public function akses(){
		$akses = Akses::all();
		$data = array("items"=>$akses->toArray());
		$this->twig->display('pengguna/akses_index',$data);
	}

	private function submit($id=0,$self=FALSE){
		// if(!$this->isOperator()) return;
		if($id==0){
			$dosen = new Dosen;
		}else{
			$dosen = Dosen::find($id);
		}

		$nidn = $this->input->post('nidn');
		$nama = $this->input->post('nama');
		$gelar_depan = $this->input->post('gelar_depan');
		$gelar_belakang = $this->input->post('gelar_belakang');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$subbidangkeahlian = $this->input->post('subbidangkeahlian');
		$jenjang_pendidikan = $this->input->post('tingkat').' - '.$this->input->post('jurusan');
		$jabatan_akademik = $this->input->post('jabatan_akademik');
		$jabatan_fungsional = $this->input->post('jabatan_fungsional');
		$fakultas = $this->input->post('fakultas');
		$program_studi = $this->input->post('program_studi');
		$alamat = $this->input->post('alamat');
		$biografi = $this->input->post('biografi');
		$formaledu = $this->input->post('formaledu');
		$telp = $this->input->post('prefix_telp').'-'.$this->input->post('telp');
		$hp = $this->input->post('hp');
		$surel = $this->input->post('surel');
		$status_dosen = $this->input->post('status_dosen');
		$status_data = $this->input->post('status_data');
		$akses = $this->input->post('akses');
		$isreviewer = $this->input->post('reviewer[]');
		$isketuaprodi = $this->input->post('ketuaprodi');
		$passworddikti = $this->input->post('passworddikti');
		$uuid = $dosen->salt != null || $dosen->salt != '' ?$dosen->salt:uniqid();
		$salt = $this->config->item('salt');
		$password = $salt.$this->input->post('password').$uuid;
		$ispass = $this->input->post('password');
		$id_schopus = $this->input->post('id_schopus');
		$id_scholar = $this->input->post('id_scholar');
		$id_sinta = $this->input->post('id_sinta');
		$id_orchid = $this->input->post('id_orchid');
		$hash = do_hash($password);

		$dosen->nidn = $nidn;
		$dosen->nama = $nama;
		$dosen->gelar_depan = $gelar_depan;
		$dosen->gelar_belakang = $gelar_belakang;
		$dosen->jenis_kelamin = $jenis_kelamin;
		$dosen->subbidangkeahlian = $subbidangkeahlian;
		$dosen->jenjang_pendidikan = $jenjang_pendidikan;
		$dosen->jabatan_akademik = $jabatan_akademik;
		$dosen->jabatan_fungsional = $jabatan_fungsional;
		$dosen->fakultas = $fakultas;
		$dosen->program_studi = $program_studi;
		$dosen->alamat = $alamat;
		$dosen->biografi = $biografi;
		$dosen->formaledu = $formaledu;
		$dosen->telp = $telp;
		$dosen->hp = $hp;
		$dosen->id_schopus = $id_schopus;
		$dosen->id_scholar = $id_scholar;
		$dosen->id_sinta = $id_sinta;
		$dosen->id_orchid = $id_orchid;
		$dosen->surel = $surel;
		if(!empty($ispass))	$dosen->password = $hash;
		$dosen->salt = $uuid;
		if(!empty($status_dosen)) $dosen->status_dosen = $status_dosen;
		if(!empty($status_data)) $dosen->status_data = $status_data;
		if(!empty($akses))	$dosen->akses = $akses;
		$count_review = count($isreviewer);
		if($count_review==1){
			$dosen->isreviewer = $isreviewer[0];
		}else if($count_review==2){
			$dosen->isreviewer = "3";
		}

		$dosen->isketuaprodi = $isketuaprodi;
		$dosen->passworddikti = $passworddikti;

		if($self){
			$bank = $this->input->post('bank');
			$dosen->bank = $bank;

			$bank_cabang = $this->input->post('bank_cabang');
			$dosen->bank_cabang = $bank_cabang;

			$bank_rek = $this->input->post('bank_rek');
			$dosen->bank_rek = $bank_rek;

			$bank_photo = $this->input->post('bank_photo_res');
			$dosen->bank_photo = $bank_photo;

			$npwp_nomor = $this->input->post('npwp_nomor');
			$dosen->npwp_nomor = $npwp_nomor;

			$npwp_photo = $this->input->post('npwp_photo_res');
			$dosen->npwp_photo = $npwp_photo;
		}


		$dosen->save();

		if($id==0){

		}else{
			Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah data Dosen baru dengan nama '.$dosen->nama,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);
		}



		return True;
	}

	public function editProfile(){
		$this->is_login();
		$id = $this->uri->segment(3);
		if(!$this->isPengguna($id)) return;
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);
		$jab_ak = Jabatan_akademik::where('isdelete','=','0')->get();
		$jab_fu = Jabatan_fungsi::where('isdelete','=','0')->get();
		$fak = Fakultas::where('isdelete','=','0')->get();
		$pro_stu = Program_studi::where('isdelete','=','0')->get();
		$aks = Akses::all(); 
		$sub_bk = Subbidangkeahlian::where('isdelete', '=', '0')->get();  
		$sub_bk->load('bidangkeahlian');
		$data = array("jab_ak"=>$jab_ak->toArray(),"jab_fu"=>$jab_fu->toArray(),"sub_bk"=>$sub_bk->toArray(),
		'fak'=>$fak->toArray(),'pro_stu'=>$pro_stu->toArray(),"aks"=>$aks->toArray(),"hide"=>TRUE);
        if ($this->form_validation->run() == FALSE){
			$jurnal = Dosen::whereId($id)->get();
			$this->twig->display('pengguna/editmyprofile', array_merge($jurnal->toArray()[0],$data));
        }else{
        	Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah profile pribadi','type'=>'0','created_at'=>date('Y-m-d H:i:s')]);
        	if ($this->submit($id,TRUE))
        		redirect('pengguna/profile');
		}
	}

	public function editLPPM(){
		$this->is_login();
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');
		if(!$this->isPengguna($id)) return;
		$this->form_validation->set_rules('reviewer', 'reviewer', 'required');
        if ($this->form_validation->run() == FALSE){
			$jurnal = Dosen::whereId($id)->get();
			$this->twig->display('pengguna/editlppm', array_merge($jurnal->toArray()[0],[]));
        }else{
        	Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah profile pribadi','type'=>'0','created_at'=>date('Y-m-d H:i:s')]);
        	if ($this->submitLPPM($id))
        		redirect('pengguna/index');
		}
	}

	public function submitLPPM($id){
		$dosen = Dosen::find($id);

		$isreviewer = $this->input->post('reviewer');
		if(!empty($akses))	$dosen->akses = $akses;
		$dosen->isreviewer = $isreviewer;
		$dosen->save();
		return True;
	}


	public function edit(){
		if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation(TRUE);
		$jab_ak = Jabatan_akademik::where('isdelete','=','0')->get();
		$jab_fu = Jabatan_fungsi::where('isdelete','=','0')->get();
		$fak = Fakultas::where('isdelete','=','0')->get();
		$pro_stu = Program_studi::where('isdelete','=','0')->get();
		$akslemlit = Akses::WhereIn("nama",$this->gakslemlit)->get();
		$akslppm = Akses::WhereIn("nama",$this->gakslppm)->get();
		$aks = Akses::WhereIn("nama",$this->gaksnonlembaga)->get();

		$data = array("jab_ak"=>$jab_ak->toArray(),"jab_fu"=>$jab_fu->toArray(),
		'fak'=>$fak->toArray(),'pro_stu'=>$pro_stu->toArray(),"aks"=>$aks->toArray(),"akslppm"=>$akslppm->toArray(),"akslemlit"=>$akslemlit->toArray(),
		"gakslemlit"=>$this->gakslemlit,"gakslppm"=>$this->gakslppm,"gaksnonlembaga"=>$this->gaksnonlembaga);
        if ($this->form_validation->run() == FALSE){
			$jurnal = Dosen::whereId($id)->get();
			$jurnal->load('akses');
			$this->twig->display('pengguna/edit', array_merge($jurnal->toArray()[0],$data));
        }else{
        	if ($this->submit($id))
        		$this->session->set_flashdata('success', 'Edit profile dosen berhasil!');
        		$page = $this->session->userdata('page_pengguna');
        		$view = $this->session->userdata('view_pengguna');
        		$page = isset($page) ? $page:1;
        		$view = isset($view) ? $view:10;
    			redirect('pengguna/?page='.$page."&view=".$view);
		}
	}

	public function delete(){
		if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$item = Dosen::find($id);
		$item->isdelete = 1;
		$item->save();

		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus data Dosen baru dengan nama '.$item->nama,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);

		redirect('pengguna/');
	}

	
	public function verified($idpendidikan,$iddosen){ 
		if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);//print_r($idpendidikan);die();
		$item = Pendidikan::find($id);
		$item->status = 'Verified';
		$item->save();

		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus data Dosen baru dengan nama '.$item->nama,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);

		redirect('pengguna/add/'.$iddosen);
	}
	public function deletePendidikan($idpendidikan,$iddosen){
		if(!$this->isOperator()) return;
		$id = $this->uri->segment(3);
		$item = Pendidikan::find($id);
		$item->isdelete = 1;
		$item->save();

		// Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus data Dosen baru dengan nama '.$item->nama,'type'=>'2','created_at'=>date('Y-m-d H:i:s')]);

		redirect('pengguna/add/'.$iddosen);
	}

	public function json(){
		$nidn = $this->uri->segment(3);
		$item = Dosen::where("nidn","=",$nidn)->get();
   		$item->load("program_studi");
   		$item->load("fakultas");
   		$item->load("jabatan_fungsional");
   		$item->load("jabatan_akademik");
   		$item->load("subbidangkeahlian");
		$data = $item->toArray();
		unset($data[0]["password"]);
		unset($data[0]["salt"]);
		unset($data[0]["issuperadmin"]);
		echo json_encode($data);
		// echo $item->toJson();
	}

	public function programstudijson(){
		// $this->is_login();
		$fakultas = $this->uri->segment(3);
		$program_studi = Program_studi::where('fakultas','=',$fakultas)->where('isdelete','=','0')->get();
		echo $program_studi->toJson();
	}

	private function refresh_session(){
		$user = Dosen::where('id', '=',$this->is_login()["id"])->with("fakultas")->with("program_studi")->with("jabatan_akademik")->with("jabatan_fungsional")->with("akses")->first();
		$this->session->set_userdata('userinfo',$user->toArray());
	}
	public function changepassword(){
		$this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->form_validation->set_message('password_verification','%s Salah');
		$this->form_validation->set_rules('oldpassword','Password Lama', 'required|callback_password_verification');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[konfirmasi]');
		$this->form_validation->set_rules('konfirmasi', 'Konfirmasi Password', 'required');

        if ($this->form_validation->run() == FALSE){
			$this->twig->display('pengguna/changepassword');
        }else{
        	if ($this->submit_password())
        		$this->twig->display('pengguna/changepassword',["success"=>true]);
		}
	}

	private function submit_password(){
		$dosen = Dosen::find($this->is_login()['id']);
		$password = $this->config->item('salt').$this->input->post('password').$dosen->salt;
		$hash = do_hash($password);
		$dosen->password = $hash;
		$dosen->save();
		return True;
	}

	public function password_verification($password){
		$user = Dosen::where('nidn', '=', $this->is_login()['nidn'])->first();
		$combined = $this->config->item('salt').$password.$user->salt;
		$hash_password = do_hash($combined);
		if(($user->password)==$hash_password){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function pdf(){
		$this->load->helper('pdf_helper');
		tcpdf();
		$obj_pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+10, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(1);
		$obj_pdf->AddPage();


		ob_start();
		$user = $this->is_login();

		$dosen = Dosen::where('isdelete','=','0');
		$jabatan = "Ketua Lemlitbang UHAMKA";
		$nama = "Prof. Dr. Suswandari, M. Pd.";

		$nidn_get = $this->input->get("nidn");
		if(isset($nidn_get) && $nidn_get!='' && is_numeric($nidn_get)){
			$dosen->where('nidn','=',$nidn_get);
		}

		$nama_get = $this->input->get("nama");
		if(isset($nama_get) && $nama_get!=''){
			$dosen->where("nama","LIKE","%".$nama_get."%");
		}

		$reviewer_get = $this->input->get("reviewer");
		if(isset($reviewer_get) && $reviewer_get!=''){
			$dosen->where("isreviewer","=",((int)$reviewer_get)-1);
		}

		$ketuaprodi_get = $this->input->get("ketuaprodi");
		if(isset($ketuaprodi_get) && $ketuaprodi_get!=''){
			$dosen->where("isketuaprodi","=",((int)$ketuaprodi_get)-1);
		}

		$akses_get = $this->input->get("akses");
		if(isset($akses_get) && $akses_get!='' && is_numeric($akses_get)) $dosen->where('akses','=',$akses_get);

		$dosen = $dosen->with('Jabatan_akademik')->with('Jabatan_fungsional')->with('Fakultas')->with('Program_studi')->with('Akses')->get();
		$this->load->view('pengguna/pdf',array("items"=>$dosen->toArray(),"nama"=>$nama,"jabatan"=>$jabatan));
	    $content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output('DataDosen-'.$this->is_login()['nidn'].'.pdf', 'I');
	}

	public function upload_tabungan(){
		$this->generate_folder('uploads/tabungan');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/tabungan'),
			'allowed_types' => 'jpg|png|gif',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}

	public function upload_npwp(){
		$this->generate_folder('uploads/npwp');
		$filename = $this->gen_uuid();
		$upload_config = [
			'file_name' => $filename,
			'upload_path' => realpath(APPPATH . '../uploads/npwp'),
			'allowed_types' => 'jpg|png|gif',
			'max_size' => 10000,
			'file_ext_tolower' => TRUE
		];
		$this->load->library('upload', $upload_config);
		$file_field = "file";
		$uploaded = TRUE;
		$uploaded = $this->upload->do_upload($file_field);
		if($uploaded) echo $this->upload->data('file_name');
		echo $this->upload->display_errors();
	}


}
