<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class BatasanDana extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p10"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("batasan_dana_pengabdian");
        

    }

	public function index(){
		if(!$this->isAkses(["Operator LPPM",'Ketua LPPM',"Sekretaris LPPM"],True)) return;
		$data = Batasan_dana_pengabdian::where('isdelete', '=', '0')->orderBy('status','DESC')->orderBy('id','DESC');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$items = ["items"=>$data->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/suratkontrak/pengabdian/batasandana/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/suratkontrak/pengabdian/batasandana/add',$this->menu);
        }else{ 
        	if ($this->insert())
        		redirect("suratkontraks/pengabdian/batasandana");
        	else echo $this->upload->display_errors();
		}
    }

    public function edit(){
    	$id = $this->uri->segment(5);
		$status = $this->input->post('status');
		$batasan = $this->input->post('batasan');
		if($status=='1'){
			$updates = Batasan_dana_pengabdian::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Batasan_dana_pengabdian::find($id);
		$data->status = $status;
		// $data->batasan = $batasan;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Surat Kontrak-Batasan Dana dengan nilai '.$data->batasan,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/pengabdian/batasandana/");
    }

    public function validation($edit=False){
    	$this->form_validation->set_rules('batasan', 'Batas maksimum pengambilan dana', 'required');
    	$this->form_validation->set_rules('status', 'Status', 'required');
    }

    public function insert(){
		$batasan = $this->input->post("batasan");
		$status = $this->input->post('status');
		if($status=='1'){
			$updates = Batasan_dana_pengabdian::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = new Batasan_dana_pengabdian;
		$data->batasan = $batasan;
		$data->status = $status;
		$data->type = "pengabdian";
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Surat Kontrak-Batasan Dana dengan nilai '.$data->batasan,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/pengabdian/batasandana/");
    }

 	public function is_exist(){
 	// 	$data = Batasan_dana_pengabdian::whereHas("dosen",function($q){
 	// 		$q->where('nidn','=',$this->input->get("nidn"));
 	// 	})->where("isdelete","=","0")->count();
 	// 	if($data>0){
		// 	$this->output->set_header('HTTP/1.0 470 Data sudah ada');
		// 	return;
 	// 	}
		// $this->output->set_header('HTTP/1.0 200 OK');
		echo "Approve";
 	}

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Batasan_dana_pengabdian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Surat Kontrak-Batasan Dana dengan nilai '.$item->batasan,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/pengabdian/batasandana/');
	}
}
