<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Surata extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p10"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("surat_pengabdian");
				$this->load->model("Dosen");
    }

	public function index(){
        if(!$this->isAkses(["Operator LPPM",'Ketua LPPM',"Sekretaris LPPM"],True)) return;
		$data = Surat_pengabdian::where('isdelete', '=', '0')->where('nomor','=','1')->orderBy('status','DESC')->orderBy('id','DESC');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$items = ["items"=>$data->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/suratkontrak/pengabdian/surata/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/suratkontrak/pengabdian/surata/add',$this->menu);
        }else{
        	if ($this->insert())
        		redirect("suratkontraks/pengabdian/surata");
        	else echo $this->upload->display_errors();
		}
    }

    public function edit(){
    $id = $this->uri->segment(5);
		$nama = $this->input->post('nama');
		$isi = $this->input->post('isi');
		$status = $this->input->post('status');
		$kop = $this->input->post('kop');
		if($status=='1'){
			$updates = Surat_pengabdian::where('isdelete', '=', '0')->where('nomor','=','1')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Surat_pengabdian::find($id);
		$data->status = $status;
		$data->nama = $nama;
		if($isi) $data->isi = $isi;
		if(isset($kop)) $data->kop = $kop;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Surat Kontrak-Surat Kontrak 1 dengan nama versi '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/pengabdian/surata/");
    }

    public function validation($edit=False){
    	$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('isi', 'Isi', 'required');
    	$this->form_validation->set_rules('status', 'Status', 'required');
    }

    public function insert(){
		$nama = $this->input->post('nama');
		$isi = $this->input->post('isi');
		$status = $this->input->post('status');
		$kop = $this->input->post('kop');
		if($status=='1'){
			$updates = Surat_pengabdian::where('isdelete', '=', '0')->where('nomor','=','1')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = new Surat_pengabdian;
		$data->status = $status;
		$data->nama = $nama;
		$data->isi = $isi;
		$data->kop = $kop;
		$data->nomor = 1;
		$data->type = "pengabdian";
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Surat Kontrak-Surat Kontrak 1 baru dengan nama versi '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/pengabdian/surata/");
    }

 	public function is_exist(){
 	// 	$data = Surat_pengabdian::whereHas("dosen",function($q){
 	// 		$q->where('nidn','=',$this->input->get("nidn"));
 	// 	})->where('isdelete', '=', '0')->where('nomor','=','1')->count();
 	// 	if($data>0){
		// 	$this->output->set_header('HTTP/1.0 470 Data sudah ada');
		// 	return;
 	// 	}
		// $this->output->set_header('HTTP/1.0 200 OK');
		echo "Approve";
 	}

	public function preview(){
		$id = $this->uri->segment(5);
		$surat = Surat_pengabdian::find($id);
		$this->load->helper('suratkontrakdummy_helper');
		$this->load->helper('pdf_helper');
		// $surat = $this->buat_surat(2);
		tcpdf();
		$obj_pdf = new SURATKONTRAKLPPMPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false,false,$surat["kop"]);
		if($surat->kop) $MARGIN_TOP = PDF_MARGIN_TOP+10;
		else $MARGIN_TOP = 10;
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "Sistem Informasi Manajemen & Kinerja Penelitian";
		$obj_pdf->SetTitle($title);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(20, $MARGIN_TOP, 20);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setListIndentWidth(10);
		// $obj_pdf->setCellHeightRatio(2);
		// $obj_pdf->setHtmlHSpace(array(
		//     'li' => array(
		//         'x' => 1, // margin in mm
		//     )
		// ));

		$obj_pdf->AddPage();

		ob_start();
		$user = $this->is_login();
		// $this->load->view('suratkontrak/pdf',array("content"=>$content));
		$content =  $surat->isi;
		$content = sk_nokontrak($content,"111");
		$content = sk_formatnomor1($content,"F123");
		$content = sk_formatnomor2($content,"X233");

		//replace JUDUL PENGUSUL
		$content = sk_judul($content,"FAKTOR-FAKTOR YANG MEMPENGARUHI EFEKTIFITAS PENDIDIKAN DAN MOTIVASI BELAJAR MAHASISWA YANG DIUKUR DENGAN TENDENSI KAPASITAS SUMBER DAYA MANUSIA TENAGA PENGAJAR (DOSEN) DI FAKULTAS EKONOMI DAN BISNIS UNIVERSITAS MUHAMMADIYAH PROF. DR. HAMKA");
		$content = sk_namapengusul($content,"Drs. Budi Sadikin");

		//replace TANGGALMULAI-TANGGALSELESAI TANGGALPEMBUATAN
		$content = sk_tglmulai($content,date('Y-m-d H:i:s'));
		$content = sk_tglselesai($content,date('Y-m-d H:i:s',strtotime("+7 day")));
		$content = sk_tanggalpembuatan($content,date('Y-m-d H:i:s'));

		//replace DANA
		$content = sk_jmldana($content,20,84000000);

		//replace TEMBUSAN
		$content = sk_tembusan($content,"<ol>
					 <li>Kepala Camat</li>
					 <li>Kepala Lurah</li>
					 <li>Ketua RW</li>
					 <li>Ketua RT</li>
					 </ol>");

		$content = sk_ejatgl($content);

		//replace KETUA-MENGETAHUI
		$content = sk_peran($content,"Drs. Sri Suprapto","pengabdian");

		$content = '<style>'.file_get_contents(FCPATH.'assets/dist/css/suratkontrak.css').'</style>'.$content;
		$content = preg_replace('/<p\s[^>]*style\s*=\s*"(.*?)">/', '<table style="$1"><tr><td>', $content);
		$content = preg_replace('/<p\s[^>]*>/', '<table><tr><td>', $content);
		$content = str_replace('<p>', '<table><tr><td>', $content);
		$content = str_replace('</p>', '</td></tr></table>', $content);

		$content = preg_replace('/<div\s[^>]*style\s*=\s*"(.*?)">/', '<table style="$1"><tr><td>', $content);
		$content = str_replace('<div>', '<table><tr><td>', $content);
		$content = str_replace('</div>', '</td></tr></table>', $content);

		// $content = str_replace('<ol>', '<ol style="padding-right:200px;">', $content);
		$content = preg_replace('/<li\s[^>]*style\s*=\s*"(.*?)">/', '<li style="$1">&nbsp;&nbsp;&nbsp;&nbsp;', $content);
		$content = str_replace('<li>', '<li>&nbsp;&nbsp;&nbsp;&nbsp;', $content);
		// $content = str_replace('</li>', '</span></li>', $content);
		// $content = str_replace('</ol>', '</ol></td></tr></table>', $content);
		//
		// $content = str_replace('<ol>', '<table style="padding-left:10px"><tr><td><ol>', $content);

		// $content = str_replace('</ol>', '</ol></td></tr></table>', $content);
		echo $content;
		// die;
		$content = ob_get_contents();
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->Output('Surat Kontrak2.pdf', 'I');
	}

	public function previewaw(){
		$id = $this->uri->segment(5);
		$surat = Surat_pengabdian::find($id);
		$this->load->helper('suratkontrakdummy_helper');
		$this->load->helper('pdf_helper');
		// $surat = $this->buat_surat(2);
		mpdf();
		$obj_pdf = new mPdf();

		ob_start();
		$user = $this->is_login();
		// $this->load->view('suratkontrak/pdf',array("content"=>$content));
		$content =  $surat->isi;
		$content = sk_nokontrak($content,"111");
		$content = sk_formatnomor1($content,"F123");
		$content = sk_formatnomor2($content,"X233");

		//replace JUDUL PENGUSUL
		$content = sk_judul($content,"Budidaya Ikan Lele dan Betok");
		$content = sk_namapengusul($content,"Drs. Budi Sadikin");

		//replace TANGGALMULAI-TANGGALSELESAI TANGGALPEMBUATAN
		$content = sk_tglmulai($content,date('d-m-Y H:i:s'));
		$content = sk_tglselesai($content,date('Y-m-d H:i:s',strtotime("+7 day")));
		$content = sk_tanggalpembuatan($content,date('d-m-Y H:i:s'));

		//replace DANA
		$content = sk_jmldana($content,20,30000000);

		//replace TEMBUSAN
		$content = sk_tembusan($content,"<ol>
					 <li>Kepala Camat</li>
					 <li>Kepala Lurah</li>
					 <li>Ketua RW</li>
					 <li>Ketua RT</li>
					 </ol>");

		//replace KETUA-MENGETAHUI
		$content = sk_peran($content,"Drs. Sri Suprapto");
		$content = '<htmlpageheader name="firstpage" style="display:none">
							    <div style="text-align:center">First Page</div>
							</htmlpageheader><sethtmlpageheader name="firstpage" value="on" show-this-page="1" />
							<sethtmlpageheader name="otherpages" value="on" />
							'.$content;
		$margin_top = 20;
		if($surat->kop){
			$margin_top = 40;
		}
		$obj_pdf->AddPage('', // L - landscape, P - portrait
		'', '', '', '',
		20, // margin_left
		20, // margin right
	 $margin_top, // margin top
	 50, // margin bottom
		5, // margin header
		10); // margin footer
		$htmlheader = '
		<div style="margin-left:-15mm;margin-right:-15mm;">
		            <div style="float:left;width:18%;text-align:right;">
		            &nbsp;&nbsp;<img src="http://127.0.0.1/simakip/assets/images/lemlit.png" style="width:110px;height:auto;">
		            </div>
		            <div style="width:80%;">
		              <div style="text-align:center;font-size:14pt;font-family: "Times New Roman", Times, serif;">UNIVERSITAS MUHAMMADIYAH PROF. DR. HAMKA</div>
		              <div style="line-height:30pt;font-size:17pt;font-weight:bold;text-align:center;font-family: "Times New Roman", Times, serif;">LEMBAGA PENELITIAN DAN PENGEMBANGAN</div>
		              <div style="font-size:12pt;font-weight:bold;text-align:center;font-family: Arial, Helvetica, sans-serif;">Jln. Tanah Merdeka, Pasar Rebo, Jakarta Timur <br>
		Telp. 021-8416624, 87781809; Fax. 87781809
		              </div>
		            </div>
								<hr style="margin-top:2px;"><hr style="margin:-10px;">
		        </div>';
		if($surat->kop){
			$obj_pdf->setHtmlHeader($htmlheader,'1', true);
		}
		$htmlfooter = '<table style="width:100%;">
										<tr>
											<td style="width:20%;text-align:left;font-size:7pt;font-family: "Times New Roman", Times, serif;">Hak Cipta © http://simakip.uhamka.ac.id</td>
											<td style="width:40%;text-align:center;font-size:7pt;font-family: "Times New Roman", Times, serif;">Tanggal Download:'.date('d-m-Y').'</td>
											<td style="width:20%;text-align:right;font-size:7pt;font-family: "Times New Roman", Times, serif;">Halaman {PAGENO} dari {nb}</td>
										</tr>
									</table>';
    // echo $content;		die;
		$obj_pdf->setHtmlFooter($htmlfooter);
		$obj_pdf->WriteHTML($content);
		$obj_pdf->Output('Surat Kontrak2.pdf', 'I');
	}

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Surat_pengabdian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Surat Kontrak-Surat Kontrak 1 dengan nama versi '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/pengabdian/surata/');
	}

	public function lock(){
		$id = $this->uri->segment(5);
		$item = Surat_pengabdian::find($id);
		$item->isvalidate = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengunci konfigurasi Surat Kontrak-Surat Kontrak 1 dengan nama versi '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/pengabdian/surata/');
	}
}
