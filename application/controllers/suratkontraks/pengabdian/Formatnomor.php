<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Formatnomor extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p10"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("Format_nomor_pengabdian");
        

    }

	public function index(){
		if(!$this->isAkses(["Operator LPPM",'Ketua LPPM',"Sekretaris LPPM"],True)) return;
		$data = Format_nomor_pengabdian::where('isdelete', '=', '0')->orderBy('status','DESC')->orderBy('id','DESC');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$items = ["items"=>$data->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/suratkontrak/pengabdian/formatnomor/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/suratkontrak/pengabdian/formatnomor/add',$this->menu);
        }else{
        	if ($this->insert())
        		redirect("suratkontraks/pengabdian/formatnomor");
        	else echo $this->upload->display_errors();
		}
    }

    public function edit(){
    	$id = $this->uri->segment(5);
		$status = $this->input->post('status');
		$format_a = $this->input->post('format_a');
		$format_b = $this->input->post('format_b');
		if($status=='1'){
			$updates = Format_nomor_pengabdian::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Format_nomor_pengabdian::find($id);
		$data->status = $status;
		// $data->format_a = $format_a;
		// $data->format_b = $format_b;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Surat Kontrak-Format Nomor dengan nama format '.$data->format_a.' '.$data->format_b,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/pengabdian/formatnomor/");
    }

    public function validation($edit=False){
    	$this->form_validation->set_rules('format_a', 'No. Kontrak 1', 'required');
    	$this->form_validation->set_rules('format_b', 'No. Kontrak 2', 'required');
    	$this->form_validation->set_rules('status', 'Status', 'required');
    }

    public function insert(){
		$format_a = $this->input->post("format_a");
		$status = $this->input->post('status');
		$format_b = $this->input->post('format_b');
		if($status=='1'){
			$updates = Format_nomor_pengabdian::where("isdelete","=","0")->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = new Format_nomor_pengabdian;
		$data->format_a = $format_a;
		$data->status = $status;
		$data->format_b = $format_b;
		$data->type = "pengabdian";
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Surat Kontrak-Format Nomor dengan nama format '.$data->format_a.' '.$data->format_b,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/pengabdian/formatnomor/");
    }

 	public function is_exist(){
 		$data = Format_nomor_pengabdian::whereHas("dosen",function($q){
 			$q->where('nidn','=',$this->input->get("nidn"));
 		})->where("isdelete","=","0")->count();
 		if($data>0){
			$this->output->set_header('HTTP/1.0 470 Data sudah ada');
			return;
 		}
		$this->output->set_header('HTTP/1.0 200 OK');
		echo "Approve";
 	}

	public function delete(){
		$id = $this->uri->segment(5);
		$item = Format_nomor_pengabdian::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Surat Kontrak-Format Nomor dengan nama format '.$item->format_a.' '.$item->format_b,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/pengabdian/formatnomor/');
	}
}
