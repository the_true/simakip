<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Suratb extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p5"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model("surat");
				$this->load->model("Dosen");

    }

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$data = Surat::where('isdelete', '=', '0')->where('nomor','=','2')->orderBy('status','DESC')->orderBy('id','DESC');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$items = ["items"=>$data->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/suratkontrak/penelitian/suratb/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/suratkontrak/penelitian/suratb/add',$this->menu);
        }else{
        	if ($this->insert())
        		redirect("suratkontraks/suratb");
        	else echo $this->upload->display_errors();
		}
    }

    public function edit(){
    $id = $this->uri->segment(4);
		$nama = $this->input->post('nama');
		$isi = $this->input->post('isi');
		$status = $this->input->post('status');
		$kop = $this->input->post('kop');
		if($status=='1'){
			$updates = Surat::where('isdelete', '=', '0')->where('nomor','=','2')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Surat::find($id);
		$data->status = $status;
		$data->nama = $nama;
		if($isi) $data->isi = $isi;
		if($kop) $data->kop = $kop;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Surat Kontrak-Surat Kontrak 2 dengan nama versi '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/suratb/");
    }

    public function validation($edit=False){
    	$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('isi', 'Isi', 'required');
    	$this->form_validation->set_rules('status', 'Status', 'required');
    }

    public function insert(){
		$nama = $this->input->post('nama');
		$isi = $this->input->post('isi');
		$status = $this->input->post('status');
		$kop = $this->input->post('kop');
		if($status=='1'){
			$updates = Surat::where('isdelete', '=', '0')->where('nomor','=','2')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = new Surat;
		$data->status = $status;
		$data->nama = $nama;
		$data->isi = $isi;
		$data->kop = $kop;
		$data->nomor = 2;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Surat Kontrak-Surat Kontrak 2 baru dengan nama versi '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/suratb/");
    }

 	public function is_exist(){
 	// 	$data = Surat::whereHas("dosen",function($q){
 	// 		$q->where('nidn','=',$this->input->get("nidn"));
 	// 	})->where('isdelete', '=', '0')->where('nomor','=','2')->count();
 	// 	if($data>0){
		// 	$this->output->set_header('HTTP/1.0 470 Data sudah ada');
		// 	return;
 	// 	}
		// $this->output->set_header('HTTP/1.0 200 OK');
		echo "Approve";
 	}

	public function delete(){
		$id = $this->uri->segment(4);
		$item = Surat::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Surat Kontrak-Surat Kontrak 2 dengan nama versi '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/suratb/');
	}

	public function lock(){
		$id = $this->uri->segment(4);
		$item = Surat::find($id);
		$item->isvalidate = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' mengunci konfigurasi Surat Kontrak-Surat Kontrak 2 dengan nama versi '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/suratb/');
	}
}
