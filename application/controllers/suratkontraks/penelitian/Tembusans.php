<?php
defined("BASEPATH") OR exit("No direct script access allowed");
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Tembusans extends MY_BaseController {

	/**
	* @category Libraries
	* @package  CodeIgniter 3.0
	* @author   Yp <purwantoyudi42@gmail.com>
	* @link     https://timexstudio.com
	* @license  Protected
	*/
	private $menu = array("p"=>"active","p5"=>"active");
	public function __construct() {
		$config = [
			"functions" => ["anchor","set_value","set_radio"],
			"functions_safe" => ["validation_errors_array","form_open"],
		];
		parent::__construct($config);
		$this->load->model("Tembusan");
		
	}

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$this->twig->addGlobal('session', $this->session);
		$id = $this->uri->segment(2);
		$item = Tembusan::where("isdelete","=","0")->get();
		$items["items"] = $item->toArray();
		$this->twig->display("pengaturan/suratkontrak/penelitian/tembusan/index",array_merge($items,$this->menu));
	}


	public function edit(){
		$user = $this->is_login();
		$data = $this->update();
		if ($data)
		$this->session->set_flashdata('success', 'Data Berhasil disimpan!');
		redirect("/suratkontraks/tembusans");
		redirect('login/pagenotfound', 'refresh');
	}

	public function update(){
		$id = $this->uri->segment(4);
		$tembusan = $this->input->post("isi");
		$isi = $this->input->post('isi');
		$status = $this->input->post('status');
		$nama = $this->input->post('nama');
		$data = Tembusan::find($id);
		if($status=='1'){
			$updates = Tembusan::where('isdelete', '=', '0')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data->nama= $nama;
		$data->isi = $tembusan;
		$data->status = $status;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Surat Kontrak-Tembusan dengan nama versi '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		return $data;
	}

	public function insert(){
		$tembusan = $this->input->post("isi");
		$isi = $this->input->post('isi');
		$status = $this->input->post('status');
		$nama = $this->input->post('nama');
		$data = new Tembusan;
		if($status=='1'){
			$updates = Tembusan::where('isdelete', '=', '0')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data->nama= $nama;
		$data->isi = $tembusan;
		$data->status = $status;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Surat Kontrak-Tembusan dengan nama versi '.$data->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		$this->session->set_flashdata('success', 'Data Berhasil disimpan!');
		redirect("/suratkontraks/tembusans");
	}

	public function delete(){
		$id = $this->uri->segment(4);
		$item = Tembusan::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Surat Kontrak-Tembusan dengan nama versi '.$item->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/tembusans/');
	}

}
