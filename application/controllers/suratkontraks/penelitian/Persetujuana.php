<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
use Illuminate\Database\Query\Expression as raw;
class Persetujuana extends MY_BaseController {

	/**
 	* @category Libraries
 	* @package  CodeIgniter 3.0
 	* @author   Yp <purwantoyudi42@gmail.com>
 	* @link     https://timexstudio.com
 	* @license  Protected
 	*/
    private $menu = array("p"=>"active","p5"=>"active");
    public function __construct() {
		$config = [
			'functions' => ['anchor','set_value','set_radio'],
		    'functions_safe' => ['validation_errors_array','form_open'],
		];
        parent::__construct($config);
        $this->load->model('Persetujuan');
        $this->load->model('Dosen');
        $this->load->model('Jabatan_akademik');
        
    }

	public function index(){
		if(!$this->isAkses(["Operator Lemlitbang",'Ketua Lemlitbang',"Sekretaris Lemlitbang"],True)) return;
		$data = Persetujuan::where('isdelete', '=', '0')->where('nomor','=','1')->orderBy('status','DESC')->orderBy('id','DESC');
		$info = $this->create_paging($data);
		$data = $data->take($info["limit"])->skip($info["skip"])->get();
		$data->load("dosen")->load("dosen.jabatan_akademik");
		$items = ["items"=>$data->toArray()];
		$items = array_merge($items,$info);
		$this->twig->display('pengaturan/suratkontrak/penelitian/persetujuana/index',array_merge($items,$this->menu));
	}

    public function add(){
		$user = $this->is_login();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->validation();

        if ($this->form_validation->run() == FALSE){
            $this->twig->display('pengaturan/suratkontrak/penelitian/persetujuana/add',$this->menu);
        }else{
        	if ($this->insert())
        		redirect("suratkontraks/persetujuana/");
        	else echo $this->upload->display_errors();
		}
    }

    public function edit(){
    	$id = $this->uri->segment(4);
		$status = $this->input->post('status');
		$peran = $this->input->post('peran');
		if($status=='1'){
			$updates = Persetujuan::where('isdelete', '=', '0')->where('nomor','=','1')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = Persetujuan::find($id);
		$data->status = $status;
		$data->peran = $peran;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'edit','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' merubah konfigurasi Surat Kontrak-Persetujuan 1 dengan nama dosen '.$data->dosen()->first()->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/persetujuana/");
    }

    public function validation($edit=False){
    	$this->form_validation->set_rules('nidn', 'NIDN', 'required');
    	$this->form_validation->set_rules('dosen', 'Dosen', 'required');
    	$this->form_validation->set_rules('status', 'Status', 'required');
    }

    public function insert(){
		$dosen = $this->input->post("dosen");
		$status = $this->input->post('status');
		$peran = $this->input->post('peran');
		if($status=='1'){
			$updates = Persetujuan::where('isdelete', '=', '0')->where('nomor','=','1')->get();
			foreach($updates as $d){
				$d->status='0';
				$d->save();
			}
		}
		$data = new Persetujuan;
		$data->dosen = $dosen;
		$data->status = $status;
		$data->peran = $peran;
    	$data->nomor = 1;
		$data->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'insert','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' membuat konfigurasi Surat Kontrak-Persetujuan 1 dengan nama dosen '.$data->dosen()->first()->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect("suratkontraks/persetujuana/");
    }

 	public function is_exist(){
 		$data = Persetujuan::whereHas("dosen",function($q){
 			$q->where('nidn','=',$this->input->get("nidn"));
 		})->where('isdelete', '=', '0')->where('nomor','=','1')->count();
 		if($data>0){
			$this->output->set_header('HTTP/1.0 470 Data sudah ada');
			return;
 		}
		$this->output->set_header('HTTP/1.0 200 OK');
		echo "Aprove";
 	}

	public function delete(){
		$id = $this->uri->segment(4);
		$item = Persetujuan::find($id);
		$item->isdelete = 1;
		$item->save();
		Log_Activity::insert(["dosen"=>$this->is_login()["id"],'action'=>'delete','content'=>'User '.$this->is_login()['nama'].' sebagai '.$this->is_login()['akses']['nama'].' menghapus konfigurasi Surat Kontrak-Persetujuan 1 dengan nama dosen '.$item->dosen()->first()->nama,'type'=>'6','created_at'=>date('Y-m-d H:i:s')]);
		redirect('suratkontraks/persetujuana/');
	}
}
