<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CSVReader {

    var $fields;            /** columns names retrieved after parsing */ 
    var $separator = ';';    /** separator used to explode each line */
    var $enclosure = '"';    /** enclosure used to decorate each field */

    var $max_row_size = 10000;    /** maximum row size to be used for decoding */

    function parse_file($p_Filepath) {
        $file = fopen($p_Filepath, 'r');
        $this->fields = fgetcsv($file, 200, $this->separator, $this->enclosure);
        $keys = str_getcsv($this->fields[0]);
        $content = array();
        $i = 1;
        $row = fgetcsv($file, 200, $this->separator, $this->enclosure);
        $i = 0;
        while ((count($row)>1) || $i<512) {
            if ($row[0] != null) { // skip empty lines     
                $values = str_getcsv($row[0]);
                if (count($keys) == count($values)) {
                    $arr = array();
                    for ($j = 0; $j < count($keys); $j++) {
                        if ($keys[$j] != "") {
                            $arr[$keys[$j]] = $values[$j];
                        }
                    }
                    $content[] = $arr;
                }
            }
            $i++;
            $row = fgetcsv($file, 200, $this->separator, $this->enclosure);
        }
        fclose($file);
        return $content;
    }

    function escape_string($data){
        $result =   array();
        foreach($data as $row){
            $result[]   =   str_replace('"', '',$row);
        }
        return $result;
    }   
}
?> 