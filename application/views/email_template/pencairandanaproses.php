<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }

        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }

    </style>

    <!-- Progressive Enhancements -->
    <style>

        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #000000 !important;
            border-color: #000000 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

        }

    </style>

</head>
<body width="100%" bgcolor="#222222" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #222222;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            Laporan validasi.
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="800" background="http://simakip.uhamka.ac.id/assets/images/head_bg.jpg" style="margin: auto;background-color: #5b1b60;background-repeat: no-repeat;" class="email-container">
			<tr>
				<td style="padding: 0px 0; text-align: center;width:20%;" rowspan="3">
					<img src="http://simakip.uhamka.ac.id/assets/images/logo.png" alt="alt_text" border="0" style="height: auto; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #000000;">
				</td>
                <td style="padding: 0px 0; text-align: center;height:" >
                    <div style="margin:5px;color:rgba(255,255,255,0.8)">
                        <span style="font-size: 25px;font-weight: bold;">SIMAKIP</span><br>
                        <span style="font-size: 20px;">Sistem Manajemen & Kinerja Penelitian</span><br>
                    </div>
                </td>
			</tr>
        </table>
        <!-- Email Header : END -->

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="800" style="margin: auto;" class="email-container">

            <!-- Hero Image, Flush : BEGIN -->
            <!-- Hero Image, Flush : END -->

            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: left; font-family: sans-serif; font-size: 15px; line-height: 30px; color: #000000;">
                    Yth. Bapak/Ibu <span class="nama"><?php echo $dosen; ?></span>,<br><br>

					Bagian Keuangan Universitas Muhammadiyah Prof DR HAMKA akan memproses pencairan Dana Penelitian <? php echo $type; ?><br>
                    dengan rincian sebagai berikut:<br>

                    <table align="center" width="300" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #ccc;">
                    <tr>
                      <td> Nama Lengkap </td>
                      <td> <?php echo $dosen; ?> </td>
                    </tr>
                    <tr>
                      <td> NIDN </td>
                      <td> <?php echo $nidn; ?> </td>
                    </tr>
                    <tr>
                      <td> Fakultas/Program Studi</td>
                      <td> <?php echo $fakultas; ?> - <?php echo $program_studi; ?> </td>
                    </tr>
                    <tr>
                      <td> Judul Penelitian </td>
                      <td> <?php echo $judul; ?> </td>
                    </tr>
                    <tr>
                      <td> Jenis Penelitian </td>
                      <td> <?php echo $jenis_penelitian; ?> </td>
                    </tr>
                    <tr>
                      <td> Dana disetujui </td>
                      <td> <?php echo $disetujui; ?> </td>
                    </tr>
                    <tr>
                      <td> </td>
                      <td> </td>
                    </tr>
                    <tr>
                      <td colspan="2"> Rincian Transfer Dana Penelitian dengan rincian sebagai berikut </td>
                    </tr>
                    <tr>
                      <td> Nomor Rek </td>
                      <td> <?php echo $bank_rek; ?> </td>
                    </tr>
                    <tr>
                      <td> Bank</td>
                      <td> <?php echo $bank; ?> </td>
                    </tr>
                    <tr>
                      <td> Atas Nama </td>
                      <td> <?php echo $dosen; ?> </td>
                    </tr>
                    <tr>
                      <td>  </td>
                      <td>  </td>
                    </tr>
                    <tr>
                      <td> Total Dana Pencairan <?php echo $type; ?> </td>
                      <td> <?php echo $dana; ?> </td>
                    </tr>
                    <tr>
                      <td> Akan ditransfer pada tanggal </td>
                      <td> <?php echo $tanggal_transfer; ?> </td>
                    </tr>
                    <tr>
                      <td>  </td>
                      <td>   </td>
                    </tr>
                    <tr>
                      <td> Status Pencairan Dana </td>
                      <td> Proses Bagian Keuangan UHAMKA </td>
                    </tr>
                  </table>

                </td>
            </tr>

            <tr>
                <td bgcolor="#ffffff" style="padding-top: 40px;padding-right:40px; padding-left: 40px; font-family: sans-serif; font-size: 15px; line-height: 30px; color: #000000;">
                    <div style="float:left">
                    Terima Kasih atas partisipasi<br>
                    SIMAKIP <b>|</b> Sistem Manajemen dan Kinerja Penelitian<br>
                    </div>
                    <div style="float:right; text-align: right;">
                    Ikuti kami:<br>
                    <a href="http://simakip.uhamka.ac.id/"><img src="http://simakip.uhamka.ac.id/assets/images/logo.png" alt="alt_text" border="0" style="height: 30px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #000000;"></a>
                    <a href="https://www.youtube.com/channel/UCc3qJJ8ZsE7l8rLoRHHyQMg"><img src="http://simakip.uhamka.ac.id/assets/images/youtube_icon.png" alt="alt_text" border="0" style="height: 30px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #000000;"></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding-top: 0px; text-align: center; font-family: sans-serif; font-size: 15px; color: #000000;">
                    <hr>
                        <span style="font-size: 10px;">Tlp. 021-8416624, 87781809; Fax. 021-87781809 | Email : lemlit@uhamka.ac.id</span><br>
                        <span style="font-size: 10px;">Lembaga Penelitian dan Pengembangan - Universitas Muhammadiyah Prof DR. HAMKA</span>

                </td>
            </tr>
        </table>
    </center>
</body>
</html>
