<!DOCTYPE html>

<?php $page="profil";?>

<?php include "_head.php";?>



<body class="hold-transition skin-uhamka sidebar-mini">

<div class="wrapper">  

      <?php include "_header.php";?> 
      
      <?php include "_sidebar.php";?> 

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Profil Saya
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-home"></i> Beranda </a></li>
              <li> Profil </li>
              <li class="active"> Detail </li>
            </ol>
          </section>

          <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx s:Main content xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
          <section class="content">  

                  <!-- <div class="box-tools settingnya">   
                        <a href="profil.php" class="btn btn-primary btn-sm pull-left mb-10"> <i class="glyphicon glyphicon-arrow-left"> </i> Kembali</a> 
                  </div> -->
                  <div class="clearfix"></div>

                  <div class="box box-widget widget-user-2"> 
                          <div class="widget-user-header bg-aqua-active">

                                <div class="widget-user-image">
                                      <img class="img-circle" src="dist/img/user2-160x160.jpg" alt="Dr. MOHAMMAD SURYADI SYARIF SE., MM. ">
                                      <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-warning btn-file btn-circle" data-toggle="tooltip" title="" data-original-title="Ganti Photo">
                                          <span class="glyphicon glyphicon-camera"></span> 
                                          <input type="file" />
                                        </span> 
                                      </div>
                                </div> 

                                <!-- /.widget-user-image -->
                               <!--  <h3 class="widget-user-username"> <input class="form-control" type="text" value=" Dr.Ir. Eduard Namaken Sembiring, MS. "></h3>
                                <h5 class="widget-user-desc">NIDN : <input class="form-control" type="text" value="091105111100130 "></h5> -->
                                <div class="clearfix"></div>
                          </div>
                          <div class="box-footer">

                              <form class="form-horizontal" role="form" action="myprofil.php">  
                                  <table width="100%" class="table table-striped">   
                                      <tbody>
                                            <tr>
                                              <td>Nama Lengkap :</td> 
                                              <td>
                                                  <input class="form-control" type="text" value="Eduard Namaken Sembiring, MS.">
                                              </td> 
                                            </tr>
                                            <tr>
                                              <td>NIDN:</td> 
                                              <td>
                                                  <input class="form-control" type="text" value="091105111100130 ">
                                              </td> 
                                            </tr>
                                            <tr>
                                              <td>Gelar Akademik Depan :</td> 
                                              <td>
                                                  <input class="form-control" type="text" value="Dr.Ir.">
                                              </td> 
                                            </tr>
                                            <tr>
                                              <td>Gelar Akademik Belakang :</td> 
                                              <td>  <input class="form-control" type="text" value="S.Kom, MM."> </td> 
                                            </tr>
                                            <tr>
                                              <td> Jenis Kelamin:</td> 
                                              <td>  <input class="form-control" type="text" value="Laki-Laki"> </td> 
                                            </tr>
                                            <tr>
                                              <td> Jenjang Pendidikan:</td> 
                                              <td>   <input class="form-control" type="text" value="S2 Ilmu Komunikasi"></td> 
                                            </tr>
                                            <tr>
                                              <td> Jabatan Akademik:</td> 
                                              <td>  <input class="form-control" type="text" value="Ketua Ilmu Komunikasi"></td> 
                                            </tr>
                                            <tr>
                                              <td> Jabatan Fungsional:</td> 
                                              <td>   <input class="form-control" type="text" value="Ketua Ilmu Komunikasi"></td> 
                                            </tr>
                                            <tr>
                                              <td> Fakultas:</td> 
                                              <td>   <input class="form-control" type="text" value="FMIPA"></td> 
                                            </tr>
                                            <tr>
                                              <td> Program Studi :</td> 
                                              <td>   <input class="form-control" type="text" value="Ilmu Manajemen"></td> 
                                            </tr>
                                            <tr>
                                              <td> Alamat Rumah:</td> 
                                              <td>  <input class="form-control" type="text" value="Jl. Jakarta Raya No.56 Pasar Rebo, Jakarta Timur"></td> 
                                            </tr>
                                            <tr>
                                              <td> No. Tlp :</td> 
                                              <td>  <input class="form-control" type="text" value="0215865228"> </td> 
                                            </tr>
                                            <tr>
                                              <td> No. HP : </td> 
                                              <td>  <input class="form-control" type="text" value="08561237589"></td> 
                                            </tr>
                                            <tr>
                                              <td> Email :</td> 
                                              <td>   <input class="form-control" type="text" value="suryadi@uhamka.co.id"></td> 
                                            </tr>
                                      </tbody>  
                                  </table>  
                                
                                <div class="tc"> 
                                       <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"> </i> Simpan</button>
                                      <button type="cancel" class="btn btn-danger"> <i class="glyphicon glyphicon-remove"> </i>  Cancel</button>    
                                </div> 
                                </form>

                          </div>
                  </div> 
              

          </section>
          <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx e:Main content xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include "_footer.php";?>  
      
       
</div>
<!-- ./wrapper --> 


<!-- REQUIRED JS SCRIPTS -->
<?php include "_js.php";?>


</body>
</html>