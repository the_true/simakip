<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1 style="text-align:center;">DATA LIST DOSEN</h1>
<h3 style="text-align:center;"> Tahun <?php
    echo date("Y");
?> </h3>

<table>
<tr>
 <th style="width:20%;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:7%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Nama Dosen</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Jabatan Akademik</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Fakultas & Prodi</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Hak Akses</th>
  </tr>
   <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo $item["nama_lengkap"]; ?> <br><?php echo $item["jenis_kelamin_show"];?> <br>NIDN: <?php echo $item["nidn"]; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo $item["jabatan_akademik"]["nama"];?> - <?php echo $item["tingkat"];?></td>
    <td style="text-align: left;padding: 8px;"><?php echo $item["fakultas"]["nama"];?> - <?php echo $item["program_studi"]["nama"];?></td>
    <td style="text-align: left;padding: 8px;"><?php echo $item["akses"]["nama"];?><?php 
                                                  if($item["isreviewer"]) echo "<br> Reviewer";
                                                  if($item["isketuaprodi"]) echo "<br> Ketua Prodi";
                                                ?></td>
  </tr>
  <?php
    }
  ?>
</table>
<br><br><br><br><br><br><br><br>
<table>
<tr>
  <th style="width:70%;"></th>
  <th>
Jakarta, <?php echo date("d M Y")?><br>
<?php echo $jabatan ?><br><br><br>

<h4><?php echo $nama ?></h4>
</th>
</tr>
</table>

</body>
</html>
