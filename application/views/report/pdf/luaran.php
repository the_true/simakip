<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1 style="text-align:center;">Reporting</h1>
<h3 style="text-align:center;">Data Luaran <?php echo $title; ?></h3>
<h3 style="text-align:center;"><?php echo $dari; ?> - <?php echo $sampai; ?></h3>

<?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);
?>


<table>
<tr>
 <th style="width:86%;"><h4><?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?></h4></th>
 <th style="width:20%;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:15%;">Fakultas</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:15%;">Publikasi Jurnal</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Buku Ajar</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Pemakalah</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:7%;">HKI</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:8%;">Luaran Lain</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:15%;">Penyelenggara</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:12%;">Penelitian Mandiri</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:7%;">Total</th>
  </tr>
  <?php
    $jurnalnya = 0;
    $buku_ajarnya =0;
    $forum_ilmiahnya=0;
    $hkinya =0;
    $luaran_lainnya = 0;
    $penyelenggara_forum_ilmiahnya = 0;
    $penelitian_hibahnya=0;
    $total = 0;
  ?>
  <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["nama"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["jurnalnya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["buku_ajarnya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["forum_ilmiahnya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["hkinya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["luaran_lainnya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["penyelenggara_forum_ilmiahnya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["penelitian_hibahnya"]) ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["total"]) ?></td>
    <?php
      $jurnalnya += (int) $item["jurnalnya"];
      $buku_ajarnya += (int) $item["buku_ajarnya"];
      $forum_ilmiahnya += (int) $item["forum_ilmiahnya"];
      $hkinya += (int) $item["hkinya"];
      $luaran_lainnya += (int) $item["luaran_lainnya"];
      $penyelenggara_forum_ilmiahnya += (int) $item["penyelenggara_forum_ilmiahnya"];
      $penelitian_hibahnya += (int) $item["penelitian_hibahnya"];
      $total += (int) $item["total"];
    ?>
  </tr>
  <?php
    }
  ?>
<tr>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;" colspan="2"> Total </td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $jurnalnya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $buku_ajarnya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $forum_ilmiahnya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $hkinya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $luaran_lainnya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $penyelenggara_forum_ilmiahnya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $penelitian_hibahnya ?></td>
  <td style="background-color: #778899;color: white;text-align: left;padding: 8px;"><?php echo $total ?></td>
</tr>
</table>
</body>
</html>
