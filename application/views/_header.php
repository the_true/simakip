<!-- UHAMKA -->  
  
 
  <header class="main-header">

    <div class="header">
        <div class="logo_utama">
            <img src="images/logo.png" alt="UHAMKA"> 
            <div class="logo_text"> Sistem Management dan Kinerja Penelitian </div>
        </div><!-- logo_utama -->

        <img src="images/head_bg.jpg" alt="Universitas Muhammadiyah Prof.Dr. Hamka">
    </div>


    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SIMAKIP</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <a href="index.php" class="logo-tambahan">
          <span class="logo-lg"><b>SIMAKIP</b></span>
      </a>

       <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav"> 
         
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"> Dr.Ir. Eduard Namaken, MSc </span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Dr.Ir. Eduard Namaken, MSc 
                  <small>Ketua Teknologi Industri Pertanian</small>  
                  <small>NIDN: 091105111100130</small>
                </p>

              </li> 
              <!-- Menu Footer-->
              <li class="user-footer">  
                  <div class="btn-group">
                    <a href="profil.php" class="btn bg-orange">Profil</a>
                    <a href="password.php" class="btn bg-orange">Password</a>
                    <a href="index.php" class="btn bg-orange">Log Out</a>
                  </div> 
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    
    </nav>
  </header>