<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1 style="text-align:center;">LAPORAN PEMAKALAH FORUM ILMIAH</h1>
<h3 style="text-align:center;"><?php 
  if(array_key_exists('tahun', $tahun))
    echo "Tahun ".$tahun["tahun"]; 
  else
    echo "Semua Data";
?> </h3>

<table>
<tr>
 <th style="width:80%;"><h4>Jenis Luaran: Forum Ilmiah</h4></th>
 <th style="width:20%;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:31%;">Nama Dosen</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:31%;">Judul Makalah</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:31%;">Penyelenggara</th>
  </tr>
  <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><h4><?php echo $item["dosen"]["gelar_depan"]." ".$item["dosen"]["nama"]." ". $item["dosen"]["gelar_belakang"]; ?></h4><br>NIDN : <?php echo $item["dosen"]["nidn"]; ?><br>Status : <?php echo $item["statusShow"]; ?></td>
    <td style="text-align: left;padding: 8px;">Tingkat :  <?php echo $item["type_show"]; ?><br>Tahun: <?php echo $item["tahun_kegiatan"]["tahun"]; ?><br><?php echo $item["judul"]; ?> <br>Forum :  <?php echo $item["forum"]; ?></td>
    <td style="text-align: left;padding: 8px;">Institusi :  <?php echo $item['penyelenggara']; ?><br>Tgl. : <?php echo $item["waktu"]; ?><br>Tempat : <?php echo $item["tempat"];?></td>
  </tr>
  <?php
    }
  ?>
</table>
<br><br><br><br><br><br><br><br>
<table>
<tr>
  <th style="width:70%;"></th>
  <th>
<?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);

?>
Jakarta, <?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?><br>
<?php echo $jabatan ?><br><br><br>

<h4><?php echo $nama ?></h4>
</th>
</tr>
</table>
</body>
</html>
