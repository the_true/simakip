<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1 style="text-align:center;">Data Rekap</h1>
<h2 style="text-align:center;">Surat Kontrak Penelitian</h2>
<h3 style="text-align:center;"><?php 
  if(array_key_exists('tahun', $tahun))
    echo "Tahun ".$tahun["tahun"]; 
  else
    echo "Semua Data";
?> </h3>

<?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);
?>


<table>
<tr>
 <th style="width:80%;"><h4><?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?></h4></th>
 <th style="width:20%;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Nama Dosen Pengusul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Batch</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Judul Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:13%;">Tahun<br>Pelaksanaan</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:13%;">No Kontrak</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Surat 1</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Surat 2</th>
  </tr>
  
  <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count ?></td>
    <td style="text-align: left;padding: 8px;">
      <?php echo $item['dosen']['nama_lengkap']; ?>
    </td>
    <td style="text-align: left;padding: 8px;">
      <?php echo $item['penelitian']['batch']['batch_penelitian']['tahun']['tahun']; ?><br>
      <?php echo $item['penelitian']['batch']['nama']; ?>
    </td>
    <td style="text-align: left;padding: 8px;">
      <?php echo $item['penelitian']['judul']; ?>
    </td>
    <td style="text-align: left;padding: 8px;">
      <?php echo $item['penelitian']['tahun_kegiatan']['tahun']; ?>
    </td>
    <td style="text-align: left;padding: 8px;">
      <?php echo $item['nomor']; ?>
    </td>
    <td style="text-align: left;padding: 8px;">
      Tersedia
    </td>
    <td style="text-align: left;padding: 8px;">
      <?php if($item['is_laporan_done']) echo 'Tersedia'; ?>
    </td>
  </tr>
  <?php
    }
  ?>
</table>
<br><br><br><br><br><br><br><br>
<!-- <table>
<tr>
  <th style="width:70%;"></th>
  <th>
<?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);

?>
Jakarta, <?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?><br>
<?php echo $jabatan ?><br><br><br>

<h4><?php echo $nama ?></h4>
</th>
</tr>
</table> -->

</body>
</html>
