<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1>Detail Penilaian Proposal</h1>
  <!-- <div style="border: 1px solid #c1c1c1;"> -->
  <?php if($isselected){ ?>
    <h2>Identitas Penilai</h2> 
    <hr>
    <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Nama Penilai : </td>
        <td style="text-align: left;padding: 8px;width:63%;border-top: 1px solid #f4f4f4;"><?php echo $hasil["dosen"]["nama_lengkap"]; ?></td>
      </tr> 
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Program Studi : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $hasil["dosen"]["program_studi"]["nama"]; ?></td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Jabatan Akademik : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $hasil["dosen"]["jabatan_akademik"]["nama"]; ?></td>
      </tr> 
      </table>
    <br><br>
  <?php } ?>

    <h2>Identitas Peneliti</h2> 
    <hr>
    <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">NIDN : </td>
        <td style="text-align: left;padding: 8px;width:63%;border-top: 1px solid #f4f4f4;"><?php echo $dosen["nidn"]; ?></td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Nama Peneliti : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $dosen["nama_lengkap"]; ?></td>
      </tr> 
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Status Dosen : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $dosen["status_dosen_text"]; ?></td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Home Base : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $dosen["jurusan"]; ?></td>
      </tr> 
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Anggota Peneliti : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php
          foreach($anggota as $a){
            echo $a["dosen"]["nama_lengkap"]."<br>";
          }
        ?>
        </td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Judul Penelitian : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $judul; ?></td>
      </tr> 
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Batch Tahun : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $batch["nama"]; ?> - <?php echo $tahun_kegiatan["tahun"]; ?></td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Lokasi Penelitian : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;"><?php echo $lokasi; ?></td>
      </tr> 
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;">Lama Penelitian : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;"><?php echo $lama; ?> Bulan </td>
      </tr>
      </table>
      <br><br>
      <h2>Biaya Penelitian</h2>
      <hr>
      <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Pagu Anggaran : </td>
        <td style="text-align: left;padding: 8px;width:63%;border-top: 1px solid #f4f4f4;">Rp <?php echo $anggaran['format_rupiah']; ?>,-</td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Yang Diusulkan : </td>
        <td style="text-align: left;padding: 8px;width:63%;border-top: 1px solid #f4f4f4;"> Rp <?php echo $rp_total_biaya; ?>,- </td>
      </tr> 
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Rekomendasi : </td>
        <td style="text-align: left;padding: 8px;width:63%;border-top: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;"> Rp <?php echo $hasil['rp_rekomendasi']; ?>,- </td>
      </tr>   

    </table>
  <!-- </div> -->

    <!-- s:tabel-->
    <div class="box bordernya">
      <h2 class="box-title">Kriteria Penilaian</h2>
      <hr>
      <!-- /.box-header -->  
      <!-- form start --> 
      <div class="box-body">  

        <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
          <tr>
            <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">#.</th>
            <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Kriteria</th>
            <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Indikator Penilaian</th>
            <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Bobot(%)</th>
            <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Skor</th>
            <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:10%;">Nilai</th>
          </tr>
          <tbody>
          <?php 
            $count=1;
            $total_skor=0;
            $total_nilai=0;
          ?>
          <?php foreach($hasil["penelitian_nilai"] as $h){ ?>
            <?php $total_skor+=$h["skor"]; ?>
            <?php $total_nilai+=$h["nilai"]; ?>
            <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
              <td style="text-align: left;padding: 8px;"><?php echo $count-1; ?>.</td>
              <td style="text-align: left;padding: 8px;"><?php echo nl2br($h["penilaian_kriteria"]["nama"]); ?></td>
              <td style="text-align: left;padding: 8px;"><?php echo nl2br($h["penilaian_kriteria"]["indikator"]); ?></td>
              <td style="text-align: left;padding: 8px;"><?php echo nl2br($h["penilaian_kriteria"]["bobot"]); ?></td> 
              <td style="text-align: left;padding: 8px;"><?php echo nl2br($h["skor"]); ?></td> 
              <td style="text-align: left;padding: 8px;"><?php echo nl2br($h["nilai"]); ?></td>
            </tr>
          <?php } ?>
          </tbody>  
          <tr style="background-color: #778899;color: white;"> 
            <th colspan="3">Total</th>
            <th>100</th>
            <th><?php echo $total_skor; ?></th>
            <th><?php echo $total_nilai; ?></th>
          </tr>
        </table>



      </div>
      <!-- /.box-body -->   
    </div>
    <?php if($isselected){ ?>
    <br><br><br><br><br><br><br><br>
    <table>
    <tr>
      <th style="width:70%;"></th>
      <th>Jakarta, <?php echo date("d M Y")?><br>Reviewer
    <br><br><br>

    <h4><?php echo $hasil["dosen"]["nama_lengkap"]; ?></h4>
    </th>
    </tr>
    </table>
    <?php } ?>
</body>
</html>
