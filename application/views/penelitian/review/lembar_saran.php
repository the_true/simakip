<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1>Lembar Saran dan Rekomendasi</h1>
  <!-- <div style="border: 1px solid #c1c1c1;"> -->
    <h2>Identitas Peneliti</h2> 
    <hr>
    <table style="width:100%;border-collapse:collapse;border-top: 1px solid #f4f4f4;" cellspacing="0" cellpadding="200%">
      <tr> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;">Nama Peneliti :</td>
        <td style="text-align: left;padding: 8px;width:63%;border-top: 1px solid #f4f4f4;"><?php echo $dosen["nama_lengkap"]; ?></td>
      </tr> 
      <tr style="background-color: #f1f1f1;"> 
        <td style="text-align: left;padding: 8px;width:35%;border-top: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;">Judul Penelitian : </td>
        <td style="text-align: left;padding: 8px;border-top: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;"><?php echo $judul; ?></td>
      </tr> 
      </table>

  <br>
  <br>
  <p>Dari hasil penilaian yang dilakukan oleh Tim Penilai (Reviewer) menyarankan / merekomendasikan bahwa judul penelitian tersebut: </p>

  <div class="box bordernya">
    <div class="box-header with-border">
      <?php if ($status_akhir==4){?>
        <h1 class="box-title" style="text-align:center;"> Diterima </h1>
      <?php }else{ ?>
        <h1 class="box-title" style="text-align:center;"> <?php echo strtoupper($hasil['status_text']); ?> </h1>
      <?php } ?>
      <hr>
    </div>
    <!-- /.box-header -->  
    <!-- form start --> 
    <?php if($hasil['status']!='1'){ ?>
    <div class="box-body">
      <?php if($hasil['status']=='3'){ ?>
      <h4>Alasan Penolakan</h4>
      <?php $i=1; ?>
      <?php foreach($review_lain as $rev){ ?>
      Reviewer <?php echo $i; $i++;?>:
      <p>
        <?php echo nl2br($rev["alasan"]); ?>
      </p>
      <?php } ?>
      <?php } ?>

      <?php if($hasil['status']=='2'){ ?>
      <h4>Saran Perbaikan</h4>
      <?php $i=1; ?>
      <?php foreach($review_lain as $rev){ ?>
      Reviewer <?php echo $i; $i++;?>:
      <p>
        <?php echo nl2br($rev["saran"]); ?>
      </p>
      <?php } ?>
      <?php } ?>
    </div>
    <?php } ?>
    <!-- /.box-body -->   
  </div>

  <?php if($isselected){ ?>
    <br><br><br><br><br><br><br><br>
    <table>
    <tr>
      <th style="width:70%;"></th>
      <th>Jakarta, <?php echo date("d M Y")?><br>Reviewer
      <br><br><br>

    <h4><?php echo $hasil["dosen"]["nama_lengkap"]; ?></h4>
    </th>
    </tr>
    </table>
  <?php } ?>
</body>
</html>
