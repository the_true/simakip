<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h3 style="text-align:center;">
  Data Akreditasi Luaran Buku Ajar <?php echo $akreditas["tahun_dari"]["tahun"]." - ".$akreditas["tahun_sampai"]["tahun"]; ?>
</h3>
<h3 style="text-align:center;">
  <?php echo $dosen["nama_lengkap"]; ?>
</h3>
<h3 style="text-align:center;">
  <?php echo $akreditas["fakultas"]["nama"]." - ".$akreditas["program_studi"]["nama"]; ?>
</h3>

<table>
<tr>
 <th style="width:80%;"><?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);

?>
<?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?></th>
 <th style="width:20%;text-align:right;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:32%;">Name Dosen</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:32%;">Judul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:32%;">Buku</th>
  </tr>
  <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><h4><?php echo $item["dosen"]["gelar_depan"]." ".$item["dosen"]["nama"]." ". $item["dosen"]["gelar_belakang"]; ?></h4><br>NIDN :  <?php echo $item["dosen"]["nidn"]; ?></td>
    <td style="text-align: left;padding: 8px;">Tahun: <?php echo $item["tahun_kegiatan"]["tahun"];?><br><?php echo $item["judul"];?></td>
    <td style="text-align: left;padding: 8px;">Penerbit: <?php echo $item["penerbit"] ?> <br>ISBN : <?php echo $item["isbn"];?><br>Jml. Halaman : <?php echo $item["halaman"]; ?></td>
  </tr>

  <?php
    }
  ?>
</table>
</body>
</html>
