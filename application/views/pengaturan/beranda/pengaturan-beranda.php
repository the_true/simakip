<!DOCTYPE html>

<?php $page="pengaturan";?>
<?php $subpage="p4";?>

<?php include "_head.php";?>



<body class="hold-transition skin-uhamka sidebar-mini">

<div class="wrapper">  

      <?php include "_header.php";?> 
      
      <?php include "_sidebar.php";?> 

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Pengaturan <small>Beranda</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-home"></i> Beranda </a></li>
              <li class="active"> Pengaturan Beranda </li>
            </ol>
          </section>

          <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx s:Main content xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
          <section class="content">

              <div class="box box-warning">
                  <div class="box-header with-border">
                        <h3 class="box-title"> Beranda </h3> 
                  </div><!-- .box-header -->

                  <div class="box-body">
                            <!-- Custom Tabs -->
                              <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                          <li class="active"><a href="{{base_url}}beranda/kontak" data-toggle="tab">Kontak</a></li>
                                          <li><a href="#tab_2" data-toggle="tab">Tautan</a></li> 
                                    </ul>
                                    <div class="tab-content">
                                          <div class="tab-pane active" id="tab_1">   
                                              <div class="bheader">
                                                    <h3 class="btitle">Kontak</h3>
                                              </div> 

                                               <!-- s:form search -->
                                              <div class="box bordernya">
                                                <div class="box-header with-border">
                                                  <h3 class="box-title">Form Kontak</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <!-- form start -->
                                                <form class="form-horizontal">
                                                  <div class="box-body">
                                                        
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">Kontak</label> 
                                                            <div class="col-sm-10">
                                                               <textarea class="form-control" rows="5" id="comment"></textarea>
                                                            </div>
                                                        </div> 
                                                        
                                                  </div>
                                                  <!-- /.box-body -->
                                                  <div class="box-footer">
                                                    <button type="cancel" class="btn btn-default pull-right"> <i class="glyphicon glyphicon-remove"> </i> Cancel</button>
                                                    <button type="submit" class="btn btn-danger pull-right mr-10"> <i class="fa fa-plus"> </i> Tambah</button>
                                                  </div>
                                                  <!-- /.box-footer -->
                                                </form>
                                              </div>
                                              <!-- e:form search -->


                                              <div class="clearfix"></div>   
                                          </div>
                                          <!-- e.1 -->
                                          <div class="tab-pane" id="tab_2">
                                              <div class="bheader">
                                                    <h3 class="btitle">Tautan</h3>
                                              </div>  

                                              <!-- s:form search -->
                                              <div class="box bordernya">
                                                <div class="box-header with-border">
                                                  <h3 class="box-title">Form Tambah Data</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <!-- form start -->
                                                <form class="form-horizontal">
                                                  <div class="box-body"> 

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">Nama Tautan</label> 
                                                            <div class="col-sm-10">
                                                              <input type="" class="form-control" id="" placeholder="">
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">
                                                        <label for="" class="col-sm-2 control-label"> Berkas </label> 

                                                        <div class="col-sm-10">  
                                                             <input type="file" id="icondemo" tabindex="-1" style="position: absolute; clip: rect(0px 0px 0px 0px);">
                                                             <div class="bootstrap-filestyle input-group">
                                                                <input type="text" class="form-control " placeholder="" disabled=""> 
                                                                <span class="group-span-filestyle input-group-btn" tabindex="0">
                                                                  <label for="icondemo" class="btn btn-warning ">
                                                                    <span class="icon-span-filestyle glyphicon glyphicon-file"></span> 
                                                                    <span class="buttonText">Select File</span>
                                                                  </label>
                                                                </span>
                                                              </div> 
                                                        </div> <!-- col-sm-10 --> 
                                                        </div> 
                                                  </div>
                                                  <!-- /.box-body -->
                                                  <div class="box-footer">
                                                    <button type="cancel" class="btn btn-default pull-right"> <i class="glyphicon glyphicon-remove"> </i> Cancel</button>
                                                    <button type="submit" class="btn btn-danger pull-right mr-10"> <i class="fa fa-plus"> </i> Tambah</button>
                                                  </div>
                                                  <!-- /.box-footer -->
                                                </form>
                                              </div>
                                              <!-- e:form search -->

                                              <table width="99%" class="table table-bordered mb-0"> 
                                                    <thead>
                                                        <tr>
                                                          <th style="width: 10px">#</th>
                                                          <th>Nama Tautan</th> 
                                                          <th>Berkas</th> 
                                                          <th style="width:60px;">Action</th>
                                                        </tr> 
                                                    </thead>
                                                    <tbody>
                                                          <tr>
                                                            <td>1.</td>
                                                            <td>
                                                                <a href="">Tautan</a>
                                                            </td>  
                                                            <td> 
                                                                <a href="#" class="btn-sm btn btn-primary" data-toggle="tooltip" title="Download"> <i class="glyphicon glyphicon-download"></i> Download Berkas</a>
                                                            </td> 
                                                            <td> 
                                                                <a href="#" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Delete"> <i class="fa fa-trash"></i> </a>
                                                            </td> 
                                                          </tr>  

                                                          <tr>
                                                            <td>2.</td>
                                                            <td>
                                                                <a href="">Tautan</a>
                                                            </td>  
                                                            <td> 
                                                                <a href="#" class="btn-sm btn btn-primary" data-toggle="tooltip" title="Download"> <i class="glyphicon glyphicon-download"></i> Download Berkas</a>
                                                            </td> 
                                                            <td> 
                                                                <a href="#" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Delete"> <i class="fa fa-trash"></i> </a>
                                                            </td> 
                                                          </tr>  

                                                          <tr>
                                                            <td>3.</td>
                                                            <td>
                                                                <a href="">Tautan</a>
                                                            </td>  
                                                            <td> 
                                                                <a href="#" class="btn-sm btn btn-primary" data-toggle="tooltip" title="Download"> <i class="glyphicon glyphicon-download"></i> Download Berkas</a>
                                                            </td> 
                                                            <td> 
                                                                <a href="#" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Delete"> <i class="fa fa-trash"></i> </a>
                                                            </td> 
                                                          </tr>  

                                                          <tr>
                                                            <td>4.</td>
                                                            <td>
                                                                <a href="">Tautan</a>
                                                            </td>  
                                                            <td> 
                                                                <a href="#" class="btn-sm btn btn-primary" data-toggle="tooltip" title="Download"> <i class="glyphicon glyphicon-download"></i> Download Berkas</a>
                                                            </td> 
                                                            <td> 
                                                                <a href="#" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Delete"> <i class="fa fa-trash"></i> </a>
                                                            </td> 
                                                          </tr>  
                                                            
                                                    </tbody> 
                                                    <tfoot> 
                                                       <tr> 
                                                          <th>#</th>
                                                          <th>Nama Tautan</th> 
                                                          <th>Berkas</th> 
                                                          <th>Action</th>
                                                        </tr> 
                                                      </tr>
                                                    </tfoot> 
                                                </table>   
                                            
                                                <ul class="pagination pull-right">
                                                  <li><a href="#">1</a></li>
                                                  <li class="active"><a href="#">2</a></li>
                                                  <li><a href="#">3</a></li>
                                                  <li><a href="#">4</a></li>
                                                  <li><a href="#">5</a></li>
                                                </ul>  


                                              <div class="clearfix"></div>   
                                          </div>
                                          <!-- e.2 -->  
                                    </div>
                                    <!-- /.tab-content -->
                              </div>
                              <!-- nav-tabs-custom -->
                        
                  </div><!-- box_body -->

              </div><!-- .box box-primary --> 

          </section>
          <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx e:Main content xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include "_footer.php";?>  
      
       
</div>
<!-- ./wrapper --> 


<!-- REQUIRED JS SCRIPTS -->
<?php include "_js.php";?>


</body>
</html>