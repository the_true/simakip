<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1 style="text-align:center;">LAPORAN PENELITIAN DIKTI</h1>

<table>
<tr>
 <th style="width:80%;"><h4>JENIS LUARAN: PENELITIAN DIKTI</h4></th>
 <th style="width:20%;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Nama Skema Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Judul Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Lama Penelitian</th>
  </tr>
  <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["skema_penelitian"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["pendanaan_penelitian"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["lama_penelitian"]);?></td>
  </tr>
  <?php
    }
  ?>
</table>
<br><br><br><br><br><br><br><br>
<table>
<tr>
  <th style="width:70%;"></th>
  <th>
<?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);

?>
Jakarta, <?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?><br>

</th>
</tr>
</table>
</body>
</html>
