<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1 style="text-align:center;">LAPORAN PUBLIKASI JURNAL</h1>
<h3 style="text-align:center;"><?php 
  if(array_key_exists('tahun', $tahun))
    echo "Tahun ".$tahun["tahun"]; 
  else
    echo "Semua Data";
?> </h3>

<table>
<tr>
 <th style="width:80%;"><h4>JENIS LUARAN: JURNAL</h4></th>
 <th style="width:20%;">Jumlah: <?php echo count($items); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Judul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Penulis Publikasi</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Jurnal</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Fakultas/Prodi</th>
  </tr>
  
  <?php
    $count = 0;
    foreach ($items as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count ?></td>
    <td style="text-align: left;padding: 8px;">TAHUN: <?php echo strtoupper($item["tahun_kegiatan"]["tahun"]); ?> <br><?php echo strtoupper($item["judul"]); ?></td>
    <td style="text-align: left;padding: 8px;"><ul><li><?php echo strtoupper($item["dosen"]["nama"]); ?></li> <?php if(strtoupper($item["personil"]["nama"])){ ?><li><?php echo strtoupper($item["personil"]["nama"]) ?></li><?php } ?></ul></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["nama_jurnal"]);?><br>ISSN : <?php echo strtoupper($item["issn"]);?><br>VOLUME : <?php echo strtoupper($item["volume"]); ?><br>NOMOR : <?php echo $item["nomor"]; ?><br>HALAMAN : <?php echo $item["halaman"]; ?><br>URL: <?php echo strtoupper($item["url"]); ?></td>
    <td style="text-align: left;padding: 8px;">FAKULTAS <?php echo strtoupper($item["dosen"]["fakultas"]["nama"]); ?><br>PROGRAM STUDI <?php echo strtoupper($item["dosen"]["program_studi"]["nama"]); ?></td>
  </tr>
  <?php
    }
  ?>
</table>
<br><br><br><br><br><br><br><br>
<table>
<tr>
  <th style="width:70%;"></th>
  <th>
<?php 
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);

?>
Jakarta, <?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?><br>
<?php echo strtoupper($jabatan) ?><br><br><br>

<h4><?php echo strtoupper($nama) ?></h4>
</th>
</tr>
</table>

</body>
</html>
