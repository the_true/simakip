<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h3 style="text-align: center">LAPORAN KINERJA PENELITIAN</h3>
<table  cellspacing="0" cellpadding="150%">
  <tr>
    <td rowspan="5" style="width:20%;">
      <img src="<?php echo base_url().$this->config->item('image_search').'uploads/user_photo/'.$dosen['photo']; ?>" style="width:150px;height:auto;" width="110"></td>
    <td>NIDN</td>
    <td>:<?php echo strtoupper($dosen["nidn"]); ?></td>
  </tr>
  <tr>
    <td>NAMA LENGKAP</td>
    <td>:<?php echo strtoupper($dosen["nama_lengkap"]); ?></td>
  </tr>
  <tr>
    <td>FAKULTAS/PROGRAM STUDI</td>
    <td>:<?php echo strtoupper($dosen["fakultas"]["nama"])."/".strtoupper($dosen["program_studi"]["nama"]); ?></td>
  </tr>
  <tr>
    <td>JABATAN AKADEMIS</td>
    <td>:<?php echo strtoupper($dosen["jabatan_akademik"]["nama"]); ?></td>
  </tr>
  <tr>
    <td>PANGKAT/GOL RUANG</td>
    <td>:<?php echo strtoupper($dosen["jabatan_fungsional"]["nama"]); ?></td>
  </tr>
</table>
<hr style ="border-top: 5px double #8c8b8b;">
<div></div>
<div></div>


<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: PUBLIKASI JURNAL</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($jurnal); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Judul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Penulis Publikasi</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:35%;">Jurnal</th>
  </tr>
  <?php
    $count = 0;
    foreach ($jurnal as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["judul"]); ?></td>
    <td style="text-align: left;padding: 8px;"><ul>
        <?php foreach ($item['jurnal_anggota'] as $key => $anggota): ?>
            <li>
            <?php echo strtoupper($anggota['dosen']["nama_lengkap"]); ?>
                </li>
        <?php endforeach; ?>

          </ul></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["nama_jurnal"]);?><br>ISSN: <?php echo strtoupper($item["issn"]);?><br>VOLUME: <?php echo strtoupper($item["volume"]); ?><br>NOMOR: <?php echo $item["nomor"]; ?><br>HALAMAN: <?php echo $item["halaman"]; ?><br>URL: <?php echo strtoupper($item["url"]); ?></td>
  </tr>
  <?php
    }
  ?>
</table>
 

<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: BUKU/BAHAN AJAR</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($buku_ajar); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:55%;">Judul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:40%;">Buku</th>
  </tr>
  <?php
    $count = 0;
    foreach ($buku_ajar as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["judul"]);?></td>
    <td style="text-align: left;padding: 8px;">PENERBIT: <?php echo strtoupper($item["penerbit"]) ?> <br>ISBN : <?php echo strtoupper($item["isbn"]);?><br>JML. HALAMAN :<?php echo $item["halaman"]; ?></td>
  </tr>
  <?php
    }
  ?>
</table>


<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: PEMAKALAH FORUM ILMIAH</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($forum_ilmiah); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Nama Dosen</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:35%;">Judul Makalah</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Penyelenggara</th>
  </tr>
  <?php
    $count = 0;
    foreach ($forum_ilmiah as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;">
        <?php foreach ($item['forum_ilmiah_anggota'] as $key => $anggota): ?>
            <h4><?php echo strtoupper($anggota["dosen"]["nama_lengkap"]); ?></h4>
            <br>NIDN : <?php echo strtoupper($anggota["dosen"]["nidn"]); ?>
            <br>STATUS : <?php echo strtoupper($anggota["status"]); ?>
            <br /><br />
        <?php endforeach; ?>
    </td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["judul"]); ?> <br>FORUM :  <?php echo strtoupper($item["forum"]); ?></td>
    <td style="text-align: left;padding: 8px;">INSTITUSI :  <?php echo strtoupper($item['penyelenggara']); ?><br>TGL. : <?php echo strtoupper($item["waktu"]); ?><br>TEMPAT : <?php echo strtoupper($item["tempat"]);?></td>
  </tr>
  <?php
    }
  ?>
</table>


<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: HKI</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($hki); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Nama Dosen</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:35%;">Judul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">HKI</th>
  </tr>
  <?php
    $count = 0;
    foreach ($hki as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><h4><?php echo strtoupper($item["dosen"]["nama_lengkap"]); ?></h4><br>NIDN :  <?php echo strtoupper($item["dosen"]["nidn"]); ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["judul"]);?></td>
    <td style="text-align: left;padding: 8px;">JENIS : <?php echo strtoupper($item["jenis_hki"]["nama"]);?><br>NO. PENDAFTARAN : <?php echo strtoupper($item["pendaftaran"]);?><br>STATUS: <?php echo strtoupper($item["status"]);?></td>
  </tr>
  <?php
    }
  ?>
</table>


<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: LUARAN LAIN</h2></th>
 <th style="width:20%;">Jumlah: <?php count($luaran_lain); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:50%;">Luaran</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:45%;">Deskripsi Singkat</th>
  </tr>
  <?php
    $count = 0;
    foreach ($luaran_lain as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><h4><?php echo strtoupper($item["judul"]);?></h4><br>JENIS LUARAN:  <?php echo strtoupper($item["jenis_luaran"]["nama"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["deskripsi"]);?></td>
  </tr>
  <?php
    }
  ?>
</table>

<!-- <br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: PPENYELENGGARA FORUM ILMIAH</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($peny_forum_ilmiah); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
        <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:40%;">Nama Kegiatan</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Mitra & Pelaksana</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Pelaksanaan</th>
  </tr>
  <?php
    $count = 0;
    foreach ($peny_forum_ilmiah as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;">Tahun: <?php echo strtoupper($item["tahun_kegiatan"]["tahun"]); ?><br><b><?php echo strtoupper($item["nama"]); ?></b><br><?php echo strtoupper($item["tingkat_text"]); ?></td>
    <td style="text-align: left;padding: 8px;">Unit Pelaksana : <?php echo strtoupper($item["unit"]); ?> <br>Mitra / Sponsorship : <?php echo strtoupper($item["mitra"]); ?></td>
    <td style="text-align: left;padding: 8px;">Tgl. : <?php echo strtoupper($item["waktu"]); ?><br>Tempat : <?php echo strtoupper($item["tempat"]); ?></td>
  </tr>
  <?php
    }
  ?>
</table>  -->



<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>PENELITIAN MANDIRI</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($penelitian_hibah); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:20%;">Tahun</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:45%;">Judul</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Lokasi</th>
  </tr>
  <?php
    $count = 0;
    foreach ($penelitian_hibah as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo $item["tahun"]["tahun"];?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["judul"]);?><br>Anggaran: Rp. <?php echo $item["rp_anggaran"]; ?>,-<br>Sumber Dana: <?php echo $item['sumber']; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["lokasi"]);?></td>
  </tr>
  <?php
    }
  ?>
</table>


<!-- <br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: PENELITIAN EXTERNAL</h2></th>
 <th style="width:20%;">Jumlah: <?php echo count($penelitian_dikti); ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Nama Skema Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Judul Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:23%;">Lama Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:26%;">Revenue Penelitian</th>
  </tr>
  <?php
    $count = 0;
    foreach ($penelitian_dikti as $item){
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["skema_penelitian"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["pendanaan_penelitian"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["lama_penelitian"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["revenue"]);?></td>
  </tr>
  <?php
    }
  ?>
</table> -->

<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>PENELITIAN INTERNAL</h2></th>
 <th style="width:20%;">Jumlah: <?php echo $count[$penelitians]; ?></th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Nama Peneliti</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Judul Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Jenis Penelitian</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:20%;">Batch Penelitian</th>
  </tr>
  <?php
    $count = 0;
    foreach ($penelitians as $item){ 
  ?>
  <tr style="<?php echo ++$count%2 == 0 ?'background-color: #f2f2f2;':'' ?>">
    <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
    <td style="text-align: left;padding: 8px;"><h4><?php echo "KETUA: ".strtoupper($item["dosen"]["gelar_depan"])." ".strtoupper($item["dosen"]["nama"])." ".strtoupper($item["dosen"]["gelar_belakang"]);?></h4>
        <?php foreach ($item['anggota'] as $key => $anggota): $key=$key+1;?>
        <br><?php echo "ANGGOTA ".$key.": ".strtoupper($anggota["dosen"]["gelar_depan"])." ".strtoupper($anggota["dosen"]["nama"])." ".strtoupper($anggota["dosen"]["gelar_belakang"]);?>
            <!-- <h4><?php echo strtoupper($anggota["dosen"]["nama_lengkap"]); ?></h4>
            <br>NIDN : <?php echo strtoupper($anggota["dosen"]["nidn"]); ?>
            <br>Status : <?php echo strtoupper($anggota["status"]); ?> -->
            <br /> 
        <?php endforeach; ?>
    </td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["judul"]);?></td>
    <td style="text-align: left;padding: 8px;"><?php echo $item["jenis_penelitian"]["nama"];?></td>
    <td style="text-align: left;padding: 8px;"><?php echo strtoupper($item["batch"]["nama"])." - ".strtoupper($item["tahun_kegiatan"]["tahun"]);?></td>
  </tr>
  <?php
    }
  ?>
</table>

<!--
<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>JENIS LUARAN: Revenue Generating</h2></th>
 <th style="width:20%;">Jumlah Berkas: 25 Buku</th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Mitra Pelaksana</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:45%;">Nama Kegiatan</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Kontrak</th>
  </tr>
  <tr style="background-color: #f2f2f2;">
    <td style="text-align: left;padding: 8px;">1</td>
    <td style="text-align: left;padding: 8px;">Mitra :  Sekertariat DPRD Kota Mojekerto</td>
    <td style="text-align: left;padding: 8px;"><h4>Pendalaman tugas pimpinan dan anggota DPRD Kota Mojokerto</h4></td>
    <td style="text-align: left;padding: 8px;">Nilai :  200.000.000 <br> No. :  214/C.01.04/2013</td>
  </tr>
  <tr>
    <td style="text-align: left;padding: 8px;">2</td>
    <td style="text-align: left;padding: 8px;">Mitra :  Sekertariat DPRD Kabupaten Majalengka</td>
    <td style="text-align: left;padding: 8px;"><h4>Bimbingan Teknis dengan Tema &quot;Sinergitas DPRD dan BPK dalam pengawasan dan pengelolaan Keuangan dan kerjasama daerah serta Peran DPRD dalam pembahasan dan persetujuan KUA PPAS Tahun Anggaran 2014</h4></td>
    <td style="text-align: left;padding: 8px;">Nilai :  306.000.000 <br> No. :  214/C.01.09/2013</td>
  </tr>
</table> -->

<!-- <br pagebreak="true"/> -->
<div></div><div></div>
<p style="text-indent: 20px;">
Semua data yang saya isikan dan tercantum dalam biodata ini adalah benar dan dapat dipertanggungjawabkan secara hukum. Apabila di kemudian hari ternyata dijumpai ketidak-sesuaian dengan kenyataan, saya sanggup menerima risikonya.</p>
<br><br>
<p style="text-indent: 20px;">
Demikian biodata ini saya buat dengan sebenarnya untuk memenuhi pelaporan kinerja penelitian dosen Universitas Muhammadiyah Prof DR HAMKA.
</p>


<table>
<tr>
  <th style="width:70%;"></th>
  <th>
<?php
$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
);

?>
Jakarta, <?php echo date("d")." ".$bulan[date('m')]." ".date("Y");?><br>
Pembuat Kinerja Penelitian<br><br><br>

<h4><?php echo strtoupper($dosen["nama_lengkap"]); ?></h4>
</th>
</tr>
</table>
</body>
</html>
