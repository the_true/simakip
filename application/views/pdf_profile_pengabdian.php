<!DOCTYPE html>
<html>

<head>
</head>

<body>
  <h3 style="text-align: center">LAPORAN KERJA PENGABDIAN</h3>
  <table cellspacing="0" cellpadding="150%">
    <tr>
      <td rowspan="5" style="width:20%;">
        <img src="<?php echo base_url() . $this->config->item('image_search') . 'uploads/user_photo/' . $dosen['photo']; ?>" style="width:150px;height:auto;" width="110"></td>
      <td>NIDN</td>
      <td>:<?php echo $dosen["nidn"]; ?></td>
    </tr>
    <tr>
      <td>NAMA LENGKAP</td>
      <td>:<?php echo $dosen["nama_lengkap"]; ?></td>
    </tr>
    <tr>
      <td>FAKULTAS/PROGRAM STUDI</td>
      <td>:<?php echo $dosen["fakultas"]["nama"] . "/" . $dosen["program_studi"]["nama"]; ?></td>
    </tr>
    <tr>
      <td>JABATAN AKADEMIS</td>
      <td>:<?php echo $dosen["jabatan_akademik"]["nama"]; ?></td>
    </tr>
    <tr>
      <td>PANGKAT/GOL RUANG</td>
      <td>:<?php echo $dosen["jabatan_fungsional"]["nama"]; ?></td>
    </tr>
  </table>
  <hr style="border-top: 5px double #8c8b8b;">
  <div></div>
  <div></div>
  <table>
    <tr>
      <th style="width:80%;">
        <h2>Pengabdian Mandiri</h2>
      </th>
      <th style="width:20%;">Jumlah: <?php echo count($penelitian_hibah); ?></th>
    </tr>
  </table>
  <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
    <tr>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:20%;">Tahun</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:45%;">Judul</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Lokasi</th>
    </tr>
    <?php
    $count = 0;
    foreach ($penelitian_hibah as $item) {
    ?>
      <tr style="<?php echo ++$count % 2 == 0 ? 'background-color: #f2f2f2;' : '' ?>">
        <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["tahun"]["tahun"]; ?></td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["judul"]; ?><br>Anggaran: Rp. <?php echo $item["rp_anggaran"]; ?>,-<br>Sumber Dana: <?php echo $item['sumber']; ?></td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["lokasi"]; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>


  <br><br><br>
  <table>
    <tr>
      <th style="width:80%;">
        <h2>Jenis Luaran: Buku/Bahan Ajar</h2>
      </th>
      <th style="width:20%;">Jumlah: <?php echo count($buku_ajar); ?></th>
    </tr>
  </table>
  <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
    <tr>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:55%;">Judul</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:40%;">Buku</th>
    </tr>
    <?php
    $count = 0;
    foreach ($buku_ajar as $item) {
    ?>
      <tr style="<?php echo ++$count % 2 == 0 ? 'background-color: #f2f2f2;' : '' ?>">
        <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["judul"]; ?></td>
        <td style="text-align: left;padding: 8px;">Penerbit: <?php echo $item["penerbit"] ?> <br>ISBN : <?php echo $item["isbn"]; ?><br>Jml. Halaman :<?php echo $item["halaman"]; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>

  <br><br><br>
  <table>
    <tr>
      <th style="width:80%;">
        <h2>Jenis Luaran: Publikasi Jurnal</h2>
      </th>
      <th style="width:20%;">Jumlah: <?php echo count($jurnal); ?></th>
    </tr>
  </table>
  <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
    <tr>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Judul</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Penulis Publikasi</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:35%;">Jurnal</th>
    </tr>
    <?php
    $count = 0;
    foreach ($jurnal as $item) {
    ?>
      <tr style="<?php echo ++$count % 2 == 0 ? 'background-color: #f2f2f2;' : '' ?>">
        <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["judul"]; ?></td>
        <td style="text-align: left;padding: 8px;">
          <ul>
            <?php foreach ($item['jurnal_anggota'] as $key => $anggota) : ?>
              <li>
                <?php echo $anggota['dosen']["nama_lengkap"]; ?>
              </li>
            <?php endforeach; ?>

          </ul>
        </td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["nama_jurnal"]; ?><br>ISSN : <?php echo $item["issn"]; ?><br>Volume : <?php echo $item["volume"]; ?><br>Nomor : <?php echo $item["nomor"]; ?><br>Halaman : <?php echo $item["halaman"]; ?><br>URL: <?php echo $item["url"]; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>


  <br><br><br>
  <table>
    <tr>
      <th style="width:80%;">
        <h2>Jenis Luaran: Forum Ilmiah</h2>
      </th>
      <th style="width:20%;">Jumlah: <?php echo count($forum_ilmiah); ?></th>
    </tr>
  </table>
  <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
    <tr>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Nama Dosen</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:35%;">Judul Makalah</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Penyelenggara</th>
    </tr>
    <?php
    $count = 0;
    foreach ($forum_ilmiah as $item) {
    ?>
      <tr style="<?php echo ++$count % 2 == 0 ? 'background-color: #f2f2f2;' : '' ?>">
        <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
        <td style="text-align: left;padding: 8px;">
          <?php foreach ($item['forum_ilmiah_anggota'] as $key => $anggota) : ?>
            <h4><?php echo $anggota["dosen"]["nama_lengkap"]; ?></h4>
            <br>NIDN : <?php echo $anggota["dosen"]["nidn"]; ?>
            <br>Status : <?php echo $anggota["status"]; ?>
            <br /><br />
          <?php endforeach; ?>
        </td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["judul"]; ?> <br>Forum :  <?php echo $item["forum"]; ?></td>
        <td style="text-align: left;padding: 8px;">Institusi :  <?php echo $item['penyelenggara']; ?><br>Tgl. : <?php echo $item["waktu"]; ?><br>Tempat : <?php echo $item["tempat"]; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>


  <br><br><br>
  <table>
    <tr>
      <th style="width:80%;">
        <h2>Jenis Luaran: Hak Cipta</h2>
      </th>
      <th style="width:20%;">Jumlah: <?php echo count($hki); ?></th>
    </tr>
  </table>
  <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
    <tr>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">Nama Dosen</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:35%;">Judul</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:30%;">HKI</th>
    </tr>
    <?php
    $count = 0;
    foreach ($hki as $item) {
    ?>
      <tr style="<?php echo ++$count % 2 == 0 ? 'background-color: #f2f2f2;' : '' ?>">
        <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
        <td style="text-align: left;padding: 8px;">
          <h4><?php echo $item["dosen"]["nama_lengkap"]; ?></h4><br>NIDN :  <?php echo $item["dosen"]["nidn"]; ?>
        </td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["judul"]; ?></td>
        <td style="text-align: left;padding: 8px;">Jenis : <?php echo $item["jenis_hki"]["nama"]; ?><br>No. Pendaftaran : <?php echo $item["pendaftaran"]; ?><br>Status: <?php echo $item["status"]; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>


  <br><br><br>
  <table>
    <tr>
      <th style="width:80%;">
        <h2>Jenis Luaran: Luaran Lainnya</h2>
      </th>
      <th style="width:20%;">Jumlah: <?php count($luaran_lain); ?></th>
    </tr>
  </table>
  <table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
    <tr>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:50%;">Luaran</th>
      <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:45%;">Deskripsi Singkat</th>
    </tr>
    <?php
    $count = 0;
    foreach ($luaran_lain as $item) {
    ?>
      <tr style="<?php echo ++$count % 2 == 0 ? 'background-color: #f2f2f2;' : '' ?>">
        <td style="text-align: left;padding: 8px;"><?php echo $count; ?></td>
        <td style="text-align: left;padding: 8px;">
          <h4><?php echo $item["judul"]; ?></h4><br>Jenis Luaran:  <?php echo $item["jenis_luaran"]["nama"]; ?>
        </td>
        <td style="text-align: left;padding: 8px;"><?php echo $item["deskripsi"]; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>

  <!--
<br><br><br>
<table>
<tr>
 <th style="width:80%;"><h2>Jenis Luaran: Revenue Generating</h2></th>
 <th style="width:20%;">Jumlah Berkas: 25 Buku</th>
</tr>
</table>
<table style="width:100%;border-collapse:collapse;" cellspacing="0" cellpadding="200%">
  <tr>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:5%;">No.</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Mitra Pelaksana</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:45%;">Nama Kegiatan</th>
    <th style="background-color: #778899;color: white;text-align: left;padding: 8px;width:25%;">Kontrak</th>
  </tr>
  <tr style="background-color: #f2f2f2;">
    <td style="text-align: left;padding: 8px;">1</td>
    <td style="text-align: left;padding: 8px;">Mitra :  Sekertariat DPRD Kota Mojekerto</td>
    <td style="text-align: left;padding: 8px;"><h4>Pendalaman tugas pimpinan dan anggota DPRD Kota Mojokerto</h4></td>
    <td style="text-align: left;padding: 8px;">Nilai :  200.000.000 <br> No. :  214/C.01.04/2013</td>
  </tr>
  <tr>
    <td style="text-align: left;padding: 8px;">2</td>
    <td style="text-align: left;padding: 8px;">Mitra :  Sekertariat DPRD Kabupaten Majalengka</td>
    <td style="text-align: left;padding: 8px;"><h4>Bimbingan Teknis dengan Tema &quot;Sinergitas DPRD dan BPK dalam pengawasan dan pengelolaan Keuangan dan kerjasama daerah serta Peran DPRD dalam pembahasan dan persetujuan KUA PPAS Tahun Anggaran 2014</h4></td>
    <td style="text-align: left;padding: 8px;">Nilai :  306.000.000 <br> No. :  214/C.01.09/2013</td>
  </tr>
</table> -->

  <!-- <br pagebreak="true"/> -->
  <div></div>
  <div></div>
  <p style="text-indent: 20px;">
    Semua data yang saya isikan dan tercantum dalam biodata ini adalah benar dan dapat dipertanggungjawabkan secara hukum. Apabila di kemudian hari ternyata dijumpai ketidak-sesuaian dengan kenyataan, saya sanggup menerima risikonya.</p>
  <br><br>
  <p style="text-indent: 20px;">
    Demikian biodata ini saya buat dengan sebenarnya untuk memenuhi pelaporan kinerja pengabdian dosen Universitas Muhammadiyah Prof DR HAMKA.
  </p>


  <table>
    <tr>
      <th style="width:70%;"></th>
      <th>
        <?php
        $bulan = array(
          '01' => 'Januari',
          '02' => 'Februari',
          '03' => 'Maret',
          '04' => 'April',
          '05' => 'Mei',
          '06' => 'Juni',
          '07' => 'Juli',
          '08' => 'Agustus',
          '09' => 'September',
          '10' => 'Oktober',
          '11' => 'November',
          '12' => 'Desember',
        );

        ?>
        Jakarta, <?php echo date("d") . " " . $bulan[date('m')] . " " . date("Y"); ?><br>
        Pembuat Kinerja Pengabdian<br><br><br>

        <h4><?php echo $dosen["nama_lengkap"]; ?></h4>
      </th>
    </tr>
  </table>
</body>

</html>