<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penilaian extends Eloquent {
    
    public $timestamps = FALSE;
    protected $table = 'penilaian';
    protected $primaryKey = 'id';
     protected $appends = ['status_text','active_penilaian'];

    function jenis_penelitian() {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id');
    }

    function jenis_pengabdian() {
        return $this->belongsTo('Jenis_pengabdian', 'jenis_penelitian', 'id');
    }

    function penilaian_kriteria(){
        return $this->hasMany('penilaian_kriteria','penilaian','id')->where("isdelete","=","0");
    }

    function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }

    public function getActivePenilaianAttribute(){
        if($this->status==1){
            return $this->penilaian_kriteria;
        }
    }

}