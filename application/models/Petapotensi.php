<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Petapotensi extends Eloquent {
    
    public $timestamps = True;
    protected $table = 'peta_potensi';
    protected $primaryKey = 'id';
     // protected $appends = ['status_text','active_penilaian'];

    function tahun() {
        return $this->belongsTo('Tahun_kegiatan', 'tahun', 'id');
    }

}