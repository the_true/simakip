<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Persetujuan extends Eloquent {

    public $timestamps = FALSE;
    protected $table = 'persetujuan';
    protected $primaryKey = 'id';
     protected $appends = ['status_text'];

     public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where("type", '=', "penelitian");
    }

    function dosen() {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }

	function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }
}
