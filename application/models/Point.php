<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Point extends Eloquent {

   public $timestamps = FALSE;
   protected $table = 'point';
   protected $primaryKey = 'id';
   protected $appends = ['status_text'];

	function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }

    function scopeActive($query,$type){
        return $query->where('status',1)->where('type', $type);
    }
}
