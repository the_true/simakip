<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Kontak extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'kontak';
    protected $primaryKey = 'id';
    protected $fillable = ['isi'];
}