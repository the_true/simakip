<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pendidikan extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'pendidikan';
    protected $primaryKey = 'id';
    protected $fillable = ['pendidikan','asalpendidikan','tahun','status'];
 
}