<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Penelitian_target_luaran extends Eloquent
{

    public $timestamps = false;
    protected $table = 'penelitian_target_luaran';
    protected $primaryKey = 'id';

    public function penelitian()
    {
        return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    public function target()
    {
        return $this->belongsTo('Target_luaran','target_luaran','id');
    }

}
