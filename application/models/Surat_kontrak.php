<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Surat_kontrak extends Eloquent {

    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'surat_kontrak';
    protected $primaryKey = 'id';
	  protected $appends = ['is_laporan_done'];

    public function newQuery($excludeDeleted = true) {
      return parent::newQuery($excludeDeleted)
          ->where("type", '=', "penelitian");
    }

    function penelitian() {
        return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    function dosen(){
      return $this->belongsTo('Dosen','dosen','id');
    }

    function getIsLaporanDoneAttribute(){
      $penelitian_laporan = Penelitian_laporan::where('penelitian','=',$this->penelitian)->where('status','=','1')->count();
      if($penelitian_laporan>0) return true;
      return false;
    }

    // function getDanaRupiahAttribute(){
    //   $rupiah = (float)$this->dana*(float)$this->penelitian()->first()->total_biaya/100;
    //   // echo $rupiah
    //   return $rupiah;
    // }

    // function getSisaDanaAttribute(){
    //   $total = $this->penelitian()->first()->total_biaya;
    //   $rupiah = (float)$this->dana*(float)$total/100;
    //   $rupiah = $total-$rupiah;
    //   // echo $rupiah
    //   return $rupiah;
    // }

}
