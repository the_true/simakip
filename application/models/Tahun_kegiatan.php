<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Tahun_kegiatan extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'tahun_kegiatan';
    protected $primaryKey = 'id';
    protected $fillable = ['tahun'];

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where('tahun', '<=', (int) date('Y')+1);
    }

    public function petapotensi(){
        return $this->hasMany('petapotensi', 'tahun', 'id');
    }

}