<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Dosen extends Eloquent
{

    public $timestamps = false;
    protected $table = 'dosen';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    protected $appends = ['nama_lengkap', 'tingkat', 'jurusan', 'status', 'status_dosen_text', 'post_telp', 'prefix_telp', 'jenis_kelamin_show', 'status_bank'];

    //kinerja
    public function jurnal()
    {
        return $this->hasMany('jurnal', 'dosen', 'id');
    }

    public function jurnal2()
    {
        return $this->hasMany('jurnal', 'personil', 'id');
    }

    public function akreditas_dosen()
    {
        return $this->hasOne('akreditas_dosen', 'dosen', 'id');
    }

    public function forum_ilmiah()
    {
        return $this->hasMany('forum_ilmiah', 'dosen', 'id');
    }

    public function hki()
    {
        return $this->hasMany('Hki', 'dosen', 'id');
    }

    public function buku_ajar()
    {
        return $this->hasMany('buku_ajar', 'dosen', 'id');
    }

    public function luaran_lain()
    {
        return $this->hasMany('luaran_lain', 'dosen', 'id');
    }

    public function penelitian_hibah()
    {
        return $this->hasMany('penelitian_hibah', 'dosen', 'id');
    }

    public function penelitian()
    {
        return $this->hasMany('penelitian', 'dosen', 'id');
    }

    public function pengabdian()
    {
        return $this->hasMany('pengabdian', 'dosen', 'id');
    }

    public function penyelenggara_forum_ilmiah()
    {
        return $this->hasMany('penyelenggara_forum_ilmiah', 'dosen', 'id');
    }

    public function jabatan_pengesah()
    {
        return $this->hasOne('Pengesah', 'dosen', 'id');
    }

    public function jabatan_pengesah_pengabdian()
    {
        return $this->hasOne('Pengesah_pengabdian', 'dosen', 'id');
    }
    //end

    public function program_studi()
    {
        return $this->belongsTo('Program_studi', 'program_studi', 'id')->select(array('id', 'nama'));
    }

    
    public function subbidangkeahlian()
    {
        return $this->belongsTo('Subbidangkeahlian', 'subbidangkeahlian', 'id');
    }
    
    public function jabatan_akademik()
    {
        return $this->belongsTo('Jabatan_akademik', 'jabatan_akademik', 'id')->select(array('id', 'nama'));
    }

    public function jabatan_fungsional()
    {
        return $this->belongsTo('Jabatan_fungsi', 'jabatan_fungsional', 'id');
    }

    public function fakultas()
    {
        return $this->belongsTo('Fakultas', 'fakultas', 'id')->select(array('id', 'nama'));
    }

    public function getStatusAttribute($value)
    {
        return $value == 1 ? "Aktif" : "Non Aktif";
    }

    public function akses()
    {
        return $this->belongsTo('Akses', 'akses', 'id');
    }

    public function getTingkatAttribute()
    {
        $result = explode(" - ", $this->jenjang_pendidikan);
        if (count($result) > 1) {
            return $result[0];
        }

    }

    public function getJurusanAttribute()
    {
        $result = explode(" - ", $this->jenjang_pendidikan);
        if (count($result) > 1) {
            return $result[1];
        }

    }

    public function getPostTelpAttribute()
    {
        $result = explode("-", $this->telp);
        if (count($result) > 1) {
            return $result[1];
        }

        return $this->telp;
    }

    public function getStatusDosenTextAttribute()
    {
        if ($this->status_dosen == "1") {
            return "Tetap Persyarikatan";
        }

        if ($this->status_dosen == "2") {
            return "PNS";
        }

        if ($this->status_dosen == "3") {
            return "Dosen Tidak Tetap";
        }

    }

    public function getPrefixTelpAttribute()
    {
        $result = explode("-", $this->telp);
        if (count($result) > 1) {
            return $result[0];
        }

        return "";
    }

    public function getJenisKelaminShowAttribute()
    {
        return $this->jenis_kelamin == "L" ? "Laki-Laki" : "Perempuan";
    }

    public function getNamaLengkapAttribute()
    {
        return $this->gelar_depan . " " . $this->nama . " " . $this->gelar_belakang;
    }

    public function getStatusBankAttribute()
    {
        return $this->bank != '' && $this->bank_cabang != '' && $this->bank_rek != '' && $this->bank_photo != '' && $this->npwp_nomor != '' && $this->npwp_photo != '';
    }

}
