<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_reviewer extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'penelitian_reviewer';
    protected $primaryKey = 'id';
    protected $appends = ['status_text','deadline_check'];

    public function penelitian(){
    	return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    public function dosen(){
    	return $this->belongsTo('Dosen','dosen','id');
    }

    public function getStatusTextAttribute(){
    	if($this->status=='0'){
    		return 'Belum Mereview';
    	}else if($this->status=='1'){
    		return 'Sudah Mereview';
    	}else if($this->status=='10'){
    		return 'Masa Kadaluarsa';
    	}
    }

    public function getDeadlineCheckAttribute($value){
        $today = strtotime(date("d-m-Y"));
        $deadline = strtotime("+1 day",strtotime($this->deadline));
        if($today<$deadline) return True;
        return False;
    }

    public function getDeadlineAttribute($value){
        $value = explode(' ', $value);
        return $value[0];
    }
}
?>