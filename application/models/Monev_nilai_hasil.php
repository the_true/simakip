<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Monev_nilai_hasil extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'monev_nilai_hasil';
    protected $primaryKey = 'id';
    protected $fillable = array('penelitian', 'dosen');
    protected $appends = ['total_skor','total_nilai'];


    public function penelitian(){
        return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    public function dosen(){
        return $this->belongsTo('Dosen', 'dosen', 'id')->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','surel','hp','jabatan_fungsional','gelar_depan','gelar_belakang','status_dosen'));
    }

    public function getSkorAttribute($value)
    {
        return json_decode($value);
    }

    public function getNilaiAttribute($value)
    {
        return json_decode($value);
    }

    public function getTotalSkorAttribute(){
        $total = 0;
        $total = array_sum($this->skor);
        return $total;
    }

    public function getTotalNilaiAttribute(){
        $total = 0;
        $total = array_sum($this->nilai);
        return $total;
    }

}
?>