<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Target_luaran_sub extends Eloquent
{

    public $timestamps = false;
    protected $table = 'target_luaran_sub';
    protected $primaryKey = 'id';

}
