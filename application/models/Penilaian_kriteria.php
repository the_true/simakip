<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penilaian_kriteria extends Eloquent {
    
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'penilaian_kriteria';
    protected $primaryKey = 'id';
    
    function penilaian() {
        return $this->belongsTo('Penilaian', 'penilaian', 'id');
    }
}