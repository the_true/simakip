<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Batch_penelitian extends Eloquent {

    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'batch_penelitian';
    protected $primaryKey = 'id';
    protected $appends = ["active_batch","expired_batch"];

    function jenis_penelitian() {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id');
    }

    function tahun(){
        return $this->belongsTo('Tahun_kegiatan','tahun','id');
    }

    function batch(){
    	return $this->hasMany('batch','batch_penelitian','id')->where("isdelete","=","0");
    }

    public function getActiveBatchAttribute(){
        foreach($this->batch as $batch){
            if($batch->is_active){
                return $batch;
            }
        }
    }

    public function getExpiredBatchAttribute(){
        foreach($this->batch as $batch){
            if($batch->is_expired){
                return $batch;
            }
        }
    }
}
