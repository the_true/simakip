<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Akses extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'akses';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];
}