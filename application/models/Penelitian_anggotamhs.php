<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_anggotamhs extends Eloquent {

    public $timestamps = FALSE;
    protected $table = 'penelitian_anggotamhs';
    protected $primaryKey = 'id';
    protected $appends = ['konfirmasi_text'];

    function mahasiswa() { 
        return $this->belongsTo('Mahasiswa', 'anggotamhs', 'id')->select(array('id', 'nama','nim','program_studi'));
    }

    function penelitian(){
        return $this->hasOne('Penelitian','id','penelitian');
    }

    public function getKonfirmasiTextAttribute(){
        if($this->konfirmasi=='1') return 'Menyetujui';
        else if($this->konfirmasi=='2') return 'Menolak';
        else return 'Menunggu Konfirmasi';
    	// return $this->konfirmasi=='0'?'Waiting':'Approved';
    }

}
