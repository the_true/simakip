<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Point_log extends Eloquent {

//    Type :
//    1 = dosen
//    2 = anggota
//
//    status :
//    1 = jurnal internasional
//    2 =  jurnal nasional
//    3 = jurnal tidak terakreditasi
//    4 = Pemakalah internasional
//    5 = Pemakalah Nasional
//    6 = Buku
//    7 = HKI
//    8 = Luaran lainnya

   public $timestamps = true;
   protected $table = 'point_log';
   protected $primaryKey = 'id';

   function dosen() {
        return $this->belongsTo('Dosen', 'user_id', 'id');
    }


}
