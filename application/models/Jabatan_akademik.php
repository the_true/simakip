<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Jabatan_akademik extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'jabatan_akademik';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

}