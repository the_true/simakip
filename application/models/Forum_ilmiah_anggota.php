<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Forum_ilmiah_anggota extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'forum_ilmiah_anggota';
    protected $primaryKey = 'id';

    public function forum_ilmiah(){
        return $this->belongsTo('Forum_ilmiah', 'forum_ilmiah', 'id');
    }
    
    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }
}
?>