<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Target_luaran_status extends Eloquent
{

    public $timestamps = false;
    protected $table = 'target_luaran_status';
    protected $primaryKey = 'id';
    
    public function target_luaran_master()
    {
        return $this->belongsTo('Target_luaran_master', 'target_luaran_master_id');
    }

}
