<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Batch extends Eloquent
{

    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'batch';
    protected $primaryKey = 'id';
    protected $appends = ['is_active', 'is_send', 'is_deadlineactive', 'is_expired', 'is_monev_expired', 'is_monevactive', 'is_monevfinish', 'pengumuman_review'];

    public function batch_penelitian()
    {
        return $this->belongsTo('Batch_penelitian', 'batch_penelitian', 'id');
    }

    public function batch_lists()
    {
        return $this->belongsTo('Batch_lists', 'batch_lists', 'id');
    }

    public function getStartAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    public function getFinishAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    // function getMonevFinishAttribute($value){
    //     if(empty($value)) return Null;
    //     return date("d/m/Y", strtotime($value));
    // }

    public function getMonevStartAttribute($value)
    {
        if (empty($value)) {
            return null;
        }

        return date("d/m/Y", strtotime($value));
    }

    public function getMonevFinishAttribute($value)
    {
        if (empty($value)) {
            return null;
        }

        return date("d/m/Y", strtotime($value));
    }

    public function getPengumumanReviewAttribute()
    {
        $date = str_replace('/', '-', $this->deadline_review);
        $deadline_review = strtotime("-1 day", strtotime($date));
        return date("d/m/Y", $deadline_review);
    }

    public function getDeadlineReviewAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    public function getIsDeadlineactiveAttribute()
    {
        if ($this->deadline_review == null) {
            return false;
        }

        $today = strtotime(date("d-m-Y"));
        $finish = strtotime("+1 day", strtotime(str_replace('/', '-', $this->deadline_review)));
        if (($today <= $finish)) {
            return true;
        }
        return false;
    }

    public function getReviewAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    public function getIsActiveAttribute()
    {
        $today = strtotime(date("d-m-Y"));
        $start = strtotime(str_replace('/', '-', $this->start));
        $finish = strtotime(str_replace('/', '-', $this->finish));
        if (($today >= $start) && ($today <= $finish)) {
            return true;
        }

        return false;
    }

    public function getIsMonevactiveAttribute()
    {
        if ($this->monev_start == null) {
            return false;
        }

        $today = strtotime(date("d-m-Y"));
        $start = strtotime(str_replace('/', '-', $this->monev_start));
        if (($today >= $start)) {
            // echo $this->monev_start." true <br>";
            return true;
        }
        return false;
    }

    public function getIsMonevfinishAttribute()
    {
        if ($this->monev_finish == null) {
            return false;
        }

        $today = strtotime(date("d-m-Y"));
        $start = strtotime(str_replace('/', '-', $this->monev_finish));
        if (($today >= $start)) {
            return true;
        }
        return false;
    }

    public function getIsSendAttribute()
    {
        $today = strtotime(date("d-m-Y"));
        $start = strtotime(str_replace('/', '-', $this->start));
        $finish = strtotime(str_replace('/', '-', $this->finish));
        $diff = $today - $start;
        $diff = floor($diff / 3600 / 24);
        if (($today >= $start) && $diff == 0) {
            return true;
        }

        return false;
    }

    public function getIsExpiredAttribute()
    {
        $today = strtotime(date("d-m-Y"));
        $start = strtotime(str_replace('/', '-', $this->start));
        $finish = strtotime(str_replace('/', '-', $this->finish));
        $diff = $today - $finish;
        $diff = floor($diff / 3600 / 24);
        if (($today >= $finish) && $diff == 1) {
            return true;
        }

        return false;
    }

    public function getIsMonevExpiredAttribute()
    {
        $today = strtotime(date("d-m-Y"));
        $finish = strtotime(str_replace('/', '-', $this->monev_finish));
        $diff = $today - $finish;
        $diff = floor($diff / 3600 / 24);
        if (($today >= $finish) && $diff == 1) {
            return true;
        }

        return false;
    }
}
