<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Jabatan_fungsi extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'jabatan_fungsi';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

}