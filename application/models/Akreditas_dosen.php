<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Akreditas_dosen extends Eloquent {

    public $timestamps = false;
    protected $table = 'akreditas_dosen';
    protected $fillable = ['dosen', 'akreditas'];
    protected $primaryKey = 'id';
    protected $appends = ['total_luaran','total_penelitian','total_penelitiantahun','total_jurnal','total_bukuajar','total_forumilmiah','total_hki','total_luaranlain','total_penyelenggaraforumilmiah','total_penelitianhibah'];

    function dosen() {
        return $this->belongsTo('Dosen', 'dosen', 'id')->select(array('id','nama','nidn','jenjang_pendidikan','jabatan_akademik','gelar_depan','gelar_belakang'));
    }

    function akreditas() {
        return $this->belongsTo('Akreditas', 'akreditas', 'id');
    }

    function getTahunRangeAttribute(){
      if(!is_null($this->tahun_range_cache)){
        return $this->tahun_range_cache;
      }
      $akreditas = $this->akreditas()->first();
      $tahun_dari = (int) $akreditas->tahun_dari()->first()->tahun;
      $tahun_sampai = (int) $akreditas->tahun_sampai()->first()->tahun;
      $data = [];
      for($v=$tahun_dari;$v<=$tahun_sampai;$v++){
        $data[]=$v;
      }
      $this->tahun_range_cache=$data;
      return $data;
    }

    function getTotalJurnalAttribute(){
      if(!is_null($this->total_jurnal_cache)){
        return $this->total_jurnal_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $jurnal = $this->dosen()->first()->jurnal()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $jurnal = $jurnal->toArray();
      foreach($jurnal as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_jurnal_cache=$data;
      return $data;
    }

    function getTotalBukuajarAttribute(){
      if(!is_null($this->total_bukuajar_cache)){
        return $this->total_bukuajar_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $buku_ajar = $this->dosen()->first()->buku_ajar()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $buku_ajar = $buku_ajar->toArray();
      foreach($buku_ajar as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_bukuajar_cache=$data;
      return $data;
    }

    function getTotalForumilmiahAttribute(){
      if(!is_null($this->total_forumilmiah_cache)){
        return $this->total_forumilmiah_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $forum_ilmiah = $this->dosen()->first()->forum_ilmiah()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $forum_ilmiah = $forum_ilmiah->toArray();
      foreach($forum_ilmiah as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_forumilmiah_cache=$data;
      return $data;
    }

    function getTotalHkiAttribute(){
      if(!is_null($this->total_hki_cache)){
        return $this->total_hki_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $hki = $this->dosen()->first()->hki()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $hki = $hki->toArray();
      foreach($hki as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_hki_cache=$data;
      return $data;
    }

    function getTotalLuaranlainAttribute(){
      if(!is_null($this->total_luaranlain_cache)){
        return $this->total_luaranlain_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $luaran_lain = $this->dosen()->first()->luaran_lain()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $luaran_lain = $luaran_lain->toArray();
      foreach($luaran_lain as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_luaranlain_cache=$data;
      return $data;
    }

    function getTotalPenyelenggaraforumilmiahAttribute(){
      if(!is_null($this->total_penyelenggaraforumilmiah_cache)){
        return $this->total_penyelenggaraforumilmiah_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $penyelenggara_forum_ilmiah = $this->dosen()->first()->penyelenggara_forum_ilmiah()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $penyelenggara_forum_ilmiah = $penyelenggara_forum_ilmiah->toArray();
      foreach($penyelenggara_forum_ilmiah as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_penyelenggaraforumilmiah_cache=$data;
      return $data;
    }

    function getTotalPenelitianhibahAttribute(){
      if(!is_null($this->total_penelitianhibah_cache)){
        return $this->total_penelitianhibah_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $penelitian_hibah = $this->dosen()->first()->penelitian_hibah()->where('isdelete','=','0')->where('isvalidate','=','1')->whereHas('tahun',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun')
      ->selectRaw('tahun, count(tahun) AS total')->with('tahun')->get();
      $penelitian_hibah = $penelitian_hibah->toArray();
      foreach($penelitian_hibah as $val){
        if(!array_key_exists("tahun",$val)) continue;
        $data[$val["tahun"]["tahun"]] = $val["total"];
      }
      $this->total_penelitianhibah_cache=$data;
      return $data;
    }

    function getTotalLuaranAttribute(){

      $jurnal = $this->total_jurnal;
      $bukuajar = $this->total_bukuajar;
      $forumilmiah = $this->total_forumilmiah;
      $hki = $this->total_hki;
      $luaranlain = $this->total_luaranlain;
      $penyelenggaraforumilmiah = $this->total_penyelenggaraforumilmiah;
      $penelitianhibah = $this->total_penelitianhibah;

      $range = $this->tahun_range;
      $total =0;
      foreach ($range as $value) {
        $total+=$jurnal[$value];
        $total+=$bukuajar[$value];
        $total+=$forumilmiah[$value];
        $total+=$hki[$value];
        $total+=$luaranlain[$value];
        $total+=$penyelenggaraforumilmiah[$value];
        $total+=$penelitianhibah[$value];
      }
      return $total;
    }

    function getTotalPenelitiantahunAttribute(){
      if(!is_null($this->total_penelitian_cache)){
        return $this->total_penelitian_cache;
      }
      $data = [];
      foreach($this->tahun_range as $tahun){
        $data[$tahun]=0;
      }
      $penelitian = $this->dosen()->first()->penelitian()->where('isdelete','=','0')->where('status','=','4')->whereHas('tahun_kegiatan',
      function($q){$q->whereIn('tahun',$this->tahun_range);})->groupby('tahun_kegiatan')
      ->selectRaw('tahun_kegiatan, count(tahun_kegiatan) AS total')->with('tahun_kegiatan')->get();
      $penelitian = $penelitian->toArray();
      foreach($penelitian as $val){
        $data[$val["tahun_kegiatan"]["tahun"]] = $val["total"];
      }
      $this->total_penelitian_cache=$data;
      return $data;
    }

    function getTotalPenelitianAttribute(){

      $penelitian = $this->total_penelitiantahun;

      $range = $this->tahun_range;
      $total =0;
      foreach ($range as $value) {
        $total+=$penelitian[$value];
      }
      return $total;
    }
}
