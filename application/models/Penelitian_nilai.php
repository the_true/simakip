<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_nilai extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'penelitian_nilai';
    protected $primaryKey = 'id';
    protected $fillable = array('penelitian_review', 'penelitian', 'penilaian_kriteria');

    public function penilaian_kriteria(){
    	return $this->belongsTo('Penilaian_kriteria','penilaian_kriteria','id')->select(array('id','nama','indikator','bobot'));
    }
}
?>