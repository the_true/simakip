<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Surat_pengabdian extends Eloquent {

    public $timestamps = FALSE;
    protected $table = 'surat';
    protected $primaryKey = 'id';
    protected $appends = ['status_text'];

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where("type", '=', "pengabdian");
    }
    
	function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }
}
