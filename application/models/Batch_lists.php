<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Batch_lists extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'batch_lists';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

    public function scopePengabdian($query)
    {
        return $query->where('type', '=', 'pengabdian');
    }

    public function scopePenelitian($query)
    {
        return $query->where('type', '=', 'penelitian');
    }

}