<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Target_luaran_master extends Eloquent
{

    public $timestamps = false;
    protected $table = 'target_luaran_master';
    protected $primaryKey = 'id';
    
    function target_luaran_sub()
    {
        return $this->hasMany('Target_luaran_sub');
    }
    
    function target_luaran_status(){
        return $this->hasMany('Target_luaran_status');
    }

}
