<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_laporan extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'penelitian_laporan';
    protected $primaryKey = 'id';
    protected $appends = ["status_text"];

    public function penelitian(){
    	return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    function getStatusTextAttribute(){
        if($this->status==0) return '<span class="text-warning">Menunggu Pengecekan</span>';
        else if($this->status==1) return '<span class="text-success">Valid</span>';
        else if($this->status==2) return '<span class="text-danger">Belum Valid</span>';
        else if($this->status==3) return '<span class="text-warning">Menunggu Pengecekan Perbaikan</span>';
    }
}
?>