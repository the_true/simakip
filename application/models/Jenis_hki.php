<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Jenis_hki extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'jenis_hki';
    protected $primaryKey = 'id';
    protected $fillable = ['body'];

}