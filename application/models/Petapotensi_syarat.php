<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Petapotensi_syarat extends Eloquent {
    
    public $timestamps = True;
    protected $table = 'peta_potensi_syarat';
    protected $primaryKey = 'id';
     protected $appends = ['luaran_text'];
    public $key = ["Publikasi Jurnal - Internasional","Publikasi Jurnal - Nasional Terakreditasi","Publikasi Jurnal - Nasional Tidak Terakreditasi","Buku Ajar/Teks","Pemakalah Forum Ilmiah - Internasional","Pemakalah Forum Ilmiah - Nasional","Pemakalah Forum Ilmiah - Regional","Hak Kekayaan Intelektual","Luaran Lain","Penyelenggara Forum Ilmiah","Penelitian Mandiri"];

    function peta_potensi() {
        return $this->belongsTo('Petapotensi', 'peta_potensi', 'id');
    }

    function jabatan_akademik(){
    	return $this->belongsTo('Jabatan_akademik','jabatan_akademik','id');
    }

    public function getLuaranTextAttribute(){
        $luaran = $this->luaran;
        $hasil = [];
        foreach ($luaran as $value) {
            $hasil[] = $this->key[(int) $value-1];
        }
        return $hasil;
    }

    public function getLuaranAttribute($value){
        return json_decode($value);
    }

    public function getJumlahAttribute($value){
        return json_decode($value);
    }

}