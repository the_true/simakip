<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_reviewer extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'pengabdian_reviewer';
    protected $primaryKey = 'id';
    protected $appends = ['status_text','deadline_check','nomor_urut'];

    public function pengabdian(){
    	return $this->belongsTo('Pengabdian', 'pengabdian', 'id');
    }

    public function dosen(){
    	return $this->belongsTo('Dosen','dosen','id');
    }

    public function getStatusTextAttribute(){
    	if($this->status=='0'){
    		return 'Belum Mereview';
    	}else if($this->status=='1'){
    		return 'Sudah Mereview';
    	}else if($this->status=='10'){
    		return 'Masa Kadaluarsa';
    	}
    }

    public function getNomorUrutAttribute(){
        $reviewers = Pengabdian_reviewer::where('pengabdian','=',$this->pengabdian)->where('isdelete','=','0')->get();
        foreach($reviewers as $key=>$value){
            if($value->dosen == $this->dosen){
                return $key+1;
            }
        }
        return 0;
    }

    public function getDeadlineCheckAttribute($value){
        $today = strtotime(date("d-m-Y"));
        $deadline = strtotime("+1 day",strtotime($this->deadline));
        if($today<$deadline) return True;
        return False;
    }

    public function getDeadlineAttribute($value){
        $value = explode(' ', $value);
        return $value[0];
    }
}
?>