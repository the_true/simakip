<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Penelitian extends Eloquent
{

    public $timestamps = true;
    protected $table = 'penelitian';
    protected $primaryKey = 'id';
    protected $appends = ["status_text", 'rp_total_biaya', 'rp_mandiri_biaya_instansi', 'rp_mandiri_biaya_total'];

    public function jenis_penelitian()
    {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id')->select(array('id', 'nama'));
    }

    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
        // ->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','surel','hp','jabatan_fungsional','gelar_depan','gelar_belakang','status_dosen','fakultas','bank','bank'))
    }

    public function target_luaran()
    {
        return $this->belongsTo('Target_luaran', 'target', 'id');
    }

    public function target_luarans()
    {
        return $this->hasMany('Penelitian_target_luaran', 'penelitian', 'id');
    }
    
    public function penelitian_target_luaran_wajib()
    {
        return $this->hasMany('Penelitian_target_luaran_wajib');
    }
    
    public function penelitian_target_luaran_tambahan()
    {
        return $this->hasMany('Penelitian_target_luaran_tambahan');
    }

    public function dosen_mengetahui()
    {
        return $this->belongsTo('Dosen', 'dosen_mengetahui', 'id')->select(array('id', 'nama', 'jenjang_pendidikan', 'jabatan_akademik', 'program_studi', 'nidn', 'jabatan_fungsional', 'gelar_depan', 'gelar_belakang'));
    }

    public function dosen_menyetujui()
    {
        return $this->belongsTo('Dosen', 'dosen_menyetujui', 'id')->select(array('id', 'nama', 'jenjang_pendidikan', 'jabatan_akademik', 'program_studi', 'nidn', 'jabatan_fungsional', 'gelar_depan', 'gelar_belakang'));
    }

    public function subject_penelitian()
    {
        return $this->hasMany('subject_penelitian', 'data', 'id')->where('type', '=', '7');
    }

    public function tahun_kegiatan()
    {
        return $this->belongsTo('Tahun_kegiatan', 'tahun_kegiatan', 'id')->select(array('id', 'tahun'));
    }

    public function anggota()
    {
        return $this->hasMany('Penelitian_anggota', 'penelitian', 'id')->select(array('*'));
    }
    public function anggotamhs()
    {
        return $this->hasMany('Penelitian_anggotamhs', 'penelitian', 'id')->select(array('*'));
    }

    public function penelitian_reviewer()
    {
        return $this->hasMany('Penelitian_reviewer', 'penelitian', 'id')->orderby('dosen', 'ASC');
    }
    
    public function monev_reviewer()
    {
        return $this->hasMany('Monev_reviewer', 'penelitian', 'id');
    }
    
    public function monev_luaran()
    {
        return $this->hasMany('Monev_luaran', 'penelitian', 'id');
    }

    public function penelitian_laporan()
    {
        return $this->hasOne('Penelitian_laporan', 'penelitian', 'id');
    }

    public function pencairan_dana_tahap1()
    {
        return $this->hasOne('Pencairan_dana_a', 'penelitian', 'id');
    }

    public function pencairan_dana_tahap2()
    {
        return $this->hasOne('Pencairan_dana_b', 'penelitian', 'id');
    }

    public function monev()
    {
        return $this->hasOne('Monev', 'penelitian', 'id');
    }

    public function surat_kontrak()
    {
        return $this->hasOne('Surat_kontrak', 'penelitian', 'id')->where('type', '=', 'penelitian');
    }

    public function batch()
    {
        return $this->belongsTo('Batch', 'batch', 'id')->select(array('id', 'nama', 'start', 'finish', 'batch_penelitian', 'monev_start', 'monev_finish'));
    }

    public function penelitian_review()
    {
        return $this->hasMany('Penelitian_review', 'penelitian', 'id');
    }

    public function penelitian_review_inselected()
    {
        return $this->hasMany('Penelitian_review', 'penelitian', 'id')->where('isselected', '=', '1')->where('isvalid', '=', '1');
    }

    // public function monev_review_inselected(){
    //     return $this->hasMany('Monev_review','penelitian','id')->where('isselected','=','1');
    // }

    public function penelitian_review_exselected()
    {
        return $this->hasMany('Penelitian_review', 'penelitian', 'id')->where('isselected', '=', '0')->where('isvalid', '=', '1')->orderby('dosen', 'ASC');
    }

    public function getStatusTextAttribute()
    {
        if ($this->status == 1) {
            return "Belum Submit";
        } else if ($this->status == 2) {
            return "Proses Pengecekan";
        } else if ($this->status == 3) {
            return "Proses review";
        } else if ($this->status == 4) {
            return "Diterima";
        } else if ($this->status == 7) {
            return "Proses Pengecekan Perbaikan";
        } else if ($this->status == 9) {
            return "Perbaikan Belum Valid";
        } else if ($this->status == 10) {
            return "Belum Valid";
        } else if ($this->status == 11) {
            return "Diterima Dengan Perbaikan";
        } else if ($this->status == 12) {
            return "Ditolak";
        }

    }
    public function getRpTotalBiayaAttribute()
    {
        return number_format($this->total_biaya, 0, '.', '.');
    }
    public function getRpMandiriBiayaInstansiAttribute()
    {
        return number_format($this->mandiri_biaya_instansi, 0, '.', '.');
    }
    public function getRpMandiriBiayaTotalAttribute()
    {
        return number_format($this->mandiri_total_biaya, 0, '.', '.');
    }
    
    public function luaran_wajib()
    {
        return $this->hasOne('Penelitian_target_luaran_wajib');
    }
    
    public function luaran_tambahan()
    {
        return $this->hasOne('Penelitian_target_luaran_tambahan');
    }
}
