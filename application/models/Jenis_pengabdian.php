<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Jenis_pengabdian extends Eloquent
{

    public $timestamps = false;
    protected $table = 'jenis_pengabdian';
    protected $primaryKey = 'id';
    protected $appends = ["active_batch", "active_penilaian", "status_text", "active_bobothasil", "bobot_luaran_aktif", "bobot_hasil_aktif"];

    public function batas_anggaran_aktif()
    {
        return $this->hasOne('batas_anggaran', 'jenis_penelitian', 'id')->where("status", "=", "1")->where("isdelete", "=", "0");
    }

    public function syarat_penelitian()
    {
        return $this->hasMany('Syarat_penelitian', 'jenis_penelitian', 'id');
    }

    // public function bobot_luaran_aktif()
    // {
    //     return $this->hasOne('bobot_luaran', 'jenis_penelitian', 'id')->where("status", "=", "1")->where("isdelete", "=", "0");
    // }

    // public function bobot_hasil_aktif()
    // {
    //     return $this->hasOne('bobot_hasil', 'jenis_penelitian', 'id')->where("status", "=", "1")->where("isdelete", "=", "0");
    // }

    public function keanggotaan()
    {
        return $this->hasOne('Keanggotaan', 'jenis_penelitian', 'id');
    }

    public function penilaian()
    {
        return $this->hasMany('penilaian', 'jenis_penelitian', 'id');
    }

    // function batch_penelitian(){
    //     return Batch_penelitian::where("isdelete","=","0")->whereNull("jenis_penelitian");
    // }

    public function jabatan_akademik()
    {
        return $this->belongsTo('Jabatan_akademik', 'jabatan_akademik', 'id');
    }

    public function getStatusTextAttribute()
    {
        return $this->status == '1' ? 'Active' : 'Non Active';
    }

    public function bobot_hasil()
    {
        return $this->hasMany('bobot_hasil', 'jenis_penelitian', 'id')->where("isdelete", "=", "0");
    }

    public function bobot_luaran()
    {
        return $this->hasMany('bobot_luaran', 'jenis_penelitian', 'id')->where("isdelete", "=", "0");
    }

    public function getActiveBatchAttribute()
    {
        // if(!array_key_exists('batch_penelitian', $this->getRelations()))return;
        if (!class_exists('batch_penelitian')) {
            return;
        }

        $this->batch_penelitian = Batch_penelitian::where("isdelete", "=", "0")->whereNull("jenis_penelitian")->get();

        $batchs = null;
        $data = [];
        foreach ($this->batch_penelitian as $batch) {
            if (!is_null($batch->active_batch)) {
                $batchs = $batch->active_batch;
                break;
                $data[] = $batch->active_batch;
            }
        }
        return $batchs;
    }

    public function getActivePenilaianAttribute()
    {
        // if(!array_key_exists('penilaian', $this->getRelations()))return;
        if (!class_exists('penilaian')) {
            return;
        }

        $data = [];
        foreach ($this->penilaian as $penilaian) {
            if (!is_null($penilaian->active_penilaian)) {
                return $penilaian->active_penilaian;
            }

            $data[] = $penilaian->active_penilaian;
        }
        return null;
    }

    public function getActiveBobothasilAttribute()
    {
        // if(!array_key_exists('penilaian', $this->getRelations()))return;
        if (!class_exists('bobot_hasil')) {
            return;
        }

        $data = [];
        foreach ($this->bobot_hasil as $penilaian) {
            if (!is_null($penilaian->active_penilaian)) {
                return $penilaian->active_penilaian;
            }

            $data[] = $penilaian->active_penilaian;
        }
        return null;
    }

    public function getBobotLuaranAktifAttribute()
    {
        // if(!array_key_exists('penilaian', $this->getRelations()))return;
        if (!class_exists('bobot_luaran')) {
            return;
        }

        foreach ($this->bobot_luaran as $penilaian) {
            if ($penilaian->status == 1) {
                return $penilaian;
            }
        }
        return null;
    }

    public function getBobotHasilAktifAttribute()
    {
        // if(!array_key_exists('penilaian', $this->getRelations()))return;
        if (!class_exists('bobot_hasil')) {
            return;
        }

        foreach ($this->bobot_hasil as $penilaian) {
            if ($penilaian->status == 1) {
                return $penilaian;
            }
        }
        return null;
    }

}
