<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Subject_tahun extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'subject_tahun';
    protected $primaryKey = 'id';

    public function tahun_dari(){
      return $this->belongsTo('Tahun_kegiatan', 'tahun_dari', 'id')->select(array('id', 'tahun'));
    }

    public function tahun_sampai(){
    	return $this->belongsTo('Tahun_kegiatan', 'tahun_sampai', 'id')->select(array('id', 'tahun'));
    }

}