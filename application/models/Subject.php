<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Subject extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'subject';
    protected $primaryKey = 'id';
    protected $appends = ['total'];

    public function penelitian(){
      return $this->hasMany('Subject_penelitian','subject','id');
    }

    public function parentnya(){
        return $this->belongsTo('Subject', 'parent', 'id')->with('parentnya');
    }

    public function child(){
        return $this->hasMany('subject','parent','id')->with('child');
    }

	function getTotalAttribute(){
        return $this->penelitian()->count();
    }
}