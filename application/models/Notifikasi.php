<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Notifikasi extends Eloquent {

    public $timestamps = TRUE;
    protected $table = 'notifikasi';
    protected $primaryKey = 'id';
    protected $fillable = [];
}
