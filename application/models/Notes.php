<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Notes extends Eloquent
{

    public $timestamps = false;
    protected $table = 'notes';
    protected $primaryKey = 'id';
    protected $fillable = ['string'];

}
