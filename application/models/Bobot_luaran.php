<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Bobot_luaran extends Eloquent {
    
    public $timestamps = FALSE;
    protected $table = 'bobot_luaran';
    protected $primaryKey = 'id';
     protected $appends = ['status_text','active_penilaian'];

    function jenis_penelitian() {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id');
    }

    function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }

    public function getActivePenilaianAttribute(){
        if($this->status==1){
            return $this->penilaian_kriteria;
        }
    }

}