<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pajak extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'pajak';
    protected $primaryKey = 'id';
    protected $fillable = ['nominal'];
    protected $appends = ['status_text'];

	function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }

}