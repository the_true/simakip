<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Syarat_penelitian extends Eloquent {
    
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'syarat_penelitian';
    protected $primaryKey = 'id';
    

    function jenis_penelitian() {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id');
    }

    function jabatan_akademik(){
        return $this->belongsTo('Jabatan_akademik','jabatan_akademik','id');
    }
}