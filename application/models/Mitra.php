<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Mitra extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'mitra';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

}