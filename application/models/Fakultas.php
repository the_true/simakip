<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Fakultas extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'fakultas';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];
}