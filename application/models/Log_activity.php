<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Log_Activity extends Eloquent {

    public $timestamps = TRUE;
    protected $table = 'log';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $appends = ['menu_text','aksi_text'];

    function dosen() {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }

    function getMenuTextAttribute(){
    	switch($this->type){
            case 0:
                return 'Profile Pribadi';
                break;
    		case 1:
                return 'Kinerja/Luaran';
    			break;
			case 2:
				return 'Pengguna Dosen';
				break;
			case 3:
                return 'Penelitian';
				break;
			case 4:
                return 'Surat Kontrak';
				break;
			case 5:
                return 'Pengumuman';
				break;
			case 6:
                return 'Konfigurasi';
                break;
            case 8:
                return 'Pengabdian';
				break;
			default:
				return '';
				break;
    	}
    }

    function getAksiTextATtribute(){
        switch($this->action){
            case 'insert':
                return 'Tambah';
                break;
            case 'edit':
                return 'Ubah';
                break;
            case 'delete':
                return 'Hapus';
                break;
            case 'validate':
                return 'Validasi';
                break;
            default:
                return $this->action;
                break;
        }
    }
}
