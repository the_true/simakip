<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Monev_luaran extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'monev_luaran';
    protected $primaryKey = 'id';
    protected $fillable = array('penelitian');
    // protected $appends = ['status_text','rp_rekomendasi'];


    public function penelitian(){
        return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    public function getContentAttribute($value)
    {
        return json_decode($value);
    }

}
?>