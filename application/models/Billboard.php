<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Billboard extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'billboard';
    protected $primaryKey = 'id';
   	protected $appends = ['status_text'];

	function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }
}