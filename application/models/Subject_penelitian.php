<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Subject_penelitian extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'subject_penelitian';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'subject', 'data', 'judul'];
    protected $appends = ['type_text'];

    function subject() {
        return $this->belongsTo('Subject', 'subject', 'id');
    }

	function getTypeTextAttribute(){
        $tipe = "";
        switch($this->type){
        	case 1:
        		$tipe = "Publikasi Jurnal";
        		break;
    		case 2:
    			$tipe = "Buku Ajar";
    			break;
			case 3:
				$tipe = "Pemakalah";
				break;
			case 4:
				$tipe = "HKI";
				break;
			case 5:
				$tipe = "Luaran Lain";
				break;
			case 6:
				$tipe = "Penelitian Mandiri";
				break;
			case 7:
				$tipe = "Penelitian Internal";
				break;
    		default:
    			break;
        }
        return $tipe;
    }
}