<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Sumberdana extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'sumberdana';
    protected $primaryKey = 'id';
    protected $fillable = ['sumberdana'];
}