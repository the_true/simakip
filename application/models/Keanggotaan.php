<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Keanggotaan extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'keanggotaan';
    protected $primaryKey = 'id';
    protected $fillable = ['isi'];
}