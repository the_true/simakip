<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Subbidangkeahlian extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'subbidangkeahlian';
    protected $primaryKey = 'id';
    protected $fillable = ['subbidangkeahlian'];
    
    function bidangkeahlian() {
        return $this->belongsTo('Bidangkeahlian', 'bidangkeahlian', 'id');
    }
}