<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Format_nomor extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'format_nomor';
    protected $primaryKey = 'id';
    // protected $fillable = ['judul'];
    protected $appends = ['status_text'];

    public function newQuery($excludeDeleted = true) {
        return parent::newQuery($excludeDeleted)
            ->where("type", '=', "penelitian");
    }

    function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }
}
?>