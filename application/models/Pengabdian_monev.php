<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_monev extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'pengabdian_monev';
    protected $primaryKey = 'id';
    protected $appends = ['status_berkas','status_borang','status_reviewer','status_review'];

    public function pengabdian(){
    	return $this->belongsTo('Pengabdian', 'pengabdian', 'id');
    }

    public function getStatusBerkasAttribute(){
    	if(!isset($this->berkas)){
    		return 'Belum';
    	}else{
    		return 'Sudah';
    	}
    }

    public function getStatusBorangAttribute(){
        if($this->borang==0){
            return 'Belum';
        }else{
            return 'Sudah';
        }
    }

    public function getStatusReviewerAttribute(){
        if($this->reviewer==0){
            return 'Belum';
        }else{
            return 'Sudah';
        }
    }

    public function getStatusReviewAttribute(){
        if($this->hasilreview==0){
            return 'Belum';
        }else{
            return 'Sudah';
        }
    }
}
?>