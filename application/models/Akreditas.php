<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Akreditas extends Eloquent {

    public $timestamps = false;
    protected $table = 'akreditas';
    protected $primaryKey = 'id';
    protected $appends = ['klasifikasi_anggota','total_tingkat','tahun_range','anggota_cache','total_jurnal','total_forumilmiah','total_bukuajar','total_hki','total_luaranlain','total_penelitian','total_penelitianhibah'];
    // protected $fillable = ['nama'];

    function dosen() {
        return $this->belongsTo('Dosen', 'dosen', 'id')->select(array('id','nama','nidn','jenjang_pendidikan','jabatan_akademik','gelar_depan','gelar_belakang'));
    }

    function fakultas(){
        return $this->belongsTo('Fakultas', 'fakultas', 'id');
    }

    function program_studi(){
        return $this->belongsTo('Program_studi', 'program_studi', 'id')->select(array('id', 'nama'));
    }

    public function tahun_dari(){
      return $this->belongsTo('Tahun_kegiatan', 'tahun_dari', 'id')->select(array('id', 'tahun'));
    }

    public function tahun_sampai(){
    	return $this->belongsTo('Tahun_kegiatan', 'tahun_sampai', 'id')->select(array('id', 'tahun'));
    }

    public function anggota(){
      return $this->hasMany('akreditas_dosen','akreditas','id');
    }

    function getKlasifiasiAkademikAttribute(){

    }

    function getTahunRangeAttribute(){
      if(!is_null($this->tahun_range_cache)){
        return $this->tahun_range_cache;
      }
      $tahun_dari = (int) $this->tahun_dari()->first()->tahun;
      $tahun_sampai = (int) $this->tahun_sampai()->first()->tahun;
      $data = [];
      $datax= [];
      for($v=$tahun_dari;$v<=$tahun_sampai;$v++){
        $data[$v]=0;
        $datax[]=$v;
      }
      $this->tahun_range_cache=$data;
      $this->tahun_range_array=$datax;
      return $data;
    }

    function getTotalJurnalAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_jurnal"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getTotalBukuajarAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_bukuajar"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getTotalForumilmiahAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_forumilmiah"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getTotalHkiAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_hki"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getTotalPenelitianhibahAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_penelitianhibah"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getTotalLuaranlainAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_luaranlain"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getTotalPenelitianAttribute(){
      $anggota = $this->anggota_cache;
      $tahun = $this->tahun_range;
      foreach ($anggota as $value) {
        foreach($value["total_penelitiantahun"] as $key=>$val){
            $tahun[$key]+=$val;
        }
      }
      return $tahun;
    }

    function getAnggotaCacheAttribute(){
      if(!is_null($this->anggota_cachex)){
        return $this->anggota_cachex;
      }
      $anggota = $this->anggota()->with('dosen')->get()->toArray();
      $this->anggota_cachex = $anggota;
      return $anggota;
    }

    function getKlasifikasiAnggotaAttribute(){
      $data = [];
      $dosen = $this->anggota()->with('dosen')->with('dosen.jabatan_akademik')->get()->toArray();
      foreach ($dosen as $item){
        if(!array_key_exists($item["dosen"]["jabatan_akademik"]["nama"],$data)) $data[$item["dosen"]["jabatan_akademik"]["nama"]]=1;
        else $data[$item["dosen"]["jabatan_akademik"]["nama"]] += 1;
      }
      return $data;
    }

    function getTotalTingkatAttribute(){
      $data = ["S1"=>0,"S2"=>0,"S3"=>0];
      $dosen = $this->anggota()->with('dosen')->get()->toArray();
      // echo $dosen; die;
      foreach ($dosen as $item){
        $data[$item["dosen"]["tingkat"]] += 1;
      }
      // echo json_encode($data); die;
      return $data;
    }

}
