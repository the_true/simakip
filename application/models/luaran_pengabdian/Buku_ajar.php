<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Buku_ajar extends Eloquent
{

    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'buku_ajar';
    protected $primaryKey = 'id';
    protected $appends = ['isvalidate_show'];

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)
            ->where("identifier", '=', "pengabdian");
    }

    public function tahun_kegiatan()
    {
        return $this->belongsTo('Tahun_kegiatan', 'tahun_kegiatan', 'id');
    }

    public function subject_penelitian()
    {
        return $this->hasMany('subject_penelitian', 'data', 'id')->where('type', '=', '2');
    }

    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }

    public function getIsvalidateShowAttribute()
    {
        if ($this->isvalidate == 1) {
            return "Valid";
        } else if ($this->isvalidate == 0) {
            return "Belum di Validasi";
        } else if ($this->isvalidate == 2) {
            return "Belum Valid";
        } else if ($this->isvalidate == 3) {
            return "Belum Disubmit";
        }

    }
}
