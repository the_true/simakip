<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Jurnal extends Eloquent{

    public $timestamps = true;
    protected $fillable = ['id', 'dosen', 'personil', 'judul',
        'nama_jurnal', 'issn', 'volume',
        'nomor', 'halaman', 'url', 'berkas'];
    protected $table = 'jurnal';
    protected $primaryKey = 'id';
    protected $appends = ['halaman_mulai', 'halaman_akhir', 'keanggotaan_dosen_show', 'keanggotaan_nondosen_show', 'isvalidate_show', 'type_show'];

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)
            ->where("identifier", '=', "pengabdian");
    }

    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }

    public function personil()
    {
        return $this->belongsTo('Dosen', 'personil', 'id');
    }

    public function jurnal_anggota()
    {
        return $this->hasMany('Jurnal_anggota', 'jurnal', 'id');
    }

    public function tahun_kegiatan()
    {
        return $this->belongsTo('Tahun_kegiatan', 'tahun_kegiatan', 'id');
    }

    public function subject_penelitian()
    {
        return $this->hasMany('subject_penelitian', 'data', 'id')->where('type', '=', '1');
    }

    public function getHalamanMulaiAttribute()
    {
        $result = explode(" - ", $this->halaman);
        if (count($result) > 1) {
            return $result[0];
        }

    }

    public function getHalamanAkhirAttribute()
    {
        $result = explode(" - ", $this->halaman);
        if (count($result) > 1) {
            return $result[1];
        }

    }

    public function getKeanggotaanDosenShowAttribute()
    {
        return $this->keanggotaan_dosen == 1 ? 'Ketua' : 'Anggota';
    }

    public function getKeanggotaanNondosenShowAttribute()
    {
        return $this->keanggotaan_nondosen == 1 ? 'Ketua' : 'Anggota';
    }

    public function getIsvalidateShowAttribute()
    {
        if ($this->isvalidate == 1) {
            return "Valid";
        } else if ($this->isvalidate == 0) {
            return "Belum di Validasi";
        } else if ($this->isvalidate == 2) {
            return "Belum Valid";
        } else if ($this->isvalidate == 3) {
            return "Belum Disubmit";
        }

    }

    public function getTypeShowAttribute()
    {
        if ($this->type == 1) {
            return "International";
        } else if ($this->type == 2) {
            return "Nasional Terakreditasi";
        } else if ($this->type == 3) {
            return "Nasional Tidak Terakreditasi";
        }

    }
}
