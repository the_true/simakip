<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Pengabdian_mandiri extends Eloquent
{

    public $timestamps = true;
    protected $table = 'penelitian_hibah';
    protected $primaryKey = 'id';
    protected $fillable = ['judul'];
    protected $appends = ['isvalidate_show', 'rp_anggaran'];

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)
            ->where("identifier", '=', "pengabdian");
    }

    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }

    public function tahun()
    {
        return $this->belongsTo('Tahun_kegiatan', 'tahun', 'id');
    }

    public function subject_penelitian()
    {
        return $this->hasMany('subject_penelitian', 'data', 'id')->where('type', '=', '6');
    }

    public function getIsvalidateShowAttribute()
    {
        if ($this->isvalidate == 1) {
            return "Valid";
        } else if ($this->isvalidate == 0) {
            return "Belum di Validasi";
        } else if ($this->isvalidate == 2) {
            return "Belum Valid";
        } else if ($this->isvalidate == 3) {
            return "Belum Disubmit";
        }

    }

    public function getRpAnggaranAttribute()
    {
        return number_format($this->anggaran, 0, '.', '.');
    }
}
