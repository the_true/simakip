<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_monev_luaran extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'pengabdian_monev_luaran';
    protected $primaryKey = 'id';
    protected $fillable = array('pengabdian');
    // protected $appends = ['status_text','rp_rekomendasi'];


    public function pengabdian(){
        return $this->belongsTo('Pengabdian', 'pengabdian', 'id');
    }

    public function getContentAttribute($value)
    {
        return json_decode($value);
    }

}
?>