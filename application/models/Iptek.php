<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Iptek extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'iptek';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

}