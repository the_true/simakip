<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Jenis_penelitian extends Eloquent {

    public $timestamps = false;
    protected $table = 'jenis_penelitian';
    protected $primaryKey = 'id';
    protected $appends = ["active_batch","active_penilaian","expired_batch","active_bobothasil"];

    function batas_anggaran_aktif() {
        return $this->hasMany('batas_anggaran', 'jenis_penelitian', 'id')->where("status","=","1")->where("isdelete","=","0");
    }

    function bobot_luaran_aktif() {
        return $this->hasOne('bobot_luaran', 'jenis_penelitian', 'id')->where("status","=","1")->where("isdelete","=","0");
    }

    function bobot_hasil_aktif() {
        return $this->hasOne('bobot_hasil', 'jenis_penelitian', 'id')->where("status","=","1")->where("isdelete","=","0");
    }

    function batch_penelitian(){
    	return $this->hasMany('batch_penelitian', 'jenis_penelitian', 'id')->where("isdelete","=","0");
    }

    public function syarat_penelitian(){
        return $this->hasMany('Syarat_penelitian','jenis_penelitian','id');
    }

    public function keanggotaan(){
        return $this->hasOne('Keanggotaan','jenis_penelitian','id');
    }

    function penilaian(){
        return $this->hasMany('penilaian', 'jenis_penelitian', 'id');
    }

    function bobot_hasil(){
        return $this->hasMany('bobot_hasil', 'jenis_penelitian', 'id');
    }

    function jabatan_akademik(){
        return $this->belongsTo('Jabatan_akademik', 'jabatan_akademik', 'id');
    }

    public function getActiveBatchAttribute(){
        // if(!array_key_exists('batch_penelitian', $this->getRelations()))return;
        if(!class_exists('batch_penelitian')) return;
    	$data = [];
    	foreach ($this->batch_penelitian as $batch){
    		if(!is_null($batch->active_batch))
    			return $batch->active_batch;
    			$data[] = $batch->active_batch;
    	}
    	return Null;
    }

    public function getExpiredBatchAttribute(){
      // if(!array_key_exists('batch_penelitian', $this->getRelations()))return;
      if(!class_exists('batch_penelitian')) return;
      $data = [];
      foreach ($this->batch_penelitian as $batch){
        if(!is_null($batch->expired_batch))
          return $batch->expired_batch;
          $data[] = $batch->expired_batch;
      }
      return Null;
    }

    public function getActivePenilaianAttribute(){
        // if(!array_key_exists('penilaian', $this->getRelations()))return;
        if(!class_exists('penilaian')) return;
        $data = [];
        foreach ($this->penilaian as $penilaian){
            if(!is_null($penilaian->active_penilaian))
                return $penilaian->active_penilaian;
                $data[] = $penilaian->active_penilaian;
        }
        return Null;
    }

    public function getActiveBobothasilAttribute(){
        // if(!array_key_exists('penilaian', $this->getRelations()))return;
        if(!class_exists('bobot_hasil')) return;
        $data = [];
        foreach ($this->bobot_hasil as $penilaian){
            if(!is_null($penilaian->active_penilaian))
                return $penilaian->active_penilaian;
                $data[] = $penilaian->active_penilaian;
        }
        return Null;
    }

}
