<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penyelenggara_forum_ilmiah extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'penyelenggara_forum_ilmiah';
    protected $primaryKey = 'id';
    protected $fillable = ['judul'];
    protected $appends = ['isvalidate_show','tingkat_text'];

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)
            ->where("identifier", '=', "penelitian");
    }
    
    function dosen() {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }

    function tahun_kegiatan() {
        return $this->belongsTo('Tahun_kegiatan', 'tahun_kegiatan', 'id');
    }

    public function getIsvalidateShowAttribute(){
        if($this->isvalidate==1) return "Valid";
        else if($this->isvalidate==0) return "Belum di Validasi";
        else if($this->isvalidate==2) return "Belum Valid";
        else if($this->isvalidate==3) return "Belum Disubmit";
    }

    public function getTingkatTextAttribute(){
        $text = "";
        if ($this->tingkat==1) $text = "Tingkat International";
        else if ($this->tingkat==2) $text = "Tingkat Nasional";
        else $text = "Tingkat Regional";
        return $text;
    }
}
?>