<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Forum_ilmiah extends Eloquent
{

    public $timestamps = true;
    protected $table = 'forum_ilmiah';
    protected $primaryKey = 'id';
    protected $fillable = ['judul'];
    protected $appends = ['statusShow', 'isvalidate_show', 'type_show'];

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)
            ->where("identifier", '=', "penelitian");
    }

    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }
    
    public function forum_ilmiah_anggota()
    {
        return $this->hasMany('Forum_ilmiah_anggota', 'forum_ilmiah', 'id');
    }

    public function subject_penelitian()
    {
        return $this->hasMany('subject_penelitian', 'data', 'id')->where('type', '=', '3');
    }

    public function getStatusShowAttribute()
    {
        return $this->status == 'pemakalah_biasa' ? "Pemakalah Biasa" : "Invited Speaker";
    }

    public function getIsvalidateShowAttribute()
    {
        if ($this->isvalidate == 1) {
            return "Valid";
        } else if ($this->isvalidate == 0) {
            return "Belum di Validasi";
        } else if ($this->isvalidate == 2) {
            return "Belum Valid";
        } else if ($this->isvalidate == 3) {
            return "Belum Disubmit";
        }

    }

    public function tahun_kegiatan()
    {
        return $this->belongsTo('Tahun_kegiatan', 'tahun_kegiatan', 'id');
    }

    public function getTypeShowAttribute()
    {
        if ($this->tingkat == 1) {
            return "Tingkat International";
        } else if ($this->tingkat == 2) {
            return "Tingkat Nasional";
        } else if ($this->tingkat == 3) {
            return "Tingkat Regional";
        }

    }
}
