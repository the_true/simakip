<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Tautan extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'tautan';
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];
}