<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Program_studi extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'program_studi';
    protected $primaryKey = 'id';
    protected $fillable = ['nama_program'];
    
    function fakultas() {
        return $this->belongsTo('Fakultas', 'fakultas', 'id');
    }
}