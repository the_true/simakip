<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Jurnal_anggota extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'jurnal_anggota';
    protected $primaryKey = 'id';

    public function jurnal(){
        return $this->belongsTo('Jurnal', 'jurnal', 'id');
    }
    
    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }
}
?>