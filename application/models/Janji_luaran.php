<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Janji_luaran extends Eloquent
{
    public $timestamps = false;
    protected $table = 'janji_luaran';
    protected $primaryKey = 'id';
}
