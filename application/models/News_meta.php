<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class News_meta extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'news_meta';
    protected $primaryKey = 'id';
    protected $fillable = ['body'];
    protected $appends = ['path_link'];

    public function getPathLinkAttribute(){
    	return "uploads/pengumuman/".$this->path;
    }
}