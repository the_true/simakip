<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_dikti_anggota extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'penelitian_dikti_anggota';
    protected $primaryKey = 'id';

    public function penelitian_dikti(){
        return $this->belongsTo('Penelitian_dikti', 'penelitian_dikti', 'id');
    }
    
    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
    }
}
?>