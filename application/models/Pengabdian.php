<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Pengabdian extends Eloquent
{

    public $timestamps = true;
    protected $table = 'pengabdian';
    protected $primaryKey = 'id';
    protected $appends = ["target_text", "status_text", 'rp_total_biaya', 'rp_mandiri_biaya_instansi', 'rp_mandiri_biaya_total'];

    protected $casts = [
        'target' => 'array',
        'anggota_mahasiswa' => 'array',
        'anggota_alumni' => 'array',
        'anggota_staff' => 'array',
    ];

    public function jenis_pengabdian()
    {
        return $this->belongsTo('Jenis_pengabdian', 'jenis_pengabdian', 'id')->select(array('id', 'nama'));
    }

    public function iptek()
    {
        return $this->belongsTo('Iptek', 'iptek', 'id')->select(array('id', 'nama'));
    }

    public function mitra()
    {
        return $this->belongsTo('Mitra', 'mitra', 'id')->select(array('id', 'nama'));
    }

    public function dosen()
    {
        return $this->belongsTo('Dosen', 'dosen', 'id');
        // ->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','surel','hp','jabatan_fungsional','gelar_depan','gelar_belakang','status_dosen','fakultas','bank','bank'))
    }

    public function dosen_mengetahui()
    {
        return $this->belongsTo('Dosen', 'dosen_mengetahui', 'id')->select(array('id', 'nama', 'jenjang_pendidikan', 'jabatan_akademik', 'program_studi', 'nidn', 'jabatan_fungsional', 'gelar_depan', 'gelar_belakang'));
    }

    public function dosen_menyetujui()
    {
        return $this->belongsTo('Dosen', 'dosen_menyetujui', 'id')->select(array('id', 'nama', 'jenjang_pendidikan', 'jabatan_akademik', 'program_studi', 'nidn', 'jabatan_fungsional', 'gelar_depan', 'gelar_belakang'));
    }

    // public function subject_penelitian(){
    //     return $this->hasMany('subject_penelitian', 'data', 'id')->where('type','=','7');
    // }

    public function tahun_kegiatan()
    {
        return $this->belongsTo('Tahun_kegiatan', 'tahun_kegiatan', 'id')->select(array('id', 'tahun'));
    }

    public function anggota()
    {
        return $this->hasMany('Pengabdian_anggota', 'pengabdian', 'id')->select(array('*'));
    }

    public function pengabdian_reviewer()
    {
        return $this->hasMany('Pengabdian_reviewer', 'pengabdian', 'id')->orderby('dosen', 'ASC');
    }

    public function monev_reviewer()
    {
        return $this->hasMany('Pengabdian_monev_reviewer', 'pengabdian', 'id');
    }

    public function pengabdian_laporan()
    {
        return $this->hasOne('pengabdian_laporan', 'pengabdian', 'id');
    }

    public function pencairan_dana_tahap1()
    {
        return $this->hasOne('Pengabdian_pencairan_dana_a', 'pengabdian', 'id');
    }

    public function pencairan_dana_tahap2()
    {
        return $this->hasOne('Pengabdian_pencairan_dana_b', 'pengabdian', 'id');
    }

    public function monev()
    {
        return $this->hasOne('pengabdian_monev', 'pengabdian', 'id');
    }

    public function surat_kontrak()
    {
        return $this->hasOne('Surat_kontrak_pengabdian', 'penelitian', 'id')->where('type', '=', 'pengabdian');
    }

    public function batch()
    {
        return $this->belongsTo('Batch', 'batch', 'id')->select(array('id', 'nama', 'start', 'finish', 'batch_penelitian', 'monev_start', 'monev_finish', 'batch_lists', 'review', 'deadline_review'));
    }

    public function pengabdian_review()
    {
        return $this->hasMany('Pengabdian_review', 'pengabdian', 'id');
    }

    public function pengabdian_review_inselected()
    {
        return $this->hasMany('Pengabdian_review', 'pengabdian', 'id')->where('isselected', '=', '1')->where('isvalid', '=', '1');
    }

    public function monev_review_inselected()
    {
        return $this->hasMany('Pengabdian_monev_review', 'penelitian', 'id')->where('isselected', '=', '1');
    }

    public function pengabdian_review_exselected()
    {
        return $this->hasMany('Pengabdian_review', 'pengabdian', 'id')->where('isselected', '=', '0')->where('isvalid', '=', '1')->orderby('dosen', 'ASC');
    }

    public function getTargetTextAttribute()
    {
        return $this->target == 1 ? "Jurnal International" : "Jurnal Nasional";
    }

    public function getStatusTextAttribute()
    {
        if ($this->status == 1) {
            return "Belum Submit";
        } else if ($this->status == 2) {
            return "Proses Pengecekan";
        } else if ($this->status == 3) {
            return "Proses review";
        } else if ($this->status == 4) {
            return "Diterima";
        } else if ($this->status == 7) {
            return "Proses Pengecekan Perbaikan";
        } else if ($this->status == 9) {
            return "Perbaikan Belum Valid";
        } else if ($this->status == 10) {
            return "Belum Valid";
        } else if ($this->status == 11) {
            return "Diterima Dengan Perbaikan";
        } else if ($this->status == 12) {
            return "Ditolak";
        }

    }
    public function getRpTotalBiayaAttribute()
    {
        return number_format($this->total_biaya, 0, '.', '.');
    }

    public function getAnggotaAlumniAttribute($value)
    {
        if ($value != "null") {
            return json_decode($value, true);
        } else {
            return [];
        }

    }

    public function getAnggotaMahasiswaAttribute($value)
    {
        if ($value != "null") {
            return json_decode($value, true);
        } else {
            return [];
        }

    }

    public function getAnggotaStaffAttribute($value)
    {
        if ($value != "null") {
            return json_decode($value, true);
        } else {
            return [];
        }

    }

    public function getRpMandiriBiayaInstansiAttribute()
    {
        return number_format($this->mandiri_biaya_instansi, 0, '.', '.');
    }
    public function getRpMandiriBiayaTotalAttribute()
    {
        return number_format($this->mandiri_total_biaya, 0, '.', '.');
    }
}
