<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_nilai extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'pengabdian_nilai';
    protected $primaryKey = 'id';
    protected $fillable = array('pengabdian_review', 'pengabdian', 'penilaian_kriteria');

    public function penilaian_kriteria(){
    	return $this->belongsTo('Penilaian_kriteria','penilaian_kriteria','id')->select(array('id','nama','indikator','bobot'));
    }
}
?>