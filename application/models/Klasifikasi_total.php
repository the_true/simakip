<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Klasifikasi_total extends Eloquent {

   public $timestamps = FALSE;
   protected $table = 'point_klasifikasi_total';
   protected $primaryKey = 'id';
   protected $appends = ['status_text'];

	function getStatusTextAttribute(){
        return $this->status==1?"Aktif":"Tidak Aktif";
    }
}
