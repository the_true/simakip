<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Bobot_kriteria extends Eloquent {
    
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'bobot_kriteria';
    protected $primaryKey = 'id';
    
    function penilaian() {
        return $this->belongsTo('bobot_hasil', 'bobot_hasil', 'id');
    }
}