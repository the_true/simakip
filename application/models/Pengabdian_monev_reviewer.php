<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_monev_reviewer extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'pengabdian_monev_reviewer';
    protected $primaryKey = 'id';
    protected $appends = ['status_text'];

    public function pengabdian(){
    	return $this->belongsTo('Pengabdian', 'pengabdian', 'id');
    }

    public function dosen(){
    	return $this->belongsTo('Dosen','dosen','id');
    }

    public function getStatusTextAttribute(){
    	if($this->status=='0'){
    		return 'Belum Mereview';
    	}else if($this->status=='1'){
    		return 'Sudah Mereview';
    	}else if($this->status=='10'){
    		return 'Masa Kadaluarsa';
    	}
    }
}
?>