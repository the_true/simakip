<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_pencairan_dana_a extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'pengabdian_pencairan_dana1';
    protected $primaryKey = 'id';
    protected $fillable = ['pengabdian','pajak','pajak_hitung','dana'];
    protected $appends = ['status_text'];

	function getStatusTextAttribute(){
		if($this->status==1) return "Menunggu Persetujuan";
		else if($this->status==2) return "Disetujui";
		else if($this->status==3) return "Proses Transfer";
        else if($this->status==4) return "Sudah Dikonfirmasi";
        return "";
    }
}