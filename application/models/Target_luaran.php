<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Target_luaran extends Eloquent
{

    public $timestamps = false;
    protected $table = 'target_luaran';
    protected $primaryKey = 'id';

    public function jenis_penelitian()
    {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id');
    }

    public function target_luaran_master()
    {
        return $this->belongsTo("Target_luaran_master", "target_luaran_master", "id");
    }

}
