<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_review extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'penelitian_review';
    protected $primaryKey = 'id';
    protected $fillable = array('penelitian', 'dosen');
    protected $appends = ['status_text','rp_rekomendasi'];

    public function penelitian_nilai(){
    	return $this->hasMany('Penelitian_nilai', 'penelitian_review', 'id');
    }

    public function penelitian(){
        return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    public function dosen(){
        return $this->belongsTo('Dosen', 'dosen', 'id')->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','surel','hp','jabatan_fungsional','gelar_depan','gelar_belakang','status_dosen'));
    }

    public function getStatusTextAttribute(){
    	if($this->status==1) return "Diterima";
        if($this->status==2) return "Diterima Dengan Perbaikan";
        if($this->status==3) return "Ditolak";
    }

    public function getRpRekomendasiAttribute(){
        $number = empty($this->rekomendasi)?0:$this->rekomendasi;
        return number_format($number, 0, '.', '.');
    }
}
?>