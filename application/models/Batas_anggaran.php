<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Batas_anggaran extends Eloquent {
    
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'batas_anggaran';
    protected $primaryKey = 'id';
	protected $appends = ['status_text','format_rupiah'];
    

    function jenis_penelitian() {
        return $this->belongsTo('Jenis_penelitian', 'jenis_penelitian', 'id');
    }

    public function getStatusTextAttribute(){
        return $this->status==1?'Aktif':'Tidak Aktif';
    }

    public function getFormatRupiahAttribute(){
        return number_format($this->batas, 0, '.', '.');
    }
}