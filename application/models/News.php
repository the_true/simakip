<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class News extends Eloquent {
    
    public $timestamps = TRUE;
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $fillable = ['title'];

    function news_meta() {
        return $this->hasMany('News_meta', 'news');
    }
}