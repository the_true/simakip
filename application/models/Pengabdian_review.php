<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_review extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'pengabdian_review';
    protected $primaryKey = 'id';
    protected $fillable = array('pengabdian', 'dosen');
    protected $appends = ['status_text','rp_rekomendasi','nomor_urut'];

    public function pengabdian_nilai(){
    	return $this->hasMany('Pengabdian_nilai', 'pengabdian_review', 'id');
    }

    public function pengabdian(){
        return $this->belongsTo('Pengabdian', 'pengabdian', 'id');
    }

    public function dosen(){
        return $this->belongsTo('Dosen', 'dosen', 'id')->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','surel','hp','jabatan_fungsional','gelar_depan','gelar_belakang','status_dosen'));
    }

    public function getNomorUrutAttribute(){
        if(!class_exists('Pengabdian_reviewer')) return 0;
        $reviewers = Pengabdian_reviewer::where('pengabdian','=',$this->pengabdian)->where('isdelete','=','0')->get();
        foreach($reviewers as $key=>$value){
            if($value->dosen == $this->dosen){
                return $key+1;
            }
        }
        return 0;
    }

    public function getStatusTextAttribute(){
    	if($this->status==1) return "Diterima";
        if($this->status==2) return "Diterima Dengan Perbaikan";
        if($this->status==3) return "Ditolak";
    }

    public function getRpRekomendasiAttribute(){
        $number = empty($this->rekomendasi)?0:$this->rekomendasi;
        return number_format($number, 0, '.', '.');
    }
}
?>