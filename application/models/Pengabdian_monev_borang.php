<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_monev_borang extends Eloquent {
    
    public $timestamps = true;
    protected $table = 'pengabdian_monev_borang';
    protected $primaryKey = 'id';
    protected $fillable = array('pengabdian', 'dosen');
    // protected $appends = ['status_text','rp_rekomendasi'];


    public function pengabdian(){
        return $this->belongsTo('Pengabdian', 'pengabdian', 'id');
    }

    public function dosen(){
        return $this->belongsTo('Dosen', 'dosen', 'id')->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','surel','hp','jabatan_fungsional','gelar_depan','gelar_belakang','status_dosen'));
    }

    public function getContentAttribute($value)
    {
        return json_decode($value);
    }

}
?>