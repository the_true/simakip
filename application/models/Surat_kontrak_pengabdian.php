<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Surat_kontrak_pengabdian extends Eloquent {

    public $timestamps = true;
    protected $fillable = [];
    protected $table = 'surat_kontrak';
    protected $primaryKey = 'id';
    protected $appends = ['is_laporan_done'];
    
    public function newQuery($excludeDeleted = true) {
      return parent::newQuery($excludeDeleted)
          ->where("type", '=', "pengabdian");
    }


    function pengabdian() {
        return $this->belongsTo('Pengabdian', 'penelitian', 'id');
    }

    function dosen(){
      return $this->belongsTo('Dosen','dosen','id');
    }

    function getIsLaporanDoneAttribute(){
      $pengabdian_laporan = Pengabdian_laporan::where('pengabdian','=',$this->penelitian)->where('status','=','1')->count();
      if($pengabdian_laporan>0) return true;
      return false;
    }

    // function getDanaRupiahAttribute(){
    //   $rupiah = (float)$this->dana*(float)$this->penelitian()->first()->total_biaya/100;
    //   // echo $rupiah
    //   return $rupiah;
    // }

    // function getSisaDanaAttribute(){
    //   $total = $this->penelitian()->first()->total_biaya;
    //   $rupiah = (float)$this->dana*(float)$total/100;
    //   $rupiah = $total-$rupiah;
    //   // echo $rupiah
    //   return $rupiah;
    // }

}
