<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;

class Penelitian_target_luaran_wajib extends Eloquent
{

    public $timestamps = false;
    protected $table = 'penelitian_target_luaran_wajib';
    protected $primaryKey = 'id';
    
    function target_luaran_sub()
    {
        return $this->belongsTo('Target_luaran_sub');
    }
    
    function target_luaran_status(){
        return $this->belongsTo('Target_luaran_status');
    }
    
}
