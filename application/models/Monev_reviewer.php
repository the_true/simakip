<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Monev_reviewer extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'monev_reviewer';
    protected $primaryKey = 'id';
    protected $appends = ['status_text'];

    public function penelitian(){
    	return $this->belongsTo('Penelitian', 'penelitian', 'id');
    }

    public function dosen(){
    	return $this->belongsTo('Dosen','dosen','id');
    }

    public function getStatusTextAttribute(){
    	if($this->status=='0'){
    		return 'Belum Mereview';
    	}else if($this->status=='1'){
    		return 'Sudah Mereview';
    	}else if($this->status=='10'){
    		return 'Masa Kadaluarsa';
    	}
    }
}
?>