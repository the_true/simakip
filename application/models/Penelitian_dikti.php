<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Penelitian_dikti extends Eloquent {
    
    protected $table = 'penelitian_dikti';
    
    public function penelitian_dikti_anggota()
    {
        return $this->hasMany('Penelitian_dikti_anggota', 'penelitian_dikti', 'id');
    }

}
?>