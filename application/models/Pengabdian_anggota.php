<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Pengabdian_anggota extends Eloquent {
    
    public $timestamps = FALSE;
    protected $table = 'pengabdian_anggota';
    protected $primaryKey = 'id';
    protected $appends = ['konfirmasi_text'];

    function dosen() {
        return $this->belongsTo('Dosen', 'anggota', 'id')->select(array('id', 'nama','jenjang_pendidikan','jabatan_akademik','program_studi','nidn','gelar_depan','gelar_belakang','surel','fakultas'));
    }

    function pengabdian(){
        return $this->hasOne('Pengabdian','id','pengabdian');
    }

    public function getKonfirmasiTextAttribute(){
        if($this->konfirmasi=='1') return 'Menyetujui';
        else if($this->konfirmasi=='2') return 'Menolak';
        else return 'Menunggu Konfirmasi';
    	// return $this->konfirmasi=='0'?'Waiting':'Approved';
    }

}