<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class Subsumberdana extends Eloquent {
    
    public $timestamps = false;
    protected $table = 'subsumberdana';
    protected $primaryKey = 'id';
    protected $fillable = ['subsumberdana'];
    
    function sumberdana() {
        return $this->belongsTo('Sumberdana', 'sumberdana', 'id');
    }
}