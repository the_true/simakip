<?php
require_once 'tcpdf.php';
class SURATKONTRAKPDF extends TCPDF
{

    protected $isHeader = true;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false, $header = true)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        $this->isHeader = $header;
    }

    //Page header
    public function Header()
    {
        $margin = $this->getMargins();
        if ($this->isHeader == false) {
            return;
        }

        if ($this->page == 1) {
            $this->SetFont('times', '', 10);
            $html = '<table  cellspacing="0" cellpadding="40%" style="">
                    <tr>
                        <td rowspan="3" style="width:20%;text-align: left;" ><img src="' . base_url() . '/assets/images/lemlit.png" style="width:80px;height:auto;"></td>
                        <td style="width:85%;text-align:center;font-size:14px;font-family: "Times New Roman", Times, serif;">UNIVERSITAS MUHAMMADIYAH PROF. DR. HAMKA</td>
                    </tr>
                    <tr>
                        <td style="width:85%;font-size:18px;font-weight:bold;text-align:center;font-family: "Times New Roman", Times, serif;">LEMBAGA PENELITIAN DAN PENGEMBANGAN</td>
                    </tr>
                      <tr>
                        <td style="width:85%;font-size:12px;font-weight:bold;text-align:center;font-family: Arial, Helvetica, sans-serif;">Jln. Tanah Merdeka, Pasar Rebo, Jakarta Timur <br>
    Telp. 021-8416624, 87781809; Fax. 87781809
                        </td>
                      </tr>
                    </table>';
            $this->writeHTMLCell($w = 0, $h = 0, $x = '10', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
            $style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0);
            $this->Line(5, $this->y + 1, $this->w - 5, $this->y + 1, $style);
            $this->Line(5, $this->y + 2, $this->w - 5, $this->y + 2, $style);
        } else {
            $html = '<table  cellspacing="0" cellpadding="40%" style="">
                    <tr>
                        <td rowspan="3" style="width:15%;" ></td>
                        <td style="width:85%;text-align:center;"><h2> </h2></td>
                    </tr>
                    <tr>
                        <td style="width:85%;text-align:center;"><h1> </h1></td>
                    </tr>
                      <tr>
                        <td style="width:85%;font-size:15px;text-align:center;"><br>
                        </td>
                      </tr>
                    </table>';
            $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
        }
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('times', '', 8);
        $this->Cell(60, 0, 'Hak Cipta © http://simakip.uhamka.ac.id', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 8);
        $this->Cell(60, 0, 'Tanggal Download: ' . date('d-m-Y'), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times', '', 8);
        $this->Cell(60, 0, 'Halaman ' . $this->getAliasNumPage() . ' dari ' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

    }
}
