<?php
require_once('tcpdf.php');
class MYPDF extends TCPDF {

    protected $isHeader = true;

    public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false, $header=true) {
         parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
         $this->isHeader=$header;
    }

    //Page header
    public function Header() {
        if($this->isHeader==false) return;
        $this->SetFont('times','', 10);
        $html = '<table  cellspacing="0" cellpadding="40%" style=""> 
                <tr>
                    <td rowspan="3" style="width:15%;" ><img src="'.base_url().'/assets/images/lemlit.png" style="width:70px;height:auto;"></td>
                    <td style="width:75%;text-align:center;"><h1>SIMAKIP</h1></td>
                </tr>
                <tr>
                    <td style="width:85%;text-align:center;"><h3>Sistem Informasi Manajemen & Kinerja Penelitian</h3></td>
                </tr>
                  <tr>
                    <td style="width:85%;font-size:10px;text-align:center;"><b>Lembaga Penelitian dan Pengembangan - Universitas Muhammadiyah Prof DR. HAMKA</b><br>
                        Tlp. 021-8416624, 87781809; Fax. 021-87781809; Email : lemlit@uhamka.ac.id
                    </td>
                  </tr>
                </table>';
        $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0);
        $this->Line(5, $this->y, $this->w - 5, $this->y, $style);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('times','', 8);
        $this->Cell(60, 0, 'Hak Cipta © http://simakip.uhamka.ac.id', 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times','', 8);
        $this->Cell(60, 0, 'Tanggal Download: '.date('d-m-Y'), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->SetFont('times','', 8);
        $this->Cell(60, 0, 'Halaman '.$this->getAliasNumPage().' dari '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

    }
}

?>