<?php

function getNotif(){
	$data=[];
	$CI = & get_instance();
	$CI->load->model('Notifikasi');
	$userinfo = $CI->session->userdata('userinfo');
	$notifikasi = Notifikasi::where('dosen','=',$userinfo['id'])->where('isread','=','0')->where('isdelete','=','0')->orderBy('created_at','desc')->take(5)->get();
	$jumlah = Notifikasi::where('dosen','=',$userinfo['id'])->where('isread','=','0')->where('isdelete','=','0')->count();
	$data['items']= $notifikasi->toArray();
	$data['total']= $jumlah;
	return $data;
}

function getUserInfo(){
	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if( isset($userinfo) ) {
	     return $userinfo;
	}
	return FALSE;
}

function request_get($get){
	$CI = & get_instance();
	return $CI->input->get($get);
}

function role($role){
	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	if($userinfo['akses']['nama']==$role) return TRUE;
	return false;
}

function roles($roles){
	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	foreach ($roles as $value) {
		if($userinfo['akses']['nama']==$value) return TRUE;
	}
	return false;
}

function issuperadmin(){
	$CI = & get_instance();
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	return FALSE;
}

function tautan(){
	$CI = & get_instance();
	$CI->load->model('Tautan');
	$tautan = Tautan::where("isdelete","=","0")->orderBy('id','DESC')->take('10')->get();
	return $tautan->toArray();
}

function get_message($message){
	$CI = & get_instance();
	$message = $CI->session->flashdata('$message');
	if( isset($message) ) {
	     return $message;
	}
	return FALSE;
}

function isakses($role){
	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	if( isset($userinfo) ) {
	     return strtolower($userinfo["akses"]["nama"]) == strtolower($role);
	}
	return FALSE;
}

function isviewall(){
	$akses_all = array(4,5,6,7,8);

	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	if( isset($userinfo) && in_array($userinfo["akses"]["id"],$akses_all)) {
	     return TRUE;
	}
	return FALSE;
}

function isaddedit(){
	$akses_all = array(1,3);

	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	if( isset($userinfo) && in_array($userinfo["akses"]["id"],$akses_all)) {
	     return TRUE;
	}
	return FALSE;
}

function angka_huruf($x){
  $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  $x = (int) ltrim($x, '0');
  if(empty($x)) return "";
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return angka_huruf($x - 10) . " Belas";
  elseif ($x < 100)
    return angka_huruf($x / 10) . " Puluh" . angka_huruf($x % 10);
  elseif ($x < 200)
    return " Seratus" . angka_huruf($x - 100);
  elseif ($x < 1000)
    return angka_huruf($x / 100) . " Ratus" . angka_huruf($x % 100);
  elseif ($x < 2000)
    return " Seribu" . angka_huruf($x - 1000);
  elseif ($x < 1000000)
    return angka_huruf($x / 1000) . " Ribu" . angka_huruf($x % 1000);
  elseif ($x < 1000000000)
    return angka_huruf($x / 1000000) . " Juta" . angka_huruf($x % 1000000);
}

function isdelete(){
	$akses_all = array(1,3,4,5,6);

	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	if( isset($userinfo) && in_array($userinfo["akses"]["id"],$akses_all)) {
	     return TRUE;
	}
	return FALSE;
}

function isnotaddeditdeldosen(){
	$akses_all = array(7,8);

	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return FALSE;
	}
	if( isset($userinfo) && in_array($userinfo["akses"]["id"],$akses_all)) {
	     return TRUE;
	}
	return FALSE;
}

function isnotkonfigurasi(){
	return FALSE;
}

function isvalidasi(){
	$akses_all = array(4,5,6);

	$CI = & get_instance();  //get instance, access the CI superobject
	$userinfo = $CI->session->userdata('userinfo');
	if($userinfo["issuperadmin"]==1){
		return TRUE;
	}
	if( isset($userinfo) && in_array($userinfo["akses"]["id"],$akses_all)) {
	     return TRUE;
	}
	return FALSE;
}

function get_kontak(){
	$CI = & get_instance();
	$CI->load->model('Kontak');
	$kontak = Kontak::find(1);
	if(!empty($kontak))
		return $kontak->toArray();
	return "";
}

function paging_creator($current_page,$total_page){
	$page_html = "";
	if($total_page>10){

		$page_html .= $current_page!=1?'<li><a class="paging" href="#" data-page="1">1</a></li>':'';
		if($current_page>4){
			$page_html .= '<li><a class="paging" href="#">...</a></li>';
		}
		for($i=$current_page-2;$i<$current_page;$i++){
			if($i<2) continue;
			$page_html.= '<li><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>';
		}
		$page_html .= '<li class="active"><a class="paging" href="#" data-page="'.$current_page.'">'.$current_page.'</a></li>';
		for($i=$current_page+1;$i<$current_page+3;$i++){
			if($i>$total_page-1) continue;
			$page_html.= '<li><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>';
		}
		if($current_page<$total_page-4){
			$page_html .= '<li><a class="paging" href="#">...</a></li>';
		}
		$page_html .= $current_page!=$total_page?'<li><a class="paging" href="#" data-page="'.$total_page.'">'.$total_page.'</a></li>':'';
	}else{
		for($i=1;$i<=$total_page;$i++){
			$page_html .= $current_page==$i?'<li class="active"><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>':'<li><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>';
		}
	}
	return $page_html;
}

function format_rupiah($nilai){
    return number_format($nilai, 0, '.', '.');
}

function hitung_persen($rupiah, $persen,$min=100000){
	$hitung = (float) $persen * (float) $rupiah/ 100;
	$hitung = ceil($hitung/$min);
	$hitung = (int) $hitung*$min;
	return $hitung;
}

function hitung_rupiah($persen,$total,$min=100000){
      $rupiah = (float)$persen*(float)$total/100;
      $rupiah = $rupiah/$min;
      $rupiah = round($rupiah)*$min;
      // echo $rupiah
      return $rupiah;
}

function stringminimize($string,$amount=4){
	$first_four_title = implode(' ', array_slice(explode(' ', $string), 0, $amount));
	return $first_four_title;
}

function searchpetapotensi($item,$luaran,$syarat){
	foreach($syarat as $value){
		if($value["pendidikan"]==$item["tingkat"] and $value["jabatan_akademik"]==$item["jabatan_akademik_id"] and array_key_exists($luaran,$value["items"])){
			return TRUE;
		}
	}
	return FALSE;
}

?>
