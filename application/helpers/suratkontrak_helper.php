<?php

	function check_aktivasi(){
		$ci =& get_instance();
		$result = true;

		$ci->load->model('Format_nomor');
		$ci->load->model('Persetujuan');
		$ci->load->model('Tembusan');
		$ci->load->model('Surat');
		$format = Format_nomor::where("status","=","1")->where("isdelete","=","0")->first();
		$result = $result & !empty($format);

		$peran1 = Persetujuan::where('isdelete','=','0')->where('status','=','1')->where('nomor','=','1')->first();
		$result = $result & !empty($peran1);

		$peran1 = Persetujuan::where('isdelete','=','0')->where('status','=','1')->where('nomor','=','2')->first();
		$result = $result & !empty($peran1);

		$batasan = Batasan_dana::where("isdelete","=","0")->where("status","=","1")->first();
		$result = $result & !empty($batasan);

		$tem = Tembusan::where('isdelete','=','0')->where('status','=','1')->first();
		$result = $result & !empty($tem);

		$surat1 = Surat::where("isdelete","=","0")->where("nomor","=",'1')->where("status","=","1")->first();
		$result = $result & !empty($surat1);

		$surat1 = Surat::where("isdelete","=","0")->where("nomor","=",'2')->where("status","=","1")->first();
		$result = $result & !empty($surat1);

		return $result;
	}

	function sk_nokontrak($content, $nomor){
		$content = str_replace("#NOKONTRAK1", $nomor, $content);
		return $content;
	}

	function sk_formatnomor1($content,$id=Null){
		$ci =& get_instance();
		$ci->load->model('Format_nomor');
		if($id){
			$format = Format_nomor::where("id","=",$id)->first();
		}else{
	        $format = Format_nomor::where("status","=","1")->where("isdelete","=","0")->first();
		}
		return str_replace("#FORMATSURAT1", $format->format_a, $content);
	}

	function sk_formatnomor2($content,$id){
		$ci =& get_instance();
		$ci->load->model('Format_nomor');
		$format = Format_nomor::where("id","=",$id)->first();
		return str_replace("#FORMATSURAT2", $format->format_b, $content);
	}

	function sk_judul($content,$judul){
		return str_replace("#JUDUL",$judul,$content);
	}

	function sk_namapengusul($content,$pengusul){
		return str_replace("#NAMAPENGUSUL",$pengusul,$content);
	}

	function sk_ejaandana($content,$dana){
		return $content;
	}

	function sk_tglmulai($content,$tglmulaix){
		$tglmulai = explode('-',explode(' ', $tglmulaix)[0]);
		$date = $tglmulai[2];
		$month = $tglmulai[1];
		$year = $tglmulai[0];

		$content = str_replace("#TANGGALMULAI", $date, $content);
		$content = str_replace("#BULANMULAI", $month, $content);
		$content = str_replace("#TAHUNMULAI", $year, $content);
		$content = str_replace("#EJATGLMULAI", trim(angka_huruf(ltrim($date, '0'))), $content);
		$content = str_replace("#EJABLNMULAI", angkatobulan((int)$month-1), $content);
		$content = str_replace("#EJATHNMULAI", trim(angka_huruf($year)), $content);
		$content = str_replace("#HARIMULAI", hari($tglmulaix), $content);
		return $content;
	}

	function sk_tglselesai($content,$tglselesaix){
		$tglselesai = explode('-',explode(' ', $tglselesaix)[0]);
		$date = $tglselesai[2];
		$month = $tglselesai[1];
		$year = $tglselesai[0];

		$content = str_replace("#TANGGALSELESAI", $date, $content);
		$content = str_replace("#BULANSELESAI", $month, $content);
		$content = str_replace("#TAHUNSELESAI", $year, $content);
		$content = str_replace("#EJATGLSELESAI", trim(angka_huruf($date)), $content);
		$content = str_replace("#EJABLNSELESAI", angkatobulan((int )$month-1), $content);
		$content = str_replace("#EJATHNSELESAI", trim(angka_huruf($year)), $content);
		$content = str_replace("#HARISELESAI", hari($tglselesaix), $content);
		return $content;
	}

	function sk_ejatgl($content){
		return $content;
	}

	function sk_tanggalpembuatan($content,$tanggal){
		$tanggal = explode('-',explode(' ', $tanggal)[0]);
		$date = $tanggal[2];
		$month = $tanggal[1];
		$year = $tanggal[0];

		$content = str_replace("#TANGGAL", $date, $content);
		$content = str_replace("#BULAN", $month, $content);
		$content = str_replace("#TAHUN", $year, $content);

		$content = str_replace("#EJATANGGAL", trim(angka_huruf($date)), $content);
		$content = str_replace("#EJABULAN", angkatobulan((int)$month-1), $content);
		$content = str_replace("#EJATAHUN", trim(angka_huruf($year)), $content);
		return $content;
		return $content;
	}

	function sk_jmldana($content,$dana,$anggaran){
		$hitung = (float) $dana * (float) $anggaran/ 100;
		$hitung = ceil($hitung/100000);
		$dana1 = (int) $hitung*100000;

		$dana2 = (int) $anggaran-$dana1;
		$content = str_replace("#JMLDANA1", format_rupiah($dana1), $content);
		$content = str_replace("#EJAANDANA1", trim(angka_huruf($dana1))." Rupiah", $content);
		$content = str_replace("#JMLDANA2", format_rupiah($dana2), $content);
		$content = str_replace("#EJAANDANA2", trim(angka_huruf($dana2))." Rupiah", $content);
		$content = str_replace("#JMLANGGARAN", format_rupiah($anggaran), $content);
		$content = str_replace("#EJAANANGGARAN", trim(angka_huruf($anggaran)), $content);
		return $content;
	}

	function sk_peran($content,$id1,$id2){
		$ci =& get_instance();
		$ci->load->model('Persetujuan');
		$peran1 = Persetujuan::where('id','=',$id1)->first();
		$peran1->load('dosen');
		$peran1 = $peran1->toArray();
		$peran2 = Persetujuan::where('id','=',$id2)->first();
		$peran2->load('dosen');
		$peran2 = $peran2->toArray();

		$content = str_replace("#NAMAKETUA", $peran1['dosen']['nama_lengkap'], $content);
		$content = str_replace("#PERANKETUA", $peran1['peran'], $content);
		$content = str_replace("#NAMAMENGETAHUI", $peran2['dosen']['nama_lengkap'], $content);
		$content = str_replace("#PERANMENGETAHUI", $peran2['peran'], $content);
		return $content;
	}

	function sk_tembusan($content,$id){
		$ci =& get_instance();
		$ci->load->model('Tembusan');
		$tem = Tembusan::where('id','=',$id)->first();

		$content = str_replace("#TEMBUSAN", $tem->isi, $content);
		return $content;
	}
?>
