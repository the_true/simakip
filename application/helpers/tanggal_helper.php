<?php



function angkatobulan($int){
  $bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
  return $bulan[(int) $int];
}

function angkatofirstbulan($int){
  $bulan = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
  return $bulan[(int)$int-1];
}

function engtohari($string){
  $hari = ["Sunday"=>"Minggu","Monday"=>"Senin","Tuesday"=>"Selasa","Wednesday"=>"Rabu","Thursday"=>"Kamis","Friday"=>"Jumat","Saturday"=>"Sabtu"];
  return $hari[$string];
}

function bulantoangka($string){
  $bulan = ["Januari"=>1, "Februari"=>2, "Maret"=>3, "April"=>4, "Mei"=>5, "Juni"=>6, "Juli"=>7, "Agustus"=>8, "September"=>9, "Oktober"=>10, "November"=>11, "Desember"=>12];
  return $bulan[$string];
}

function hari($string){
	$day_name = engtohari(date('l',strtotime($string)));
	return $day_name;
}

function tanggaltoformat($string){
	$day = date('d',strtotime($string));
	$month = date('n',strtotime($string));
	$year = date('Y',strtotime($string));
	$day_name = hari($string);
	$month = angkatobulan((int) $month-1);
	return $day_name.', '.$day.'/'.$month.'/'.$year;
}

function formattotanggal($string){
	//senin, 30/12/2017
	$string = explode(', ', $string);
	if(count($string)!=2) return false;
	$string = explode('/', $string[1]);
	$string = $string[0].'-'.bulanToAngka($string[1]).'-'.$string[2];
	// $date = str_replace('/', '-', $string);
	return date('Y-m-d', strtotime($string));
}

?>
