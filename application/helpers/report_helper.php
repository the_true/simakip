<?php
use Illuminate\Database\Query\Expression as raw;
function fc($string,$table){
	return str_replace("created_at",$table.".created_at",$string);
}

function cm($tahun,$table){
	$query = "";
	foreach($tahun as $value){
		$res = explode('-',$value);
		$query .= " count(CASE WHEN MONTH(".$table.".created_at)=".$res[1]." AND YEAR(".$table.".created_at)=".$res[0]." THEN ".$table.".id END) as bulan".$res[0].$res[1].",";
	}
	return $query;
}

function cs($tahun){
	$query = "";
	foreach($tahun as $value){
		$res = explode('-',$value);
		$query .= "SUM(bulan".$res[0].$res[1].") as '$value',";
	}
	return $query;
}

function psum($tahun,$table,$default_table="created_at"){
	$query = "";
	foreach($tahun as $value){
		$res = explode('-',$value);
		$query .= " SUM(CASE WHEN MONTH(".$table.".".$default_table.")=".$res[1]." AND YEAR(".$table.".".$default_table.")=".$res[0]." THEN ".$table.".point else 0 END) as bulan".$res[0].$res[1].",";
	}
	return $query;
}

function mbetweendate($a,$b){
	$date = [];
	$year = [];
	$start    = (new DateTime($a))->modify('first day of this month');
	$end      = (new DateTime($b))->modify('last day of this month');
	$interval = DateInterval::createFromDateString('1 month');
	$period   = new DatePeriod($start, $interval, $end);

	foreach ($period as $dt) {
	    $date[] = $dt->format("Y-m");
	    $year[$dt->format("Y")][]=$dt->format("m");
	}
	return [$date,$year];
}


function getPointFakultas(){
	$CI = & get_instance();
	$data = [];
	$periode = getPeriode();
	$dosens = Dosen::leftJoin('point_log', 'dosen.id', '=', 'point_log.user_id')
					->leftJoin('fakultas','dosen.fakultas','=','fakultas.id')
		            ->select(new raw('SUM(point_log.point) as total'),'fakultas.nama as fakultas','fakultas.id as fakultas_id')
		            ->groupBy('dosen.fakultas')
		            ->orderBy('total','DESC')
		            ->whereNotNull('point_log.point')->where('dosen.isdelete','=','0')->whereBetween('point_log.created_date', [$periode["dari"].'-01-01', $periode["sampai"].'-12-31'])
		            ->limit(1)
		            ->get();
   foreach($dosens->toArray() as $value){
   		$total_dosen = Dosen::where("isdelete",'=','0')->where('fakultas','=',$value["fakultas_id"])->count();
   		$average = (float) $value["total"]/(float) $total_dosen;
   		
   		// echo $total_dosen; die;
   		
   		$CI->load->model("Klasifikasi_total");
   		$klasifikasi = Klasifikasi_total::where("status","=",1)->where('ketua','<=',$average)->where('anggota','>=',$average)->first();
   		$total = 0;
   		$setengah = false;
   		if($klasifikasi){
	   		$setengah = $klasifikasi->type % 2 ? false:true;
	   		if($klasifikasi->type>0) $total = round($klasifikasi->type/2);
		}
		// echo $total; die;
    	$data[] = ["fakultas"=>$value["fakultas"],"total"=>$average,"bintang"=>$total,"setengah"=>$setengah];
    }
    return $data;
}

function getPointProstud(){
	$CI = & get_instance();
	$data = [];
	$periode = getPeriode();
	$dosens = Dosen::leftJoin('point_log', 'dosen.id', '=', 'point_log.user_id')
					->leftJoin('program_studi','dosen.program_studi','=','program_studi.id')
		            ->select(new raw('SUM(point_log.point) as total'),'program_studi.nama as program_studi','program_studi.id as program_studi_id')
		            ->groupBy('dosen.program_studi')
		            ->orderBy('total','DESC')
		            ->whereNotNull('point_log.point')->where('dosen.isdelete','=','0')->whereBetween('point_log.created_date', [$periode["dari"].'-01-01', $periode["sampai"].'-12-31'])
		            ->limit(1)
		            ->get();
	// echo $dosens->toJson(); die;
   foreach($dosens->toArray() as $value){
   		// echo json_encode($value); die;
   		$total_dosen = Dosen::where("isdelete",'=','0')->where('program_studi','=',$value["program_studi_id"])->count();
   		$average = (float) $value["total"]/(float) $total_dosen;

   		$CI->load->model("Klasifikasi_total");
   		$klasifikasi = Klasifikasi_total::where("status","=",1)->where('ketua','<=',$average)->where('anggota','>=',$average)->first();

   		$total = 0;
   		$setengah = false;
   		if($klasifikasi){
	   		$setengah = $klasifikasi->type % 2 ? false:true;
	   		if($klasifikasi->type>0) $total = round($klasifikasi->type/2);
		}
		// echo $setengah;die;
    	$data[] = ["prostud"=>$value["program_studi"],"total"=>$average,"bintang"=>$total,"setengah"=>$setengah];
    }
    return $data;
}

function getPointDosen($id=null){
	$CI = & get_instance();
	$data = [];
	$periode = getPeriode();
	$dosens = Dosen::leftJoin('point_log', 'dosen.id', '=', 'point_log.user_id')
		            ->select(new raw('SUM(point_log.point) as total'),'dosen.nama as nama','gelar_depan','gelar_belakang')
		            ->groupBy('dosen.id')
		            ->orderBy('total','DESC')
		            ->whereNotNull('point_log.point')->where('dosen.isdelete','=','0')->whereBetween('point_log.created_date', [$periode["dari"].'-01-01', $periode["sampai"].'-12-31']);
    if($id){
    	$dosens= $dosens->where('dosen.id','=',$id);
    }
	$dosens = $dosens->limit(1)->get();
	// echo $dosens->toJson(); die;
   foreach($dosens->toArray() as $value){
   		$CI->load->model("Klasifikasi_total");
   		$klasifikasi = Klasifikasi_total::where("status","=",1)->where('ketua','<=',$value["total"])->where('anggota','>=',$value["total"])->first();
   		if(empty($klasifikasi)){
   			$klasifikasi = Klasifikasi_total::where("status","=",1)->orderBy('anggota','desc')->first();
   		}
   		$total = 0;
   		$setengah = false;
   		if($klasifikasi){
	   		$setengah = $klasifikasi->type % 2 ? false:true;
	   		if($klasifikasi->type>0) $total = round($klasifikasi->type/2);
		}
    	$data[] = ["nama"=>$value["nama_lengkap"],"total"=>$value["total"],"bintang"=>$total,"setengah"=>$setengah];
    }
    if(count($dosens->toArray())<1){
		$data[] = ["nama"=>"","total"=>0];
    }
    return $data;
}

function getJabatanAkademik(){
	$data = [];
	$dosens = Dosen::leftJoin('jabatan_akademik', 'dosen.jabatan_akademik', '=', 'jabatan_akademik.id')
	            ->select('jabatan_akademik.nama',new raw('count(dosen.jabatan_akademik) as jumlah'))
	            ->groupBy('dosen.jabatan_akademik')
	            ->whereNotNull('jabatan_akademik.nama')->where('dosen.isdelete','=','0')
	            ->get();
    $data = [];
    foreach($dosens->toArray() as $value){
    	$data[] = ["name"=>$value["nama"],"y"=>(int)$value["jumlah"]];
    }
    return $data;
}

function getJabatanFungsi(){
	$data = [];
	$dosens = Dosen::leftJoin('jabatan_fungsi', 'dosen.jabatan_fungsional', '=', 'jabatan_fungsi.id')
	            ->select('jabatan_fungsi.nama',new raw('count(dosen.jabatan_fungsional) as jumlah'))
	            ->groupBy('dosen.jabatan_fungsional')
	            ->whereNotNull('jabatan_fungsi.nama')->where('dosen.isdelete','=','0')
	            ->get();
    $data = [];
    foreach($dosens->toArray() as $value){
    	$data[] = ["name"=>$value["nama"],"y"=>(int)$value["jumlah"]];
    }
    return $data;
}

function getPendidikanDosen(){
	$data = [];
		// SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1) AS nama, count(SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1)) as jumlah
		// FROM devel_simakip.dosen
		// WHERE SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1) <> ''
		// GROUP BY SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1)
	$dosens = Dosen::select(new raw("SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1) AS nama"),new 					raw("count(SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1)) as jumlah"))
	            ->groupBy(new raw("SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1)"))
	            ->whereRaw("SUBSTRING_INDEX(SUBSTRING_INDEX(jenjang_pendidikan, ' ', 1), ' ', -1) <> ''")->where('isdelete','=','0')->get();
    $data = [];
    foreach($dosens->toArray() as $value){
    	$data[] = ["name"=>$value["nama"],"y"=>(int)$value["jumlah"]];
    }
    return $data;
}

function getPeriode(){
	$now = (int) Date('Y');
	for($start=2016;$start<$now;$start+=2){
		if($start<=$now and $now<=$start+2)
			return array("dari"=>$start,"sampai"=>$start+2);
	}
}

?>