<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function celery()
{
    require_once 'celery/celery.php';
    require_once 'celery/redisconnector.php';
    $CI = &get_instance();
    // $celeryParams = array('host'=>'localhost','login'=>'guest','password'=>'guest','vhost'=>null,'port'=>'6379','connector'=>'redis');
    $celery = new Celery($CI->config->item('redis_url'), null, null, null, $CI->config->item('celery_queue'), $CI->config->item('celery_queue'), 6379, 'redis');
    return $celery;
}

function send_notif($dosen, $type, $content)
{
    celery()->PostTask('tasks.send_notif', array(base_url(), $dosen, $type, $content));
}

function send_notif_luaran($dosen, $content)
{
    celery()->PostTask('tasks.send_notif', array(base_url(), $dosen, 'Luaran', $content));
}

function send_notif_penelitian($dosen, $content)
{
    celery()->PostTask('tasks.send_notif', array(base_url(), $dosen, 'Penelitian', $content));
}

function send_notif_pengabdian($dosen, $content)
{
    celery()->PostTask('tasks.send_notif', array(base_url(), $dosen, 'Pengabdian', $content));
}

function send_notif_suratkontrak($dosen, $content)
{
    celery()->PostTask('tasks.send_notif', array(base_url(), $dosen, 'Surat kontrak', $content));
}

function send_email_penelitian_anggota($datas)
{
    celery()->PostTask('tasks.send_mail_anggota', array(base_url(), $datas));
}

function send_email_penelitian_dibuat($data)
{
    celery()->PostTask('tasks.send_mail_penelitian_dibuat', array(base_url(), $data));
}

function send_email_penelitian_diperbaiki($data)
{
    celery()->PostTask('tasks.send_mail_penelitian_diperbaiki', array(base_url(), $data));
}

function sendemail_penelitian_tersedia($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Skim Penelitian Internal UHAMKA telah Tersedia');
    $CI->email->message($CI->load->view('email_template/email_penelitian_tersedia', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_diperpanjang($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Perpanjangan Pengusulan Skim Penelitian Internal UHAMKA');
    $CI->email->message($CI->load->view('email_template/email_penelitian_diperpanjang', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_expire($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Skim Penelitian Internal UHAMKA telah ditutup');
    $CI->email->message($CI->load->view('email_template/email_penelitian_expire', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_anggota($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Keanggotaan Penelitian');
    $CI->email->message($CI->load->view('email_template/email_penelitian_anggota', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pengabdian_anggota($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Keanggotaan Pengabdian');
    $CI->email->message($CI->load->view('email_template/email_pengabdian_anggota', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_dibuat($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Usulan Proposal Penelitian ' . $list["jenis_penelitian"] . " " . $list["nama_batch"] . "-" . $list["tahun_batch"]);
    $CI->email->message($CI->load->view('email_template/email_penelitian_dibuat', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_diperbaiki($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 5));
    $CI->email->subject('Perbaikan usulan penelitian ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_diperbaiki', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_disubmit($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 5));
    $CI->email->subject('Submit Usulan Penelitian Judul:' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_disubmit', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pengabdian_disubmit($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 5));
    $CI->email->subject('Submit Usulan Pengabdian Judul:' . $judul);
    $CI->email->message($CI->load->view('email_template/email_pengabdian_disubmit', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_valid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Hasil Validasi Usulan Proposal');
    $CI->email->message($CI->load->view('email_template/email_penelitian_valid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pengabdian_valid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Hasil Validasi Usulan Proposal');
    $CI->email->message($CI->load->view('email_template/email_pengabdian_valid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pengabdian_pelaksanaan($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Lokasi dan tempat pelaksanaan monev');
    $CI->email->message($CI->load->view('email_template/email_pengabdian_pelaksanaan', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_invalid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Hasil Validasi Usulan Proposal');
    $CI->email->message($CI->load->view('email_template/email_penelitian_invalid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pengabdian_invalid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Hasil Validasi Usulan Proposal');
    $CI->email->message($CI->load->view('email_template/email_pengabdian_invalid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_reviewer($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Undangan Review Usulan Proposal UHAMKA - ' . date('m') . ' ' . date('Y'));
    $CI->email->message($CI->load->view('email_template/email_penelitian_reviewer', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pengabdian_reviewer($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $CI->email->subject('Undangan Review Usulan Proposal UHAMKA - ' . date('m') . ' ' . date('Y'));
    $CI->email->message($CI->load->view('email_template/email_pengabdian_reviewer', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_diterima($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Hasil review usulan proposal ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_diterima', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_diterimasyarat($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Hasil review usulan proposal ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_diterimasyarat', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_ditolak($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Hasil review usulan proposal ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_ditolak', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_syarat_invalid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Perbaikan usulan proposal ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_syarat_invalid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_penelitian_syarat_valid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Perbaikan usulan proposal ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_penelitian_syarat_valid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_laporan_disubmit($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Laporan Penelitian, Judul: ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_laporan_disubmit', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_laporan_pengabdian_disubmit($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Laporan Pengabdian, Judul: ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_laporan_disubmit', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_laporan_valid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Laporan Penelitian, Judul: ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_laporan_valid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_laporan_pengabdian_valid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Laporan Pengabdian, Judul: ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_laporan_pengabdian_valid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_laporan_invalid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Laporan Penelitian, Judul: ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_laporan_invalid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_laporan_pengabdian_invalid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Pengabdian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Laporan Pengabdian, Judul: ' . $judul);
    $CI->email->message($CI->load->view('email_template/email_laporan_pengabdian_invalid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_suratkontrak_disubmit($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Upload Surat Kontrak 1, Judul:' . $judul);
    $CI->email->message($CI->load->view('email_template/email_suratkontrak_disubmit', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_suratkontrak_invalid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Hasil Validasi Surat Kontrak, Judul:' . $judul);
    $CI->email->message($CI->load->view('email_template/email_suratkontrak_invalid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_suratkontrak_valid($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Hasil Validasi Surat Kontrak, Judul:' . $judul);
    $CI->email->message($CI->load->view('email_template/email_suratkontrak_valid', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pencairandanaproses($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Informasi Pencairan dana penelitian ' . $batch . '-' . $tahun . ' ' . $type);
    $CI->email->message($CI->load->view('email_template/pencairandanaproses', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_pencairandanaconfirm($list)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }

    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($list["surel"]);

    $judul = implode(' ', array_slice(explode(' ', $list["judul"]), 0, 3));
    $CI->email->subject('Informasi Pencairan dana penelitian ' . $batch . '-' . $tahun . ' ' . $type);
    $CI->email->message($CI->load->view('email_template/pencairandanaconfirm', $list, true));
    $CI->email->set_mailtype('html');
    $CI->email->send();
}

function sendemail_testemail($email)
{
    $CI = &get_instance();
    $emailconfig = $CI->config->item('email');
    if (!empty($emailconfig)) {
        $CI->load->library('email', $emailconfig);
        $CI->email->set_newline("\r\n");
    } else {
        $CI->load->library('email');
    }
    $CI->email->from('lemlit@uhamka.ac.id', 'Lembaga Penelitian UHAMKA');
    $CI->email->to($email);

    $CI->email->subject('Test email');
    $CI->email->message('The email send using codeigniter library test');
    $CI->email->set_mailtype('html');
    $CI->email->send();
    echo $CI->email->print_debugger();
}
