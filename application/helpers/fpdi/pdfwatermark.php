<?php
require('fpdf.php');
require_once 'fpdi.php';


class PDF_Rotate extends FPDI {

var $angle = 0;

function Rotate($angle, $x = -1, $y = -1) {
    if ($x == -1)
        $x = $this->x;
    if ($y == -1)
        $y = $this->y;
    if ($this->angle != 0)
        $this->_out('Q');
    $this->angle = $angle;
    if ($angle != 0) {
        $angle*=M_PI / 180;
        $c = cos($angle);
        $s = sin($angle);
        $cx = $x * $this->k;
        $cy = ($this->h - $y) * $this->k;
        $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
    }
}

function _endpage() {
    if ($this->angle != 0) {
        $this->angle = 0;
        $this->_out('Q');
    }
    parent::_endpage();
}

}



$fullPathToFile = "chinmay235.pdf";

class PDF extends PDF_Rotate {

var $_tplIdx;

public function __construct($file,$watermarkurl) {
     $this->filepdf=$file;
     $this->watermarkurl=$watermarkurl;
     parent::__construct();
}

// function Header() {
//     //Put the watermark
//     $this->Image($this->watermarkurl, 40, 100, 100, 0, 'PNG');
//     // $this->SetFont('Arial', 'B', 50);
//     // $this->SetTextColor(255, 192, 203);
//     // $this->RotatedText(20, 230, 'Raddyx Technologies Pvt. Ltd.', 45);

//     if (is_null($this->_tplIdx)) {

//         // THIS IS WHERE YOU GET THE NUMBER OF PAGES
//         $this->numPages = $this->setSourceFile($this->filepdf);
//         // echo $this->importPage(1); die;
//         $this->_tplIdx = $this->importPage(1);
//     }
//     $this->useTemplate($this->_tplIdx, 0, 0, 200);


// }

function watermark(){
    if (is_null($this->_tplIdx)) {

        // THIS IS WHERE YOU GET THE NUMBER OF PAGES
        $this->numPages = $this->setSourceFile($this->filepdf);
        // echo $this->importPage(1); die;
    }
    for($i = 1; $i <= $this->numPages; $i++){
        $this->addPage();//<- moved from outside loop
        $tplidx = $this->importPage($i);
        $this->useTemplate($tplidx, 0, 0, 200);
        $this->Image($this->watermarkurl, 0, 0, 45, 0, 'PNG');
    }
}

function RotatedText($x, $y, $txt, $angle) {
    //Text rotated around its origin
    $this->Rotate($angle, $x, $y);
    $this->Text($x, $y, $txt);
    $this->Rotate(0);
}

}


?>