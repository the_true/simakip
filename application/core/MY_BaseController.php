<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_BaseController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct($twig_config=[],$check_login=True){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$twig_config["functions"][] = "getUserInfo";
		$twig_config["functions"][] = "getNotif";
		$twig_config["functions"][] = "request_get";
		$twig_config["functions"][] = "isviewall";
		$twig_config["functions"][] = "isvalidasi";
		$twig_config["functions"][] = "isaddedit";
		$twig_config["functions"][] = "isdelete";
		$twig_config["functions"][] = "tautan";
		$twig_config["functions"][] = "role";
		$twig_config["functions"][] = "roles";
		$twig_config["functions"][] = "issuperadmin";
		$twig_config["functions"][] = "isnotaddeditdeldosen";
		$twig_config["functions"][] = "isnotkonfigurasi";
		$twig_config["functions"][] = "get_kontak";
		$twig_config["functions_safe"][] = "paging_creator";
		$twig_config["functions"][] = "set_select";
		$twig_config["functions"][] = "set_checkbox";
		$twig_config["functions"][] = "current_url";
		// $twig_config["functions"][] = "get_message";
		$this->load->library('twig', $twig_config);
		$this->load->helper('url');
		$this->load->model('Log_Activity');
		$this->twig->addGlobal('session', $this->session);
		if($check_login) $this->is_login();
 	}

    function _remap($method, $params=array())
    {
        $methodToCall = method_exists($this, $method) ? $method : 'index';
        return call_user_func_array(array($this, $methodToCall), $params);
    }

	public function index()
	{
		$this->is_login();
		$this->load->view('welcome_message');
	}

	public function is_login(){
		$userinfo = $this->session->userdata('userinfo');
		if(!isset($userinfo))
			redirect('login', 'refresh');
		return $userinfo;
	}

	public function check_login(){
		$userinfo = $this->session->userdata('userinfo');
		if(!isset($userinfo))
			return False;
		return True;
	}

	public function create_paging($model){
		$default_view = 10;
		$page = 0;

		$view_get = $this->input->get("views");
		$page_get = $this->input->get("page");
		if(isset($page_get)) $page = $page_get-1;
		if(isset($view_get)) $default_view = $view_get;

		$default_skip = $page*$default_view;
		$count  = $model->count();
		$total_pages = $count>0? ceil($count/$default_view):1;
		return ["page"=>($page+1),"views"=>$default_view,"limit"=>$default_view,"skip"=>$default_skip,"total_pages"=>$total_pages,"total"=>$count];
	}

	public function gen_uuid() {
	    return sprintf( '%04x%04x%04x%04x%04x%04x%04x%04x',
	        // 32 bits for "time_low"
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

	        // 16 bits for "time_mid"
	        mt_rand( 0, 0xffff ),

	        // 16 bits for "time_hi_and_version",
	        // four most significant bits holds version number 4
	        mt_rand( 0, 0x0fff ) | 0x4000,

	        // 16 bits, 8 bits for "clk_seq_hi_res",
	        // 8 bits for "clk_seq_low",
	        // two most significant bits holds zero and one for variant DCE1.1
	        mt_rand( 0, 0x3fff ) | 0x8000,

	        // 48 bits for "node"
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	    );
	}

	protected function isOperator(){
		if($this->is_login()['akses']['nama']!='Operator Lemlitbang' && 
		$this->is_login()['akses']['nama']!='Sekretaris Lemlitbang' && 
		$this->is_login()['akses']['nama']!='Ketua Lemlitbang' &&
		$this->is_login()['akses']['nama']!='Operator LPPM' && 
		$this->is_login()['akses']['nama']!='Sekretaris LPPM' && 
		$this->is_login()['akses']['nama']!='Ketua LPPM'){
			$this->twig->display('404');
			return FALSE;
		}
		return TRUE;
	}

	protected function isAkses($akses,$isredirect=False){
		if($this->is_login()['issuperadmin']==1)  return true;
		if(in_array($this->is_login()['akses']['nama'], $akses)){
			return TRUE;
		}
		if(!$isredirect){
			$this->twig->display('404');
			return FALSE;
		}
		redirect('');
		return FALSE;
	}

	protected function isPengguna($id){
		if($this->is_login()['id']!=$id && !$this->isOperator()){
			$this->twig->display('404');
			return FALSE;
		}
		return TRUE;
	}
}
