<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
 */
$route['default_controller'] = 'login';
$route['pengaturan/jenishki/add'] = 'pengaturan/jenishkiadd';
$route['404_override'] = 'login/pagenotfound';
$route['translate_uri_dashes'] = false;
$route['(?i)suratkontraks/pengabdian/(:any)'] = 'suratkontraks/pengabdian/$1';
$route['(?i)suratkontraks/pengabdian/(:any)/(.+)'] = 'suratkontraks/pengabdian/$1/$2';
$route['(?i)suratkontraks/(:any)'] = 'suratkontraks/penelitian/$1';
$route['(?i)suratkontraks/(:any)/(.+)'] = 'suratkontraks/penelitian/$1/$2';
$route['(?i)akreditas/(:any)'] = 'akreditas/$1';
$route['(?i)reports/(:any)'] = 'reports/$1';
$route['(?i)points/(:any)'] = 'points/$1';
$route['(?i)repository/(:any)'] = 'repos/$1';
$route['(?i)monev/(:any)'] = 'monev/$1';
$route['(?i)penelitian/(:any)'] = 'penelitian/$1';
$route['(?i)petapotensi/(:any)'] = 'petapotensi/$1';
$route['(?i)pencairandana/(:any)'] = 'pencairandana/$1';
$route['(?i)pengaturan/penelitian/(:any)'] = 'pengaturan/penelitian/$1';
$route['(?i)pengaturan/pengabdian/(:any)'] = 'pengaturan/pengabdian/$1';
$route['(?i)pengaturan/pengabdian/(:any)/(.+)'] = 'pengaturan/pengabdian/$1/$2';
$route['(?i)pengaturan/potensipenelitian/(:any)'] = 'pengaturan/potensipenelitian/$1';

$route['isianpengesahans'] = 'pengaturan/penelitian/isianpengesahans';
$route['isianpengesahans/(.+)'] = 'pengaturan/penelitian/isianpengesahans/$1';
$route['jenispenelitians'] = 'pengaturan/penelitian/jenispenelitians';
$route['jenispenelitians/(.+)'] = 'pengaturan/penelitian/jenispenelitians/$1';
$route['batchlists'] = 'pengaturan/penelitian/batchlists';
$route['batchlists/(.+)'] = 'pengaturan/penelitian/batchlists/$1';

$route['batasanggarans'] = 'pengaturan/penelitian/batasanggarans';
$route['batasanggarans/(.+)'] = 'pengaturan/penelitian/batasanggarans/$1';
$route['batchpenelitians'] = 'pengaturan/penelitian/batchpenelitians';
$route['batchpenelitians/(.+)'] = 'pengaturan/penelitian/batchpenelitians/$1';
$route['batchs'] = 'pengaturan/penelitian/batchs';
$route['batchs/(.+)'] = 'pengaturan/penelitian/batchs/$1';
$route['targetluarans'] = 'pengaturan/penelitian/targetluarans';
$route['targetluarans/(.+)'] = 'pengaturan/penelitian/targetluarans/$1';

$route['janjiluarans'] = 'pengaturan/penelitian/janjiluarans';
$route['janjiluarans/(.+)'] = 'pengaturan/penelitian/janjiluarans/$1';

$route['keanggotaans'] = 'pengaturan/penelitian/keanggotaans';
$route['keanggotaans/(.+)'] = 'pengaturan/penelitian/keanggotaans/$1';
$route['penilaians'] = 'pengaturan/penelitian/penilaians';
$route['penilaians/(.+)'] = 'pengaturan/penelitian/penilaians/$1';
$route['penilaiankriterias'] = 'pengaturan/penelitian/penilaiankriterias';
$route['penilaiankriterias/(.+)'] = 'pengaturan/penelitian/penilaiankriterias/$1';
$route['syaratpenelitians'] = 'pengaturan/penelitian/syaratpenelitians';
$route['syaratpenelitians/(.+)'] = 'pengaturan/penelitian/syaratpenelitians/$1';

$route['jurnals'] = 'manajemenluaran/jurnals';
$route['jurnals/(.+)'] = 'manajemenluaran/jurnals/$1';
$route['bukuajars'] = 'manajemenluaran/bukuajars';
$route['bukuajars/(.+)'] = 'manajemenluaran/bukuajars/$1';
$route['forumilmiahs'] = 'manajemenluaran/forumilmiahs';
$route['forumilmiahs/(.+)'] = 'manajemenluaran/forumilmiahs/$1';
$route['luaranlains'] = 'manajemenluaran/luaranlains';
$route['luaranlains/(.+)'] = 'manajemenluaran/luaranlains/$1';
$route['hkis'] = 'manajemenluaran/hkis';
$route['hkis/(.+)'] = 'manajemenluaran/hkis/$1';
$route['pforumilmiahs'] = 'manajemenluaran/pforumilmiahs';
$route['pforumilmiahs/(.+)'] = 'manajemenluaran/pforumilmiahs/$1';
$route['penelitianhibahs'] = 'manajemenluaran/penelitianhibahs';
$route['penelitianhibahs/(.+)'] = 'manajemenluaran/penelitianhibahs/$1';
$route['penelitiandiktis'] = 'manajemenluaran/penelitiandiktis';
$route['penelitiandiktis/(.+)'] = 'manajemenluaran/penelitiandiktis/$1';

$route['(?i)pengabdian/luaran/(:any)'] = 'pengabdian/luaran/$1';
$route['(?i)pengabdian/luaran/(:any)/(.+)'] = 'pengabdian/luaran/$1/$2';

$route['penelitians'] = 'penelitian/penelitians';
$route['penelitians/(.+)'] = 'penelitian/penelitians/$1';
$route['penelitianslaporans'] = 'penelitian/penelitianslaporans';
$route['penelitianslaporans/(.+)'] = 'penelitian/penelitianslaporans/$1';
$route['penelitianspemenangs'] = 'penelitian/penelitianspemenangs';
$route['penelitianspemenangs/(.+)'] = 'penelitian/penelitianspemenangs/$1';
$route['penelitiansreviews'] = 'penelitian/penelitiansreviews';
$route['penelitiansreviews/(.+)'] = 'penelitian/penelitiansreviews/$1';
$route['penelitianstertolaks'] = 'penelitian/penelitianstertolaks';
$route['penelitianstertolaks/(.+)'] = 'penelitian/penelitianstertolaks/$1';
$route['suratkontrak'] = 'penelitian/suratkontrak';
$route['suratkontrak/(.+)'] = 'penelitian/suratkontrak/$1';
// $route['penelitiansacc'] = 'penelitian/penelitiansacc';
// $route['penelitiansacc/(.+)'] = 'penelitian/penelitiansacc/$1';

$route['jenispengabdian'] = 'pengaturan/pengabdian/jenispengabdian';
$route['jenispengabdian/(.+)'] = 'pengaturan/pengabdian/jenispengabdian/$1';
$route['batchlistspengabdian'] = 'pengaturan/pengabdian/batchlistspengabdian';
$route['batchlistspengabdian/(.+)'] = 'pengaturan/pengabdian/batchlistspengabdian/$1';
$route['iptek'] = 'pengaturan/pengabdian/iptekpengabdian';
$route['iptek/(.+)'] = 'pengaturan/pengabdian/iptekpengabdian/$1';
$route['mitra'] = 'pengaturan/pengabdian/mitrapengabdian';
$route['mitra/(.+)'] = 'pengaturan/pengabdian/mitrapengabdian/$1';
$route['catatanpenilaian'] = 'pengaturan/pengabdian/catatanpenilaian';
$route['catatanpenilaian/(.+)'] = 'pengaturan/pengabdian/catatanpenilaian/$1';
