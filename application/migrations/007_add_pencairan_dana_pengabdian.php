<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_add_pencairan_dana_pengabdian extends CI_Migration
{

    public function up()
    {
        ## Create Table pencairan_dana1
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => false,
                'auto_increment' => true,
            ),
            'pengabdian' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => false,

            ),
            'status' => array(
                'type' => 'SMALLINT',
                'constraint' => 6,
                'null' => true,
                'default' => '1',

            ),
            'pajak' => array(
                'type' => 'DOUBLE',
                'null' => false,

            ),
            'dana' => array(
                'type' => 'DECIMAL',
                'constraint' => 19, 0,
                'null' => true,

            ),
            'pajak_hitung' => array(
                'type' => 'DECIMAL',
                'constraint' => 19, 0,
                'null' => true,

            ),
            'tanggal_transfer' => array(
                'type' => 'TEXT',
                'null' => true,

            ),
            'tanggal_konfirm' => array(
                'type' => 'TEXT',
                'null' => true,

            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true,

            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true,

            ),
        ));
        $this->dbforge->add_key("id", true);
        $this->dbforge->create_table("pengabdian_pencairan_dana1", true);
        $this->db->query('ALTER TABLE  `pengabdian_pencairan_dana1` ENGINE = InnoDB');

        ## Create Table pencairan_dana2
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => false,
                'auto_increment' => true,
            ),
            'pengabdian' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => true,

            ),
            'status' => array(
                'type' => 'SMALLINT',
                'constraint' => 6,
                'null' => true,
                'default' => '2',

            ),
            'pajak' => array(
                'type' => 'DOUBLE',
                'null' => true,

            ),
            'dana' => array(
                'type' => 'DECIMAL',
                'constraint' => 19, 0,
                'null' => true,

            ),
            'pajak_hitung' => array(
                'type' => 'DECIMAL',
                'constraint' => 19, 0,
                'null' => true,

            ),
            'tanggal_transfer' => array(
                'type' => 'TEXT',
                'null' => true,

            ),
            'tanggal_konfirm' => array(
                'type' => 'TEXT',
                'null' => true,

            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true,

            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true,

            ),
        ));
        $this->dbforge->add_key("id", true);
        $this->dbforge->create_table("pengabdian_pencairan_dana2", true);
        $this->db->query('ALTER TABLE  `pengabdian_pencairan_dana2` ENGINE = InnoDB');

    }

    public function down()
    {
        $this->dbforge->drop_table("pencairan_dana1", true);
        $this->dbforge->drop_table("pencairan_dana2", true);
    }
}
