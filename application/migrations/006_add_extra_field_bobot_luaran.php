<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_add_extra_field_bobot_luaran extends CI_Migration
{

    public function up()
    {
        // add field target_luaran_master
        $fields = array(
            'data7' => array(
                'type' => 'VARCHAR',
                'constraint' => 45,
                'null' => true,

            ),
        );
        $this->dbforge->add_column('bobot_luaran', $fields);

    }

    public function down()
    {
        $this->dbforge->drop_column('bobot_luaran', 'data7');
    }
}
