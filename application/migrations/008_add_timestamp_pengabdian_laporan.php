<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_add_timestamp_pengabdian_laporan extends CI_Migration
{

    public function up()
    {
        // add field target_luaran_master
        $fields = array(
            'updated_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true,

            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => true,

            )
        );
        $this->dbforge->add_column('pengabdian_laporan', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('pengabdian_laporan', 'created_at');
        $this->dbforge->drop_column('pengabdian_laporan', 'updated_at');
    }
}
