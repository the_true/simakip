<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_add_string_table extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ),
            'key' => array(
                'type' => 'VARCHAR',
                'constraint' => 150,
                'null' => true,
            ),
            'string' => array(
                'type' => 'TEXT',
                'null' => true,
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('notes');
    }

    public function down()
    {
        $this->dbforge->drop_table('notes');
    }
}
