<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_modify_luaran_pengabdian extends CI_Migration
{

    public function up()
    {
        // add field target_luaran_master
        $fields = array(
            'identifier' => array(
                'type' => 'VARCHAR',
                'constraint' => 45,
                'null' => true,
                'default' => 'penelitian',

            ),
        );
        $this->dbforge->add_column('jurnal', $fields);
        $this->dbforge->add_column('buku_ajar', $fields);
        $this->dbforge->add_column('forum_ilmiah', $fields);
        $this->dbforge->add_column('hki', $fields);
        $this->dbforge->add_column('luaran_lain', $fields);
        $this->dbforge->add_column('penelitian_hibah', $fields);
        $this->dbforge->add_column('penyelenggara_forum_ilmiah', $fields);

    }

    public function down()
    {
        $this->dbforge->drop_column('jurnal', 'identifier');
        $this->dbforge->drop_column('buku_ajar', 'identifier');
        $this->dbforge->drop_column('forum_ilmiah', 'identifier');
        $this->dbforge->drop_column('hki', 'identifier');
        $this->dbforge->drop_column('luaran_lain', 'identifier');
        $this->dbforge->drop_column('penelitian_hibah', 'identifier');
        $this->dbforge->drop_column('penyelenggara_forum_ilmiah', 'identifier');
    }
}
