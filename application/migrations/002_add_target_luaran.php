<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_add_target_luaran extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ),
            'penelitian' => array(
                'type' => 'INT',
            ),
            'target_luaran' => array(
                'type' => 'INT',
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('penelitian_target_luaran');
    }

    public function down()
    {
        $this->dbforge->drop_table('penelitian_target_luaran');
    }
}
