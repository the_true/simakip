<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_base extends CI_Migration {

	public function up() {

		## Create Table akreditas
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'fakultas' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'program_studi' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'tahun_dari' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'tahun_sampai' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("akreditas", TRUE);
		$this->db->query('ALTER TABLE  `akreditas` ENGINE = InnoDB');

		## Create Table akreditas_dosen
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'akreditas' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("akreditas_dosen", TRUE);
		$this->db->query('ALTER TABLE  `akreditas_dosen` ENGINE = InnoDB');

		## Create Table akses
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("akses", TRUE);
		$this->db->query('ALTER TABLE  `akses` ENGINE = MyISAM');

		## Create Table akses_page
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'akses' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'akses_menu' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,

			),
			'akses_method' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("akses_page", TRUE);
		$this->db->query('ALTER TABLE  `akses_page` ENGINE = InnoDB');

		## Create Table akses_user
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'akses' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("akses_user", TRUE);
		$this->db->query('ALTER TABLE  `akses_user` ENGINE = InnoDB');

		## Create Table batas_anggaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'batas' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("batas_anggaran", TRUE);
		$this->db->query('ALTER TABLE  `batas_anggaran` ENGINE = InnoDB');

		## Create Table batasan_dana
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'batasan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("batasan_dana", TRUE);
		$this->db->query('ALTER TABLE  `batasan_dana` ENGINE = InnoDB');

		## Create Table batch
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'batch_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'batch_lists' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'start' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'finish' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'deadline_review' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'review' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'monev_start' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'monev_finish' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("batch", TRUE);
		$this->db->query('ALTER TABLE  `batch` ENGINE = InnoDB');

		## Create Table batch_lists
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => FALSE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("batch_lists", TRUE);
		$this->db->query('ALTER TABLE  `batch_lists` ENGINE = InnoDB');

		## Create Table batch_penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'tahun' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("batch_penelitian", TRUE);
		$this->db->query('ALTER TABLE  `batch_penelitian` ENGINE = InnoDB');

		## Create Table billboard
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'url' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE,

			),
			'image' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'deskripsi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("billboard", TRUE);
		$this->db->query('ALTER TABLE  `billboard` ENGINE = MyISAM');

		## Create Table bobot_hasil
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("bobot_hasil", TRUE);
		$this->db->query('ALTER TABLE  `bobot_hasil` ENGINE = InnoDB');

		## Create Table bobot_kriteria
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'bobot_hasil' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'indikator' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'bobot' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("bobot_kriteria", TRUE);
		$this->db->query('ALTER TABLE  `bobot_kriteria` ENGINE = InnoDB');

		## Create Table bobot_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => FALSE,

			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'data1' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'data2' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'data3' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'data4' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'data5' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'data6' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("bobot_luaran", TRUE);
		$this->db->query('ALTER TABLE  `bobot_luaran` ENGINE = InnoDB');

		## Create Table buku_ajar
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isbn' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'halaman' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'penerbit' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("buku_ajar", TRUE);
		$this->db->query('ALTER TABLE  `buku_ajar` ENGINE = MyISAM');

		## Create Table dosen
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'gelar_depan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'gelar_belakang' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'jenis_kelamin' => array(
				'type' => 'VARCHAR',
				'constraint' => 1,
				'null' => TRUE,

			),
			'jenjang_pendidikan' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'jabatan_akademik' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'jabatan_fungsional' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'fakultas' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'program_studi' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'alamat' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'telp' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'hp' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'surel' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'status_dosen' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'status_data' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'akses' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'password' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'salt' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'photo' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,
				'default' => 'default.png',

			),
			'issuperadmin' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,
				'default' => '0',

			),
			'nidn' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isreviewer' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,
				'default' => '0',

			),
			'isketuaprodi' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,
				'default' => '0',

			),
			'passworddikti' => array(
				'type' => 'VARCHAR',
				'constraint' => 250,
				'null' => TRUE,

			),
			'bank' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'bank_cabang' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'bank_rek' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'bank_photo' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'npwp_nomor' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'npwp_photo' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("dosen", TRUE);
		$this->db->query('ALTER TABLE  `dosen` ENGINE = MyISAM');

		## Create Table fakultas
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("fakultas", TRUE);
		$this->db->query('ALTER TABLE  `fakultas` ENGINE = MyISAM');

		## Create Table format_nomor
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'format_a' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,

			),
			'format_b' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("format_nomor", TRUE);
		$this->db->query('ALTER TABLE  `format_nomor` ENGINE = InnoDB');

		## Create Table forum_ilmiah
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'forum' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'penyelenggara' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'tempat' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'tingkat' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'waktu' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("forum_ilmiah", TRUE);
		$this->db->query('ALTER TABLE  `forum_ilmiah` ENGINE = MyISAM');

		## Create Table hki
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'pendaftaran' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'jenis' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("hki", TRUE);
		$this->db->query('ALTER TABLE  `hki` ENGINE = MyISAM');

		## Create Table iptek
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("iptek", TRUE);
		$this->db->query('ALTER TABLE  `iptek` ENGINE = InnoDB');

		## Create Table jabatan_akademik
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jabatan_akademik", TRUE);
		$this->db->query('ALTER TABLE  `jabatan_akademik` ENGINE = MyISAM');

		## Create Table jabatan_fungsi
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jabatan_fungsi", TRUE);
		$this->db->query('ALTER TABLE  `jabatan_fungsi` ENGINE = MyISAM');

		## Create Table jenis_hki
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jenis_hki", TRUE);
		$this->db->query('ALTER TABLE  `jenis_hki` ENGINE = MyISAM');

		## Create Table jenis_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jenis_luaran", TRUE);
		$this->db->query('ALTER TABLE  `jenis_luaran` ENGINE = MyISAM');

		## Create Table jenis_penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'jabatan_akademik' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'syarat' => array(
				'type' => 'VARCHAR',
				'constraint' => 12,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jenis_penelitian", TRUE);
		$this->db->query('ALTER TABLE  `jenis_penelitian` ENGINE = InnoDB');

		## Create Table jenis_pengabdian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jenis_pengabdian", TRUE);
		$this->db->query('ALTER TABLE  `jenis_pengabdian` ENGINE = InnoDB');

		## Create Table jenis_pgabungan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jenis_pgabungan", TRUE);
		$this->db->query('ALTER TABLE  `jenis_pgabungan` ENGINE = MyISAM');

		$this->db->query(
			"CREATE DEFINER=`root`@`localhost` TRIGGER `sync_id_penelitian`\r\n
		BEFORE INSERT ON `jenis_penelitian`\r\n
		FOR EACH ROW\r\n
		BEGIN\r\n
			DECLARE newID INT;\r\n
			INSERT INTO `jenis_pgabungan` (id,type) values (NULL,'penelitian');\r\n
			SET newID = (SELECT id FROM `jenis_pgabungan` WHERE type = 'penelitian' ORDER BY id DESC LIMIT 1); \r\n
			SET NEW.id := newID;\r\n
		END\r\n"
		);

		$this->db->query(
		"CREATE DEFINER=`root`@`localhost` TRIGGER `sync_id_pengabdian`\r\n
		BEFORE INSERT ON `jenis_pengabdian`\r\n
		FOR EACH ROW\r\n
		BEGIN\r\n
			DECLARE newID INT;\r\n
			INSERT INTO `jenis_pgabungan` (id,type) values (NULL,'pengabdian');\r\n
			SET newID = (SELECT id FROM `jenis_pgabungan` WHERE type = 'pengabdian' ORDER BY id DESC LIMIT 1); \r\n
			SET NEW.id := newID;\r\n
		END\r\n"
		);

		## Create Table jurnal
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'personil' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'nama_jurnal' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'issn' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'volume' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'nomor' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'url' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'halaman' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'nama_nondosen' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'program_studi_nondosen' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'institusi_nondosen' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'keanggotaan_dosen' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'keanggotaan_nondosen' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("jurnal", TRUE);
		$this->db->query('ALTER TABLE  `jurnal` ENGINE = MyISAM');

		## Create Table karyawan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'jenis_kelamin' => array(
				'type' => 'VARCHAR',
				'constraint' => 1,
				'null' => TRUE,

			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'telp' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alamat' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'hp' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'surel' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'jabatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => 1,
				'null' => TRUE,

			),
			'akses' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'username' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'password' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("karyawan", TRUE);
		$this->db->query('ALTER TABLE  `karyawan` ENGINE = MyISAM');

		## Create Table keanggotaan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'notes' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("keanggotaan", TRUE);
		$this->db->query('ALTER TABLE  `keanggotaan` ENGINE = InnoDB');

		## Create Table kontak
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'isi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("kontak", TRUE);
		$this->db->query('ALTER TABLE  `kontak` ENGINE = MyISAM');

		## Create Table log
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'action' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("log", TRUE);
		$this->db->query('ALTER TABLE  `log` ENGINE = InnoDB');

		## Create Table luaran_lain
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'deskripsi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("luaran_lain", TRUE);
		$this->db->query('ALTER TABLE  `luaran_lain` ENGINE = MyISAM');

		## Create Table mitra
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("mitra", TRUE);
		$this->db->query('ALTER TABLE  `mitra` ENGINE = InnoDB');

		## Create Table monev
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'borang' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'reviewer' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'hasilreview' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev", TRUE);
		$this->db->query('ALTER TABLE  `monev` ENGINE = InnoDB');

		## Create Table monev_borang
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev_borang", TRUE);
		$this->db->query('ALTER TABLE  `monev_borang` ENGINE = InnoDB');

		## Create Table monev_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev_luaran", TRUE);
		$this->db->query('ALTER TABLE  `monev_luaran` ENGINE = InnoDB');

		## Create Table monev_nilai_hasil
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'monev_review' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'bobot_hasil' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'skor' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'nilai' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'komentar' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev_nilai_hasil", TRUE);
		$this->db->query('ALTER TABLE  `monev_nilai_hasil` ENGINE = InnoDB');

		## Create Table monev_nilai_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'monev_review' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'bobot_luaran' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'skor' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'nilai' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'komentar' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev_nilai_luaran", TRUE);
		$this->db->query('ALTER TABLE  `monev_nilai_luaran` ENGINE = InnoDB');

		## Create Table monev_review
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev_review", TRUE);
		$this->db->query('ALTER TABLE  `monev_review` ENGINE = InnoDB');

		## Create Table monev_reviewer
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("monev_reviewer", TRUE);
		$this->db->query('ALTER TABLE  `monev_reviewer` ENGINE = InnoDB');

		## Create Table news
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'title' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'`created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("news", TRUE);
		$this->db->query('ALTER TABLE  `news` ENGINE = MyISAM');

		## Create Table news_meta
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'body' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'path' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'news' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'url' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("news_meta", TRUE);
		$this->db->query('ALTER TABLE  `news_meta` ENGINE = MyISAM');

		## Create Table notifikasi
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'isread' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("notifikasi", TRUE);
		$this->db->query('ALTER TABLE  `notifikasi` ENGINE = InnoDB');

		## Create Table pajak
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nominal' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pajak", TRUE);
		$this->db->query('ALTER TABLE  `pajak` ENGINE = InnoDB');

		## Create Table pencairan_dana1
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '1',

			),
			'pajak' => array(
				'type' => 'DOUBLE',
				'null' => FALSE,

			),
			'dana' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'pajak_hitung' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'tanggal_transfer' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal_konfirm' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pencairan_dana1", TRUE);
		$this->db->query('ALTER TABLE  `pencairan_dana1` ENGINE = InnoDB');

		## Create Table pencairan_dana2
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '2',

			),
			'pajak' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'dana' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'pajak_hitung' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'tanggal_transfer' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal_konfirm' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pencairan_dana2", TRUE);
		$this->db->query('ALTER TABLE  `pencairan_dana2` ENGINE = InnoDB');

		## Create Table penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'batch' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'keyword' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'lokasi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'target' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'lama' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'total_biaya' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'mandiri_total_biaya' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'mandiri_biaya_instansi' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'dosen_mengetahui' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen_menyetujui' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas_perbaikan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '1',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'submited_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'acc' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian", TRUE);
		$this->db->query('ALTER TABLE  `penelitian` ENGINE = InnoDB');

		## Create Table penelitian_anggota
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'peran' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'konfirmasi' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian_anggota", TRUE);
		$this->db->query('ALTER TABLE  `penelitian_anggota` ENGINE = InnoDB');

		## Create Table penelitian_hibah
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tahun' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'lokasi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'anggaran' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'sumber' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian_hibah", TRUE);
		$this->db->query('ALTER TABLE  `penelitian_hibah` ENGINE = MyISAM');

		## Create Table penelitian_laporan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'laporan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'artikel' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian_laporan", TRUE);
		$this->db->query('ALTER TABLE  `penelitian_laporan` ENGINE = InnoDB');

		## Create Table penelitian_nilai
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian_review' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'penilaian_kriteria' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'skor' => array(
				'type' => 'DECIMAL',
				'constraint' => 9,1,
				'null' => TRUE,

			),
			'nilai' => array(
				'type' => 'DECIMAL',
				'constraint' => 9,2,
				'null' => TRUE,

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian_nilai", TRUE);
		$this->db->query('ALTER TABLE  `penelitian_nilai` ENGINE = InnoDB');

		## Create Table penelitian_review
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rekomendasi' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'saran' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'catatan_review' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'catatan_rekomendasi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian_review", TRUE);
		$this->db->query('ALTER TABLE  `penelitian_review` ENGINE = InnoDB');

		## Create Table penelitian_reviewer
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'deadline' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penelitian_reviewer", TRUE);
		$this->db->query('ALTER TABLE  `penelitian_reviewer` ENGINE = InnoDB');

		## Create Table pengabdian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'batch' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'keyword' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'lokasi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'target' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'anggota_mahasiswa' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'anggota_alumni' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'anggota_staff' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'lama' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'iptek' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'mitra' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'total_biaya' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'mandiri_total_biaya' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'mandiri_biaya_instansi' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'dosen_mengetahui' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen_menyetujui' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas_surat' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas_perbaikan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '1',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'submited_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'acc' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian` ENGINE = InnoDB');

		## Create Table pengabdian_anggota
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'peran' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'konfirmasi' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_anggota", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_anggota` ENGINE = InnoDB');

		## Create Table pengabdian_laporan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'laporan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'artikel' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'abstrak' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_laporan", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_laporan` ENGINE = InnoDB');

		## Create Table pengabdian_monev
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'borang' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'reviewer' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'hasilreview' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'waktu' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'lokasi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev` ENGINE = InnoDB');

		## Create Table pengabdian_monev_borang
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev_borang", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev_borang` ENGINE = InnoDB');

		## Create Table pengabdian_monev_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev_luaran", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev_luaran` ENGINE = InnoDB');

		## Create Table pengabdian_monev_nilai_hasil
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian_monev_review' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'bobot_hasil' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'skor' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'nilai' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'komentar' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev_nilai_hasil", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev_nilai_hasil` ENGINE = InnoDB');

		## Create Table pengabdian_monev_nilai_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian_monev_review' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'bobot_luaran' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'skor' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'nilai' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'komentar' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev_nilai_luaran", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev_nilai_luaran` ENGINE = InnoDB');

		## Create Table pengabdian_monev_review
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'content' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev_review", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev_review` ENGINE = InnoDB');

		## Create Table pengabdian_monev_reviewer
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_monev_reviewer", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_monev_reviewer` ENGINE = InnoDB');

		## Create Table pengabdian_nilai
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian_review' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'penilaian_kriteria' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'skor' => array(
				'type' => 'DECIMAL',
				'constraint' => 9,1,
				'null' => TRUE,

			),
			'nilai' => array(
				'type' => 'DECIMAL',
				'constraint' => 9,2,
				'null' => TRUE,

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'catatan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_nilai", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_nilai` ENGINE = InnoDB');

		## Create Table pengabdian_review
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rekomendasi' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,0,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'saran' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isvalid' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isselected' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'catatan_review' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'catatan_rekomendasi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_review", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_review` ENGINE = InnoDB');

		## Create Table pengabdian_reviewer
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'pengabdian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'deadline' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengabdian_reviewer", TRUE);
		$this->db->query('ALTER TABLE  `pengabdian_reviewer` ENGINE = InnoDB');

		## Create Table pengesah
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'jabatan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("pengesah", TRUE);
		$this->db->query('ALTER TABLE  `pengesah` ENGINE = InnoDB');

		## Create Table penilaian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penilaian", TRUE);
		$this->db->query('ALTER TABLE  `penilaian` ENGINE = InnoDB');

		## Create Table penilaian_kriteria
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'penilaian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'indikator' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'bobot' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penilaian_kriteria", TRUE);
		$this->db->query('ALTER TABLE  `penilaian_kriteria` ENGINE = InnoDB');

		## Create Table penyelenggara_forum_ilmiah
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'unit' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'mitra' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'skala' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tempat' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'waktu' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tingkat' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'tahun_kegiatan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'alasan' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("penyelenggara_forum_ilmiah", TRUE);
		$this->db->query('ALTER TABLE  `penyelenggara_forum_ilmiah` ENGINE = MyISAM');

		## Create Table persetujuan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'peran' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'nomor' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("persetujuan", TRUE);
		$this->db->query('ALTER TABLE  `persetujuan` ENGINE = InnoDB');

		## Create Table peta_potensi
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'tahun' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("peta_potensi", TRUE);
		$this->db->query('ALTER TABLE  `peta_potensi` ENGINE = InnoDB');

		## Create Table peta_potensi_syarat
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'peta_potensi' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'jabatan_akademik' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'syarat' => array(
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => TRUE,

			),
			'luaran' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'jumlah' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("peta_potensi_syarat", TRUE);
		$this->db->query('ALTER TABLE  `peta_potensi_syarat` ENGINE = InnoDB');

		## Create Table point
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'versi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'ketua' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("point", TRUE);
		$this->db->query('ALTER TABLE  `point` ENGINE = InnoDB');

		## Create Table point_klasifikasi_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'versi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'ketua' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("point_klasifikasi_luaran", TRUE);
		$this->db->query('ALTER TABLE  `point_klasifikasi_luaran` ENGINE = InnoDB');

		## Create Table point_klasifikasi_penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'versi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'ketua' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("point_klasifikasi_penelitian", TRUE);
		$this->db->query('ALTER TABLE  `point_klasifikasi_penelitian` ENGINE = InnoDB');

		## Create Table point_klasifikasi_total
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'versi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'ketua' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("point_klasifikasi_total", TRUE);
		$this->db->query('ALTER TABLE  `point_klasifikasi_total` ENGINE = InnoDB');

		## Create Table point_log
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'type' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'point' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'created_date' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'jenis' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'jenis_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("point_log", TRUE);
		$this->db->query('ALTER TABLE  `point_log` ENGINE = MyISAM');

		## Create Table point_penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'versi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'ketua' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'anggota' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("point_penelitian", TRUE);
		$this->db->query('ALTER TABLE  `point_penelitian` ENGINE = InnoDB');

		## Create Table program_studi
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'fakultas' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("program_studi", TRUE);
		$this->db->query('ALTER TABLE  `program_studi` ENGINE = MyISAM');

		## Create Table role
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'title' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));

		$this->dbforge->create_table("role", TRUE);
		$this->db->query('ALTER TABLE  `role` ENGINE = MyISAM');

		## Create Table subject
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => FALSE,

			),
			'deskripsi' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
			'parent' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("subject", TRUE);
		$this->db->query('ALTER TABLE  `subject` ENGINE = MyISAM');

		## Create Table subject_penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'subject' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'type' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,

			),
			'data' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'judul' => array(
				'type' => 'TEXT',
				'null' => FALSE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("subject_penelitian", TRUE);
		$this->db->query('ALTER TABLE  `subject_penelitian` ENGINE = MyISAM');

		## Create Table subject_tahun
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'tahun_dari' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'tahun_sampai' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("subject_tahun", TRUE);
		$this->db->query('ALTER TABLE  `subject_tahun` ENGINE = MyISAM');

		## Create Table surat
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'isi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'nomor' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'kop' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '1',

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'isvalidate' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("surat", TRUE);
		$this->db->query('ALTER TABLE  `surat` ENGINE = InnoDB');

		## Create Table surat_kontrak
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'dosen' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'nomor' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'tanggal_mulai' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'tanggal_selesai' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'dana' => array(
				'type' => 'DECIMAL',
				'constraint' => 5,2,
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'status' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,

			),
			'isvalidate' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => '0',

			),
			'note' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'surat1' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'surat2' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'persetujuan1' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'persetujuan2' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'tembusan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'batasan_dana' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'format_nomor' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("surat_kontrak", TRUE);
		$this->db->query('ALTER TABLE  `surat_kontrak` ENGINE = InnoDB');

		## Create Table syarat_penelitian
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'syarat' => array(
				'type' => 'VARCHAR',
				'constraint' => 12,
				'null' => TRUE,

			),
			'jabatan_akademik' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("syarat_penelitian", TRUE);
		$this->db->query('ALTER TABLE  `syarat_penelitian` ENGINE = InnoDB');

		## Create Table tahun_kegiatan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'tahun' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("tahun_kegiatan", TRUE);
		$this->db->query('ALTER TABLE  `tahun_kegiatan` ENGINE = MyISAM');

		## Create Table target_luaran
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'jenis_penelitian' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'flag' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("target_luaran", TRUE);
		$this->db->query('ALTER TABLE  `target_luaran` ENGINE = InnoDB');

		## Create Table tautan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'type' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'berkas' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'url' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'default' => '0',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("tautan", TRUE);
		$this->db->query('ALTER TABLE  `tautan` ENGINE = InnoDB');

		## Create Table tembusan
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'isi' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'isdelete' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'status' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,
				'default' => '0',

			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
				'default' => 'penelitian',

			),
		));
		$this->dbforge->add_key("id",true);
		$this->dbforge->create_table("tembusan", TRUE);
		$this->db->query('ALTER TABLE  `tembusan` ENGINE = InnoDB');
	 }

	public function down()	{
		### Drop table akreditas ##
		$this->dbforge->drop_table("akreditas", TRUE);
		### Drop table akreditas_dosen ##
		$this->dbforge->drop_table("akreditas_dosen", TRUE);
		### Drop table akses ##
		$this->dbforge->drop_table("akses", TRUE);
		### Drop table akses_page ##
		$this->dbforge->drop_table("akses_page", TRUE);
		### Drop table akses_user ##
		$this->dbforge->drop_table("akses_user", TRUE);
		### Drop table batas_anggaran ##
		$this->dbforge->drop_table("batas_anggaran", TRUE);
		### Drop table batasan_dana ##
		$this->dbforge->drop_table("batasan_dana", TRUE);
		### Drop table batch ##
		$this->dbforge->drop_table("batch", TRUE);
		### Drop table batch_lists ##
		$this->dbforge->drop_table("batch_lists", TRUE);
		### Drop table batch_penelitian ##
		$this->dbforge->drop_table("batch_penelitian", TRUE);
		### Drop table billboard ##
		$this->dbforge->drop_table("billboard", TRUE);
		### Drop table bobot_hasil ##
		$this->dbforge->drop_table("bobot_hasil", TRUE);
		### Drop table bobot_kriteria ##
		$this->dbforge->drop_table("bobot_kriteria", TRUE);
		### Drop table bobot_luaran ##
		$this->dbforge->drop_table("bobot_luaran", TRUE);
		### Drop table buku_ajar ##
		$this->dbforge->drop_table("buku_ajar", TRUE);
		### Drop table dosen ##
		$this->dbforge->drop_table("dosen", TRUE);
		### Drop table fakultas ##
		$this->dbforge->drop_table("fakultas", TRUE);
		### Drop table format_nomor ##
		$this->dbforge->drop_table("format_nomor", TRUE);
		### Drop table forum_ilmiah ##
		$this->dbforge->drop_table("forum_ilmiah", TRUE);
		### Drop table hki ##
		$this->dbforge->drop_table("hki", TRUE);
		### Drop table iptek ##
		$this->dbforge->drop_table("iptek", TRUE);
		### Drop table jabatan_akademik ##
		$this->dbforge->drop_table("jabatan_akademik", TRUE);
		### Drop table jabatan_fungsi ##
		$this->dbforge->drop_table("jabatan_fungsi", TRUE);
		### Drop table jenis_hki ##
		$this->dbforge->drop_table("jenis_hki", TRUE);
		### Drop table jenis_luaran ##
		$this->dbforge->drop_table("jenis_luaran", TRUE);
		### Drop table jenis_penelitian ##
		$this->dbforge->drop_table("jenis_penelitian", TRUE);
		### Drop table jenis_pengabdian ##
		$this->dbforge->drop_table("jenis_pengabdian", TRUE);
		### Drop table jenis_pgabungan ##
		$this->dbforge->drop_table("jenis_pgabungan", TRUE);
		### Drop table jurnal ##
		$this->dbforge->drop_table("jurnal", TRUE);
		### Drop table karyawan ##
		$this->dbforge->drop_table("karyawan", TRUE);
		### Drop table keanggotaan ##
		$this->dbforge->drop_table("keanggotaan", TRUE);
		### Drop table kontak ##
		$this->dbforge->drop_table("kontak", TRUE);
		### Drop table log ##
		$this->dbforge->drop_table("log", TRUE);
		### Drop table luaran_lain ##
		$this->dbforge->drop_table("luaran_lain", TRUE);
		### Drop table mitra ##
		$this->dbforge->drop_table("mitra", TRUE);
		### Drop table monev ##
		$this->dbforge->drop_table("monev", TRUE);
		### Drop table monev_borang ##
		$this->dbforge->drop_table("monev_borang", TRUE);
		### Drop table monev_luaran ##
		$this->dbforge->drop_table("monev_luaran", TRUE);
		### Drop table monev_nilai_hasil ##
		$this->dbforge->drop_table("monev_nilai_hasil", TRUE);
		### Drop table monev_nilai_luaran ##
		$this->dbforge->drop_table("monev_nilai_luaran", TRUE);
		### Drop table monev_review ##
		$this->dbforge->drop_table("monev_review", TRUE);
		### Drop table monev_reviewer ##
		$this->dbforge->drop_table("monev_reviewer", TRUE);
		### Drop table news ##
		$this->dbforge->drop_table("news", TRUE);
		### Drop table news_meta ##
		$this->dbforge->drop_table("news_meta", TRUE);
		### Drop table notifikasi ##
		$this->dbforge->drop_table("notifikasi", TRUE);
		### Drop table pajak ##
		$this->dbforge->drop_table("pajak", TRUE);
		### Drop table pencairan_dana1 ##
		$this->dbforge->drop_table("pencairan_dana1", TRUE);
		### Drop table pencairan_dana2 ##
		$this->dbforge->drop_table("pencairan_dana2", TRUE);
		### Drop table penelitian ##
		$this->dbforge->drop_table("penelitian", TRUE);
		### Drop table penelitian_anggota ##
		$this->dbforge->drop_table("penelitian_anggota", TRUE);
		### Drop table penelitian_hibah ##
		$this->dbforge->drop_table("penelitian_hibah", TRUE);
		### Drop table penelitian_laporan ##
		$this->dbforge->drop_table("penelitian_laporan", TRUE);
		### Drop table penelitian_nilai ##
		$this->dbforge->drop_table("penelitian_nilai", TRUE);
		### Drop table penelitian_review ##
		$this->dbforge->drop_table("penelitian_review", TRUE);
		### Drop table penelitian_reviewer ##
		$this->dbforge->drop_table("penelitian_reviewer", TRUE);
		### Drop table pengabdian ##
		$this->dbforge->drop_table("pengabdian", TRUE);
		### Drop table pengabdian_anggota ##
		$this->dbforge->drop_table("pengabdian_anggota", TRUE);
		### Drop table pengabdian_laporan ##
		$this->dbforge->drop_table("pengabdian_laporan", TRUE);
		### Drop table pengabdian_monev ##
		$this->dbforge->drop_table("pengabdian_monev", TRUE);
		### Drop table pengabdian_monev_borang ##
		$this->dbforge->drop_table("pengabdian_monev_borang", TRUE);
		### Drop table pengabdian_monev_luaran ##
		$this->dbforge->drop_table("pengabdian_monev_luaran", TRUE);
		### Drop table pengabdian_monev_nilai_hasil ##
		$this->dbforge->drop_table("pengabdian_monev_nilai_hasil", TRUE);
		### Drop table pengabdian_monev_nilai_luaran ##
		$this->dbforge->drop_table("pengabdian_monev_nilai_luaran", TRUE);
		### Drop table pengabdian_monev_review ##
		$this->dbforge->drop_table("pengabdian_monev_review", TRUE);
		### Drop table pengabdian_monev_reviewer ##
		$this->dbforge->drop_table("pengabdian_monev_reviewer", TRUE);
		### Drop table pengabdian_nilai ##
		$this->dbforge->drop_table("pengabdian_nilai", TRUE);
		### Drop table pengabdian_review ##
		$this->dbforge->drop_table("pengabdian_review", TRUE);
		### Drop table pengabdian_reviewer ##
		$this->dbforge->drop_table("pengabdian_reviewer", TRUE);
		### Drop table pengesah ##
		$this->dbforge->drop_table("pengesah", TRUE);
		### Drop table penilaian ##
		$this->dbforge->drop_table("penilaian", TRUE);
		### Drop table penilaian_kriteria ##
		$this->dbforge->drop_table("penilaian_kriteria", TRUE);
		### Drop table penyelenggara_forum_ilmiah ##
		$this->dbforge->drop_table("penyelenggara_forum_ilmiah", TRUE);
		### Drop table persetujuan ##
		$this->dbforge->drop_table("persetujuan", TRUE);
		### Drop table peta_potensi ##
		$this->dbforge->drop_table("peta_potensi", TRUE);
		### Drop table peta_potensi_syarat ##
		$this->dbforge->drop_table("peta_potensi_syarat", TRUE);
		### Drop table point ##
		$this->dbforge->drop_table("point", TRUE);
		### Drop table point_klasifikasi_luaran ##
		$this->dbforge->drop_table("point_klasifikasi_luaran", TRUE);
		### Drop table point_klasifikasi_penelitian ##
		$this->dbforge->drop_table("point_klasifikasi_penelitian", TRUE);
		### Drop table point_klasifikasi_total ##
		$this->dbforge->drop_table("point_klasifikasi_total", TRUE);
		### Drop table point_log ##
		$this->dbforge->drop_table("point_log", TRUE);
		### Drop table point_penelitian ##
		$this->dbforge->drop_table("point_penelitian", TRUE);
		### Drop table program_studi ##
		$this->dbforge->drop_table("program_studi", TRUE);
		### Drop table role ##
		$this->dbforge->drop_table("role", TRUE);
		### Drop table subject ##
		$this->dbforge->drop_table("subject", TRUE);
		### Drop table subject_penelitian ##
		$this->dbforge->drop_table("subject_penelitian", TRUE);
		### Drop table subject_tahun ##
		$this->dbforge->drop_table("subject_tahun", TRUE);
		### Drop table surat ##
		$this->dbforge->drop_table("surat", TRUE);
		### Drop table surat_kontrak ##
		$this->dbforge->drop_table("surat_kontrak", TRUE);
		### Drop table syarat_penelitian ##
		$this->dbforge->drop_table("syarat_penelitian", TRUE);
		### Drop table tahun_kegiatan ##
		$this->dbforge->drop_table("tahun_kegiatan", TRUE);
		### Drop table target_luaran ##
		$this->dbforge->drop_table("target_luaran", TRUE);
		### Drop table tautan ##
		$this->dbforge->drop_table("tautan", TRUE);
		### Drop table tembusan ##
		$this->dbforge->drop_table("tembusan", TRUE);

	}
}