<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_modify_target_luaran extends CI_Migration
{

    public function up()
    {
        // add field target_luaran_master
        $fields = array(
            'target_luaran_master' => array('type' => 'INT'),
        );
        $this->dbforge->add_column('target_luaran', $fields);

        // add table target_luaran_master
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => false,
                'auto_increment' => true,
            ),
            'nama' => array(
                'type' => 'TEXT',
                'null' => true,

            ),
            'isdelete' => array(
                'type' => 'SMALLINT',
                'constraint' => 6,
                'null' => true,
                'default' => 0,

            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null' => true,
                'default' => 'penelitian',
            ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('target_luaran_master');
    }

    public function down()
    {
        $this->dbforge->drop_column('target_luaran', 'target_luaran_master');
        $this->dbforge->drop_table('target_luaran_master');
    }
}
