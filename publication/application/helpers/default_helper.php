<?php

function get_request($get){
	$CI = & get_instance();
	return $CI->input->get($get);
}

function most_count_dosen(){
	$CI = & get_instance();
	$items = file_get_contents($CI->api.'/dosen_tertinggi?views=5');
	$items = json_decode($items, true);
	return $items;
}

function tahun_kegiatan_range(){
	$CI = & get_instance();
	$items = file_get_contents($CI->api.'/tahun_kegiatan_range?views=5');
	$items = json_decode($items, true);
	return $items;
}

function most_count_subject(){
	$CI = & get_instance();
	$items = file_get_contents($CI->api.'/subjectlistsorder?views=5');
	$items = json_decode($items, true);
	return $items;
}

function paging_creator($current_page,$total_page){
	$page_html = "";
	if($total_page>10){

		$page_html .= $current_page!=1?'<li><a class="paging" href="#" data-page="1">1</a></li>':'';
		if($current_page>4){
			$page_html .= '<li><a class="paging" href="#">...</a></li>';
		}
		for($i=$current_page-2;$i<$current_page;$i++){
			if($i<2) continue;
			$page_html.= '<li><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>';
		}
		$page_html .= '<li class="active"><a class="paging" href="#" data-page="'.$current_page.'">'.$current_page.'</a></li>';
		for($i=$current_page+1;$i<$current_page+3;$i++){
			if($i>$total_page-1) continue;
			$page_html.= '<li><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>';
		}
		if($current_page<$total_page-4){
			$page_html .= '<li><a class="paging" href="#">...</a></li>';
		}
		$page_html .= $current_page!=$total_page?'<li><a class="paging" href="#" data-page="'.$total_page.'">'.$total_page.'</a></li>':'';
	}else{
		for($i=1;$i<=$total_page;$i++){
			$page_html .= $current_page==$i?'<li class="active"><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>':'<li><a class="paging" href="#" data-page="'.$i.'">'.$i.'</a></li>';
		}
	}
	return $page_html;
}

function checkfilesize($url,$id){
	$CI = & get_instance();
	$filesize = strlen(file_get_contents($CI->download."/?type=$url&id=$id"));
	return formatSizeUnits($filesize);
}

function formatSizeUnits($bytes){
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }
    return $bytes;
}

?>