                       
                      
                      <div class="lead mb-10">Search Data</div>  

                      <div class="btn-toolbar mb-10"> 
                        <div class="btn-group btn-group-sm">
                          <button class="btn btn-default">A</button>
                          <button class="btn btn-default">B</button>
                          <button class="btn btn-default">C</button>
                          <button class="btn btn-default">D</button>
                          <button class="btn btn-default">E</button>
                          <button class="btn btn-default">F</button>
                          <button class="btn btn-default">G</button>
                          <button class="btn btn-default">H</button>
                          <button class="btn btn-default">I</button>
                          <button class="btn btn-default">J</button>
                          <button class="btn btn-default">K</button>
                          <button class="btn btn-default">L</button>
                          <button class="btn btn-default">M</button>
                          <button class="btn btn-default">N</button>
                          <button class="btn btn-default">O</button>
                          <button class="btn btn-default">P</button>
                          <button class="btn btn-default">Q</button>
                          <button class="btn btn-default">R</button>
                          <button class="btn btn-default">S</button>
                          <button class="btn btn-default">T</button>
                          <button class="btn btn-default">U</button>
                          <button class="btn btn-default">V</button>
                          <button class="btn btn-default">W</button>
                          <button class="btn btn-default">X</button>
                          <button class="btn btn-default">Y</button>
                          <button class="btn btn-default">Z</button>
                        </div>
                      </div> 
                           
                        <div class="row"> 

                            <div class="col-md-6">

                                <form action="" class="form-horizontal mb-10">
                                
                                <div class="input-group input-group-sm">

                                    <input type="text" class="form-control">
                                    <span class="input-group-btn"> 
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span class="fa fa-caret-down"></span></button>
                                        <button type="button" class="btn btn-primary btn-sm"> <i class="fa fa-search"></i> </button>
                                        <!-- s:menu --> 
                                        <div class="dropdown-menu"> 
                                          <div class="form-group input-group-sm">
                                              <div class="checkbox">
                                                <label> 
                                                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                                                  Title
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                                  Year
                                                </label>
                                              </div>  
                                               <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                                  Submit Date
                                                </label>
                                              </div>
                                              <div class="divider"></div> 

                                              <div class="checkbox">
                                                <label> 
                                                  <input type="radio" name="optionsRadiosa" id="optionsRadios4" value="option4" checked="">
                                                    Latest
                                                </label>
                                              </div> 
                                               <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosa" id="optionsRadios5" value="option5">
                                                    Oldest
                                                </label>
                                              </div>
                                              <div class="divider"></div> 

                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios6" value="option6" checked="">
                                                  5
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios7" value="option7">
                                                  10
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios8" value="option8">
                                                  20
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios9" value="option9">
                                                  40
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios10" value="option10">
                                                  60
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios11" value="option11">
                                                  80
                                                </label>
                                              </div> 
                                              <div class="checkbox">
                                                <label>
                                                  <input type="radio" name="optionsRadiosaa" id="optionsRadios12" value="option12">
                                                  100
                                                </label>
                                              </div> 
                                        </div>
                                      </div><!-- dropdown-menu -->

                                      <!-- s:menu -->
                                  </span>
                              </div>   
                              </form>
                              

                            </div><!-- col-md-6 .col-md-offset-6 -->

                        </div><!-- row --> 

                        <p class="text-muted">Hasil pencarian <b>"item"</b>, 539 hasil ditemukan</p> 