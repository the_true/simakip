<!DOCTYPE html>
<?php $page="title";?> 

<?php include "_head.php";?>

<body class="hold-transition  layout-boxed skin-uhamka sidebar-mini">

<div class="wrapper">  

      <?php include "_header.php";?> 
      
      <?php include "_sidebar.php";?> 

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
             Title
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-home"></i> Home </a></li>
              <li><a href="">Title</a></li> 
            </ol> 
          </section>

          <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx s:Main content xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
          <section class="content">  


              <div class="box"> 

                  <div class="box-body">  

                        <?php include "_search.php";?>
                        
                        <ul class="list_berita_1"> 

                              <li>
                                  <h2><a href="collection-publikasi-detail.php">Pengembangan Perangkat Pembelajaran Matematika dengan Strategi Knowledge Sharing untuk Meningkatkan Kemampuan Generalisasi dan Berpikir Reflektif
Matematis serta Self-Development Siswa SMA</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. - Publikasi Jurnal - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>

                               <li>
                                  <h2><a href="collection-buku-detail.php">Pengembangan Desain Pembelajaran Gema dalam Meningkatkan Kemampuan Pemecahan Masalah
Matematis Siswa SD</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. - Buku Ajar - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>

                               <li>
                                  <h2><a href="collection-pemakalah-detail.php">Studi Pemanfaatan Sayursan Sawi Hijau (Brassica juncea)
Sebagai Sumber Biokatalis Untuk Sintesis Senyawa Dimer Thymol dan Uji Bioaktif Sebagai Antioksidan</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. - Pemakalah Forum Ilmiah - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>

                              <li>
                                  <h2><a href="collection-kekayaan-detail.php">Kombinasi Ekstrak Etanol Rimpang Zingiber officinale
Roscoe Dengan Zn Sebagai Hipoglikemia, Hipolipidemia, dan Antiaterosklerosis Pada Mencit Diabetik Diet Tinggi
Kolesterol
</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. -  Hak Kekayaan Intelektual - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>

                              <li>
                                  <h2><a href="collection-luaran-detail.php">Implikasi Sosial Adopsi Teknologi
Komunikasi(Internet)di Kalangan Pondok Pesantren Muhammadiyah di Paciran, Kabupaten Lamongan, Jawa Timur
</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. - Luaran Lain - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>

                              <li>
                                  <h2><a href="collection-penelitian_mandiri-detail.php">Kajian Banding Struktur Cerita Bidadari Nusantara dan Muatan Budaya Lokal
</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. - Penelitian Mandiri - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>

                              <li>
                                  <h2><a href="collection-penelitian_internal-detail.php">Serunya Grebeg Penjalin di Desa Wisata Kerajinan Rotan Sukoharjo</a> </h2>
                                  <span class="author">Drs. Bunyamin, M.PdI. - Penelitian Internal - 2016</span>
                                  <div class="desc">
                                   Gunungan yang ditunggu-tunggu warga akhirnya tiba. Ratusan orang yang sudah menantinya, 
                                   langsung menyerbu ke arah gunungan dalam acara Grebeg Penjalin. 
                                   </div>
                              </li>
                              
                        </ul>
                        <div class="tc">
                            <ul class="pagination">
                                <li><a href="#">1</a></li>
                                <li class="active"><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                        </div>

                  </div><!-- box_body -->

              </div><!-- .box box-primary --> 

          </section>
          <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx e:Main content xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include "_footer.php";?>  
      
      <div class="control-sidebar-bg"></div>  
</div>
<!-- ./wrapper --> 


<!-- REQUIRED JS SCRIPTS -->
<?php include "_js.php";?>


</body>
</html>