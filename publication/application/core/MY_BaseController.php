<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_BaseController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct($twig_config=[]){
		parent::__construct();
		$twig_config["functions"][] = "most_count_dosen";
		$twig_config["functions"][] = "tahun_kegiatan_range";
		$twig_config["functions"][] = "most_count_subject";
		$twig_config["functions"][] = "get_request";
		$this->load->helper('form');
		$this->load->library('twig', $twig_config);
		$this->load->helper('url');
		$this->api = $this->config->item('server_url')."api";
		$this->download = $this->config->item('server_url')."download";
		$this->assets = $this->config->item('server_url')."uploads/billboard/";
 	}

    function _remap($method, $params=array())
    {
        $methodToCall = method_exists($this, $method) ? $method : 'index';
        return call_user_func_array(array($this, $methodToCall), $params);
    }

	public function index()
	{
		$this->twig->display('welcome_message');
	}

}
