<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Author extends My_BaseController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$items = file_get_contents($this->api.'/dosen_tertinggi?'.$_SERVER['QUERY_STRING']);
		// echo $items; die;
		$items = json_decode($items, true);
		$data["items"] = $items["items"];
		$data = array_merge($data,$items['paging']);
		$this->twig->display('author/index',$data);
	}

	public function lists($id,$nama){
		$items = file_get_contents($this->api."/union?dosen=$id&".$_SERVER['QUERY_STRING']);
		$items = json_decode($items, true);
		$data["items"] = $items["items"];
		$data["type"] = urldecode($nama);
		$data = array_merge($data,$items['paging']);
		$this->twig->display('author/lists',$data);
	}
}
