<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Welcome extends My_BaseController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
		$config = [
			'functions' => ['checkfilesize']
		];
        parent::__construct($config);
    }

	public function index()
	{
		$billboard = file_get_contents($this->api.'/billboard');
		$billboard = json_decode($billboard, true);

		$items = file_get_contents($this->api."/union?views=5&".$_SERVER['QUERY_STRING']);
		$items = json_decode($items, true);

		$data = [];
		$data["items"] = $items["items"];
		$data["download_url"] = $this->download;
		$data["billboard"] = $billboard;
		$data["url_assets"] = $this->assets;
		$data = array_merge($data,$items["paging"]);
		$this->twig->display('index',$data);
	}
}
