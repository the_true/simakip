/*
 Navicat Premium Data Transfer

 Source Server         : docker
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 127.0.0.1
 Source Database       : devel_simakip

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : utf-8

 Date: 03/07/2019 04:34:01 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `batch_lists_pengabdian`
-- ----------------------------
DROP TABLE IF EXISTS `batch_lists_pengabdian`;
CREATE TABLE `batch_lists_pengabdian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isdelete` smallint(6) DEFAULT '0',
  `start` timestamp NULL DEFAULT NULL,
  `finish` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `batch_lists_pengabdian`
-- ----------------------------
BEGIN;
INSERT INTO `batch_lists_pengabdian` VALUES ('3', 'batch  2', '2019-03-06 21:29:32', '2019-03-06 21:33:14', '0', '2019-03-07 00:00:00', '2019-03-23 00:00:00');
COMMIT;

-- ----------------------------
--  Table structure for `jenis_pengabdian`
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pengabdian`;
CREATE TABLE `jenis_pengabdian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `status` smallint(6) DEFAULT NULL,
  `isdelete` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `jenis_pengabdian`
-- ----------------------------
BEGIN;
INSERT INTO `jenis_pengabdian` VALUES ('1', 'Jenis Pengabdian update', '0', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
