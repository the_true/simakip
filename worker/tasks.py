from celery import Celery
from celery.schedules import crontab
import requests
import json
import os
from datetime import timedelta
redis_string = os.getenv('REDIS_URL', 'redis://localhost:6379/0')
app = Celery('tasks', broker=redis_string,backend=redis_string)

status = os.getenv('STATUS','LOCAL')
url = "http://127.0.0.1/simakip/"
queue = "local"
if status=="PRODUCTION":
    url = "http://simakip.uhamka.ac.id/"
    queue = "live"
elif status=="DEVELOPMENT":
    url="http://simakip.getality.com/"
    queue = "devel"
elif status=="DOCKER":
    url="http://web/"
    queue = "devel"
print url

app.conf.update(
timezone = 'Asia/Jakarta',
task_default_queue = queue,
CELERY_TASK_SERIALIZER='json',
CELERY_ACCEPT_CONTENT=['json'],  # Ignore other content
CELERY_RESULT_SERIALIZER='json',
CELERYBEAT_SCHEDULE =
{'penelitian_active': {
    'task': 'tasks.check_penelitian_active',
    'schedule': crontab(minute=10, hour=17),
    'args': (url,),
    'options': {'queue': queue}
},
'penelitian_inactive': {
    'task': 'tasks.check_penelitian_expire',
    'schedule': crontab(minute=10, hour=17),
    'args': (url,),
    'options': {'queue': queue}
},
'penelitian_finalreview': {
    'task': 'tasks.check_penelitian_finalreview',
    'schedule': crontab(minute=10, hour=17),
    'args': (url,),
    'options': {'queue': queue}
    },
'penelitian_finalreview': {
    'task': 'tasks.check_monev_review',
    'schedule': crontab(minute=10, hour=17),
    'args': (url,),
    'options': {'queue': queue}
    },
'pengabdian_finalreview': {
    'task': 'tasks.check_pengabdian_monev_review',
    'schedule': crontab(minute=10, hour=17),
    'args': (url,),
    'options': {'queue': queue}
    },
'pengabdian_monev_expired': {
    'task': 'tasks.check_pengabdian_monev_expired',
    'schedule': crontab(minute=10, hour=17),
    'args': (url,),
    'options': {'queue': queue}
    }
    
})

@app.task
def pencairan_dana_proses(url,notifs):
    for data in notifs:
        request = requests.post(url+'notif/pencairandanaproses',data={"list":json.dumps(data)});
        print request.content

@app.task
def pencairan_dana_confirm(url,notifs):
    for data in notifs:
        request = requests.post(url+'notif/pencairandanaconfirm',data={"list":json.dumps(data)});
        print request.content


@app.task
def send_notif(urlx,dosen,jenis,content):
    post = {
        "content":content,
        "dosen":dosen,
        "jenis":jenis
    }
    print url+'notif/addnotif'
    request = requests.post(url+'notif/addnotif',data=post)
    print request.content
    return True

@app.task
def penelitian_submit(url,id):
    request = requests.post(url+'notif/penelitiansubmit',data={"id":id},timeout=60);

@app.task
def pengabdian_submit(url,id):
    request = requests.post(url+'notif/pengabdiansubmit',data={"id":id},timeout=60);

@app.task
def send_mail_anggota(url,datas):
    for data in datas:
        request = requests.post(url+'notif/sendpenelitiananggota',data={"list":json.dumps(data)});
        print request.content

@app.task
def send_mail_anggota_pengabdian(url,datas):
    for data in datas:
        request = requests.post(url+'notif/sendpengabdiananggota',data={"list":json.dumps(data)});
        print request.content

@app.task
def send_mail_penelitian_dibuat(url,data):
    request = requests.post(url+'notif/sendpenelitiandibuat',data={"list":json.dumps(data)});
    print request.content

@app.task
def send_mail_penelitian_diperbaiki(url,data):
    request = requests.post(url+'notif/sendpenelitiandiperbaiki',data={"list":json.dumps(data)});
    print request.content

@app.task
def check_penelitian_expire(url):
    request = requests.get(url+'notif/checkpenelitianexpire');
    result_json = json.loads(request.content)
    for res in result_json:
        send_mail_expire.delay(url,res)

@app.task
def check_penelitian_finalreview(url):
    request = requests.get(url+'notif/checktanggalpengumuman');
    # result_json = json.loads(request.content)
    print "sukseslah"

@app.task
def check_pengabdian_monev_expired(url):
    request = requests.get(url+'notif/checkpengabdianmonevexpired');
    # result_json = json.loads(request.content)
    print "sukseslah"

@app.task
def check_monev_review(url):
    request = requests.get(url+'notif/checkmonevreview');
    # result_json = json.loads(request.content)
    print "sukseslah"
    
@app.task
def check_pengabdian_monev_review(url):
    request = requests.get(url+'notif/checkpengabdianmonevreview');
    # result_json = json.loads(request.content)
    print "sukseslah"

@app.task
def send_mail_expire(url,data):
    request = requests.post(url+'notif/sendpenelitianexpire',data={"list":json.dumps(data)});
    print request.content

@app.task
def check_perpanjang_penelitian(url,id_batch):
    request = requests.get(url+'notif/checkperpanjangpenelitian?batch='+str(id_batch));
    result_json = json.loads(request.content)
    for res in result_json:
        send_perpanjang_penelitian.delay(url,res)

@app.task
def send_perpanjang_penelitian(url,data):
    request = requests.post(url+'notif/sendperpanjangpenelitian',data={"list":json.dumps(data)});
    print request.content



@app.task
def check_penelitian_active(url):
    print url+'notif/checkpenelitianaktif'
    request = requests.get(url+'notif/checkpenelitianaktif');
    result_json = json.loads(request.content)
    # print result_json[0]
    for res in result_json:
        # send_mail_active.delay(url,res)
        request = requests.post(url+'notif/sendpenelitianaktif',data={"list":json.dumps(res)});
        print request.content

@app.task
def send_mail_active(url,data):
    print data
    request = requests.post(url+'notif/sendpenelitianaktif',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_valid(url,data):
    print data
    request = requests.post(url+'notif/penelitianvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def pengabdian_valid(url,data):
    print data
    request = requests.post(url+'notif/pengabdianvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_invalid(url,data):
    request = requests.post(url+'notif/penelitianinvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def pengabdian_invalid(url,data):
    request = requests.post(url+'notif/pengabdianinvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_reviewers(url,data):
    for dat in data:
        request = requests.post(url+'notif/penelitianreviewer',data={"list":json.dumps(dat)});
        print request.content

@app.task
def pengabdian_reviewers(url,data):
    for dat in data:
        request = requests.post(url+'notif/pengabdianreviewer',data={"list":json.dumps(dat)});
        print request.content

@app.task
def monevpenelitian_reviewers(url,data):
    for dat in data:
        request = requests.post(url+'notif/monevpenelitianreviewer',data={"list":json.dumps(dat)});
        print request.content
        # penelitian_reviewer.delay(url,dat)

@app.task
def monevpengabdian_reviewers(url,data):
    for dat in data:
        request = requests.post(url+'notif/monevpengabdianreviewer',data={"list":json.dumps(dat)});
        print request.content

@app.task
def penelitian_reviewer(url,data):
    request = requests.post(url+'notif/penelitianreviewer',data={"list":json.dumps(data)});
    print request.content

@app.task
def monev_penelitian_diterima(url,data):
    request = requests.post(url+'notif/monevpenelitianditerima',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_diterima(url,data):
    request = requests.post(url+'notif/penelitianditerima',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_diterimasyarat(url,data):
    request = requests.post(url+'notif/penelitianditerimasyarat',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_ditolak(url,data):
    request = requests.post(url+'notif/penelitianditolak',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_syarat_invalid(url,data):
    print data
    request = requests.post(url+'notif/penelitiansyaratinvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def penelitian_syarat_valid(url,data):
    print data
    request = requests.post(url+'notif/penelitiansyaratvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def laporan_submit(url,id):
    request = requests.post(url+'notif/laporansubmit',data={"id":id},timeout=60);

@app.task
def laporan_pengabdian_submit(url,id):
    request = requests.post(url+'notif/laporanpengabdiansubmit',data={"id":id},timeout=60);

@app.task
def laporan_valid(url,data):
    print data
    request = requests.post(url+'notif/laporanvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def laporan_pengabdian_valid(url,data):
    print data
    request = requests.post(url+'notif/laporanpengabdianvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def laporan_invalid(url,data):
    request = requests.post(url+'notif/laporaninvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def laporan_pengabdian_invalid(url,data):
    request = requests.post(url+'notif/laporanpengabdianinvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def suratkontrak_submit(url,id):
    request = requests.post(url+'notif/suratkontraksubmit',data={"id":id},timeout=60);

@app.task
def suratkontrak_valid(url,data):
    print data
    request = requests.post(url+'notif/suratkontrakvalid',data={"list":json.dumps(data)});
    print request.content

@app.task
def suratkontrak_invalid(url,data):
    request = requests.post(url+'notif/suratkontrakinvalid',data={"list":json.dumps(data)});
    print request.content
