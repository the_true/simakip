<?php

function get_request($get){
	$CI = & get_instance();
	return $CI->input->get($get);
}

function most_count_dosen(){
	$CI = & get_instance();
	$items = file_get_contents($CI->api.'/dosen_tertinggi?views=5');
	$items = json_decode($items, true);
	return $items;
}

function tahun_kegiatan_range(){
	$CI = & get_instance();
	$items = file_get_contents($CI->api.'/tahun_kegiatan_range?views=5');
	$items = json_decode($items, true);
	return $items;
}

function most_count_subject(){
	$CI = & get_instance();
	$items = file_get_contents($CI->api.'/subjectlistsorder?views=5');
	$items = json_decode($items, true);
	return $items;
}

?>