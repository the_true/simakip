<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Bukuajar extends My_BaseController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = [];
		$items = file_get_contents($this->api.'/bukuajar?'.$_SERVER['QUERY_STRING']);
		$items = json_decode($items, true);
		$data["items"] = $items["items"];
		$data["type"] = "Buku Ajar";
		$data = array_merge($data,$items['paging']);
		$this->twig->display('collections/bukuajar/index',$data);
	}

	public function detail($id){
		$items = file_get_contents($this->api.'/bukuajar?id='.$id);
		$items = json_decode($items, true);
		$data = $items["items"][0];
		if(count($items["items"][0]["subject_penelitian"])>0) $data["subject"] = $this->recursive_check($items["items"][0]["subject_penelitian"][0]["subject"]);
		$data["type"] = "Buku Ajar/Teks"; 
		$this->twig->display('collections/bukuajar/detail',$data);
	}

	private function recursive_check($items,$data=[]){
		if($items["parentnya"]){
			$data = $this->recursive_check($items["parentnya"],$data);
			$data[] = ["nama"=>$items["nama"],"id"=>$items["id"]];
			return $data;
		}else{
			return [["nama"=>$items["nama"],"id"=>$items["id"]]];
		}
	}
}
