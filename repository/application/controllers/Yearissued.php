<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Yearissued extends My_BaseController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$items = file_get_contents($this->api.'/tahun_kegiatan');
		$items = json_decode($items, true);
		$this->twig->display('year/index',["items"=>$items]);
	}

	public function lists($dari,$sampai){
		$items = file_get_contents($this->api."/union?dari=$dari&sampai=$sampai&".$_SERVER['QUERY_STRING']);
		$items = json_decode($items, true);
		$data["items"] = $items["items"];
		$data["type"] = "$dari - $sampai";
		$data = array_merge($data,$items['paging']);
		$this->twig->display('year/lists',$data);
	}
}
