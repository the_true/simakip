<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once("application/core/MY_BaseController.php");
class Search extends My_BaseController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$items = file_get_contents($this->api."/subjectlists?".$_SERVER['QUERY_STRING']);
		$subjects = file_get_contents($this->api.'/subject?'.$_SERVER['QUERY_STRING']);
		$subjects = json_decode($subjects, true);
		$items = json_decode($items, true);
		$data = [];
		$data["items"] = $items["items"];
		$data["subjects"] = $subjects;
		// $data["type"] = urldecode($nama);
		$data = array_merge($data,$items['paging']);
		$this->twig->display('global_search',$data);
	}
}
